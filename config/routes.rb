Handyworks::Application.routes.draw do


  # ORDER is important, the devise routes must appear before any associated
  #       restful route declarations on the same class.
  # Override the Devise::ConfirmationsController
  mount Ckeditor::Engine => '/ckeditor'

  mount StripeEvent::Engine => '/stripe'

  devise_for :users, :controllers => {
    :confirmations => 'confirmations',
    :registrations => 'registrations'
  }
  devise_scope :user do
    put "/confirm" => "confirmations#confirm", :as => :user_confirm
    # this route makes it easier to sign out a current session via either JS or manually with browser location
    #  - otherwise a special _method=delete parameter is required to match the devise default sign out path.
    get "users/sign_out" => "devise/sessions#destroy", :as => :get_destroy_user_session
  end

  scope '/admin' do
    resource :subscription, :only => [:edit, :destroy] do
      put 'update_plan', :to => 'subscriptions#update_plan'
      put 'update_card', :to => 'subscriptions#update_card'
    end
    resource :account, :only => [:edit, :update ] do
      resource :account_setup, :only => [:edit, :update], :path => 'setup'
    end
    # modify path to avoid conflict with devise routes for registration
    resources :users, :path => 'manage_users' do
      resource :permissions, :only => [ :edit, :update ]
      collection do
        get :permissions, :to => "permissions#edit_collection", :as => "edit_permissions"
        put :permissions, :to => "permissions#update_collection", :as => "permissions"
      end
    end
    resources :fee_schedules
    resources :import_groups do
      collection do
        get 'import_status'
      end
    end
    resources :imports
  end

  scope '/setup' do
    resources :providers do
      resource :schedule, :only => [:edit, :update]
    end
    resources :clinics
    resources :schedules, :only => [:show, :update]
    resources :legacy_id_labels, :only => [ :destroy ]
    resources :events, :only => [:create, :update, :destroy]
    resources :procedure_codes
    resources :diagnosis_codes
    resources :third_parties do
      resources :hcfa_form_options, :controller => 'carrier_hcfa_form_options', :except => [ :index ]
    end
    resources :attorneys, :as => :third_parties, :controller => 'third_parties'
    resources :insurance_carriers, :as => :third_parties, :controller => 'third_parties'
    resources :references
  end

  scope '/schedule' do
    resource :calendar, :only => [:show, :edit, :update], :as => "schedule" # has to use a different helper name the appointments calendar paths below
    resources :rooms
    resources :providers, :as => :provider_availability, :only => [:index]
    resources :appointment_types
  end

  scope '/billing' do
    resources :hcfa_form_options, :only => [ :index, :show, :edit, :update, :destroy ]
  end

  scope '/reporting' do
    resources :messages
  end

  scope '/interaction' do
    resources :patients do
      collection do
        get 'letters'
      end
    end
    resource :calendar, :only => [:show, :edit, :update]
  end

  # public appointments
  match '/appts' => 'public_appointments#index', :as => :public_appointments
  match '/appts/:account_id' => 'public_appointments#new', :as => :appointments_by_account
  match '/appts/:account_id/clinics/:clinic_id' => 'public_appointments#new', :as => :appointments_by_clinic
  match '/appts/:account_id/times' => 'public_appointments#times', :as => :appointments_times
  match '/appointments/export_pdf' => 'appointments#export_pdf'
  resource :calendar, :only => [:show, :edit, :update]
  resource :session_settings, :only => [:update]
  resources :appointments, :only => [:new, :show, :create, :update, :destroy]

  resources :subscription_plans, :only => [:index, :show, :create]

  # resources yet to be created
  # match 'patients' => 'home#under_construction', :as => :patients
  match 'cases' => 'home#under_construction', :as => :cases
  match 'transactions' => 'home#under_construction', :as => :transactions
  match 'reports' => 'home#under_construction', :as => :reports
  match 'billing' => 'home#under_construction', :as => :billing

  match 'plans' => 'subscription_plans#index', :as => 'plans'
  match 'news' => 'home#news', :as => 'news'
  match 'news_item' => 'home#news_item', :as => 'news_item'
  match 'contact' => 'home#contact', :as => 'contact'
  match 'features' => 'home#features', :as => 'features'
  match 'faq' => 'home#faq', :as => 'faq'

  get "content/basic"
  get "content/plus"
  get "content/premium"
  get "content/max"
  get "users/show"
  get "subscriptions/create"
  get "providers/getData" => "providers#getData"
  authenticated :user do
    root :to => 'home#index'
  end

  root :to => "home#index"

  #-----------------------------------------------------------------------------
  # dynamic routing to return Help information via AJAX requests.
  #   will match 
  #-----------------------------------------------------------------------------
  match 'help(/:label)' => 'helps#get_help', :as => 'get_help'

  #-----------------------------------------------------------------------------
  # field validation 
  #-----------------------------------------------------------------------------
  match 'validate(/:action/(:object(.:format)))' => 'validate#:action', :as => 'validate'

  scope '/mgmt' do
    resources :accounts
    resources :questions
    resources :helps
  end
  post '/contact_us/send_us'
  get '/webhooks/receiver' => 'webhooks#receiver'
  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
  #-----------------------------------------------------------------------------
  # setup the web interface to resque. going to require an app_admin role to
  # access this interface.
  #-----------------------------------------------------------------------------
  namespace :mgmt do
    constraints CanAccessResque do
      mount Resque::Server, :at => "/resque" 
    end
  end
  StripeEvent::Engine.routes.draw do
    post '/webhooks/receiver' => 'webhooks#receiver'
  end
end
