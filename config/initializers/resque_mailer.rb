#-------------------------------------------------------------------------------
# NOTE : This does not seem to have any effect on the current devise-async gem's
#        behavior ... it still queues them up.
#
# config/initializers/resque_mailer.rb
#-------------------------------------------------------------------------------
Resque::Mailer.excluded_environments = [:test, :cucumber]
