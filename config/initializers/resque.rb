require 'yaml'
rails_root = ENV['RAILS_ROOT'] || File.dirname(__FILE__) + '/../..'
rails_env = ENV['RAILS_ENV'] || 'development'

resque_config = YAML.load_file(rails_root + '/config/resque.yml')

redis_config = resque_config[rails_env]

# NOTE: the namespace configuration must be after the instantiation of the
#       Redis connection otherwise it gets overwritten to the default resque:
#       namespace.
Resque.redis = Redis.new(:host => redis_config['host'],
                         :port => redis_config['port'],
                         :password => redis_config['pwd'])


# use a separate namespace
#  there were some problems with something in the default expression being nil when
#  run on Amazon AWS, so switched to a static configuration for production. This 
#  makes more sense given the "elastic" nature of the production deployment. Its
#  quite possible the first character of the environment HOSTNAME could change if
#  the application gets migrated to a different physical production server, or if we
#  scale to the point where we've got multiple request processing servers.
#
if Rails.env == "production"
  Resque.redis.namespace = "resque:PA"
else
  Resque.redis.namespace = "resque:#{Rails.env[0..0].upcase}#{ENV['HOSTNAME'].upcase}"
end


# configure resque scheduler
require 'resque_scheduler'
require 'resque_scheduler/server'
Resque.schedule = YAML.load_file(rails_root + '/config/schedule.yml')

#-------------------------------------------------------------------------------
# Monkey Patch the Resque encode method
#  what was happening was after installing the Backbone.js module which wants
#  to format JSON without a root element, 
#
# This did not solve my problem as the class being encoded was not the model
# itself, but confirmation_instructions maybe, but it did work as intended.
#-------------------------------------------------------------------------------
#module Resque
#  module Helpers
#    def encode_with_root(object)
#      r = encode_without_root(object)
#      if object.class.ancestors.include?(ActiveRecord::Base) and !ActiveRecord::Base.include_root_in_json
#        r = "{\"#{object.class.model_name.element}\":#{r}}"
#      end
#      return r
#    end
#
#    alias encode_without_root encode
#    alias encode encode_with_root
#  end
#end
