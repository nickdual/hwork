# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format
# (all these examples are active by default):
# ActiveSupport::Inflector.inflections do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
# end
#
# These inflection rules are supported but not enabled by default:
ActiveSupport::Inflector.inflections do |inflect|
  inflect.human 'ThirdParty', '3rd party'
  inflect.human 'ThirdParties', '3rd parties'
  inflect.human 'third_party', '3rd party'
  inflect.human 'Reference', 'Referral'
  inflect.human 'References','Referrals'
  inflect.human 'reference', 'referral'
end
