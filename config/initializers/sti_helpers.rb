
#-----------------------------------------------------------------------------
# For STI classes in development environments, need to make sure they are
# loaded in correct order initially, if the subclass gets loaded prior to 
# the base class you may end up missing some functionality.
#-----------------------------------------------------------------------------
# sti class loading
##if Rails.env.development?
##  %w[hcfa_form_option carrier_hcfa_form_option account_hcfa_form_option].each do |c|
##    require_dependency File.join("app","models","#{c}.rb")
##  end
##end
