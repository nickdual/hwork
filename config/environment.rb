# check to make sure the HOSTNAME is set if not set it to something
# the Hostname is used in lib/payment_processing and config/initializers/resque
# and if not present casue the system to fail.
unless ENV['HOSTNAME']
  require "socket"
  ENV['HOSTNAME'] = Socket.gethostname
end
# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Handyworks::Application.initialize!

