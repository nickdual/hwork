source 'http://rubygems.org'

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
gem 'rails', '3.2.11'
gem 'thin' # fixes warning from WeBrick
gem 'mysql2'
gem "stripe" # payment processing
gem "stripe_event"
gem "haml", ">= 3.1.7"
gem "devise", ">= 2.1.2"
gem "devise-async" # supports asynchronous notifications through devise.
gem "cancan", ">= 1.6.8"
gem "rolify", ">= 3.2.0"
gem "simple_form", ">= 2.0.4"
gem "figaro", ">= 0.5.0"
gem "american_date" # fix ruby 1.9 date parsing for US formats
gem "gritter"
gem 'paper_trail', '~> 2' # active record auditing
gem 'rabl' # support for more structured json data API's

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
gem 'formtastic' # form builder, deprecate this gem and transition to simple_form
gem 'formtastic-bootstrap' # form builder compatible with bootstrap
gem 'will_paginate' # for server side managed pagination
gem 'default_value_for'
gem 'foreigner'
gem 'oily_png', :require => false # improves compass stylesheet compilation times for png image sprites
gem 'yajl-ruby', :require => false # fast json processing
gem 'resque', :require => 'resque/server' # background processing
gem 'resque_mailer' #background processing for emails.
gem 'resque-scheduler' # recurring schedule for resque
gem 'paperclip' # file storage 
gem 'state_machine' # adds workflow support to ActiveRecord
gem 'rpm_contrib' # newrelic add-on which supports monitoring background jobs e.g. Resque
gem 'newrelic_rpm' # production process monitoring
gem 'money-rails'
# gem 'aws-s3', :require => 'aws/s3'
gem 'execjs'
gem 'therubyracer'
gem 'wicked_pdf'

gem "wkhtmltopdf-binary", "~> 0.9.9.1"

#-------------------------------------------------------------------------------
# Reporting Tools
#   - ruport appears to be abandoned, need to research more current solutions
#-------------------------------------------------------------------------------
#gem 'ruport'
#gem 'ruport-util'
#gem 'acts_as_reportable'

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'
  gem 'uglifier', '>= 1.0.3'
  gem 'jquery-rails'
  gem "bootstrap-sass", ">= 2.1.1.0"
  gem "rails-backbone" # rails backbone integration
  gem "backbone-support" # thoughtbot custom backbone extensions (swaping router composite view)
  gem 'compass-rails'
  gem 'ruby-haml-js' # compiles haml.js templates under JST namespace.
  gem 'jquery-ui-rails'
  gem 'jquery-datatables-rails' #, github: 'rweng/jquery-datatables-rails'
  gem 'ckeditor', :git => "git://github.com/galetahub/ckeditor.git", # includes js wysiwyg editor, edge version for 4.0.1 update
                  :ref => "91e7b1bb2deede00c43a7f2fbe913e0354cff9f0"
  gem 'font-awesome-sass-rails', "~> 3.0.0" # extended font and icons for twitter bootstrap
  gem 'fancybox2-rails', :git => "git://github.com/kyparn/fancybox2-rails.git", # exposes fancybox jQuery to asset pipeline
                         :ref => "a73e588299da0756acda629732b4c610616fa366"
  gem 'datejs-rails' # exposes updated date.js library to asset pipeline
  gem 'farbtastic-rails' # exposes the farbtastic color picker
  gem 'jstree-rails' # exposes jstree plugin
  gem 'jquery-layout-rails'
end

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
group :development do
  gem 'sqlite3'
  gem "haml-rails", ">= 0.3.5"
  gem "hpricot", ">= 0.8.6", :require => false
  gem "ruby_parser", ">= 3.1.0", :require => false
  gem "quiet_assets", ">= 1.0.1", :require => false
  gem "meta_request" # supports the Chrome plugin RailsPanel
  # allow guard and its dependency listen gem to select the best available
  # filesystem monitoring gem
  gem 'rb-inotify', '~> 0.8.8', :require => false
  gem 'rb-fsevent', :require => false
  gem 'rb-fchange', :require => false
end

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
group :test do
  gem "database_cleaner", ">= 0.9.1"
  gem "email_spec", ">= 1.4.0"
  gem "action_mailer_cache_delivery" # required for using email_spec with Selenium
  gem "cucumber-rails", ">= 1.3.0", :require => false
  gem "launchy", ">= 2.1.2"
  gem "capybara", ">= 2.0.1"
  gem "capybara-firebug" # supports debugging Selenium tests with FireBug plugin
  gem "shoulda-matchers"
  gem "guard-spork"
  gem "spork", "~> 0.9.0.rc"
  #
  #gem "capybara-webkit"
  #gem 'sham_rack'
  #gem 'vcr'
  #gem 'fakeweb'
  #gem 'simplecov', :require => false  # code coverage, replacement for Rcov which is not compatible with Ruby 1.9
  #gem 'simplecov-rcov', '0.2.3', :require => false  # formats simplecov output in rcov format to integrate with Jenkins
  #gem 'pickle' # for email testing
  #
  #gem 'turn', :require => false
  #gem "bourne"
  #gem "nokogiri"
end

#-------------------------------------------------------------------------------
# Bundle gems for the local environment. Make sure to
# put test-only gems in this group so their generators
# and rake tasks are available in development mode:
#-------------------------------------------------------------------------------
group :development, :test do
  gem "better_errors", ">= 0.2.0" # fancy display of errors in browser
  gem "binding_of_caller", ">= 0.6.8", :require => false # enhances better errors with interactive console
  gem "rspec-rails", ">= 2.11.4"
  gem "rspec" # FIXME this dependency is required by the "edge" shoulda-matchers gem, but really this should be considered a bug in that gem.
  #gem "debugger"
  gem 'pry' # use alternative IRB shell.
  #gem 'pry-remote'
  gem 'pry-stack_explorer' # enable use of pry for ruby debugging
  gem 'pry-debugger' # enable the use of navigation commands (next, step, continue) for pry debugging.
  gem "factory_girl_rails", ">= 4.1.0"
  gem 'jasminerice' # javascript testing like rspec w/ coffeescript
  gem 'guard-jasmine',"~> 1.15.1"
end

#-------------------------------------------------------------------------------
# Define a group of gems which are required by rake tasks, but not required
# in any Rails server environments
#-------------------------------------------------------------------------------
group :cli do
  gem 'highline', :require => false # provides masked command line password prompts
end
