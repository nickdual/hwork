require 'resource_seeder'
#-------------------------------------------------------------------------------
# Clinics
#  these are loaded from a CSV file initially exported from Access Handyworks
#-------------------------------------------------------------------------------
account = Account.find(Account::DEFAULT_ACCOUNT_ID)
ResourceSeeder.seed_clinics(account)
