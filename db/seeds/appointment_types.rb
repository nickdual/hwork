#-------------------------------------------------------------------------------
# Appointment Types
#  loads default single service type
#-------------------------------------------------------------------------------
account = Account.find(Account::DEFAULT_ACCOUNT_ID)
AppointmentType.create!(
  :name => 'Initial Consult',
  :is_public => true,
  :for_new_patients => true,
  :account_id => account.id) if AppointmentType.where(:account_id => account.id, :name => 'Initial Consult').empty?
AppointmentType.create!(
  :name => 'Adjustment',
  :is_public => false,
  :duration => 30,
  :provider_time => 30,
  :account_id => account.id) if AppointmentType.where(:account_id => account.id, :name => 'Adjustment').empty?
AppointmentType.create!(
  :name => 'Massage',
  :is_public => true,
  :for_new_patients => true,
  :duration => 30,
  :provider_time => 30,
  :duration_max => 120,
  :account_id => account.id) if AppointmentType.where(:account_id => account.id, :name => 'Massage').empty?
