#-------------------------------------------------------------------------------
# HCFA Form Option Defaults
#  loads the default HCFA forms by state into the DEFAULT account
#  these are loaded from a CSV file initially exported from Access Handyworks
#-------------------------------------------------------------------------------
require 'resource_seeder'
account = Account.find(Account::DEFAULT_ACCOUNT_ID)
ResourceSeeder.seed_hcfa_form_options(account)
