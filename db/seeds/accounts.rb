#-------------------------------------------------------------------------------
# Accounts
#  - creates the GLOBAL DEFAULTS account to receive default configuration
#     values for various pieces of the application (e.g. default permissions
#     configuration.
#  - this account should be the first thing loaded when seeding new database
#-------------------------------------------------------------------------------
if Account.where(:id => Account::DEFAULT_ACCOUNT_ID).empty?
  account = Account.new(
    :clinic_name => "GLOBAL DEFAULTS")
  account.id = Account::DEFAULT_ACCOUNT_ID
  account.state = 'active'
  account.save!
end
