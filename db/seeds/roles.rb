#-------------------------------------------------------------------------------
# Roles
#   - create first the global roles
#   - then use the Permission API to create the roles defined in
#     config/permission.yml
#-------------------------------------------------------------------------------
Role.create!( :name => 'ApplicationAdmin', :description => 'Super Admin Role for Application Administration') unless Role.find_by_name('ApplicationAdmin')
Role.create!( :name => 'AccountAdmin', :description => 'Account Administrator, including subscription plan options') unless Role.find_by_name('AccountAdmin')
Role.create!( :name => 'Staff', :description => 'Clinic Office Staff') unless Role.find_by_name('Staff')
Permission.initialize_all_roles()
