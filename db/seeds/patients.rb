#-------------------------------------------------------------------------------
# Patients
#  these are loaded from a CSV file initially exported from Access Handyworks
#-------------------------------------------------------------------------------
require 'resource_seeder'
account = Account.find(Account::DEFAULT_ACCOUNT_ID)
ResourceSeeder.seed_patients(account)
