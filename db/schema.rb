# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130418080338) do

  create_table "account_setups", :force => true do |t|
    t.string   "state"
    t.integer  "account_id"
    t.boolean  "first_session"
    t.boolean  "clinics"
    t.boolean  "providers"
    t.boolean  "schedules"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "accounts", :force => true do |t|
    t.string   "full_domain"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "state"
    t.string   "clinic_name"
  end

  add_index "accounts", ["full_domain"], :name => "index_accounts_on_full_domain"

  create_table "addresses", :force => true do |t|
    t.string   "street"
    t.string   "street2"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "label"
    t.string   "addressable_id"
    t.string   "addressable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "addresses", ["addressable_id", "addressable_type", "label"], :name => "addressable_idx"

  create_table "appointment_types", :force => true do |t|
    t.string   "name"
    t.integer  "duration_min",                                        :default => 30
    t.integer  "duration_max",                                        :default => 90
    t.integer  "schedule_lead_time",                                  :default => 0
    t.integer  "pre_appointment_gap",                                 :default => 0
    t.integer  "post_appointment_gap",                                :default => 0
    t.integer  "duration",                                            :default => 60
    t.string   "description"
    t.boolean  "disabled",                                            :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_public",                                           :default => false
    t.boolean  "for_new_patients",                                    :default => false
    t.integer  "account_id"
    t.integer  "can_create_deadline",                                 :default => 3
    t.integer  "can_change_deadline",                                 :default => 3
    t.integer  "can_cancel_deadline",                                 :default => 3
    t.integer  "max_number_of_appointments_user_can_book_in_advance", :default => 6
    t.boolean  "auto_confirm_appointment_create",                     :default => true
    t.boolean  "auto_confirm_appointment_change",                     :default => true
    t.boolean  "auto_confirm_appointment_delete",                     :default => true
    t.boolean  "allow_user_to_change_appointment",                    :default => true
    t.boolean  "allow_user_to_cancel_appointment",                    :default => true
    t.boolean  "allow_user_to_delete_appointment",                    :default => true
    t.string   "appointment_color",                                   :default => "#e5804a"
    t.integer  "provider_time",                                       :default => 60
  end

  add_index "appointment_types", ["account_id"], :name => "appointment_types_accounts_fk"

  create_table "appointments", :force => true do |t|
    t.integer  "provider_id"
    t.integer  "room_id"
    t.integer  "contact_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "calendar_id"
    t.integer  "appointment_type_id"
    t.string   "state",               :default => "new"
    t.string   "alt_id"
  end

  add_index "appointments", ["appointment_type_id"], :name => "appointments_appointment_types_fk"
  add_index "appointments", ["calendar_id"], :name => "appointment_calendars_fk"
  add_index "appointments", ["provider_id"], :name => "appointment_provider_fk"

  create_table "calendars", :force => true do |t|
    t.integer  "account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "provider_schedule_by_clinic", :default => false
    t.integer  "days",                        :default => 1
    t.integer  "version",                     :default => 0
    t.integer  "time_slots_per_hour",         :default => 4
    t.string   "view_by",                     :default => "room"
  end

  add_index "calendars", ["account_id"], :name => "calendars_accounts_fk"

  create_table "case_carriers", :force => true do |t|
    t.integer  "order",                           :default => 0
    t.integer  "case_id"
    t.integer  "carrier_id"
    t.integer  "guarantor_id"
    t.string   "guarantor_relation", :limit => 1, :default => "S"
    t.boolean  "assigned",                        :default => false
    t.boolean  "hold",                            :default => false
    t.string   "hcfa_box_1a"
    t.string   "policy_number"
    t.string   "group_number"
    t.string   "authorization"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "alt_carrier_id"
    t.string   "alt_guarantor_id"
    t.integer  "deductible_cents",                :default => 0
    t.integer  "paid_cents",                      :default => 0
  end

  add_index "case_carriers", ["carrier_id"], :name => "index_case_carriers_on_carrier_id"
  add_index "case_carriers", ["case_id"], :name => "index_case_carriers_on_case_id"
  add_index "case_carriers", ["guarantor_id"], :name => "index_case_carriers_on_guarantor_id"

  create_table "case_diagnoses", :force => true do |t|
    t.integer  "case_id"
    t.integer  "diagnosis_code_id"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "alt_diagnosis_code_id"
  end

  add_index "case_diagnoses", ["case_id"], :name => "index_case_diagnoses_on_case_id"
  add_index "case_diagnoses", ["diagnosis_code_id"], :name => "case_diagnoses_diagnosis_codes_fk"

  create_table "cases", :force => true do |t|
    t.integer  "account_id"
    t.integer  "patient_id"
    t.integer  "provider_id"
    t.string   "description"
    t.date     "similar_symptoms_date"
    t.boolean  "employment_related",                           :default => false
    t.boolean  "auto_accident",                                :default => false
    t.boolean  "non_auto_accident",                            :default => false
    t.date     "onset_date"
    t.string   "prior_authorization"
    t.date     "first_treatment_date"
    t.date     "accident_date"
    t.date     "hcfa_accident_date"
    t.string   "hcfa_box19_accident_description"
    t.string   "treatment_phase",                 :limit => 1, :default => "A"
    t.integer  "fee_schedule_id"
    t.boolean  "active",                                       :default => true
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "alt_id"
    t.string   "alt_patient_id"
    t.string   "alt_provider_id"
    t.string   "alt_attorney_id"
    t.string   "alt_referral_id"
    t.string   "alt_fee_schedule_id"
  end

  add_index "cases", ["account_id"], :name => "index_cases_on_account_id"
  add_index "cases", ["fee_schedule_id"], :name => "cases_fee_schedules_fk"
  add_index "cases", ["patient_id"], :name => "index_cases_on_patient_id"
  add_index "cases", ["provider_id"], :name => "index_cases_on_provider_id"

  create_table "ckeditor_assets", :force => true do |t|
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], :name => "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_ckeditor_assetable_type"

  create_table "clinics", :force => true do |t|
    t.string   "tax_uuid"
    t.string   "type_ii_npi_uuid"
    t.integer  "account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "alt_id"
    t.integer  "schedule_future_max_days",  :default => 30
    t.integer  "schedule_history_max_days", :default => 60
    t.boolean  "master_billing_address",    :default => false
    t.integer  "location_clinic_id"
    t.string   "website_url"
  end

  add_index "clinics", ["account_id"], :name => "clinics_accounts_fk"

  create_table "contact_us", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "city"
    t.string   "phone"
    t.text     "comment"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "contacts", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "company_name"
    t.string   "attention"
    t.text     "notes"
    t.string   "title"
    t.integer  "contactable_id"
    t.string   "contactable_type"
    t.string   "occupation"
    t.string   "middle_initial"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type"
  end

  add_index "contacts", ["contactable_type", "contactable_id"], :name => "contactable_idx"

  create_table "diagnosis_codes", :force => true do |t|
    t.string   "name"
    t.string   "code"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "account_id"
    t.string   "alt_id"
    t.string   "icd10"
  end

  add_index "diagnosis_codes", ["account_id"], :name => "diagnosis_codes_accounts_fk"

  create_table "emails", :force => true do |t|
    t.string   "email"
    t.integer  "emailable_id"
    t.string   "emailable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "label"
  end

  create_table "employers", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "event_locations", :force => true do |t|
    t.integer  "event_id"
    t.integer  "clinic_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "event_locations", ["event_id"], :name => "event_locations_events_fk"

  create_table "events", :force => true do |t|
    t.integer  "day_of_week"
    t.date     "start_date"
    t.time     "start_time"
    t.integer  "lead_time"
    t.integer  "lag_time"
    t.integer  "eventable_id"
    t.string   "eventable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "duration"
    t.boolean  "free",           :default => false
    t.string   "title"
    t.text     "notes"
  end

  add_index "events", ["eventable_id", "eventable_type"], :name => "events_eventable_idx"

  create_table "fee_schedules", :force => true do |t|
    t.string   "label"
    t.integer  "account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "alt_id"
  end

  add_index "fee_schedules", ["account_id"], :name => "fee_schedules_accounts_fk"

  create_table "fees", :force => true do |t|
    t.boolean  "is_percentage",                                                  :default => true
    t.integer  "procedure_code_id"
    t.integer  "fee_schedule_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "alt_id"
    t.integer  "fee_cents",                                                      :default => 0
    t.integer  "copay_cents",                                                    :default => 0
    t.decimal  "copay_pct",                        :precision => 8, :scale => 7, :default => 1.0
    t.integer  "expected_insurance_payment_cents",                               :default => 0
  end

  add_index "fees", ["fee_schedule_id"], :name => "fees_fee_schedules_fk"
  add_index "fees", ["procedure_code_id"], :name => "fees_procedure_codes_fk"

  create_table "hcfa_form_options", :force => true do |t|
    t.integer  "form_type_code"
    t.string   "state"
    t.integer  "third_party_id"
    t.string   "form_name"
    t.integer  "plan_name"
    t.integer  "address"
    t.integer  "box4_g_name_is_same"
    t.integer  "box4_7"
    t.integer  "box5"
    t.integer  "box6"
    t.integer  "box7_g_addr_is_same"
    t.integer  "box7_g_addr_is_p_addr"
    t.integer  "box8"
    t.integer  "box9"
    t.integer  "box9a"
    t.integer  "box9c"
    t.integer  "box9d"
    t.integer  "box9_s_name_is_same"
    t.integer  "box10"
    t.integer  "box11"
    t.integer  "box11a"
    t.integer  "box11c"
    t.integer  "box11d"
    t.string   "date_auth"
    t.string   "date_format"
    t.string   "box17_upin"
    t.string   "box17_name"
    t.string   "box17_npi"
    t.string   "box19"
    t.integer  "outside_lab"
    t.integer  "print_diagnosis_description"
    t.integer  "box21"
    t.integer  "date_to"
    t.integer  "date_from"
    t.string   "box24_tos"
    t.string   "box24_pos"
    t.integer  "box24_cpt_97014"
    t.string   "box24_modifier"
    t.integer  "box24_diags"
    t.string   "box24_amount_format"
    t.string   "box24j_npi"
    t.string   "box24j_legacy"
    t.string   "box24i"
    t.string   "box25"
    t.string   "box27_30_format"
    t.integer  "amount_paid"
    t.integer  "total_owed"
    t.integer  "box27"
    t.integer  "box29"
    t.string   "license"
    t.integer  "box31_provider_code"
    t.integer  "clin_phone"
    t.integer  "box32"
    t.string   "box32_name"
    t.integer  "box32a"
    t.string   "box32b"
    t.string   "box33"
    t.string   "box33_name"
    t.string   "box33a"
    t.integer  "box33b"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "account_id"
    t.integer  "box1a"
    t.string   "type"
    t.string   "alt_id"
  end

  add_index "hcfa_form_options", ["account_id"], :name => "index_hcfa_form_options_on_account_id"
  add_index "hcfa_form_options", ["third_party_id"], :name => "index_hcfa_form_options_on_third_party_id"

  create_table "helps", :force => true do |t|
    t.string   "label"
    t.text     "body"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "title"
  end

  create_table "import_groups", :force => true do |t|
    t.string   "comment"
    t.integer  "duplicate_code",    :default => 2
    t.integer  "error_code",        :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "account_id"
    t.integer  "user_id"
    t.string   "import_group_type"
    t.string   "status",            :default => "Pending"
    t.integer  "clinic_id"
  end

  add_index "import_groups", ["account_id"], :name => "import_groups_accounts_fk"

  create_table "import_records", :force => true do |t|
    t.integer  "import_id"
    t.integer  "status",     :default => 0
    t.text     "record"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "model_type"
    t.integer  "model_id"
  end

  add_index "import_records", ["import_id"], :name => "import_records_imports_fk"

  create_table "imports", :force => true do |t|
    t.integer  "account_id"
    t.integer  "user_id"
    t.string   "import_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "import_group_id"
    t.string   "data_file_name"
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.datetime "data_updated_at"
    t.string   "status",            :default => "Pending"
  end

  add_index "imports", ["account_id"], :name => "imports_accounts_fk"
  add_index "imports", ["import_group_id"], :name => "imports_import_groups_fk"

  create_table "legacy_id_labels", :force => true do |t|
    t.string   "label"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "account_id"
    t.string   "alt_id"
  end

  add_index "legacy_id_labels", ["account_id"], :name => "legacy_id_labels_accounts_fk"

  create_table "limits", :force => true do |t|
    t.integer  "plan_id"
    t.string   "name"
    t.integer  "value",      :limit => 8, :default => 0,        :null => false
    t.string   "value_type",              :default => "number", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "limits", ["plan_id"], :name => "index_limits_on_plan_id"

  create_table "messages", :force => true do |t|
    t.string   "type"
    t.string   "label"
    t.integer  "messageable_id"
    t.string   "messageable_type"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "alt_id"
    t.string   "message_format",   :default => "Letter (snail)"
  end

  create_table "new_patient_options", :force => true do |t|
    t.integer  "clinic_id"
    t.string   "internal_account_uid_scheme"
    t.string   "additional_transaction_number"
    t.string   "patient_number_scheme"
    t.string   "transaction_number_scheme"
    t.boolean  "should_send_statements_when_overdue"
    t.boolean  "should_charge_overdue_account"
    t.string   "insurance_carrier_assignment_policy"
    t.integer  "payment_display_code"
    t.boolean  "should_split_bills_by_provider"
    t.integer  "default_place_of_service"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "default_provider_id"
    t.float    "overdue_fee_percentage"
    t.string   "default_provider_alt_id"
  end

  add_index "new_patient_options", ["clinic_id"], :name => "new_patient_options_clinics_fk"
  add_index "new_patient_options", ["default_provider_id"], :name => "new_patient_options_providers_fk"

  create_table "patients", :force => true do |t|
    t.integer  "clinic_id"
    t.date     "birthdate"
    t.integer  "employer_id"
    t.string   "ssn"
    t.string   "spouse_name"
    t.boolean  "is_active",                      :default => true
    t.string   "statement_message"
    t.string   "address_stationery_to_label"
    t.integer  "marital_status_code",            :default => 5
    t.integer  "disability_status_code",         :default => 0
    t.boolean  "should_send_overdue_statements", :default => true
    t.integer  "overdue_fee_percentage"
    t.text     "notes"
    t.string   "account_code"
    t.integer  "employment_status_code",         :default => 0
    t.string   "category"
    t.integer  "parent_patient_id"
    t.integer  "import_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "account_id"
    t.string   "alt_id"
    t.integer  "student_status_code",            :default => 0
    t.string   "clinic_alt_id"
    t.string   "race"
    t.string   "ethnicity"
    t.string   "language"
    t.boolean  "smoker"
    t.string   "nick_name"
    t.integer  "sex",                            :default => 0
  end

  add_index "patients", ["account_id"], :name => "patients_accounts_fk"
  add_index "patients", ["clinic_id"], :name => "patients_clinics_fk"
  add_index "patients", ["employer_id"], :name => "patients_employers_fk"

  create_table "phones", :force => true do |t|
    t.string   "number"
    t.integer  "phoneable_id"
    t.string   "phoneable_type"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "label"
    t.string   "country",        :default => "+1"
    t.boolean  "primary",        :default => false
  end

  create_table "procedure_codes", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "type_code"
    t.integer  "service_type_code"
    t.string   "modifier"
    t.string   "modifier2"
    t.string   "modifier3"
    t.string   "cpt_code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "tax_rate_percentage"
    t.integer  "account_id"
    t.string   "alt_id"
  end

  add_index "procedure_codes", ["account_id"], :name => "procedure_codes_accounts_fk"

  create_table "provider_appointment_types", :force => true do |t|
    t.integer  "provider_id"
    t.integer  "appointment_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "provider_legacy_id_labels", :force => true do |t|
    t.integer  "legacy_id_label_id"
    t.integer  "provider_id"
    t.string   "legacy_id_value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "alt_id"
  end

  add_index "provider_legacy_id_labels", ["legacy_id_label_id"], :name => "plil_legacy_id_labels_fk"
  add_index "provider_legacy_id_labels", ["provider_id"], :name => "plil_providers_fk"

  create_table "provider_precedences", :force => true do |t|
    t.integer  "room_id"
    t.integer  "provider_id"
    t.integer  "order"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "provider_precedences", ["provider_id"], :name => "provider_precedences_providers_fk"
  add_index "provider_precedences", ["room_id"], :name => "provider_precedences_rooms_fk"

  create_table "providers", :force => true do |t|
    t.string   "signature_name"
    t.string   "provider_type_code"
    t.string   "tax_uid"
    t.text     "notes"
    t.string   "npi_uid"
    t.integer  "account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "appointment_color",  :default => "#68a1e5"
    t.string   "alt_id"
    t.string   "tag_line"
    t.string   "your_code"
  end

  add_index "providers", ["account_id"], :name => "providers_accounts_fk"

  create_table "questions", :force => true do |t|
    t.string   "title"
    t.text     "body"
    t.string   "state"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "recurrences", :force => true do |t|
    t.integer  "event_id"
    t.integer  "interval"
    t.integer  "count"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "interval_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "recurrences", ["event_id"], :name => "recurrences_events_fk"

  create_table "references", :force => true do |t|
    t.integer  "account_id"
    t.string   "upin"
    t.string   "npi"
    t.string   "alt_id"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "references", ["account_id"], :name => "references_accounts_fk"

  create_table "referrals", :force => true do |t|
    t.integer  "referrable_id"
    t.string   "referrable_type"
    t.integer  "reference_id"
    t.string   "reference_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "alt_reference_id"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "resource_type"
    t.integer  "resource_id"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "role_resource_idx"
  add_index "roles", ["name"], :name => "role_name_idx"

  create_table "room_appointment_types", :force => true do |t|
    t.integer  "room_id"
    t.integer  "appointment_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "room_locations", :force => true do |t|
    t.integer  "room_id"
    t.integer  "clinic_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "room_locations", ["clinic_id"], :name => "room_locations_clinics_fk"
  add_index "room_locations", ["room_id"], :name => "room_locations_rooms_fk"

  create_table "rooms", :force => true do |t|
    t.integer  "account_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "alt_id"
    t.integer  "appointment_type_id"
    t.integer  "position",            :default => 0
  end

  add_index "rooms", ["appointment_type_id"], :name => "rooms_appointment_types_fk"

  create_table "schedules", :force => true do |t|
    t.integer  "schedulable_id"
    t.string   "schedulable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "max_concurrent_appointments", :default => 1
    t.integer  "max_appointments_per_hour",   :default => 3
  end

  create_table "subscription_plans", :force => true do |t|
    t.string   "name"
    t.integer  "user_limit"
    t.integer  "renewal_period",                        :default => 1
    t.integer  "trial_period",                          :default => 1
    t.integer  "patient_limit"
    t.integer  "events_limit"
    t.integer  "emarketing_campaigns_limit_per_month"
    t.integer  "clinic_limit"
    t.integer  "office_staff_limit"
    t.integer  "provider_limit"
    t.integer  "file_storage_limit_megabytes"
    t.boolean  "is_import_export_limited"
    t.boolean  "is_hcfa_printing_limited"
    t.boolean  "is_invoicing_limited"
    t.boolean  "time_tracking"
    t.boolean  "is_appointment_scheduling_limited"
    t.boolean  "online_patient_scheduling"
    t.boolean  "website_templates"
    t.boolean  "vendor_integration"
    t.boolean  "inventory_tracking"
    t.boolean  "own_domain_name"
    t.boolean  "soap_note_report_writer"
    t.boolean  "address_verification"
    t.boolean  "labs_integration"
    t.boolean  "user_tracking_and_auditing"
    t.boolean  "daily_backups"
    t.boolean  "bookkeeping"
    t.boolean  "custom_transaction_gateway"
    t.integer  "yearly_amount"
    t.boolean  "is_user_tracking_and_auditing_limited"
    t.boolean  "is_online_patient_scheduling_limited"
    t.boolean  "is_website_templates_limited"
    t.boolean  "is_office_staff_unlimited"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "amount_cents",                          :default => 0
    t.integer  "setup_amount_cents",                    :default => 0
  end

  create_table "subscriptions", :force => true do |t|
    t.datetime "next_renewal_at"
    t.string   "state",                   :default => "trial"
    t.integer  "subscription_plan_id"
    t.integer  "account_id"
    t.integer  "user_limit"
    t.integer  "renewal_period",          :default => 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "subscription_payment_id"
    t.integer  "amount_cents",            :default => 0
    t.string   "last_4_digits"
    t.string   "stripe_token"
  end

  add_index "subscriptions", ["account_id"], :name => "index_subscriptions_on_account_id"
  add_index "subscriptions", ["subscription_plan_id"], :name => "subscriptions_subscription_plans_fk"

  create_table "third_parties", :force => true do |t|
    t.integer  "account_id"
    t.integer  "insurance_carrier_type_code"
    t.integer  "insurance_type_code"
    t.string   "payer_code"
    t.string   "claims_office_sub_code"
    t.string   "clinic_code"
    t.string   "medigap_code"
    t.string   "alias_name"
    t.integer  "legacy_id_label_id"
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "alt_id"
    t.string   "plan_name"
    t.boolean  "suppress_ebill",              :default => false
  end

  add_index "third_parties", ["account_id"], :name => "index_third_parties_on_account_id"
  add_index "third_parties", ["legacy_id_label_id"], :name => "third_parties_legacy_id_labels_fk"

  create_table "third_party_bills", :force => true do |t|
    t.datetime "bill_date"
    t.integer  "case_id"
    t.integer  "third_party_id"
    t.integer  "provider_id"
    t.boolean  "benefits_assigned"
    t.boolean  "initial_or_followup_comp_bill"
    t.string   "primary_secondary_tertiary"
    t.string   "claim_number_for_secondary_filing"
    t.string   "electronic_bill_status"
    t.string   "ansi5010field_clm05_3"
    t.text     "collection_notes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                 :default => "", :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.integer  "failed_attempts",                       :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "login"
    t.integer  "account_id"
    t.integer  "clinic_id"
    t.string   "unconfirmed_email"
  end

  add_index "users", ["account_id"], :name => "index_users_on_account"
  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_email"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["role_id"], :name => "users_roles_roles_fk"
  add_index "users_roles", ["user_id", "role_id"], :name => "users_roles_joins_idx"

  create_table "versions", :force => true do |t|
    t.string   "item_type",  :null => false
    t.integer  "item_id",    :null => false
    t.string   "event",      :null => false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.string   "ip"
    t.integer  "account_id"
  end

  add_index "versions", ["account_id"], :name => "index_versions_on_account_id"
  add_index "versions", ["item_type", "item_id"], :name => "index_versions_on_item_type_and_item_id"

  create_table "visit_details", :force => true do |t|
    t.integer  "visit_id"
    t.integer  "procedure_code_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "provider_id"
  end

  add_index "visit_details", ["procedure_code_id"], :name => "visit_details_procedure_codes_fk"
  add_index "visit_details", ["provider_id"], :name => "visit_details_providers_fk"
  add_index "visit_details", ["visit_id"], :name => "visit_details_vists_fk"

  create_table "visit_diagnoses", :force => true do |t|
    t.integer  "visit_id"
    t.integer  "diagnosis_code_id"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "visits", :force => true do |t|
    t.date     "start_date"
    t.integer  "case_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "transaction_date"
    t.date     "onset_date"
    t.date     "first_treatment_date"
    t.text     "notes"
  end

  add_index "visits", ["case_id"], :name => "visits_cases_fk"

  create_table "webhooks", :force => true do |t|
    t.string   "message"
    t.integer  "subscription_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_foreign_key "appointment_types", "accounts", :name => "appointment_types_accounts_fk"

  add_foreign_key "appointments", "appointment_types", :name => "appointments_appointment_types_fk"
  add_foreign_key "appointments", "calendars", :name => "appointment_calendars_fk"
  add_foreign_key "appointments", "providers", :name => "appointment_provider_fk"

  add_foreign_key "calendars", "accounts", :name => "calendars_accounts_fk"

  add_foreign_key "case_carriers", "cases", :name => "case_carriers_cases_fk"
  add_foreign_key "case_carriers", "third_parties", :name => "case_carriers_third_parties_fk", :column => "carrier_id"

  add_foreign_key "case_diagnoses", "cases", :name => "case_diagnoses_cases_fk"
  add_foreign_key "case_diagnoses", "diagnosis_codes", :name => "case_diagnoses_diagnosis_codes_fk"

  add_foreign_key "cases", "accounts", :name => "cases_accounts_fk"
  add_foreign_key "cases", "fee_schedules", :name => "cases_fee_schedules_fk"
  add_foreign_key "cases", "providers", :name => "cases_providers_fk"

  add_foreign_key "clinics", "accounts", :name => "clinics_accounts_fk"

  add_foreign_key "diagnosis_codes", "accounts", :name => "diagnosis_codes_accounts_fk"

  add_foreign_key "event_locations", "events", :name => "event_locations_events_fk", :dependent => :delete

  add_foreign_key "fee_schedules", "accounts", :name => "fee_schedules_accounts_fk"

  add_foreign_key "fees", "fee_schedules", :name => "fees_fee_schedules_fk"
  add_foreign_key "fees", "procedure_codes", :name => "fees_procedure_codes_fk"

  add_foreign_key "hcfa_form_options", "accounts", :name => "hfo_accounts_fk"
  add_foreign_key "hcfa_form_options", "third_parties", :name => "hfo_third_parties_fk"

  add_foreign_key "import_groups", "accounts", :name => "import_groups_accounts_fk", :dependent => :delete

  add_foreign_key "import_records", "imports", :name => "import_records_imports_fk", :dependent => :delete

  add_foreign_key "imports", "accounts", :name => "imports_accounts_fk", :dependent => :delete
  add_foreign_key "imports", "import_groups", :name => "imports_import_groups_fk", :dependent => :delete

  add_foreign_key "legacy_id_labels", "accounts", :name => "legacy_id_labels_accounts_fk", :dependent => :delete

  add_foreign_key "new_patient_options", "clinics", :name => "new_patient_options_clinics_fk", :dependent => :delete
  add_foreign_key "new_patient_options", "providers", :name => "new_patient_options_providers_fk", :column => "default_provider_id"

  add_foreign_key "patients", "accounts", :name => "patients_accounts_fk"
  add_foreign_key "patients", "clinics", :name => "patients_clinics_fk"
  add_foreign_key "patients", "employers", :name => "patients_employers_fk"

  add_foreign_key "procedure_codes", "accounts", :name => "procedure_codes_accounts_fk"

  add_foreign_key "provider_legacy_id_labels", "legacy_id_labels", :name => "plil_legacy_id_labels_fk", :dependent => :delete
  add_foreign_key "provider_legacy_id_labels", "providers", :name => "plil_providers_fk", :dependent => :delete

  add_foreign_key "provider_precedences", "providers", :name => "provider_precedences_providers_fk", :dependent => :delete
  add_foreign_key "provider_precedences", "rooms", :name => "provider_precedences_rooms_fk", :dependent => :delete

  add_foreign_key "providers", "accounts", :name => "providers_accounts_fk"

  add_foreign_key "recurrences", "events", :name => "recurrences_events_fk", :dependent => :delete

  add_foreign_key "references", "accounts", :name => "references_accounts_fk", :dependent => :delete

  add_foreign_key "room_locations", "clinics", :name => "room_locations_clinics_fk"
  add_foreign_key "room_locations", "rooms", :name => "room_locations_rooms_fk", :dependent => :delete

  add_foreign_key "rooms", "appointment_types", :name => "rooms_appointment_types_fk"

  add_foreign_key "subscriptions", "accounts", :name => "subscriptions_accounts_fk"
  add_foreign_key "subscriptions", "subscription_plans", :name => "subscriptions_subscription_plans_fk"

  add_foreign_key "third_parties", "accounts", :name => "third_parties_accounts_fk"
  add_foreign_key "third_parties", "legacy_id_labels", :name => "third_parties_legacy_id_labels_fk"

  add_foreign_key "users", "accounts", :name => "users_accounts_fk"

  add_foreign_key "users_roles", "roles", :name => "users_roles_roles_fk"
  add_foreign_key "users_roles", "users", :name => "users_roles_users_fk", :dependent => :delete

  add_foreign_key "visit_details", "procedure_codes", :name => "visit_details_procedure_codes_fk"
  add_foreign_key "visit_details", "providers", :name => "visit_details_providers_fk"
  add_foreign_key "visit_details", "visits", :name => "visit_details_vists_fk"

  add_foreign_key "visits", "cases", :name => "visits_cases_fk"

end
