class AddAppointmentTypeToRooms < ActiveRecord::Migration
  def self.up
    add_column :rooms, :appointment_type_id, :integer
    add_foreign_key(:rooms, :appointment_types, :name => 'rooms_appointment_types_fk')
  end

  def self.down
    remove_foreign_key(:rooms, :appointment_types, :name => 'rooms_appointment_types_fk')
    remove_column :rooms, :appointment_type_id, :integer
  end
end
