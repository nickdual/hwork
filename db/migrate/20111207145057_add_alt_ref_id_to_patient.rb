class AddAltRefIdToPatient < ActiveRecord::Migration
  def self.up
    add_column :referrals, :alt_reference_id, :string
  end

  def self.down
    remove_column :referrals, :alt_reference_id
  end
end
