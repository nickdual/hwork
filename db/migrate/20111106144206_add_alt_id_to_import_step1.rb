class AddAltIdToImportStep1 < ActiveRecord::Migration
  def self.up
    add_column :clinics, :alt_id, :string
    add_column :messages, :alt_id, :string
    add_column :hcfa_form_options, :alt_id, :string
    add_column :third_parties, :alt_id, :string
  end

  def self.down
    remove_column :clinics, :alt_id
    remove_column :messages, :alt_id
    remove_column :hcfa_form_options, :alt_id
    remove_column :third_parties, :alt_id
  end
end
