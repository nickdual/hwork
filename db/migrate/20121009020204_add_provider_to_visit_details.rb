class AddProviderToVisitDetails < ActiveRecord::Migration
  def self.up
    add_column :visit_details, :provider_id, :integer
    add_foreign_key(:visit_details, :providers, :name => 'visit_details_providers_fk')
  end

  def self.down
    remove_foreign_key(:visit_details, :providers, :name => 'visit_details_providers_fk')
    remove_column :visit_details, :provider_id
  end
end
