class CsvImportBackgroundProcessing < ActiveRecord::Migration
  def self.up
    add_column :import_groups, :status, :string, :default => "Pending"
    add_column :import_groups, :clinic_id, :integer
    add_column :imports, :status, :string, :default => "Pending"
  end

  def self.down
    remove_column :import_groups, :status
    remove_column :import_groups, :clinic_id
    remove_column :imports, :status, :string
  end
end
