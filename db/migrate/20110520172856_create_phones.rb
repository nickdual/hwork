class CreatePhones < ActiveRecord::Migration
  def self.up
    create_table :phones do |t|
      t.string :area_code
      t.string :number
      t.string :extension
      t.references :phoneable, :polymorphic => true
      t.string :type
      t.timestamps
    end
  end

  def self.down
    drop_table :phones
  end
end
