class ChangeDefaultImportGroupsDuplicateCode < ActiveRecord::Migration
  def self.up
    change_column :import_groups, :duplicate_code, :integer, :default => 2
  end

  def self.down
    change_column :import_groups, :duplicate_code, :integer, :default => 1
  end
end
