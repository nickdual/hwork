class AddIcdtenToDiagnosisCodes < ActiveRecord::Migration
  def self.up
	add_column :diagnosis_codes, :ICD10, :string
  end

  def self.down
	remove_column :diagnosis_codes, :ICD10, :string
  end
end
