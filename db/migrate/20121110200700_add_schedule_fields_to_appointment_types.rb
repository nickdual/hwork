class AddScheduleFieldsToAppointmentTypes < ActiveRecord::Migration
  def self.up
        add_column :appointment_types, :can_create_deadline, :integer, :default => 3
        add_column :appointment_types, :can_change_deadline, :integer, :default => 3
        add_column :appointment_types, :can_cancel_deadline, :integer, :default => 3
        add_column :appointment_types, :max_number_of_appointments_user_can_book_in_advance, :integer, :default => 6
        add_column :appointment_types, :auto_confirm_appointment_create, :boolean, :default => true
        add_column :appointment_types, :auto_confirm_appointment_change, :boolean, :default => true
        add_column :appointment_types, :auto_confirm_appointment_delete, :boolean, :default => true
        add_column :appointment_types, :allow_user_to_change_appointment, :boolean, :default => true
        add_column :appointment_types, :allow_user_to_cancel_appointment, :boolean, :default => true
        add_column :appointment_types, :allow_user_to_delete_appointment, :boolean, :default => true
  end

  def self.down
        drop_column :appointment_types, :can_create_deadline
        drop_column :appointment_types, :can_change_deadline
        drop_column :appointment_types, :can_cancel_deadline
        drop_column :appointment_types, :max_number_of_appointments_user_can_book_in_advance
        drop_column :appointment_types, :auto_confirm_appointment_create
        drop_column :appointment_types, :auto_confirm_appointment_change
        drop_column :appointment_types, :auto_confirm_appointment_delete
        drop_column :appointment_types, :allow_user_to_change_appointment
        drop_column :appointment_types, :allow_user_to_cancel_appointment
        drop_column :appointment_types, :allow_user_to_delete_appointment
  end
end
