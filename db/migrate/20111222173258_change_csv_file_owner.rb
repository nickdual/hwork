class ChangeCsvFileOwner < ActiveRecord::Migration
  def self.up
    add_column :csv_files, :import_id, :integer
    remove_column :csv_files, :import_group_id
  end

  def self.down
    remove_column :csv_files, :import_id
    add_column :csv_files, :import_group_id, :integer
  end
end
