class AddEventsIndex < ActiveRecord::Migration
  def up
    add_index :events, [:eventable_id, :eventable_type], :name => 'events_eventable_idx';
  end

  def down
    remove_index :events, :name => 'events_eventable_idx';
  end
end
