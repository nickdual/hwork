class MonetizeSubscriptionPlans < ActiveRecord::Migration
  def self.up
    add_column :subscription_plans, :amount_cents, :integer, :default => 0
    add_column :subscription_plans, :setup_amount_cents, :integer, :default => 0

    execute <<-SQL
      update subscription_plans set amount_cents = amount * 100, setup_amount_cents = setup_amount * 100 where 1 = 1
    SQL

    remove_column :subscription_plans, :amount
    remove_column :subscription_plans, :setup_amount
  end

  def self.down
    add_column :subscription_plans, :amount, :decimal, :precision => 10, :scale => 2
    add_column :subscription_plans, :setup_amount, :decimal, :precision => 10, :scale => 2

    execute <<-SQL
      update subscription_plans set amount = amount_cents / 100, setup_amount = setup_amount_cents / 100 where 1 = 1
    SQL

    remove_column :subscription_plans, :amount_cents
    remove_column :subscription_plans, :setup_amount_cents
  end
end
