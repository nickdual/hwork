class CreatePatients < ActiveRecord::Migration
  def self.up
    create_table :patients do |t|
      t.integer :clinic_id
      t.date :birthdate
      t.integer :employer_id
      t.string :ssn
      t.string :spouse_name
      t.integer :referral_id
      t.boolean :is_active, :default => true
      t.string :statement_message
      t.string :address_stationery_to_label
      t.boolean :is_full_time_student, :default => false
      t.integer :marital_status_code, :default => 5
      t.integer :disability_status_code, :default => 0
      t.boolean :should_send_overdue_statements, :default => true
      t.integer :overdue_fee_percentage
      t.text :notes
      t.string :account_code
      t.integer :employment_status_code, :default => 0
      t.string :category
      t.integer :parent_patient_id
      t.integer :import_id

      t.timestamps
    end
  end

  def self.down
    drop_table :patients
  end
end
