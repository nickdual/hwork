class AddClinicAltIdToPatients < ActiveRecord::Migration
  def self.up
    add_column :patients, :clinic_alt_id, :string
  end

  def self.down
    remove_column :patients, :clinic_alt_id
  end
end
