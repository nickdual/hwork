class ChangeHcfaFormOptionBox29ToInteger < ActiveRecord::Migration
  def self.up
    change_column :hcfa_form_options, :box29, :integer
  end

  def self.down
    change_column :hcfa_form_options, :box29, :string
  end
end
