class RenameFeeModels < ActiveRecord::Migration
  def self.up
    rename_table :fee_schedules, :fees
    rename_table :fee_schedule_labels, :fee_schedules
  end

  def self.down
    rename_table :fee_schedules, :fee_schedule_labels
    rename_table :fees, :fee_schedules
  end
end
