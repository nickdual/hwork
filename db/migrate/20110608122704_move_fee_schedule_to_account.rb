class MoveFeeScheduleToAccount < ActiveRecord::Migration
  def self.up
    change_table :fee_schedules do |t|
      t.rename(:clinic_id, :account_id)
      t.column(:description, :string)
    end
  end

  def self.down
    change_table :fee_schedules do |t|
      t.rename(:account_id, :clinic_id)
      t.remove(:description)
    end
  end
end
