class AddScheduleHistoryLimitToCalendar < ActiveRecord::Migration
  def self.up
    add_column :calendars, :schedule_history_max_days, :integer, :default => 60
    add_column :calendars, :schedule_future_max_days, :integer, :default => 30
    remove_column :account_preferences, :schedule_future_max_days
  end

  def self.down
    remove_column :calendars, :schedule_history_max_days
    remove_column :calendars, :schedule_future_max_days
    add_column :account_preferences, :schedule_future_max_days, :integer, :default => 30
  end
end
