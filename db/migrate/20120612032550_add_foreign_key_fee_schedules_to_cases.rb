class AddForeignKeyFeeSchedulesToCases < ActiveRecord::Migration
  def self.up
    add_foreign_key(:cases, :fee_schedules, :name => 'cases_fee_schedules_fk')
  end

  def self.down
    remove_foreign_key(:cases, :fee_schedules, :name => 'cases_fee_schedules_fk')
  end
end
