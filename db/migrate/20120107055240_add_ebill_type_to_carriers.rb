class AddEbillTypeToCarriers < ActiveRecord::Migration
  def self.up
    add_column :third_parties, :ebill_type_code, :integer
  end

  def self.down
    remove_column :third_parties, :ebill_type_code
  end
end
