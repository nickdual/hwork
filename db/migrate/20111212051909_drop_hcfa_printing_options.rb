class DropHcfaPrintingOptions < ActiveRecord::Migration
  def self.up
    remove_column :new_patient_options, :box_32_use
    remove_column :new_patient_options, :box_33_use
    remove_column :new_patient_options, :hcfa_left_margin
    remove_column :new_patient_options, :hcfa_top_margin
    remove_column :new_patient_options, :should_print_diagnosis_description_on_hcfa
  end

  def self.down
    add_column :new_patient_options, :box_32_use, :string
    add_column :new_patient_options, :box_33_use, :string
    add_column :new_patient_options, :hcfa_left_margin, :float
    add_column :new_patient_options, :hcfa_top_margin, :float
    add_column :new_patient_options, :should_print_diagnosis_description_on_hcfa, :boolean
  end
end
