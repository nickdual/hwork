class RemoveProviderAssignments < ActiveRecord::Migration
  def self.up
    drop_table :provider_assignments
  end

  def self.down
    create_table :provider_assignments do |t|
      t.integer :provider_id
      t.integer :clinic_id
      t.timestamps
    end
  end
end
