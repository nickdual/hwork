class AddClinicLocationToClinic < ActiveRecord::Migration
  def self.up
    add_column :clinics, :location_clinic_id, :integer
  end

  def self.down
    remove_column :clinics, :location_clinic_id
  end
end
