class FixReferralAssociation2 < ActiveRecord::Migration
  def self.up
    add_column :patients, :referral_id, :integer
  end

  def self.down
    remove_column :patients, :referral_id
  end
end
