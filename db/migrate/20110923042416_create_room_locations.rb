class CreateRoomLocations < ActiveRecord::Migration
  def self.up
    create_table :room_locations do |t|
      t.integer :room_id
      t.integer :clinic_id

      t.timestamps
    end
  end

  def self.down
    drop_table :room_locations
  end
end
