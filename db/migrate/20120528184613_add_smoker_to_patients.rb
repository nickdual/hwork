class AddSmokerToPatients < ActiveRecord::Migration
  def self.up
	    add_column :patients, :smoker, :boolean
  end

  def self.down
    	remove_column :patients, :smoker, :boolean  end
end
