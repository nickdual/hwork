class HwUpgradeSync2 < ActiveRecord::Migration
  def up

    ##begin
    ##  remove_foreign_key :role_assignments, :roles, :name => 'role_assignments_roles_fk'
    ##  remove_foreign_key :role_assignments, :users, :name => 'role_assignments_users_fk'
    ##rescue :error => e
    ##  puts "no role assignment foreign keys"
    ##end

    begin
      drop_table :role_assignments
    rescue :error => e
      puts "no role assignments table to drop"
    end

    add_column :users, :unconfirmed_email, :string
    remove_index :users, :name => 'index_users_on_email'
    add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  end

  def down
    create_table "role_assignments", :force => true do |t|
      t.integer  "role_id"
      t.integer  "user_id"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    ##add_foreign_key "role_assignments", "roles", :name => "role_assignments_roles_fk"
    ##add_foreign_key "role_assignments", "users", :name => "role_assignments_users_fk", :dependent => :delete

    add_index "role_assignments", ["role_id"], :name => "role_assignments_roles_fk"
    add_index "role_assignments", ["user_id"], :name => "role_assignments_users_fk"

    remove_column :users, :unconfirmed_email, :string

    remove_index :users, :name => 'index_users_on_email'
    add_index "users_roles", ["user_id", "role_id"], :name => "users_roles_joins_idx"
  end
end
