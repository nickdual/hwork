class ConvertFeeToMoney < ActiveRecord::Migration
  def self.up
    add_column :fees, :fee_cents, :integer, :default => 0
    add_column :fees, :copay_cents, :integer, :default => 0
    add_column :fees, :copay_pct, :decimal, :precision => 8, :scale => 4, :default => 100.00
    add_column :fees, :expected_insurance_payment_cents, :integer, :default => 0

    execute <<-SQL
      update fees set fee_cents = fee * 100, expected_insurance_payment_cents = expected_insurance_payment * 100 where 1 = 1
    SQL

    execute <<-SQL
      update fees set copay_cents = (copay * 100), copay_pct = (copay / fee) where is_percentage = 0
    SQL

    execute <<-SQL
      update fees set copay_cents = (fee * copay), copay_pct = (copay / 100) where is_percentage = 1
    SQL

    remove_column :fees, :fee
    remove_column :fees, :copay
    remove_column :fees, :expected_insurance_payment
  end

  def self.down
    add_column :fees, :fee, :decimal, :precision => 8, :scale => 2, :default => 0.0
    add_column :fees, :copay, :decimal, :precision => 8, :scale => 2, :default => 0.0
    add_column :fees, :expected_insurance_payment, :decimal, :precision => 8, :scale => 2, :default => 0.0

    execute <<-SQL
      update fees set fee = (fee_cents / 100 ), expected_insurance_payment = (expected_insurance_payment_cents / 100) where 1 = 1
    SQL

    execute <<-SQL
      update fees set copay = (copay_cents / 100) where is_percentage = 0
    SQL

    execute <<-SQL
      update fees set copay = (copay_pct * 100) where is_percentage = 1
    SQL

    remove_column :fees, :fee_cents
    remove_column :fees, :copay_cents
    remove_column :fees, :copay_pct
    remove_column :fees, :expected_insurance_payment_cents
  end
end
