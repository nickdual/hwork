class ConvertHcfaFormCarriersToThirdParties < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      update hcfa_form_options
        set insurance_carrier_id = insurance_carrier_id + 2000
        where insurance_carrier_id is not null
          and insurance_carrier_id < 2000
    SQL
  end

  def self.down
    execute <<-SQL
      update hcfa_form_options
        set insurance_carrier_id = insurance_carrier_id + 2000
        where insurance_carrier_id is not null
          and insurance_carrier_id < 2000
    SQL
  end
end
