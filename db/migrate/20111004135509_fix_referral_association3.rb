class FixReferralAssociation3 < ActiveRecord::Migration
  def self.up
    remove_column :patients, :referral_id
  end

  def self.down
    add_column :patients, :referral_id, :integer
  end
end
