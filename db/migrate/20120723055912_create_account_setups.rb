class CreateAccountSetups < ActiveRecord::Migration
  def self.up
    create_table :account_setups do |t|
      t.string :state
      t.integer :account_id
      t.boolean :first_session
      t.boolean :clinics
      t.boolean :proviers
      t.boolean :schedules

      t.timestamps
    end
  end

  def self.down
    drop_table :account_setups
  end
end
