class CreateHandyworksObjectGroups < ActiveRecord::Migration
  def self.up
    create_table :handyworks_object_groups do |t|
      t.string :label
      t.integer :account_id

      t.timestamps
    end
    account = Account.new(:name => "GLOBAL DEFAULTS")
    account.id = Account::DEFAULT_ACCOUNT_ID
    account.save!
"""
    HandyworksObjectGroup.create!(
      :account_id => Account::DEFAULT_ACCOUNT_ID,
      :label => "Account")
    HandyworksObjectGroup.create!(
      :account_id => Account::DEFAULT_ACCOUNT_ID,
      :label => "Clinical")
    HandyworksObjectGroup.create!(
      :account_id => Account::DEFAULT_ACCOUNT_ID,
      :label => "Financial")
    HandyworksObjectGroup.create!(
      :account_id => Account::DEFAULT_ACCOUNT_ID,
      :label => "Records")
    HandyworksObjectGroup.create!(
      :account_id => Account::DEFAULT_ACCOUNT_ID,
      :label => "Patients")

"""
  end

  def self.down
    drop_table :handyworks_object_groups
    Account.destroy(Account::DEFAULT_ACCOUNT_ID)
  end
end
