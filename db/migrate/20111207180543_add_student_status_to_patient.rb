class AddStudentStatusToPatient < ActiveRecord::Migration
  def self.up
    add_column :patients, :student_status_code, :integer, :default => 0
    remove_column :patients, :is_full_time_student
  end

  def self.down
    remove_column :patients, :student_status_code
    add_column :patients, :is_full_time_student, :boolean, :default => false
  end
end
