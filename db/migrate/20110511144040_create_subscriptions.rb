class CreateSubscriptions < ActiveRecord::Migration
  def self.up
    create_table :subscriptions do |t|
      t.decimal :amount, :precision => 10, :scale => 2
      t.datetime :next_renewal_at
      t.string :card_number
      t.string :card_expiration
      t.string :state, :default => "trial"
      t.integer :subscription_plan_id
      t.integer :account_id
      t.integer :user_limit
      t.integer :renewal_period, :default => 1
      t.string :billing_id

      t.timestamps
    end

    add_index :subscriptions, [:account_id], :name => "index_subscriptions_on_account_id"
  end

  def self.down
    drop_table :subscriptions
  end
end
