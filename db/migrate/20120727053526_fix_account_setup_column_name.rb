class FixAccountSetupColumnName < ActiveRecord::Migration
  def self.up
    rename_column :account_setups, :proviers, :providers
  end

  def self.down
    rename_column :account_setups, :providers, :proviers
  end
end
