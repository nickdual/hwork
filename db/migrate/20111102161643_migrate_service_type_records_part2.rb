class MigrateServiceTypeRecordsPart2 < ActiveRecord::Migration
  def self.up
    execute <<-SQL
    UPDATE appointment_types AS dest
      INNER JOIN ( SELECT m.id, m.account_id
     FROM calendars m) AS source
      SET dest.serviceable_type = 'Calendar',
              dest.serviceable_id = source.id
            WHERE dest.serviceable_id = source.account_id
              AND dest.serviceable_type = 'Account'
    SQL
  end

  def self.down
    execute <<-SQL
    UPDATE appointment_types AS dest
      INNER JOIN ( SELECT m.id, m.account_id
     FROM calendars m) AS source
      SET dest.serviceable_type = 'Account',
              dest.serviceable_id = source.account_id
            WHERE dest.serviceable_id = source.id
              AND dest.serviceable_type = 'Calendar'
    SQL
  end
end
