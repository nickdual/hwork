class AddPositionToRooms < ActiveRecord::Migration
  def self.up
    add_column :rooms, :position, :integer, :default => 0
  end

  def self.down
    remove_column :rooms, :position
  end
end
