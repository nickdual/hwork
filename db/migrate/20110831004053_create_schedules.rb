class CreateSchedules < ActiveRecord::Migration
  def self.up
    create_table :schedules do |t|
      t.integer :schedulable_id
      t.string :schedulable_type

      t.timestamps
    end
  end

  def self.down
    drop_table :schedules
  end
end
