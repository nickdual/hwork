class AddMoreFieldsToVisits < ActiveRecord::Migration
  def self.up
      add_column :visits, :transaction_date, :date
      add_column :visits, :onset_date, :date
      add_column :visits, :first_treatment_date, :date
      add_column :visits, :notes, :text
  end

  def self.down
      drop_column :visits, :transaction_date
      drop_column :visits, :onset_date
      drop_column :visits, :first_treatment_date
      drop_column :visits, :notes
  end
end
