class DropColumnsFromPatients < ActiveRecord::Migration
  def self.up
    remove_column :patients, :sex
    remove_column :patients, :occupation
    remove_column :patients, :title
    add_column :patients, :nick_name, :string
  end

  def self.down
    add_column :patients, :sex, :integer, :default => 0
    add_column :patients, :occupation, :string
    add_column :patients, :title, :string
    remove_column :patients, :nick_name
  end
end
