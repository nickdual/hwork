class MovePatientClientIdToAltId < ActiveRecord::Migration
  def self.up
    add_column :patients, :alt_id, :string
    execute <<-SQL
      update patients set alt_id = client_id where id is not null
    SQL
    remove_column :patients, :client_id
  end

  def self.down
    add_column :patients, :client_id, :string
    execute <<-SQL
      update patients set client_id = alt_id where id is not null
    SQL
    remove_column :patients, :alt_id
  end
end
