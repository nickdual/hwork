class AddRecurIntervalTypeToEvents < ActiveRecord::Migration
  def self.up
    add_column :events, :recur_interval_type, :integer
  end

  def self.down
    drop_column :events, :recur_interval_type, :integer
  end
end
