class AddAccountToPatient < ActiveRecord::Migration
  def self.up
    add_column :patients, :account_id, :integer
  end

  def self.down
    remove_column :patients, :account_id
  end
end
