class AddNotesToInsuranceCarriers < ActiveRecord::Migration
  def self.up
    add_column :insurance_carriers, :notes, :text
  end

  def self.down
    remove_column :insurance_carriers, :notes
  end
end
