class CreateImportGroups < ActiveRecord::Migration
  def self.up
    create_table :import_groups do |t|
      t.string :comment
      t.integer :duplicate_code, :default => 0
      t.integer :error_code, :default => 0
      t.timestamps
    end
  end

  def self.down
    drop_table :import_groups
  end
end
