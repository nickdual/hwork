class AddLableToPhone < ActiveRecord::Migration
  def self.up
    add_column :phones, :label , :string
    add_column :emails, :label , :string
    add_column :contacts, :type , :string
  end

  def self.down
    drop_column :phones, :label
    drop_column :emails, :label
    drop_column :contacts, :type
  end
end
