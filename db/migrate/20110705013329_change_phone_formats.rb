class ChangePhoneFormats < ActiveRecord::Migration
  def self.up
    # combines the area_code, number and extension into a single unformatted number field.
    execute <<-SQL
      update phones set number = replace(concat_ws(area_code,number,extension),'-','')
    SQL
    remove_column :phones, :area_code
    remove_column :phones, :extension
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
