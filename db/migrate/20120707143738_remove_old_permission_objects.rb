class RemoveOldPermissionObjects < ActiveRecord::Migration
  def self.up
    remove_foreign_key :handyworks_object_group_assignments, :name => "hoga_hog_fk"
    remove_foreign_key :handyworks_object_group_assignments, :name => "hoga_ho_fk"
    remove_foreign_key :handyworks_object_group_permissions, :name => "hogp_hog_fk"
    remove_foreign_key :handyworks_object_group_permissions, :name => "hogp_users_fk"
    remove_foreign_key :handyworks_object_groups, :name => "hog_accounts_fk"
    drop_table :handyworks_object_group_permissions
    drop_table :handyworks_object_group_assignments
    drop_table :handyworks_object_groups
    drop_table :handyworks_objects
  end

  def self.down

    create_table "handyworks_objects", :force => true do |t|
      t.string   "object_class"
      t.string   "description"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    create_table "handyworks_object_groups", :force => true do |t|
      t.string   "label"
      t.integer  "account_id"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "handyworks_object_groups", ["account_id"], :name => "hog_accounts_fk"

    create_table "handyworks_object_group_assignments", :force => true do |t|
      t.integer  "handyworks_object_id"
      t.integer  "handyworks_object_group_id"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "handyworks_object_group_assignments", ["handyworks_object_group_id"], :name => "hoga_hog_fk"
    add_index "handyworks_object_group_assignments", ["handyworks_object_id"], :name => "hoga_ho_fk"


    create_table "handyworks_object_group_permissions", :force => true do |t|
      t.integer  "user_id"
      t.integer  "handyworks_object_group_id"
      t.integer  "access_level"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "handyworks_object_group_permissions", ["handyworks_object_group_id"], :name => "hogp_hog_fk"
    add_index "handyworks_object_group_permissions", ["user_id"], :name => "hogp_users_fk"

    add_foreign_key "handyworks_object_group_assignments", "handyworks_object_groups", :name => "hoga_hog_fk"
    add_foreign_key "handyworks_object_group_assignments", "handyworks_objects", :name => "hoga_ho_fk"
    add_foreign_key "handyworks_object_group_permissions", "handyworks_object_groups", :name => "hogp_hog_fk"
    add_foreign_key "handyworks_object_group_permissions", "users", :name => "hogp_users_fk"
    add_foreign_key "handyworks_object_groups", "accounts", :name => "hog_accounts_fk"
  end
end
