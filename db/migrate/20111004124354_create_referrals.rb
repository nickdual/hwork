class CreateReferrals < ActiveRecord::Migration
  def self.up
    create_table :referrals do |t|
      t.integer :referrable_id
      t.string :referrable_type
      t.integer :reference_id
      t.string :reference_type

      t.timestamps
    end
  end

  def self.down
    drop_table :referrals
  end
end
