class CreateRecurrences < ActiveRecord::Migration
  def self.up
    create_table :recurrences do |t|
      t.integer :event_id
      t.integer :interval
      t.integer :count
      t.date :start_date
      t.date :end_date
      t.integer :interval_type

      t.timestamps
    end

    remove_column :events, :recur
    remove_column :events, :recur_interval
    remove_column :events, :recur_count
    remove_column :events, :recur_start_date
    remove_column :events, :recur_end_date
    remove_column :events, :recur_interval_type

    add_column :events, :free, :boolean, :default => false

  end

  def self.down
    drop_table :recurrences

    add_column :events, :recur, :boolean
    add_column :events, :recur_interval, :integer
    add_column :events, :recur_count, :integer
    add_column :events, :recur_start_date, :date
    add_column :events, :recur_end_date, :date
    add_column :events, :recur_interval_type, :integer

    remove_column :events, :free
  end
end
