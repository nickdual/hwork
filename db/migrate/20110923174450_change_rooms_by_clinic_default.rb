class ChangeRoomsByClinicDefault < ActiveRecord::Migration
  def self.up
    change_column_default(:account_preferences, :rooms_by_clinic, false)
    execute <<-SQL
      update account_preferences set rooms_by_clinic = 0 where rooms_by_clinic is null
    SQL
  end

  def self.down
    change_column_default(:account_preferences, :rooms_by_clinic, nil)
    execute <<-SQL
      update account_preferences set rooms_by_clinic = null where rooms_by_clinic = 0
    SQL
  end
end
