class SwitchFromWorkflowToStateMachine < ActiveRecord::Migration
  def self.up
    add_column :accounts, :state, :string
    add_column :accounts, :payment_status, :string
    remove_column :accounts, :workflow_state
  end

  def self.down
    remove_column :accounts, :state
    remove_column :accounts, :payment_status
    add_column :accounts, :workflow_state, :string
  end
end
