class MoveHcfaFormOptionToAccount < ActiveRecord::Migration
  def self.up
    add_column :hcfa_form_options, :account_id, :integer
    remove_column :hcfa_form_options, :clinic_id
  end

  def self.down
    add_column :hcfa_form_options, :clinic_id, :integer
    remove_column :hcfa_form_options, :account_id
  end
end
