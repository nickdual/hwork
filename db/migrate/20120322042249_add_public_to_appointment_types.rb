class AddPublicToAppointmentTypes < ActiveRecord::Migration
  def self.up
    add_column :appointment_types, :is_public, :boolean, :default => false
    add_column :appointment_types, :for_new_patients, :boolean, :default => false
  end

  def self.down
    remove_column :appointment_types, :is_public
    remove_column :appointment_types, :for_new_patients
  end
end
