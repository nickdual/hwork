class CreateFeeScheduleLabels < ActiveRecord::Migration
  def self.up
    create_table :fee_schedule_labels do |t|
      t.string :label
      t.integer :clinic_id

      t.timestamps
    end
  end

  def self.down
    drop_table :fee_schedule_labels
  end
end
