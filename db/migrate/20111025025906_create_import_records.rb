class CreateImportRecords < ActiveRecord::Migration
  def self.up
    create_table :import_records do |t|
      t.integer :import_id
      t.integer :status
      t.text :record

      t.timestamps
    end
  end

  def self.down
    drop_table :import_records
  end
end
