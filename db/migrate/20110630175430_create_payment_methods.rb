class CreatePaymentMethods < ActiveRecord::Migration
  def self.up
    create_table :payment_methods do |t|
      t.string :method_payment_id
      t.integer :payment_type
      t.integer :payer_id
      t.string :payer_type
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :payment_methods
  end
end
