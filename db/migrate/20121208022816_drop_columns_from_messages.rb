class DropColumnsFromMessages < ActiveRecord::Migration
  def self.up
    remove_column :messages, :date_format
    remove_column :messages, :date_placement
    remove_column :messages, :patient_address
    remove_column :messages, :sign_off_placement
    remove_column :messages, :signator
    remove_column :messages, :sign_off
  end

  def self.down
    add_column :messages, :date_format, :integer, :default => 2
    add_column :messages, :date_placement, :integer, :default => 1
    add_column :messages, :patient_address, :integer, :default => 0
    add_column :messages, :sign_off_placement, :integer, :default => 1
    add_column :messages, :signator, :integer, :default => 1
    add_column :messages, :sign_off, :string, :default => "Sincerely"
  end
end
