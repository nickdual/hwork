class FixCopayPct < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      update fees set copay_pct = 1.0 where copay_pct > 1.0
    SQL

    change_column :fees, :copay_pct, :decimal, :precision => 8, :scale => 7, :default => 1.0
  end

  def self.down
    change_column :fees, :copay_pct, :decimal, :precision => 8, :scale => 4, :default => 100.0
  end
end
