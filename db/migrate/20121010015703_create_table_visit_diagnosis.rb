class CreateTableVisitDiagnosis < ActiveRecord::Migration
  def self.up
    create_table :visit_diagnoses, :force => true do |t|
      t.integer  "visit_id"
      t.integer  "diagnosis_code_id"
      t.string   "description"
      t.datetime "created_at"
      t.datetime "updated_at"
    end
  end

  def self.down
    drop_table :visit_diagnoses
  end
end
