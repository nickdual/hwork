class AddCountryCodeToPhones < ActiveRecord::Migration
  def self.up
    add_column :phones, :country, :string, :default => "+1"
  end

  def self.down
    remove_column :phones, :country
  end
end
