class MonetizeCaseCarriers < ActiveRecord::Migration
  def self.up
    add_column :case_carriers, :deductible_cents, :integer, :default => 0
    add_column :case_carriers, :paid_cents, :integer, :default => 0
    execute <<-SQL
      update case_carriers set deductible_cents = deductible * 100, paid_cents = paid * 100 where 1 = 1
    SQL
    execute <<-SQL
      update case_carriers set deductible_cents = 0 where deductible_cents is null
    SQL
    execute <<-SQL
      update case_carriers set paid_cents = 0 where paid_cents is null
    SQL
    remove_column :case_carriers, :deductible
    remove_column :case_carriers, :paid
  end

  def self.down
    add_column :case_carriers, :deductible, :decimal, :precision => 10, :scale => 2
    add_column :case_carriers, :paid, :decimal, :precision => 10, :scale => 2
    execute <<-SQL
      update case_carriers set deductible = deductible_cents / 100 where 1 = 1
    SQL
    execute <<-SQL
      update case_carriers set paid = paid_cents / 100 where 1 = 1
    SQL
    remove_column :case_carriers, :deductible_cents
    remove_column :case_carriers, :paid_cents
  end
end
