class AddForeignKeys < ActiveRecord::Migration
  def self.up
    add_foreign_key(:appointments, :providers, :name => 'appointment_provider_fk')
    add_foreign_key(:appointments, :calendars, :name => 'appointment_calendars_fk')
  end

  def self.down
    remove_foreign_key(:appointments, :providers, :name => 'appointment_provider_fk')
    remove_foreign_key(:appointments, :calendars, :name => 'appointment_calendars_fk')
  end
end
