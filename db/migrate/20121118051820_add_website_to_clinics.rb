class AddWebsiteToClinics < ActiveRecord::Migration
  def self.up
        add_column :clinics, :website_url, :string
  end

  def self.down
        drop_column :clinics, :website_url
  end
end
