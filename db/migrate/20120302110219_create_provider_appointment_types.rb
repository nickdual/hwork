class CreateProviderAppointmentTypes < ActiveRecord::Migration
  def self.up
    create_table :provider_appointment_types do |t|
      t.integer :provider_id
      t.integer :appointment_type_id

      t.timestamps
    end
  end

  def self.down
    drop_table :provider_appointment_types
  end
end
