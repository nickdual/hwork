class CreateServiceTypes < ActiveRecord::Migration
  def self.up
    create_table :service_types do |t|
      t.string :name
      t.integer :duration_min
      t.integer :duration_max
      t.integer :schedule_lead_time
      t.integer :pre_appointment_gap
      t.integer :post_appointment_gap
      t.integer :serviceable_id
      t.string :serviceable_type

      t.timestamps
    end
  end

  def self.down
    drop_table :service_types
  end
end
