class CreateFeeSchedules < ActiveRecord::Migration
  def self.up
    create_table :fee_schedules do |t|
      t.integer :fee
      t.integer :copay
      t.boolean :is_percentage
      t.integer :expected_insurance_payment_cents
      t.integer :procedure_code_id
      t.integer :fee_schedule_label_id

      t.timestamps
    end
  end

  def self.down
    drop_table :fee_schedules
  end
end
