class CreateClinicFeeSchedules < ActiveRecord::Migration
  def self.up
    create_table :clinic_fee_schedules do |t|
      t.integer :fee_schedule_id
      t.integer :clinic_id

      t.timestamps
    end
  end

  def self.down
    drop_table :clinic_fee_schedules
  end
end
