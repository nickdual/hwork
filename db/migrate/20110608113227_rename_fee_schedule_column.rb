class RenameFeeScheduleColumn < ActiveRecord::Migration
  def self.up
    change_table :fees do |t|
      t.rename(:fee_schedule_label_id, :fee_schedule_id)
    end
  end

  def self.down
    change_table :fees do |t|
      t.rename(:fee_schedule_id, :fee_schedule_label_id)
    end
  end
end
