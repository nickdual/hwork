class AddPaymentIdToAccount < ActiveRecord::Migration
  def self.up
    add_column :accounts, :payment_system_id, :string
  end

  def self.down
    remove_column :accounts, :payment_system_id
  end
end
