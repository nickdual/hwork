class RemoveTimeFieldsFromRooms < ActiveRecord::Migration
  def self.up
    remove_column :rooms, :first_appointment_time
    remove_column :rooms, :last_appointment_time
  end

  def self.down
    add_column :rooms, :first_appointment_time, :time
    add_column :rooms, :last_appointment_time, :time
  end
end
