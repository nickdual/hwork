class CreateHandyworksObjectGroupPermissions < ActiveRecord::Migration
  def self.up
    create_table :handyworks_object_group_permissions do |t|
      t.integer :user_id
      t.integer :handyworks_object_group_id
      t.integer :access_level

      t.timestamps
    end
  end

  def self.down
    drop_table :handyworks_object_group_permissions
  end
end
