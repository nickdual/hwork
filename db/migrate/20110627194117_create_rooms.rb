class CreateRooms < ActiveRecord::Migration
  def self.up
    create_table :rooms do |t|
      t.integer :account_id
      t.string :name
      t.time :first_appointment_time
      t.time :last_appointment_time

      t.timestamps
    end
  end

  def self.down
    drop_table :rooms
  end
end
