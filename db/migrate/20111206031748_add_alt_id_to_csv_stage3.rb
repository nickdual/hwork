class AddAltIdToCsvStage3 < ActiveRecord::Migration
  def self.up
    add_column :diagnosis_codes, :alt_id, :string
    add_column :procedure_codes, :alt_id, :string
    add_column :fee_schedules, :alt_id, :string
    add_column :fees, :alt_id, :string
  end

  def self.down
    remove_column :diagnosis_codes, :alt_id
    remove_column :procedure_codes, :alt_id
    remove_column :fee_schedules, :alt_id
    remove_column :fees, :alt_id
  end
end
