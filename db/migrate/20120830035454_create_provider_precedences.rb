class CreateProviderPrecedences < ActiveRecord::Migration
  def self.up
    create_table :provider_precedences do |t|
      t.integer "room_id"
      t.integer "provider_id"
      t.integer "order"
      t.timestamps
    end
  end

  def self.down
    drop_table :provider_precedences
  end
end
