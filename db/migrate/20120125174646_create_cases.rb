class CreateCases < ActiveRecord::Migration
  def self.up
    create_table :cases do |t|
      t.integer "account_id"
      t.integer "patient_id"
      t.integer "provider_id"
      t.string "description"
      t.integer "third_party_id"
      t.date "similar_symptoms_date"
      t.boolean "employment_related", :default => false
      t.boolean "auto_accident", :default => false
      t.boolean "non_auto_accident", :default => false
      t.date "onset_date"
      t.string "prior_authorization"
      t.date "first_treatment_date"
      t.date "accident_date"
      t.date "hcfa_accident_date"
      t.string "hcfa_box19_accident_description"
      t.string "treatment_phase", :limit => 1, :default => "A"
      t.integer "fee_schedule_id"
      t.boolean "active", :default => true
      t.text "notes"
      t.timestamps
    end

    add_index :cases, :account_id
    add_index :cases, :patient_id
    add_index :cases, :provider_id
    add_index :case_diagnoses, :case_id
    add_index :case_carriers, :case_id
    add_index :case_carriers, :carrier_id
    add_index :case_carriers, :guarantor_id
  end

  def self.down
    drop_table :cases
    remove_index :cases, :account_id
    remove_index :cases, :patient_id
    remove_index :cases, :provider_id
    remove_index :case_diagnoses, :case_id
    remove_index :case_carriers, :case_id
    remove_index :case_carriers, :carrier_id
    remove_index :case_carriers, :guarantor_id
  end
end
