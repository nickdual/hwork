class AddPlanNameToCarrier < ActiveRecord::Migration
  def self.up
    add_column :third_parties, :plan_name, :string
  end

  def self.down
    remove_column :third_parties, :plan_name
  end
end
