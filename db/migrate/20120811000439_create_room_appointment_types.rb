class CreateRoomAppointmentTypes < ActiveRecord::Migration
  def self.up
  create_table "room_appointment_types", :force => true do |t|
    t.integer  "room_id"
    t.integer  "appointment_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end
end

  def self.down
    drop_table "room_appointment_types"
  end
end
