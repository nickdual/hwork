class AddForeignKeys3 < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      update new_patient_options set default_provider_id = null
        where default_provider_id is not null
         and default_provider_id not in (select id from providers)
    SQL
    execute <<-SQL
      update patients set clinic_id = null
        where clinic_id is not null
         and clinic_id not in (select id from clinics)
    SQL
    execute <<-SQL
      delete from provider_legacy_id_labels where provider_id is not null
        and provider_id not in (select id from providers)
    SQL
    execute <<-SQL
      delete from themes where clinic_id is not null
        and clinic_id not in (select id from clinics)
    SQL
    execute <<-SQL
      delete from themes where user_id is not null
        and user_id not in (select id from users)
    SQL
    execute <<-SQL
      delete from role_assignments where role_id is not null
        and role_id not in (select id from roles)
    SQL
    execute <<-SQL
      delete from subscriptions where subscription_plan_id is not null
        and subscription_plan_id not in (select id from subscription_plans)
    SQL
    execute <<-SQL
      update third_parties set legacy_id_label_id = null
        where legacy_id_label_id is not null
         and legacy_id_label_id not in (select id from legacy_id_labels)
    SQL
    add_foreign_key(:imports, :accounts, :name => 'imports_accounts_fk', :dependent => :delete)
    add_foreign_key(:imports, :import_groups, :name => 'imports_import_groups_fk', :dependent => :delete)
    add_foreign_key(:legacy_id_labels, :accounts, :name => 'legacy_id_labels_accounts_fk', :dependent => :delete)
    add_foreign_key(:new_patient_options, :clinics, :name => 'new_patient_options_clinics_fk', :dependent => :delete)
    add_foreign_key(:new_patient_options, :providers, :name => 'new_patient_options_providers_fk', :column => :default_provider_id)
    add_foreign_key(:patients, :clinics, :name => 'patients_clinics_fk')
    add_foreign_key(:patients, :employers, :name => 'patients_employers_fk')
    add_foreign_key(:patients, :accounts, :name => 'patients_accounts_fk')
    add_foreign_key(:procedure_codes, :accounts, :name => 'procedure_codes_accounts_fk')
    add_foreign_key(:provider_legacy_id_labels, :providers, :name => 'plil_providers_fk', :dependent => :delete)
    add_foreign_key(:provider_legacy_id_labels, :legacy_id_labels, :name => 'plil_legacy_id_labels_fk', :dependent => :delete)
    add_foreign_key(:providers, :accounts, :name => 'providers_accounts_fk')
    add_foreign_key(:recurrences, :events, :name => 'recurrences_events_fk', :dependent => :delete)
    add_foreign_key(:references, :accounts, :name => 'references_accounts_fk', :dependent => :delete)
    add_foreign_key(:role_assignments, :roles, :name => 'role_assignments_roles_fk')
    add_foreign_key(:role_assignments, :users, :name => 'role_assignments_users_fk', :dependent => :delete)
    add_foreign_key(:room_locations, :rooms, :name => 'room_locations_rooms_fk', :dependent => :delete)
    add_foreign_key(:room_locations, :clinics, :name => 'room_locations_clinics_fk')
    add_foreign_key(:subscriptions, :accounts, :name => 'subscriptions_accounts_fk')
    add_foreign_key(:subscriptions, :subscription_plans, :name => 'subscriptions_subscription_plans_fk')
    add_foreign_key(:themes, :users, :name => 'themes_users_fk', :dependent => :delete)
    add_foreign_key(:themes, :clinics, :name => 'themes_clinics_fk', :dependent => :delete)
    add_foreign_key(:third_parties, :accounts, :name => 'third_parties_accounts_fk')
    add_foreign_key(:third_parties, :legacy_id_labels, :name => 'third_parties_legacy_id_labels_fk')
    add_foreign_key(:users, :accounts, :name => 'users_accounts_fk')
  end

  def self.down
    remove_foreign_key(:imports, :accounts, :name => 'imports_accounts_fk', :dependent => :delete)
    remove_foreign_key(:imports, :import_groups, :name => 'imports_import_groups_fk', :dependent => :delete)
    remove_foreign_key(:legacy_id_labels, :accounts, :name => 'legacy_id_labels_accounts_fk', :dependent => :delete)
    remove_foreign_key(:new_patient_options, :clinics, :name => 'new_patient_options_clinics_fk', :dependent => :delete)
    remove_foreign_key(:new_patient_options, :providers, :name => 'new_patient_options_providers_fk', :column => :default_provider_id)
    remove_foreign_key(:patients, :clinics, :name => 'patients_clinics_fk')
    remove_foreign_key(:patients, :employers, :name => 'patients_employers_fk')
    remove_foreign_key(:patients, :accounts, :name => 'patients_accounts_fk')
    remove_foreign_key(:procedure_codes, :accounts, :name => 'procedure_codes_accounts_fk')
    remove_foreign_key(:provider_legacy_id_labels, :providers, :name => 'plil_providers_fk', :dependent => :delete)
    remove_foreign_key(:provider_legacy_id_labels, :legacy_id_labels, :name => 'plil_legacy_id_labels_fk', :dependent => :delete)
    remove_foreign_key(:providers, :accounts, :name => 'providers_accounts_fk')
    remove_foreign_key(:recurrences, :events, :name => 'recurrences_events_fk', :dependent => :delete)
    remove_foreign_key(:references, :accounts, :name => 'references_accounts_fk', :dependent => :delete)
    remove_foreign_key(:role_assignments, :roles, :name => 'role_assignments_roles_fk')
    remove_foreign_key(:role_assignments, :users, :name => 'role_assignments_users_fk', :dependent => :delete)
    remove_foreign_key(:room_locations, :rooms, :name => 'room_locations_rooms_fk', :dependent => :delete)
    remove_foreign_key(:room_locations, :clinics, :name => 'room_locations_clinics_fk')
    remove_foreign_key(:subscriptions, :accounts, :name => 'subscriptions_accounts_fk')
    remove_foreign_key(:subscriptions, :subscription_plans, :name => 'subscriptions_subscription_plans_fk')
    remove_foreign_key(:themes, :users, :name => 'themes_users_fk', :dependent => :delete)
    remove_foreign_key(:themes, :clinics, :name => 'themes_clinics_fk', :dependent => :delete)
    remove_foreign_key(:third_parties, :accounts, :name => 'third_parties_accounts_fk')
    remove_foreign_key(:third_parties, :legacy_id_labels, :name => 'third_parties_legacy_id_labels_fk')
    remove_foreign_key(:users, :accounts, :name => 'users_accounts_fk')
  end
end
