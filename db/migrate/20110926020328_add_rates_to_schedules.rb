class AddRatesToSchedules < ActiveRecord::Migration
  def self.up
    add_column :schedules, :max_concurrent_appointments, :integer, :default => 1
    add_column :schedules, :max_appointments_per_hour, :integer, :default => 3
  end

  def self.down
    remove_column :schedules, :max_concurrent_appointments, :integer
    remove_column :schedules, :max_appointments_per_hour, :integer
  end
end
