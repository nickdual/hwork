class RenameClassNameToObjectClass < ActiveRecord::Migration
  def self.up
    rename_column :handyworks_objects, :class_name, :object_class
  end

  def self.down
    rename_column :handyworks_objects, :object_class, :class_name
  end
end
