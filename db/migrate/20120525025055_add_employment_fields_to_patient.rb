class AddEmploymentFieldsToPatient < ActiveRecord::Migration
  def self.up
    remove_column :employers, :title
    remove_column :employers, :occupation
    add_column :patients, :title, :string
    add_column :patients, :occupation, :string
  end

  def self.down
    add_column :employers, :title, :string
    add_column :employers, :occupation, :string
    remove_column :patients, :title
    remove_column :patients, :occupation
  end
end
