class CreateProviders < ActiveRecord::Migration
  def self.up
    create_table :providers do |t|
      t.string :signature_name
      t.string :provider_type_code
      t.string :tax_uid
      t.string :upin_uid
      t.string :license
      t.text :notes
      t.string :nycomp_testify
      t.string :npi_uid
      t.integer :account_id

      t.timestamps
    end
  end

  def self.down
    drop_table :providers
  end
end
