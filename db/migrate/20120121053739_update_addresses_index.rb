class UpdateAddressesIndex < ActiveRecord::Migration
  def self.up
    remove_index :addresses, :name => :addressable_idx
    add_index :addresses, [:addressable_id, :addressable_type, :label], :name => :addressable_idx
  end

  def self.down
    remove_index :addresses, :name => :addressable_idx
    add_index :addresses, [:addressable_id, :addressable_type], :name => :addressable_idx
  end
end
