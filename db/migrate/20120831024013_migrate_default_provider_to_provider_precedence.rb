class MigrateDefaultProviderToProviderPrecedence < ActiveRecord::Migration
  def self.up
    # use SQL to create the provider precedence records
    #   use backticks to isolate the `order` column b/c order is a reserved keyworkd in MySQL.
    execute <<-SQL
      insert into provider_precedences (`room_id`, `provider_id`, `order`, `created_at` ) select id, provider_id, 0, curdate() from rooms where provider_id is not null
    SQL
    remove_column :rooms, :provider_id
    add_foreign_key "provider_precedences", "rooms", :name => "provider_precedences_rooms_fk", :dependent => :delete
    add_foreign_key "provider_precedences", "providers", :name => "provider_precedences_providers_fk", :dependent => :delete

  end

  def self.down
    remove_foreign_key "provider_precedences", :name => "provider_precedences_rooms_fk"
    removeadd_foreign_key "provider_precedences", :name => "provider_precedences_providers_fk"
    add_column :rooms, :provider_id, :integer
    execute <<-SQL
      update rooms r set provider_id = ( select provider_id from provider_precedences where room_id = r.id and `order` = 0 )
       where exists ( select 1 from provider_precedences where room_id = r.id and `order` = 0 )
    SQL
  end
end
