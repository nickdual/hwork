class ChangeIcd10ColumnCase < ActiveRecord::Migration
  def self.up
    rename_column :diagnosis_codes, :ICD10, :icd10
  end

  def self.down
    rename_column :diagnosis_codes, :icd10, :ICD10
  end
end
