class AdjustEventSchema < ActiveRecord::Migration
  def self.up
    add_column :events, :duration, :integer
    remove_column :events, :end_time 
    remove_column :events, :end_date 
    rename_column :events, :interval, :recur_interval
  end

  def self.down
    remove_column :events, :duration
    add_column :events, :end_time, :time
    add_column :events, :end_date, :date
    rename_column :events, :recur_interval, :interval
  end
end
