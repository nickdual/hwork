class AddFirstClinicNameToAccount < ActiveRecord::Migration
  def self.up
    add_column :accounts, :clinic_name, :string
  end

  def self.down
    remove_column :accounts, :clinic_name
  end
end
