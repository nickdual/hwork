class AddTimeslotsPerHourToCalendar < ActiveRecord::Migration
  def self.up
    add_column :calendars, :time_slots_per_hour, :integer, :default => 4
  end

  def self.down
    remove_column :calendars, :time_slots_per_hour
  end
end
