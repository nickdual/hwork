class AddForeignKeys4 < ActiveRecord::Migration
  def self.up
    add_foreign_key(:event_locations, :events, :name => 'event_locations_events_fk', :dependent => :delete)
  end

  def self.down
    remove_foreign_key(:event_locations, :events, :name => 'event_locations_events_fk', :dependent => :delete)
  end
end
