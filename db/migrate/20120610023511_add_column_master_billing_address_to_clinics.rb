class AddColumnMasterBillingAddressToClinics < ActiveRecord::Migration
  def self.up
      add_column :clinics, :master_billing_address, :boolean, :default => false
    end

    def self.down
      remove_column :clinics, :master_billing_address
    end
end
