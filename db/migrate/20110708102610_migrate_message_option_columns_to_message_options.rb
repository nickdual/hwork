class MigrateMessageOptionColumnsToMessageOptions < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      UPDATE messages AS dest
          INNER JOIN ( SELECT m.id, 'MessageOption' type,
          a.id message_id
          FROM message_options m,
          new_patient_options n,
          messages a
          WHERE a.messageable_id = n.id
          AND n.clinic_id = m.clinic_id
          ) AS source
        SET dest.messageable_id = source.id,
          dest.messageable_type = source.type
        WHERE dest.id = source.message_id
    SQL
    execute <<-SQL
      update message_options as dest
          inner join ( select n.should_use_clinic_name, 
          n.should_show_clinic_on_letter,
          n.should_show_clinic_on_bill,
          n.should_print_clinic_address_on_envelope,
          n.statement_use,
          n.letter_use,
          n.clinic_id
          from new_patient_options n, message_options m
          where n.clinic_id = m.clinic_id ) as source
        set dest.should_use_clinic_name = source.should_use_clinic_name,
          dest.should_show_clinic_on_letter = source.should_show_clinic_on_letter,
          dest.should_show_clinic_on_bill = source.should_show_clinic_on_bill,
          dest.should_print_clinic_address_on_envelope = source.should_print_clinic_address_on_envelope,
          dest.statement_use = source.statement_use,
          dest.letter_use = source.letter_use
        where dest.clinic_id = source.clinic_id
    SQL
    remove_column :new_patient_options, :should_use_clinic_name
    remove_column :new_patient_options, :should_show_clinic_on_letter
    remove_column :new_patient_options, :should_show_clinic_on_bill
    remove_column :new_patient_options, :should_print_clinic_address_on_envelope
    remove_column :new_patient_options, :statement_use
    remove_column :new_patient_options, :letter_use
  end

  def self.down
    add_column :new_patient_options, :should_use_clinic_name, :boolean
    add_column :new_patient_options, :should_show_clinic_on_letter, :boolean
    add_column :new_patient_options, :should_show_clinic_on_bill, :boolean
    add_column :new_patient_options, :should_print_clinic_address_on_envelope, :boolean
    add_column :new_patient_options, :statement_use, :string
    add_column :new_patient_options, :letter_use, :boolean
    execute <<-SQL
      update new_patient_options as dest
          inner join ( select m.should_use_clinic_name, 
          m.should_show_clinic_on_letter,
          m.should_show_clinic_on_bill,
          m.should_print_clinic_address_on_envelope,
          m.statement_use,
          m.letter_use,
          m.clinic_id
          from new_patient_options n, message_options m
          where n.clinic_id = m.clinic_id ) as source
        set dest.should_use_clinic_name = source.should_use_clinic_name,
          dest.should_show_clinic_on_letter = source.should_show_clinic_on_letter,
          dest.should_show_clinic_on_bill = source.should_show_clinic_on_bill,
          dest.should_print_clinic_address_on_envelope = source.should_print_clinic_address_on_envelope,
          dest.statement_use = source.statement_use,
          dest.letter_use = source.letter_use
        where dest.clinic_id = source.clinic_id
    SQL
    execute <<-SQL
      UPDATE messages AS dest
          INNER JOIN ( SELECT n.id, 'NewPatientOption' type,
          a.id message_id
          FROM message_options m,
          new_patient_options n,
          messages a
          WHERE a.messageable_id = n.id
          AND n.clinic_id = m.clinic_id
          ) AS source
        SET dest.messageable_id = source.id,
          dest.messageable_type = source.type
        WHERE dest.id = source.message_id
    SQL
  end
end
