class RemoveUnecessaryStateColumnFromAccounts < ActiveRecord::Migration
  def self.up
    remove_column :accounts, :payment_status
  end

  def self.down
    add_column :accounts, :payment_status, :string
  end
end
