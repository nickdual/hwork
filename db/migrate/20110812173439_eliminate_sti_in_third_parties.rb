class EliminateStiInThirdParties < ActiveRecord::Migration
  def self.up
    remove_column :third_parties, :type
  end

  def self.down
    add_column :third_parties, :type, :string
  end
end
