#-------------------------------------------------------------------------------
# This migration is a "kludge" to put the sex column back on the patients table
# where it may or may not be at the time this migration is run depending on the
# state of the db your running it against.
#-------------------------------------------------------------------------------
class PutSexOnPatient < ActiveRecord::Migration
  def up
    begin
      add_column :patients, :sex, :integer, :default => 0
    rescue
    end
    begin
      remove_column :contacts, :sex
    rescue
    end
  end

  def down
    begin
      remove_column :patients, :sex
    rescue
    end
    begin
      add_column :contacts, :sex, :string, :default => "0"
    rescue
    end
  end
end
