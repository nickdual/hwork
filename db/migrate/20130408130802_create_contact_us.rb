class CreateContactUs < ActiveRecord::Migration
  def change
    create_table :contact_us do |t|
      t.string :name
      t.string :email
      t.string :city
      t.string :phone
      t.text :comment

      t.timestamps
    end
  end
end
