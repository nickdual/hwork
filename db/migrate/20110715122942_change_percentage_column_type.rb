class ChangePercentageColumnType < ActiveRecord::Migration
  def self.up
    add_column :procedure_codes, :new_tax_rate_percentage, :float
    execute <<-SQL
      update procedure_codes set new_tax_rate_percentage = tax_rate_percentage where id > 0
    SQL
    remove_column :procedure_codes, :tax_rate_percentage
    rename_column :procedure_codes, :new_tax_rate_percentage, :tax_rate_percentage
  end

  def self.down
    add_column :procedure_codes, :new_tax_rate_percentage, :integer
    execute <<-SQL
      update procedure_codes set new_tax_rate_percentage = tax_rate_percentage where id > 0
    SQL
    remove_column :procedure_codes, :tax_rate_percentage
    rename_column :procedure_codes, :new_tax_rate_percentage, :tax_rate_percentage
  end
end
