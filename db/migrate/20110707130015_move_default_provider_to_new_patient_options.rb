class MoveDefaultProviderToNewPatientOptions < ActiveRecord::Migration
  def self.up
    add_column :new_patient_options, :default_provider_id, :integer
    execute <<-SQL
      update new_patient_options as dest
        inner join ( select n.clinic_id, c.main_provider_id
        from new_patient_options n, clinics c
        where n.clinic_id = c.id ) as source
      set dest.default_provider_id = source.main_provider_id
      where dest.clinic_id = source.clinic_id
    SQL
    remove_column :clinics, :main_provider_id
  end

  def self.down
    add_column :clinics, :main_provider_id, :integer
    execute <<-SQL
      update clinics as dest
        inner join ( select c.id, n.default_provider_id
        from new_patient_options n, clinics c
        where n.clinic_id = c.id ) as source
      set dest.main_provider_id = source.default_provider_id
      where dest.id = source.id
    SQL
    remove_column :new_patient_options, :default_provider_id
  end
end
