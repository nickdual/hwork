class AddGenderToPatient < ActiveRecord::Migration
  def self.up
    add_column :patients, :sex, :integer, :default => 0
  end

  def self.down
    remove_column :patients, :sex
  end
end
