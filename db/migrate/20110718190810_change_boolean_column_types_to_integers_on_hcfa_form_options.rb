class ChangeBooleanColumnTypesToIntegersOnHcfaFormOptions < ActiveRecord::Migration
  def self.up
    change_column :hcfa_form_options, :box5, :integer
    change_column :hcfa_form_options, :plan_name, :integer
    change_column :hcfa_form_options, :address, :integer
    change_column :hcfa_form_options, :box4_g_name_is_same, :integer
    change_column :hcfa_form_options, :box4_7, :integer
    change_column :hcfa_form_options, :box6, :integer
    change_column :hcfa_form_options, :box7_g_addr_is_same, :integer
    change_column :hcfa_form_options, :box7_g_addr_is_p_addr, :integer
    change_column :hcfa_form_options, :box8, :integer
    change_column :hcfa_form_options, :box9, :integer
    change_column :hcfa_form_options, :box10, :integer
    change_column :hcfa_form_options, :box11, :integer
    change_column :hcfa_form_options, :box11a, :integer
    change_column :hcfa_form_options, :outside_lab, :integer
    change_column :hcfa_form_options, :print_diagnosis_description, :integer
    change_column :hcfa_form_options, :box21, :integer
    change_column :hcfa_form_options, :date_to, :integer
    change_column :hcfa_form_options, :date_from, :integer
    change_column :hcfa_form_options, :box24_cpt_97014, :integer
    change_column :hcfa_form_options, :amount_paid, :integer
    change_column :hcfa_form_options, :total_owed, :integer
    change_column :hcfa_form_options, :box31_provider_code, :integer
    change_column :hcfa_form_options, :clin_phone, :integer
    change_column :hcfa_form_options, :box32, :integer
  end

  def self.down
    change_column :hcfa_form_options, :box5, :boolean
    change_column :hcfa_form_options, :plan_name, :boolean
    change_column :hcfa_form_options, :address, :boolean
    change_column :hcfa_form_options, :box4_g_name_is_same, :boolean
    change_column :hcfa_form_options, :box4_7, :boolean
    change_column :hcfa_form_options, :box6, :boolean
    change_column :hcfa_form_options, :box7_g_addr_is_same, :boolean
    change_column :hcfa_form_options, :box7_g_addr_is_p_addr, :boolean
    change_column :hcfa_form_options, :box8, :boolean
    change_column :hcfa_form_options, :box9, :boolean
    change_column :hcfa_form_options, :box10, :boolean
    change_column :hcfa_form_options, :box11, :boolean
    change_column :hcfa_form_options, :box11a, :boolean
    change_column :hcfa_form_options, :outside_lab, :boolean
    change_column :hcfa_form_options, :print_diagnosis_description, :boolean
    change_column :hcfa_form_options, :box21, :boolean
    change_column :hcfa_form_options, :date_to, :boolean
    change_column :hcfa_form_options, :date_from, :boolean
    change_column :hcfa_form_options, :box24_cpt_97014, :boolean
    change_column :hcfa_form_options, :amount_paid, :boolean
    change_column :hcfa_form_options, :total_owed, :boolean
    change_column :hcfa_form_options, :box31_provider_code, :boolean
    change_column :hcfa_form_options, :clin_phone, :boolean
    change_column :hcfa_form_options, :box32, :boolean
  end
end
