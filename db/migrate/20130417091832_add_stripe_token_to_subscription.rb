class AddStripeTokenToSubscription < ActiveRecord::Migration
  def change
    add_column :subscriptions, :stripe_token, :string
  end
end
