class CreateThirdParties < ActiveRecord::Migration
  def self.up
    create_table :third_parties do |t|
      t.integer :account_id
      t.integer :insurance_carrier_type_code
      t.integer :insurance_type_code
      t.string :payer_code
      t.string :claims_office_sub_code
      t.string :clinic_code
      t.string :medigap_code
      t.string :alias_name
      t.integer :legacy_id_label_id
      t.text :notes
      t.string :type

      t.timestamps
    end
  end

  def self.down
    drop_table :third_parties
  end
end
