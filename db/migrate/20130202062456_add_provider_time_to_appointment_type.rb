class AddProviderTimeToAppointmentType < ActiveRecord::Migration
  def change
    add_column :appointment_types, :provider_time, :integer, :default => 60
  end
end
