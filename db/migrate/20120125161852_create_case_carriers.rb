class CreateCaseCarriers < ActiveRecord::Migration
  def self.up
    create_table :case_carriers do |t|
      t.integer "order", :default => 0
      t.integer "case_id"
      t.integer "carrier_id"
      t.integer "guarantor_id" 
      t.string "guarantor_relation", :limit => 1, :default => 'S'
      t.boolean "assigned", :default => false
      t.decimal "deductible", :precision => 10, :scale => 2
      t.decimal "paid", :precision => 10, :scale => 2
      t.boolean "hold", :default => false
      t.string "hcfa_box_1a"
      t.string "policy_number"
      t.string "group_number"
      t.string "authorization"
      t.timestamps
    end
  end

  def self.down
    drop_table :case_carriers
  end
end
