class DropTablesAttorneysAndInsuranceCarriers < ActiveRecord::Migration
  def self.up
    drop_table :attorneys
    drop_table :insurance_carriers
  end

  def self.down
    create_table :attorneys do |t|
      t.integer :account_id
      t.integer :insurance_carrier_id
      t.text :notes
      t.timestamps
    end

    create_table :insurance_carriers do |t|
      t.integer  :account_id
      t.integer  :insurance_carrier_type_code
      t.integer  :insurance_type_code
      t.string   :payer_code
      t.string   :claims_office_sub_code
      t.string   :clinic_code
      t.string   :medigap_code
      t.string   :alias_name
      t.integer  :legacy_id_label_id
      t.text     :notes
    end
  end
end
