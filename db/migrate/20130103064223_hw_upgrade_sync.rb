class HwUpgradeSync < ActiveRecord::Migration
  def up
    add_column :accounts, :last_4_digits, :string

    create_table "ckeditor_assets", :force => true do |t|
      t.string   "data_file_name",                  :null => false
      t.string   "data_content_type"
      t.integer  "data_file_size"
      t.integer  "assetable_id"
      t.string   "assetable_type",    :limit => 30
      t.string   "type",              :limit => 30
      t.integer  "width"
      t.integer  "height"
      t.datetime "created_at",                      :null => false
      t.datetime "updated_at",                      :null => false
    end

    add_index "ckeditor_assets", ["assetable_type", "assetable_id"], :name => "idx_ckeditor_assetable"
    add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_ckeditor_assetable_type"

    drop_table :dashboards
    drop_table :themes

  end

  def down
    remove_column :accounts, :last_4_digits
    drop_table :ckeditor_assets

    create_table "dashboards", :force => true do |t|
      t.integer  "account_id"
      t.string   "description", :default => "Welcome to your new account!"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "dashboards", ["account_id"], :name => "index_dashboards_on_account_id"

    create_table "themes", :force => true do |t|
      t.integer  "user_id"
      t.integer  "clinic_id"
      t.string   "name"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "themes", ["clinic_id"], :name => "themes_clinics_fk"
    add_index "themes", ["user_id"], :name => "themes_users_fk"
  end
end
