class AddSupressEbillTypeToThirdParty < ActiveRecord::Migration
  def self.up
    add_column :third_parties, :suppress_ebill, :boolean, :default => false
    remove_column :third_parties, :ebill_type_code
  end

  def self.down
    remove_column :third_parties, :suppress_ebill
    add_column :third_parties, :ebill_type_code, :integer, :default => 1
  end
end
