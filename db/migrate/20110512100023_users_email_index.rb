class UsersEmailIndex < ActiveRecord::Migration
  # this migration is removing the unique constraint on users.email
  def self.up
    remove_index :users, :column => :email
    add_index :users, :email, :name => 'index_users_email'
  end

  def self.down
    remove_index :users, :column => :email
    add_index :users, :email, :unique => true
  end
end
