class CreateWebhooks < ActiveRecord::Migration
  def change
    create_table :webhooks do |t|
      t.string :message
      t.integer :subscription_id
      t.timestamps
    end
  end
end
