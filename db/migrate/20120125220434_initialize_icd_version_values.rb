class InitializeIcdVersionValues < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      update diagnosis_codes set icd_version = 0 where icd_version is null
    SQL
  end

  def self.down
    execute <<-SQL
      update diagnosis_codes set icd_version = null where icd_version = 0
    SQL
  end
end
