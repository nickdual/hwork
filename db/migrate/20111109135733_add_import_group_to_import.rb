class AddImportGroupToImport < ActiveRecord::Migration
  def self.up
    add_column :imports, :import_group_id, :integer
  end

  def self.down
    remove_column :imports, :import_group_id
  end
end
