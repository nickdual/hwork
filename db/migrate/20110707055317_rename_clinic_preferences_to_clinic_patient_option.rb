class RenameClinicPreferencesToClinicPatientOption < ActiveRecord::Migration
  def self.up
    rename_table :clinic_preferences, :new_patient_options
  end

  def self.down
    rename_table :new_patient_options, :clinic_preferences
  end
end
