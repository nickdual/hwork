class CreateProviderLegacyIdLabels < ActiveRecord::Migration
  def self.up
    create_table :provider_legacy_id_labels do |t|
      t.integer :legacy_id_label_id
      t.integer :provider_id
      t.string :legacy_id_value

      t.timestamps
    end
  end

  def self.down
    drop_table :provider_legacy_id_labels
  end
end
