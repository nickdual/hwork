class AddStateToAppointments < ActiveRecord::Migration
  def self.up
    add_column :appointments, :state, :string, :default => "new"
  end

  def self.down
    remove_column :appointments, :state
  end
end
