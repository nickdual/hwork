class CreateTableThirdPartyBill < ActiveRecord::Migration
  def self.up
      create_table :third_party_bills, :force => true do |t|
      t.datetime "bill_date"
      t.integer  "case_id"
      t.integer  "third_party_id"
      t.integer  "provider_id"
      t.boolean  "benefits_assigned"
      t.boolean  "initial_or_followup_comp_bill"
      t.string   "primary_secondary_tertiary"     
      t.string   "claim_number_for_secondary_filing"
      t.string   "electronic_bill_status"
      t.string   "ansi5010field_clm05_3"
      t.text     "collection_notes"
      t.datetime "created_at"
      t.datetime "updated_at"
    end
  end

  def self.down
    drop_table :third_party_bills
  end
end
