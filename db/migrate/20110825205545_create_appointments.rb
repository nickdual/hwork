class CreateAppointments < ActiveRecord::Migration
  def self.up
    create_table :appointments do |t|
      t.integer :provider_id
      t.integer :room_id
      t.integer :contact_id
      t.integer :account_id

      t.timestamps
    end
  end

  def self.down
    drop_table :appointments
  end
end
