class SetStateOnExistingAccounts < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      update accounts x set state = 'active' where state is null and exists ( select * from payment_methods p where p.payer_type = 'Account' and p.payer_id = x.id )
    SQL

    execute <<-SQL
      update accounts x set state = 'collections' where state is null and not exists ( select * from payment_methods p where p.payer_type = 'Account' and p.payer_id = x.id )
    SQL
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
