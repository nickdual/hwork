class CreateContacts < ActiveRecord::Migration
  def self.up
    create_table :contacts do |t|
      t.string :first_name
      t.string :last_name
      t.string :company_name
      t.string :attention
      t.text :notes
      t.string :title
      t.integer :contactable_id
      t.string :contactable_type
      t.string :sex
      t.string :occupation
      t.string :middle_initial

      t.timestamps
    end
  end

  def self.down
    drop_table :contacts
  end
end
