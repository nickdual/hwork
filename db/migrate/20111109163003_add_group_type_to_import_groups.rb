class AddGroupTypeToImportGroups < ActiveRecord::Migration
  def self.up
    add_column :import_groups, :import_group_type, :string
  end

  def self.down
    remove_column :import_groups, :import_group_type
  end
end
