class AddImportColumnsToCases < ActiveRecord::Migration
  def self.up
    add_column :cases, :alt_id, :string
    add_column :cases, :alt_patient_id, :string
    add_column :cases, :alt_provider_id, :string
    add_column :cases, :alt_attorney_id, :string
    add_column :cases, :alt_referral_id, :string
    add_column :cases, :alt_fee_schedule_id, :string
    add_column :case_carriers, :alt_carrier_id, :string
    add_column :case_carriers, :alt_guarantor_id, :string
    add_column :case_diagnoses, :alt_diagnosis_code_id, :string
  end

  def self.down
    remove_column :cases, :alt_id
    remove_column :cases, :alt_patient_id
    remove_column :cases, :alt_provider_id
    remove_column :cases, :alt_attorney_id
    remove_column :cases, :alt_referral_id
    remove_column :cases, :alt_fee_schedule_id
    remove_column :case_carriers, :alt_carrier_id
    remove_column :case_carriers, :alt_guarantor_id
    remove_column :case_diagnoses, :alt_diagnosis_code_id
  end
end
