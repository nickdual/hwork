class AddEhrFieldsToPatients < ActiveRecord::Migration
  def self.up
	    add_column :patients, :race, :string
	    add_column :patients, :ethnicity, :string
	    add_column :patients, :language, :string
  end

  def self.down
    	remove_column :patients, :race, :string
    	remove_column :patients, :ethnicity, :string
    	remove_column :patients, :language, :string
  end
end
