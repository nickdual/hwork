class RemoveIcdVersionFromDiagnosisCodes < ActiveRecord::Migration
  def self.up
     remove_column :diagnosis_codes, :icd_version
  end

  def self.down
     add_column :diagnosis_codes, :icd_version, :integer, :default => 0
  end
end
