class AddColorToProvider < ActiveRecord::Migration
  def self.up
    add_column :providers, :appointment_color, :string, :default => '#68a1e5'
  end

  def self.down
    remove_column :providers, :appointment_color
  end
end
