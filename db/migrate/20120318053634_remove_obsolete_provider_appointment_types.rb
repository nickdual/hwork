class RemoveObsoleteProviderAppointmentTypes < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      delete from appointment_types where serviceable_type = 'Provider'
    SQL
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
