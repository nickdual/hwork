class CreateAppointmentTypes < ActiveRecord::Migration
  def self.up
    create_table :appointment_types do |t|
      t.string :name
      t.integer :duration_min, :default => 30
      t.integer :duration_max, :default => 90
      t.integer :schedule_lead_time, :default => 0
      t.integer :pre_appointment_gap, :default => 0
      t.integer :post_appointment_gap, :default => 0
      t.integer :serviceable_id
      t.string :serviceable_type
      t.integer :duration, :default => 60
      t.string :description
      t.boolean :disabled, :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :appointment_types
  end
end
