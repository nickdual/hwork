class ChangeDefaultImportGroupOptions < ActiveRecord::Migration
  def self.up
    change_column :import_groups, :duplicate_code, :integer, :default => 1
    change_column :import_groups, :error_code, :integer, :default => 1
  end

  def self.down
    change_column :import_groups, :duplicate_code, :integer, :default => 0
    change_column :import_groups, :error_code, :integer, :default => 0
  end
end
