class MigrateExistingMessageRecords < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      UPDATE messages AS dest
      INNER JOIN ( select c.account_id, mo.id from clinics c, message_options mo
      where c.id = mo.clinic_id) AS source
      SET alt_id = -1 * dest.messageable_id,
      dest.messageable_type = 'Account',
      dest.messageable_id = source.account_id,
      dest.type = 'Letter'
      WHERE source.id = dest.messageable_id
      AND dest.type = 'MessageStatement'
      AND dest.messageable_type = 'MessageOption'
    SQL
  end

  def self.down
    execute <<-SQL
      UPDATE messages AS dest
      SET dest.messageable_type = 'MessageOption',
      dest.messageable_id = -1 * dest.alt_id,
      dest.type = 'MessageStatment',
      dest.alt_id = null
      WHERE dest.type = 'Letter'
      AND dest.messageable_type = 'Account'
      AND dest.alt_id like '-%'
    SQL
  end
end
