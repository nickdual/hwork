class CreateAccounts < ActiveRecord::Migration
  def self.up
    create_table :accounts do |t|
      t.string :name
      t.string :full_domain

      t.timestamps
    end

    add_index "accounts", ["full_domain"], :name => "index_accounts_on_full_domain"

  end

  def self.down
    drop_table :accounts
  end
end
