class AssignStaffRole < ActiveRecord::Migration
  def self.up
    User.all.each do |user|
      unless user.admin? then
        Role.get_role("Staff").assign(user)
      end
    end
  end

  def self.down
    Role.get_role("Staff").users.delete_all
  end
end
