class AddBox1aToHcfaFormOption < ActiveRecord::Migration
  def self.up
    add_column :hcfa_form_options, :box1a, :integer
  end

  def self.down
    remove_column :hcfa_form_options, :box1a
  end
end
