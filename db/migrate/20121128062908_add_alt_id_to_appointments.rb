class AddAltIdToAppointments < ActiveRecord::Migration
  def self.up
    add_column :appointments, :alt_id, :string
  end

  def self.down
    remove_column :appointments, :alt_id, :string
  end
end
