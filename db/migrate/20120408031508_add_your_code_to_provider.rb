class AddYourCodeToProvider < ActiveRecord::Migration
  def self.up
	add_column :providers, :your_code, :string
  end

  def self.down
	remove_column :providers, :your_code, :string
  end
end
