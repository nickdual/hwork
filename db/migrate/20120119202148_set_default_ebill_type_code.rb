class SetDefaultEbillTypeCode < ActiveRecord::Migration
  def self.up
    change_column_default :third_parties, :ebill_type_code, 1
  end

  def self.down
    change_column_default :third_parties, :ebill_type_code, nil
  end
end
