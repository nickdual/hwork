class FixFeeStorageValue < ActiveRecord::Migration
  def self.up
    change_column :fees, :fee, :decimal, :precision => 8, :scale => 2
    change_column :fees, :copay, :decimal, :precision => 8, :scale => 2
    change_column :fees, :expected_insurance_payment, :decimal, :precision => 8, :scale => 2
  end

  def self.down
    change_column :fees, :fee, :decimal, :precision => 10, :scale => 0
    change_column :fees, :copay, :decimal, :precision => 10, :scale => 0
    change_column :fees, :expected_insurance_payment, :decimal, :precision => 10, :scale => 0
  end
end
