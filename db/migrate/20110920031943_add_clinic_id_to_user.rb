class AddClinicIdToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :clinic_id, :integer
  end

  def self.down
    remove_column :users, :clinic_id
  end
end
