class CreateClinics < ActiveRecord::Migration
  def self.up
    create_table :clinics do |t|
      t.string :tax_uuid
      t.string :type_ii_npi_uuid
      t.integer :main_provider_id
      t.integer :account_id

      t.timestamps
    end
  end

  def self.down
    drop_table :clinics
  end
end
