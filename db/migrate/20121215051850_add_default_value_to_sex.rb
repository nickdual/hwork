class AddDefaultValueToSex < ActiveRecord::Migration
  def self.up
    change_column_default :contacts, :sex, 0
  end

  def self.down
    change_column_default :contacts, :sex, nil
  end
end
