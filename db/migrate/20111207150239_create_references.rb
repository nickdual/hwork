class CreateReferences < ActiveRecord::Migration
  def self.up
    create_table :references do |t|
      t.integer :account_id
      t.string :upin
      t.string :npi
      t.string :alt_id
      t.text :comment

      t.timestamps
    end
  end

  def self.down
    drop_table :references
  end
end
