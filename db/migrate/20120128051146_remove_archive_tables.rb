class RemoveArchiveTables < ActiveRecord::Migration
  def self.up
    drop_table :archived_accounts
    drop_table :archived_addresses
    drop_table :archived_clinic_preferences
    drop_table :archived_clinics
    drop_table :archived_subscription_plans
    drop_table :archived_users
  end

  def self.down
    create_table "archived_accounts", :force => true do |t|
      t.string   "name"
      t.string   "full_domain"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.datetime "deleted_at"
    end
    add_index "archived_accounts", ["full_domain"], :name => "index_accounts_on_full_domain"

    create_table "archived_addresses", :force => true do |t|
      t.string   "street"
      t.string   "street2"
      t.string   "city"
      t.string   "state"
      t.string   "zip"
      t.string   "label"
      t.string   "addressable_id"
      t.string   "addressable_type"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.datetime "deleted_at"
    end

    create_table "archived_clinic_preferences", :force => true do |t|
      t.integer  "clinic_id"
      t.string   "internal_account_uid_scheme"
      t.string   "additional_transaction_number"
      t.string   "patient_number_scheme"
      t.string   "transaction_number_scheme"
      t.integer  "overdue_fee_percentage"
      t.boolean  "should_use_clinic_name"
      t.boolean  "should_print_diagnosis_description_on_hcfa"
      t.boolean  "should_send_statements_when_overdue"
      t.boolean  "should_charge_overdue_account"
      t.string   "insurance_carrier_assignment_policy"
      t.boolean  "should_show_clinic_on_letter"
      t.boolean  "should_show_clinic_on_bill"
      t.boolean  "should_print_clinic_address_on_envelope"
      t.integer  "payment_display_code"
      t.boolean  "should_split_bills_by_provider"
      t.integer  "default_place_of_service"
      t.string   "box_32_use"
      t.string   "box_33_use"
      t.string   "letter_use"
      t.string   "statement_use"
      t.float    "hcfa_left_margin"
      t.float    "hcfa_top_margin"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.datetime "deleted_at"
    end

    create_table "archived_clinics", :force => true do |t|
      t.string   "tax_uuid"
      t.string   "type_ii_npi_uuid"
      t.integer  "main_provider_id"
      t.integer  "account_id"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.datetime "deleted_at"
    end

    create_table "archived_subscription_plans", :force => true do |t|
      t.string   "name"
      t.decimal  "amount",                                :precision => 10, :scale => 2
      t.integer  "user_limit"
      t.integer  "renewal_period",                                                       :default => 1
      t.decimal  "setup_amount",                          :precision => 10, :scale => 2
      t.integer  "trial_period",                                                         :default => 1
      t.integer  "patient_limit"
      t.integer  "events_limit"
      t.integer  "emarketing_campaigns_limit_per_month"
      t.integer  "clinic_limit"
      t.integer  "office_staff_limit"
      t.integer  "provider_limit"
      t.integer  "file_storage_limit_megabytes"
      t.boolean  "is_import_export_limited"
      t.boolean  "is_hcfa_printing_limited"
      t.boolean  "is_invoicing_limited"
      t.boolean  "time_tracking"
      t.boolean  "is_appointment_scheduling_limited"
      t.boolean  "online_patient_scheduling"
      t.boolean  "website_templates"
      t.boolean  "vendor_integration"
      t.boolean  "inventory_tracking"
      t.boolean  "own_domain_name"
      t.boolean  "soap_note_report_writer"
      t.boolean  "address_verification"
      t.boolean  "labs_integration"
      t.boolean  "user_tracking_and_auditing"
      t.boolean  "daily_backups"
      t.boolean  "bookkeeping"
      t.boolean  "custom_transaction_gateway"
      t.integer  "yearly_amount"
      t.boolean  "is_user_tracking_and_auditing_limited"
      t.boolean  "is_online_patient_scheduling_limited"
      t.boolean  "is_website_templates_limited"
      t.boolean  "is_office_staff_unlimited"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.datetime "deleted_at"
    end

    create_table "archived_users", :force => true do |t|
      t.string   "email",                                 :default => "", :null => false
      t.string   "encrypted_password",     :limit => 128, :default => "", :null => false
      t.string   "reset_password_token"
      t.datetime "reset_password_sent_at"
      t.datetime "remember_created_at"
      t.integer  "sign_in_count",                         :default => 0
      t.datetime "current_sign_in_at"
      t.datetime "last_sign_in_at"
      t.string   "current_sign_in_ip"
      t.string   "last_sign_in_ip"
      t.string   "confirmation_token"
      t.datetime "confirmed_at"
      t.datetime "confirmation_sent_at"
      t.integer  "failed_attempts",                       :default => 0
      t.string   "unlock_token"
      t.datetime "locked_at"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "login"
      t.integer  "account_id"
      t.datetime "deleted_at"
      t.integer  "clinic_id"
    end

    add_index "archived_users", ["account_id"], :name => "index_users_on_account"
    add_index "archived_users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
    add_index "archived_users", ["email"], :name => "index_users_email"
    add_index "archived_users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
    add_index "archived_users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true
  end
end
