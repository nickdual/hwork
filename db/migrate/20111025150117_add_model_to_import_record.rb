class AddModelToImportRecord < ActiveRecord::Migration
  def self.up
    add_column :import_records, :model_type, :string
    add_column :import_records, :model_id, :integer
    change_column :import_records, :status, :integer, :default => 0
  end

  def self.down
    remove_column :import_records, :model_type
    remove_column :import_records, :model_id
    remove_column :import_records, :status
    change_column :import_records, :status, :integer
  end
end
