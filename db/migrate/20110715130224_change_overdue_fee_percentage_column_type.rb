class ChangeOverdueFeePercentageColumnType < ActiveRecord::Migration
  def self.up
    add_column :new_patient_options, :new_overdue_fee_percentage, :float
    execute <<-SQL
      update new_patient_options set new_overdue_fee_percentage = overdue_fee_percentage where id > 0
    SQL
    remove_column :new_patient_options, :overdue_fee_percentage
    rename_column :new_patient_options, :new_overdue_fee_percentage, :overdue_fee_percentage
  end

  def self.down
    add_column :new_patient_options, :new_overdue_fee_percentage, :integer
    execute <<-SQL
      update new_patient_options set new_overdue_fee_percentage = overdue_fee_percentage where id > 0
    SQL
    remove_column :new_patient_options, :overdue_fee_percentage
    rename_column :new_patient_options, :new_overdue_fee_percentage, :overdue_fee_percentage
  end
end
