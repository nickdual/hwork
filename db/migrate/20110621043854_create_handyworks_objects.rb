class CreateHandyworksObjects < ActiveRecord::Migration
  # use a local copy of the model to avoid conflicts with code changes
  class HandyworksObject < ActiveRecord::Base
  end

  def self.up
    create_table :handyworks_objects do |t|
      t.string :class_name
      t.string :description

      t.timestamps
    end

    HandyworksObject.create!(
      :class_name => 'User',
      :description => 'Web Account Users')
    HandyworksObject.create!(
      :class_name => 'Clinic',
      :description => 'Clinics')
    HandyworksObject.create!(
      :class_name => 'ClinicPreference',
      :description => 'Clinic Preferences')
    HandyworksObject.create!(
      :class_name => 'Account',
      :description => 'Account Preferences')
    HandyworksObject.create!(
      :class_name => 'ProcedureCode',
      :description => 'Procedure Codes')
    HandyworksObject.create!(
      :class_name => 'DiagnosisCode',
      :description => 'Diagnosis Codes')
    HandyworksObject.create!(
      :class_name => 'Provider',
      :description => 'Providers')
    HandyworksObject.create!(
      :class_name => 'Fee',
      :description => 'Procedure Code Fees')
    HandyworksObject.create!(
      :class_name => 'Bill',
      :description => 'Bills')
    HandyworksObject.create!(
      :class_name => 'Statement',
      :description => 'Statements')
    HandyworksObject.create!(
      :class_name => 'FinancialReport',
      :description => 'Financial Reports')
    HandyworksObject.create!(
      :class_name => 'InsuranceCarrier',
      :description => 'Insurance Carriers')
    HandyworksObject.create!(
      :class_name => 'Attorney',
      :description => 'Attorneys')
    HandyworksObject.create!(
      :class_name => 'Referrer',
      :description => 'Referrers')
    HandyworksObject.create!(
      :class_name => 'FeeSchedule',
      :description => 'Fee Schedules')
    HandyworksObject.create!(
      :class_name => 'Patient',
      :description => 'Patients')
    HandyworksObject.create!(
      :class_name => 'Case',
      :description => 'Patient Cases')
    HandyworksObject.create!(
      :class_name => 'Visit',
      :description => 'Patient Visits')
    HandyworksObject.create!(
      :class_name => 'Appointment',
      :description => 'Appointments')
    HandyworksObject.create!(
      :class_name => 'PatientReport',
      :description => 'Patient Reports')
  end

  def self.down
    drop_table :handyworks_objects
  end
end
