class CreateCaseDiagnoses < ActiveRecord::Migration
  def self.up
    create_table :case_diagnoses do |t|
      t.integer "case_id"
      t.integer "diagnosis_code_id"
      t.string "description"
      t.timestamps
    end
  end

  def self.down
    drop_table :case_diagnoses
  end
end
