class AddOwnerFieldsToImportGroup < ActiveRecord::Migration
  def self.up
    add_column :import_groups, :account_id, :integer
    add_column :import_groups, :user_id, :integer
  end

  def self.down
    remove_column :import_groups, :account_id
    remove_column :import_groups, :user_id
  end
end
