class ProviderRemoveObsoleteFields < ActiveRecord::Migration
  def self.up
    remove_column :providers, :upin_uid
    remove_column :providers, :license
    remove_column :providers, :nycomp_testify
  end

  def self.down
    add_column :providers, :upin_uid, :string
    add_column :providers, :license, :string
    add_column :providers, :nycomp_testify, :string
  end
end
