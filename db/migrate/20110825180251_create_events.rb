class CreateEvents < ActiveRecord::Migration
  def self.up
    create_table :events do |t|
      t.integer :day_of_week
      t.date :start_date
      t.date :end_date
      t.time :start_time
      t.time :end_time
      t.integer :lead_time
      t.integer :lag_time
      t.boolean :recur
      t.integer :interval
      t.integer :recur_count
      t.date :recur_start_date
      t.date :recur_end_date
      t.integer :eventable_id
      t.string :eventable_type

      t.timestamps
    end
  end

  def self.down
    drop_table :events
  end
end
