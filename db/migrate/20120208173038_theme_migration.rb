class ThemeMigration < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      update themes set name = 'start' where name = 'pepper_grinder'
    SQL
    execute <<-SQL
      update themes set name = 'ui-lightness' where name = 'sunny'
    SQL
    execute <<-SQL
      update themes set name = 'ob-custom' where name = 'hotsneaks'
    SQL
    execute <<-SQL
      update themes set name = 'redmond' where name = 'smoothness'
    SQL
  end

  def self.down
    execute <<-SQL
      update themes set name = 'pepper_grinder' where name = 'start'
    SQL
    execute <<-SQL
      update themes set name = 'sunny' where name = 'ui-lightness'
    SQL
    execute <<-SQL
      update themes set name = 'hotsneaks' where name = 'ob-custom'
    SQL
    execute <<-SQL
      update themes set name = 'smoothness' where name = 'redmond'
    SQL
  end
end
