class AddDefaultValuesToNewFeeSchedules < ActiveRecord::Migration
  def self.up
      change_column_default( :fees, :copay, 100) 
      change_column_default( :fees, :is_percentage, true)
  end

  def self.down
      change_column_default( :fees, :copay, nil) 
      change_column_default( :fees, :is_percentage, nil)
  end
end
