class CreateHandyworksObjectGroupAssignments < ActiveRecord::Migration
  # use a local copy of the model to avoid conflicts with code changes
  class HandyworksObject < ActiveRecord::Base
  end
  class HandyworksObjectGroup < ActiveRecord::Base
  end
  class HandyworksObjectGroupAssignment < ActiveRecord::Base
    belongs_to :handyworks_object
    belongs_to :handyworks_object_group
  end

  def self.up
    create_table :handyworks_object_group_assignments do |t|
      t.integer :handyworks_object_id
      t.integer :handyworks_object_group_id

      t.timestamps
    end
    account = Account.get_default
    group = account.handyworks_object_groups.find_by_label("Account")
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("User"))
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("Clinic"))
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("Account"))
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("ClinicPreference"))
    group = account.handyworks_object_groups.find_by_label("Clinical")
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("ProcedureCode"))
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("DiagnosisCode"))
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("Provider"))
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("Fee"))
    group = account.handyworks_object_groups.find_by_label("Financial")
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("Bill"))
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("Statement"))
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("FinancialReport"))
    group = account.handyworks_object_groups.find_by_label("Records")
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("InsuranceCarrier"))
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("Attorney"))
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("Referrer"))
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("FeeSchedule"))
    group = account.handyworks_object_groups.find_by_label("Patients")
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("Patient"))
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("Case"))
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("Visit"))
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("Appointment"))
    HandyworksObjectGroupAssignment.create!(
      :handyworks_object_group => group,
      :handyworks_object => HandyworksObject.find_by_class_name("PatientReport"))

  end

  def self.down
    drop_table :handyworks_object_group_assignments
  end
end
