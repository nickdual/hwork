class ChangeContactTypesToThirdParty < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      update contacts
        set type = 'ContactThirdParty'
          where contactable_type = 'Attorney'
    SQL
    execute <<-SQL
      update contacts
        set type = 'ContactThirdParty'
          where contactable_type = 'InsuranceCarrier'
    SQL
  end

  def self.down
    execute <<-SQL
      update contacts
        set type = 'ContactAttorney'
          where contactable_type = 'Attorney'
    SQL
    execute <<-SQL
      update contacts
        set type = 'ContactInsuranceCarrier'
          where contactable_type = 'InsuranceCarrier'
    SQL
  end
end
