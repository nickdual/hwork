class ApplyMonetize < ActiveRecord::Migration
  def self.up
    add_column :subscriptions, :amount_cents, :integer, :default => 0
    execute <<-SQL
      update subscriptions set amount_cents = amount * 100 where 1 = 1
    SQL
    remove_column :subscriptions, :amount
  end

  def self.down
    add_column :subscriptions, :amount, :decimal, :precision => 10, :scale => 2
    execute <<-SQL
      update subscriptions set amount = amount_cents / 100 where 1 = 1
    SQL
    remove_column :subscriptions, :amount_cents
  end
end
