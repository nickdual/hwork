class AddAttachmentDataToImport < ActiveRecord::Migration
  def self.up
    add_column :imports, :data_file_name, :string
    add_column :imports, :data_content_type, :string
    add_column :imports, :data_file_size, :integer
    add_column :imports, :data_updated_at, :datetime
    drop_table :csv_files
  end

  def self.down
    remove_column :imports, :data_file_name
    remove_column :imports, :data_content_type
    remove_column :imports, :data_file_size
    remove_column :imports, :data_updated_at

    create_table "csv_files", :force => true do |t|
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "data_file_name"
      t.string   "data_content_type"
      t.integer  "data_file_size"
      t.datetime "data_updated_at"
      t.integer  "import_id"
    end
  end
end
