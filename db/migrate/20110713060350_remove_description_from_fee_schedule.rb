class RemoveDescriptionFromFeeSchedule < ActiveRecord::Migration
  def self.up
    remove_column :fee_schedules, :description
  end

  def self.down
    add_column :fee_schedules, :description, :string
  end
end
