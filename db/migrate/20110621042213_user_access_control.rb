class UserAccessControl < ActiveRecord::Migration
  # use a local copy of the model to avoid conflicts with code changes
  class Role < ActiveRecord::Base
  end

  def self.up
    r = Role.find_by_name("Provider")
    Role.destroy(r.id) unless r.nil?
  end

  def self.down
    Role.create!(
      :name => 'Provider',
      :description => 'Medical Service Provider')
  end
end
