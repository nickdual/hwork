class DropAccountPreference < ActiveRecord::Migration
  def self.up
    drop_table :account_preferences
  end

  def self.down
    create_table "account_preferences", :force => true do |t|
      t.integer  "account_id"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "rooms_by_clinic", :default => false
    end
  end
end
