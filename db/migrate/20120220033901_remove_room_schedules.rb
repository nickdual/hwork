class RemoveRoomSchedules < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      delete from schedules where schedulable_type = 'Room'
    SQL
    execute <<-SQL
      delete from events where eventable_type = 'Schedule' and eventable_id not in (select id from schedules)
    SQL
    execute <<-SQL
      delete from recurrences where event_id not in (select id from events)
    SQL
    execute <<-SQL
      delete from event_locations where event_id not in (select id from events)
    SQL
  end

  def self.down
    # no way to accurately reset the updated accounts
    raise ActiveRecord::IrreversibleMigration
  end
end
