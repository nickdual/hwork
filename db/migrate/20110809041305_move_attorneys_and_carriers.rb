class MoveAttorneysAndCarriers < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      insert into third_parties (id, type, account_id, notes, created_at, updated_at) 
      select id + 1000, 'Attorney', account_id, notes, created_at, updated_at from attorneys
    SQL
    execute <<-SQL
      insert into third_parties (id,type, account_id, notes, created_at, updated_at,
      insurance_carrier_type_code, insurance_type_code, payer_code, claims_office_sub_code,
      clinic_code, medigap_code, alias_name, legacy_id_label_id)
      select id + 2000, 'InsuranceCarrier',
      account_id, notes, created_at, updated_at,
      insurance_carrier_type_code, insurance_type_code, payer_code, claims_office_sub_code,
      clinic_code, medigap_code, alias_name, legacy_id_label_id
      from insurance_carriers
    SQL
    execute <<-SQL
      update contacts 
       set contactable_id = contactable_id + 1000
        where contactable_type = 'Attorney'
    SQL
    execute <<-SQL
      update contacts
        set contactable_id = contactable_id + 2000
         where contactable_type = 'InsuranceCarrier'
    SQL
  end

  def self.down
    execute <<-SQL
      truncate table third_parties
    SQL
    execute <<-SQL
      update contacts 
       set contactable_id = contactable_id - 1000
        where contactable_type = 'Attorney'
    SQL
    execute <<-SQL
      update contacts
        set contactable_id = contactable_id - 2000
         where contactable_type = 'InsuranceCarrier'
    SQL
  end
end
