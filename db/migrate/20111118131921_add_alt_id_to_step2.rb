class AddAltIdToStep2 < ActiveRecord::Migration
  def self.up
    add_column :providers, :alt_id, :string
    add_column :legacy_id_labels, :alt_id, :string
    add_column :provider_legacy_id_labels, :alt_id, :string
  end

  def self.down
    remove_column :providers, :alt_id
    remove_column :legacy_id_labels, :alt_id
    remove_column :provider_legacy_id_labels, :alt_id
  end
end
