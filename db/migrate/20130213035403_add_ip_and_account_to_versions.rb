class AddIpAndAccountToVersions < ActiveRecord::Migration
  def change
    add_column :versions, :ip, :string
    add_column :versions, :account_id, :integer
    add_index :versions, :account_id
  end
end
