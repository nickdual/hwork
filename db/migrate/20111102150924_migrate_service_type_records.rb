class MigrateServiceTypeRecords < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      insert into appointment_types
(id, name, duration_min, duration_max, schedule_lead_time, pre_appointment_gap, post_appointment_gap, serviceable_id, serviceable_type, created_at, updated_at, duration)
      select 
          `id`,
          `name`,
          `duration_min`,
          `duration_max`,
          `schedule_lead_time`,
          `pre_appointment_gap`,
          `post_appointment_gap`,
          `serviceable_id`,
          `serviceable_type`,
          `created_at`,
          `updated_at`,
          (`duration_min`+`duration_max`)/2
      from
          `service_types`
    SQL
    drop_table :service_types
  end

  def self.down
    create_table :service_types do |t|
      t.string :name
      t.integer :duration_min
      t.integer :duration_max
      t.integer :schedule_lead_time
      t.integer :pre_appointment_gap
      t.integer :post_appointment_gap
      t.integer :serviceable_id
      t.string :serviceable_type
      t.timestamps
    end
    execute <<-SQL
      insert into service_types
(id, name, duration_min, duration_max, schedule_lead_time, pre_appointment_gap, post_appointment_gap, serviceable_id, serviceable_type, created_at, updated_at)
      select 
          `id`,
          `name`,
          `duration_min`,
          `duration_max`,
          `schedule_lead_time`,
          `pre_appointment_gap`,
          `post_appointment_gap`,
          `serviceable_id`,
          `serviceable_type`,
          `created_at`,
          `updated_at`
      from
          `service_types`
    SQL
  end
end
