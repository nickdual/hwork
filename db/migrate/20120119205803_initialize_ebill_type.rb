class InitializeEbillType < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      update third_parties set ebill_type_code = 1 where ebill_type_code is null
    SQL
  end

  def self.down
    execute <<-SQL
      update third_parties set ebill_type_code = null where ebill_type_code = 1
    SQL
  end
end
