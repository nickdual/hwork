class AddMessageFormatToMessages < ActiveRecord::Migration
  def self.up
      add_column :messages, :message_format, :string, :default => 'Letter (snail)'
  end

  def self.down
    drop_column :messages, :message_format
  end
end
