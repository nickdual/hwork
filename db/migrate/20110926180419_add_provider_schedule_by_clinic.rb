class AddProviderScheduleByClinic < ActiveRecord::Migration
  def self.up
    add_column :account_preferences, :provider_schedule_by_clinic, :boolean, :default => false
  end

  def self.down
    remove_column :account_preferences, :provider_schedule_by_clinic
  end
end
