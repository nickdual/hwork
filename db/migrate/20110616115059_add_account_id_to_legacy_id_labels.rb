class AddAccountIdToLegacyIdLabels < ActiveRecord::Migration
  def self.up
    add_column :legacy_id_labels, :account_id, :integer
  end

  def self.down
    remove_column :legacy_id_labels, :account_id
  end
end
