class CreateMessageOptions < ActiveRecord::Migration
  def self.up
    create_table :message_options do |t|
      t.integer :clinic_id
      t.boolean :should_use_clinic_name
      t.boolean :should_show_clinic_on_letter
      t.boolean :should_show_clinic_on_bill
      t.boolean :should_print_clinic_address_on_envelope
      t.string :statement_use

      t.timestamps
    end
  end

  def self.down
    drop_table :message_options
  end
end
