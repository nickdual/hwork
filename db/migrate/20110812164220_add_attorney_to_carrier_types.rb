class AddAttorneyToCarrierTypes < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      update third_parties set insurance_carrier_type_code = 14 where type = 'Attorney'
    SQL
  end

  def self.down
    execute <<-SQL
      update third_parties set insurance_carrier_type_code = null where type = 'Attorney'
    SQL
  end
end
