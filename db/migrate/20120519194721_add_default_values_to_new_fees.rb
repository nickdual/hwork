class AddDefaultValuesToNewFees < ActiveRecord::Migration
  def self.up
      change_column_default( :fees, :fee, 0) 
      change_column_default( :fees, :expected_insurance_payment, 0)
  end

  def self.down
      change_column_default( :fees, :fee, 0) 
      change_column_default( :fees, :expected_insurance_payment, 0)
  end
end
