class MakeLoginMatchCaseSensitive < ActiveRecord::Migration
  def self.up
    execute <<-SQL
       ALTER TABLE users MODIFY `login` varchar(255) CHARACTER SET latin1 COLLATE latin1_bin
    SQL
  end

  def self.down
    execute <<-SQL
       ALTER TABLE users MODIFY `login` varchar(255) CHARACTER SET latin1 COLLATE latin1
    SQL
  end
end
