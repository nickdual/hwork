class CreateDiagnosisCodes < ActiveRecord::Migration
  def self.up
    create_table :diagnosis_codes do |t|
      t.string :name
      t.string :code
      t.string :description
      t.integer :clinic_id

      t.timestamps
    end
  end

  def self.down
    drop_table :diagnosis_codes
  end
end
