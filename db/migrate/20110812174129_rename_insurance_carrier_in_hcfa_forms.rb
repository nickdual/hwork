class RenameInsuranceCarrierInHcfaForms < ActiveRecord::Migration
  def self.up
    rename_column :hcfa_form_options, :insurance_carrier_id, :third_party_id
  end

  def self.down
    rename_column :hcfa_form_options, :third_party_id, :insurance_carrier_id
  end
end
