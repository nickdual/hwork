class AddPrimaryFlagToPhone < ActiveRecord::Migration
  def self.up
    add_column :phones, :primary, :boolean, :default => false
    execute <<-SQL
     update phones set label = 'Home' where type = 'HomePhone'
    SQL
    execute <<-SQL
     update phones set label = 'Mobile' where type = 'MobilePhone'
    SQL
    execute <<-SQL
     update phones set label = 'Fax' where type = 'FaxPhone'
    SQL
    execute <<-SQL
     update phones set label = 'Work' where type = 'BusinessPhone'
    SQL
  end

  def self.down
    remove_column :phones, :primary
  end
end
