class CreateVisitDetails < ActiveRecord::Migration
  def self.up
    create_table :visit_details do |t|
      t.integer "visit_id"
      t.integer "procedure_code_id"
      t.timestamps
    end
    add_foreign_key(:visit_details, :visits, :name => 'visit_details_vists_fk')
    add_foreign_key(:visit_details, :procedure_codes, :name => 'visit_details_procedure_codes_fk')
  end

  def self.down
    remove_foreign_key(:visit_details, :visits, :name => 'visit_details_vists_fk')
    remove_foreign_key(:visit_details, :procedure_codes, :name => 'visit_details_procedure_codes_fk')
    drop_table :visit_details
  end
end
