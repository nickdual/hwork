class AddAccountIdToProcedureCodes < ActiveRecord::Migration
  def self.up
    add_column :procedure_codes, :account_id, :integer
    add_column :diagnosis_codes, :account_id, :integer
    #-----------------------------------------------------------------------------
    # fill in the correct account_id based on the current clinic_id
    #-----------------------------------------------------------------------------
    execute <<-SQL
      update procedure_codes p set account_id = (select account_id from clinics c where c.id = p.clinic_id)
      where p.clinic_id is not null
    SQL
    execute <<-SQL
      update diagnosis_codes p set account_id = (select account_id from clinics c where c.id = p.clinic_id)
      where p.clinic_id is not null
    SQL
    remove_column :procedure_codes, :clinic_id
    remove_column :diagnosis_codes, :clinic_id

  end

  def self.down
    add_column :procedure_codes, :clinic_id, :integer
    add_column :diagnosis_codes, :clinic_id, :integer
    execute <<-SQL
      update procedure_codes p set clinic_id = (select id from clinics c where c.account_id = p.account_id limit 1)
      where p.account_id is not null
    SQL
    execute <<-SQL
      update diagnosis_codes p set clinic_id = (select id from clinics c where c.account_id = p.account_id limit 1)
      where p.account_id is not null
    SQL
    remove_column :procedure_codes, :account_id
    remove_column :diagnosis_codes, :account_id
  end
end
