class MoveProviderScheduleByClinicToCalendar < ActiveRecord::Migration
  def self.up
    add_column :calendars, :provider_schedule_by_clinic, :boolean, :default => false
    execute <<-SQL
      update calendars c
      inner join (select a.id account_id, ap.provider_schedule_by_clinic 
      from accounts a, account_preferences ap
      where a.id = ap.account_id) as source
      set c.provider_schedule_by_clinic = source.provider_schedule_by_clinic
      where c.account_id = source.account_id
    SQL
    remove_column :account_preferences, :provider_schedule_by_clinic
  end

  def self.down
    add_column :account_preferences, :provider_schedule_by_clinic, :boolean, :default => false
    execute <<-SQL
      update account_preferences c
      inner join (select a.id account_id, ap.provider_schedule_by_clinic 
      from accounts a, calendars ap
      where a.id = ap.account_id) as source
      set c.provider_schedule_by_clinic = source.provider_schedule_by_clinic
      where c.account_id = source.account_id
    SQL
    remove_column :calendars, :provider_schedule_by_clinic
  end
end
