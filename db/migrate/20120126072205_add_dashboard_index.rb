class AddDashboardIndex < ActiveRecord::Migration
  def self.up
    change_column :dashboards, :description, :string, :default => "Welcome to your new account!"
    add_index :dashboards, :account_id
  end

  def self.down
    change_column :dashboards, :description, :string, :default => ""
    remove_index :dashboards, :account_id
  end
end
