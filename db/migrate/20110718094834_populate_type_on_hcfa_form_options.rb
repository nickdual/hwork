class PopulateTypeOnHcfaFormOptions < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      update hcfa_form_options set type = 'AccountHcfaFormOption' where id > 0
    SQL
  end

  def self.down
    execute <<-SQL
      update hcfa_form_options set type = null where id > 0
    SQL
  end
end
