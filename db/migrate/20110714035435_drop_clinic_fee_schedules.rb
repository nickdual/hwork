class DropClinicFeeSchedules < ActiveRecord::Migration
  def self.up
    drop_table :clinic_fee_schedules
  end

  def self.down
    create_table :clinic_fee_schedules do |t|
      t.integer :fee_schedule_id
      t.integer :clinic_id

      t.timestamps
    end
  end
end
