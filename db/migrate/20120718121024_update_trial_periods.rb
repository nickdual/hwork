class UpdateTrialPeriods < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      update subscription_plans set trial_period = 15 where 1 = 1;
    SQL
  end

  def self.down
    execute <<-SQL
      update subscription_plans set trial_period = 0 where 1 = 1;
    SQL
  end
end
