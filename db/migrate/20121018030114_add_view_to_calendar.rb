class AddViewToCalendar < ActiveRecord::Migration
  def self.up
    add_column :calendars, :view_by, :string, :default => "room"
  end

  def self.down
    remove_column :calendars, :view_by
  end
end
