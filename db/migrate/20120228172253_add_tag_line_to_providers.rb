class AddTagLineToProviders < ActiveRecord::Migration
  def self.up
    add_column :providers, :tag_line, :string
  end

  def self.down
    remove_column :providers, :tag_line
  end
end
