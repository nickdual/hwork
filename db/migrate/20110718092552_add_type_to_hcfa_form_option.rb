class AddTypeToHcfaFormOption < ActiveRecord::Migration
  def self.up
    add_column :hcfa_form_options, :type, :string
  end

  def self.down
    remove_column :hcfa_form_options, :type
  end
end
