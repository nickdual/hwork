class AddVersionToSchedules < ActiveRecord::Migration
  def self.up
    add_column :calendars, :version, :integer, :default => 0
  end

  def self.down
    remove_column :calendars, :version
  end
end
