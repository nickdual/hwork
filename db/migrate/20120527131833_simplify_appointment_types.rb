class SimplifyAppointmentTypes < ActiveRecord::Migration
  def self.up
    add_column :appointment_types, :account_id, :integer
    execute <<-SQL
      update appointment_types a set account_id = 
        ( select account_id from providers p where p.id = a.serviceable_id )
      where a.serviceable_type = 'Provider'
    SQL
    execute <<-SQL
      update appointment_types a set account_id = 
        ( select account_id from calendars p where p.id = a.serviceable_id )
      where a.serviceable_type = 'Calendar'
    SQL
    remove_column :appointment_types, :serviceable_type
    remove_column :appointment_types, :serviceable_id
  end

  def self.down
    add_column :appointment_types, :serviceable_type, :string
    add_column :appointment_types, :serviceable_id, :integer
    execute <<-SQL
      update appointment_types a set serviceable_type = 'Calendar',
       serviceable_id = 
         ( select id from calendars c where c.account_id = a.account_id )
    SQL
    remove_column :appointment_types, :account_id
  end
end
