class AddAltIdToRooms < ActiveRecord::Migration
  def self.up
    add_column :rooms, :alt_id, :string
  end

  def self.down
    remove_column :rooms, :alt_id
  end
end
