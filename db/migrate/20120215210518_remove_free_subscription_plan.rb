class RemoveFreeSubscriptionPlan < ActiveRecord::Migration
  def self.up
    # remove the free plan, and just to keep things functional
    # update the current accounts on the free plan to the basic plan.
    execute <<-SQL
      update subscriptions set subscription_plan_id = 2 where subscription_plan_id = 1
    SQL
    execute <<-SQL
      delete from subscription_plans where id = 1
    SQL
  end

  def self.down
    # no way to accurately reset the updated accounts
    raise ActiveRecord::IrreversibleMigration
  end
end
