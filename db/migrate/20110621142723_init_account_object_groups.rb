class InitAccountObjectGroups < ActiveRecord::Migration

  class Account < ActiveRecord::Base
    has_many :handyworks_object_groups
    DEFAULT_ACCOUNT_ID = 999999 # reference global defaults account
    def self.get_default
      find(DEFAULT_ACCOUNT_ID)
    end
    def just_do_it
      if self.handyworks_object_groups.size == 0 then
	Account.transaction do
	  Account.get_default.handyworks_object_groups.each do |og|
	    hog = og.clone
	    hog.account_id = self.id
	    hog.save!
	    og.handyworks_object_group_assignments.each do |oga|
	      hoga = oga.clone
	      hoga.handyworks_object_group_id = hog.id
	      hoga.save!
	    end
	  end
	  self.save!
	end
      end
    end
  end

  def self.up
    # define object groups for all current accounts
    Account.all.each do |account|
      account.just_do_it unless account.id == Account::DEFAULT_ACCOUNT_ID
    end
  end

  def self.down
    # delete all object groups except for those belonging to the default account
    execute <<-SQL
      DELETE FROM handyworks_object_group_assignments WHERE HANDYWORKS_OBJECT_GROUP_ID IN
        (SELECT ID FROM handyworks_object_groups WHERE ACCOUNT_ID != 999999)
        OR HANDYWORKS_OBJECT_GROUP_ID IS NULL
    SQL

    execute <<-SQL
      DELETE FROM handyworks_object_groups WHERE ACCOUNT_ID != 999999
    SQL
  end
end
