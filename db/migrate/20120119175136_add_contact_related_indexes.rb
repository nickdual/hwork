class AddContactRelatedIndexes < ActiveRecord::Migration
  def self.up
    add_index :third_parties, :account_id
    add_index :contacts, [:contactable_type, :contactable_id], :name => :contactable_idx
    add_index :addresses, [:addressable_type, :addressable_id], :name => :addressable_idx
    add_index :hcfa_form_options, :account_id
    add_index :hcfa_form_options, :third_party_id
  end

  def self.down
    remove_index :third_parties, :account_id
    remove_index :contacts, :name => :contactable_idx
    remove_index :addresses, :name => :addressable_idx
    remove_index :hcfa_form_options, :account_id
    remove_index :hcfa_form_options, :third_party_id
  end
end
