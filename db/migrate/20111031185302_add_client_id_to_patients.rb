class AddClientIdToPatients < ActiveRecord::Migration
  def self.up
    add_column :patients, :client_id, :string
  end

  def self.down
    remove_column :patients, :client_id
  end
end
