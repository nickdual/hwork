class CreateImports < ActiveRecord::Migration
  def self.up
    create_table :imports do |t|
      t.integer :account_id
      t.integer :user_id
      t.string :import_type

      t.timestamps
    end
  end

  def self.down
    drop_table :imports
  end
end
