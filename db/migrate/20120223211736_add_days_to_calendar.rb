class AddDaysToCalendar < ActiveRecord::Migration
  def self.up
    add_column :calendars, :days, :integer, :default => 5
  end

  def self.down
    remove_column :calendars, :days
  end
end
