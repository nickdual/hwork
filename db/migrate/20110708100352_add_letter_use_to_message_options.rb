class AddLetterUseToMessageOptions < ActiveRecord::Migration
  def self.up
    add_column :message_options, :letter_use, :string
  end

  def self.down
    remove_column :message_options, :letter_use
  end
end
