class AddTitleToEmployer < ActiveRecord::Migration
  def self.up
    add_column :employers, :title, :string
    add_column :employers, :occupation, :string
  end

  def self.down
    remove_column :employers, :title
    remove_column :employers, :occupation
  end
end
