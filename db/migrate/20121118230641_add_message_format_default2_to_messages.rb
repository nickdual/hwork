class AddMessageFormatDefault2ToMessages < ActiveRecord::Migration
  def self.up
    change_column :messages, :message_format, :string, :default => 'Letter (snail)'
  end

  def self.down
    drop_column :messages, :message_format
  end
end
