class MovePaymentProcessingToSubscription < ActiveRecord::Migration
  def up
    remove_column :accounts, :payment_system_id
    remove_column :accounts, :last_4_digits
    add_column :subscriptions, :last_4_digits, :string
    drop_table :payment_methods
  end

  def down
    add_column :accounts, :payment_system_id, :string
    add_column :accounts, :last_4_digits, :string
    remove_column :subscriptions, :last_4_digits

    create_table "payment_methods", :force => true do |t|
      t.string   "method_payment_id"
      t.integer  "payment_type"
      t.integer  "payer_id"
      t.string   "payer_type"
      t.string   "name"
      t.datetime "created_at"
      t.datetime "updated_at"
    end
  end
end
