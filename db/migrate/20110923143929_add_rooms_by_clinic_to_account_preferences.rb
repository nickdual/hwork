class AddRoomsByClinicToAccountPreferences < ActiveRecord::Migration
  def self.up
    add_column :account_preferences, :rooms_by_clinic, :boolean
  end

  def self.down
    remove_column :account_preferences, :rooms_by_clinic
  end
end
