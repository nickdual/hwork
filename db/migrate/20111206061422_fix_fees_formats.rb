class FixFeesFormats < ActiveRecord::Migration
  def self.up
    rename_column :fees, :expected_insurance_payment_cents, :expected_insurance_payment
    change_column :fees, :fee, :decimal
    change_column :fees, :copay, :decimal
    change_column :fees, :expected_insurance_payment, :decimal
  end

  def self.down
    rename_column :fees, :expected_insurance_payment, :expected_insurance_payment_cents
    change_column :fees, :fee, :integer
    change_column :fees, :copay, :integer
    change_column :fees, :expected_insurance_payment_cents, :integer
  end
end
