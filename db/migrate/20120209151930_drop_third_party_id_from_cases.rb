class DropThirdPartyIdFromCases < ActiveRecord::Migration
  def self.up
    remove_foreign_key(:cases, :name => 'cases_third_parties_fk')
    remove_column :cases, :third_party_id
  end

  def self.down
    add_column :cases, :third_party_id, :integer
    add_foreign_key(:cases, :third_parties, :name => 'cases_third_parties_fk')
  end
end
