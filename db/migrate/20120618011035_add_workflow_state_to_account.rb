class AddWorkflowStateToAccount < ActiveRecord::Migration
  def self.up
    add_column :accounts, :workflow_state, :string
  end

  def self.down
    remove_column :accounts, :workflow_state
  end
end
