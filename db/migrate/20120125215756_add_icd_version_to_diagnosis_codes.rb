class AddIcdVersionToDiagnosisCodes < ActiveRecord::Migration
  def self.up
    add_column :diagnosis_codes, :icd_version, :integer, :default => 0
  end

  def self.down
    remove_column :diagnosis_codes, :icd_version
  end
end
