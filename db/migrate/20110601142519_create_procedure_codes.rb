class CreateProcedureCodes < ActiveRecord::Migration
  def self.up
    create_table :procedure_codes do |t|
      t.string :name
      t.string :description
      t.integer :type_code
      t.integer :service_type_code
      t.string :modifier
      t.integer :tax_rate_percentage
      t.string :modifier2
      t.string :modifier3
      t.string :cpt_code
      t.integer :clinic_id

      t.timestamps
    end
  end

  def self.down
    drop_table :procedure_codes
  end
end
