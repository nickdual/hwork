class CreateVisits < ActiveRecord::Migration
  def self.up
    create_table :visits do |t|
      t.date "start_date"
      t.integer "case_id"
      t.timestamps
    end
    add_foreign_key(:visits, :cases, :name => 'visits_cases_fk')
  end

  def self.down
    remove_foreign_key(:visits, :cases, :name => 'visits_cases_fk')
    drop_table :visits
  end
end
