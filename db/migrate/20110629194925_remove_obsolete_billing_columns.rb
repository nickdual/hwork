class RemoveObsoleteBillingColumns < ActiveRecord::Migration
  def self.up
    remove_column :subscriptions, :card_number
    remove_column :subscriptions, :card_expiration
    remove_column :subscriptions, :billing_id
  end

  def self.down
    # these columns were empty before handyworks 0.1.0
    add_column :subscriptions, :card_number, :string
    add_column :subscriptions, :card_expiration, :string
    add_column :subscriptions, :billing_id, :string
  end
end
