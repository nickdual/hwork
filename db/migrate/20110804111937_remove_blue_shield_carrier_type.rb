class RemoveBlueShieldCarrierType < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      update insurance_carriers set insurance_carrier_type_code = 8 where insurance_carrier_type_code = 9
    SQL
    execute <<-SQL
      update hcfa_form_options set form_type_code = 8 where form_type_code = 9
    SQL
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
