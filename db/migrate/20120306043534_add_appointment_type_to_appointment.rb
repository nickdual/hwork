class AddAppointmentTypeToAppointment < ActiveRecord::Migration
  def self.up
    add_column :appointments, :appointment_type_id, :integer
    add_foreign_key(:appointments, :appointment_types, :name => 'appointments_appointment_types_fk')
  end

  def self.down
    remove_foreign_key(:appointments, :appointment_types, :name => 'appointments_appointment_types_fk')
    remove_column :appointments, :appointment_type_id
  end
end
