class RolifyCreatePermissions < ActiveRecord::Migration
  def self.up
    # enhance existing roles table
    add_column :roles, :resource_type, :string
    add_column :roles, :resource_id, :integer

    # create a vanilla HABTM joins table, it appears the gem needs it this way
    create_table(:users_roles, :id => false) do |t|
      t.references :user
      t.references :role
    end

    # suggested indexes on the joins tables and roles table.
    add_index(:roles, :name, :name => 'role_name_idx')
    add_index(:roles, [ :name, :resource_type, :resource_id ], :name => 'role_resource_idx')
    add_index(:users_roles, [ :user_id, :role_id ], :name => 'users_roles_joins_idx')

    # copy over the current has_many_through relationships
    execute <<-SQL
      insert into users_roles ( role_id, user_id )
      select role_id, user_id from role_assignments
    SQL

    # add in the expected foreign keys for the new joins table
    add_foreign_key "users_roles", "roles", :name => "users_roles_roles_fk"
    add_foreign_key "users_roles", "users", :name => "users_roles_users_fk", :dependent => :delete

    # ditch the old joins table.
    drop_table :role_assignments
  end

  def self.down
    remove_index(:roles, :name => 'role_name_idx')
    remove_index(:roles, :name => 'role_resource_idx')
    remove_index(:users_roles, :name => 'users_roles_joins_idx')

    create_table "role_assignments", :force => true do |t|
      t.integer  "role_id"
      t.integer  "user_id"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    execute <<-SQL
      insert into role_assignments ( id, role_id, user_id )
      SELECT @rownum:=@rownum+1 rownum, t.role_id, t.user_id FROM (SELECT @rownum:=0) r, users_roles t 
    SQL

    drop_table :users_roles

    remove_column :roles, :resource_type
    remove_column :roles, :resource_id

    add_foreign_key "role_assignments", "roles", :name => "role_assignments_roles_fk"
    add_foreign_key "role_assignments", "users", :name => "role_assignments_users_fk", :dependent => :delete
  end
end
