class ChangeDefaultDaysToOne < ActiveRecord::Migration
  def self.up
    change_column_default :calendars, :days, 1
  end

  def self.down
    change_column_default :calendars, :days, 5
  end
end
