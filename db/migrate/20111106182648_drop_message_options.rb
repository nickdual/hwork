class DropMessageOptions < ActiveRecord::Migration
  def self.up
    drop_table :message_options
  end

  def self.down
    create_table "message_options", :force => true do |t|
      t.integer  "clinic_id"
      t.boolean  "should_use_clinic_name"
      t.boolean  "should_show_clinic_on_letter"
      t.boolean  "should_show_clinic_on_bill"
      t.boolean  "should_print_clinic_address_on_envelope"
      t.string   "statement_use"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "letter_use"
    end
  end
end
