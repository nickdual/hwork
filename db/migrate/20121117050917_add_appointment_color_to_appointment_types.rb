class AddAppointmentColorToAppointmentTypes < ActiveRecord::Migration
  def self.up
    add_column :appointment_types, :appointment_color, :string, :default => "#e5804a"
  end

  def self.down
    remove_column :appointment_types, :appointment_color
  end
end
