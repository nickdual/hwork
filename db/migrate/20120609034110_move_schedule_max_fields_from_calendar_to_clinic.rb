class MoveScheduleMaxFieldsFromCalendarToClinic < ActiveRecord::Migration
  def self.up
    remove_column :calendars, :schedule_future_max_days
    remove_column :calendars, :schedule_history_max_days
    add_column :clinics, :schedule_future_max_days, :integer, :default => 30
    add_column :clinics, :schedule_history_max_days, :integer, :default => 60
  end

  def self.down
    add_column :calendars, :schedule_future_max_days, :integer, :default => 30
    add_column :calendars, :schedule_history_max_days, :integer, :default => 60
    remove_column :clinics, :schedule_future_max_days
    remove_column :clinics, :schedule_history_max_days   
  end
end
