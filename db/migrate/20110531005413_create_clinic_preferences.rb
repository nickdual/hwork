class CreateClinicPreferences < ActiveRecord::Migration
  def self.up
    create_table :clinic_preferences do |t|
      t.integer :clinic_id
      t.string :internal_account_uid_scheme
      t.string :additional_transaction_number
      t.string :patient_number_scheme
      t.string :transaction_number_scheme
      t.integer :overdue_fee_percentage
      t.boolean :should_use_clinic_name
      t.boolean :should_print_diagnosis_description_on_hcfa
      t.boolean :should_send_statements_when_overdue
      t.boolean :should_charge_overdue_account
      t.string :insurance_carrier_assignment_policy
      t.boolean :should_show_clinic_on_letter
      t.boolean :should_show_clinic_on_bill
      t.boolean :should_print_clinic_address_on_envelope
      t.integer :payment_display_code
      t.boolean :should_split_bills_by_provider
      t.integer :default_place_of_service
      t.string :box_32_use
      t.string :box_33_use
      t.string :letter_use
      t.string :statement_use
      t.float :hcfa_left_margin
      t.float :hcfa_top_margin

      t.timestamps
    end
  end

  def self.down
    drop_table :clinic_preferences
  end
end
