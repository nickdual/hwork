class CreateHcfaFormOptions < ActiveRecord::Migration
  def self.up
    create_table :hcfa_form_options do |t|
      t.integer :clinic_id
      t.integer :form_type_code
      t.string :state
      t.integer :insurance_carrier_id
      t.string :form_name
      t.boolean :plan_name
      t.boolean :address
      t.boolean :box4_g_name_is_same
      t.boolean :box4_7
      t.boolean :box5
      t.boolean :box6
      t.boolean :box7_g_addr_is_same
      t.boolean :box7_g_addr_is_p_addr
      t.boolean :box8
      t.boolean :box9
      t.integer :box9a
      t.integer :box9c
      t.integer :box9d
      t.integer :box9_s_name_is_same
      t.boolean :box10
      t.boolean :box11
      t.boolean :box11a
      t.integer :box11c
      t.integer :box11d
      t.string :date_auth
      t.string :date_format
      t.string :box17_upin
      t.string :box17_name
      t.string :box17_npi
      t.string :box19
      t.boolean :outside_lab
      t.boolean :print_diagnosis_description
      t.boolean :box21
      t.boolean :date_to
      t.boolean :date_from
      t.string :box24_tos
      t.string :box24_pos
      t.boolean :box24_cpt_97014
      t.string :box24_modifier
      t.integer :box24_diags
      t.string :box24_amount_format
      t.string :box24j_npi
      t.string :box24j_legacy
      t.string :box24i
      t.string :box25
      t.string :box27_30_format
      t.boolean :amount_paid
      t.boolean :total_owed
      t.integer :box27
      t.string :box29
      t.string :license
      t.boolean :box31_provider_code
      t.boolean :clin_phone
      t.boolean :box32
      t.string :box32_name
      t.integer :box32a
      t.string :box32b
      t.string :box33
      t.string :box33_name
      t.string :box33a
      t.integer :box33b
      t.timestamps
    end
  end

  def self.down
    drop_table :hcfa_form_options
  end
end
