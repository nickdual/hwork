class DefaultFeeIsPercentageValue < ActiveRecord::Migration
  def self.up
    change_column :fees, :is_percentage, :boolean, :default => false
  end

  def self.down
    change_column :fees, :is_percentage, :boolean
  end
end
