class RenameAccountOptions < ActiveRecord::Migration
  def self.up
    rename_table :account_options, :account_preferences
  end

  def self.down
    rename_table :account_preferences, :account_options
  end
end
