class AddFkToAppointmentTypes < ActiveRecord::Migration
  def self.up
    add_foreign_key(:appointment_types, :accounts, :name => 'appointment_types_accounts_fk')
  end

  def self.down
    remove_foreign_key(:appointment_types, :accounts, :name => 'appointment_types_accounts_fk')
  end
end
