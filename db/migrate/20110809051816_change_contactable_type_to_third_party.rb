class ChangeContactableTypeToThirdParty < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      update contacts
        set contactable_type = 'ThirdParty'
          where contactable_id >= 1000 
            and contactable_id <= 1999
            and contactable_type = 'Attorney'
    SQL
    execute <<-SQL
      update contacts
        set contactable_type = 'ThirdParty'
          where contactable_id >= 2000 
            and contactable_id <= 2999
            and contactable_type = 'InsuranceCarrier'
    SQL
  end

  def self.down
    execute <<-SQL
      update contacts
        set contactable_type = 'Attorney'
          where contactable_id >= 1000 
            and contactable_id <= 1999
            and contactable_type = 'ThirdParty'
    SQL
    execute <<-SQL
      update contacts
        set contactable_type = 'InsuranceCarrier'
          where contactable_id >= 2000 
            and contactable_id <= 2999
            and contactable_type = 'ThirdParty'
    SQL
  end
end
