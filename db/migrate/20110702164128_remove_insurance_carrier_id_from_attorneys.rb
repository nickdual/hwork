class RemoveInsuranceCarrierIdFromAttorneys < ActiveRecord::Migration
  def self.up
    remove_column :attorneys, :insurance_carrier_id
  end

  def self.down
    add_column :attorneys, :insurance_carrier_id, :integer
  end
end
