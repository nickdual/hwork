class AddScheduleFutureMaxDaysToAccountPreferences < ActiveRecord::Migration
  def self.up
    add_column :account_preferences, :schedule_future_max_days, :integer, :default => 30
  end

  def self.down
    remove_column :account_preferences, :schedule_future_max_days
  end
end
