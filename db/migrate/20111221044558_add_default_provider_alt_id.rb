class AddDefaultProviderAltId < ActiveRecord::Migration
  def self.up
    add_column :new_patient_options, :default_provider_alt_id, :string
  end

  def self.down
    remove_column :new_patient_options, :default_provider_alt_id
  end
end
