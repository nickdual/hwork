class CreateLegacyIdLabels < ActiveRecord::Migration
  def self.up
    create_table :legacy_id_labels do |t|
      t.string :label

      t.timestamps
    end
  end

  def self.down
    drop_table :legacy_id_labels
  end
end
