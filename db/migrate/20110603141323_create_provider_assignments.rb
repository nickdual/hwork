class CreateProviderAssignments < ActiveRecord::Migration
  def self.up
    create_table :provider_assignments do |t|
      t.integer :provider_id
      t.integer :clinic_id

      t.timestamps
    end
  end

  def self.down
    drop_table :provider_assignments
  end
end
