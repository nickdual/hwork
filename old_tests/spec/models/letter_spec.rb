require 'spec_helper'

describe Letter do
  before(:each) do
    @letter = FactoryGirl.create(:letter)
  end

  it { should validate_presence_of(:label) }

  it "should be valid" do
    @letter.should be_valid
  end

  it "should be the correct class" do
    @letter.should be_an_instance_of(Letter)
  end

  [ :date_format, :date_placement, :patient_address, :sign_off_placement, :signator ].each do |field|
    describe "integer (mapped) field #{field}" do
      map_name = field.to_s.pluralize.upcase
      v_list = eval "Letter::#{map_name}"
      key_accessor = "#{field}_key".to_sym

      it " should contain a definition of the value map " do
        v_list.should be_kind_of(Hash)
      end

      describe "allows key values from value map" do
        v_list.keys.each do |v|
          it { should allow_value(v).for(field) }
        end
      end

      describe "allows value values from value map" do
        v_list.values.each do |v|
          it { should allow_value(v).for(field) }
        end
      end

      describe "defines key value accessor method" do
        it { should respond_to(key_accessor.to_sym) }
      end

      [ 'no good', 'not real' ].each do |p|
        it { should_not allow_value(p).for(field) }
      end
    end
  end

  describe "default values" do
    before(:each) do
      @rc = Letter.new
    end

    it "should use 2 for date_format" do
      @rc.date_format.should eql(2)
    end
    it "should use 1 for date_placement" do
      @rc.date_placement.should eql(1)
    end
    it "should use 0 for patient_address" do
      @rc.patient_address.should eql(0)
    end
    it "should use 1 for sign_off_placement" do
      @rc.sign_off_placement.should eql(1)
    end
    it "should use 'Sincerely' for sign_off" do
      @rc.sign_off.should eql('Sincerely')
    end
    it "should use 1 for signator" do
      @rc.signator.should eql(1)
    end
  end

end
