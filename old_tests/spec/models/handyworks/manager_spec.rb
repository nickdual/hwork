require 'spec_helper'

describe Handyworks::Manager do

  before(:each) do
    @hwm = Handyworks::Manager.new
  end

  describe "authenticate_manager" do
    # test successful authentication
    it "should be ok with the valid manager password" do
      pending "implement the manager password"
    end

    # test failed authentication raises and exception
    it "should raise exception with invalid password" do
      pending "implement the manager password"
    end
  end

  describe "create_app_admin" do
    # test basic happy path
    #  - is valid user
    #  - has role application admin
    #  - has account default account
    it "should create an application admin account" do
      new_user = @hwm.create_app_admin('testlogin','testpass', 'testlogin@example.com')
      new_user.should be_valid
      new_user.account_id.should eql(Account::DEFAULT_ACCOUNT_ID)
      new_user.roles.should include(Role.find_by_name(Role::APPLICATION_ADMIN))
    end

    # test invalid / duplicate login name
    # test invalid email
    it "should do validations on the user before creation" do
      user = Factory.create(:user, :login => 'testlogin')
      expect { @hwm.create_app_admin('testlogin','testpass', 'testlogin@example.com') }.to raise_error(/login/i)
      expect { @hwm.create_app_admin('testloginB','testpass', 'testlogin.example.com') }.to raise_error(/email/i)
      #expect { @hwm.create_app_admin('testloginC','pass', 'testlogin@example.com') }.to raise_error(/password/i)
    end
  end
end
