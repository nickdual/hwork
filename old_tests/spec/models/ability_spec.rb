require 'spec_helper'
require 'cancan/matchers'

describe Ability do
  before(:each) do
    @account1 = Factory.create(:account)
    @account2 = Factory.create(:account)
    @admin_role = Role.find_by_name(Role::ACCOUNT_ADMIN)
    @staff_role = Role.find_by_name(Role::ACCOUNT_STAFF)
    @app_admin_role = Role.find_by_name(Role::APPLICATION_ADMIN)
    @admin_user = Factory.create(:user, :account => @account1)
    @staff_user = Factory.create(:user, :account => @account1)
    @app_admin_user = Factory.create(:user, :account => @account1)
    @admin_role.assign(@admin_user)
    @staff_role.assign(@staff_user)
    @app_admin_role.assign(@app_admin_user)
    @admin_user.save!
    @staff_user.save!
    @app_admin_user.save!
    @app_admin_ability = Ability.new(@app_admin_user)
    @staff_ability = Ability.new(@staff_user)
    @admin_ability = Ability.new(@admin_user)
  end

  describe("on Schedule") do
    describe("for Providers") do
      before(:each) do
        @provider1 = Factory.create(:provider, :account => @account1)
        @provider2 = Factory.create(:provider, :account => @account2)
        @schedule1 = Factory.create(:schedule, :schedulable => @provider1)
        @schedule2 = Factory.create(:schedule, :schedulable => @provider2)
      end
      describe("by admins") do
        it { @admin_ability.should be_able_to(:manage, @schedule1) }
        it { @admin_ability.should_not be_able_to(:manage, @schedule2) }
        it { @admin_ability.should be_able_to(:manage, Schedule) }
        it "should not be allowed to create schedule outside account" do
          schedule = Factory.build(:schedule, :schedulable_type => 'Provider', :schedulable_id => @provider2.id)
          @admin_ability.should_not be_able_to(:create,schedule)
        end
      end
      describe("by staff") do
      end
      describe("by app admins") do
      end
      describe("with Events") do
        before(:each) do
          @event1 = Factory.create(:event, :eventable => @schedule1)
          @event2 = Factory.create(:event, :eventable => @schedule2)
        end

        describe("by admins") do
          it { @admin_ability.should be_able_to(:manage, Event) }
          it { @admin_ability.should_not be_able_to(:manage, @event2) }

          it "should NOT allow admin to create events outside of accounnt" do
            event = Factory.build(:event, :eventable_type => 'Schedule', :eventable_id => @schedule2.id)
            @admin_ability.should_not be_able_to(:create, event)
          end
        end

        describe("by staff") do
          it { @staff_ability.should be_able_to(:read, @event1) }
          it { @staff_ability.should_not be_able_to(:read, @event2) }
        end

        describe("by app admins") do
          it { @app_admin_ability.should_not be_able_to(:manage,@event1) }
          it { @app_admin_ability.should_not be_able_to(:manage,@event2) }
        end

      end
    end
    describe("for Clinics") do
      before(:each) do
        @clinic1 = Factory.create(:clinic, :account => @account1)
        @clinic2 = Factory.create(:clinic, :account => @account2)
        @schedule1 = Factory.create(:schedule, :schedulable => @clinic1)
        @schedule2 = Factory.create(:schedule, :schedulable => @clinic2)
      end
      describe("by admins") do
        it { @admin_ability.should be_able_to(:manage, @schedule1) }
        it { @admin_ability.should_not be_able_to(:manage, @schedule2) }
        it { @admin_ability.should be_able_to(:manage, Schedule) }

        it "should not be allowed to create schedule outside account" do
          schedule = Factory.build(:schedule, :schedulable_type => 'Clinic', :schedulable_id => @clinic2.id)
          @admin_ability.should_not be_able_to(:create,schedule)
        end
      end
      describe("by staff") do
      end
      describe("by app admins") do
      end
      describe("with Events") do
        before(:each) do
          @event1 = Factory.create(:event, :eventable => @schedule1)
          @event2 = Factory.create(:event, :eventable => @schedule2)
        end

        describe("by admins") do
          it { @admin_ability.should be_able_to(:manage, Event) }
          it { @admin_ability.should_not be_able_to(:manage, @event2) }

          it "should NOT allow admin to create events outside of accounnt" do
            event = Factory.build(:event, :eventable_type => 'Schedule', :eventable_id => @schedule2.id)
            @admin_ability.should_not be_able_to(:create, event)
          end
        end

        describe("by staff") do
          it { @staff_ability.should be_able_to(:read, @event1) }
          it { @staff_ability.should_not be_able_to(:read, @event2) }
        end

        describe("by app admins") do
          it { @app_admin_ability.should_not be_able_to(:manage,@event1) }
          it { @app_admin_ability.should_not be_able_to(:manage,@event2) }
        end

      end
    end
  end
end
