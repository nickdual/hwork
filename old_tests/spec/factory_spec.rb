require 'spec_helper'

# this file contains a set of tests designed to verify the factory definitions
# are functioning as intended. its focused on the trickier nested relationships.

describe Factory do
  describe "sequences" do
    it "should have a unique email for each user" do
      u1 = Factory.create(:user)
      u2 = Factory.create(:user)
      u1.email == u2.email
    end
  end

  describe "inheritance" do
    it "should have parent values" do
      b = Factory.create(:business_phone)
      b.number.should_not be_nil
    end
  end

  describe "complex" do
    it "should have role assigned" do
      u = Factory.create(:account_admin_user)
      u.admin?.should be_true
    end
  end

  describe "physical_address" do
    it "should have a physical address" do
      c = Factory.create(:contact_clinic)
      c.physical_address.should_not be_nil
    end

    it "should have a physical address too" do
      account = Factory(:account_with_clinic)
      user = Factory(:user, :account => @account)
      clinic = Clinic.find_by_account_id(account.id)
      clinic.should_not be_nil
      clinic.physical_address_one_liner.should_not be_nil
    end
  end
end
