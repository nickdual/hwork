module AppHelp

  def test_sign_in
    @request ? @request.env["devise.mapping"] = Devise.mappings[:user] : nil;
    @account = Factory(:account)
    @clinic = Factory(:clinic, :account => @account)
    @user = Factory(:user, :account => @account)
    begin
      sign_in @user
    rescue NoMethodError
      # try to login a different way
    end
  end

  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end

  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.user
  end

end
