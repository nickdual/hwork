require 'spec_helper'

describe "ProcedureCodes" do
  include AppHelp

  def login(user)
      post user_session_path, :login => user.login, :password => 'please'
      response.should be_success
  end

  before(:each) do
    @account = Factory(:account_with_clinic)
    @clinic = @account.clinics.first
    @user = Factory(:user, :account => @account)
  end

  describe "GET /procedure_codes" do
    it "works! (now write some real specs)" do
      pending "login test method is not working"
      login @user
      get clinic_procedure_codes_path(@clinic)
      response.status.should be(200)
    end
  end
end
