require 'spec_helper'

describe "Providers" do
  include AppHelp

  def login(user)
      post user_session_path, :login => user.login, :password => 'please'
      response.should be_success
  end

  before(:each) do
    @account = Factory(:account_with_clinic)
    @user = Factory(:user, :account => @account)
    @provider = Factory(:provider, :account => @account)
  end

  describe "GET /providers" do
    it "works! (now write some real specs)" do
      pending "login method is not working"
      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
      login @user
      get providers_path
      response.status.should be(200)
    end
  end
end
