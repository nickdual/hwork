require 'spec_helper'

describe "Clinics" do
  include AppHelp

  def login(user)
      post user_session_path, :login => user.login, :password => 'please'
      response.should be_success
  end

  before(:each) do
    @account = Factory(:account_with_clinic)
    @user = Factory(:user, :account => @account)
  end

  describe "GET /clinics" do
    it "works (now write some real specs)" do
      pending "login test method is not working"
      login @user
      get clinics_path
      response.status.should be(200)
    end
  end
end
