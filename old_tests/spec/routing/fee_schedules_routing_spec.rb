require "spec_helper"

describe FeeSchedulesController do
  describe "routing" do

    it "recognizes and generates #index" do
      { :get => "/admin/fee_schedules" }.should route_to(:controller => "fee_schedules", :action => "index")
    end

    it "recognizes and generates #new" do
      { :get => "/admin/fee_schedules/new" }.should route_to(:controller => "fee_schedules", :action => "new")
    end

    it "recognizes and generates #show" do
      { :get => "/admin/fee_schedules/1" }.should route_to(:controller => "fee_schedules", :action => "show", :id => "1")
    end

    it "recognizes and generates #edit" do
      { :get => "/admin/fee_schedules/1/edit" }.should route_to(:controller => "fee_schedules", :action => "edit", :id => "1")
    end

    it "recognizes and generates #create" do
      { :post => "/admin/fee_schedules" }.should route_to(:controller => "fee_schedules", :action => "create")
    end

    it "recognizes and generates #update" do
      { :put => "/admin/fee_schedules/1" }.should route_to(:controller => "fee_schedules", :action => "update", :id => "1")
    end

    it "recognizes and generates #destroy" do
      { :delete => "/admin/fee_schedules/1" }.should route_to(:controller => "fee_schedules", :action => "destroy", :id => "1")
    end

  end
end
