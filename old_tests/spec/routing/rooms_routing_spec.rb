require "spec_helper"

describe RoomsController do
  describe "routing" do

    it "recognizes and generates #index" do
      { :get => "/setup/rooms" }.should route_to(:controller => "rooms", :action => "index")
    end

    it "recognizes and generates #new" do
      { :get => "/setup/rooms/new" }.should route_to(:controller => "rooms", :action => "new")
    end

    it "recognizes and generates #show" do
      { :get => "/setup/rooms/1" }.should route_to(:controller => "rooms", :action => "show", :id => "1")
    end

    it "recognizes and generates #edit" do
      { :get => "/setup/rooms/1/edit" }.should route_to(:controller => "rooms", :action => "edit", :id => "1")
    end

    it "recognizes and generates #create" do
      { :post => "/setup/rooms" }.should route_to(:controller => "rooms", :action => "create")
    end

    it "recognizes and generates #update" do
      { :put => "/setup/rooms/1" }.should route_to(:controller => "rooms", :action => "update", :id => "1")
    end

    it "recognizes and generates #destroy" do
      { :delete => "/setup/rooms/1" }.should route_to(:controller => "rooms", :action => "destroy", :id => "1")
    end

  end
end
