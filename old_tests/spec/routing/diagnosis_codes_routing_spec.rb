require "spec_helper"

describe DiagnosisCodesController do
  include AppHelp
  describe "routing" do
    before(:each) do
      begin
        test_sign_in
      rescue NoMethodError
        post user_session_path, :login => @user.login, :password => 'please'
        response.should be_success
      end
    end

    it "recognizes and generates #index" do
      { :get => "/setup/diagnosis_codes" }.should route_to(:controller => "diagnosis_codes", :action => "index")
    end

    it "recognizes and generates #new" do
      { :get => "/setup/diagnosis_codes/new" }.should route_to(:controller => "diagnosis_codes", :action => "new")
    end

    it "recognizes and generates #show" do
      { :get => "/setup/diagnosis_codes/1" }.should route_to(:controller => "diagnosis_codes", :action => "show", :id => "1")
    end

    it "recognizes and generates #edit" do
      { :get => "/setup/diagnosis_codes/1/edit" }.should route_to(:controller => "diagnosis_codes", :action => "edit", :id => "1")
    end

    it "recognizes and generates #create" do
      { :post => "/setup/diagnosis_codes" }.should route_to(:controller => "diagnosis_codes", :action => "create")
    end

    it "recognizes and generates #update" do
      { :put => "/setup/diagnosis_codes/1" }.should route_to(:controller => "diagnosis_codes", :action => "update", :id => "1")
    end

    it "recognizes and generates #destroy" do
      { :delete => "/setup/diagnosis_codes/1" }.should route_to(:controller => "diagnosis_codes", :action => "destroy", :id => "1")
    end

  end
end
