require "spec_helper"

describe ProcedureCodesController do
  include AppHelp
  describe "routing" do
    before(:each) do
      begin
        test_sign_in
      rescue NoMethodError
        post user_session_path, :login => @user.login, :password => 'please'
        response.should be_success
      end
    end

    it "recognizes and generates #index" do
      { :get => "/setup/procedure_codes" }.should route_to(:controller => "procedure_codes", :action => "index")
    end

    it "recognizes and generates #new" do
      { :get => "/setup/procedure_codes/new" }.should route_to(:controller => "procedure_codes", :action => "new")
    end

    it "recognizes and generates #show" do
      { :get => "/setup/procedure_codes/1" }.should route_to(:controller => "procedure_codes", :action => "show", :id => "1")
    end

    it "recognizes and generates #edit" do
      { :get => "/setup/procedure_codes/1/edit" }.should route_to(:controller => "procedure_codes", :action => "edit", :id => "1")
    end

    it "recognizes and generates #create" do
      { :post => "/setup/procedure_codes" }.should route_to(:controller => "procedure_codes", :action => "create")
    end

    it "recognizes and generates #update" do
      { :put => "/setup/procedure_codes/1" }.should route_to(:controller => "procedure_codes", :action => "update", :id => "1")
    end

    it "recognizes and generates #destroy" do
      { :delete => "/setup/procedure_codes/1" }.should route_to(:controller => "procedure_codes", :action => "destroy", :id => "1")
    end

  end
end
