require "spec_helper"

describe ImportsController do
  describe "routing" do

    it "recognizes and generates #index" do
      { :get => "/imports" }.should route_to(:controller => "imports", :action => "index")
    end

    it "recognizes and generates #new" do
      { :get => "/imports/new" }.should route_to(:controller => "imports", :action => "new")
    end

    it "recognizes and generates #show" do
      { :get => "/imports/1" }.should route_to(:controller => "imports", :action => "show", :id => "1")
    end

    it "recognizes and generates #edit" do
      { :get => "/imports/1/edit" }.should route_to(:controller => "imports", :action => "edit", :id => "1")
    end

    it "recognizes and generates #create" do
      { :post => "/imports" }.should route_to(:controller => "imports", :action => "create")
    end

    it "recognizes and generates #update" do
      { :put => "/imports/1" }.should route_to(:controller => "imports", :action => "update", :id => "1")
    end

    it "recognizes and generates #destroy" do
      { :delete => "/imports/1" }.should route_to(:controller => "imports", :action => "destroy", :id => "1")
    end

  end
end
