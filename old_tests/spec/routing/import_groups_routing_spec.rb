require "spec_helper"

describe ImportGroupsController do
  describe "routing" do

    it "recognizes and generates #index" do
      { :get => "/admin/import_groups" }.should route_to(:controller => "import_groups", :action => "index")
    end

    it "recognizes and generates #new" do
      { :get => "/admin/import_groups/new" }.should route_to(:controller => "import_groups", :action => "new")
    end

    it "recognizes and generates #show" do
      { :get => "/admin/import_groups/1" }.should route_to(:controller => "import_groups", :action => "show", :id => "1")
    end

    it "recognizes and generates #edit" do
      { :get => "/admin/import_groups/1/edit" }.should route_to(:controller => "import_groups", :action => "edit", :id => "1")
    end

    it "recognizes and generates #create" do
      { :post => "/admin/import_groups" }.should route_to(:controller => "import_groups", :action => "create")
    end

    it "recognizes and generates #update" do
      { :put => "/admin/import_groups/1" }.should route_to(:controller => "import_groups", :action => "update", :id => "1")
    end

    it "recognizes and generates #destroy" do
      { :delete => "/admin/import_groups/1" }.should route_to(:controller => "import_groups", :action => "destroy", :id => "1")
    end

  end
end
