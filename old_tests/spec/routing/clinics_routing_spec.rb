require "spec_helper"

describe ClinicsController do
  describe "routing" do

    it "recognizes and generates #index" do
      { :get => "/admin/clinics" }.should route_to(:controller => "clinics", :action => "index")
    end

    it "recognizes and generates #new" do
      { :get => "/admin/clinics/new" }.should route_to(:controller => "clinics", :action => "new")
    end

    it "recognizes and generates #show" do
      { :get => "/admin/clinics/1" }.should route_to(:controller => "clinics", :action => "show", :id => "1")
    end

    it "recognizes and generates #edit" do
      { :get => "/admin/clinics/1/edit" }.should route_to(:controller => "clinics", :action => "edit", :id => "1")
    end

    it "recognizes and generates #create" do
      { :post => "/admin/clinics" }.should route_to(:controller => "clinics", :action => "create")
    end

    it "recognizes and generates #update" do
      { :put => "/admin/clinics/1" }.should route_to(:controller => "clinics", :action => "update", :id => "1")
    end

    it "recognizes and generates #destroy" do
      { :delete => "/admin/clinics/1" }.should route_to(:controller => "clinics", :action => "destroy", :id => "1")
    end

  end
end
