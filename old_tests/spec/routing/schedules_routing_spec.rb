require "spec_helper"

describe SchedulesController do
  include AppHelp
  describe "routing" do
    before(:each) do
      begin
        test_sign_in
        @provider = Factory.create(:provider, :account => @account)
        @clinic = Factory.create(:clinic, :account => @account)
      rescue NoMethodError
        post user_session_path, :login => @user.login, :password => 'please'
        response.should be_success
      end
    end

    # routes as resource for providers
    it "recognizes and generates #edit" do
      { :get => "/setup/providers/#{@provider.id}/schedule/edit" }.should route_to(:controller => "schedules", :action => "edit", :provider_id => @provider.id.to_s)
    end

    it "recognizes and generates #update" do
      { :put => "/setup/providers/#{@provider.id}/schedule" }.should route_to(:controller => "schedules", :action => "update", :provider_id => @provider.id.to_s)
    end

    # routes as resource for clinics
    it "recognizes and generates #edit" do
      { :get => "/admin/clinics/#{@clinic.id}/schedule/edit" }.should route_to(:controller => "schedules", :action => "edit", :clinic_id => @clinic.id.to_s)
    end

    it "recognizes and generates #update" do
      { :put => "/admin/clinics/#{@clinic.id}/schedule" }.should route_to(:controller => "schedules", :action => "update", :clinic_id => @clinic.id.to_s)
    end
  end
end
