require "spec_helper"

describe CalendarsController do
  include AppHelp
  describe "routing" do
    before(:each) do
      begin
        test_sign_in
      rescue NoMethodError
        post user_session_path, :login => @user.login, :password => 'please'
        response.should be_success
      end
    end

    it "recognizes and generates #edit" do
      { :get => "/calendar/edit" }.should route_to(:controller => "calendars", :action => "edit")
    end

    it "recognizes and generates #update" do
      { :put => "/calendar" }.should route_to(:controller => "calendars", :action => "update")
    end

  end
end
