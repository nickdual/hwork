require "spec_helper"

describe ThirdPartiesController do
  describe "routing" do

    it "recognizes and generates #index" do
      { :get => "/setup/third_parties" }.should route_to(:controller => "third_parties", :action => "index")
    end

    it "recognizes and generates #new" do
      { :get => "/setup/third_parties/new" }.should route_to(:controller => "third_parties", :action => "new")
    end

    it "recognizes and generates #show" do
      { :get => "/setup/third_parties/1" }.should route_to(:controller => "third_parties", :action => "show", :id => "1")
    end

    it "recognizes and generates #edit" do
      { :get => "/setup/third_parties/1/edit" }.should route_to(:controller => "third_parties", :action => "edit", :id => "1")
    end

    it "recognizes and generates #create" do
      { :post => "/setup/third_parties" }.should route_to(:controller => "third_parties", :action => "create")
    end

    it "recognizes and generates #update" do
      { :put => "/setup/third_parties/1" }.should route_to(:controller => "third_parties", :action => "update", :id => "1")
    end

    it "recognizes and generates #destroy" do
      { :delete => "/setup/third_parties/1" }.should route_to(:controller => "third_parties", :action => "destroy", :id => "1")
    end

  end
end
