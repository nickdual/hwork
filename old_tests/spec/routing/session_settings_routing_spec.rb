require 'spec_helper'

describe SessionSettingsController do
  include AppHelp
  describe "routing" do
    before(:each) do
    end

    it "recognizes and generates #update" do
      { :put => '/session_settings' }.should route_to(:controller => 'session_settings', :action => 'update')
    end

    describe "only update" do
      [:get, :post, :delete].each do |method|
        it "does not handle #{method}" do
          { method => '/session_settings' }.should_not be_routable
        end
      end
    end
  end
end
