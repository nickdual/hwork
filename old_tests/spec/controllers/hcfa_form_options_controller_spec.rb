require 'spec_helper'

describe HcfaFormOptionsController do
  include AppHelp

  def mock_hcfa_form_option(stubs={:account_id => @account.id})
    @mock_hcfa_form_option ||= mock_model(HcfaFormOption, stubs).as_null_object
  end

  def valid_attributes
    Factory.attributes_for(:hcfa_form_option, :account_id => @user.account_id )
  end

  before(:each) do
    test_sign_in
    Role.find_by_name(Role::ACCOUNT_ADMIN).assign(@user)
  end


  describe "GET edit" do
    it "assigns the requested hcfa_form_option as @hcfa_form_option" do
      HcfaFormOption.stub(:find).with("37") { mock_hcfa_form_option }
      get :edit, :id => "37"
      assigns(:hcfa_form_option).should be(mock_hcfa_form_option)
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested hcfa_form_option" do
        HcfaFormOption.stub(:find).with("37") { mock_hcfa_form_option }
        mock_hcfa_form_option.should_receive(:update_attributes).with({'these' => 'params'})
        put :update, :id => "37", :hcfa_form_option => {'these' => 'params'}
      end

      it "assigns the requested hcfa_form_option as @hcfa_form_option" do
        HcfaFormOption.stub(:find) { mock_hcfa_form_option(:update_attributes => true) }
        put :update, :id => "1"
        assigns(:hcfa_form_option).should be(mock_hcfa_form_option)
      end

      it "redirects to the index" do
        HcfaFormOption.stub(:find) { mock_hcfa_form_option(:update_attributes => true, :account_id => @account.id) }
        put :update, :id => "1"
        response.should redirect_to(hcfa_form_options_url)
      end
    end

    describe "with invalid params" do
      it "assigns the hcfa_form_option as @hcfa_form_option" do
        HcfaFormOption.stub(:find) { mock_hcfa_form_option(:update_attributes => false) }
        put :update, :id => "1"
        assigns(:hcfa_form_option).should be(mock_hcfa_form_option)
      end

      it "re-renders the 'edit' template" do
        HcfaFormOption.stub(:find) { mock_hcfa_form_option(:update_attributes => false, :account_id => @account.id) }
        put :update, :id => "1"
        response.should render_template("edit")
      end
    end
  end
end
