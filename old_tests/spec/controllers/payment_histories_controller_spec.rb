require 'spec_helper'

describe PaymentHistoriesController do
  include AppHelp

  before(:each) do
    test_sign_in
    Role.find_by_name(Role::ACCOUNT_ADMIN).assign(@user)
  end

  describe "GET 'show'" do
    use_vcr_cassette

    it "should be successful" do
      get 'show'
      response.should be_success
    end
  end

end
