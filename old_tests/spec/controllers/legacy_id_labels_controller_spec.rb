require 'spec_helper'

describe LegacyIdLabelsController do
  include AppHelp

  def mock_legacy_id_label(stubs={:account_id => @account.id})
    @mock_legacy_id_label ||= mock_model(LegacyIdLabel, stubs).as_null_object
  end

  before(:each) do
    test_sign_in
    Role.find_by_name(Role::ACCOUNT_ADMIN).assign(@user)
  end

  describe "DELETE destroy" do
    it "destroys the requested legacy_id_label" do
      LegacyIdLabel.stub(:find).with("37") { mock_legacy_id_label }
      mock_legacy_id_label.should_receive(:destroy)
      delete :destroy, :id => "37"
    end

    it "redirects to the fee_schedules list" do
      LegacyIdLabel.stub(:find) { mock_legacy_id_label }
      delete :destroy, :id => "1"
      response.should redirect_to(providers_url)
    end
  end

end
