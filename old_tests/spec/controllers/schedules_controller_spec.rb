require 'spec_helper'

describe SchedulesController do
  include AppHelp
  before(:each) do
    test_sign_in
    Role.find_by_name(Role::ACCOUNT_ADMIN).assign(@user)
  end

  describe "for providers" do
    before(:each) do
      @provider = Factory.create(:provider, :account => @account)
    end

    def mock_schedule(stubs={:schedulable_type => 'Provider', :schedulable_id => @provider.id})
      @mock_schedule ||= mock_model(Schedule, stubs).as_null_object
    end

    describe "GET edit" do
      it "assigns the requested schedule as @schedule" do
        get :edit, :provider_id => @provider.id
        assigns(:schedule).should == @provider.schedule
      end
    end

    describe "PUT update" do
      describe "with valid params" do
        it "updates the requested schedule" do
          schedule_id = @provider.schedule.id
          put :update, :provider_id => @provider.id, :schedule => {'schedulable_type' => 'Something'}
          schedule = Schedule.find(schedule_id)
          schedule.schedulable_type.should eql('Something')
        end

        it "assigns the requested schedule as @schedule" do
          Schedule.stub(:find) { mock_schedule(:update_attributes => true, :provider_id => @provider.id) }
          put :update, :provider_id => @provider.id, :id => "1"
          assigns(:schedule).should be(mock_schedule)
        end

        it "redirects to the schedulable path" do
          pending "trouble with authorization in testing"
          schedule = mock_schedule(:update_attributes => true,:schedulable_type => 'Provider', :schedulable_id => @provider.id)
          Schedule.stub(:find) { schedule }
          @provider.schedule = schedule
          put :update, :id => "1", :proivider_id => @provider.id
          response.should redirect_to(provider_path(@provider))
        end
      end

      describe "with invalid params" do
        it "assigns the schedule as @schedule" do
          Schedule.stub(:find) { mock_schedule(:update_attributes => false) }
          put :update, :id => "1", :provider_id => @provider.id
          assigns(:schedule).should be(mock_schedule)
        end

        it "re-renders the 'edit' template" do
          pending "trouble with authorization in testing"
          Schedule.stub(:find) { mock_schedule(:update_attributes => false, :schedulable_id => @provider.id) }
          put :update, :id => "1", :schedulable_id => @provider.id
          response.should render_template("edit")
        end
      end
    end
  end

  describe "for clinics" do
    before(:each) do
      @clinic = Factory.create(:clinic)
    end

    def mock_schedule(stubs={:schedulable => @clinic })
      @mock_schedule ||= mock_model(Schedule, stubs).as_null_object
    end

    describe "GET edit" do
      it "assigns the requested schedule as @schedule" do
        get :edit, :clinic_id => @clinic.id
        assigns(:schedule).should == @clinic.schedule
      end
    end

    describe "PUT update" do
      describe "with valid params" do
        it "updates the requested schedule" do
          schedule_id = @clinic.schedule.id
          put :update, :clinic_id => @clinic.id, :schedule => {'schedulable_type' => 'Something'}
          schedule = Schedule.find(schedule_id)
          schedule.schedulable_type.should eql('Something')
        end

        it "assigns the requested schedule as @schedule" do
          Schedule.stub(:find) { mock_schedule(:update_attributes => true, :clinic_id => @clinic.id) }
          put :update, :clinic_id => @clinic.id, :id => "1"
          assigns(:schedule).should be(mock_schedule)
        end

        it "redirects to the schedulable path" do
          pending "trouble with authorization in testing"
          Schedule.stub(:find) { mock_schedule(:update_attributes => true, :schedulable_id => @clinic.id) }
          put :update, :id => "1", :schedulable_id => @clinic.id
          response.should redirect_to(clinic_path(@clinic))
        end
      end

      describe "with invalid params" do
        it "assigns the schedule as @schedule" do
          Schedule.stub(:find) { mock_schedule(:update_attributes => false) }
          put :update, :id => "1", :clinic_id => @clinic.id
          assigns(:schedule).should be(mock_schedule)
        end

        it "re-renders the 'edit' template" do
          pending "trouble with authorization in testing"
          Schedule.stub(:find) { mock_schedule(:update_attributes => false, :clinic_id => @clinic.id) }
          put :update, :id => "1", :clinic_id => @clinic.id
          response.should render_template("edit")
        end
      end
    end
  end

end
