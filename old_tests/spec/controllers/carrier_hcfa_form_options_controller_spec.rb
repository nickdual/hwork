require 'spec_helper'

describe CarrierHcfaFormOptionsController do
  include AppHelp

  before(:each) do
    test_sign_in
    Role.find_by_name(Role::ACCOUNT_ADMIN).assign(@user)
    @carrier = Factory(:insurance_carrier, :account => @account)
  end

  def mock_carrier_hcfa_form_option(stubs={:third_party_id => @carrier.id})
    @mock_carrier_hcfa_form_option ||= mock_model(CarrierHcfaFormOption, stubs).as_null_object
  end

  describe "GET show" do
    it "assigns the requested carrier_hcfa_form_option as @carrier_hcfa_form_option" do
      CarrierHcfaFormOption.stub(:find).with("37") { mock_carrier_hcfa_form_option }
      get :show, :id => "37", :third_party_id => @carrier.id
      assigns(:carrier_hcfa_form_option).should be(mock_carrier_hcfa_form_option)
    end
  end

  describe "GET new" do
    it "assigns a new carrier_hcfa_form_option as @carrier_hcfa_form_option" do
      CarrierHcfaFormOption.stub(:new) { mock_carrier_hcfa_form_option }
      get :new, :third_party_id => @carrier.id
      assigns(:carrier_hcfa_form_option).should be(mock_carrier_hcfa_form_option)
    end
  end

  describe "GET edit" do
    it "assigns the requested carrier_hcfa_form_option as @carrier_hcfa_form_option" do
      CarrierHcfaFormOption.stub(:find).with("37") { mock_carrier_hcfa_form_option }
      get :edit, :id => "37", :third_party_id => @carrier.id
      assigns(:carrier_hcfa_form_option).should be(mock_carrier_hcfa_form_option)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "assigns a newly created carrier_hcfa_form_option as @carrier_hcfa_form_option" do
        CarrierHcfaFormOption.stub(:new).with({'these' => 'params'}) { mock_carrier_hcfa_form_option(:save => true) }
        post :create, :carrier_hcfa_form_option => {'these' => 'params'}, :third_party_id => @carrier.id
        assigns(:carrier_hcfa_form_option).should be(mock_carrier_hcfa_form_option)
      end

      it "redirects to the created carrier_hcfa_form_option" do
        pending 
        CarrierHcfaFormOption.stub(:new) { mock_carrier_hcfa_form_option(:save => true, :third_party_id => @carrier.id) }
        post :create, :third_party_id => @carrier.id, :carrier_hcfa_form_option => Factory.attributes_for(:carrier_hcfa_form_option, :insurance_carrier => @carrier)
        response.should redirect_to(insurance_carrier_hcfa_form_option_url(@carrier, mock_carrier_hcfa_form_option))
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved carrier_hcfa_form_option as @carrier_hcfa_form_option" do
        CarrierHcfaFormOption.stub(:new).with({'these' => 'params'}) { mock_carrier_hcfa_form_option(:save => false) }
        post :create, :carrier_hcfa_form_option => {'these' => 'params'}, :third_party_id => @carrier.id
        assigns(:carrier_hcfa_form_option).should be(mock_carrier_hcfa_form_option)
      end

      it "re-renders the 'new' template" do
        pending
        CarrierHcfaFormOption.stub(:new) { mock_carrier_hcfa_form_option(:save => false, :third_party_id => @carrier.id) }
        post :create, :carrier_hcfa_form_option => {:third_party_id => @carrier.id}, :third_party_id => @carrier.id
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested carrier_hcfa_form_option" do
        pending
        CarrierHcfaFormOption.stub(:find).with("37") { mock_carrier_hcfa_form_option }
        mock_carrier_hcfa_form_option.should_receive(:update_attributes).with({'these' => 'params'})
        put :update, :third_party_id => @carrier.id, :id => "37", :carrier_hcfa_form_option => {'these' => 'params'}
      end

      it "assigns the requested carrier_hcfa_form_option as @carrier_hcfa_form_option" do
        CarrierHcfaFormOption.stub(:find) { mock_carrier_hcfa_form_option(:update_attributes => true, :third_party_id => @carrier.id) }
        put :update, :third_party_id => @carrier.id, :id => "1"
        assigns(:carrier_hcfa_form_option).should be(mock_carrier_hcfa_form_option)
      end

      it "redirects to the carrier_hcfa_form_option" do
        pending
        CarrierHcfaFormOption.stub(:find) { mock_carrier_hcfa_form_option(:update_attributes => true, :third_party_id => @carrier.id) }
        put :update, :id => "1", :third_party_id => @carrier.id
        response.should redirect_to(insurance_carrier_hcfa_form_option_url(@carrier, mock_carrier_hcfa_form_option))
      end
    end

    describe "with invalid params" do
      it "assigns the carrier_hcfa_form_option as @carrier_hcfa_form_option" do
        CarrierHcfaFormOption.stub(:find) { mock_carrier_hcfa_form_option(:update_attributes => false) }
        put :update, :id => "1", :third_party_id => @carrier.id
        assigns(:carrier_hcfa_form_option).should be(mock_carrier_hcfa_form_option)
      end

      it "re-renders the 'edit' template" do
        pending
        CarrierHcfaFormOption.stub(:find) { mock_carrier_hcfa_form_option(:update_attributes => false, :third_party_id => @carrier.id) }
        put :update, :id => "1", :third_party_id => @carrier.id
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested carrier_hcfa_form_option" do
      pending
      CarrierHcfaFormOption.stub(:find).with("37") { mock_carrier_hcfa_form_option }
      mock_carrier_hcfa_form_option.should_receive(:destroy)
      delete :destroy, :third_party_id => @carrier.id, :id => "37"
    end

    it "redirects to the carrier_hcfa_form_options list" do
      pending
      CarrierHcfaFormOption.stub(:find) { mock_carrier_hcfa_form_option }
      delete :destroy, :id => "1", :third_party_id => @carrier.id
      response.should redirect_to(insurance_carrier_hcfa_form_options_url)
    end
  end
end
