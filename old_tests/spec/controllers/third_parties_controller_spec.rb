require 'spec_helper'

describe ThirdPartiesController do
  include AppHelp

  before(:each) do
    test_sign_in
    Role.find_by_name(Role::ACCOUNT_ADMIN).assign(@user)
  end

  def valid_attributes
    Factory.attributes_for(:third_party, :account_id => @user.account_id )
  end

  def mock_third_party(stubs={:account_id => @account.id})
    @mock_third_party ||= mock_model(ThirdParty, stubs).as_null_object
  end

  describe "GET index" do
    it "assigns all third_parties as @third_parties" do
      ThirdParty.stub_chain(:accessible_by, :index_associations) { [mock_third_party] }
      get :index
      assigns(:third_parties).should eq([mock_third_party])
    end
  end

  describe "GET show" do
    it "assigns the requested third_party as @third_party" do
      ThirdParty.stub(:find).with("37") { mock_third_party }
      get :show, :id => "37"
      assigns(:third_party).should be(mock_third_party)
    end
  end

  describe "GET new" do
    it "assigns a new third_party as @third_party" do
      ThirdParty.stub(:new) { mock_third_party }
      get :new
      assigns(:third_party).should be(mock_third_party)
    end
  end

  describe "GET edit" do
    it "assigns the requested third_party as @third_party" do
      ThirdParty.stub(:find).with("37") { mock_third_party }
      get :edit, :id => "37"
      assigns(:third_party).should be(mock_third_party)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "assigns a newly created third_party as @third_party" do
        ThirdParty.stub(:new).with({'these' => 'params'}) { mock_third_party(:save => true) }
        post :create, :third_party => {'these' => 'params'}
        assigns(:third_party).should be(mock_third_party)
      end

      it "redirects to the created third_party" do
        ThirdParty.stub(:new) { mock_third_party(:save => true, :account_id => @account.id) }
        post :create, :third_party => valid_attributes
        response.should redirect_to(third_party_url(mock_third_party))
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved third_party as @third_party" do
        ThirdParty.stub(:new).with({'these' => 'params'}) { mock_third_party(:save => false) }
        post :create, :third_party => {'these' => 'params'}
        assigns(:third_party).should be(mock_third_party)
      end

      it "re-renders the 'new' template" do
        ThirdParty.stub(:new) { mock_third_party(:save => false, :account_id => @account.id) }
        post :create, :third_party => valid_attributes
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested third_party" do
        ThirdParty.stub(:find).with("37") { mock_third_party }
        mock_third_party.should_receive(:update_attributes).with({'these' => 'params'})
        put :update, :id => "37", :third_party => {'these' => 'params'}
      end

      it "assigns the requested third_party as @third_party" do
        ThirdParty.stub(:find) { mock_third_party(:update_attributes => true) }
        put :update, :id => "1"
        assigns(:third_party).should be(mock_third_party)
      end

      it "redirects to the third_party" do
        ThirdParty.stub(:find) { mock_third_party(:update_attributes => true, :account_id => @account.id) }
        put :update, :id => "1"
        response.should redirect_to(third_parties_url)
      end
    end

    describe "with invalid params" do
      it "assigns the third_party as @third_party" do
        ThirdParty.stub(:find) { mock_third_party(:update_attributes => false) }
        put :update, :id => "1"
        assigns(:third_party).should be(mock_third_party)
      end

      it "re-renders the 'edit' template" do
        ThirdParty.stub(:find) { mock_third_party(:update_attributes => false, :account_id => @account.id) }
        put :update, :id => "1"
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested third_party" do
      ThirdParty.stub(:find).with("37") { mock_third_party }
      mock_third_party.should_receive(:destroy)
      delete :destroy, :id => "37"
    end

    it "redirects to the third_parties list" do
      ThirdParty.stub(:find) { mock_third_party }
      delete :destroy, :id => "1"
      response.should redirect_to(third_parties_url)
    end
  end

end
