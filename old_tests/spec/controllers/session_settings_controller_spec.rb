require 'spec_helper'

describe SessionSettingsController do
  include AppHelp

  before(:each) do
    test_sign_in
  end

  describe "PUT update" do
    it "should not allow setting clinic id to inaccessible clinic" do
      clinic = double(Clinic)
      Clinic.stub(:find).with("37") { clinic }
      put :update, :clinic_id => "37"
      response.should_not be_success
    end

    it "should update the session variable clinic_id" do
      @clinic = Factory.create(:clinic, :account => @account)
      session[:clinic_id].should_not eql(@clinic.id)
      put :update, :clinic_id => @clinic.id
      session[:clinic_id].should eql(@clinic.id)
    end
  end

end
