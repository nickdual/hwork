require 'spec_helper'

describe AppointmentTypesController do
  include AppHelp

  def mock_appointment_type(stubs={:serviceable_id => @account.id, :serviceable_type => 'Account'})
    @mock_appointment_type ||= mock_model(AppointmentType, stubs).as_null_object
  end

  before(:each) do
    test_sign_in
    Role.find_by_name(Role::ACCOUNT_ADMIN).assign(@user)
  end

  describe "DELETE destroy" do
    it "destroys the requested appointment_type" do
      AppointmentType.stub(:find).with("37") { mock_appointment_type }
      mock_appointment_type.should_receive(:destroy)
      delete :destroy, :id => "37", :whereto => '/anywhere'
    end

    it "redirects to the fee_schedules list" do
      AppointmentType.stub(:find) { mock_appointment_type }
      delete :destroy, :id => "1", :whereto => '/fooit'
      response.should redirect_to('/fooit')
    end
  end
end
