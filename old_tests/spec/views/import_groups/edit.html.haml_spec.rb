require 'spec_helper'

describe "import_groups/edit.html.haml" do
  before(:each) do
    @import_group = assign(:import_group, stub_model(ImportGroup))
  end

  it "renders the edit import_group form" do
      pending "this is just the scaffold test, fix or remove"
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => import_groups_path(@import_group), :method => "post" do
    end
  end
end
