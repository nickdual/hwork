require 'spec_helper'

describe "import_groups/new.html.haml" do
  before(:each) do
    assign(:import_group, stub_model(ImportGroup).as_new_record)
  end

  it "renders new import_group form" do
    pending "this is just the scaffold test, fix or remove"
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => import_groups_path, :method => "post" do
    end
  end
end
