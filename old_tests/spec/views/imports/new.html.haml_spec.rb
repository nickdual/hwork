require 'spec_helper'

describe "imports/new.html.haml" do
  before(:each) do
    assign(:import, stub_model(Import,
      :account_id => 1,
      :user_id => 1,
      :import_type => "MyString"
    ).as_new_record)
  end

  it "renders new import form" do
      pending "this is just the scaffold test, fix or remove"
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => imports_path, :method => "post" do
      assert_select "input#import_account_id", :name => "import[account_id]"
      assert_select "input#import_user_id", :name => "import[user_id]"
      assert_select "input#import_import_type", :name => "import[import_type]"
    end
  end
end
