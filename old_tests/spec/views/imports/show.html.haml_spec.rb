require 'spec_helper'

describe "imports/show.html.haml" do
  before(:each) do
    @import = assign(:import, stub_model(Import,
      :account_id => 1,
      :user_id => 1,
      :import_type => "Import Type"
    ))
  end

  it "renders attributes in <p>" do
      pending "this is just the scaffold test, fix or remove"
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Import Type/)
  end
end
