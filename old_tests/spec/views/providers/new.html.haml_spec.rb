require 'spec_helper'

describe "providers/new.html.haml" do
  include AppHelp
  before(:each) do
    test_sign_in
    assign(:provider, stub_model(Provider,
      :signature_name => "MyString",
      :provider_type_code => "MyString",
      :tax_uid => "MyString",
      :notes => "MyText",
      :npi_uid => "MyString",
      :contact_id => 1,
      :account_id => 1
    ).as_new_record)
  end

  it "renders new provider form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => providers_path, :method => "post" do
      assert_select "input#provider_signature_name", :name => "provider[signature_name]"
      assert_select "select#provider_provider_type_code", :name => "provider[provider_type_code]"
      assert_select "input#provider_tax_uid", :name => "provider[tax_uid]"
      assert_select "textarea#provider_notes", :name => "provider[notes]"
      assert_select "input#provider_npi_uid", :name => "provider[npi_uid]"
      assert_select "input#provider_account_id", :name => "provider[account_id]"
    end
  end
end
