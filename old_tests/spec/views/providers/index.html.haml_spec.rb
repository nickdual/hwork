require 'spec_helper'

describe "providers/index.html.haml" do
  include AppHelp
  before(:each) do
    test_sign_in
    assign(:providers, [
      stub_model(Provider,
        :signature_name => "Signature Name",
        :provider_type_code => "Provider Type Code",
        :tax_uid => "Tax Uid",
        :notes => "MyText",
        :npi_uid => "Npi Uid",
        :account_id => @account.id
      ),
      stub_model(Provider,
        :signature_name => "Signature Name",
        :provider_type_code => "Provider Type Code",
        :tax_uid => "Tax Uid",
        :notes => "MyText",
        :npi_uid => "Npi Uid",
        :account_id => @account.id
      )
    ])
  end

  it "renders a list of providers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    #assert_select "td", :text => "Title".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Provider Type Code".to_s, :count => 2
  end
end
