require 'spec_helper'

describe "providers/show.html.haml" do
  include AppHelp
  before(:each) do
    test_sign_in
    @provider = assign(:provider, stub_model(Provider,
      :signature_name => "Signature Name",
      :provider_type_code => "Provider Type Code",
      :tax_uid => "Tax Uid",
      :upin_uid => "Upin Uid",
      :license => "License",
      :notes => "MyText",
      :nycomp_testify => "Nycomp Testify",
      :npi_uid => "Npi Uid",
      :account_id => @account.id
    ))
  end

  it "renders attributes in <p>" do
    pending "failing in the _switcher partial"
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Signature Name/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Tax Uid/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Upin Uid/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/License/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/MyText/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nycomp Testify/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Npi Uid/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
  end
end
