require 'spec_helper'

describe "providers/edit.html.haml" do
  include AppHelp
  before(:each) do
    test_sign_in
    @provider = assign(:provider, stub_model(Provider,
      :signature_name => "MyString",
      :provider_type_code => "MyString",
      :tax_uid => "MyString",
      :upin_uid => "MyString",
      :license => "MyString",
      :notes => "MyText",
      :nycomp_testify => "MyString",
      :npi_uid => "MyString",
      :account_id => @account.id 
    ))
  end

  it "renders the edit provider form" do
    pending "failing in the _switcher partial"
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => providers_path(@provider), :method => "post" do
      assert_select "input#provider_signature_name", :name => "provider[signature_name]"
      assert_select "select#provider_provider_type_code", :name => "provider[provider_type_code]"
      assert_select "input#provider_tax_uid", :name => "provider[tax_uid]"
      assert_select "input#provider_upin_uid", :name => "provider[upin_uid]"
      assert_select "input#provider_license", :name => "provider[license]"
      assert_select "textarea#provider_notes", :name => "provider[notes]"
      assert_select "input#provider_nycomp_testify", :name => "provider[nycomp_testify]"
      assert_select "input#provider_npi_uid", :name => "provider[npi_uid]"
      assert_select "input#provider_account_id", :name => "provider[account_id]"
    end
  end
end
