require 'spec_helper'

describe "fee_schedules/index.html.haml" do
  include AppHelp
  before(:each) do
    test_sign_in
    assign(:fee_schedules, [
      stub_model(FeeSchedule, 
                :label => "Label",
                :account_id => @account.id),
      stub_model(FeeSchedule,
                :label => "Label",
                :account_id => @account.id)
    ])
  end

  it "renders a list of fee_schedules" do
    render
    assert_select "tr>td", :text => "Label".to_s, :count => 2
  end
end
