require 'spec_helper'

describe "fee_schedules/new.html.haml" do
  before(:each) do
    assign(:fee_schedule, stub_model(FeeSchedule).as_new_record)
  end

  it "renders new fee_schedule form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => fee_schedules_path, :method => "post" do
    end
  end
end
