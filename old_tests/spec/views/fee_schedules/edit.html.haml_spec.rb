require 'spec_helper'

describe "fee_schedules/edit.html.haml" do
  before(:each) do
    @fee_schedule = assign(:fee_schedule, stub_model(FeeSchedule))
  end

  it "renders the edit fee_schedule form" do
    pending "failing in the _switcher partial which is working fine"
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => fee_schedules_path(@fee_schedule), :method => "post" do
    end
  end
end
