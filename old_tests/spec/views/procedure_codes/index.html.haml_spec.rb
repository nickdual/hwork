require 'spec_helper'

describe "procedure_codes/index.html.haml" do
  before(:each) do
    assign(:procedure_codes, [
      stub_model(ProcedureCode,
        :name => "Name",
        :description => "Description",
        :type_code => 1,
        :service_type_code => 1,
        :modifier => "Modifier",
        :tax_rate_percentage => 1,
        :modifier2 => "Modifier2",
        :modifier3 => "Modifier3",
        :cpt_code => "Cpt Code",
        :account_id => 1
      ),
      stub_model(ProcedureCode,
        :name => "Name",
        :description => "Description",
        :type_code => 1,
        :service_type_code => 1,
        :modifier => "Modifier",
        :tax_rate_percentage => 1,
        :modifier2 => "Modifier2",
        :modifier3 => "Modifier3",
        :cpt_code => "Cpt Code",
        :account_id => 1
      )
    ])
  end

  it "renders a list of procedure_codes" do
    pending "move view tests to Cucumber"
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Modifier".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Modifier2".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Modifier3".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Cpt Code".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
