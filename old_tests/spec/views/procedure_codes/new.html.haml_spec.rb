require 'spec_helper'

describe "procedure_codes/new.html.haml" do
  before(:each) do
    assign(:procedure_code, stub_model(ProcedureCode,
      :name => "MyString",
      :description => "MyString",
      :type_code => 1,
      :service_type_code => 1,
      :modifier => "MyString",
      :tax_rate_percentage => 1,
      :modifier2 => "MyString",
      :modifier3 => "MyString",
      :cpt_code => "MyString",
      :account_id => 1
    ).as_new_record)
  end

  it "renders new procedure_code form" do
    pending "move view tests to Cucumber"
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => procedure_codes_path, :method => "post" do
      assert_select "input#procedure_code_name", :name => "procedure_code[name]"
      assert_select "input#procedure_code_description", :name => "procedure_code[description]"
      assert_select "input#procedure_code_type_code", :name => "procedure_code[type_code]"
      assert_select "input#procedure_code_service_type_code", :name => "procedure_code[service_type_code]"
      assert_select "input#procedure_code_modifier", :name => "procedure_code[modifier]"
      assert_select "input#procedure_code_tax_rate_percentage", :name => "procedure_code[tax_rate_percentage]"
      assert_select "input#procedure_code_modifier2", :name => "procedure_code[modifier2]"
      assert_select "input#procedure_code_modifier3", :name => "procedure_code[modifier3]"
      assert_select "input#procedure_code_cpt_code", :name => "procedure_code[cpt_code]"
      assert_select "input#procedure_code_account_id", :name => "procedure_code[account_id]"
    end
  end
end
