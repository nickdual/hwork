require 'spec_helper'

describe "procedure_codes/show.html.haml" do
  before(:each) do
    @procedure_code = assign(:procedure_code, stub_model(ProcedureCode,
      :name => "Name",
      :description => "Description",
      :type_code => 1,
      :service_type_code => 1,
      :modifier => "Modifier",
      :tax_rate_percentage => 1,
      :modifier2 => "Modifier2",
      :modifier3 => "Modifier3",
      :cpt_code => "Cpt Code",
      :account_id => 1
    ))
  end

  it "renders attributes in <p>" do
    pending "move view tests to Cucumber"
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Description/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Modifier/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Modifier2/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Modifier3/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Cpt Code/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
  end
end
