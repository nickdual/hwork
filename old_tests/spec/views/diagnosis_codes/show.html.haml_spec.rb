require 'spec_helper'

describe "diagnosis_codes/show.html.haml" do
  before(:each) do
    @diagnosis_code = assign(:diagnosis_code, stub_model(DiagnosisCode,
      :name => "Name",
      :code => "Code",
      :description => "Description",
      :account_id => 1
    ))
  end

  it "renders attributes in <p>" do
    pending "move view tests to Cucumber"
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Code/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Description/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
  end
end
