require 'spec_helper'

describe "diagnosis_codes/index.html.haml" do
  before(:each) do
    assign(:diagnosis_codes, [
      stub_model(DiagnosisCode,
        :name => "Name",
        :code => "Code",
        :description => "Description",
        :account_id => 1
      ),
      stub_model(DiagnosisCode,
        :name => "Name",
        :code => "Code",
        :description => "Description",
        :account_id => 1
      )
    ])
  end

  it "renders a list of diagnosis_codes" do
    pending "move view tests to Cucumber"
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Code".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
