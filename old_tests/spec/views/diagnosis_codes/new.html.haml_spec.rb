require 'spec_helper'

describe "diagnosis_codes/new.html.haml" do
  before(:each) do
    assign(:diagnosis_code, stub_model(DiagnosisCode,
      :name => "MyString",
      :code => "MyString",
      :description => "MyString",
      :account_id => 1
    ).as_new_record)
  end

  it "renders new diagnosis_code form" do
    pending "move view tests to Cucumber"
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => diagnosis_codes_path, :method => "post" do
      assert_select "input#diagnosis_code_name", :name => "diagnosis_code[name]"
      assert_select "input#diagnosis_code_code", :name => "diagnosis_code[code]"
      assert_select "input#diagnosis_code_description", :name => "diagnosis_code[description]"
      assert_select "input#diagnosis_code_account_id", :name => "diagnosis_code[account_id]"
    end
  end
end
