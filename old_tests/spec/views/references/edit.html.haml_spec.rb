require 'spec_helper'

describe "references/edit.html.haml" do
  before(:each) do
    @reference = assign(:reference, stub_model(Reference,
      :upin => "MyString",
      :npi => "MyString",
      :comment => "MyText"
    ))
  end

  it "renders the edit reference form" do
    pending "implement view tests for reference resource #{__FILE__}"
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => references_path(@reference), :method => "post" do
      assert_select "input#reference_upin", :name => "reference[upin]"
      assert_select "input#reference_npi", :name => "reference[npi]"
      assert_select "textarea#reference_comment", :name => "reference[comment]"
    end
  end
end
