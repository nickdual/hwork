require 'spec_helper'

describe "references/show.html.haml" do
  before(:each) do
    @reference = assign(:reference, stub_model(Reference,
      :upin => "Upin",
      :npi => "Npi",
      :comment => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    pending "implement view tests for reference resource #{__FILE__}"
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Upin/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Npi/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/MyText/)
  end
end
