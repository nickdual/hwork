require 'spec_helper'

describe "references/index.html.haml" do
  before(:each) do
    assign(:references, [
      stub_model(Reference,
        :upin => "Upin",
        :npi => "Npi",
        :comment => "MyText"
      ),
      stub_model(Reference,
        :upin => "Upin",
        :npi => "Npi",
        :comment => "MyText"
      )
    ])
  end

  it "renders a list of references" do
    pending "implement view tests for reference resource #{__FILE__}"
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Upin".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Npi".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
