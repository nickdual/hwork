require 'spec_helper'

describe "clinics/index.html.haml" do
  before(:each) do
    c1 = stub_model(Clinic, :physical_address_one_liner => 'hello' )
    c2 = stub_model(Clinic, :physical_address_one_liner => 'hello' )
    assign(:clinics, [
           c1, c2
    ])
  end

  it "renders a list of clinics" do
    pending "not sure if we'll do view testing with RSpec."
    render
  end
end
