require 'spec_helper'

describe "clinics/new.html.haml" do
  before(:each) do
    assign(:clinic, stub_model(Clinic).as_new_record)
  end

  it "renders new clinic form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => clinics_path, :method => "post" do
    end
  end
end
