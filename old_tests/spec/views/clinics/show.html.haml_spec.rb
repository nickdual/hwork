require 'spec_helper'

describe "clinics/show.html.haml" do
  include Devise::TestHelpers

  before(:each) do
    @user = Factory.create(:user)
    sign_in @user
    @clinic = assign(:clinic, stub_model(Clinic))
    @clinic.new_patient_option = assign(:new_patient_option, stub_model(NewPatientOption))
  end

  it "renders attributes in <p>" do
    pending "failing in the _switcher partial"
    render
  end
end
