require 'spec_helper'

describe "clinics/edit.html.haml" do
  include AppHelp
  before(:each) do
    test_sign_in
    @clinic = assign(:clinic, stub_model(Clinic))
  end

  it "renders the edit clinic form" do
    pending "failing in the _switcher partial"
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => clinics_path(@clinic), :method => "post" do
    end
  end
end
