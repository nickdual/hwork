require 'spec_helper'

describe "rooms/new.html.haml" do
  include AppHelp
  before(:each) do
    assign(:room, stub_model(Room,
      :account_id => 1,
      :name => "MyString"
    ).as_new_record)
    test_sign_in
  end

  it "renders new room form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => rooms_path, :method => "post" do
      assert_select "input#room_account_id", :name => "room[account_id]"
      assert_select "input#room_name", :name => "room[name]"
    end
  end
end
