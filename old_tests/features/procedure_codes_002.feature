Feature: Procedure Codes

  As a Medical Service Provider
  I want to keep track of the services I provide
  And I want to keep track of the fees for those services
  So I need standard procedure codes to be supported

  @javascript
  Scenario: adding a new fee schedule when editing a procedure code
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    Then a procedure_code should exist with name: "PC1"
    When I edit the procedure_code
    And I follow "Add Fee Schedule"
    And I wait for "1" seconds
    And I type "NEW_FS4" within the new fee schedule label
    And I type "1000" within the new fee schedule fee
    And I type "300" within the new fee schedule copay
    When I press "procedure_code_submit"
    Then I should see "Procedure code was successfully updated"
    And fee_schedule should exist with label: "NEW_FS4"
    And fee_schedule should be one of procedure_code's fee_schedules
