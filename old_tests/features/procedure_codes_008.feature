Feature: Procedure Codes

  As a Medical Service Provider
  I want to keep track of the services I provide
  And I want to keep track of the fees for those services
  So I need standard procedure codes to be supported


  @javascript
  Scenario: admin can delete a fee schedule when updating a procedure code
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    Then a procedure_code should exist with name: "PC1"
    When I edit the procedure_code
    And I wait for "2" seconds
    Then I should see css=".delete_fee_schedule"
    And I should see xpath="//td/input[@value='FS1']"
    When within the "FS1" fees row, I follow "Delete complete fee schedule" and confirm popup
    Then I should see "Fee schedule has been successfully destroyed"
    When I fill in "Description" with "A new description"
    And I press "procedure_code_submit" 
    Then I should see "Procedure code was successfully updated"

