Feature: Insurance Carriers

  As a healthcare professional
  I want to conduct business with insurance companies
  So that I can get paid for my services through the healthcare system we have today
 

  @javascript
  Scenario: you can add a new provider legacy id label
    Given I am going to create a new carrier
    And I wait for "2" seconds
    Then I should see "Legacy ID Label"
    When I click ".combo_fields_link" within the carrier legacy id label
    And I type "NEW_LIL7" within the carrier legacy id label
    And I press "third_party_submit"
    Then insurance_carrier should exist with alias_name: "Test A Plan"
    And legacy_id_label should exist with label: "NEW_LIL7"
    And legacy_id_label should be insurance_carrier's legacy_id_label
