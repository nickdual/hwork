Feature: user interface efficiency

  As a user of Handyworks
  I want to enjoy using the Handyworks application
  And work in Handyworks Efficiently
  So that I will continue to user this application and recommend it to others.

  Scenario: primary navigation bar
    Given I am a Handyworks user
    And I have the role "AccountAdmin"
    When I login to Handyworks
    Then I should see "Patients"
    And I should see "Cases"
    And I should see "Appointments"
    And I should see "Transactions"
    And I should see "Reports"
    And I should see "Billing"
    And I should see "Admin"
    And I should see "Clinic"

  Scenario: flash messages are styled with theme classes
    Given I am a Handyworks user
    And I have the role "AccountAdmin"
    When I go to Providers
    Then I should see "You need to sign in or sign up before continuing"
    And I should see css="#flash div.flash.ui-state-error"
    And I should see css="#flash span.ui-icon-alert"
    When I login to Handyworks
    Then I should see "Signed in successfully"
    And I should see css="#flash div.flash.ui-state-highlight"
    And I should see css="#flash span.ui-icon-info"
