Feature: Procedure Codes

  As a Medical Service Provider
  I want to keep track of the services I provide
  And I want to keep track of the fees for those services
  So I need standard procedure codes to be supported

  Scenario: admin can create new procedure codes
    Given I am going to create a new procedure code
    When I press "procedure_code_submit"
    Then I should see "Procedure code was successfully created"

  Scenario: ONLY admin can create new procedure codes
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    When I view the list of procedure_codes
    Then I should not see "New Procedure code"

  @pending
  Scenario: from edit screen a user can switch procedure codes

  @pending
  Scenario: index view displays the text form of type code

  @pending
  Scenario: index view displays the text form of service type code

  Scenario: a new procedure code will offer all fee schedules for the account
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    Then I should see "Clinic1"
    When I create a new procedure_code
    Then the "procedure_code_fees_attributes_0_fee_schedule_attributes_label" field should contain "FS1"
    And the "procedure_code_fees_attributes_1_fee_schedule_attributes_label" field should contain "FS2"
    And I should not see "FS3"

  @pending
  Scenario: when editing a procedure code, expected insurance is updated automatically

  @pending
  Scenario: when editing a procedure code, expected insurance is updated automatically by percentage

  @pending
  Scenario: when editing a procedure code, new fee schedules are available


  Scenario: no option to add fee schedule when viewing a procedure code
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    When I view the list of procedure_codes
    When I follow "Show" within ".procedure_code_record"
    Then I should not see "Add Fee Schedule"


  Scenario: remove fee schedule from while creating a procedure code

  @pending
  Scenario: editing fee schedule fields with negative expected insurance payment

  @pending
  Scenario: only billable procedure codes, service type codes, should have fees

  @pending 
  Scenario: repeat tests for fee schedules on procedure code edit view

  @pending
  Scenario: edit view allows use of resource switcher

  @pending
  Scenario: switcher only displays resources by clinic

  @pending
  Scenario: switcher default selected value matches current view

  @pending
  Scenario: unused procedure code can be switched to any other type

  @pending
  Scenario: used service type procedure codes can ONLY change between billable services

  @pending
  Scenario: used payment type procedure codes can ONLY change between payment services

  @pending
  Scenario: used adjustment type procedure codes can ONLY change between adjustment services

  @pending @javascript
  Scenario: when creating new procedure code, fees only display for billable codes
    # waiting on patient_visits model to be completed

  @pending
  Scenario: when changing an unused code from billable to non, fees are deleted.
    # waiting on patient_visits model to be completed

  Scenario: editing a fee schedule when creating a procedure code
    Given I am going to create a new procedure code
    And fee_schedule should exist with label: "FS1"
    When I fill in "procedure_code_fees_attributes_0_fee_schedule_attributes_label" with "UPD_FS1"
    And I press "procedure_code_submit"
    Then I should see "Procedure code was successfully created"
    And the fee_schedule's label should be "UPD_FS1"
    And procedure_code should exist with name: "NEW_PC4"
    And fee_schedule should be one of procedure_code's fee_schedules

  Scenario: editing a fee schedule when updating a procedure code
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    Then fee_schedule should exist with label: "FS1"
    And procedure_code should exist with name: "PC1"
    When I edit the procedure_code
    And I wait for "2" seconds
    And I fill in "procedure_code_fees_attributes_0_fee_schedule_attributes_label" with "UPD_FS1"
    And I press "procedure_code_submit"
    Then I should see "Procedure code was successfully updated"
    And the fee_schedule's label should be "UPD_FS1"
    And fee_schedule should be one of procedure_code's fee_schedules

  @pending
  Scenario: after updating a procedure code you return to the procedure codes list
