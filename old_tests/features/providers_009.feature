Feature: Provider

  As a healthcare provider
  I want to store my information in Handyworks
  So that I can efficiently render services to my clients


  @javascript
  Scenario: admin can delete a legacy id label when updating a provider
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Providers
    And I follow "Edit" within the "Bob Robson MD" provider record
    And I wait for "2" seconds
    Then I should see css=".delete_legacy_id_label"
    And I should see xpath="//td/input[@value='LIL1']"
    When within the "LIL1" legacy ids row, I follow "Delete complete legacy id label" and confirm popup
    Then I should see "Legacy ids have been successfully destroyed"
    When I fill in "Last name" with "A new last name"
    And I press "provider_submit" 
    Then I should see "Provider was successfully updated"
