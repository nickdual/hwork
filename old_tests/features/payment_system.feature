Feature: Integration with Braintree Payments

  As a manager of a SaaS application
  I want to support automated recurring billing
  So that I can collect the system subscription fees in a timely manner

  Scenario: After registering and confirming a new account, the account is assigned a payment_system_id
    Given I register for account "westy"
    When I follow "Confirm my account" in the email
    And I wait for "5" seconds
    Then account "westy" should have :payment_system_id

  Scenario: Each account will require a credit card to be added after the first login
    Given I register and confirm a new account "westy" and then sign on
    Then I should see "You need to setup a payment method"

  Scenario: only "owner" will be required to enter credit card information
    Given I register for account "westy"
    When I follow "Confirm my account" in the email
    And I do not have the role "AccountAdmin"
    And I login to Handyworks with :login = "tom" and :password = "AltaIalfa8"
    Then I should not see "You need to setup a payment method"

  Scenario: payment methods can be accessed through Account Preferences
    Given I register and confirm a new account "westy" and then sign on
    When I follow "links.account_preferences"
    Then I should see "Payment Methods"
    When I follow "Payment Methods"
    Then I should see "New Payment Method"

  Scenario: system displays a secure payments logo on new credit card page
    Given I register and confirm a new account "westy" and then sign on
    Then I should see "New Payment Method"
    And I should see css="#braintree_logo"

  Scenario: system displays a secure payments logo on edit credit card page
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I follow "links.account_preferences"
    Then I should see "Payment Methods"
    When I follow "Payment Methods"
    Then I should see "Editing Payment method"
    And I should see css="#braintree_logo"
    And I should see css="#credit_card_type"

  @pending
  Scenario: cannot create a payment method if you don't have a valid payment system account

  Scenario: United States is the default country selected for credit card billing address
    Given I register and confirm a new account "westy" and then sign on
    Then I should see "New Payment Method"
    And "United States of America" should be selected for "Country"

  Scenario: current month is the default selected expiration month
    Given I register and confirm a new account "westy" and then sign on
    Then I should see "New Payment Method"
    And the current month should be selected for "credit_card_expiration_month"

  Scenario: current year is the default selected exipiration year
    Given I register and confirm a new account "westy" and then sign on
    Then I should see "New Payment Method"
    And the current year should be selected for "credit_card_expiration_year"

  Scenario: extended address is visible
    Given I register and confirm a new account "westy" and then sign on
    Then I should see "New Payment Method"
    Then I should see css="#credit_card_billing_address_extended_address"

  @pending
  Scenario: on show/edit, the correct month is selected for expiration

  @pending
  Scenario: on show/edit, the correct year is selected for expiration

  @pending @security
  Scenario: the credit card entry form has autocomplete disabled (number and cvv)

  @pending
  Scenario: when submitting a credit card form with errors, most fields repopulate

  @pending
  Scenario: errors are reported back to the user when submitting a  credit card fails.

  @pending @security
  Scenario: after error submitting credit card, card number is not populated

  @pending 
  Scenario: admin can delete credit card information 

  @pending
  Scenario: cvv verification required when creating credit card
    # use cvv 200 ( or see http://www.braintreepayments.com/docs/ruby/reference/sandbox for
    # test failure codes ).

  @pending
  Scenario: cvv verification required when updating credit card

  @pending
  Scenario: you can change a credit card number and keep the same payment token

        #Create Payment Account Success
        #Create Payment Account Failure
        #Delete Payment Account Success (maybe)
        #Delete Payment Account Failure (maybe)
        #Delete Payment Account Failure - Not Found (maybe)
        #Create Payment Method Success
        #Create Payment Method Failure -- e.g. invalid customer_id 
