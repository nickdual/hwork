Feature: Provider

  As a healthcare provider
  I want to store my information in Handyworks
  So that I can efficiently render services to my clients

  @javascript
  Scenario: edit view allows use of resource switcher
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Providers
    And I follow "Edit" within the "Bob Robson MD" provider record
    And I wait for "8" seconds
    Then I should see "Switch Provider" within the data switcher
    And I should see "Editing Provider Bob Robson MD"
    When I pick "Rob Bobson MD" within the data switcher
    And I wait for "8" seconds
    Then I should see "Editing Provider Rob Bobson MD"

