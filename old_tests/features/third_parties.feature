Feature: Third Parties

  As a provider of medical services
  I want to keep track of common 3rd party payers (Insurance Carriers and Attorneys)
  So that I can efficiently process, track and report on their cases

  @pending
  Scenario: switcher only displays resources by account

  @pending
  Scenario: switcher default selected value matches current view

  @pending
  Scenario: convert an attorney into a carrier

  @pending
  Scenario: convert a carrier into an attorney

  @pending
  Scenario: convert between carrier types

  @pending
  Scenario: third party form has an attention field

  @pending
  Scenario: fragment caching expiration for third parties
    # verify when adding, editing or deleting a third party
    # the cached fragments for the index and switcher are
    # expired.
  @pending
  Scenario: after updating a third party you return to the third parties list
