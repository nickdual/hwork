Feature: user registers for an account

  As a targeted medical service provider
  I want to register to use Handyworks
  And select my subscription plan
  So that I can setup my clinic and support data.

  As an associate of a Handyworks provider
  I want to register to use Handyworks
  So that I can work with Handyworks in the providers clinics.

  As a patient of a Handyworks provider
  I want to register to use Handyworks
  So that I can schedule or review my appointments.

  Scenario: anonymous user can sign up 
    Given I am not signed in to Handyworks
    When I go to the home page
    Then I should see "links.register"

  Scenario: signup creating a new account
    Given I am not signed in to Handyworks
    And all emails have been delivered
    And I am on the home page
    When I follow "links.register"
    Then I should see "titles.subscription_plans"

    When I follow "links.select_plan" 
    Then I should see "Account name"
    And I should see "Email"
    
    When I fill in the following:
      | Account name          | testy           |
      | Login                 | tom             |
      | Email                 | tom@example.com |
      | Password              | AltaIalfa8      |
      | Password confirmation | AltaIalfa8      |
    And I press "buttons.sign_up"
    Then I should see "flash.notice.account_created"
    And 1 email should be delivered to "tom@example.com"
    And the email should contain "mailer.titles.confirmation"

    When I login to Handyworks with :login = "tom" and :password = "AltaIalfa8"
    Then I should see "devise.failure.unconfirmed"

    When I follow "Confirm my account" in the email
    Then I should see "devise.confirmations.confirmed"

    Given I am not signed in to Handyworks
    When I login to Handyworks with :login = "tom" and :password = "AltaIalfa8"
    Then I should see "devise.sessions.signed_in"

  @pending
  Scenario: passwords must be secure
    # TODO add tests for various password validation conditions

  Scenario: signup creating a new account with plan 
    Given I register for account "zesty"

  Scenario: signup with duplicate login name
    Given a user exists with login: "tom", email: "tom@example.come"
    And I am not signed in to Handyworks
    When I register for a Handyworks "Basic" account with the following:
      | Account name          | zesty           |
      | Login                 | tom             |
      | Email                 | tom@example.com |
      | Password              | AltaIalfa8      |
      | Password confirmation | AltaIalfa8      |
    Then I should see "login has already been taken"

  Scenario: bug #11 confirmation email external link
    Given I register for account "festy"
    Then the email should not contain "localhost:3000"

  Scenario: bug #12 using the confirmation link multiple times
    Given I register for account "pesty"

    When I follow "Confirm my account" in the email
    Then I should see "devise.confirmations.confirmed"
    # following the link a 2nd time
    When I follow "Confirm my account" in the email
    And I should see "Confirmation token is invalid"
    And I should see "Resend confirmation instructions"

  Scenario: after confirmation user should be at the login screen
    Given I register for account "besty"
    When I follow "Confirm my account" in the email
    Then I should see "Sign in"
    And I should see css="input#user_login"
    And I should see css="input#user_password"
    #TODO : prepoluate field with user --> And the "user_login" field should contain "tom"


    # TODO should show error message on form
    # BLS : these should probably go in RSpec tests.
    # TODO test allow duplicate email addresses
    # TODO test assigns account admin role 
    # TODO test users.account = account after registration
    # TODO test users.account.subscription.subscription_plan_id = correct thing.

  @pending
  Scenario: registration using resque mailer sends the desired email
    # ensure there's no problem with processing the resque job on registration
    # what was specifically broken was the :id attribute was not being recorded
    # for the user who registered.
