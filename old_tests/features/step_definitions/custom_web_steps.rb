#-------------------------------------------------------------------------------
# BLS - added selection by xpath for table of fields with dynamic ids / labels
#       relies on the use of within wrapper to get to the right select field
#       field is Capybara::Element (see :http://rubydoc.info/gems/capybara/0.4.0/Capybara/Element)
#-------------------------------------------------------------------------------
module KeyPressHelper
  def key_press(key, field=nil)
    field ||= first(:css, "*")
    field.native.send_keys(key.to_sym)
  end
end
World(KeyPressHelper)

# Single-line step scoper
#When /^(.*) and press key ([^:]+)$/ do |step, key|
#  When step
#  key_press(key)
#end

When /^(?:|I )pick "([^"]*)"$/ do |value|
  field = first(:css, "select")
  #field.select(value)
  field.select(value)
end

When /^(?:|I )type "([^"]*)"$/ do |value|
  field = first(:css, "input[type=text]")
  field.set(value)
  #field.native.send_keys(:tab)
end

When /^(?:|I )click "([^"]*)"$/ do |matcher|
  field = find(:css, matcher)
  field.click
end

# currently only support the text, not textarea based on the hard-coded css selector
Then /^check the field(?: in (.*))? should contain "([^"]*)"$/ do |parent, value|
  with_scope(parent) do
    field = first(:css, "input[type=text]")
    field_value = (field.tag_name == 'textarea') ? field.text : field.value
    if field_value.respond_to? :should
      field_value.should =~ /#{value}/
    else
      assert_match(/#{value}/, field_value)
    end
  end
end

# currently only support the text, not textarea based on the hard-coded css selector
Then /^check the field(?: in (.*))? should not contain "([^"]*)"$/ do |parent, value|
  with_scope(parent) do
    field = first(:css, "input[type=text]")
    field_value = (field.tag_name == 'textarea') ? field.text : field.value
    if field_value.respond_to? :should_not
      field_value.should_not =~ /#{value}/
    else
      assert_no_match(/#{value}/, field_value)
    end
  end
end

Then /^(?:|I )should see (css|xpath)="([^"]*)"$/ do |style, text|
  text = check_translation(text)
  if page.respond_to? :should
    if style == "css"
      page.should have_css(text)
    else
      page.should have_xpath(text)
    end
  else
    if style == "css"
      assert page.has_css?(text)
    else
      assert page.has_xpath?(text)
    end
  end
end

Then /^(?:|I )should not see (css|xpath)="([^"]*)"$/ do |style, text|
  text = check_translation(text)
  if page.respond_to? :should
    if style == "css"
      page.should_not have_css(text)
    else
      page.should_not have_xpath(text)
    end
  else
    if style == "css"
      assert page.has_no_css?(text)
    else
      assert page.has_no_xpath?(text)
    end
  end
end

Then /^I should find all of \[([^\]]*)\]$/ do |list_o_stuff|
  list_o_stuff.split(',').each do |text|
    sleep 1
    text = check_translation(text)
    puts "finding #{text}"
    assert page.has_content?(text)
  end
end

# destroy a model
Then(/^#{capture_model} is destroyed(?: with #{capture_fields})?$/) do |name, fields|
  find_model!(name, fields).destroy
end

#-------------------------------------------------------------------------------
# for stressing some client side "toggling" views
#-------------------------------------------------------------------------------
When /^I toggle between "([^"]*)" and "([^"]*)" for "([^"]*)" times$/ do |link1, link2, iterations|
  for i in (1..iterations.to_i) do
    When %Q{I follow "#{link1}"}
    When %Q{I follow "#{link2}"}
  end
end


#-------------------------------------------------------------------------------
#  The following work fine in capybara internal driver, but fail in selenium
#-------------------------------------------------------------------------------
#  #-------------------------------------------------------------------------------
#  # http://www.agilereasoning.com/2009/05/31/verifying-select-field-value-with-cucumber-and-webrat/
#  #  -- capybara : http://groups.google.com/group/ruby-capybara/msg/009fd3eda0542c8f
#  #-------------------------------------------------------------------------------
#  Then /^"([^"]*)" should be selected for "([^"]*)"$/ do |value, field|
  #  # webrat version field_labeled(field).element.search(".//option[@selected = 'selected']").inner_html.should =~ /#{value}/
  #  field_labeled(field).native.xpath(".//option[@selected = 'selected']").inner_html.should =~ /#{value}/
#  end

#-------------------------------------------------------------------------------
# Select Fields and timing
#     http://tesoriere.com/tags/capybara/
#
#  2011-09-20 : I updated these with better options which are now available.
#-------------------------------------------------------------------------------
Then /^"([^"]*)" should be selected for "([^"]*)"$/ do |value, field|
  #assert page.has_xpath?("//option[@selected = 'selected' and contains(string(), '#{value}')]") 
  assert page.has_select?(field, :selected => value)
end

Then /^"([^"]*)" should be seen within "([^"]*)"$/ do |value, field|
  #assert page.has_xpath?("//option[contains(string(), '#{value}')]") 
  assert page.has_select?(field, :options => [value])
end

When /^I wait until I can see "([^"]*)"$/ do |selector|
  page.has_css?("#{selector}", :visible => true)
end

#-------------------------------------------------------------------------------
# http://stackoverflow.com/questions/2458632/how-to-test-a-confirm-dialog-with-cucumber
# http://www.jarra.nl/news/show/43
#-------------------------------------------------------------------------------
  When /^(.*) and (\S*) popup(?: containing "([^"]*)")?$/ do |step,confirm_dismiss,text|
    handle_js_confirm(confirm_dismiss == "confirm") do
      When step
      if text
        _text = get_confirm_text
        puts "FIXME : the clause containing \"#{text}\" is not working"
        #if _text.respond_to? :should
        #  _text.should =~ /#{text}/
        #else
        #  assert_match(/#{text}/,_text)
        #end
      end
    end
  end

# the following slightly modified within step is required to integrate with the popup
# step. it will cause the within portion to be lower priority and thus be wrapped inside
# the popup step. The idea for this custom within step came from:
#  http://stackoverflow.com/questions/6302653/cucumber-step-ambiguity-when-using-built-in-within-step-scoper
Then /^within ([^,]*), ((?:(?!popup).)+)$/ do |parent, step|
  with_scope(parent) { When step }
end

#-------------------------------------------------------------------------------
# key press support ... mostly custom
#-------------------------------------------------------------------------------
When /^I press tab$/ do
  field = first(:css, "input")
  field.native.send_keys(:tab)
end

#-------------------------------------------------------------------------------
# http://jjinux.blogspot.com/2009/08/rails-dynamic-404s-authlogic-cucumber.html
#-------------------------------------------------------------------------------
Then /^I should get a "(\d+) ([^"]+)" response$/ do |http_status, message|
  assert_response_status(http_status, message)
end

Given /^I am simulating a remote request$/ do
  header "REMOTE-ADDR", "10.0.1.1"
end

When /^I wait for "([^"]*)" seconds$/ do |time|
  sleep time.to_i
end

When /^DEBUG "([^"]*)"$/ do |command|
  puts "debug: #{eval command}"
end

#-------------------------------------------------------------------------------
# Integrates with Picklet steps to capture a model based on a previously 
# captured model's association (e.g. has_one or belongs_to).
#-------------------------------------------------------------------------------
#Then(/^#{capture_model}(?:'s)? #{capture_model} should exist$/) do |owner, association|
#  record = model!(owner).send(association)
#  record.should_not be_nil
#  factory, label = *parse_model(association)
#  store_model(factory,label,record)
#end
