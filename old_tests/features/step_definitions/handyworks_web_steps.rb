
Given /^I am not signed in to Handyworks$/ do
  visit path_to('the logout page')
end

Given /^I[\'| a]m ready to test$/ do
  # nothing
end

#-------------------------------------------------------------------------------
# These step definitions related to resque need some work to be meaningful
#  since I'm executing the resque jobs synchronously in the test execution,
#  it's not critical at the moment.
# TODO : tie these steps to the resque functionality to be accurrate 
#-------------------------------------------------------------------------------
Then /^I should have a resque job in "([^\"]*)"$/ do |queue_name|
  puts "TODO : tie to resque"
end
When /^I wait for my resque jobs to complete$/ do
  puts "TODO : tie to resque"
end
#-------------------------------------------------------------------------------
# This step will setup a fairly complete user to exercise test scenarios
#  primarily relies on Factory Girl to build the necessary objects / 
#  associations.
#-------------------------------------------------------------------------------
Given /^I am a Handyworks user$/ do
  user
end

Given /^I am signed in to Handyworks$/ do
  Given "I am a Handyworks user"
  And "I login to Handyworks"
end


When /^I login to Handyworks$/ do
  login
end

When /^I login to Handyworks with :login = "([^"]*)" and :password = "([^"]*)"$/ do |login, password|
  login(login,password)
end

When /^I register for a Handyworks "([^"]*)" account with the following:$/ do |plan, registration_table|
  plan_link = "#{plan.underscore}_plan"
  Given "I am not signed in to Handyworks"
  Given "all emails have been delivered"
  Given "I am on the home page"
  When 'I follow "links.register"'
  When "I follow \"#{plan_link}\""
  When "I fill in the following:", registration_table
  When 'I press "buttons.sign_up"'
end

Given /^I register for account "([^"]*)"$/ do |account_name|
  steps %Q{
  When I register for a Handyworks "Basic" account with the following:
    | Account name          | #{account_name} |
    | Login                 | tom             |
    | Email                 | tom@example.com |
    | Password              | AltaIalfa8      |
    | Password confirmation | AltaIalfa8      |
  }
  Then %Q{I should see "flash.notice.account_created"}
  Then %Q{1 email should be delivered to "tom@example.com"}
  Then %Q{the email should contain "mailer.titles.confirmation"}
  @user = User.find_by_login('tom')
end

Given /^I register and confirm a new account "([^"]*)" and then sign on$/ do |account_name|
  Given %Q{I register for account "#{account_name}"}
  #When %Q{I follow "Confirm my account" in the email} # not working for Selenium tests currently
  @user = User.confirm_by_token(@user.confirmation_token)
  @user.account.setup(@user)
  And %Q{I login to Handyworks with :login = "tom" and :password = "AltaIalfa8"}
end

When /^I add a new legacy_id_label "([^"]*)" to the current account$/ do |new_label|
  Factory.create(:legacy_id_label, :account => @account, :label => new_label)
end

Then /^the current month should be selected for "([^"]*)"$/ do |field_name|
  current_month = Time.now.strftime("%B")
  Then %Q{"#{current_month}" should be selected for "#{field_name}"}
end

Then /^the current year should be selected for "([^"]*)"$/ do |field_name|
  current_year = Time.now.year
  Then %Q{"#{current_year}" should be selected for "#{field_name}"}
end

Given /^I have the role "([^"]*)"$/ do |role|
  @user.add_role(role)
end

Given /^I do not have the role "([^"]*)"$/ do |role|
  @user.remove_role(role)
  @user.save
end

Given /^I am a Handyworks user with the "([^"]*)" role$/ do |role|
  Given "I am a Handyworks user"
  Given "I have the role \"#{role}\""
end

Given /^an account with clinic exists with name "([^"]*)"$/ do |clinic|
  account_with_clinic(clinic)
end

Given /^this account has a clinic with name "([^"]*)"$/ do |clinic|
  clinic =  Factory(:clinic, :account => @account)
  clinic.name = clinic
end

Then /^the "([^"]*)" insurance carrier should not have a carrier_hcfa_form_option$/ do |value|
  carrier = ThirdParty.find_by_alias_name(value)
  carrier.carrier_hcfa_form_option.should be_nil
end

#-------------------------------------------------------------------------------
# Sets a user theme preference for the indicated clinic
#-------------------------------------------------------------------------------
Given /^"([^"]+)" sets "([^"]+)" as my theme for clinic "([^"]+)"$/ do |user, theme, clinic|
  c = Clinic.find_by_name(clinic)
  u = User.find_by_login(user)
  u.themes.create!(:name => theme, :clinic_id => c.id)
end
#-------------------------------------------------------------------------------
# From Devise Tests with Cucumber information.
#   - modified to use the paths helper
#-------------------------------------------------------------------------------
Then /^I am redirected to "([^\"]*)"$/ do |page_name|
  assert [301, 302].include?(@integration_session.status), "Expected status to be 301 or 302, got #{@integration_session.status}"
  location = @integration_session.headers["Location"]
  assert_equal path_to(url), location
  visit location
end

#-------------------------------------------------------------------------------
# Schedule Creation Steps
#-------------------------------------------------------------------------------
Then /^room "([^\"]*)" should have a schedule$/ do |name|
  r = Room.find_by_name(name)
  r.should_not be_nil
  r.schedule.should_not be_nil
end

Then /^provider "([^\"]*)" should have a schedule$/ do |name|
  r = Provider.find_by_signature_name(name)
  r.should_not be_nil
  r.schedule.should_not be_nil
end

When /^I open the schedule for room "([^\"]*)"$/ do |record_name|
  When "I go to Rooms"
  sleep 2
  When %Q{I follow "Schedule" within the "#{record_name}" room record}
end
#-------------------------------------------------------------------------------
# Payment System Test Steps
#-------------------------------------------------------------------------------
Then /^account "([^\"]+)" should have :(.+)$/ do |a_name, field|
  a = Account.find_by_name(a_name)
  a.should_not be_nil
  v = eval "a.#{field}"
  v.should_not be_nil
end

Then /^account "([^\"]+)" should have (\d+) :(.+)$/ do |a_name, count, field|
  a = Account.find_by_name(a_name)
  a.should_not be_nil
  v = eval "a.#{field}"
  v.should_not be_nil
  v.size.should eql(count.to_i)
end
#-------------------------------------------------------------------------------
# A basic set of Handyworks Data
#   payment method ( customer_id = 'ACCOUNT1' and credit_card.token = 'ACCOUNT1_CC'
#   need to be setup in Braintree sandbox.
#-------------------------------------------------------------------------------
Given /^a basic set of Handyworks data$/ do
  @account = Factory.create(:account, :name => "Account1", :payment_system_id => 'ACCOUNT1')
  @payment_method = Factory.create(:account_payment_method, :payer_id => @account.id, :method_payment_id => 'ACCOUNT1_CC')
  @account2 = Factory.create(:account, :name => "Account2")
  @clinic1 = Factory.create(:clinic, :account => @account)
  @clinic2 = Factory.create(:clinic, :account => @account)
  @clinic3 = Factory.create(:clinic, :account => @account2)
  @clinic1.contact.company_name = "Clinic1"
  @clinic2.contact.company_name = "Clinic2"
  @clinic3.contact.company_name = "Clinic3"
  @clinic1.save! # necessary
  @clinic2.save! # necessary
  @clinic3.save! # necessary
  @appointment_type1 = Factory.create(:appointment_type, :name => 'OldService1', :serviceable => @account.calendar)
  @appointment_type2 = Factory.create(:appointment_type, :name => 'OldService2', :serviceable => @account2.calendar)
  @room1 = Factory.create(:room, :account => @account, :name => "Room1")
  @room2 = Factory.create(:room, :account => @account, :name => "Room2")
  @room3 = Factory.create(:room, :account => @account2, :name => "Room3")
  @app_admin_user = Factory.create(:user, :account => Account.get_default, :login => 'app_admin.login', :password => 'app_admin.pass')
  Role.find_by_name(Role::APPLICATION_ADMIN).assign(@app_admin_user)
  @admin_user = Factory.create(:user, :account => @account, :login => 'admin.login', :password => 'admin.pass')
  Role.find_by_name(Role::ACCOUNT_ADMIN).assign(@admin_user)
  @staff_user = Factory.create(:user, :account => @account, :login => 'staff.login', :password => 'staff.pass')
  @provider1 = Factory.create(:provider, :account => @account, :signature_name => "Rob Bobson MD")
  @provider2 = Factory.create(:provider, :account => @account, :signature_name => "Bob Robson MD")
  @provider3 = Factory.create(:provider, :account => @account2, :signature_name => "Don Jobson MD")
  @fee_schedule1 = Factory.create(:fee_schedule, :label => "FS1", :account => @account)
  @fee_schedule2 = Factory.create(:fee_schedule, :label => "FS2", :account => @account)
  @fee_schedule3 = Factory.create(:fee_schedule, :label => "FS3", :account => @account2)
  @procedure_code1 = Factory.create(:procedure_code, :name => 'PC1', :account => @account)
  @procedure_code2 = Factory.create(:procedure_code, :name => 'PC2', :account => @account)
  @procedure_code3 = Factory.create(:procedure_code, :name => 'PC3', :account => @account2)
  @fee1 = Factory.create(:fee, :fee_schedule => @fee_schedule1, :procedure_code => @procedure_code1)
  @fee2 = Factory.create(:fee, :fee_schedule => @fee_schedule2, :procedure_code => @procedure_code1)
  @procedure_code1.fees << @fee1
  @procedure_code1.fees << @fee2
  @procedure_code1.save!
  @legacy_id_label1 = Factory.create(:legacy_id_label, :label => "LIL1", :account => @account)
  @legacy_id_label2 = Factory.create(:legacy_id_label, :label => "LIL2", :account => @account)
  @legacy_id_label3 = Factory.create(:legacy_id_label, :label => "LIL3", :account => @account2)
  @carrier1 = Factory.create(:insurance_carrier, :alias_name => "Carrier1", :account => @account)
  @carrier2 = Factory.create(:insurance_carrier, :alias_name => "Carrier2", :account => @account)
  @carrier3 = Factory.create(:insurance_carrier, :alias_name => "Carrier3", :account => @account2)
  @carrier1.contact.company_name = "Carrier1 Company"
  @carrier2.contact.company_name = "Carrier2 Company"
  @carrier3.contact.company_name = "Carrier3 Company"
  @carrier1.save!
  @carrier2.save!
  @carrier3.save!
  @attorney1 = Factory.create(:attorney, :account => @account)
  @attorney2 = Factory.create(:attorney, :account => @account)
  @attorney3 = Factory.create(:attorney, :account => @account2)
  @attorney1.contact.first_name = "Attorney1"
  @attorney1.contact.company_name = "Attorney1"
  @attorney2.contact.first_name = "Attorney2"
  @attorney2.contact.company_name = "Attorney2"
  @attorney3.contact.first_name = "Attorney3"
  @attorney3.contact.company_name = "Attorney3"
  @attorney1.save!
  @attorney2.save!
  @attorney3.save!
end

#-------------------------------------------------------------------------------
# Augmentations of the base fixture for specific purpose
#-------------------------------------------------------------------------------
Given /^a clinic with a different address$/ do
  @clinic4 = Factory.create(:clinic, :account => @account)
  @clinic4.contact.physical_address.street = "201 New Dr."
  @clinic4.save!
end

Given /^provider "([^\"]+)" has appointment type "([^\"]+)"$/ do |provider_name,service_name|
  provider = Provider.find_by_signature_name(provider_name)
  provider.appointment_types<< AppointmentType.new(:name => service_name)
  provider.save!
end

#-------------------------------------------------------------------------------
# custom step for calendar scenarios
#-------------------------------------------------------------------------------
Then /^the day and date today will be displayed$/ do
  # the date
  day = Time.now.strftime('%A')
  date = Time.now.strftime('%b %d, %Y')
  next_day = (Time.now + (24 * 60 * 60)).strftime('%A')
  next_date = (Time.now + (24 * 60 * 60)).strftime('%A')
  prev_day = (Time.now + (24 * 60 * 60)).strftime('%A')
  prev_date = (Time.now + (24 * 60 * 60)).strftime('%A')
  Then %Q{I should see "#{day}"}
  Then %Q{I should see "#{date}"}
  Then %Q{I should not see "#{next_day}"}
  Then %Q{I should not see "#{next_date}"}
  Then %Q{I should not see "#{prev_day}"}
  Then %Q{I should not see "#{prev_date}"}
end

#-------------------------------------------------------------------------------
# Selenium still sees hidden elements, so to check for hidden elements you
# can't use "should not see". This step definition comes from:
#   http://makandra.com/notes/1049-check-that-a-page-element-is-not-visible-with-selenium
# Also referenced
#   http://jhollingworth.com/2011/03/30/take-a-screenshot-with-capybara-selenium/
#-------------------------------------------------------------------------------
Then /^"([^\"]+)" should not be visible$/ do |text|
  paths = [
    "//*[@class='hidden']/*[contains(.,'#{text}')]",
    "//*[@class='invisible']/*[contains(.,'#{text}')]",
    "//*[@style='display: none;']/*[contains(.,'#{text}')]"
  ]
  xpath = paths.join '|'
  page.should have_xpath(xpath)
end

Given /^I am going to create a new room schedule exception for "([^\"]*)"$/ do |room|
    Given %Q{a basic set of Handyworks data}
    When %Q{I login to Handyworks with :login = "admin.login" and :password = "admin.pass"}
    And %Q{I open the schedule for room "#{room}"}
    Then %Q{I should see css="button[title='Add Exception']"}
    When %Q{I click "#add_schedule_exception"}
    Then %Q{I should see "New Schedule Exception"}
end

# setup a new provider form with minimal data
Given /^I am going to create a new provider$/ do
    Given "a basic set of Handyworks data"
    And %Q{I login to Handyworks with :login = "admin.login" and :password = "admin.pass"}
    When "I go to Providers"
    And %Q{I follow "New Provider"}
    And %Q{I select "Chiropractor" from "Provider type code"}
    steps %Q{
    And I fill in the following:
      |Full Name and credentials|Dr. Test Tester|
      |Tax uid                  |111-33-9999    |
      |Notes                    |Good Test      |
      |First name               |Test           |
      |Last name                |Tester         |
      |Email                    |drt@test.com   |
      |Mobile phone             |(333) 444-5555 |
    }
end

Given /^I am going to create a new carrier$/ do
    Given "a basic set of Handyworks data"
    And %Q{I login to Handyworks with :login = "admin.login" and :password = "admin.pass"}
    When "I go to Carriers"
    And %Q{I follow "New 3rd Party"}
    And %Q{I select "PPO" from "Carrier Type"}
    And %Q{I select "Hawaii" from "State"}
    steps %Q{
    And I fill in the following:
      |Carrier / Law Firm Name  |Test A Carrier |
      |Plan name                |Test A Plan    |
      |Notes                    |Good Test      |
      |Address                  |11 Honolulu Dr |
      |City                     |Onesville      |
      |Zip                      |99331          |
      |Email                    |carrier@bc.com |
      |Business phone           |(999) 212-8989 |
    }
end

Given /^I am going to create a new procedure code$/ do
    Given "a basic set of Handyworks data"
    Given %Q{I login to Handyworks with :login = "admin.login" and :password = "admin.pass"}
    When %Q{I create a new procedure_code}
    And %Q{I select "Ins Billable Procedure" from "Type code"}
    And %Q{I select "Chiropractic" from "Service type code"}
    steps %Q{
    And I fill in the following:
      |Name                     |NEW_PC4        |
      |Description              |Test New Code  |
      |Cpt code                 |10013122       |
      |Tax rate percentage      |13             |
    }
end

Given /^I am going to create a new attorney$/ do
    Given "a basic set of Handyworks data"
    And %Q{I login to Handyworks with :login = "admin.login" and :password = "admin.pass"}
    When "I go to Attorneys"
    When %Q{I follow "New 3rd Party"}
    steps %Q{
    And I fill in the following:
      |Carrier / Law Firm Name  |Test And Tester|
      |Notes                    |Good Test      |
      |First name               |Test           |
      |Last name                |Tester         |
      |Address                  |11 One Drive   |
      |City                     |Onesville      |
      |Zip                      |73132          |
      |Email                    |mrt@test.com   |
      |Business phone           |(333) 444-5555 |
    }
end

Given /^I am going to create a new clinic$/ do
    Given "a basic set of Handyworks data"
    And %Q{I login to Handyworks with :login = "admin.login" and :password = "admin.pass"}
    When "I go to Clinics"
    And %Q{I follow "New Clinic"}
    And %Q{I check "use_billing_address"}
    And %Q{I select "Oklahoma" from "clinic_contact_attributes_physical_address_attributes_state"}
    And %Q{I select "Texas" from "clinic_contact_attributes_billing_address_attributes_state"}
    steps %Q{
    And I fill in the following:
      |Clinic Name        |TestClinicD         |
      |Address            |000 Test Drive Rd   |
      |City               |Norman              |
      |Zip                |73033               |
      |Email              |drt@test.com        |
      |Business phone     |(111) 111-2222      |
    }
end

Given /^I am going to create a new room$/ do
    steps %Q{
      Given a basic set of Handyworks data
      And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
      When I go to Rooms
      When I follow "New Room"
      And I fill in the following:
        |Name                  |TestRoomA               |
    }
end
