#-------------------------------------------------------------------------------
# These steps were written to replace the pickle_steps which was generating
# an error related to "regular expression too big"
#-------------------------------------------------------------------------------
Given /^(?:a )?(\S*) should exist with (?:an )?(\S*): "([^\"]*)"$/ do |model, attr, value|
  given_model(model,attr,value)
  puts ("captured model: #{model}, attr: #{attr}, value: #{value}")
end

Then /^the (\S*)'s (\S*) should exist/ do |model, attr|
  check_belongs_exists(model,attr)
end

Then /^the (\S*)'s (\S*) should be "([^\"]*)"$/ do |model, attr, value|
  check_model(model,attr,value)
  puts ("checked model: #{model}, attr: #{attr}, value: #{value}")
end

Then /^(\S*) should be one of (\S*)'s (\S*)$/ do |child, parent, association|
  check_association(child, parent, association)
  puts ("checked association: #{association}, child: #{child}, parent: #{parent}")
end

Then /^(\S*) should be (\S*)'s (\S*)$/ do |parent, child, association|
  check_belongs(parent, child, association)
  puts ("checked belongs: #{parent}, child: #{child}, association: #{association}")
end

Then /^(\S*) should have (\d*) (\S*)$/ do |model, count, attr|
  check_association_count(model,count, attr)
  puts ("checked counts: #{model}, count: #{count}, attr: #{attr}")
end

When /^I edit the (\S*)$/ do |model|
  go_to_edit(model)
end

When /^I create a new (\S*)$/ do |model|
  go_to_new(model)
end

When /^I view the list of (\S*)$/ do |model|
  go_to_index(model)
end
