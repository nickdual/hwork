Feature: Schedule Management

  As a provider of services
  I want to maintain my schedule of availability
  So that customers can book appointments when I can best serve them.

  As a manager of rooms
  I want to maintain a schedule for my rooms
  So that appointments can be appropriately associated to a location.

  Scenario: when a provider is created, they get a schedule
    Given I am going to create a new provider
    When I press "provider_submit"
    Then I should see "Provider was successfully created"
    And provider should exist with signature_name: "Dr. Test Tester"
    And provider "Dr. Test Tester" should have a schedule

  Scenario: when a room is created, it gets a schedule
    Given I am going to create a new room
    When I press "room_submit"
    Then I should see "Room was successfully created"
    And room should exist with name: "TestRoomA"
    And room "TestRoomA" should have a schedule

  Scenario: a provider has a configurable schedule
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I go to Providers
    And I wait for "2" seconds
    Then I should see css="a[title='Schedule']" within the "Bob Robson MD" provider record

  Scenario: a room has a configurable schedule
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I go to Rooms
    And I wait for "2" seconds
    Then I should see css="a[title='Schedule']" within the "Room1" room record

  @javascript
  Scenario: from provider index screen, schedule is accessible in popup
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I go to Providers
    Then I should not see css="#fancybox-content"
    When I follow "Schedule" within the "Bob Robson MD" provider record
    Then I should see css="#fancybox-content"
    And I should see "Editing Bob Robson MD Provider Schedule"

  @javascript
  Scenario: from rooms index screen, schedule is accessible in popup
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I go to Rooms
    Then I should not see css="#fancybox-content"
    When I follow "Schedule" within the "Room1" room record
    Then I should see css="#fancybox-content"
    And I should see "Editing Room1 Room Schedule"

  @javascript
  Scenario: can open and close different provider schedules
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I go to Providers
    And I follow "Schedule" within the "Bob Robson MD" provider record
    Then I should see "Editing Bob Robson MD Provider Schedule"
    And I should see css="#fancybox-close"
    And I should see css="#schedule_calendar"
    When I click "#fancybox-close"
    And I wait for "3" seconds
    Then I should not see "Editing Bob Robson MD Provider Schedule"
    When I follow "Schedule" within the "Rob Bobson MD" provider record
    Then I should see "Editing Rob Bobson MD Provider Schedule"
    And I should see css="#fancybox-close"
    And I should see css="#schedule_calendar"

  @javascript
  Scenario: can open and close different room schedules
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I go to Rooms
    And I follow "Schedule" within the "Room1" room record
    Then I should see "Editing Room1 Room Schedule"
    And I should see css="#fancybox-close"
    And I should see css="#schedule_calendar"
    When I click "#fancybox-close"
    And I wait for "3" seconds
    Then I should not see "Editing Room1 Room Schedule"
    When I follow "Schedule" within the "Room2" room record
    Then I should see "Editing Room2 Room Schedule"
    And I should see css="#fancybox-close"
    And I should see css="#schedule_calendar"

  @javascript
  Scenario: schedule view displays 7 week days
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I open the schedule for room "Room1"
    And I wait for "2" seconds
    Then I should find all of [Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday]

  @javascript @pending
  Scenario: clicking on schedule creates an event
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I open the schedule for room "Room1"
    And I click

  @javascript @pending
  Scenario: if not allowed to edit schedule, calendar change events are disabled

  @javascript @pending
  Scenario: when creating a schedule exception, start date must be before end date

  @javascript @pending
  Scenario: schedule exception form offers available and unavailable

  @javascript @pending
  Scenario: schedule exception list displays in a table

  @javascript @pending
  Scenario: schedule and exceptions display when screen initially loads

  @javascript @pending
  Scenario: schedule popup can be opened and closed multiple times

  @javascript @pending
  Scenario: schedue exception table should indicate when there are no exceptions
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I open the schedule for room "Room1"
    Then I should see "No Current Schedule Exceptions" within "#schedule_exceptions_list"

  @javascript
  Scenario: new schedue exception form required fields
    Given I am going to create a new room schedule exception for "Room1"
    When I fill in the following:
      |Title                    |New Exception          |
      |Start Date               |11/11/2011 13:30       |
    And I press "event_submit"
    Then I should see "This field is required"

  @javascript
  Scenario: new schedue exception form validates end date after start date
    Given I am going to create a new room schedule exception for "Room1"
    When I fill in the following:
      |Title                    |New Exception          |
      |Start Date               |11/11/2011 13:30       |
      |End Date                 |11/10/2011 13:30       |
    And I press "event_submit"
    Then I should see "Must be after Start Date"

  @javascript @pending
  Scenario: new schedule exception form reset clears errors
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I open the schedule for room "Room1"
    And I click "#add_schedule_exception"
    Then I should see "This field is required"
    When I click "#event_reset"
    Then I should not see "This field is required"

  @javascript @pending
  Scenario: feature 150 : providers schedule can include locations
    # different help message indicates the need to schedule information.

  @javascript @pending
  Scenario: feature 150 : open and close event location dialog repeated

  @javascript @pending
  Scenario: feature 150 : events missing required location info will display in a different color

  @javascript @pending
  Scenario: feature 150 : if there's only 1 location, event locations are not required

