Feature: Third Parties

  As a provider of medical services
  I want to keep track of common 3rd party payers (Insurance Carriers and Attorneys)
  So that I can efficiently process, track and report on their cases

  @javascript
  Scenario: show view allows use of resource switcher
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    When I go to Carriers
    And I follow "Show" within the "Carrier1" third party record
    And I wait for "2" seconds
    Then I should see "Switch 3rd party" within the data switcher
    Then I should see "HMO Carrier1"
    When I pick "Carrier2" within the data switcher
    And I wait for "2" seconds
    Then I should see "HMO Carrier2"
