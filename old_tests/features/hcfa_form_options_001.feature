Feature: HCFA Form Options

  As a medical service provider
  I want to be able to standardize the format of my HCFA forms
  So that I can efficiently receive payments for my services from insurance companies

  @javascript
  Scenario: edit view allows use of resource switcher
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to HCFA Form Options
    And I follow "Edit" within the "Commercial" hcfa form option record
    And I wait for "8" seconds
    Then I should see "Switch Hcfa form option" within the data switcher
    And I should see "Editing HCFA Form Options Connecticut - Commercial"
    When I pick "Connecticut - Automobile" within the data switcher
    And I wait for "8" seconds
    Then I should see "Editing HCFA Form Options Connecticut - Automobile"


