Feature: Integration with Braintree Payments

  As a manager of a SaaS application
  I want to support automated recurring billing
  So that I can collect the system subscription fees in a timely manner

  @javascript
  Scenario: admin can add credit card information (creates payment subscription resource)
    Given I register and confirm a new account "westy" and then sign on
    When I follow "links.account_preferences"
    Then I should see "Payment Methods"
    When I follow "Payment Methods"
    Then I should see "New Payment Method"
    When I fill in the following:
      |Cardholder name          |Bob Smith        |
      |Number                   |4012000033330026 |
      |Company                  |Bob Smith LLC.   |
      |CVV                      |555              |
    And I select "March" from "credit_card_expiration_month"
    And I select "2015" from "credit_card_expiration_year"
    And I press "credit_card_submit"
    And I wait for "5" seconds
    Then I should see "Successfully created payment method"
    And I should see "Account preferences"
    And I should have a resque job in "subscriptions"
    When I wait for my resque jobs to complete
    Then account "westy" should have :subscription.subscription_payment_id


