Feature: Provider

  As a healthcare provider
  I want to store my information in Handyworks
  So that I can efficiently render services to my clients

  @javascript
  Scenario: select a new legacy id label while creating a provider
    Given I am going to create a new provider
    And I follow "Add Legacy ID"
    And I type "NEW_LIL" within the new legacy id label
    And I type "MMM-33" within the new legacy id value
    And I press "provider_submit"
    Then I should see "Provider was successfully created"
    And provider should exist with signature_name: "Dr. Test Tester"
    And legacy_id_label should exist with label: "NEW_LIL"
    And legacy_id_label should be one of provider's legacy_id_labels


