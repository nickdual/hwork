Feature: Diagnosis Codes

  As a provider of medical services
  I want to use a standardized set of diagnostic codes
  So that I can efficiently and accurately track the condition of my clients

  @pending
  Scenario: diagnostic codes are configurable by clinic
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I follow "Clinic"
    Then I should see "Diagnosis"

  @pending
  Scenario: can not be deleted if they are involved in patient cases

  Scenario: account admin can add a new diagnosis code
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I view the list of diagnosis_codes
    Then I should see css="a[title='New Diagnosis code']"

  Scenario: ONLY account admin can add a new diagnosis code
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    When I view the list of diagnosis_codes
    Then I should not see css="a[title='New Diagnosis code']"

  Scenario: creating a new diagnosis code
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I create a new diagnosis_code
    And I fill in the following:
      |Name        |123TTT               |
      |Description |123TTT Diagnosis Code|
      |Code        |123123               |
    And I press "Create Diagnosis code"
    Then I should see "Diagnosis code was successfully created"
    And I should see "123TTT"

  @pending
  Scenario: edit view allows use of resource switcher

  @pending
  Scenario: show view allows use of resource switcher

  @pending
  Scenario: switcher only displays resources by clinic

  @pending
  Scenario: switcher default selected value matches current view

  @pending
  Scenario: diagnosis codes are not (nominally) shared between clinics

  @pending
  Scenario: after updating a diagnosis_code you return to the diagnosis_codes list
