Feature: Schedule setup

  As a manger of an office providing services.
  I would like to configure the scheduling parameters
  So that the scheduling tool can be used in manner supporting my office's procedures.

  Scenario: choose to separate providers schedule by clinic
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I go to Schedule setup
    Then I should see "Separate provider's schedule by clinic?"

  Scenario: can specify how far out they'll allow appointments to be booked
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I go to Schedule setup
    Then I should see "Future Appointment Limit"
    When I fill in "120" for "Future Appointment Limit"
    And I press "calendar_submit"
    Then I should see "Schedule was successfully updated"

  Scenario: can specify how far back they'll keep appointments events
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I go to Schedule setup
    Then I should see "Appointment History Limit"
    When I fill in "120" for "Appointment History Limit"
    And I press "calendar_submit"
    Then I should see "Schedule was successfully updated"
