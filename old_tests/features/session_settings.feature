Feature: Session Settings

  As a user of Handyworks
  I would like to use values like current patient, and current clinic
  To improve the efficiency of using Handyworks application

  @javascript
  Scenario: if there are no clinics defined, i don't see anything
    Given I am signed in to Handyworks
    Then I should see "Clinic:"
    And I should not see css="#clinic_selector"

  @javascript @pending
  Scenario: if there is only 1 clinic defined, i see the clinic, but no selection box

  @javascript
  Scenario: if there are multiple clinics, i can switch between them.
    Given a basic set of Handyworks data
    And "staff.login" sets "smoothness" as my theme for clinic "Clinic1"
    And "staff.login" sets "sunny" as my theme for clinic "Clinic2"
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    Then I should see css="body.bp.smoothness"
    And I should see css="#clinic_selector"
    When I select "Clinic2" from "clinic_selector"
    And I wait for "5" seconds
    Then I should see css="body.bp.sunny"
    And "Clinic2" should be selected for "clinic_selector"

  @pending
  Scenario: if there are no patients defined, session controls don't display anything

  @pending
  Scenario: current patient is visible in the session if there is at least one patient
