Feature: HCFA Form Options

  As a medical service provider
  I want to be able to standardize the format of my HCFA forms
  So that I can efficiently receive payments for my services from insurance companies

  @pending
  Scenario: switcher only displays resources by account

  @pending
  Scenario: switcher default selected value matches current view

  Scenario: admin can edit existing HCFA form options
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to HCFA Form Options
    And I follow "Edit" within the "Commercial" hcfa form option record
    Then I should see "Editing HCFA Form Options"

  Scenario: staff can show existing HCFA form options
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    When I go to HCFA Form Options
    And I follow "Show" within the "Commercial" hcfa form option record
    Then I should see "HCFA Form Options"
    And I should not see "Editing"

  Scenario: ONLY admin can edit existing HCFA form options
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    When I go to HCFA Form Options
    Then I should not see "Edit" within ".hcfa_form_option_record"

  @pending
  Scenario: there is no option to create "new" HCFA form options
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to HCFA Form Options
    Then I should not see "New Hcfa Form"
    When I go to the new HCFA Form Options
    Then show me the page
    #Then I should get a "404 Not Found" response

  Scenario: there is no destroy option for HCFA forms
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to HCFA Form Options
    Then I should not see "Destroy"
    #When I go to the new HCFA Form Options
    #Then I should get a "404 Not Found" response

  Scenario: when adding clinics, default HCFA forms will be initialized for the clinic's state
    Given I am going to create a new clinic
    And I select "Alabama" from "clinic_contact_attributes_physical_address_attributes_state"
    When I press "clinic_submit"
    Then I should see "Clinic was successfully created"
    When I go to HCFA Form Options
    And I follow "Edit" within the "Automobile" hcfa form option record
    Then I should see "Editing HCFA Form Options Alabama - Automobile"

    #Then I should see "Switch Hcfa form option" within the data switcher
    #And I should see "Editing HCFA Form Options Connecticut - Commercial"
    #When I pick "Connecticut - Automobile" within the data switcher
    #And I wait for "8" seconds
    #Then I should see "Editing HCFA Form Options Connecticut - Automobile"

  @pending
  Scenario: HCFA form options index should only show current clinic's state + carrier hcfa forms

  @pending
  Scenario: the list of states accessible from the HCFA form should be in sorted order

  @pending
  Scenario: after updating a hcfa form you return to the hcfa forms list
