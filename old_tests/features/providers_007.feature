Feature: Provider

  As a healthcare provider
  I want to store my information in Handyworks
  So that I can efficiently render services to my clients


  @javascript
  Scenario: admin can delete legacy id labels through new provider interface
    Given I am going to create a new provider
    Then I should see css=".delete_legacy_id_label"
    And I should see xpath="//td/input[@value='LIL1']"
    When within the "LIL1" legacy ids row, I follow "Delete complete legacy id label" and confirm popup containing "This will delete the legacy id label and all associated legacy ids"
    Then I should see "Legacy ids have been successfully destroyed"
    And I should not see xpath="//td/input[@value='LIL1']"

