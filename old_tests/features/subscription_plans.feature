Feature: subscription plan features

  As a medical service provider interested in Handyworks
  I want to learn about the subscription plans offered with Handyworks
  So that I can learn about the features and cost of using Handyworks.

  Scenario: anonymous user can see plan listing
    Given I am not signed in to Handyworks
    When I go to the home page
    Then I should see "links.plan_options"

  Scenario: admin users can see plan listing
    Given I am a Handyworks user
    And I have the role "AccountAdmin"
    And I login to Handyworks
    When I follow "links.account_preferences"
    Then I should see css="#account_submit"

  Scenario: ONLY admin users can see plan listing
    Given I am a Handyworks user
    When I login to Handyworks
    Then I should not see "links.account_preferences"

  Scenario: anonymous user can see plan details
    Given I am not signed in to Handyworks
    And I go to the subscription_plans page
    Then I should see "links.plan_details"

  Scenario: anonymous user can sign up by plan
    Given I am not signed in to Handyworks
    When I go to the subscription_plans page
    Then I should see "links.select_plan" within "a#free_plan"
    And I should see "links.select_plan" within "a#basic_plan"

  Scenario: users will not see sign up links
    Given I am signed in to Handyworks
    And I have the role "AccountAdmin"
    When I go to the subscription_plans page
    Then I should not see "links.select_plan"

  Scenario: anonymous user can sign up through show plan
    Given I am not signed in to Handyworks
    And I go to the subscription_plans page
    When I follow "links.plan_details"
    Then I should see "links.select_plan"

  Scenario: unlimited subscription plan limits
    Given I am not signed in to Handyworks
    And I go to the subscription_plans page
    When I follow "links.plan_details" within "#max_details"
    Then I should see "Providers: unlimited"

  Scenario: subscription plan amounts display as currency
    Given I am not signed in to Handyworks
    And I go to the subscription_plans page
    When I follow "links.plan_details" within "#basic_details"
    Then I should see "$" 

  Scenario: users can see all plan options from the detail page
    Given I am not signed in to Handyworks
    And I go to the subscription_plans page
    When I follow "links.plan_details" within "#basic_details"
    Then I should see "links.all_plans" 
