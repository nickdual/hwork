Feature: User Management

  As an account administrator
  I want to manage users
  So that I can delegate tasks to the appropriate users

  @javascript
  Scenario: account admin can edit user account with out changing password
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Users
    And I follow "Edit" within the staff user record
    And I fill in "Email" with "b@c.dom"
    And I press "user_submit" 
    Then I should see "User was successfully updated"
    And the "Email" field should contain "b@c.dom"


