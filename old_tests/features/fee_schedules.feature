Feature: Fee Schedules

  As a provider of health-care services
  I want to maintain a list of payment fee models
  So that I can efficiently maintain separate charges for different procedure codes
  And that I can effectively manage which fee schedule will be applied to a particular patient for services

  Scenario: application admin can view current fee schedules 
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "app_admin.login" and :password = "app_admin.pass"
    When I follow "Clinic"
    Then I should see "Fee Schedules"


  Scenario: application admin can add a new Fee Schedule
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "app_admin.login" and :password = "app_admin.pass"
    When I go to Fee Schedules
    Then I should see css="a[title='New Fee Schedule']"
    When I follow "New Fee Schedule"
    And I fill in the following:
      |Fee Schedule Label|TS0|
    And I press "fee_schedule_submit"
    Then a fee schedule should exist with label: "TS0"

  Scenario: account admin does not have a Fee Schedules interface
    Given I am a Handyworks user with the "AccountAdmin" role
    When I login to Handyworks
    And I follow "Clinic"
    Then I should not see "Fee Schedules"

  @pending
  Scenario: new accounts will start with "default" and "auto" fee schedules

  @pending
  Scenario: edit view allows use of resource switcher

  @pending
  Scenario: show view allows use of resource switcher

  @pending
  Scenario: switcher only displays resources by account

  @pending
  Scenario: switcher default selected value matches current view

  @pending
  Scenario: after updating a fee_schedule you return to the fee_schedules list
