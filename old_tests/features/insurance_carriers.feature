Feature: Insurance Carriers

  As a healthcare professional
  I want to conduct business with insurance companies
  So that I can get paid for my services through the healthcare system we have today
 
  Scenario: account admin can view current carriers
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Carriers
    Then I should see "Carrier1"
    And I should see "Carrier2"
    And I should not see "Carrier3"

  Scenario: account admin can add a new carrier
    Given I am going to create a new carrier
    When I press "third_party_submit"
    Then I should see "ThirdParty was successfully created"

  Scenario: ONLY account admin can add a new carrier
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    When I go to Carriers
    Then I should not see "New Insurance carrier"

  Scenario: when creating a carrier, you can select the provider legacy id label
    Given I am going to create a new carrier
    And legacy_id_label should exist with label: "LIL1"
    Then I should see "Legacy ID Label"
    When I pick "LIL1" within the carrier legacy id label
    And I press "third_party_submit"
    Then insurance_carrier should exist with alias_name: "Test A Plan"
    And legacy_id_label should be insurance_carrier's legacy_id_label

  @javascript
  Scenario: you can add a new provider legacy id label
    Given I am going to create a new carrier
    And I wait for "2" seconds
    Then I should see "Legacy ID Label"
    When I click ".combo_fields_link" within the carrier legacy id label
    And I type "NEW_LIL7" within the carrier legacy id label
    And I press "third_party_submit"
    Then insurance_carrier should exist with alias_name: "Test A Plan"
    And legacy_id_label should exist with label: "NEW_LIL7"
    And legacy_id_label should be insurance_carrier's legacy_id_label

  @pending
  Scenario: insurance carrier can provide a separate billing address

  @pending
  Scenario: can create a carrier without a legacy label id

  # bugfix #91 : business email is not a required field
  Scenario: admin creates a new carrier, with only a name
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Carriers
    And I follow "New 3rd Party"
    And I select "Oklahoma" from "State"
    And I select "PPO" from "Carrier Type"
    And I fill in the following:
      |Carrier / Law Firm Name  |Test Carrier   |
    And I press "third_party_submit"
    Then I should see "ThirdParty was successfully created"

  # bugfix #49
  Scenario: carriers display the carrier type as a string
    Given I am going to create a new carrier
    When I select "Medicare" from "Carrier Type"
    And I press "third_party_submit"
    Then I should see "ThirdParty was successfully created"
    When I go to Carriers
    Then I should see "Plan Name"
    And I should not see "Alias Name"
    And I should see "Medicare" within "#third_parties_list"

  #bugfix #91
  Scenario: carrier email is validated when entered
    Given I am going to create a new carrier
    And I fill in "Email" with "r@z"
    When I press "third_party_submit"
    Then I should see "should be a valid email address"



