Feature: User Management

  As an account administrator
  I want to manage users
  So that I can delegate tasks to the appropriate users

  Scenario: account admin users can add a new user account
    Given I am a Handyworks user with the "AccountAdmin" role
    And I login to Handyworks
    When I go to Users
    Then I should see css="a[title='New User']"

  Scenario: account admin users can edit a user account
    Given I am a Handyworks user with the "AccountAdmin" role
    And I login to Handyworks
    When I go to Users
    Then I should see css=".user-edit" within ".user_record"

  Scenario: bugfix #22 non-admin user can access user index
    Given I am a Handyworks user
    And I login to Handyworks
    When I go to Users
    Then I should see css=".user-show" within ".user_record"

  @pending
  Scenario: edit view allows use of resource switcher

  @pending
  Scenario: show view allows use of resource switcher

  @pending
  Scenario: switcher only displays resources by account

  @pending
  Scenario: switcher default selected value matches current view

  @pending
  Scenario: after updating a user you return to the users list
