Feature: User Profile

  As a user of Handyworks
  I want to be able to manage my profile
  So that I can preserve preferences, and maintain account security

  Scenario: users have access to their account profile
    Given I am signed in to Handyworks
    Then I should see "My Account"

  @javascript
  Scenario: current password is required to change account info
    Given I am signed in to Handyworks
    And I follow "My Account"
    When I fill in the following:
      |Email                |stevent@example.com            |
    And I press "user_submit"
    Then I should see "Current password can't be blank"

  @javascript
  Scenario: users can change their email address
    Given I am signed in to Handyworks
    When I follow "My Account"
    Then I should see "Email"
    When I fill in the following:
      |Email                |stevent@example.com            |
      |Current password     |please                         |
    And I press "user_submit"
    Then I should see "You updated your account successfully"

  @javascript
  Scenario: user email address changes are validated
    Given I am signed in to Handyworks
    When I follow "My Account"
    Then I should see "Email"
    When I fill in the following:
      |Email                |steven@com                     |
      |Current password     |please                         |
    And I press "user_submit"
    Then I should see "Email is invalid"

  @javascript
  Scenario: user can specify their default clinic
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    And I follow "My Account"
    Then I should see "Clinic Preferences"
    When I follow "Clinic Preferences"
    Then I should see "Default clinic" within the user clinic profile
    And "Clinic1" should be selected for "Default clinic" within the user clinic profile
    When I select "Clinic2" from "Default clinic" within the user clinic profile
    And I press "user_submit" within the user clinic profile
    Then I should see "You updated your account successfully"
    When I follow "My Account"
    And I follow "Clinic Preferences"
    Then "Clinic2" should be selected for "Default clinic" within the user clinic profile

  @javascript
  Scenario: when a user logs in thier current clinic is their default clinic
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    And I follow "My Account"
    #Then I should see "Clinic: Clinic1"
    Then "Clinic1" should be selected for "clinic_selector"
    When I follow "Clinic Preferences"
    And I select "Clinic2" from "Default clinic" within the user clinic profile
    And I press "user_submit" within the user clinic profile
    Then I should see "You updated your account successfully"
    When I follow "Sign Out"
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    And I wait for "10" seconds
    #Then I should see "Clinic: Clinic2"
    Then "Clinic2" should be selected for "clinic_selector"

  @javascript
  Scenario: users can select themes for each clinic
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    Then I should see "Clinic: Clinic1"
    And I should see css="body.bp.smoothness"
    When I follow "My Account"
    And I follow "Clinic Preferences"
    Then I should see "Themes" within the user clinic profile
    And I should see "Clinic1" within the user clinic profile
    And I should see "Clinic2" within the user clinic profile
    When I select "High Contrast" from "Clinic1" within the user clinic profile
    And I press "user_submit"
    Then I should see "You updated your account successfully"
    And I should see css="body.bp.hotsneaks"

