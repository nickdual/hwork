Feature: Procedure Codes

  As a Medical Service Provider
  I want to keep track of the services I provide
  And I want to keep track of the fees for those services
  So I need standard procedure codes to be supported

  @javascript
  Scenario: Bug #148 procedure code show view should reflect new fee schedules
    Given I am going to create a new procedure code
    When I follow "Add Fee Schedule"
    And I type "NEW_FS4" within the new fee schedule label
    And I type "1000" within the new fee schedule fee
    And I type "300" within the new fee schedule copay
    When I press "procedure_code_submit"
    Then I should see "Procedure code was successfully created"
    When I pick "PC2" within the data switcher
    And I wait for "2" seconds
    Then I should see "PC2"
    And the "procedure_code_fees_attributes_2_fee_schedule_attributes_label" field should contain "NEW_FS4"

