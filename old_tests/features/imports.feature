Feature: Imports

  As a new user of Handyworks R3
  I would like to migrate my data from Handyworks Access
  So that I can begin using Handyworks R3 with little disruption to my business

  @pending
  Scenario: import feature is accessible by direct path

  @pending
  Scenario: admin can view previous import data sets

  @pending
  Scenario: admin can delete previous import data sets

  @pending
  Scenario: admin uploads a csv file with invalid records

  @pending
  Scenario: new import form requires a file to be specified

  @pending
  Scenario: import list indicates the number of records imported / total

  @pending
  Scenario: import list only includes imports for the current account

  @pending
  Scenario: successful import redirects user to the imports list.

  @pending
  Scenario: records can be skipped based skip_condition configuration

  @pending
  Scenario: files are stored in temporary location and are not accessible publicly

  @pending
  Scenario: when an import is deleted, all files are deleted as well
