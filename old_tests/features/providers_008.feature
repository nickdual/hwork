Feature: Provider

  As a healthcare provider
  I want to store my information in Handyworks
  So that I can efficiently render services to my clients


  @javascript
  Scenario: admin can cancel the legacy id label delete request
    Given I am going to create a new provider
    Then I should see css=".delete_legacy_id_label"
    And I should see xpath="//td/input[@value='LIL1']"
    When within the "LIL1" legacy ids row, I follow "Delete complete legacy id label" and dismiss popup containing "This will delete the legacy id label and all associated legacy ids"
    And I wait for "10" seconds
    Then I should see xpath="//td/input[@value='LIL1']"
