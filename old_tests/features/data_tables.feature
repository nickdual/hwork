Feature: Data Tables display lists

  As a user of Handyworks
  I want a consistent display of data in easily usable interfaces
  So that I can work with any part of the Handyworks data intuitively

  @pending
  Scenario: data tables will default to showing 100 records.

  @pending
  Scenario: users can change the size of icons displayed in the data tables.
    # using slider

  @pending
  Scenario: "change cols" option is not available by default to users
    # available if expert is true
