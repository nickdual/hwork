Feature: Account Defaults

  As a new account subscriber
  I want to do as little configuration as possible 
  So that I can get up and running quickly

  Scenario Outline: handyworks objects
    Given I'm ready to test
    Then handyworks_object should exist with description: "<description>"

  @pending
  Examples: all handyworks objects
    |description         |
    |Web Account Users   |
    |Clinics             |
    |Clinic Preferences  |
    |Account Preferences |
    |Procedure Codes     |
    |Diagnosis Codes     |
    |Providers           |
    |Procedure Code Fees |
    |Bills               |
    |Statements          |
    |Financial Reports   |
    |Insurance Carriers  |
    |Attorneys           |
    |Referrers           |
    |Fee Schedules       |
    |Patients            |
    |Patient Cases       |
    |Patient Visits      |
    |Appointments        |
    |Patient Reports     |

  Scenario Outline: default object group template
    Given I'm ready to test
    Then account should exist with name: "GLOBAL DEFAULTS"
    And handyworks_object_group should exist with label: "<label>"
    And account should be handyworks_object_group's account

  @pending
  Examples: default object groups
    | label     |
    | Account   |
    | Clinical  |
    | Financial |
    | Records   |
    | Patients  |

  Scenario Outline: default object group assignment template
    Given I'm ready to test
    Then handyworks_object should exist with description: "<description>"
    And handyworks_object_group_assignment should exist with handyworks_object: handyworks_object

  @pending
  Examples: all handyworks objects
    |description         |
    |Web Account Users   |
    |Clinics             |
    |Clinic Preferences  |
    |Account Preferences |
    |Procedure Codes     |
    |Diagnosis Codes     |
    |Providers           |
    |Procedure Code Fees |
    |Bills               |
    |Statements          |
    |Financial Reports   |
    |Insurance Carriers  |
    |Attorneys           |
    |Referrers           |
    |Fee Schedules       |
    |Patients            |
    |Patient Cases       |
    |Patient Visits      |
    |Appointments        |
    |Patient Reports     |
    |NewYork Reports     |

  Scenario: account default legacy id labels
    Given I register and confirm a new account "westy" and then sign on
    Then account should exist with name: "westy"
    And account should have 1 legacy_id_labels

  Scenario: account default appointment types
    Given I'm ready to test
    Then appointment_type should exist with name: "General Service"

  @pending
  Scenario: new accounts are initialized with default appointment types
    Given I register and confirm a new account "westy" and then sign on
    Then account should exist with name: "westy"
    And the account's calendar should exist
    And calendar should have 1 appointment_types
