Feature: Menu Controls

  As a rich web application user
  I want an intuitive menu interface
  So that I can work productively

  @javascript
  Scenario: basic menus are visible on login
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    Then I should see "Patients"
    Then I should see "Cases"
    Then I should see "Appointments"
    Then I should see "Transactions"
    Then I should see "Reports"
    Then I should see "Billing"
    Then I should see "Clinic"
    Then I should see "Admin"

  @javascript
  Scenario: most controls are not regularly visible
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    Then I should not see css="#case_controls"
    Then I should not see css="#appointment_controls"
    Then I should not see css="#transaction_controls"
    Then I should not see css="#report_controls"
    Then I should not see css="#billing_controls"
    Then I should not see css="#setup_controls"
    Then I should not see css="#patient_controls"

  @javascript
  Scenario: you can click on menu tab to see those controls
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I wait for "1" seconds
    Then I should not see css="#patient_controls"
    When I follow "Patients"
    And I wait for "1" seconds
    Then I should see css="#patient_controls"
    Then I should not see css="#case_controls"
    When I follow "Cases"
    And I wait for "1" seconds
    Then I should see css="#case_controls"
    Then I should not see css="#appointment_controls"
    When I follow "Appointments"
    And I wait for "1" seconds
    Then I should see css="#appointment_controls"
    Then I should not see css="#transaction_controls"
    When I follow "Transactions"
    And I wait for "1" seconds
    Then I should see css="#transaction_controls"
    Then I should not see css="#report_controls"
    When I follow "Reports"
    And I wait for "1" seconds
    Then I should see css="#report_controls"
    Then I should not see css="#billing_controls"
    When I follow "Billing"
    And I wait for "1" seconds
    Then I should see css="#billing_controls"
    Then I should not see css="#setup_controls"
    When I follow "Clinic"
    And I wait for "1" seconds
    Then I should see css="#setup_controls"

  @pending
  Scenario: Handyworks menus require a mouse click to change between different "drop down" menus.

  @pending
  Scenario: 2nd level menu indicates which view user is currently on (active)

  @pending
  Scenario: 2nd level menu updates when client side view changes
    # test this using the Appointments tab, switching from Weekly, 5-day etc. should update the active button.
