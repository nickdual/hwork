Feature: Import Groups

  As a new user of the Handyworks web application
  I would like to quickly and efficiently load my current Handyworks Access data set.
  So that I can transition to the new system without significant disruption

  @pending
  Scenario: import groups are accessed under setup

  @pending
  Scenario: loading the initial import group of data for a new account

  @pending
  Scenario: CSV error handling when loading an import group

  @pending
  Scenario: required files in an import group will be marked as required

  @pending
  Scenario: import group list includes record counts

  @pending
  Scenario: import group list includes links to view errors

  @pending
  Scenario: import groups may only be imported in an expected order
    # initially only Base Data import allowed
    # then [ Providers, Procedures, Patients ] in sequence

  @pending
  Scenario: by default import groups will fail transaction if errors occur
    # verify nothing is imported when one file returns errors by default

  @pending
  Scenario: importing legacies should link with expected provider and legacy labels

  @pending
  Scenario: import carrier specific hcfa forms along with thier associcated carrier records

  @pending
  Scenario: after submitting import groups, submit button is disabled with "Please wait..."

  @pending
  Scenario: loading patients referrals from other patients

  @pending
  Scenario: loaing patients referrals from generic references

  @pending
  Scenario: filter input file selection dialogs to only look for CSV files

  @pending
  Scenario: default duplicate handling is to replace existing records

  @pending
  Scenario: default error handling is to continue on errors

  @pending:
  Scenario: duplicate and error handling options are not available on new import group view

  @pending
  Scenario: use expert parameter to access duplicate handling option

  @pending
  Scenario: use expert parameter to access error handling option

  @pending
  Scenario: user is REQUIRED to match the selected filename to expected filename

  @pending
  Scenario: expert user is NOT REQUIRED to match the selected filename to the expected filename, only warned

  @pending
  Scenario: import of HWPref sets default provider from ProviderID field

  @pending
  Scenario: when an import group is deleted, all imports for that group are deleted

  @pending
  Scenario: asynchronous processing of import group submissions through account preferences interface

  @pending
  Scenario: only one import group can be processed by an account a one time

  @pending
  Scenario: if an import step fails server side validation, the user can resubmit with changes
    # this was demonstrated by disabling the required file validation on the client side
    # when the server detected the missing required file, the error should be displayed to 
    # the user, they should be able to select the missing file and continue with thier
    # form submission

  @pending
  Scenario: unexpected error returned from the server is displayed to the user
    # this was demonstrated by explicitly adding a "raise" line to the ImportGroupsController.create method
    # when a server side exception occurs in submitting the form, a message should be displayed
    # to the user and they should be fix the form and re-submit.
    
  @pending
  Scenario: ID column is not visible when users view the import groups index page

  @pending
  Scenario: import group index view shall automatically refresh if any import groups are not finished loading

  @pending
  Scenario: all files are required on the first import for any group
