Feature: Insurance Carriers

  As a healthcare professional
  I want to conduct business with insurance companies
  So that I can get paid for my services through the healthcare system we have today
 

  @javascript
  Scenario: legacy id labels for carriers have account scope
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Carriers
    And I follow "New 3rd Party"
    And I wait for "3" seconds
    Then I should see "LIL1"
    Then I should not see "LIL3"

