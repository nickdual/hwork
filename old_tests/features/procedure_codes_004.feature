Feature: Procedure Codes

  As a Medical Service Provider
  I want to keep track of the services I provide
  And I want to keep track of the fees for those services
  So I need standard procedure codes to be supported

  # Bug #94 ( and also #60 ) 
  # Currently this test only works when the window is in the foreground of the current
  # X session, not sure why that is, but it works when run manually, not when unattended
  @pending @javascript
  Scenario: when adding a fee schedule, expected insruance is updated automatically
    Given I am going to create a new procedure code
    When I follow "Add Fee Schedule"
    And I type "NEW_FS4" within the new fee schedule label
    And I type "1000" within the new fee schedule fee
    And I press tab
    Then check the field in the new fee schedule expected insurance should contain "1000.00"
    When I type "30" within the new fee schedule copay
    And I press tab
    Then check the field in the new fee schedule expected insurance should contain "970.00"
    When I click "input[type=checkbox]" within the new fee schedule percentage
    And I press tab
    Then check the field in the new fee schedule expected insurance should contain "700.00"
