Feature: Attorneys

  As a provider of chiropractic care
  I want to keep track of attorneys I do work for
  So that I can efficiently track information about their cases

  Scenario: attorneys are defined at the account level
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I follow "Clinic"
    Then I should see "titles.third_parties"
    When I follow "titles.third_parties"
    Then I should see "Attorney1"
    And I should see "Attorney2"
    And I should not see "Attorney3"

  Scenario: admin can create a new attorney with only name and firm
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Attorneys
    Then I should see css="a[title='New 3rd Party']"
    When I follow "New 3rd Party"
    And I select "Attorney" from "Carrier Type"
    And I fill in the following:
      |Carrier / Law Firm Name  |Test And Tester|
      |First name               |Test           |
      |Last name                |Tester         |
    And I press "third_party_submit"
    Then I should see "ThirdParty was successfully created"
    And contact_third_party should exist with company_name: "Test And Tester"
    
  Scenario: ONLY admin can create a new attorney
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    When I go to Attorneys
    Then I should not see "New 3rd Party"

  Scenario: create an attorney with a fax number
    Given I am going to create a new attorney
    Then I should see "Fax phone"
    When I fill in the following:
      |Fax phone               |(212) 444-5555 |
    And I press "third_party_submit"
    Then I should see "ThirdParty was successfully created"

  Scenario: email is validated if it is entered
    Given I am going to create a new attorney
    And I fill in "Email" with "jeff"
    When I press "third_party_submit"
    Then I should see "should be a valid email address"

  Scenario: phone is validated if it is entered
    Given I am going to create a new attorney
    And I fill in "Business phone" with "(33) 444-5555"
    When I press "third_party_submit"
    Then I should see "invalid format"
    
  Scenario: fax is validated if it is entered
    Given I am going to create a new attorney
    And I fill in "Fax phone" with "(333) 44-5555"
    When I press "third_party_submit"
    Then I should see "invalid format"
