Feature: Third Parties

  As a provider of medical services
  I want to keep track of common 3rd party payers (Insurance Carriers and Attorneys)
  So that I can efficiently process, track and report on their cases

  @javascript
  Scenario: edit view allows use of resource switcher
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Third Parties
    When I follow "Edit" within the "Attorney1" third party record
    And I wait for "8" seconds
    Then I should see "Switch 3rd party" within the data switcher
    And I should see "Editing Attorney Attorney1"
    When I pick "Attorney2" within the data switcher
    And I wait for "8" seconds
    Then I should see "Editing Attorney Attorney2"

