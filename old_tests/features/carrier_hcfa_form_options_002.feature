Feature: Carrier HCFA Form Options

  As a medical service provider
  I want the ability to customize my HCFA forms for individual carriers
  So that I will be reimbursed in a timely manner when submitting claims to those carriers

  @javascript
  Scenario: admin can create new custom carrier hcfa options from 'edit' view
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Carriers
    And I follow "Edit" within the "Carrier1 Company" third party record
    And I wait for "2" seconds
    Then I should see css="a[title='New HCFA Form Options']"
    When I follow "New HCFA Form Options"
    And I press "hcfa_form_option_submit"
    Then I should see "Carrier hcfa form option was successfully created"


