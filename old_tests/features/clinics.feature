Feature: Clinic Management

  As a health care service organization
  I want to have one or more Clinics defined in Handyworks
  So that I can model the actual running of my practice(s)

  Scenario: account admin users can create new clinics
    Given I am a Handyworks user with the "AccountAdmin" role
    And I login to Handyworks
    When I go to Clinics
    Then I should see css="a[title='New Clinic']"

  Scenario: ONLY account admin users can create new clinics
    Given I am a Handyworks user
    And I login to Handyworks
    When I go to Clinics
    Then I should not see "New Clinic"

  Scenario: users can see their current clinic
    Given an account with clinic exists with name "Test Clinic A"
    And I am a Handyworks user
    When I login to Handyworks
    And I should see "Clinic: Test Clinic A"

  Scenario: users can see a list of the clinics on their account
    Given an account with clinic exists with name "Test Clinic A"
    And I am a Handyworks user
    And this account has a clinic with name "Test Clinic B"
    And I login to Handyworks
    When I go to Clinics
    And I should see "Test Clinic A"

  Scenario: account admin users can update clinics
    Given I am a Handyworks user with the "AccountAdmin" role
    And I login to Handyworks
    When I go to Clinics
    Then I should see css="a[title=Edit]" within ".clinic_record"

  Scenario: ONLY account admin users can update clinics
    Given I am a Handyworks user
    And I login to Handyworks
    When I go to Clinics
    Then I should not see css="a[title=Edit]" within ".clinic_record"

  Scenario: account admin users can destroy clinics
    Given I am a Handyworks user with the "AccountAdmin" role
    And I login to Handyworks
    When I go to Clinics
    Then I should see css="a[title=Destroy]" within ".clinic_record"

  Scenario: ONLY account admin users can destroy clinics
    Given I am a Handyworks user
    And I login to Handyworks
    When I go to Clinics
    Then I should not see "Destroy" within ".clinic_record"

  @pending
  Scenario: index only shows clinics from the current account

  @pending
  Scenario: A clinic has a main provider

  @pending
  Scenario: edit view allows use of resource switcher

  @pending
  Scenario: show view allows use of resource switcher

  @pending
  Scenario: switcher only displays resources by account

  @pending
  Scenario: switcher default selected value matches current view

  Scenario: create a clinic filling in all required fields
    Given I am going to create a new clinic
    When I press "clinic_submit"
    Then I should see "Clinic was successfully created"

  Scenario: creating a clinic only requires a name and state
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Clinics
    And I follow "New Clinic"
    And I press "clinic_submit"
    Then I should see "name can't be blank"
    And I should see "state can't be blank"
    When I fill in "Clinic Name" with "HowDoYaDo"
    And I select "Florida" from "clinic_contact_attributes_physical_address_attributes_state"
    And I press "clinic_submit"
    Then I should see "Clinic was successfully created"

  @pending
  Scenario: the list of states available from the clinic entry from should be sorted

  @pending
  Scenario: after updating a clinic you return to the clinics list
