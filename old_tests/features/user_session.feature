Feature: users sessions
  As an anonymous user
  I want to authenticate with Handyworks Application
  So that I may access my data

  As a user finished with my application work session
  I want to sign out of the Handyworks Application
  So that my practice data remains secure

  As an authenticated user who has been idle
  I want the application to end my work session
  So that my practice data remains secure

  Scenario: anonymous user log in
    Given I am not signed in to Handyworks
    When I am on the home page
    Then I should see "links.log_in"

  Scenario: authenticated user can log out
    Given I am a Handyworks user
    When I login to Handyworks
    Then I should see "links.log_out"

  @pending
  Scenario: authenticated user session time out
    Given I am a Handyworks user
    When I sit idle for "11" minutes
    Then I am redirected to "the home page"
