Feature: Procedure Codes

  As a Medical Service Provider
  I want to keep track of the services I provide
  And I want to keep track of the fees for those services
  So I need standard procedure codes to be supported


  @javascript
  Scenario: admin can cancel the fee schedule delete request
    Given I am going to create a new procedure code
    Then I should see css=".delete_fee_schedule"
    And I should see xpath="//td/input[@value='FS1']"
    When within the "FS1" fees row, I follow "Delete complete fee schedule" and dismiss popup containing "This will delete the fee schedule and all associated fees"
    And I wait for "10" seconds
    Then I should see xpath="//td/input[@value='FS1']"

