Feature: Provider

  As a healthcare provider
  I want to store my information in Handyworks
  So that I can efficiently render services to my clients


  @javascript
  Scenario: remove new legacy id label from an existing procedure code
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I go to Providers
    And I wait for "1" seconds
    When I follow "Edit" within the "Rob Bobson MD" provider record
    Then the "provider_provider_legacy_id_labels_attributes_0_legacy_id_label_attributes_label" field should contain "LIL1"
    And the "provider_provider_legacy_id_labels_attributes_1_legacy_id_label_attributes_label" field should contain "LIL2"
    And I should not see css=".stop" within "#provider_legacy_id_labels"
    When I follow "Add Legacy ID"
    Then I should see css=".stop" within "#provider_legacy_id_labels"
    When I follow "Cancel new legacy id" within "#provider_legacy_id_labels"
    And I wait for "2" seconds
    Then I should not see css=".stop" within "#provider_legacy_id_labels"

