Feature: Provider

  As a healthcare provider
  I want to store my information in Handyworks
  So that I can efficiently render services to my clients


  @javascript
  Scenario: select a new legacy id label while updating a provider
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And provider should exist with signature_name: "Rob Bobson MD"
    When I go to Providers
    And I follow "Edit" within the "Rob Bobson MD" provider record
    And I follow "Add Legacy ID"
    And I type "NEW_LIL2" within the new legacy id label
    And I type "DDD-MM" within the new legacy id value
    And I press "provider_submit"
    Then legacy_id_label should exist with label: "NEW_LIL2"
    And legacy_id_label should be in provider's legacy_id_labels

