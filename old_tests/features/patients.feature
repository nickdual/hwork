Feature: Patients

  As a provider of medical services
  I would like to maintain a database of my clients
  To efficiently track their case history and perform billing operations

  @javascript
  Scenario: bug #185 empty patient list exception
    Given I am signed in to Handyworks
    When I follow "Patients"
    And I follow "List"
    And I wait for "2" seconds
    Then I should see "No data available in table"

  @pending
  Scenario: current patient is based on the prefered clinic if defined

  @pending
  Scenario: current patient can be set from the patient index list

  @pending
  Scenario: after creating a new patient, the new patient is the current patient

  @pending
  Scenario: after editing a patient, the edited patient is the current patient

  @pending
  Scenario: after showing a patient, the displayed patient is the current patient

  @pending
  Scenario: a patient can be created using only first OR last name

  @pending
  Scenario: p patient can not be created with no first and last name

  @pending
  Scenario: create a patient using nominal settings

  @javascript @pending
  Scenario: patient field birthdate validates the date format, but is not required

  @pending
  Scenario: editing a patient displays the birthdate in the correct format

  @pending
  Scenario: sex field for a new patient defaults to female

  @pending
  Scenario: sex value must be in the set of male(1) ro femail(0).

  @javascript @pending
  Scenario: personal email field validates a valid email address, but is not required

  @javascript @pending
  Scenario: delete a patient currently indicated as the current patient in the upper right corner

  @javascript @pending
  Scenario: a patient record can include employer information

  @pending
  Scenario: after updating a patient you return to the patients list
