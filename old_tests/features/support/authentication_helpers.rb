# from: http://blog.devinterface.com/2009/12/add-current_user-to-cucumber-step-definitions/
#authentication_steps.rb
=begin
module AuthenticationHelpers
  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end

  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.user
  end
end
World(AuthenticationHelpers)
=end
