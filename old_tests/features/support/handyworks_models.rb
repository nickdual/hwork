module HandyworksModelHelpers

  def given_model(model,attr,value)
    m1 = eval "#{model.camelize}.find_by_#{attr}('#{value}')"
    raise "no such model instance" if m1.nil?
    @captured_model ||= {}
    @captured_model[model] = m1
  end

  def check_model(model,attr,value)
    # calling reload to ensure attributes are up to date.
    v1 = @captured_model[model].reload.attributes[attr]
    puts ("check_model: #{model}, #{attr} ==> #{v1}")
    raise "unexpected value #{v1}" unless v1 == value
  end

  def check_association(child, parent, association)
    result = eval "@captured_model[parent].#{association}.member?(@captured_model[child])"
    raise "no association" unless result
  end

  def check_belongs(parent, child, association)
    result = eval "@captured_model[child].#{association} == @captured_model[parent]"
    raise "no belongs to" unless result
  end

  def check_belongs_exists(model, attr)
    r = @captured_model[model].attributes[attr]
    raise "attribute does not exist" if r.nil?
  end

  def go_to_edit(model)
    m1 = @captured_model[model]
    path = eval "edit_#{model}_path(m1)"
    puts("going to edit: #{model} --> #{path}")
    visit path
  end

  def go_to_new(model)
    path = eval "new_#{model}_path"
    puts ("going to new: #{model} --> #{path}")
    visit path
  end

  def go_to_index(model)
    path = eval "#{model.pluralize}_path"
    puts ("going to index: #{model} --> #{path}")
    visit path
  end
end

World(HandyworksModelHelpers)

