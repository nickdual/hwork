module ApplicationHelpers
  # Custom Test Helpers developed with Handyworks.

  #-------------------------------------------------------------------------------
  # will determine if there is a translation defined for the given text and 
  # return either the translated or original text if no translation is available
  #-------------------------------------------------------------------------------
  def check_translation(text)
    _text = I18n.translate(text, :default => '')
    _text == '' ? text : _text
  end

  def user 
    @account ||= account_with_clinic(:name => 'Hello')
    @user ||= Factory(:user, :account => @account)
  end

  def account_with_clinic(name)
    @account ||= Factory(:account, :name => name)
    @account.clinics.clear
    @clinic ||= Factory(:clinic, :account => @account)
    @clinic.name = name
    @account.clinics << @clinic
    @account.save!
    @account
  end

  def login(login = @user.login, password = @user.password)
   visit path_to('the home page') 
   click_link I18n.t('links.log_in') 
   fill_in('user_login', :with => login)
   fill_in('user_password', :with => password)
   click_button I18n.t('buttons.log_in')
  end

  # http://jjinux.blogspot.com/2009/08/rails-dynamic-404s-authlogic-cucumber.html
  def assert_response_status(http_status, message)
    response.status.should == "#{http_status} #{message}"
  end

  #-----------------------------------------------------------------------------
  # The following 2 functions to wrap handing a JavaScript confirm dialog
  # through Capybara are from:
  #  https://github.com/thoughtbot/capybara-webkit/issues/84
  #-----------------------------------------------------------------------------
  def handle_js_confirm(accept=true)
    puts "--> I should be accepting: #{accept}"
    page.execute_script "window.original_confirm_function = window.confirm"
    page.execute_script "window.confirmMsg = null"
    page.execute_script "window.confirm = function(msg) { window.confirmMsg = msg; return #{!!accept}; }"
    puts "handle_js_confirm setup complete"
    yield
    puts "--> finished handle_js_confirm_wrapper"
  ensure
    page.execute_script "window.confirm = window.original_confirm_function"
    puts "--> restored existing window.confirm"
  end

  def get_confirm_text
    t = page.evaluate_script "window.confirmMsg"
    puts "confirm text finds: #{t}"
    return t
  end

end

World(ApplicationHelpers)
