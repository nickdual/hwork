Feature: Resque Job Scheduling

  As a Handyworks system operator,
  I would like to schedule recurring processing jobs
  So I can be sure they are executed regularly.

  Scenario: resque web interface is available to app admins
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "app_admin.login" and :password = "app_admin.pass"
    And I go to Resque
    Then I should see "Powered by Resque"

  @pending
  Scenario: resque web is ONLY available to app admins

  Scenario: resque scheduler is available through web interface
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "app_admin.login" and :password = "app_admin.pass"
    And I go to Resque
    Then I should see "Schedule"
    And I should see "Delayed"
    When I follow "Schedule" 
    Then I should see "The list below contains all scheduled jobs"
    When I follow "Delayed"
    Then I should see "This list below contains the timestamps for scheduled delayed jobs"

