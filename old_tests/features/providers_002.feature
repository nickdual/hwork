Feature: Provider

  As a healthcare provider
  I want to store my information in Handyworks
  So that I can efficiently render services to my clients

  @javascript
  Scenario: show view allows use of resource switcher
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    When I go to Providers
    And I follow "Show" within the "Bob Robson MD" provider record
    And I wait for "8" seconds
    Then I should see "Switch Provider" within the data switcher
    And I should see "Provider Bob Robson MD"
    When I pick "Rob Bobson MD" within the data switcher
    And I wait for "8" seconds
    Then I should see "Provider Rob Bobson MD"


