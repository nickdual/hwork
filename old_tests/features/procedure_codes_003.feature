Feature: Procedure Codes

  As a Medical Service Provider
  I want to keep track of the services I provide
  And I want to keep track of the fees for those services
  So I need standard procedure codes to be supported

  @javascript
  Scenario: remove new fee schedule from an existing procedure code
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    Then a procedure_code should exist with name: "PC1"
    When I edit the procedure_code
    Then the "Name" field should contain "PC1"
    And the "procedure_code_fees_attributes_0_fee_schedule_attributes_label" field should contain "FS1"
    And the "procedure_code_fees_attributes_1_fee_schedule_attributes_label" field should contain "FS2"
    And I should not see "remove" within "#procedure_code_fees_table"
    When I follow "Add Fee Schedule"
    Then I should see css=".stop" within "#procedure_code_fees_table"
    When I follow "Cancel new fee schedule" within "#procedure_code_fees_table"
    And I wait for "2" seconds
    Then I should not see css=".stop" within "#procedure_code_fees_table"
