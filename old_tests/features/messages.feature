Feature: Messages

  As an office manager
  I would like to store message templates
  So that I can quickly generate things like mailings and notifications

  @pending
  Scenario: letters are configured under Clinic

  @pending
  Scenario: admin can create a new letter template

  @pending
  Scenario: staff user can view current letter templates

  @pending
  Scenario: creating a new letter template only requires a label to be supplied

  @pending
  Scenario: admin can edit an existing letter template

  @pending
  Scenario: admin can destroy an existing letter template

  @pending
  Scenario: letter parameters include date format

  @pending
  Scenario: letter parameters include signator

  @pending
  Scenario: letter parameters include patient adddress

  @pending
  Scenario: after updating a message you return to the messages list
