Feature: Appointment Scheduling

  As a office member for a service provider,
  I want to schedule, review and update appointments
  So that we can manage our service provider's time

  @javascript
  Scenario: weekly calendar view of appointments
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    Then I should see "Appointments"
    When I follow "Appointments"
    Then I should see "Weekly"
    And I should see "Daily"
    When I follow "Weekly"
    And I wait for "2" seconds
    Then I should see css="#appointment_views"
    And I should see css=".wc-container"
    And I should see "Sunday"
    And I should see "Monday"
    And I should see "Tuesday"
    And I should see "Wednesday"
    And I should see "Thursday"
    And I should see "Friday"
    And I should see "Saturday"

  @javascript
  Scenario: daily calendar view of appointments
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I follow "Appointments"
    Then I should see "Daily"
    When I follow "Daily"
    Then the day and date today will be displayed

  @javascript
  Scenario: day week toggle endurance test
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I follow "Appointments"
    And I toggle between "Weekly" and "Daily" for "100" times
    Then I should see "Appointments"

  @javascript
  Scenario: rooms are displayed as columns in the appointment calendar
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I follow "Appointments"
    And I follow "Daily"
    Then I should see "Room1"
    And I should see "Room2"
    And I should not see "Room3"

  @javascript @pending
  Scenario: room columns are displayed in alphabetical order

  @javascript
  Scenario: multiple day view options are available
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I follow "Appointments"
    Then I should see "Weekly"
    And I should see "5-day"
    And I should see "3-day"
    And I should see "Daily"

  @javascript @pending
  Scenario: appointment calendar should display 8AM hour by default.

  @javascript @pending
  Scenario: user is blocked from adding events during periods marked as unavailable

  #-----------------------------------------------------------------------------
  # Free Busy Tests
  #-----------------------------------------------------------------------------
  @javascript @pending
  Scenario: empty clinic schedule should display as all busy

  @javascript @pending
  Scenario: simple clinic schedule shows weekly availability

  @javascript @pending
  Scenario: exception event adds availability outside normal business hours

  @javascript @pending
  Scenario: exception event to make a room unavailable one day during regular business hours

  @javascript @pending
  Scenario: clicking on a busy time slot does not create a new appointment

  #-----------------------------------------------------------------------------
  # Calendar Navigation Tests
  #-----------------------------------------------------------------------------
  @javascript @pending
  Scenario: appointment calendar initially displays current date

  @javascript @pending
  Scenario: appointment calendar has a calendar control to navigate to different dates

  @javascript @pending
  Scenario: navigate to a different date with a 7-day view

  @javascript @pending
  Scenario: navigate to a different date with a 5-day view

  @javascript @pending
  Scenario: navigate to a different date with a 3-day view

  @javascript @pending
  Scenario: navigate to a different date with a 1-day view

  #-----------------------------------------------------------------------------
  # Calendar View Options
  #-----------------------------------------------------------------------------
  @javascript @pending
  Scenario: by default the calendar view is by rooms

  @javascript @pending
  Scenario: switch the calendar view from "by rooms" to "by providers"

  @javascript @pending
  Scenario: provider appointment color coded legend displayed

  @javascript @pending
  Scenario: user can deselect rooms and see columns disappear when viewed by room

  @javascript @pending
  Scenario: user can deselect rooms and see appointments disappear when viewed by provider

  @javascript @pending
  Scenario: user can deselect providers and see columns disappear when viewed by provider

  @javascript @pending
  Scenario: user can deselect providers and see appointments disappear when viewed by room

  #-----------------------------------------------------------------------------
  # Calendar Date Limits
  #-----------------------------------------------------------------------------
  @javascript @pending
  Scenario: calendar navigation is limited by future appointment limit
    # controls are disabled beyond the last allowed appointment day

  @javascript @pending
  Scenario: calenar navigation is limited by history appointment limit
    #controls are disabled before the oldest allowed historical appointment

  @javascript @pending
  Scenario: fetch a new date range from the server for future appointments

  @javascript @pending
  Scenario: fetch a new date range from the server from further in the past

  #-----------------------------------------------------------------------------
  # New Appointment Details Dialog
  #-----------------------------------------------------------------------------

  @javascript
  Scenario: current patient is the default when creating a new appointment
    Given I've setup: account, clinic, clinic_schedule, room, provider, provider_schedule, patient
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And "Patient001" is my current patient
    And I create a new appointment at 8:00AM
    Then I should see "Patient001" within the new appointment popup
    And I should see "Existing Patient" is checked

  @javascript @pending
  Scenario: patient additional information contents
    Then I should not see "Client Type:"
    Then I should see "D.O.B:"
    Then I should see "Spouse"

  @javascript @pending
  Scenario: user can open / close an appointment details dialog multiple times

  @javascript @pending
  Scenario: clicking on the calendar brings up the new appointment dialog

  @javascript @pending
  Scenario: appointment dialog can be navigated with next / previous buttons

  @javascript @pending
  Scenario: appointment dialog previous button does not appear on first step

  @javascript @pending
  Scenario: user can click on any previous tab in appointment details to return to that step

  @javascript @pending
  Scenario: a user with expired session making an appointment workflow request will be sent to the login screen

  @javascript @pending
  Scenario: closing and reopening the details dialog starts a new workflow
    # open workflow, see patient matches current patient
    # close workflow, and select a different patient
    # open another workflow and see the patient matches the new current patient

  @javascript @pending
  Scenario: appointment workflow updates server state on next/previous when there are changes

  @javascript @pending
  Scenario: when searching for a contact, an exact match must be selected

  @javascript @pending
  Scenario: user can specify duration and add notes to appointment
    # test changing the fields independently as this is a potential weakness
    # if only differences are transmitted to the server
    # ensure the values persist when doing a next / previous bounce

  @javascript @pending
  Scenario: appointment duration when min == max

  @javascript @pending
  Scenario: appointment duration defaults to service type duration

  @javascript @pending
  Scenario: user selects a different service after specifying incompatible duration

  @javascript @pending
  Scenario: cancel new appointment details dialog removes the event from the calendar
    # should remove the event from the calendar
    # try both ESC and pressing the X

  @javascript @pending
  Scenario: when viewed by room, appointment workflow does not include room step
    # because clicking on the calendar is already picking the room

  @javascript @pending
  Scenario: when viewed by provider, appointment workflow does not include provider step
    # because clicking on the calendar is already picking the provider
    #
  @javascript @pending
  Scenario: user must leave at least one provider selected in the appointments view

  @javascript @pending
  Scenario: user must leave at least one room selected in the appointments view
