Feature: Rooms

  As a manager of a medical practice office
  I want to ensure I know what rooms are scheduled for appointments
  So that I can efficiently use my service rooms

  Scenario: rooms are scoped by account
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Rooms
    Then I should see "Room1"
    And I should see "Room2"
    And I should not see "Room3"

  Scenario: admin can create a new room
    Given I am going to create a new room
    When I press "room_submit"
    Then I should see "Room was successfully created"
    And room should exist with name: "TestRoomA"

  Scenario: ONLY admin can create a new room
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    When I view the list of rooms
    Then I should not see "New Room"

  @pending
  Scenario: switcher only displays resources by clinic

  @pending
  Scenario: switcher default selected value matches current view

  Scenario: rooms assigned to clinics is not required by default
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Rooms
    And I follow "Edit" within the "Room1" room record
    Then I should not see css="#room_clinics_input"

  Scenario: rooms assigned to clinics is available with multiple locations
    Given a basic set of Handyworks data
    And a clinic with a different address
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Rooms
    And I follow "Edit" within the "Room1" room record
    Then I should see css="#room_clinics_input"

  @pending
  Scenario: after updating a room you return to the rooms list

  @wip
  Scenario: rooms have a primary provider specified for scheduling purposes
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And a room should exist with name: "Room1"
    When I edit the room
    Then I should see "Primary Provider"

