Feature: Provider

  As a healthcare provider
  I want to store my information in Handyworks
  So that I can efficiently render services to my clients

  Scenario: account admin users can create new Providers
    Given I am a Handyworks user with the "AccountAdmin" role
    And I login to Handyworks
    When I go to Providers
    Then I should see css="a[title='New Provider']"

  Scenario: ONLY account admin users can create new Providers
    Given I am a Handyworks user
    And I login to Handyworks
    When I go to Providers
    Then I should not see "New Provider"

  Scenario: users can see the current list of providers
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    When I go to Providers
    Then I should see "Rob Bobson MD"
    And I should see "Bob Robson MD"

  @pending
  Scenario: switcher only displays resources by account

  @pending
  Scenario: switcher default selected value matches current view

  Scenario: admin creates a provider
    Given I am going to create a new provider
    And I press "provider_submit"
    Then I should see "Provider was successfully created"

  Scenario: legacy id links are not present on the show page
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    When I go to Providers
    And I follow "Show" within ".provider_record"
    Then I should not see "Add Legacy ID"

  @pending
  Scenario: remove legacy id should confirm

  Scenario: scenario new legacy id labels are added to a provier on edit
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And provider should exist with signature_name: "Rob Bobson MD"
    And I add a new legacy_id_label "LIL4" to the current account
    When I go to Providers
    And I follow "Edit" within the "Rob Bobson MD" provider record
    Then the "provider_provider_legacy_id_labels_attributes_0_legacy_id_label_attributes_label" field should contain "LIL1"
    And the "provider_provider_legacy_id_labels_attributes_1_legacy_id_label_attributes_label" field should contain "LIL2"
    And the "provider_provider_legacy_id_labels_attributes_2_legacy_id_label_attributes_label" field should contain "LIL4"

  Scenario: editing a legacy id label when creating a provider
    Given I am going to create a new provider
    Then legacy_id_label should exist with label: "LIL1"
    When I fill in "provider_provider_legacy_id_labels_attributes_0_legacy_id_label_attributes_label" with "UPD_LIL1"
    And I press "provider_submit"
    Then I should see "Provider was successfully created"
    And the legacy_id_label's label should be "UPD_LIL1"
    And provider should exist with signature_name: "Dr. Test Tester"
    And legacy_id_label should be one of provider's legacy_id_labels

  Scenario: editing a legacy_id_label when updating a provider
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Providers
    And I follow "Edit" within the "Bob Robson MD" provider record
    And I wait for "2" seconds
    Then legacy_id_label should exist with label: "LIL1"
    And provider should exist with signature_name: "Bob Robson MD"
    When I fill in "provider_provider_legacy_id_labels_attributes_0_legacy_id_label_attributes_label" with "UPD_LIL1"
    And I press "provider_submit"
    Then I should see "Provider was successfully updated"
    And the legacy_id_label's label should be "UPD_LIL1"
    And legacy_id_label should be one of provider's legacy_id_labels

  Scenario: providers can see the account appointment types on their edit screen
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Providers
    And I follow "Edit" within the "Bob Robson MD" provider record
    Then I should see "Appointment Types"
    And I should see "Account Appointment Types"
    And I should see "OldService1"

  @javascript @icon
  Scenario: providers can specify appointment types
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Providers
    And I follow "Edit" within the "Bob Robson MD" provider record
    And I wait for "2" seconds
    Then I should see "Add Appointment Type"
    When I follow "Add Appointment Type"
    And I wait for "10" seconds
    And I type "NewService" within the new provider appointment type name
    And I press "provider_submit"
    Then I should see "Provider was successfully updated"
    And provider should exist with signature_name: "Bob Robson MD"
    And appointment_type should exist with name: "NewService"
    And appointment_type should be one of provider's appointment_types

  @javascript
  Scenario: providers can delete a custom appointment type
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And provider "Bob Robson MD" has appointment type "NewService1"

  @javascript @icon
  Scenario: providers can cancel adding a new appointment type
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Providers
    And I follow "Edit" within the "Bob Robson MD" provider record
    And I follow "Add Appointment Type"
    And I wait for "10" seconds
    Then I should see css="a.stop" within the new provider appointment type row
    When I follow "Cancel new appointment type" within the new provider appointment type row
    Then I should not see css="a.stop" within the appointment types list

  Scenario: providers can specify their appointment color
    Given I am going to create a new provider
    Then I should see "Appointment color"
    And I should see css="#appointment_color_picker"

  @javascript @pending
  Scenario: you can use a color wheel to change the providers appointment color

  @pending
  Scenario: after updating a provider you return to the providers list
