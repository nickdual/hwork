Feature: Provider

  As a healthcare provider
  I want to store my information in Handyworks
  So that I can efficiently render services to my clients

  @javascript
  Scenario: legacy id labels for providers have account scope
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Providers
    And I follow "New Provider"
    Then the "provider_provider_legacy_id_labels_attributes_0_legacy_id_label_attributes_label" field should contain "LIL1"
    And the "provider_provider_legacy_id_labels_attributes_1_legacy_id_label_attributes_label" field should contain "LIL2"
    And I should not see "LIL3"


