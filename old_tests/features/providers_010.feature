Feature: Provider

  As a healthcare provider
  I want to store my information in Handyworks
  So that I can efficiently render services to my clients


  @javascript
  Scenario: Bug #148 provider show view should reflect new legacy id labels
    Given I am going to create a new provider
    And I follow "Add Legacy ID"
    And I type "NEW_LIL" within the new legacy id label
    And I type "MMM-33" within the new legacy id value
    And I press "provider_submit"
    Then I should see "Provider was successfully created"
    When I pick "Rob Bobson MD" within the data switcher
    And I wait for "4" seconds
    Then I should see "Rob Bobson MD"
    And the "provider_provider_legacy_id_labels_attributes_2_legacy_id_label_attributes_label" field should contain "NEW_LIL"

