Feature: Procedure Codes

  As a Medical Service Provider
  I want to keep track of the services I provide
  And I want to keep track of the fees for those services
  So I need standard procedure codes to be supported


  @javascript
  Scenario: show view allows use of resource switcher
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "staff.login" and :password = "staff.pass"
    When I view the list of procedure_codes
    And I follow "Show" within the "PC1" procedure_code record
    And I wait for "4" seconds
    Then I should see "Switch Procedure code" within the data switcher
    And I should see "PC1"
    When I pick "PC2" within the data switcher
    And I wait for "2" seconds
    Then I should see "PC2"

