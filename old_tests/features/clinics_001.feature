Feature: Clinic Management

  As a health care service organization
  I want to have one or more Clinics defined in Handyworks
  So that I can model the actual running of my practice(s)

  @javascript
  Scenario: bugfix #24 billing address fields should not be displayed by default
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Clinics
    And I follow "New Clinic"
    Then I should not see "#billing_address_form_fields"
    And I should see "Address"


