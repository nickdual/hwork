Feature: Carrier HCFA Form Options

  As a medical service provider
  I want the ability to customize my HCFA forms for individual carriers
  So that I will be reimbursed in a timely manner when submitting claims to those carriers

  Scenario: carriers do not have custom hcfa options by default
    Given I am going to create a new carrier
    And I fill in "Plan name" with "NoHcfaCarrier"
    When I press "third_party_submit"
    Then I should see "ThirdParty was successfully created"
    And the "NoHcfaCarrier" insurance carrier should not have a carrier_hcfa_form_option

  @pending
  Scenario: new custom carrier hcfa options are based on the current account's HCFA options

  @pending
  Scenario: admin can edit an existing custom carrier hcfa option

  @pending
  Scenario: admin can view an existing custom carrier hcfa option

  @pending
  Scenario: admin can destroy an existing customer carrier hcfa option from 'show' view

  @pending
  Scenario: admin can destroy an existing customer carrier hcfa option from 'edit' view

  @pending
  Scenario: ONLY admin can create a new custom carrier hcfa option

  @pending
  Scenario: when a carrier is deleted, so is the associated hcfa_form_options
