Feature: Edit View Popup Displays

  As a Handyworks user
  I want to quickly access additional data for a selected resource
  And I want to be able to edit the data
  And I want to submit changes easily
  So that I can maintain a productive workflow.

  @pending
  Scenario: single clicking on a record displays the lightbox style popup form

  @pending
  Scenario: content overflowing the lightbox display is accessible via scroll bar

  @pending
  Scenario: updates are saved automatically when the lightbox popup is closed
    # test both clicking the close and pressing the escape key.

  @pending
  Scenario: delete button tooltip says delete and not destory

  @pending
  Scenario: pressing CANCEL button closes the popup and does not preserve any changes.
