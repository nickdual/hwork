Feature: Rooms

  As a manager of a medical practice office
  I want to ensure I know what rooms are scheduled for appointments
  So that I can efficiently use my service rooms


  @javascript
  Scenario: edit view allows use of resource switcher
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Rooms
    And I follow "Edit" within ".room_record"
    And I wait for "4" seconds
    Then I should see "Switch Room" within the data switcher
    And I should see "Editing Room Room1"
    When I pick "Room2" within the data switcher
    And I wait for "2" seconds
    Then I should see "Editing Room Room2"

