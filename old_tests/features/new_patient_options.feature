Feature: New Patient Options

  As a clinic owner
  I want to customize my clinic's new patient options
  So that I will enjoy working with my clinic through Handyworks

  @pending
  Scenario: after creating a clinic, I see the preferences

  @pending
  Scenario: allows multiple insurance carrier assignment selections

  @pending
  Scenario: default value used for overdue charge percentage

  @pending
  Scenario: default value used for place of service

  @pending
  Scenario: account admins can edit preferences on clinics
    Given I am a Handyworks user with the "AccountAdmin" role
    And I login to Handyworks
    When I view the list of clinics
    # need to follow edit for existing clinic, then verify "New Patient Options" fields

  @pending
  Scenario: ONLY account admins can edit preferences on clinics

  @pending
  Scenario: a new clinic allows any provider to be the default provider
    Given a basic set of Handyworks data
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I go to Clinics
    # need to set and check the default provider field to complete this test.

  # BLS - in the scenario below the final step fails because it is specifically looking
  #       for the "selected" attribute to be set for some option in the selection list;
  #       however, the desired option is what is selected on the screen
  #       2011-12-12 - would need to be updated
  @pending
  Scenario: if there is only one provider for an account, it will be the default provider
    Given a basic set of Handyworks data
    And a provider is destroyed with signature_name: "Rob Bobson MD"
    And I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    When I go to Clinics
