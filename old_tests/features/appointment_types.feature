Feature: Appointment Types

  As a appointment provider
  I want to offer multiple appointments which may have different scheduling parameters
  So that I can effectively manage and maximize my appointment schedule

  Scenario: appointment types are managed through schedule setup
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I go to Schedule setup
    Then I should see "Appointment Types"

  @javascript
  Scenario: adding a new Appointment type
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I go to Schedule setup
    Then I should see "Add Appointment Type"
    When I follow "Add Appointment Type"
    And I wait for "3" seconds
    And I type "NewService" within the new appointment type name
    And I press "calendar_submit"
    Then I should see "Schedule was successfully updated"
    And account should exist with name: "Account1"
    And appointment_type should exist with name: "NewService"
    And account's calendar should exist
    And appointment_type should be one of calendar's appointment_types

  
  Scenario: appointment types are scoped by account
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I go to Schedule setup
    Then I should see xpath="//td//input[@value='OldService1']"
    And I should not see xpath="//td//input[@value='OldService2']"

  @javascript
  Scenario: deleting a appointment type
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I go to Schedule setup
    Then I should see xpath="//td//input[@value='OldService1']"
    And I should see css=".delete_appointment_type"
    # verify cancel option works
    When within the "OldService1" appointment type row, I follow "Delete complete appointment type" and dismiss popup containing "This will delete the appointment type, but not any appointments. Are you sure?"
    And I wait for "10" seconds
    Then I should see xpath="//td//input[@value='OldService1']"
    # verify accept option
    When within the "OldService1" appointment type row, I follow "Delete complete appointment type" and confirm popup
    Then I should see "Appointment type has been successfully destroyed"
    And I should not see xpath="//td//input[@value='OldService1']"

  @javascript
  Scenario: cancel adding a new appointment type
    Given a basic set of Handyworks data
    When I login to Handyworks with :login = "admin.login" and :password = "admin.pass"
    And I go to Schedule setup
    Then I should see "Add Appointment Type"
    When I follow "Add Appointment Type"
    And I wait for "3" seconds
    Then I should see css="a.stop" within the new appointment type row
    When I follow "Cancel new appointment type" within the new appointment type row
    # selector for the new appointment type row.
    Then I should not see css="a.stop" within the new appointment type row


