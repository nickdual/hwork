Feature: SSL Encryption

  As a consumer of an internet hosted medical software service
  I want the entire site to be SSL encrypted
  So that I know the transactions are secure between browsers and the server.

  Scenario: SSL Encryption Seal should be displayed
    Given I am on the home page
    Then I should see css="#ssl_certification_seal"
