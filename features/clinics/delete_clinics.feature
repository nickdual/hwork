Feature: Deleting a Clinic

Background:
  Given I am an account owner
    And I have a clinic "Clinic1"

@javascript
Scenario: The last clinic can't be deleted
   When I navigate to "Location / Clinics" under "User Files"
   Then I should not find ".clinic-delete" within "#clinic-1"
   When I have a clinic "Clinic2"
    And I navigate to "Location / Clinics" under "User Files"
   Then I should find ".clinic-delete" within "#clinic-1"
   When I follow "Delete this record?" within "#clinic-2"
    And I confirm popup
   Then I should not find ".clinic-delete" within "#clinic-1"
   
