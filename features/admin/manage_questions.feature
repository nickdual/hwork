Feature: Manage FAQ Questions
  In order to provide current and helpful support to site visitors
  As an admin
  I would like to be able to perform CRUD operations on questions.

  Scenario: There's a menu option for question management
    Given I am an admin user
    Then I should see "Questions"

##Scenario: The index includes all questions
##  Given I am an admin user
##   And I have 2 published questions
##   And I have 2 draft questions
##  When I go to the questions index
##  Then I should see 4 questions in the index


