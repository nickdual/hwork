Feature: Resque Web
  In order to ensure the application is running expected processes.
  An admin
  Should be able to access the resque web interface.

  Scenario: Admin users access resque web interface
    Given I am an admin user
    When I go to the resque web 
    Then I should see "Queues"
    And I should see "Workers"

  Scenario: Admin users access job processing schedule
    Given I am an admin user
    When I go to the resque web 
    Then I should see "Schedule"
    And I should see "Delayed"

  # TODO : implement a test to verify the non-admin is not able to access the /admin/resque interface.
  @allow-rescue
  Scenario: 
    Given I am not an admin user
    And I am simulating a remote request
    When I try to go to the resque web 
    Then this scenario is pending
    Then I should see "Routing Error"
