Feature: Appointment Type Providers

Background:
  Given I am an account owner
    And I have a provider "Dr. Doc1"

@javascript
Scenario: Single provider provides all services
    Bug #283 : Error when creating an appointment type for account with single provider
  When I navigate to "Appointment Types" under "User Files"
   And I follow "New Appointment Type"
  Then I should not see "Which providers offer this service"
   And I should not see "Dr. Doc1"
  When I fill in "Describe this Service" with "Some Service"
   And I press "Save"
  Then I should see a success message
   And I should see "Some Service" within "#appointment_types_list"
   And appointment type "Some Service" should have "Dr. Doc1" provider

@javascript
Scenario: Multiple providers include provider association
  Given I have a provider "Dr. Doc2"
   When I navigate to "Appointment Types" under "User Files"
    And I follow "New Appointment Type"
   Then I should see "Which providers offer this service"
    And I should see "Dr. Doc1"
   When I fill in "Describe this Service" with "Some Service"
    And I press "Save"
   Then I should see a success message
    And I should see "Some Service" within "#appointment_types_list"
    And appointment type "Some Service" should not have "Dr. Doc1" provider
