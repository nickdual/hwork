Feature: Deleting an Appointment Type
  When services are no longer offered
  As a Handyworks Account Admin
  I want to delete the appointment type record.

  Background:
    Given I have a practice "Happy Practice" for "Dr. Smith"
      And I am configured to provide "GoodService" service in "Room1" room

  @javascript
  Scenario: Deleting an appointment type after visiting the Calendar
    When I follow "Calendar"
     And I navigate to "Appointment Types" under "User Files"
    Then I should see "GoodService" within "#appointment_types_list"
    When I follow "Delete this record?" within "#appointment_types_list"
     And I confirm popup
    Then I should not see "GoodService" within "#appointment_types_list"

  @javascript
  Scenario: Basic deleting an appointment type
    When I navigate to "Appointment Types" under "User Files"
    Then I should see "GoodService" within "#appointment_types_list"
    When I follow "Delete this record?" within "#appointment_types_list"
     And I confirm popup
    Then I should not see "GoodService" within "#appointment_types_list"

