Feature: Public Navigation Menu
  In order to keep users engaged with the web application.
  As an anonymous user
  Should have access to site information through a navigation menu.

  Scenario: Provides a List of Features
    Given I am not logged in
    When I am on the home page
    Then I should see "Features" within the navigation menu

  Scenario: Provides a Contact Function
    Given I am not logged in
    When I am on the home page
    Then I should see "Contact" within the navigation menu

  Scenario: Provides acess to Support
    Given I am not logged in
    When I am on the home page
    Then I should see "Support" within the navigation menu
