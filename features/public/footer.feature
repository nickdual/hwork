Feature: Footer
  In order to market the site effectively
  As an anonymous user
  I want to be able learn about Handyworks and get my questions answered.

  Background:
    Given I am not logged in
      And I am on the home page

  Scenario: Copyright notice is displayed.
     Then I should see "©" within the footer

  Scenario: Contact Form is displayed.
     Then I should see "Contact Us" within the footer
      And I should see the following form fields:
          |form_field|field_type|
          |Name      |text      |
          |Email     |text      |
          |Comments  |textarea  |

  @javascript
  Scenario: Contact Form Sending
     When I fill in the following:
          |Name      |Bob        |
          |Email     |jj@bob.com |
          |Comments  |Hello there|
      And I press "Send"
     Then I should see "Contact was successfully sent"
      And "abc@gmail.com" should receive an email

  @javascript
  Scenario: Contact Form Invalid Input
     When I fill in the following:
          |Name      |Bob        |
          |Email     |bad_address|
          |Comments  |Hello there|
      And I press "Send"
     Then I should see "Contact was not successfully sent"
      And "abc@gmail.com" should receive no emails
