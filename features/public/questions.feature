Feature: Public Support Questions
  In order to address common questions about Handyworks
  As a user
  I would like to see a list of frequently asked questions

  Background:
    Given I am not logged in

  Scenario: There are no questions published
    Given there are no questions
    When I am on the faq page
    Then I should see "No current questions."

  Scenario: There is a published question
    Given I have 1 published question
    When I am on the faq page
    Then I should see 1 question

  Scenario: There are multiple published questions
    Given I have 2 published questions
    When I am on the faq page
    Then I should see 2 questions

  Scenario: There is a draft question
    Given I have 1 published questions
      And I have 1 draft questions
    When I am on the faq page
    Then I should see 1 question
