Feature: Public Contact Us Page

  Background:
    Given I am not logged in
      And I am on the contact page

  Scenario: Contact Form is displayed.
     Then I should see the following form fields within "#contact":
          |form_field|field_type|
          |Name      |text      |
          |Email     |text      |
          |Phone     |text      |
          |City      |text      |
          |Comment   |textarea  |

  @javascript
  Scenario: Contact Form Sending
     When I fill in the following within "#contact":
          |Name      |Bob         |
          |Email     |jj@bob.com  |
          |Phone     |999-888-7777|
          |City      |Somewhere   |
          |Comments  |Hello there |
      And I press "Send" within "#contact"
     Then I should see "Contact was successfully sent"
      And "abc@gmail.com" should receive an email

  @javascript
  Scenario: Contact Form Invalid Input
     When I fill in the following within "#contact":
          |Name      |Bob         |
          |Email     |jj@bob.com  |
          |Phone     |999-888-7777|
          |City      |Somewhere   |
          |Comments  |            |
      And I press "Send" within "#contact"
     Then I should see "Contact was not successfully sent"
      And "abc@gmail.com" should receive no emails
