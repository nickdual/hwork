Feature: User signs up with stripe
  Note: you must have stripe setup with basic plan
  for these tests to run correctly.

  Background:
    Given There is a basic plan
    And I am on the home page
    When I subscribe for basic
    Then I should see "Basic Subscription Plan"

  @javascript
  Scenario: With valid card data
    Then this scenario is pending
    Given I fill in the following:
      | user_login                 | Testy McUserson   |
      | user_email                 | testy@testing.com |
      | user_password              | secret_password   |
      | user_password_confirmation | secret_password   |
      | Credit Card Number         | 4242424242424242  |
      | card_code                  | 111               |
    Then I select "5 - May" as the "month"
    And I select "2015" as the "year"
    When I press "Sign up"
    Then I'll be on the "content basic" page
    And I should see a successful sign up message

  @javascript
  Scenario: With invalid card number
    Then this scenario is pending
    Given I fill in the following:
      | user_login                 | Testy McBadCard   |
      | user_email                 | testy@testing.com |
      | user_password              | secret_password   |
      | user_password_confirmation | secret_password   |
      | Credit Card Number         | 5555555555555     |
      | card_code                  | 111               |
    Then I select "1 - January" as the "month"
    And I select "2016" as the "year"
    When I press "Sign up"
    Then I'll be on the new basic user registration page
    And I should see "Your card number is incorrect"

  @javascript
  Scenario: With invalid card security code
    Then this scenario is pending
    Given I fill in the following:
      | user_login                 | Testy McBadCode   |
      | user_email                 | testy@testing.com |
      | user_password              | secret_password   |
      | user_password_confirmation | secret_password   |
      | Credit Card Number         | 4242424242424242  |
      | card_code                  | 6                 |
    Then I select "10 - October" as the "month"
    And I select "2016" as the "year"
    When I press "Sign up"
    Then I'll be on the new basic user registration page
    And I should see "Your card's security code is invalid"

  @javascript
  Scenario: With declined card
    Then this scenario is pending
    Given I fill in the following:
      | user_login                 | Testy McDecline   |
      | user_email                 | testy@testing.com |
      | user_password              | secret_password   |
      | user_password_confirmation | secret_password   |
      | Credit Card Number         | 4000000000000002  |
      | card_code                  | 111               |
    Then I select "10 - October" as the "month"
    And I select "2016" as the "year"
    When I press "Sign up"
    Then I'll be on the "user registration" page
    And I should see "declined"


