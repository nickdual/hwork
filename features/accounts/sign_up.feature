Feature: Sign up
  In order to get access to protected sections of the site
  As a user
  I want to be able to sign up

  Background:
    Given I am not logged in

  @javascript
  Scenario: User signs up with valid data
    Given There is a basic plan
    When I sign up with valid user data
    Then I should see a successful sign up message

  @javascript
  Scenario: User signs up with invalid email
    Given There is a basic plan
    When I sign up with an invalid email
    Then I should see an invalid email message

  @javascript
  Scenario: User signs up without a subscription plan
    When I sign up without a subscription plan
    Then I'll be on the "plans" page
