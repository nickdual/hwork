Feature: Account Confirmation
  In order to ensure user's intention to use Handyworks
  The System
  Shall confirm a new account subscription

  Background:
    Given I am not logged in
    And There is a basic plan
    When I subscribe for basic
    Then I should see "Basic Subscription Plan"

  Scenario: confirmation email is sent

  Scenario: a valid confirmation token displays confirmation form

  Scenario: invalid token displays error message

  @javascript
  Scenario: Confirmation without password
    Then this scenario is pending
    When I sign up without a password
    Then I should see a missing password message

  @javascript
  Scenario: Confirmation without password confirmation
    Then this scenario is pending
    When I sign up without a password confirmation
    Then I should see a missing password confirmation message

  @javascript
  Scenario: Confirmation with mismatched password and confirmation
    Then this scenario is pending
    When I sign up with a mismatched password confirmation
    Then I should see a mismatched password message

