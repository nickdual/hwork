Feature: Accounts In Cancelled State
  When my account has been cancelled
  As a user
  I would like to be informed and have a grace period during which re-activation is possible.

  @javascript
  Scenario: Cancelled accounts can't login
    Given I am an account owner
    When I cancel my account
     And I log in
    Then I should see "Your account has been cancelled."
     And I should be signed out


