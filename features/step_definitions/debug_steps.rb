When "I pause" do
  $stdout.puts "Press enter to continue..."
  $stdout.flush
  $stdin.gets
end

When "I stop here" do
  ask('Press enter to continue...')
end
