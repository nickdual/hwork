
Given /^there are no questions$/ do
  Question.all.each do |question|
    question.destroy
  end
end

Given /^I have (\d+) (.*) questions?$/ do |question_cnt, question_state|
  @questions = []
  for i in (0..question_cnt.to_i)
    @questions << FactoryGirl.create(:question,
                                     title: "Question Title #{question_state} #{i}", 
                                     body: "Question Body #{i}",
                                     state: question_state)
  end
end

Then /^I should see (\d+) questions?$/ do |question_cnt|
  for i in (0..question_cnt.to_i)
    page.should have_content("Question Title published #{i}")
  end
end
