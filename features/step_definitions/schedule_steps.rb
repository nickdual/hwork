When /^I drag Monday block at 8:00 AM to the top$/ do
  target = page.find('td.wc-day-column-header.wc-day-2')
  time = page.find('div.wc-day-column-inner.day-2').find('div.wc-cal-event div.wc-time', :text => "08:00 am to 12:00 pm")
  time.drag_to(target)
end
