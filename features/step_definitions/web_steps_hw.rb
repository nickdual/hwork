#When /^I should find "([^"]*)"$/ do |selector|
#  find(selector)
#end

Then /^I should see a(?:|n) ([^ ]*) message$/ do |message_type|
  find(".#{message_type}")
end

When /^(?:|I )set the fancy editor "([^"]*)" with "([^"]*)"$/ do |field, value|
  page.execute_script("CKEDITOR.instances['message_body'].setData('#{value}');")
end

When /^I confirm popup$/ do
  page.driver.browser.switch_to.alert.accept    
end

When /^I dismiss popup$/ do
  page.driver.browser.switch_to.alert.dismiss
end

Then /^I should see the following form fields:$/ do |table|
  table.hashes.each do |row|
    f = find_field(row['form_field'])
    if row['field_type'] == 'text'
      f.tag_name.should == 'input'
      f[:type].should == 'text'
    elsif row['field_type'] == 'textarea'
      f.tag_name.should == 'textarea'
    end
  end
end

#-------------------------------------------------------------------------------
# css elements steps
#-------------------------------------------------------------------------------
Then /^(?:|I )should find "([^"]*)"$/ do |css_match|
  if page.respond_to? :should
    page.should have_selector(css_match)
  else
    assert page.has_selector?(css_match)
  end
end

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
Then /^(?:|I )should not find "([^"]*)"$/ do |css_match|
  if page.respond_to? :should
    page.should have_no_selector(css_match)
  else
    assert page.has_no_selector?(css_match)
  end
end
