### UTILITY METHODS ###

def create_visitor
  @visitor ||= { :login => "Testy McUserton", :email => "example@example.com",
    :password => "please", :password_confirmation => "please"}
end

def find_user
  @user ||= User.first conditions: {:email => @visitor[:email]}
end

def create_unconfirmed_user
  create_visitor
  delete_user
  sign_up
  visit '/users/sign_out'
end

#-------------------------------------------------------------------------------
# creates a user / account / subscription with 'basic' plan access.
#-------------------------------------------------------------------------------
def create_user
  create_visitor
  delete_user
  @user = FactoryGirl.create(:subscribed_user, email: @visitor[:email])
end

def create_account_owner
  user = create_user
  user.add_role(:AccountAdmin)
  @account = user.account
end

#-------------------------------------------------------------------------------
# creates an "owner" / account / subscription with 'basic' plan access.
#-------------------------------------------------------------------------------
def create_admin_user
  create_visitor
  delete_user
  @user = FactoryGirl.create(:subscribed_user, email: @visitor[:email])
  @user.add_role(:ApplicationAdmin)
end

def delete_user
  @user ||= User.first conditions: {:email => @visitor[:email]}
  @user.destroy unless @user.nil?
end

def sign_up
  delete_user
  visit '/users/sign_up/?plan=1'
  #fill_in "user[login]", :with => @visitor[:login]
  fill_in "user[email]", :with => @visitor[:email]
  #fill_in "user_password", :with => @visitor[:password]
  #fill_in "user_password_confirmation", :with => @visitor[:password_confirmation]
  click_button "Sign up"
  find_user
end

def sign_in
  visit '/users/sign_in'
  fill_in "user[email]", :with => @visitor[:email]
  fill_in "Password", :with => @visitor[:password]
  click_button "Sign In"
end

#-------------------------------------------------------------------------------
# log in the current user information stored in @visitor hash.
#-------------------------------------------------------------------------------
When /^I log in$/ do
  visit destroy_user_session_path
  sign_in
end

### GIVEN ###
Given /^I am not logged in$/ do
  visit destroy_user_session_path
end

Given /^I am logged in$/ do
  create_user
  sign_in
end

Given /^I am an admin user$/ do
  create_admin_user
  sign_in
end

Given /^I am an account owner$/ do
  create_account_owner
  sign_in
end

Given /^I am not an admin user$/ do
  step "I am logged in"
end

Given /^I exist as a user$/ do
  create_user
end

Given /^I do not exist as a user$/ do
  create_visitor
  delete_user
end

Given /^I exist as an unconfirmed user$/ do
  create_unconfirmed_user
end

### WHEN ###
When /^I sign in with valid credentials$/ do
  create_visitor
  sign_in
end

When /^I sign out$/ do
  visit '/users/sign_out'
end

When /^I sign up with valid user data$/ do
  create_visitor
  sign_up
end

When /^I sign up with an invalid email$/ do
  create_visitor
  @visitor = @visitor.merge(:email => "notanemail")
  sign_up
end

When /^I sign up without a password confirmation$/ do
  create_visitor
  @visitor = @visitor.merge(:password_confirmation => "")
  sign_up
end

When /^I sign up without a password$/ do
  create_visitor
  @visitor = @visitor.merge(:password => "")
  sign_up
end

When /^I sign up without a subscription plan$/ do
  visit '/users/sign_up'
end

When /^I sign up with a mismatched password confirmation$/ do
  create_visitor
  @visitor = @visitor.merge(:password_confirmation => "please123")
  sign_up
end

When /^I return to the site$/ do
  visit '/'
end

When /^I sign in with a wrong email$/ do
  @visitor = @visitor.merge(:email => "wrong@example.com")
  sign_in
end

When /^I sign in with a wrong password$/ do
  @visitor = @visitor.merge(:password => "wrongpass")
  sign_in
end

When /^I change my email address$/ do
  click_link "session-menu"
  click_link "Edit Profile"
  fill_in "user_email", :with => "different@example.com"
  fill_in "user_current_password", :with => @visitor[:password]
  click_button "Update"
end

When /^I delete my account$/ do
  visit '/admin/subscription/edit'
  click_link "Cancel my subscription"
  page.driver.browser.switch_to.alert.accept
end

When /^I subscribe for basic$/ do
  visit '/users/sign_up/?plan=1'
end

### THEN ###
Then /^I should be signed in$/ do
  click_link "session-menu"
  page.should have_content "Sign Out"
  page.should_not have_content "Login"
end

Then /^I should be signed out$/ do
  page.should have_content "Login"
  page.should_not have_content "Logout"
end

##Then /^I should see "(.*?)"$/ do |text|
##  page.should have_content text
##end

Then /^I'll be on the "([^"]*)" page$/ do |path_name|
  current_path.should == send("#{path_name.parameterize('_')}_path")
end

Then /I'll be on the new basic user registration page$/ do
  current_path_with_args.should == '/users/sign_up/?plan=1'
end

Then /^I see an unconfirmed account message$/ do
  page.should have_content "You have to confirm your account before continuing."
end

Then /^I see a successful sign in message$/ do
  page.should have_content "Signed in: Welcome back!"
end

Then /^I should see a successful sign up message$/ do
  page.should have_content "Welcome! You have successfully signed up."
end

Then /^I should see an invalid email message$/ do
  page.should have_content "Please enter a valid email address."
end

Then /^I should see a missing password message$/ do
  page.should have_content "can't be blank"
end

Then /^I should see a missing password confirmation message$/ do
  page.should have_content "doesn't match confirmation"
end

Then /^I should see a mismatched password message$/ do
  page.should have_content "doesn't match confirmation"
end

Then /^I should see a missing subscription plan message$/ do
  page.should have_content "Please select a subscription plan below"
end

Then /^I should see a signed out message$/ do
  page.should have_content "Signed out: See ya real soon!"
end

Then /^I see an invalid login message$/ do
  page.should have_content "Invalid email or password."
end

Then /^I should see an account edited message$/ do
  page.should have_content "You updated your account successfully"
end

Then /^I should see an account deleted message$/ do
  page.should have_content "We have cancelled your subscription."
end

Then /^I should see my name$/ do
  create_user
  page.should have_content @user[:login]
end
