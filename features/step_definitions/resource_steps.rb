When /^there (?:|is|are) (\d*) clinic(?:|s)$/ do |field|
  FactoryGirl.create(:clinic, :account => @user.account)
end

When /^(?:|I )create a new patient "([^"]*)"$/ do |full_name|
  first_name, last_name = full_name.split(' ')
  step "I fill in \"Last name\" with \"#{last_name}\""
  step "I fill in \"patient[contact_attributes][first_name]\" with \"#{first_name}\""
  step 'I follow "Cancel new phone"'
  step 'I press "Save"'
end

Given /^I have a room "(.*?)"$/ do |room_name|
  @room = FactoryGirl.create(:room, :name => room_name, :account => @account)
end

Given /^I have an appointment type "(.*?)"$/ do |service_name|
  @appointment_type = FactoryGirl.create(:appointment_type, :name => service_name, :account => @account)
end

Given /^I have a provider "(.*?)"$/ do |signature_name|
  @provider = FactoryGirl.create(:provider, :signature_name => signature_name, :account => @account)
end

Given /^I have a typical schedule configuration$/ do
  # TODO : this logic should be implicit in the models for "singular" cases (1 doc, 1 room, 1 service)
  @appointment_type.providers << @provider
  @appointment_type.rooms << @room
  @room.appointment_type = @appointment_type
  @room.providers << @provider
end

Given /^I have a patient "(.*?)"$/ do |name|
  # OPTIMIZE : There should be some way to pass the transient name properties value through FactoryGirl directly.
  @patient = FactoryGirl.create(:patient, :account => @account, :clinic => @clinic)
  fnl_name = name.split(' ')
  @patient.contact.first_name = fnl_name[0]
  @patient.contact.last_name = fnl_name[1]
  @patient.save!
end

def create_test_appt(appt_info)
  contact = ContactPatient.find_by_last_name(appt_info[:patient])
  provider = Provider.find_by_signature_name(appt_info[:provider])
  room = Room.find_by_name(appt_info[:room])
  appointment_type = AppointmentType.find_by_name(appt_info[:service])
  hour_min = appt_info[:time].split(':')
  start = Time.now.at_beginning_of_day + hour_min[0].to_i.hours + hour_min[1].to_i.minutes
  event = FactoryGirl.build(:event,
                            :start_time => start,
                            :start_date => start,
                            :title => "Appt #{appt_info[:patient]}")
  @appointment = FactoryGirl.create(:appointment,
                                    :contact => contact,
                                    :provider => provider,
                                    :calendar => provider.try(:account).try(:calendar),
                                    :room => room,
                                    :appointment_type => appointment_type,
                                    :event => event)
end

Given /^I have the following appointments today:$/ do |appointments|
  # appointments is a Cucumber::Ast::Table
  appointments.hashes.each do |appt|
    create_test_appt(appt)
  end
end

Given /^I have a clinic "(.*?)"$/ do |clinic_name|
  if @account
    @clinic = FactoryGirl.create(:clinic, :account => @account)
  else
    @clinic = FactoryGirl.create(:clinic_with_account)
  end
  @clinic.contact.company_name = clinic_name
  @clinic.save!
end

When /^I select the "(.*?)" appointment$/ do |appointment_title|
  # OPTIMIZE find a way to click on an appointment without the jQuery JavaScript
  page.execute_script(%Q@jQuery('.wc-cal-event .event-title[title="#{appointment_title}"]').closest('.wc-cal-event').click();@)
end

When /^I delete the "(.*?)" appointment$/ do |appointment_title|
  # OPTIMIZE find a way to click on delete an appointment without the jQuery JavaScript
  page.execute_script(%Q@jQuery('.wc-cal-event span[title="#{appointment_title}"]').closest('.wc-time').find('.wc-cal-event-delete').first().click();@)
end

Given /^I have a practice "([^"]*?)" for "([^"]*?)"$/ do |clinic_name, provider_name|
  step %{I am an account owner}
  step %{I have a clinic "#{clinic_name}"}
  step %{I have a provider "#{provider_name}"}
end

#-------------------------------------------------------------------------------
# This step will either lookup or create the requested room / appointment_type
#-------------------------------------------------------------------------------
Given /^I am configured to provide "([^"]*?)" service in "([^"]*?)" room$/ do |appt_type, room_name|
  room = @account.rooms.find_by_name(room_name)
  step %{I have a room "#{room_name}"} unless room
  @room = room if room
  service = @account.appointment_types.find_by_name(appt_type)
  step %{I have an appointment type "#{appt_type}"} unless service
  @appointment_type = service if service
  #step %{I have a typical schedule configuration}
  @appointment_type.rooms << @room
  @room.appointment_type = @appointment_type
end

Then /^there should be a link to "(.*?)" address$/ do |clinic_name|
  clinic = @account.clinics.find_by_name(clinic_name)
  step %{I should see "#{clinic.contact.physical_address.street}" within "ul.locations_list"} 
end

Given /^"(.*?)" is at the same location as "(.*?)"$/ do |clinic2, clinic1|
  clinic1 = @account.clinics.find_by_name(clinic1)
  clinic2 = @account.clinics.find_by_name(clinic2)
  a1 = clinic1.contact.physical_address
  a2 = clinic2.contact.physical_address
  a2.street = a1.street
  a2.street2 = a1.street2
  a2.city = a1.city
  a2.state = a1.state
  a2.zip = a1.zip
  a2.save!
end

Given /^I only have (\d+) service$/ do |count_s|
  count = count_s.to_i
  c = @account.appointment_types.count
  if c > count then
    for i in (count+1)..c
      @account.appointment_types.last.destroy
    end
  elsif c < count then
    for i in c..(count)
      FactoryGirl.create(:appointment_type, :account => @account)
    end
  end
end

Given /^Service "(.*?)" has been configured as not public$/ do |service_name|
  service = @account.appointment_types.find_by_name(service_name)
  service.is_public = false
  service.save!
end

Given /^Provider "(.*?)" provides "(.*?)"$/ do |provider_name, service_name|
  provider = @account.providers.find_by_signature_name(provider_name)
  service  = @account.appointment_types.find_by_name(service_name)
  service.providers << provider
  service.save!
end

Given /^I do not have any appointments scheduled$/ do
  Appointment.all.each do |appt|
  end
end

Then /^I should see the following time slots available:$/ do |time_slots|
  # table is a Cucumber::Ast::Table
  time_slots.hashes.each do |time|
    step %{I should see "#{time[:time_slot]}" within ".times_list"}
  end
end

Then /^room "(.*?)" should have "(.*?)" provider$/ do |room, provider|
  r = Room.find_by_name(room)
  p = Provider.find_by_signature_name(provider)
  r.providers.should include(p)
end

Then /^room "(.*?)" should not have "(.*?)" provider$/ do |room, provider|
  r = Room.find_by_name(room)
  p = Provider.find_by_signature_name(provider)
  r.providers.should_not include(p)
end

Then /^appointment type "(.*?)" should have "(.*?)" provider$/ do |appt_type, provider|
  a = AppointmentType.find_by_name(appt_type)
  p = Provider.find_by_signature_name(provider)
  a.providers.should include(p)
end

Then /^appointment type "(.*?)" should not have "(.*?)" provider$/ do |appt_type, provider|
  a = AppointmentType.find_by_name(appt_type)
  p = Provider.find_by_signature_name(provider)
  a.providers.should_not include(p)
end
