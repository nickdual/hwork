
Given /^I navigate to "([^"]*)" under "([^"]*)"$/ do |item, menu|
  click_link menu
  click_link item
end
