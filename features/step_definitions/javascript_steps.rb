# from http://www.lindelauf.com/waiting-for-jquery-animations-to-finish-in-cucumbercapybaraselenium-specs/
#  NOTE : wait_until was deprecated and removed in Capybara 2.0
When /^I wait until all animations have finished$/ do 
  wait_until do    
    page.evaluate_script('$(":animated").length') == 0  
  end
end
