When /^(?:|I )try to go to (.+)$/ do |page_name|
  begin
    get path_to(page_name)
  rescue => @error
  end
end

Given /^I am simulating a remote request$/ do
  header "REMOTE-ADDR", "10.0.1.1"
end

Then /^this scenario is pending/ do
  pending
end
