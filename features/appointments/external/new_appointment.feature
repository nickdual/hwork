Feature: New (public) Appointment
  As a customer of Handyworks provider
  I want to schedule an appointment
  So that I can conveniently access the provider's services

  Background:
    Given I have a practice "Clinic1" for "Dr. Doc1"
      And I am configured to provide "Service1" service in "Room1" room
      And Provider "Dr. Doc1" provides "Service1"
      And I am not logged in

  @javascript
  Scenario: Publicly accessible appointment scheduling interface, 1 Clinic
    When I go to the public appointment scheduling page
    Then I should see "Clinic1 Appointment"

  @javascript
  Scenario: Publicly accessible appointment scheduling, multiple locations
    Given I have a clinic "Clinic2"
      And I am configured to provide "Service2" service in "Room1" room
    When I go to the public appointment scheduling page
    Then I should see "Pick a Location"
     And there should be a link to "Clinic1" address
     And there should be a link to "Clinic2" address
    When I follow "Clinic2"
    Then I should see "Clinic2 Appointment"
     And I should see "Select a Service"

  @javascript
  Scenario: New Appointment started from the clinic level
    Given I have a clinic "Clinic2"
    When I go to the public appointment scheduling page for "Clinic1"
    Then I should see "Clinic1 Appointment"

  @javascript
  Scenario: Multiple clinics at a single physical address 
    Given I have a clinic "Clinic3"
      And "Clinic3" is at the same location as "Clinic1"
     When I go to the public appointment scheduling page
     Then I should see "Clinic1 Appointment"

  @javascript
  Scenario: If only one service, don't ask the user to choose
    Given I only have 1 service
      And I have a provider "Dr. Doc2"
      And Provider "Dr. Doc2" provides "Service1"
     When I go to the public appointment scheduling page for "Clinic1"
     Then I should see "Select a Provider"

  @javascript
  Scenario: Only offer services available for new patients
    Given I am configured to provide "Service2" service in "Room1" room
      And I am configured to provide "Service3" service in "Room1" room
      And Service "Service2" has been configured as not public
     When I go to the public appointment scheduling page for "Clinic1"
     Then I should see "Service1"
      And I should see "Service3"
      And I should not see "Service2"

  @javascript
  Scenario: Only offer providers for the requested service
    Given I have a provider "Dr. Doc2"
      And I have a provider "Dr. Doc3"
      And I have an appointment type "Service2"
      And Provider "Dr. Doc2" provides "Service2"
      And Provider "Dr. Doc3" provides "Service2"
     When I go to the public appointment scheduling page
      And I follow "Service2"
     Then I should not see "Dr. Doc1"
      And I should see "Dr. Doc2"
      And I should see "Dr. Doc3"

  @javascript
  Scenario: If only one provider (for the requested service), don't ask the user to choose.
    Given I have an appointment type "Service2"
     When I go to the public appointment scheduling page
      And I follow "Service1"
     Then I should not see "Dr. Doc1"
      And I should see "Select a Time"

  @javascript
  Scenario: If there are no providers (for the requested service), give a warning.
    Given I have an appointment type "Service2"
     When I go to the public appointment scheduling page
      And I follow "Service2"
     Then I should see "There are currently no providers available"

  @javascript @pause @firebug
  Scenario: Display the date and available times to click on.
    Given I do not have any appointments scheduled
     When I go to the public appointment scheduling page
     Then I should see "Select a Time"
      And I should see the following time slots available:
          |time_slot|
          |8:00 AM  |
          |8:15 AM  |
          |8:30 AM  |
          |8:45 AM  | 
          |9:00 AM  |
     When I follow "9:00 AM"
     Then I should see "Please confirm this appointment"
      And I should see "Dr. Doc1" within ".appointment_details"
      And I should see "Service1" within ".appointment_details"
      And I should see "9:00 AM"  within ".appointment_details"


