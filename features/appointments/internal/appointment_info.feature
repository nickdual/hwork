Feature: Appointment Info LHS
  Displaying appointment information to left hand side panel
  As an office scheduler
  Manipulate appointment attributes quickly.

  Background:
    Given I have a practice "Clinic1" for "Dr. Doc1"
      And I am configured to provide "Service1" service in "Room1" room
      And I have a patient "Fname Patient1"

  @javascript
  Scenario: Deleting the selected appointment will clear the Appointment Info

    Given I have the following appointments today:
      |patient    |provider    |room     |service      |time        |
      |Patient1   |Dr. Doc1    |Room1    |Service1     |10:30       |
    When I follow "Calendar"
     And I select the "Fname Patient1" appointment
    Then I should see "Patient1" within ".appointment_info"
    When I delete the "Fname Patient1" appointment
     And I confirm popup
    Then I should not see "Patient1" within ".appointment_info"

  # FIXME: There appears to be some order dependency with these scenarios.

  @javascript
  Scenario: Appointment Info focus cleared when returning to calendar
    Given I have the following appointments today:
      |patient    |provider    |room     |service      |time        |
      |Patient1   |Dr. Doc1    |Room1    |Service1     |10:00       |
    When I follow "Calendar"
    Then I should see "Fname Patient1" within ".wc-cal-event"
    When I select the "Fname Patient1" appointment
    Then I should see "Patient1" within ".appointment_info"
    When I navigate to "Diagnosis Codes" under "User Files"
     And I follow "Calendar"
    Then I should not see "Patient1" within ".appointment_info"
     And I should see "Fname Patient1" within ".wc-cal-event"
    When I select the "Fname Patient1" appointment
    Then I should see "Patient1" within ".appointment_info"

