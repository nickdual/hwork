Feature: Appointments for New Patients
  To create an appointment for a new patient.
  As an apointment scheduler
  I want to create a new patient as the current patient

  Background:
    Given I have a practice "Clinic1" for "Dr. Doc1"
      And I am configured to provide "Service1" service in "Room1" room

  @javascript
  Scenario: Creating a new patient for an appointment
    When I follow "Calendar"
     And I follow "New Patient"
    Then I should see "New Patient" within ".fancybox-inner"
    When I fill in "Last name" with "TestD"
     And I follow "Cancel new phone"
     And I press "Save"
    Then I should see "TestD" within "li.current-patient"
