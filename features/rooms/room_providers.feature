Feature: Room Providers Association

Background:
  Given I am an account owner
    And I have a provider "Dr. Doc1"

@javascript
Scenario: Single provider should be automatically associated
  When I navigate to "Treatment Rooms" under "User Files"
   And I follow "New Room"
  Then I should not see "Providers that can schedule this room"
   And I should not see "Can Schedule"
  When I fill in "Treatment Room" with "Room1"
   And I press "Save"
  Then I should see a success message
   And I should see "Room1" within "#rooms_list"
   And room "Room1" should have "Dr. Doc1" provider

@javascript
Scenario: Multiple providers include provider association 
  Given I have a provider "Dr. Doc2"
   When I navigate to "Treatment Rooms" under "User Files"
    And I follow "New Room"
   Then I should see "Providers that can schedule this room"
    And I should see "Can Schedule"
   When I fill in "Treatment Room" with "Room1"
    And I press "Save"
   Then I should see a success message
    And I should see "Room1" within "#rooms_list"
    And room "Room1" should not have "Dr. Doc1" provider

