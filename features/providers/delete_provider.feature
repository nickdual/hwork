Feature: Deleting a Provider
  To maintain handyworks practice configuration
  As an Account Admin
  I want to delete provider records

  Background:
    Given I have a practice "Happy Practice" for "Dr. Smiles"

  # This scenario ensures the client side providers list has been initialized
  # when trying to delete provider. while this heavily duplicates the previous
  # scenario, for the moment, this clearly ensures that both delete conditions
  # are tested.
  @javascript
  Scenario: Deleting a Provider after visiting the Calendar
    When I follow "Calendar"
     And I navigate to "Provider Times and Info" under "User Files"
    Then I should see "Dr. Smiles" within "#provider_list"
    When I follow "Delete this record?" within "#provider_list"
     And I confirm popup
    Then I should not see "Dr. Smiles" within "#provider_list"

  @javascript
  Scenario: Basic Deleting a Provider from the List
    When I navigate to "Provider Times and Info" under "User Files"
    Then I should see "Dr. Smiles" within "#provider_list"
    When I follow "Delete this record?" within "#provider_list"
     And I confirm popup
    Then I should not see "Dr. Smiles" within "#provider_list"
