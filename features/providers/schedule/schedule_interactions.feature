Feature: Provider Schedule Dynamic UI

  Background:
    Given I am an account owner
      And I have a clinic "Clinic1"
      And I have a provider "Dr. Doc1"

  @javascript
  Scenario: Fix for disapearing schedule blocks
    When I follow "Calendar"
     And I navigate to "Provider Times and Info" under "User Files"
     And I follow "Provider Schedule"
    Then I should see "Dr. Doc1 Available Hours"
     And I should see "08:00 am to 12:00 pm" within ".wc-day-column-first.day-2"
    When I drag Monday block at 8:00 AM to the top
    Then I should see "12:00 am to 04:00 am" within ".wc-day-column-first.day-2"
