#-------------------------------------------------------------------------------
# Interactively step through steps, just tag the scenario with @pause
# and it will prompt you after each step.
#
# http://itshouldbeuseful.wordpress.com/2011/06/23/step-through-your-cucumber-features-interactively/
#-------------------------------------------------------------------------------
AfterStep('@pause') do
    print "Press Return to continue..."
      STDIN.getc
end

#-------------------------------------------------------------------------------
# From The Cucumber Book, 8.5 Building the User Interface
#  - While this approach sounds good, the resulting page is not particularly
#    useful in this app, perhaps its do to the dynamic nature of some of HW's
#    page elements. Anyway leaving this here for reference
#-------------------------------------------------------------------------------
#After do |scenario|
#  save_and_open_page if scenario.failed?
#end

