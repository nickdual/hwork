module HtmlSelectorsHelpers
  # Maps a name to a selector. Used primarily by the
  #
  #   When /^(.+) within (.+)$/ do |step, scope|
  #
  # step definitions in web_steps.rb
  #
  def selector_for(locator)
    case locator

    when /the page/
      "html > body"

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #  when /the (notice|error|info) flash/
    #    ".flash.#{$1}"
    when /the footer/
      "#footer"

    when /the navigation menu/
      "header.navbar"
    #
    # OLD Selectors Defined below here from rails 3.0 app version
    when /the user clinic profile/
      [:xpath, "id('clinic_user_edit')"]

    when /the user access profile/
      [:xpath, "id('access_user_edit')"]

    when /the provider legacy id field labels/
      [:xpath, "//table/tbody/tr[contains(@class,'fields')]/td[1]"]

    when /the provider legacy id field values/
      [:xpath, "//table/tbody/tr[contains(@class,'fields')]/td[2]"]

    when /the provider legacy id destroyer/
      [:xpath, "//table/tbody/tr[contains(@class,'fields')]/td[3]"]

    when /the provider legacy id fields/
      [:xpath, "//table/tbody/tr[contains(@class,'fields')]"]

    when /the carrier legacy id label/
      [:xpath, "//form/fieldset[2]/ol/span"]

    when /the legacy ids list/
      [:xpath, "id('provider_legacy_id_labels')"]

    when /the "(.+)" legacy ids row/
      [:xpath, "id('provider_legacy_id_labels')/tbody/tr[contains(@class,'arow')][td/input[@value='#{$1}']]"]

    when /the new legacy id label/
      [:xpath, "id('provider_legacy_id_labels')/tbody/tr[4]/td[1]"]

    when /the new legacy id value/
      [:xpath, "id('provider_legacy_id_labels')/tbody/tr[4]/td[2]"]

    when /the appointment types list/
      [:xpath, "id('appointment_types_list')"]

    when /the new provider appointment type row/
      [:xpath, "id('appointment_types_list')/tbody/tr[4]"]

    when /the new provider appointment type name/
      [:xpath, "id('appointment_types_list')/tbody/tr[4]/td[1]"]

    when /the new appointment type row/
      [:xpath, "id('appointment_types_list')/tbody/tr[2]"]

    when /the new appointment type name/
      [:xpath, "id('appointment_types_list')/tbody/tr[2]/td[1]"]

    when /the "(.+)" appointment type row/
      [:xpath, "id('appointment_types_list')/tbody/tr[contains(@class,'arow')][td//input[@value='#{$1}']]"]

    when /the fees list/
      [:xpath, "id('procedure_code_fees_table')"]

    when /the "(.+)" fees row/
      [:xpath, "id('procedure_code_fees_table')/tbody/tr[contains(@class,'arow')][td/input[@value='#{$1}']]"]

    when /the new fee schedule label/
      [:xpath, "id('procedure_code_fees_table')/tbody/tr[3]/td[1]"]

    when /the new fee schedule fee/
      [:xpath, "id('procedure_code_fees_table')/tbody/tr[3]/td[2]"]

    when /the new fee schedule copay/
      [:xpath, "id('procedure_code_fees_table')/tbody/tr[3]/td[3]"]

    when /the new fee schedule percentage/
      [:xpath, "id('procedure_code_fees_table')/tbody/tr[3]/td[4]"]

    when /the new fee schedule expected insurance/
      [:xpath, "id('procedure_code_fees_table')/tbody/tr[3]/td[5]"]

    when /the "(.+)" fee schedule record/
      [:xpath, "//tr[contains(@class,'fee_schedule_record')][td[contains(text(),'#{$1}')]]"]

    when /the "(.+)" procedure_code record/
      [:xpath, "//tr[contains(@class,'procedure_code_record')][td[contains(text(),'#{$1}')]]"]

    when /the "(.+)" provider record/
      [:xpath, "//tr[contains(@class,'provider_record')][td[contains(text(),'#{$1}')]]"]

    when /the "(.+)" room record/
      [:xpath, "//tr[contains(@class,'room_record')][td[contains(text(),'#{$1}')]]"]

    when /the "(.+)" third party record/
      [:xpath, "//tr[contains(@class,'third_party_record')][td[contains(text(),'#{$1}')]]"]

    when /the admin user record/
      [:xpath, '//tr[contains(@class,"user_record")][td[contains(text(),"admin.login")]]']

    when /the staff user record/
      [:xpath, '//tr[contains(@class,"user_record")][td[contains(text(),"staff.login")]]']

    when /the "(.+)" permission record/
      [:xpath, '//table[@id="permissions_list"]/tbody/tr[td[contains(text(),"staff.login")]]']

    when /the "(.+)" hcfa form option record/
      [:xpath, "//tr[contains(@class,'hcfa_form_option_record')][td[contains(text(),'#{$1}')]]"]

    when /the data switcher/
      [:xpath, "//p[@class='data_switcher']"]

    # You can also return an array to use a different selector
    # type, like:
    #
    #  when /the header/
    #    [:xpath, "//header"]

    # This allows you to provide a quoted selector as the scope
    # for "within" steps as was previously the default for the
    # web steps:
    when /"(.+)"/
      $1

    else
      raise "Can't find mapping from \"#{locator}\" to a selector.\n" +
        "Now, go and add a mapping in #{__FILE__}"
    end
  end
end

World(HtmlSelectorsHelpers)
