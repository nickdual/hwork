Before do
  module Resque 
    def self.enqueue(klass, *args)
      klass.perform(*args)
    end
  end
end
