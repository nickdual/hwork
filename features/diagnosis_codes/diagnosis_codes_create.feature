Feature: Create a Diagnosis Code
  To keep track of patient conditions
  As a provider
  I want to define new diagnosis codes

  Background:
    Given I am an account owner
      And I navigate to "Diagnosis Codes" under "User Files"
    When I follow "New Diagnosis code"
    Then I should see "New Diagnosis"

  @javascript
  Scenario: Creating a new diagnosis code
    When I fill in the following:
     |Your Code      | ABCDE         |
     |ICD-9          | 370.55        |
     |Description    | Test Diag     |
     And I press "Save"
    Then I should see a success message
     And I should see "Test Diag" within "#diagnosis_codes_list"
  
  @javascript
  Scenario: Your Code is required
    When I fill in the following:
     |ICD-9          | 370.55        |
     |Description    | Test Diag     |
     And I press "Save"
    Then I should see "This field is required" within "#diagnosis_code_name_input"

  @javascript
  Scenario: ICD-9 is required
    When I fill in the following:
     |Your Code      | ABCDE         |
     |Description    | Test Diag     |
     And I press "Save"
    Then I should see "This field is required" within "#diagnosis_code_code_input"

  @javascript
  Scenario: Description is required
    When I fill in the following:
     |Your Code      | ABCDE         |
     |ICD-9          | 370.55        |
     And I press "Save"
    Then I should see "This field is required" within "#diagnosis_code_description_input"
