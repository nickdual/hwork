Feature: Create Messages
  To communicate with patients
  As a practice owner
  I would like to compose and store message templates.

  Background:
    Given I am an account owner
      And I navigate to "Messages" under "User Files"
    When I follow "New Letter"
    Then I should see "New Message"

  @javascript
  Scenario: Creating a new message
    When I fill in the following:
     |Message Name   | MESSTEST      |
     And I set the fancy editor "Text" with "Hello World\n\n test."
     And I select "HTML for Email" from "Message Format"
     And I press "Save"
    Then I should see a success message
     And I should see "MESSTEST" within "#messages_list"
     And I should see "Hello World" within "#messages_list"

  @javascript
  Scenario: Message Name is required
    When I set the fancy editor "Text" with "Hello World\n\n test."
     And I select "HTML for Email" from "Message Format"
     And I press "Save"
    Then I should see "This field is required" within "#message_label_input"
