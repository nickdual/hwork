Feature: Edit User
  As a registered user of the website
  I want to delete my user profile
  so I can close my account

    @javascript
    Scenario: I sign in and delete my account
      Given There is a basic plan
        And I am an account owner
      When I delete my account
      Then I should see an account deleted message

    @javascript
    Scenario: I create a new subscription and delete my account
      Given There is a basic plan
      And I am on the home page
      When I subscribe for basic
      Then I should see "Basic Subscription Plan"
      Given I fill in the following:
        | user_email                 | short@testing.com |
      When I press "Sign up"
      Then I should see a successful sign up message
