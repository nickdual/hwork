Feature: Account User Management
  To allow users to manage a set of accounts for office users
  As an Account Admin
  I want to reset passwords, update emails addresses etc.

Background:
  Given I am an account owner

# TODO : right now this test was created just to fix a bug in the users_datatables, it is not complete
@javascript
Scenario: Reset my password
   When I navigate to "Users Permissions" under "User Files"
   Then I should see "example@example.com" within "#users_list"
