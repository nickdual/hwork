Feature: Deleting a patient
  When I delete a patient
  As a staff member
  I want references to be cleaned up

  Background:
    Given I have a practice "SomeClinic" for "Dr. Doctor"
      And I have a patient "Myfirst Patient"

  @javascript
  Scenario: Deleting a patient removes the index view record and clears the current patient
     When I follow "Patients"
     Then I should see "Myfirst Patient" within "li.current-patient"
      And I should see "Myfirst" within "#patients_list"
     When I follow "Delete this record?" within "#patients_list" 
      And I confirm popup
     Then I should not see "Myfirst Patient" within "li.current-patient"
      And I should not see "Myfirst" within "#patients_list"
