Feature: Creating a Patient
  In order to track peoples treatments
  As a provider
  I want to create patient records for new clients

  Background:
    Given I am an account owner
      And there is 1 clinic
     When I follow "Patients"
      And I follow "New Patient"

  @javascript
  Scenario: Creating a patient (basic scenario)
    When I fill in "Last name" with "TestA"
     And I fill in "patient_contact_attributes_phones_attributes_0_number" with "(555) 444-3333"
     And I press "Save"
    Then I should see a success message
     And I should see "TestA" within "#patients_list"

  @javascript
  Scenario: Creating a new patient without a phone
    When I fill in "Last name" with "TestB"
     And I follow "Cancel new phone"
     And I press "Save"
    Then I should see "TestB" within "#patients_list"

  @javascript
   Scenario: New patient is the current patient after creation.
    When I create a new patient "TestFN TestLN"
    Then I should see "TestFN TestLN" within "li.current-patient"
