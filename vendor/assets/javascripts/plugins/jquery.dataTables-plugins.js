/*
 * TODO : submit a pull request to add these two plugins to the gem, then remove this file.
 *   https://github.com/rweng/jquery-datatables-rails
 *
 * These functions come from : http://datatables.net/plug-ins/api
 */
$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
{
	return {
		"iStart":         oSettings._iDisplayStart,
		"iEnd":           oSettings.fnDisplayEnd(),
		"iLength":        oSettings._iDisplayLength,
		"iTotal":         oSettings.fnRecordsTotal(),
		"iFilteredTotal": oSettings.fnRecordsDisplay(),
		"iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
		"iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
	};
}

$.fn.dataTableExt.oApi.fnDisplayRow = function ( oSettings, nRow )
{
    // Account for the "display" all case - row is already displayed
    if ( oSettings._iDisplayLength == -1 )
    {
        return;
    }
  
    // Find the node in the table
    var iPos = -1;
    for( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
    {
        if( oSettings.aoData[ oSettings.aiDisplay[i] ].nTr == nRow )
        {
            iPos = i;
            break;
        }
    }
      
    // Alter the start point of the paging display
    if( iPos >= 0 )
    {
        oSettings._iDisplayStart = ( Math.floor(i / oSettings._iDisplayLength) ) * oSettings._iDisplayLength;
        this.oApi._fnCalculateEnd( oSettings );
    }
      
    this.oApi._fnDraw( oSettings );
};
