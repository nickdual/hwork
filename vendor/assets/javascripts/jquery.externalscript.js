/*******************************************************************************
 * A script to either fetch an external JavaScript or load it from a cache.
 *  --> http://railsapps.github.com/tutorial-rails-stripe-membership-saas.html
 ******************************************************************************/
jQuery.externalScript = function(url, options) {
    options = $.extend(options || {}, {
        dataType: "script",
            cache: true,
            url: url
    });
    return jQuery.ajax(options);
};
