module SessionHelper
  def current_account
    if user_signed_in? then
      current_user.account
    end
  end

  #-----------------------------------------------------------------------------
  # References the current clinic context for this session. 
  #  Only returns a clinic if a user is signed in.
  #  It will first look for the clinic in the session
  #  Then it will reference the user's default clinic
  #  Then it will just take the first clinic result for the current account.
  #
  # FIXME : because the last command to return clinic is conditional, there are situations where current clinic returns nothing. This should probably always return at a minimum current_account.clinics.first
  #-----------------------------------------------------------------------------
  def current_clinic
    if current_user then
      if session[:clinic_id] then
        begin
          current_clinic = Clinic.find(session[:clinic_id])
        rescue ActiveRecord::RecordNotFound
          session[:clinic_id] = nil
        end
      else
        if current_user.clinic then
          current_clinic = current_user.clinic
        else
          current_clinic = current_user.account.clinics.first
        end
        session[:clinic_id] = current_clinic.id if current_clinic
      end
      @current_ability ||= ::Ability.new(current_user)
      return current_clinic if @current_ability.can?(:show, current_clinic)
    end
  end

  #-----------------------------------------------------------------------------
  # Returns the list of clinics which are included in the Clinic switching
  # interface for the current user. If a user is not signed in then this list
  # will be nil.
  #-----------------------------------------------------------------------------
  def current_clinic_list
    if current_user then
      return Clinic.accessible_by(Ability.new(current_user,current_account.try(:id),current_clinic.try(:id)))
    end
  end

  def current_patient
    if current_user then
      if session[:patient_id] then
        begin
         patient = Patient.find(session[:patient_id])
        rescue ActiveRecord::RecordNotFound
          session[:patient_id] = nil
        end
      else
        if current_clinic
          patient = current_clinic.try(:patients).try(:first)
        else
          patient = current_user.account.patients.first
        end
        session[:patient_id] = patient.id if patient
      end
      @current_ability ||= ::Ability.new(current_user)
      # OPTIMIZE : seems like this helper will end up creating a new Ability with each request. this would be suboptimal
      #            one option would be to just use the can? 
      return patient if @current_ability.can?(:show, patient)
    end
  end
end
