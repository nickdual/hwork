module AccountsHelper

  #-----------------------------------------------------------------------------
  # indicates if the current user is an AccountAdmin
  #-----------------------------------------------------------------------------
  def is_admin?
    current_account.admin?(current_user)
  end

  #-----------------------------------------------------------------------------
  # indicates if the current user is an AccountAdmin
  #-----------------------------------------------------------------------------
  def is_app_admin?
    current_account.app_admin?(current_user)
  end

  #-----------------------------------------------------------------------------
  # indicates if the current user is an AccountAdmin
  #-----------------------------------------------------------------------------
  def is_owner?
    current_user.eql(current_account.owner)
  end
end
