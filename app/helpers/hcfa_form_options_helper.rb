module HcfaFormOptionsHelper
  #-----------------------------------------------------------------------------
  # encapsulate and standardize the markup generated to display fields on the
  # HCFA Form options screen.
  #
  #  #box21_field.hcfa_field
  #    = f.label :box21, 'Box 21 Diagnosis'
  #    = f.select :box21, HcfaFormOption::BOX21S
  #-----------------------------------------------------------------------------
  def hcfa_field_tag
    # TODO : fill this markup helper in as described above and refactor the hcfa_form_options/_form.html.haml
  end

  #-----------------------------------------------------------------------------
  # Returns the best match HCFA Form Options available.
  #   1. match account & carrier id is best
  #   2. match account & current_clinic.state & form_type is next best
  #   3. match global account & state & form_type is defaults
  #   4. else just return a new object
  #-----------------------------------------------------------------------------
  def get_hcfa_option(account,clinic,form_type_code,third_party)
    hrel = HcfaFormOption.where(:account_id => account.id, :third_party_id => third_party) unless account.nil? or third_party.nil?
    return hrel.first unless hrel.empty?
    hrel = HcfaFormOption.where(:account_id => account.id, :state => clinic.state, :form_type_code => form_type_code) unless account.nil? or clinic.nil?
    return hrel.first unless hrel.empty?
    hrel = HcfaFormOption.where(:account_id => Account::DEFAULT_ACCOUNT_ID, :state => clinic.state, :form_type_code => form_type_code) unless clinic.nil?
    return hrel.first unless hrel.empty?
    return HcfaFormOption.new
  end
end
