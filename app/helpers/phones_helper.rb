module PhonesHelper
  include ActionView::Helpers::NumberHelper
  # BLS : if the complexity of formatting and parsing phones becomes much more
  #       complex, should consider using the Phoner::Phone gem
  # https://github.com/carr/phone
  #-----------------------------------------------------------------------------
  # format the phone nicely for display
  #-----------------------------------------------------------------------------
  def phone_fmt(number)
    if number.length <= 7 then
      return number_to_phone(number, :area_code => false)
    elsif number.length <= 10 then
      return number_to_phone(number, :area_code => true)
    elsif number.length > 10 then
      return number_to_phone(number[0..9], :area_code => true, :extension => number[10..number.length])
    end
  rescue
    return number
  end

  def phone_parse(new_number)
    /^\+(\d+)/ =~ new_number
    country = $1
    ap = new_number.split(/[+() x-]/).reject{ |s| s.empty? }
    number =  country.nil? ? ap.join : ap.join.gsub(/^#{country}/,"")
    return [country, number]
  rescue
    return [nil, new_number]
  end


  #-----------------------------------------------------------------------------
  # Custom Phone Validator to ensure we're getting either 7 or 10 or more
  # digits stored for the numbers, if we enhance for multiple countries, this
  # validation might need to move into the model class to allow for a 
  # country code to drive the phone validation.
  #  - allows for up to 6 digit extension
  #-----------------------------------------------------------------------------
  class PhoneFormatValidator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
      # accepts either 7 or 10 or more numbers
      ln = 0
      ln = value.length unless value.nil?
      unless ln == 7 or ( ln >= 10 and ln <= 16 ) then
        record.errors.add attribute, "invalid format (###) ###-####[x######]"
      end
    end
  end

end
