module ApplicationHelper
  def title
    "Handyworks - Medical Client Tracker"
  end

  #-----------------------------------------------------------------------------
  # A helper method that will generate a simple bootstrap label markup
  #-----------------------------------------------------------------------------
  def bootstrap_label(content, type)
    render('shared/bootstrap_marker', :type => type, :content => content )
  end

  #-----------------------------------------------------------------------------
  # A helper method that will generate a simple bootstrap badge markup
  #-----------------------------------------------------------------------------
  def bootstrap_badge(content, type)
    render('shared/bootstrap_marker', :type => type, :content => content, :marker => 'badge' )
  end

  #-----------------------------------------------------------------------------
  # Create an Array of Editor Replacements. This is going to be loaded into the
  # CkEditor hwrepl plugin. This make the plugin non-portable, but DRY's up 
  # the list of fields that can be replaced.
  #-----------------------------------------------------------------------------
  def replacements_for_js
    replacements = "["
    APP_CONFIG['replacements'].keys.sort.each do |repl|
      replacements.concat(",") if replacements.length > 1
      replacements.concat("['#{repl.titleize}','#{repl}']")
    end
    replacements.concat("]")
  end

  #-----------------------------------------------------------------------------
  # popover notification helper. we want to display a popover on page load, but
  # to avoid being annoying about it, we'll only do that for say the first 
  # page display during a session
  #
  # if no_initial is passed as true it will prevent the popup from being
  # displayed automatically even on the first load of the session.
  #-----------------------------------------------------------------------------
  def popover_init(type, no_initial=false)
    session[:popover] ||= []
    result = "pop pop-initial" unless session[:popover].include?(type) or no_initial
    result ||= "pop"
    session[:popover] << type
    return result
  end

  #-----------------------------------------------------------------------------
  # Twitter Bootstrap class identifying form button CSS class
  #-----------------------------------------------------------------------------
  def button_class
    "btn"
  end

  #-----------------------------------------------------------------------------
  # a slight bit of help to setup remote help links
  # FIXME : remove the dummy arguments once all help is converted.
  #-----------------------------------------------------------------------------
  def link_to_help(label_key,options, dummy = nil)
    begin
      _options = options.merge(:remote => true)
      link_to get_help_path(label_key), _options do
        "&nbsp;".html_safe
      end
    rescue Exception => e
      link_to get_help_path('help_error'), :remote => true do
        "&nbsp;".html_safe
      end
    end
  end

  #-----------------------------------------------------------------------------
  # Displays a button link to a remote help_text record.
  #-----------------------------------------------------------------------------
  def help_button(help_label)
    link_to "Help", get_help_path(help_label), :remote => true, :class => 'btn button-info'
  end

  #-----------------------------------------------------------------------------
  # Methods from Railscasts #197 for nested forms.
  #   slight modification for rails 3 which is to remove the h() function
  #   that was resulting in rendering the escaped HTML
  #
  #   another slight modification to move the render views to the shared
  #   folder.
  #-----------------------------------------------------------------------------
  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)")
  end

  #-----------------------------------------------------------------------------
  #  @param callback - optional function to be called after the fields are 
  #                    added
  #-----------------------------------------------------------------------------
  def link_to_add_fields(name, f, association, callback=nil)
    content = get_form_for_association(f, association)
    if callback then
      func = "add_fields(this, '#{association}', '#{content}', #{callback})"
    else
      func = "add_fields(this, '#{association}', '#{content}')"
    end
    link_to_function(name, func)
  end

  #-----------------------------------------------------------------------------
  # compute the content necessary to dynamically add an association
  # return this in a format suitable to be transmitted as JavaScript variable
  #-----------------------------------------------------------------------------
  def get_form_for_association(f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render("shared/" + association.to_s.singularize + "_fields", :f => builder)
    end
    return escape_javascript(fields)
  end

  #-----------------------------------------------------------------------------
  # A method to swap a select box for a text field ... This is applicable to 
  # nested forms where the user can either select from an existing list of objects
  # to fulfill an association, or user can create a new object to be referenced.
  #-----------------------------------------------------------------------------
  def link_to_add_combo_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render("shared/" + association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(nil, "combo_fields(this, '#{association}', '#{escape_javascript(fields)}')", :class => "combo_fields_link icon16 add")
  end
  
  #-----------------------------------------------------------------------------
  # allows for adding multiple flash messages while processing single request
  #   source: http://stackoverflow.com/questions/2448789/rails-easy-way-to-add-more-than-one-flashnotice-at-a-time
  #-----------------------------------------------------------------------------
  def flash_message(type, text)
    flash[type] ||= []
    flash[type] << text
  end

  #-----------------------------------------------------------------------------
  # Return a class consistent with jQuery UI themes to style flash messages
  #-----------------------------------------------------------------------------
  def get_flash_theme(type)
    logger.debug("get flash theme #{type}")
    if type == :success
      return 'alert alert-success' #'ui-state-highlight'
    elsif type == :notice
      return 'alert alert-info' #'ui-state-highlight'
    elsif type == :alert or type == :error
      return 'alert alert-error' #'ui-state-error'
    end
    return ''
  end

  def get_flash_theme_icon(type)
    if type == :success
      return 'ui-icon ui-icon-check'
    elsif type == :notice
      return 'ui-icon ui-icon-info'
    elsif type == :alert
      return 'ui-icon ui-icon-notice'
    elsif type == :error
      return 'ui-icon ui-icon-alert'
    end
    return ''
  end

  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end

  def format_time_array(array)
    if array.length > 2
      for i in 0..array.length - 1
        if array[i] != '**'
          hour = array[i].hour
          hour2 = array[i+1].hour
          if hour2 - hour > 1
            array.insert(i+1,'**')
          end
        end
      end
    end
  end

  def get_index(array,item)
    for i in 0...array.length
      if item == array[i]
        return i + 1
      end
    end
  end
  def format_time(item)
    arr = []
    h = item.hour()
    m = item.min()
    if m == 0
      m = '00'
    end
    if h > 12
      arr.push(h%12, m)
      temp = arr.join(':')
      arr.clear
      arr.push(temp,'PM')
      time = arr.join(' ')
    elsif h == 12 && m > 0
      arr.push(h%12,m)
      temp = arr.join(':')
      arr.clear
      arr.push(temp,'PM')
      time = arr.join(' ')
    else
      arr.push(h,m)
      temp = arr.join(':')
      arr.clear
      arr.push(temp,'AM')
      time = arr.join(' ')
    end
    return time
  end
  
end
