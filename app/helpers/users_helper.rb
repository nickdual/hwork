module UsersHelper
  def user_name(user)
    if @user.contact.first_name.blank?
      "User: " + @user.email.downcase 
    else
       "User: " + @user.email.downcase + " - " + @user.contact.first_name.titleize
    end
  end  
end
