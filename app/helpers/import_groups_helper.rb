module ImportGroupsHelper
  #-----------------------------------------------------------------------------
  # get_csv_import_configuration
  #  - produces json output to transfer CSV import configuration data to 
  #    JavaScript side for use in Data Import screens
  #-----------------------------------------------------------------------------
  def get_csv_import_configuration_for_js
    APP_CONFIG['csv_import']['configuration'].to_json
  end

  #-----------------------------------------------------------------------------
  # get_available_import_groups
  #   determine which groups are available to the current user to load
  #-----------------------------------------------------------------------------
  def get_available_import_groups
    ImportGroup.get_available_import_groups(current_account,current_user)
  end

  #-----------------------------------------------------------------------------
  # returns the list of current imports for this account to be used in 
  # determining which imports are now available to the user to load.
  #-----------------------------------------------------------------------------
  def get_current_imports_for_js
    Import.accessible_by(@current_ability).map { |i| i.import_type }.to_json
  end

  #-----------------------------------------------------------------------------
  # generates JavaScript output which is going to transmit the status of the
  # indicated import_group to the browser through updating a variable defined
  # on the client as HandyworksApp.Data.import_group. It is used from both an
  # HTML response view and a JavaScript response view.
  #-----------------------------------------------------------------------------
  def import_status_to_js(import_group)
    out = ""
    if import_group.valid?
      out << %Q{HandyworksApp.Data.import_group.status = "#{import_group.status}";}
      # we need to go back through the configuration list of imported files to ensure
      # we have the complete list of files in the same order as they will appear on the
      # HTML form b/c if a file is optional and not included, there will be no associated 
      # import record for that file.
      config = ImportGroup.get_import_group_configuration(import_group.import_group_type)
      config['files'].each_with_index do |file, index|
        import = import_group.imports.select { |i| i.import_type == file['type'] }[0]
        unless import.nil?
          out << %Q{HandyworksApp.Data.import_group.files[#{index}] = { status: "#{import.status}" };}
        else
          out << %Q{HandyworksApp.Data.import_group.files[#{index}] = { status: " " };}
        end
      end
    else
      import_group.errors.full_messages.each do |msg|
        out << %Q{HandyworksApp.Data.import_group.errors.push("#{msg}");}
      end
    end
    out.html_safe
  end
end
