class EventsController < AuthorizedController
  skip_load_and_authorize_resource :only => :create
  respond_to :json

  def create
    @event = Event.new(whitelist(params))
    # BLS : this method is being skipped by the higher level load_and_authorize_resource b/c
    #       it was generating an error trying to look at the schedule's account_id for an 
    #       empty event.
    authorize! :create, @event
    if @event.save
      last_used_new(@event)
      complete_setup_for("Schedule")
      respond_with(@event)
    else
      respond_with(@event, :status => :unprocessable_entity)
    end
  end

  def update
    # set by CanCan @event = Event.find(params[:id])
    # look for location updates under "clinics" parameter
    clinic_ids = params[:clinics].map { |c| c[:id] } if params[:clinics]
    logger.debug "updating locations #{clinic_ids}" if params[:clinics]
    if @event.update_attributes(whitelist(params.merge({ :clinic_ids => clinic_ids } )))
      complete_setup_for("Schedule")
      respond_with(@event)
    else
      respond_with(@event, :status => :unprocessable_entity)
    end
  end

  def destroy
    # set by CanCan @event = Event.find(params[:id])
    @event.destroy
    respond_to do |format|
      complete_setup_for("Schedule")
      format.json do
        # BLS this is necessary to avoid a 1-byte respons
        #  - see http://stackoverflow.com/questions/7064127/http-post-head-ok-returning-1-mystery-byte
        render :text => "", :status => :ok
      end
    end
  end

  private
  #-----------------------------------------------------------------------------
  # filters parameters from the request parameters which are valid (safe) from
  # JSON requests.
  #-----------------------------------------------------------------------------
  def whitelist(params)
    params.slice(:title,:start_date,:start_time,:duration,:eventable_id,:eventable_type,:free,:notes,:clinic_ids)
  end
end
