class FeeSchedulesController < AuthorizedController
  respond_to :html, :json, :js

  # GET /fee_schedules
  # GET /fee_schedules.json
  def index
    # set by CanCan @fee_schedules = FeeSchedule.by_account(current_user.account_id).ordered
    @fee_schedules = @fee_schedules.ordered()
    respond_with(@fee_schedules)
  end

  # GET /fee_schedules/1
  # GET /fee_schedules/1.json
  def show
    # set by CanCan @fee_schedule = FeeSchedule.find(params[:id])
    respond_with(@fee_schedlule)
  end

  # GET /fee_schedules/new
  # GET /fee_schedules/new.json
  def new
    @fee_schedule = FeeSchedule.new
    @fee_schedule.account = current_account
    respond_with(@fee_schedule)
  end

  # GET /fee_schedules/1/edit
  def edit
    # set by CanCan @fee_schedule = FeeSchedule.find(params[:id])
  end

  # POST /fee_schedules
  # POST /fee_schedules.json
  def create
    @fee_schedule = FeeSchedule.new(params[:fee_schedule])

    respond_to do |format|
      if @fee_schedule.save
        last_used_new(@fee_schedule)
        format.html { redirect_to(@fee_schedule, :notice => 'Fee schedule added.') }
        format.json  { render :json => @fee_schedule, :status => :created, :location => @fee_schedule }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @fee_schedule.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /fee_schedules/1
  # PUT /fee_schedules/1.json
  def update
    # set by CanCan @fee_schedule = FeeSchedule.find(params[:id])

    respond_to do |format|
      if @fee_schedule.update_attributes(params[:fee_schedule])
        format.html { redirect_to(fee_schedules_path, :notice => 'Fee schedule updated.') }
        format.json  { head :ok }
        # NOTE: if you add a format.js option here the editPopup request would choose that response
      else
        format.html { render :action => "edit" }
        format.json  { render :json => @fee_schedule.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @fee_schedule.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /fee_schedules/1
  # DELETE /fee_schedules/1.json
  def destroy
    # set by CanCan @fee_schedule = FeeSchedule.find(params[:id])
    @fee_schedule.destroy

    respond_to do |format|
      format.json do
        # BLS this is necessary to avoid a 1-byte respons
        #  - see http://stackoverflow.com/questions/7064127/http-post-head-ok-returning-1-mystery-byte
        render :text => "", :status => :ok
      end
      format.html { redirect_to(fee_schedules_path, :notice => 'Fee schedule removed.') }
      format.js
    end
  end
end
