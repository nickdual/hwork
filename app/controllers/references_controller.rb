class ReferencesController < AuthorizedController
  respond_to :html, :json, :js
  # GET /references
  # GET /references.json
  def index
    # @references = Reference.all handled by CanCan
    @datatable = ReferencesDatatable.new(view_context,@references)
    respond_to do |format|
      format.html
      format.json { render :json => @datatable }
    end
  end

  # GET /references/1
  # GET /references/1.json
  def show
    # @reference = Reference.find(params[:id]) handled by CanCan
    respond_with(@reference)
  end

  # GET /references/new
  # GET /references/new.json
  def new
    @reference = Reference.build
    @reference.contact.build_business_email
    @reference.contact.build_fax_phone
    @reference.account = current_account
    respond_with(@reference)
  end

  # GET /references/1/edit
  def edit
    # @reference = Reference.find(params[:id]) handled by CanCan
    respond_with(@reference)
  end

  # POST /references
  # POST /references.json
  def create
    @reference = Reference.new(params[:reference])

    respond_to do |format|
      if @reference.save
        last_used_new(@reference)
        format.html { redirect_to(references_path, :notice => 'Referral added.') }
        format.json  { head :ok }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @reference.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @reference.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /references/1
  # PUT /references/1.json
  def update
    # @reference = Reference.find(params[:id]) handled by CanCan

    respond_to do |format|
      if @reference.update_attributes(params[:reference])
        format.html { redirect_to(references_path, :notice => 'Referral updated.') }
        format.json  { head :ok }
        # NOTE: if you add a format.js option here the editPopup request would choose that response
      else
        format.html { render :action => "edit" }
        format.json  { render :json => @reference.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @reference.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /references/1
  # DELETE /references/1.json
  def destroy
    # @reference = Reference.find(params[:id]) handled by CanCan
    @reference.destroy

    respond_to do |format|
      format.json do
        # BLS this is necessary to avoid a 1-byte respons
        #  - see http://stackoverflow.com/questions/7064127/http-post-head-ok-returning-1-mystery-byte
        render :text => "", :status => :ok
      end
      format.html { redirect_to(references_url) }
    end
  end
end
