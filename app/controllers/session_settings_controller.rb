#-------------------------------------------------------------------------------
# Session Settings
#  This controller is specifically designed to provide an interface to 
#  sessions specific settings like current_clinic and current_patient
#  which the user can change to affect their workflow. Specifically it
#  is expected to be triggered via AJAX request.
#-------------------------------------------------------------------------------
class SessionSettingsController < ApplicationController

  respond_to :json

  #-----------------------------------------------------------------------------
  # FIXME : The "last_used" functionality was intended create a session cache of
  # the users most recently used resources. The way it was implemented, its 
  # actually using the --> Controller <-- name as part of the cached key instead
  # of the resource name. This is problematic for this controller where we're
  # trying to manipulate the resources which typically belong to other controllers
  #
  # So to work around this cross controller caching for the moment, I'm just
  # performing the cache using a hard-coded controller names. It would be better
  # to figure out how to encapsulate the logic within application_controller /
  # last_used_key.
  #-----------------------------------------------------------------------------
  def update
    error = false

    begin
      if params[:clinic_id] then
        # authorize this clinic for the current user
        @clinic = Clinic.find(params[:clinic_id])
        authorize! :read, @clinic
        session[:clinic_id] = params[:clinic_id]
        #last_used_new(@clinic)
        session['ClinicsController_mru'] = params[:clinic_id]
        data = {}
      end
      if params[:patient_id] then
        # authorize this patient for the current_user
        @patient = Patient.find(params[:patient_id])
        authorize! :read, @patient
        session[:patient_id] = params[:patient_id]
        #last_used_new(@patient)
        session['PatientsController_mru'] = params[:patient_id]
        data = { :name => @patient.name }
      end
    rescue
      # handles access denied excpetions
      error = true
    end

    respond_to do |format|
      unless error
        format.json do
          render :text => data.to_json, :status => :ok
        end
      else
        format.json do
          render :text => data.to_json, :status => :unprocessable_entity
        end
      end
    end

  end

end
