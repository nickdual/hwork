class RegistrationsController < Devise::RegistrationsController
  
  #-------------------------------------------------------------------------------
  # Check to make sure subscription plan has been specified
  #-------------------------------------------------------------------------------
  def new
    if params[:plan]
      @subscription_plan = SubscriptionPlan.find(params[:plan])
      unless @subscription_plan.nil?
        return super
      end
    end
    redirect_to plans_path, :notice => 'Please select a subscription plan below'
  end

  #-------------------------------------------------------------------------------
  # setup the account and subscription for this user if necessary
  #  - we're only checking the 'email' errors to determine if its a valid 
  #    and unique address that was supplied on the registration screen. other
  #    validation issues might be corrected during this procedure.
  #-------------------------------------------------------------------------------
  def create
    super
    if resource.errors['email'].empty?
      if resource_name == :user and resource.account.nil?
        logger.info("new account setup for #{resource.email}")
        resource.account = Account.new() if resource.account.nil?
        resource.account.subscription = Subscription.new(:subscription_plan_id => params[:plan])
        resource.save!
        resource.account.init_user(resource) unless resource.account.init_user?
      end
    end
  end

  ###-------------------------------------------------------------------------------
  ###
  ###   When updating a profile, some forms will require the user to enter their
  ###   current password, while others won't. In order prevent circumventing this
  ###   protection, email, password, password_confirmation and current_password
  ###   parameters will be deleted from params if the update type indicates it
  ###   should not require a password.
  ###-------------------------------------------------------------------------------
  ##def update
  ##  # handle user clinic preferences form
  ##  if ( params[:user][:update] == '1' ) then
  ##    if resource.update_attributes(whitelist(params[resource_name]))
  ##      set_flash_message :notice, :updated if is_navigational_format?
  ##      #sign_in resource_name, resource, :bypass => true
  ##      respond_to do |format|
  ##        format.html { redirect_to resource, :location => after_update_path_for(resource) }
  ##        format.js   { head :ok }
  ##        format.json { head :ok }
  ##      end
  ##    else
  ##      clean_up_passwords(resource)
  ##      respond_with_navigational(resource){ render_with_scope :edit }
  ##    end
  ##  else
  ##    respond_to do |format|
  ##      if resource.update_with_password(params[resource_name])
  ##        set_flash_message :notice, :updated if is_navigational_format?
  ##        sign_in resource_name, resource, :bypass => true
  ##        format.html { respond_with resource, :location => after_update_path_for(resource) }
  ##        format.js { head :ok }
  ##      else
  ##        clean_up_passwords(resource)
  ##        format.html { respond_with resource }
  ##        format.json  { render :json => resource.errors, :status => :unprocessable_entity }
  ##        # NOTE: its actually the js format that's being rendered
  ##        #       when called from the editPopup windows, not quite sure why
  ##        format.js {
  ##          render :json => { :errors => resource.errors }, :status => :unprocessable_entity
  ##        }
  ##      end
  ##    end
  ##  end
  ##end

  private

  #-------------------------------------------------------------------------------
  # Scrubs the parameter list of senstive account parameters which require a 
  # passord confirmation to update.
  #-------------------------------------------------------------------------------
  def whitelist(params)
    # TODO : probably some way to do hash filtering with a single line of code
    params.delete(:password)
    params.delete(:password_confirmation)
    params.delete(:login)
    params.delete(:email)
    params.delete(:update)
    return params
  end

  def build_resource(*args)
    super
    if params[:plan]
      resource.add_role(params[:plan])
    end
  end
end

