class PublicAppointmentsController < ApplicationController
  layout 'public_appointments'

  def index
  end

  def new
    @account = Account.find(params[:account_id])
    @clinic = @account.clinics.find(params[:clinic_id]) if params[:clinic_id]
    @clinic ||= @account.clinics.first unless @account.has_multiple_locations?
    render :new
  rescue ActiveRecord::RecordNotFound
    redirect_to public_appointments_path, notice: 'The object you tried to access does not exist'
  end

  #-----------------------------------------------------------------------------
  # This method returns a list of times as JSON available for the requested
  # appointment (clinic, service, provider, start_date)
  #
  # times will consist of ( utc epoch, list of room ids ).
  #
  # if any of the input parameters are not set, an error will be returned.
  #-----------------------------------------------------------------------------
  def times
    unless Appointment.validate_times_inputs(params)
      render :json => {:errors => "invalid inputs"}, :status => :unprocessable_entity
      return
    end
    t = Time.now.to_i * 1000
    @times = [
      { :time => t, :rooms => [ 1, 2, 3 ] },
      { :time => t + 1500000, :rooms => [ 1, 2 ] },
      { :time => t + 3000000, :rooms => [ 2, 3 ] }
    ]
    render :json => @times, :status => :ok
  end

end
