class SchedulesController < AuthorizedController
  respond_to :html, :xml, :json, :js

  def show
    # set by CanCan @schedule = Schedule.find(params[:id])
    respond_with(@schedule)
  end

  def edit
    @schedulable = find_schedulable(params)
    @schedule = @schedulable.schedule
    respond_with(@schedule)
  end

  def update
    @schedulable = find_schedulable(params)
    @schedule = @schedulable.schedule

    if @schedule.update_attributes(whitelist(params))
      respond_with(@schedule)
    else
      respond_with(@schedule, :status => :unprocessable_entity)
    end
  end

  protected

  #-----------------------------------------------------------------------------
  # since this controller is used for both Providers and Clinics, this will
  # hide the complexity of determing how to look up the belongs_to object
  # for this schedule.
  #-----------------------------------------------------------------------------
  def find_schedulable(params)
    schedulable = nil
    if params.include?(:provider_id)
      schedulable = Provider.find(params[:provider_id])
    elsif params.include?(:clinic_id)
      schedulable = Clinic.find(params[:clinic_id])
    elsif params.include?(:schedulable_id)
      if params[:schedulable_type] == "Provider"
        schedulable = Provider.find(params[:schedulable_id])
      elsif params[:schedulable_type] == "Clinic"
        schedulable = Clinic.find(params[:schedulable_id])
      end
    end
    return schedulable
  end

  def whitelist(params)
    # include :schedule to support non-JSON requests.
    if params[:schedule]
      params[:schedule]
    else
      params.slice(:max_concurrent_appointments, :max_appointments_per_hour)
    end
  end

end
