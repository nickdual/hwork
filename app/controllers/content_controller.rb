class ContentController < ApplicationController
  before_filter :authenticate_user!

  def basic
    authorize! :view, :basic, :message => 'Access limited to Basic Plan subscribers.'
  end

  def plus
    authorize! :view, :plus, :message => 'Access limited to Plus Plan subscribers.'
  end

  def premium
    authorize! :view, :premium, :message => 'Access limited to Premium Plan subscribers.'
  end

  def max
    authorize! :view, :max, :message => 'Access limited to Max Plan subscribers.'
  end

end
