class DiagnosisCodesController < AuthorizedController
  respond_to :html, :json, :js

  # GET /diagnosis_codes
  # GET /diagnosis_codes.json
  def index
    # set by CanCan @diagnosis_codes = DiagnosisCode.by_account(current_user.account)
    @datatable = DiagnosisCodesDatatable.new(view_context,@diagnosis_codes)
    respond_to do |format|
      format.html
      format.json { render :json => @datatable }
    end
  end

  # GET /diagnosis_codes/1
  # GET /diagnosis_codes/1.json
  def show
    # set by CanCan @diagnosis_code = DiagnosisCode.find(params[:id])
    respond_with(@diagnosis_code)
  end

  # GET /diagnosis_codes/new
  # GET /diagnosis_codes/new.json
  def new
    @diagnosis_code = DiagnosisCode.new
    @diagnosis_code.account = current_account
    respond_with(@diagnosis_code)
  end

  # GET /diagnosis_codes/1/edit
  def edit
    # set by CanCan @diagnosis_code = DiagnosisCode.find(params[:id])
    respond_with(@diagnosis_code)
  end

  # POST /diagnosis_codes
  # POST /diagnosis_codes.json
  def create
    @diagnosis_code = DiagnosisCode.new(params[:diagnosis_code])

    respond_to do |format|
      if @diagnosis_code.save
        last_used_new(@diagnosis_code)
        format.html { redirect_to(diagnosis_codes_path, :notice => 'Diagnosis code added.') }
        format.json  { head :ok }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @diagnosis_code.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @diagnosis_code.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /diagnosis_codes/1
  # PUT /diagnosis_codes/1.json
  def update
    # set by CanCan @diagnosis_code = DiagnosisCode.find(params[:id])

    respond_to do |format|
      if @diagnosis_code.update_attributes(params[:diagnosis_code])
        format.html { redirect_to(diagnosis_codes_path, :notice => 'Diagnosis code updated.') }
        format.json  { head :ok }
        # NOTE: if you add a format.js option here the editPopup request would choose that response
      else
        format.html { render :action => "edit" }
        format.json  { render :json => @diagnosis_code.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @diagnosis_code.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /diagnosis_codes/1
  # DELETE /diagnosis_codes/1.json
  def destroy
    # set by CanCan @diagnosis_code = DiagnosisCode.find(params[:id])
    @diagnosis_code.destroy

    respond_to do |format|
      format.json do
        # BLS this is necessary to avoid a 1-byte respons
        #  - see http://stackoverflow.com/questions/7064127/http-post-head-ok-returning-1-mystery-byte
        render :text => "", :status => :ok
      end
      format.html { redirect_to(diagnosis_codes_path) }
    end
  end
end
