class SubscriptionsController < AuthorizedController
  skip_load_and_authorize_resource :only => :create
  respond_to :html, :xml, :json, :js

  #-----------------------------------------------------------------------------
  # Subscription accessed as a singlular resource
  # GET /subscription
  # GET /subscription
  #-----------------------------------------------------------------------------
  def show
    @subscription = current_account.subscription
    respond_with(@subscription)
  end

  def edit
    @subscription = current_account.subscription
    respond_with(@subscription)
  end

  # POST /subscriptions
  # POST /subscriptions.xml
  def create
    @subscription = Subscription.new(params[:subscription])

    respond_to do |format|
      if @subscription.save
        last_used_new(@subscription)
        format.html { redirect_to(@subscription, :notice => 'Subscription was successfully created.') }
        format.xml  { render :xml => @subscription, :status => :created, :location => @subscription }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @subscription.errors, :status => :unprocessable_entity }
      end
    end
  end

  #-----------------------------------------------------------------------------
  # Updates the subscription plan for this subscription. It will push the update
  # along to the Stripe payment system through the Subscription model 
  # implementaiton.
  #-----------------------------------------------------------------------------
  def update_plan
    @subscription = current_account.subscription
    plan = SubscriptionPlan.find(params[:subscription][:subscription_plan_id]) unless params[:subscription][:subscription_plan_id].nil?
    if plan and @subscription.update_plan(plan)
      redirect_to edit_subscription_path, :notice => 'Updated plan.'
    else
      redirect_to edit_subscription_path, :alert => 'Unable to update plan.'
    end
  end

  #-----------------------------------------------------------------------------
  # Updates or Creates the credit card on file for this subscription. 
  # The updated card information is handled by Stripe's API to produce a 
  # stripe_token which is the reference that will be "confirmed" back to Stripe
  # to apply the new active_card in a PCI compliant manner.
  #-----------------------------------------------------------------------------
  def update_card
    @subscription = current_account.subscription
    stripe_token = params[:subscription][:stripe_token]
    if stripe_token.length > 0 and @subscription.update_card(stripe_token)
      redirect_to edit_subscription_path, :notice => 'Updated card.'
    else
      redirect_to edit_subscription_path, :alert => 'Unable to update card.'
    end
  end

  # TODO : implement subscription cancellation procedure.
  def destroy
    # set by CanCan @room = Room.find(params[:id])
    subscription = current_account.try(:subscription)
    if subscription 
      authorize! :destroy, subscription
      current_account.cancel!
      subscription.destroy
      sign_out current_user

      respond_to do |format|
        format.html do
          redirect_to root_path, :notice => 'We have cancelled your subscription.'
        end
      end
    end
  end

end
