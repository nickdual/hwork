class AccountsController < ApplicationController
  respond_to :html, :xml, :json
  before_filter :authenticate_user!

  def index
    authorize! :index, @account, :message => 'Not authorized as an adminstrator.'
    @accounts = Account.all
    logger.info("getting accounts #{@accounts.count}")
  end

  def edit
    @account = Account.find(params[:id]) if params[:id] 
    @account ||= current_user.account
    authorize! :update, @account
    respond_with(@account)
  end

  # PUT /accounts/1
  # PUT /accounts/1.xml
  def update
    @account = Account.find(params[:id]) if params[:id] 
    @account ||= current_user.account
    authorize! :update, @account

    respond_to do |format|
      if @account.update_attributes(params[:account])
        format.html { redirect_to(root_path, :notice => 'Account was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @account.errors, :status => :unprocessable_entity }
      end
    end
  end

end
