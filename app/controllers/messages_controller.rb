class MessagesController < AuthorizedController
  respond_to :html, :json, :js
  # GET /setup/messages
  def index
    # set by CanCan : @messages
    @datatable = MessagesDatatable.new(view_context,@messages)
    respond_to do |format|
      format.html
      format.json { render :json => @datatable }
    end
  end

  # GET /setup/messages/1
  def show
    # @message set by CanCan
    respond_with(@message)
  end

  # GET /setup/messages/new
  def new
    @message = Message.new
    respond_with(@message)
  end

  # GET /setup/messages/1/edit
  def edit
    respond_with(@message)
  end

  # POST /setup/messages
  # POST /setup/messages.json
  def create
    @message = Message.new(params[:message])
    @message.type = 'Letter'
    @message.messageable = current_account # right now all letters are account level
    # TODO : add a check to make sure newly created message will be accessible by current user
    respond_to do |format|
      if @message.save
        last_used_new(@message)
        format.html { redirect_to(messages_path, :notice => 'Message added.') }
        format.json  { head :ok }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @message.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @message.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /setup/messages/1
  def update
    respond_to do |format|
      if @message.update_attributes(params[:message])
        format.html { redirect_to(messages_path, :notice => 'Message updated.') }
        format.json  { head :ok }
        # NOTE: if you add a format.js option here the editPopup request would choose that response
      else
        format.html { render :action => "edit" }
        format.json  { render :json => @message.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @message.errors }, :status => :unprocessable_entity }
      end
    end
  end


  # DELETE /setup/messages/1
  def destroy
    @message.destroy
    respond_to do |format|
      format.html { redirect_to(messages_url) }
      format.json  { render :text => "", :status => :ok }
    end
  end
end
