class AuthorizedController < ApplicationController
  layout 'authorized'
  # based on the following recipe for integrating CanCan + Devise
  # https://github.com/plataformatec/devise/wiki/How-To:-Use-CanCan-+-Devise,-The-easy-way.
  before_filter :authenticate_user!, :be_helpful
  #----------------------------------------------------------------------------
  # By default, all controllers should be checking authorization
  #  if you want a particular controller to skip authorization, then
  #  add skip_authorization_check to that controller.
  #  Since this check is not in the ApplicationController, it only applies
  #  to controllers which extend this controller.
  #----------------------------------------------------------------------------
  check_authorization
  load_and_authorize_resource

  #----------------------------------------------------------------------------
  # check to see if the user needs to_go to a specific location to continue 
  # their current workflow, and help them get there.
  #
  # this method is disabled for testing because it makes most testing much
  # more difficult.
  #----------------------------------------------------------------------------
  def be_helpful
    unless Rails.env == 'test'
      to_go = check_needs
      # this check is essential to prevent redirect loops
      if to_go and not request.path.starts_with?(to_go) 
        logger.debug("be_helpful send #{to_go} but your are #{request.url}")
        redirect_to to_go
        return false
      end
    end
  end

  #----------------------------------------------------------------------------
  # Generically handle base class exceptions
  #----------------------------------------------------------------------------
  rescue_from Errors::HandyworksError, :with => :handle_error

  protected

  #----------------------------------------------------------------------------
  # This method called as an exception handler via rescue_from. It appears
  # it needs to explicitly call the flash_headers method (defined in 
  # ApplicationController) as the exception handling must take the request
  # flow out of its normal callback sequence, the flash_headers (an after_filter)
  # is not getting called otherwise.
  #----------------------------------------------------------------------------
  def handle_error(exception)
    # TODO : this should probably be dependent on the current state of the user (e.g. signed in).
    flash[:error] = exception.message
    self.flash_headers
    respond_to do |format|
      format.json { render :text => "", :status => :forbidden }
      format.html { redirect_to root_path }
    end
  end

end
