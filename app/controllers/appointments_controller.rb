class AppointmentsController < AuthorizedController
  include ApplicationHelper
  skip_load_and_authorize_resource :only => [ :create, :new ]
  skip_authorization_check :only => [ :new ]
  respond_to :json
  
  # GET /appointments/1
  # GET /appointments/1.json
  def show
    # set by CanCan @appointment = Appointment.find(params[:id])
    respond_with(@appointment)
  end

  def new
    @appointment = Appointment.new
    # new appointments will default to use the active patient
    @appointment.contact = current_patient.try(:contact)
    respond_with(@appointment)
  end

  def create
    @appointment = Appointment.new(whitelist(params))
    @appointment.event = Event.new(event_whitelist(params['event']))
    @appointment.event.notes = "scheduled #{Time.now().to_s()}\n by #{current_user.display_name}"
    @appointment.calendar = current_account.calendar
    # BLS : this method is being skipped by the higher level load_and_authorize_resource b/c
    #       it was generating an error trying to look at the calendars's account_id for an 
    #       empty appointment.
    authorize! :create, @appointment
    if @appointment.save
      puts '1'
      last_used_new(@appointment)
      respond_with(@appointment)
    else
      respond_with(@appointment, :status => :unprocessable_entity)
    end
  end

  def update
    # set by CanCan @appointment = Appointment.find(params[:id])
    # handle the creation of a new patient
    if params[:contact_id] and params[:contact_id] == 0
      p = Patient.newForAppointment(current_account,current_clinic,params[:new_first_name],params[:new_last_name],params[:new_phone])
      logger.info("New patient created #{p.id} : #{p.contact.id}")
      @appointment.contact_id = p.contact.id
      params.delete :contact_id
    end
    if @appointment.update_attributes(whitelist(params)) and
      @appointment.event.update_attributes(event_whitelist(params['event']))
      #respond_with(@appointment)
      render :json => @appointment
    else
      respond_with(@appointment, :status => :unprocessable_entity)
    end
  end

  def destroy
    # set by CanCan @appointment = Appointment.find(params[:id])
    @appointment.destroy
    respond_to do |format|
      format.json do
        render :text => "", :status => :ok
      end
    end
  end

  def export_pdf
    @full_time = params[:fulltime]
    time_string = params[:today]
    params_room = ActiveSupport::JSON.decode(params[:room_ids])
    room_ids = params_room['x']
    result_array = [] #result
    appointment_array = [] #contain appointment with time
    time_array = []  #contain time
    @total = ['Total']
    @room = current_user.account.rooms
    appointment_array.push('')
    room_ids.each do |id|
      @r = @room.find(id)
      appointment_array.push(@r.name)
      @total.push(@r.appointments.joins(:event).where('events.start_date' => time_string ).length)
    end
    result_array.push(appointment_array)
    @appointments = Appointment.joins(:event).where('events.start_date' => time_string )
    @appointments.each do |app|
      if ! time_array.include? app.event.start_time
        time_array.push(app.event.start_time )
      end
    end
    time_array = time_array.sort
    format_time_array(time_array)
    time_array.each do |time|
      temp = Array.new(room_ids.length + 1)
      temp[0]= time != '**' ? format_time(time): time
      @app_time = @appointments.joins(:event).where('events.start_time' => time)
      @app_time.each do |a_t|
        flag = get_index(room_ids,a_t.room_id)
        temp[flag] = a_t.event.title
      end
      result_array.push(temp)
    end
    if result_array.length > 2 || result_array.length == 1
      temp = Array.new(room_ids.length + 1)
      temp[0]= '**'
      result_array.push(temp)
    end
    @result_array = result_array
    respond_to do |format|
      format.pdf do
        render pdf: 'daily_appointments_listing',
               :layout  => 'pdf.html.haml',
               :formats => [:pdf],
               :handlers => [:haml]
      end
    end
  end
  
  private

  def whitelist(params)
    # put updatable params into this list
    params.slice(:room_id, :provider_id, :contact_id, :appointment_type_id) 
  end

  # TODO : this is the same whitelist I'm using in the events_controller
  def event_whitelist(params)
    params.slice(:title,:start_date,:start_time,:duration,:eventable_id,:eventable_type,:free,:notes,:clinic_ids)
  end


end

