class LegacyIdLabelsController < AuthorizedController
  respond_to :html, :xml, :json

  # DELETE /legacy_id_labels/1
  # DELETE /legacy_id_labels/1.xml
  def destroy
    # set by CanCan: @legacy_id_label = LegacyIdLabel.find(params[:id])

    respond_to do |format|
      if @legacy_id_label.destroy
        format.json do
          # BLS this is necessary to avoid a 1-byte respons
          #  - see http://stackoverflow.com/questions/7064127/http-post-head-ok-returning-1-mystery-byte
          render :text => "", :status => :ok
        end
        format.html  { redirect_to(providers_path) }
        format.js
      else
        format.json { render :json => { :errors => @legacy_id_label.errors }, :status => :unprocessable_entity }
      end
    end
  end
end
