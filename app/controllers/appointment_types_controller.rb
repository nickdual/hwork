class AppointmentTypesController < AuthorizedController
  respond_to :html, :xml, :json
  # GET /appointment_types
  # GET /appointment_types
  def index
    # set by CanCan @appointment_types = AppointmentType.by_account(current_user.account)
    @datatable = AppointmentTypesDatatable.new(view_context,@appointment_types)
    respond_to do |format|
      format.html
      format.json { render :json => @datatable }
    end
  end

  # GET /appointment_types/1
  # GET /appointment_types/1.json
  def show
    # set by CanCan @appointment_type = AppointmentType.find(params[:id])
    respond_with(@appointment_type)
  end

  # GET /appointment_types/new
  # GET /appointment_types/new.json
  def new
    @appointment_type = AppointmentType.new
    @appointment_type.account = current_account
    colour = "#" + "%06x" % (rand * 0xffffff)
    @appointment_types = AppointmentType.where(:account_id => current_account).select('appointment_color')
    while @appointment_types.to_a.map{|x| x.appointment_color}.include?(colour)
      colour = "#" + "%06x" % (rand * 0xffffff)
    end
    @appointment_type.appointment_color = colour
    respond_with(@appointment_type)
  end

  # GET /appointment_types/1/edit
  def edit
    # set by CanCan @appointment_type = AppointmentType.find(params[:id])
    respond_with(@appointment_type)
  end

  # POST /appointment_types
  # POST /appointment_types.json
  def create
    params[:provider_ids] = params[:appointment_type][:provider_ids]
    params[:appointment_type][:provider_ids] = [""]
    @appointment_type = AppointmentType.new(params[:appointment_type])
    if params[:provider_ids] != [""]
      @appointment_type.save
      @appointment_type.provider_ids = params[:provider_ids]
    end
    respond_to do |format|
      if @appointment_type.save
        last_used_new(@appointment_type)
        format.html { redirect_to(appointment_types_path, :notice => 'Appointment type added.') }
        format.json  { head :ok }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @appointment_type.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @appointment_type.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /appointment_types/1
  # PUT /appointment_types/1.json
  def update
    # set by CanCan @appointment_type = AppointmentType.find(params[:id])

    respond_to do |format|
      if @appointment_type.update_attributes(params[:appointment_type])
        format.html { redirect_to(appointment_types_path, :notice => 'Appointment type updated.') }
        format.json  { head :ok }
        # NOTE: if you add a format.js option here the editPopup request would choose that response
      else
        format.html { render :action => "edit" }
        format.json  { render :json => @appointment_type.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @appointment_type.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /appointment_types/1
  # DELETE /appointment_types/1.json
  def destroy
    # set by CanCan @appointment_type = AppointmentType.find(params[:id])
    @appointment_type = AppointmentType.find(params[:id])
    @appointment_type.destroy

    respond_to do |format|
      format.json do
        # BLS this is necessary to avoid a 1-byte respons
        #  - see http://stackoverflow.com/questions/7064127/http-post-head-ok-returning-1-mystery-byte
        render :text => "", :status => :ok
      end
      format.html { redirect_to(appointment_types_path) }
    end
  end
end
