class UsersController < AuthorizedController
  respond_to :html, :json, :js

  # GET /users
  # GET /users.json
  def index
    # set by CanCan @users = User.by_account(current_user.account_id)
    @datatable = UsersDatatable.new(view_context,@users)
    respond_to do |format|
      format.html
      format.json { render :json => @datatable }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    # set by CanCan @user = User.find(params[:id])
    respond_with(@user)
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])
    @user.account = current_account if current_account
    @user.skip_confirmation!
    Permission.initialize_user_roles(@user)

    respond_to do |format|
      if @user.save
        last_used_new(@user)
        format.html { redirect_to(users_path, :notice => 'User created.') }
        format.json  { head :ok }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @user.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @user.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # GET /clinics/new
  # GET /clinics/new.json
  def new
    @user = User.build
    respond_with(@user)
  end

  # GET /clinics/1/edit
  def edit
    # set by CanCan @user = User.find(params[:id])
    Permission.initialize_user_roles(@user)
    @permission = Permission.new()
  end


  # PUT /clinics/1
  # PUT /clinics/1.json
  def update
    # set by CanCan @user = User.find(params[:id])

    relogin = false
    # only reset the password if requested
    if params[:user][:password].blank? 
      params[:user].delete(:password) 
      params[:user].delete(:password_confirmation)
    elsif @user.id == current_user.id
      relogin = true
    end

    respond_to do |format|
      # We're using the roles_attributes in kind of a strange way, we've got
      # it configured in the user model as attr_accessible, and accepts_nested_attributes_for
      # which makes the view form setup more straightforward. But we don't actually want
      # to UPDATE the roles, what we're really updating is the role associations, users_roles.
      if params[:user][:roles_attributes]
        Permission.update_roles(@user,params[:user][:roles_attributes])
        params[:user].delete(:roles_attributes)
      end
      if @user.update_attributes(params[:user])
        if relogin
          sign_in(@user, :bypass => true) if relogin
        end
        format.html { redirect_to(users_path, :notice => 'User was successfully updated.') }
        format.json  { head :ok }
        # NOTE: if you add a format.js option here the editPopup request would choose that response
      else
        format.html { render :action => "edit" }
        format.json  { render :json => @user.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @user.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /clinics/1
  # DELETE /clinics/1.json
  def destroy
    # set by CanCan @user = User.find(params[:id])
    unless @user == current_user
      @user.destroy

      respond_to do |format|
        format.json do
          # BLS this is necessary to avoid a 1-byte respons
          #  - see http://stackoverflow.com/questions/7064127/http-post-head-ok-returning-1-mystery-byte
          render :text => "", :status => :ok
        end
        format.html { redirect_to(users_path) }
      end

    else
      redirect_to users_path, :notice => "Can't delete yourself."
    end

  end

end
