class ClinicsController < AuthorizedController
  respond_to :html, :json, :js

  # GET /clinics
  # GET /clinics.json
  def index
    # set by CanCan @clinics = Clinic.by_account(current_user.account_id).alphabetically
    @datatable = ClinicsDatatable.new(view_context,@clinics)
    respond_to do |format|
      format.html
      format.json { render :json =>  @datatable }
    end
  end

  # GET /clinics/1
  # GET /clinics/1.json
  def show
    # set by CanCan @clinic = Clinic.find(params[:id])
    session[:clinic_id] = @clinic.id
    respond_with(@clinic)
  end

  # GET /clinics/new
  # GET /clinics/new.json
  def new
    @clinic = Clinic.build
    # need some extra fields
    @clinic.contact.build_business_email
    @clinic.contact.build_fax_phone
    @clinic.contact.build_billing_address
    @clinic.account = current_account
    respond_with(@clinic)
  end

  # GET /clinics/1/edit
  def edit
    # set by CanCan @clinic = Clinic.find(params[:id])
    session[:clinic_id] = @clinic.id
    respond_with(@clinic)
  end

  # POST /clinics
  # POST /clinics.json
  def create
    @clinic = Clinic.new(params[:clinic])
    # FIXME : fix explicit setting of the new_patient_option.clinic when creating a new clinic
    #         the current solution would seem to indicate some problem with either the form or underlying models as it
    @clinic.new_patient_option.clinic = @clinic

    respond_to do |format|
      if @clinic.save
        last_used_new(@clinic)
        complete_setup_for(@clinic)
        session[:clinic_id] = @clinic.id
        format.html { redirect_to(clinics_path, :notice => 'Clinic was successfully created.') }
        format.json  { head :ok }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @clinic.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /clinics/1
  # PUT /clinics/1.json
  def update
    # set by CanCan @clinic = Clinic.find(params[:id])

    respond_to do |format|
      if @clinic.update_attributes(params[:clinic])
        complete_setup_for(@clinic)
        format.html { redirect_to(clinics_path, :notice => 'Clinic was successfully updated.') }
        format.json  { head :ok }
        # NOTE: if you add a format.js option here the editPopup request would choose that response
      else
        format.html { render :action => "edit" }
        format.json  { render :json => @clinic.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @clinic.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /clinics/1
  # DELETE /clinics/1.json
  def destroy
    # set by CanCan @clinic = Clinic.find(params[:id])
    @clinic.destroy

    respond_to do |format|
      format.json do
        # BLS this is necessary to avoid a 1-byte respons
        #  - see http://stackoverflow.com/questions/7064127/http-post-head-ok-returning-1-mystery-byte
        render :text => "", :status => :ok
      end
      format.html { redirect_to(clinics_path) }
    end
  end
end
