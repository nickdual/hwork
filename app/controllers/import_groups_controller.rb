class ImportGroupsController < AuthorizedController
  respond_to :html, :json, :js

  # GET /import_groups
  # GET /import_groups.json
  def index
    # loaded by CanCan in before_filter @import_groups = ImportGroup.all
    @datatable = ImportGroupsDatatable.new(view_context,@import_groups)
    respond_to do |format|
      format.html
      format.json { render :json => @datatable }
    end
  end

  # GET /import_groups/1
  # GET /import_groups/1.json
  def show
    #loaded by CanCan in before_filter @import_group = ImportGroup.find(params[:id])
    respond_with(@import_group)
  end

  # GET /import_groups/new
  # GET /import_groups/new.json
  def new
    @import_group = ImportGroup.new
    # set the default import group to the "next" logical import group
    @import_group.import_group_type = ImportGroup.get_available_import_groups(current_account,current_user).last
    @import_group.account = current_account
    @import_group.user = current_user
    respond_with(@import_group)
  end

  # GET /import_groups/1/edit
  def edit
    # loaded by CanCan in before_filter @import_group = ImportGroup.find(params[:id])
    respond_with(@import_group)
  end

  #-----------------------------------------------------------------------------
  # return the import status for the current account, indicating the 
  # current import running for this account
  #-----------------------------------------------------------------------------
  def import_status
    @import_group = ImportGroup.current_import(current_account.id)
    # TODO : add an HTML return option to ImportGroupsController.import_status
    respond_to do |format|
      format.js { render "status", :layout => false }
      format.html { render "create", :layout => false }
    end
  end

  #-----------------------------------------------------------------------------
  # this method will only be called asynchronously, but is coming through an
  # iframe (for file upload in older browsers, i.e. IE). As such the Rails
  # framework will try to process these respones as HTML, so we're passing 
  # an extra parameter :ajaxForm which is being added by the jquery.form 
  # plugin that will be used to determine how to package the response.
  # POST /import_groups
  #-----------------------------------------------------------------------------
  def create
    @import_group = ImportGroup.new(params[:import_group])
    @import_group.account = current_account
    @import_group.user = current_user
    @import_group.clinic = current_clinic
    respond_to do |format|
      if @import_group.save and @import_group.create_job()
        last_used_new(@import_group)
        logger.info("import group has been received with #{@import_group.imports.size} imports")
        @import_group.status = "Processing"
        format.js { render "create", :layout => false }
        format.html do
          if params[:ajaxForm]
            render "create", :layout => false
          else
            redirect_to(import_groups_path, :notice => 'Import group was successfully created.')
          end
        end 
        format.json  { render :json => @import_group, :status => :created, :location => @import_group }
      else
        format.html do
          if params[:ajaxForm] 
            render "create", :layout => false, :status => :unprocessable_entity
          else
            render :action => "new"
          end
        end
        format.json { render :json => @import_group.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /import_groups/1
  # PUT /import_groups/1.json
  def update
    # loaded by CanCan in before filter @import_group = ImportGroup.find(params[:id])
    respond_to do |format|
      if @import_group.update_attributes(params[:import_group])
        format.html { redirect_to(import_groups_path, :notice => 'Import group was successfully updated.') }
        format.json  { head :ok }
        # NOTE: if you add a format.js option here the editPopup request would choose that response
      else
        format.html { render :action => "edit" }
        format.json  { render :json => @import_group.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @import_group.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /import_groups/1
  # DELETE /import_groups/1.json
  def destroy
    # loaded by CanCan in before filter @import_group = ImportGroup.find(params[:id])
    @import_group.destroy

    respond_to do |format|
      format.html { redirect_to(import_groups_path, :notice => 'Import group was deleted.') }
      format.json  { render :text => "", :status => :ok }
    end
  end
end
