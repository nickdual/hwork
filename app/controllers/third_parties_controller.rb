class ThirdPartiesController < AuthorizedController
  respond_to :html, :json, :js

  # GET /third_parties
  # GET /third_parties.json
  def index
    #@third_parties = ThirdParty.by_account(current_account.id) set by CanCan
    @datatable = ThirdPartiesDatatable.new(view_context,@third_parties)
    respond_to do |format|
      format.html
      format.json { render :json => @datatable }
    end
  end

  # GET /third_parties/1
  # GET /third_parties/1.json
  def show
    # @third_party = ThirdParty.find(params[:id]) set by CanCan
    respond_with(@third_party)
  end

  # GET /third_parties/new
  # GET /third_parties/new.json
  def new
    @third_party = ThirdParty.build
    @third_party.contact.build_business_email
    @third_party.contact.build_fax_phone
    @third_party.account = current_account
    respond_with(@third_party)
  end

  # GET /third_parties/1/edit
  def edit
    # @third_party = ThirdParty.find(params[:id]) set by CanCan
    @third_party.contact.build_business_email if @third_party.contact.business_email.nil?
    respond_with(@third_party)
  end

  # POST /third_parties
  # POST /third_parties.json
  def create
    @third_party = ThirdParty.new(params[:third_party])

    respond_to do |format|
      if @third_party.save
        last_used_new(@third_party)
        format.html { redirect_to(third_parties_path, :notice => 'Third Party Added.') }
        format.json  { head :ok }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @third_party.errors, :status => :unprocessable_entity }
      end
    end
  end



  # PUT /third_parties/1
  # PUT /third_parties/1.json
  def update
    # @third_party = ThirdParty.find(params[:id]) set by CanCan

    respond_to do |format|
      if @third_party.update_attributes(params[:third_party])
        format.html { redirect_to(third_parties_path, :notice => 'Third Party updated.') }
        format.json  { head :ok }
        # NOTE: if you add a format.js option here the editPopup request would choose that response
      else
        format.html { render :action => "edit" }
        format.json  { render :json => @third_party.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @third_party.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /third_parties/1
  # DELETE /third_parties/1.json
  def destroy
    # @third_party = ThirdParty.find(params[:id]) set by CanCan
    @third_party.destroy

    respond_to do |format|
      format.json do
        # BLS this is necessary to avoid a 1-byte respons
        #  - see http://stackoverflow.com/questions/7064127/http-post-head-ok-returning-1-mystery-byte
        render :text => "", :status => :ok
      end
      format.html { redirect_to(third_parties_path) }
    end
  end

end
