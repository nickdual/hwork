class ContactUsController < ApplicationController
  skip_load_and_authorize_resource :only => [ :send_us ]
  respond_to :html, :xml, :json,:js
  def send_us
    @contact = ContactUs.new(params[:contact_us])
     if @contact.save
       ContactUsMailer.send_contact(@contact.id, ENV["ADMIN_EMAIL"]).deliver!
       flash[:notice] = 'Contact was successfully sent.'
       respond_to do |format|
         format.html # index.html.erb
         format.json { render :json => @contact }
         format.js { render :js => " $('form#new_contact_us')[0].reset();  $('form#new_contact_us button').text('Send') ;
         $('form#new_contact_us button').removeAttr('disabled');" }
       end
     else
       flash[:error] = 'Contact was not successfully sent.'
       respond_to do |format|
         format.html # index.html.erb
         format.json { render :json => @contact }
         format.js { render :js => "console.log('Contact was not successfully sent.');" }
       end
     end
   end
end
