class RoomsController < AuthorizedController
  respond_to :html, :json, :js
  # GET /rooms
  # GET /rooms.json
  def index
    # set by CanCan @rooms = Room.by_account(current_account.id)
    @datatable = RoomsDatatable.new(view_context,@rooms)
    respond_to do |format|
      format.html
      format.json { render :json => @datatable }
    end
  end

  # GET /rooms/1
  # GET /rooms/1.json
  def show
    # set by CanCan @room = Room.find(params[:id])
    respond_with(@room)
  end

  # GET /rooms/new
  # GET /rooms/new.json
  def new
    @room = Room.new
    @room.account = current_account
    respond_with(@room)
  end

  # GET /rooms/1/edit
  def edit
    # set by CanCan @room = Room.find(params[:id])
    respond_with(@room)
  end

  # POST /rooms
  # POST /rooms.json
  def create
    @room = Room.new(params[:room])

    respond_to do |format|
      if @room.save
        last_used_new(@room)
        format.html { redirect_to(rooms_path, :notice => 'Room created.') }
        format.json  { head :ok }
     else
        format.html { render :action => "new" }
        format.json  { render :json => @room.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @room.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /rooms/1
  # PUT /rooms/1.json
  def update
    # set by CanCan @room = Room.find(params[:id])
    #
    # set the params to be processed based on this being a AJAX request or not
    #  AJAX requests are being used to update the Room position from the Appointments
    #  view.
    if request.xhr? and not params[:room]
      if @room.update_attributes(whitelist(params))
        respond_with(@room)
      else
        respond_with(@room, :status => :unprocessable_entity)
      end
    else
      old_position = @room.position
      respond_to do |format|
        if @room.update_attributes(params[:room])
          Room.reorder(old_position,@room,current_account.rooms)
          format.html { redirect_to(rooms_path, :notice => 'Room was successfully updated.') }
          format.json  { render :json => true }
          # NOTE: if you add a format.js option here the editPopup request would choose that response
        else
          format.html { render :action => "edit" }
          format.json  { render :json => @room.errors, :status => :unprocessable_entity }
          # NOTE: its actually the js format that's being rendered
          #       when called from the editPopup windows, not quite sure why
          format.js { render :json => { :errors => @room.errors }, :status => :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /rooms/1
  # DELETE /rooms/1.json
  def destroy
    # set by CanCan @room = Room.find(params[:id])
    @room.destroy

    respond_to do |format|
      format.json do
        # BLS this is necessary to avoid a 1-byte respons
        #  - see http://stackoverflow.com/questions/7064127/http-post-head-ok-returning-1-mystery-byte
        render :text => "", :status => :ok
      end
      format.html { redirect_to(rooms_path) }
    end
  end

  private
  #-----------------------------------------------------------------------------
  # filters parameters from the request parameters which are valid (safe) from
  # JSON requests.
  #-----------------------------------------------------------------------------
  def whitelist(params)
    params.slice(:name,:position)
  end
end
