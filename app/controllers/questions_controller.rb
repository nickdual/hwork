class QuestionsController < AuthorizedController
  layout 'admin'
  respond_to :html, :json, :js

  def index
    @datatable = QuestionsDatatable.new(view_context,@questions)
    respond_to do |format|
      format.html
      format.json { render :json => @datatable }
    end
  end

  def show
    respond_with(@question)
  end

  def new
    @question = Question.new
    respond_with(@question)
  end

  def edit
    respond_with(@question)
  end

  def create
    @question = Question.new(params[:question])

    respond_to do |format|
      if @question.save
        last_used_new(@question)
        format.html { redirect_to(questions_path, :notice => 'question created.') }
        format.json  { head :ok }
     else
        format.html { render :action => "new" }
        format.json  { render :json => @question.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @question.errors }, :status => :unprocessable_entity }
      end
    end
  end

  def update
    # set by CanCan @question = Question.find(params[:id])
    respond_to do |format|
      if @question.update_attributes(params[:question])
        format.html { redirect_to(questions_path, :notice => 'question was successfully updated.') }
        format.json  { head :ok }
        # NOTE: if you add a format.js option here the editPopup request would choose that response
      else
        format.html { render :action => "edit" }
        format.json  { render :json => @question.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @question.errors }, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @question.destroy

    respond_to do |format|
      format.json do
        render :text => "", :status => :ok
      end
      format.html { redirect_to(questions_path) }
    end
  end
end
