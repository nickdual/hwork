class AccountSetupsController < AuthorizedController
  respond_to :html, :xml, :json

  def edit
    @account_setup = current_account.account_setup
    respond_with(@account_setup)
  end

  def update
    @account_setup = current_account.account_setup
    if ( @account_setup.update_attributes(params[:account_setup]) )
      flash[:notice] = 'Account setup successfully updated.'
      respond_with(@account_setup, :location => edit_account_account_setup_path)
    else
      respond_with(@account_setup, :location => edit_account_account_setup_path, :status => :unprocessable_entity)
    end
  end
end
