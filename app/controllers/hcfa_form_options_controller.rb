class HcfaFormOptionsController < AuthorizedController
  respond_to :html, :json, :js

  # GET /hcfa_form_options
  # GET /hcfa_form_options.json
  def index
    # @hcfa_form_options set by CanCan
    @hcfa_form_options = @hcfa_form_options.where('hcfa_form_options.state = ? or hcfa_form_options.type = ?',current_clinic.state, 'CarrierHcfaFormOption') unless current_clinic.nil?
    @datatable = HcfaFormOptionsDatatable.new(view_context,@hcfa_form_options)
    respond_to do |format|
      format.html
      format.json { render :json => @datatable }
    end
  end

  # GET /hcfa_form_options/1
  # GET /hcfa_form_options/1.json
  def show
    # set by CanCan @hcfa_form_option = HcfaFormOption.find(params[:id])
    respond_with(@hcfa_form_option)
  end

  # GET /hcfa_form_options/1/edit
  def edit
    # set by CanCan @hcfa_form_option = HcfaFormOption.find(params[:id])
    respond_with(@hcfa_form_option)
  end

  # PUT /hcfa_form_options/1
  # PUT /hcfa_form_options/1.json
  def update
    # set by CanCan @hcfa_form_option = HcfaFormOption.find(params[:id])

    respond_to do |format|
      if @hcfa_form_option.update_attributes(params[:hcfa_form_option])
        format.html { redirect_to(hcfa_form_options_path, :notice => 'Billing Setup updated.') }
        format.json  { head :ok }
        # NOTE: if you add a format.js option here the editPopup request would choose that response
      else
        format.html { render :action => "edit" }
        format.json  { render :json => @hcfa_form_option.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @hcfa_form_option.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /hcfa_form_options/1
  # DELETE /hcfa_form_options/1.json
  def destroy
    # set by CanCAn @hcfa_form_option = HcfaFormOption.find(params[:id])
    @hcfa_form_option.destroy

    respond_to do |format|
      format.json do
        # BLS this is necessary to avoid a 1-byte respons
        #  - see http://stackoverflow.com/questions/7064127/http-post-head-ok-returning-1-mystery-byte
        render :text => "", :status => :ok
      end
      format.html { redirect_to(hcfa_form_options_path, :notice => 'Billing Setup removed') }
    end
  end
end
