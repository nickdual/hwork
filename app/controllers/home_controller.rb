class HomeController < ApplicationController
  include SessionHelper
  layout 'application'
  # top level application does not require authorization
  # so it extends the base ApplicationController

  #-----------------------------------------------------------------------------
  # This is the application root. It will direct any authenticated user to the
  # respective plan page.
  #-----------------------------------------------------------------------------
  def index
    if user_signed_in? then
      plan_path = user_plan_path
      flash.keep # don't expire the flash messages on this redirect.
      redirect_to plan_path unless plan_path == root_path
    end
  end

  def contact
  end

  def features
  end

  def faq
    @questions = Question.accessible_by(current_ability)
  end

  def news
  end

  def news_item
  end

  def under_construction
    @location = request.request_uri.gsub(/\//,' ').lstrip
  end

end
