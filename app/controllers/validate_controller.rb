#===============================================================================
# Controller used to do client side validations via remote AJAX calls
# Primarily used for unique elements
#===============================================================================
class ValidateController < ApplicationController
  respond_to :json, :js

  def appointment_type
    respond_to do |format|
      if params[:object] == 'name'
        value = params[:appointment_type][:name]
        id = params[:appointment_type][:id]
        if current_account.appointment_types.where('lower(name) = ? and id != ?', value.downcase, id).empty?
          format.json { render :json => true }
        else
          format.json { render :json => false }
        end
      end
    end
  end

  def diagnosis_code
    respond_to do |format|
      if params[:object] == 'name'
        value = params[:diagnosis_code][:name]
        id = params[:diagnosis_code][:id]
        if current_account.diagnosis_codes.where('lower(name) = ? and id != ?', value.downcase, id).empty?
          format.json { render :json => true }
        else
          format.json { render :json => false }
        end
      end
    end
  end

  def message
    respond_to do |format|
      if params[:object] == 'label'
        value = params[:message][:label]
        id = params[:message][:id]
        if current_account.messages.where('lower(label) = ? and id != ?', value.downcase, id).empty?
          format.json { render :json => true }
        else
          format.json { render :json => false }
        end
      end
    end
  end

  def procedure_code
    respond_to do |format|
      if params[:object] == 'name'
        value = params[:procedure_code][:name]
        id = params[:procedure_code][:id]
        if current_account.procedure_codes.where('lower(name) = ? and id != ?', value.downcase, id).empty?
          format.json { render :json => true }
        else
          format.json { render :json => false }
        end
      end
    end
  end

  def provider
    respond_to do |format|
      if params[:object] == 'your_code'
        value = params[:provider][:your_code]
        id = params[:provider][:id]
        if current_account.providers.where('lower(your_code) = ? and id != ?', value.downcase, id).empty?
          format.json { render :json => true }
        else
          format.json { render :json => false }
        end
      end
      if params[:object] == 'signature_name'
        value = params[:provider][:signature_name]
        id = params[:provider][:id]
        if current_account.providers.where('lower(signature_name) = ? and id != ?', value.downcase, id).empty?
          format.json { render :json => true }
        else
          format.json { render :json => false }
        end
      end
    end
  end

  def reference
    respond_to do |format|
      if params[:object] == 'company_name'
        value = params[:reference][:contact_attributes][:company_name]
        id = params[:reference][:id]
        if current_account.references.joins(:contact).where('lower(contacts.company_name) = ? and references.id != ?', value.downcase, id).empty?
          format.json { render :json => true }
        else
          format.json { render :json => false }
        end
      end
    end
  end

  def room
    respond_to do |format|
      if params[:object] == 'name'
        value = params[:room][:name]
        id = params[:room][:id]
        if current_account.rooms.where('lower(name) = ? and id != ?', value.downcase, id).empty?
          format.json { render :json => true }
        else
          format.json { render :json => false }
        end
      end
    end
  end

  def third_party
    respond_to do |format|
      if params[:object] == 'alias_name'
        value = params[:third_party][:alias_name]
        id = params[:third_party][:id]
        if current_account.third_parties.where('lower(alias_name) = ? and id != ?', value.downcase, id).empty?
          format.json { render :json => true }
        else
          format.json { render :json => false }
        end
      end
    end
  end

  def fee_schedule
  end

  def legacy_id_label
  end

  def user
    respond_to do |format|
      if params[:object] == 'email'
        value = params[:user][:email]
        id = params[:user][:id]
        if User.where('lower(email) = ? and id != ?', value.downcase, id).empty?
          format.json { render :json => true }
        else
          format.json { render :json => false }
        end
      end
      if params[:object] == 'account_role'
        value = params[:user][:account_role]
        id = params[:user][:id]
        if value != Role::ACCOUNT_ADMIN
          begin
            if Role.find_by_name(Role::ACCOUNT_ADMIN).can_revoke?(User.find(id))
              format.json { render :json => true }
            else
              format.json { render :json => false }
            end
          rescue
            format.json { render :json => false }
          end
        else
          # right now we're only checking if admin can be revoked
          format.json { render :json => true }
        end
      end
    end
  end

end
