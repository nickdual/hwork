class ProvidersController < AuthorizedController
  respond_to :html, :json, :js

  # GET /providers
  # GET /providers.json
  def index
    # set by CanCan @providers = Provider.by_account(current_user.account_id).alphabetically
    @datatable = ProvidersDatatable.new(view_context,@providers)
    respond_to do |format|
      format.html
      format.json { render :json => @datatable }
    end
  end
  def getData
    @value = ''
    id = params['p-id']
    @datatable = ProvidersDatatable.new(view_context,@providers)
    @data = JSON.parse(@datatable.to_json)
    @data['aaData'].each do |data|
      if data["DT_RowId"] == id
        @value = data['6']
      end
    end
    respond_to do |format|
      format.html
      format.json { render :json => @value.to_json }
    end
  end
  # GET /providers/1
  # GET /providers/1.json
  def show
    # set by CanCan @provider = Provider.find(params[:id])
    prepare_legacy_ids(@provider, false)
    respond_with(@provider)
  end

  # GET /providers/new
  # GET /providers/new.json
  def new
    @provider = Provider.build
    @provider.contact.build_business_email
    @provider.account = current_account
    prepare_legacy_ids(@provider)
    respond_with(@provider)
  end

  # GET /providers/1/edit
  def edit
    # set by CanCan @provider = Provider.find(params[:id])
    prepare_legacy_ids(@provider)
    respond_with(@provider)
  end

  # POST /providers
  # POST /providers.json
  def create
    @provider = Provider.new(params[:provider])

    respond_to do |format|
      if @provider.save
        last_used_new(@provider)
        complete_setup_for(@provider)
        format.html { redirect_to(providers_path, :notice => 'Provider created.') }
        format.json  { head :ok }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @provider.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @provider.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /providers/1
  # PUT /providers/1.json
  def update
    # set by CanCan @provider = Provider.find(params[:id])

    respond_to do |format|
      if @provider.update_attributes(params[:provider])
        complete_setup_for(@provider)
        format.html { redirect_to(providers_path, :notice => 'Provider was successfully updated.') }
        format.json  { head :ok }
        # NOTE: if you add a format.js option here the editPopup request would choose that response
      else
        format.html { render :action => "edit" }
        format.json { render :json => @provider.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @provider.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /providers/1
  # DELETE /providers/1.json
  def destroy
    # set by CanCan @provider = Provider.find(params[:id])
    @provider.destroy

    respond_to do |format|
      format.json do
        # BLS this is necessary to avoid a 1-byte respons
        #  - see http://stackoverflow.com/questions/7064127/http-post-head-ok-returning-1-mystery-byte
        render :text => "", :status => :ok
      end
      format.html { redirect_to(providers_path) }
    end
  end

  # TODO - move this method to a helper class.
  def prepare_legacy_ids(provider, persist = true)
    @provider.preload_existing_legacy_id_labels
  end
end
