require_dependency 'needs_to'
require_dependency 'errors'

class ApplicationController < ActionController::Base
  include SessionHelper
  include NeedsTo
  protect_from_forgery
  layout :check_set_layout

  rescue_from CanCan::AccessDenied do |e|
    logger.warn "CanCan Access #{e.message} - #{e.action} :: #{e.subject.inspect}"
    unless request.xhr?
      redirect_to root_path, :alert => e.message
    else
      flash[:alert] = e.message
      # push the flash messages into this response.
      flash_headers
    end
  end

  #-----------------------------------------------------------------------------
  # The following code to store page parameters in the session comes from 
  # https://gist.github.com/791291
  #
  # Use a before_filter on index action to remember the current page
  # for will_paginate.  This applies to all controllers.
  #-----------------------------------------------------------------------------
  before_filter :page_params, :only => :index

  def page_key
    (self.class.to_s + "_page").to_sym
  end

  def page_params
    if params[:iDisplayStart] or params[:iDisplayLength]
      session[page_key] = [ params[:iDisplayStart], params[:iDisplayLength] ]
    elsif session[page_key]
      params[:iDisplayStart] = session[page_key][0] 
      params[:iDisplayLength] = session[page_key][1] 
    end
  end
  #-----------------------------------------------------------------------------
  # The following logic is going to persist the id of the "selected" resource
  # specifically to be recorded 
  #   - edit is really the most important one, you'd expect the :update which
  #     follows the edit would always reference the same resource, but
  #     including edit will include cases where the user is only looking at
  #     records.
  #-----------------------------------------------------------------------------
  before_filter :last_used, :only => [:edit, :update, :delete]
  before_filter :last_used_out, :only => :index

  # generate a class specific key to store the most recently used resource id
  # in the session store
  def last_used_key
    (self.class.to_s + "_mru").to_sym
  end

  # automatically store the id of the affected resource before any update or
  # delete action
  def last_used
    if params[:id]
      session[last_used_key] = params[:id]
    end
  end

  # called manually after a successful save is performed on newly created 
  # resources to store their new id as the most recently used for this class
  def last_used_new(resource)
    if resource.try(:id)
      session[last_used_key] = resource.id
    end
  end

  # called before the index view is going to be rendered to set the id
  # last used into a parameter to be referenced by the dataTables class.
  def last_used_out
    params[:mru] = session[last_used_key]
  end

  #-----------------------------------------------------------------------------
  # tip to avoid rendering layouts for AJAX requests
  #   http://jamiedubs.com/always-render-rails-views-without-the-full-layout-when-using-ajax-degradable-javascript
  #   needed to modify because args was coming as empty Array
  #
  # for some reason this method generates an exception "index 67345 out of string"
  # when called during import_group upload or status monitoring. we really didn't
  # need to concern ourselves with layout on either the upload or the status
  # requests, so I added the catch block.
  #-----------------------------------------------------------------------------
  def render(*args)
    if request.xhr?
      begin
        args[0] ||= {}
        args[0][:layout] = false
      rescue
        logger.warn("error occured on ajax request arguments")
      end
    end
    #logger.debug("NOW you've got #{args.inspect}")
    return super(*args)
  end

  #-----------------------------------------------------------------------------
  # Handle the transmission of Rails flash messages to the client with AJAX
  # requests. This suggestion comes from:
  # http://ashleyangell.com/2010/10/handling-rails-flash-message-with-ajax-requests/
  #-----------------------------------------------------------------------------
  after_filter :flash_headers

  def flash_headers
    # This will discontinue execution if Rails detects that the request is not
    # from an AJAX request, i.e. the header wont be added for normal requests
    return unless request.xhr?
    # If we're sending back a redirect the browser is going to handle this 
    # internally, so there's no need to attach the flash message to this 
    # response, by default it'll be attached to the subsequent redirect request.
    return if response.status == 302
    logger.info("convert flash to headers #{response.status}")
   
    # Add the appropriate flash messages to the header, add or remove as
    # needed, but I think you'll get the point
    #response.headers['x-flash'] = flash[:error]  unless flash[:error].blank?
    #response.headers['x-flash'] = flash[:notice]  unless flash[:notice].blank?
    #response.headers['x-flash'] = flash[:warning]  unless flash[:warning].blank?
    response.headers['X-flash'] = flash.to_json unless flash.empty?
   
    # Stops the flash appearing when you next refresh the page
    flash.discard
  end

  #-----------------------------------------------------------------------------
  # to keep track of setup steps which are being completed either intentionally
  # or unintentionally (meaning they're exploring on their own).
  #-----------------------------------------------------------------------------
  def complete_setup_for(resource)
    r_name = resource.kind_of?(String) ? resource : resource.class.name
    if r_name == "Clinic"
      current_account.account_setup.setup_clinic! if current_account.try(:account_setup).try(:setup_clinic)
    elsif r_name == "Provider"
      current_account.account_setup.setup_provider! if current_account.try(:account_setup).try(:setup_provider)
    elsif r_name == "Schedule"
      current_account.account_setup.setup_schedule! if current_account.try(:account_setup).try(:setup_schedule)
    end
  end

  protected

  #-----------------------------------------------------------------------------
  # switch to the appropriate layout depending on authentication state
  #    XXX - might make sense to put the majority of the application views
  #          under the "application" layout, so swap these files.
  #-----------------------------------------------------------------------------
  def check_set_layout
    unless user_signed_in?
      'application'
    else
      unless current_user.app_admin?
        'authorized'
      else
        'admin'
      end
    end
  end

  #-----------------------------------------------------------------------------
  # This method is going to direct the user to the initial screen based on 
  # their subscription plan.
  #-----------------------------------------------------------------------------
  def after_sign_in_path_for(resource)
    if current_user.app_admin?
      accounts_path
    else
      unless current_account.can_login?
        flash[:notice] = current_account.notice
        sign_out current_user
        return root_path
      end
      l = check_needs
      if l == destroy_user_session_path
        sign_out current_user
        l = root_path
      end
      current_account.login! if current_account
      if l.nil?
        # FIXME : this logic should have been working in the account state machine block,but it wasn't
        # account setup
        if current_account.try(:account_setup) and !current_account.account_setup.completed?
          return edit_account_account_setup_path
        end

        return user_plan_path

      else
        return l
      end
      super(resource)
    end
  end

  #-----------------------------------------------------------------------------
  # defines the default (root / index) view for users based on their account
  # access level.
  #-----------------------------------------------------------------------------
  def user_plan_path
    case current_user.account.subscription.subscription_plan_name
    when 'basic'
      return content_basic_path
    when 'plus'
      return content_plus_path
    when 'premium'
      return content_premium_path
    when 'max'
      return content_max_path
    else
      return root_path
    end
  end

  #-----------------------------------------------------------------------------
  # pass additional information to the CanCan Ability class
  #   https://github.com/ryanb/cancan/wiki/accessing-request-data
  #-----------------------------------------------------------------------------
  def current_ability
    @current_ability ||= Ability.new(current_user,current_account.try(:id),current_clinic.try(:id))
  end

  #-----------------------------------------------------------------------------
  # will add the necessary headers to request that a page not be cached
  #   http://stackoverflow.com/questions/711418/how-to-prevent-browser-page-caching-in-rails
  #-----------------------------------------------------------------------------
  def do_not_cache
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

  #-----------------------------------------------------------------------------
  # Override the default metadata collected by paper_trail for versions / 
  # audit logs.
  #-----------------------------------------------------------------------------
  def info_for_paper_trail
    { :ip => request.remote_ip, :account_id => current_account.try(:id) }
  end

end
