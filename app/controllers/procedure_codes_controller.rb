class ProcedureCodesController < AuthorizedController
  respond_to :html, :json, :js

  # GET /procedure_codes
  # GET /procedure_codes.json
  def index
    # set by CanCan @procedure_codes = ProcedureCode.by_account(current_user.account)
    @datatable = ProcedureCodesDatatable.new(view_context,@procedure_codes)
    respond_to do |format|
      format.html
      format.json { render :json => @datatable }
    end
  end

  # GET /procedure_codes/1
  # GET /procedure_codes/1.json
  def show
    # set by CanCan @procedure_code = ProcedureCode.find(params[:id])
    prepare_fee_schedule(@procedure_code, false)
    respond_with(@procedure_code)
  end

  # GET /procedure_codes/new
  # GET /procedure_codes/new.json
  def new
    @procedure_code = ProcedureCode.new
    @procedure_code.account = current_account
    prepare_fee_schedule(@procedure_code)
    respond_with(@procedure_code)
  end

  # GET /procedure_codes/1/edit
  def edit
    # set by CanCan @procedure_code = ProcedureCode.find(params[:id])
    prepare_fee_schedule(@procedure_code)
    respond_with(@procedure_code)
  end

  # POST /procedure_codes
  # POST /procedure_codes.json
  def create
    @procedure_code = ProcedureCode.new(params[:procedure_code])

    respond_to do |format|
      if @procedure_code.save
        last_used_new(@procedure_code)
        format.html { redirect_to(procedure_codes_path, :notice => 'Procedure code created.') }
        format.json  { head :ok }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @procedure_code.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @procedure_code.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /procedure_codes/1
  # PUT /procedure_codes/1.json
  def update
    # set by CanCan @procedure_code = ProcedureCode.find(params[:id])

    respond_to do |format|
      if @procedure_code.update_attributes(params[:procedure_code])
        format.html { redirect_to(procedure_codes_path, :notice => 'Procedure code updated.') }
        format.json  { head :ok }
        # NOTE: if you add a format.js option here the editPopup request would choose that response
      else
        logger.error("procedure code update failed: #{@procedure_code.errors.inspect}")
        format.html { render :action => "edit" }
        format.json  { render :json => @procedure_code.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @procedure_code.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /procedure_codes/1
  # DELETE /procedure_codes/1.json
  def destroy
    # set by CanCan @procedure_code = ProcedureCode.find(params[:id])
    @procedure_code.destroy

    respond_to do |format|
      format.json do
        # BLS this is necessary to avoid a 1-byte respons
        #  - see http://stackoverflow.com/questions/7064127/http-post-head-ok-returning-1-mystery-byte
        render :text => "", :status => :ok
      end
      format.html { redirect_to(procedure_codes_path) }
    end
  end

  # TODO move this method to a helper class.
  def prepare_fee_schedule(procedure_code, persist = true)
    procedure_code.preload_existing_fee_schedules(persist)
  end
end
