class ImportsController < AuthorizedController
  respond_to :html, :json, :js

  # GET /imports
  # GET /imports.json
  def index
    # loaded by CanCan in before_filter. @imports = Import.all
    @datatable = ImportsDatatable.new(view_context,@imports)
    respond_to do |format|
      format.html
      format.json { render :json => @datatable }
    end
  end

  # GET /imports/1
  # GET /imports/1.json
  def show
    # loaded by CanCan in before filter @import = Import.find(params[:id])
    respond_with(@import)
  end

  # GET /imports/new
  # GET /imports/new.json
  def new
    @import = Import.new
    @import.account = current_account
    @import.user = current_user
    @import.import_type = "Patients" # TODO : just a temporary default value
    respond_with(@import)
  end

  # GET /imports/1/edit
  def edit
    # loaded by CanCan in before_filter @import = Import.find(params[:id])
    respond_with(@import)
  end

  # POST /imports
  # POST /imports.json
  def create
    @import = Import.new(params[:import])
    @import.account = current_account
    @import.user = current_user

    respond_to do |format|
      if @import.load_data(current_user, current_clinic, current_account) and @import.save
        last_used_new(@import)
        format.html { redirect_to(imports_path, :notice => "Import #{@import.id} was successfully created.") }
        format.json  { render :json => @import, :status => :created, :location => @import }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @import.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /imports/1
  # PUT /imports/1.json
  def update
    # loaded by CanCan in before filter @import = Import.find(params[:id])

    respond_to do |format|
      if @import.update_attributes(params[:import])
        format.html { redirect_to(imports_path, :notice => 'Import was successfully updated.') }
        format.json  { head :ok }
        # NOTE: if you add a format.js option here the editPopup request would choose that response
      else
        format.html { render :action => "edit" }
        format.json  { render :json => @import.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @import.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /imports/1
  # DELETE /imports/1.json
  def destroy
    # loaded by CanCan in before filter @import = Import.find(params[:id])
    @import.destroy

    respond_to do |format|
      format.html { redirect_to(imports_path, :notice => 'Import was deleted.') }
      format.json  { render :text => "", :status => :ok }
    end
  end
end
