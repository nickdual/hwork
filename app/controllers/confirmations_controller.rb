class ConfirmationsController < Devise::ConfirmationsController
  #-------------------------------------------------------------------------------
  # override the default layout for this controller, because the user will be 
  # signed in when they confirm their account, but we still want them to be 
  # viewing the public view.
  #-------------------------------------------------------------------------------
  layout 'application'
  #-------------------------------------------------------------------------------
  # Devise Confirmation Customization
  #   this method comes form the Devise controller which automatically logs
  #   the person in after confirmation, this is not desireable as it provides
  #   direct account access using an insecure email token.
  #-------------------------------------------------------------------------------
  # GET /resource/confirmation?confirmation_token=abcdef
  def show
    self.resource = resource_class.find_by_confirmation_token(params[:confirmation_token])
    if resource.nil? or resource.confirmed?
      super
    else
      if resource_name == :user
        logger.info("user followed confirmation link #{resource.inspect}")
        resource.build_contact.build if resource.contact.nil?
      end
    end
  end

  def confirm
    self.resource = resource_class.find_by_confirmation_token(params[resource_name][:confirmation_token])
    if ( not resource.nil? ) and resource.update_attributes(params[resource_name]) and resource.password_match?
      self.resource = resource_class.confirm_by_token(params[resource_name][:confirmation_token])
      set_flash_message :notice, :confirmed
      if resource_name == :user
        resource.try(:account).confirm!
      end
      sign_in_and_redirect(resource_name, resource)
    else
      render :action => "show"
    end
  end  

end
