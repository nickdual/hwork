require 'hcfa_form_options_helper'

class CarrierHcfaFormOptionsController < AuthorizedController
  include HcfaFormOptionsHelper
  respond_to :html, :json, :js

  # GET /carrier_hcfa_form_options/1
  # GET /carrier_hcfa_form_options/1.json
  def show
    @third_party = ThirdParty.find(params[:third_party_id])
    # set by CanCan @carrier_hcfa_form_option = CarrierHcfaFormOption.find(params[:id])
    respond_with(@carrier_hcfa_form_option)
  end

  # GET /carrier_hcfa_form_options/new
  # GET /carrier_hcfa_form_options/new.json
  def new
    @third_party = ThirdParty.find(params[:third_party_id])
    # look to see if there is an existing 
    hcfa_defaults = get_hcfa_option(current_account,current_clinic,@third_party.insurance_carrier_type_code, @third_party)
    @carrier_hcfa_form_option = CarrierHcfaFormOption.new(hcfa_defaults.attributes)
    @carrier_hcfa_form_option.third_party = @third_party
    @carrier_hcfa_form_option.account = current_account
    respond_with(@carrier_hcfa_form_option)
  end

  # GET /carrier_hcfa_form_options/1/edit
  def edit
    @third_party = ThirdParty.find(params[:third_party_id])
    # set by CanCan @carrier_hcfa_form_option = CarrierHcfaFormOption.find(params[:id])
    respond_with(@carrier_hcfa_form_option)
  end

  # POST /carrier_hcfa_form_options
  # POST /carrier_hcfa_form_options.json
  def create
    @third_party = ThirdParty.find(params[:third_party_id])
    @carrier_hcfa_form_option = CarrierHcfaFormOption.new(params[:hcfa_form_option])
    @carrier_hcfa_form_option.third_party = @third_party
    @carrier_hcfa_form_option.account = current_account

    respond_to do |format|
      if @carrier_hcfa_form_option.save
        last_used_new(@carrier_hcfa_form_option)
        path = request.xhr? ? third_parties_path : third_party_hcfa_form_option_path(@third_party, @carrier_hcfa_form_option)
        format.html { redirect_to(path, :notice => 'Carrier hcfa form option was successfully created.') }
        format.json  { render :json => @carrier_hcfa_form_option, :status => :created, :location => @carrier_hcfa_form_option }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @carrier_hcfa_form_option.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /carrier_hcfa_form_options/1
  # PUT /carrier_hcfa_form_options/1.json
  def update
    @third_party = ThirdParty.find(params[:third_party_id])
    # set by CanCan @carrier_hcfa_form_option = CarrierHcfaFormOption.find(params[:id])

    respond_to do |format|
      if @carrier_hcfa_form_option.update_attributes(params[:hcfa_form_option])
        path = request.xhr? ? third_parties_path : third_party_hcfa_form_option_path(@third_party, @carrier_hcfa_form_option)
        format.html { redirect_to(path, :notice => 'Carrier hcfa form option was successfully updated.') }
        format.json  { head :ok }
        # NOTE: if you add a format.js option here the editPopup request would choose that response
      else
        format.html { render :action => "edit" }
        format.json  { render :json => @carrier_hcfa_form_option.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @carrier_hcfa_form_option.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /carrier_hcfa_form_options/1
  # DELETE /carrier_hcfa_form_options/1.json
  def destroy
    @third_party = ThirdParty.find(params[:third_party_id])
    # set by CanCAn @carrier_hcfa_form_option = CarrierHcfaFormOption.find(params[:id])
    @carrier_hcfa_form_option.destroy

    respond_to do |format|
      path = request.xhr? ? third_parties_path : third_party_path(@third_party)
      format.json do
        # BLS this is necessary to avoid a 1-byte respons
        #  - see http://stackoverflow.com/questions/7064127/http-post-head-ok-returning-1-mystery-byte
        render :text => "", :status => :ok
      end
      format.html { redirect_to(path, :notice => 'Carrier hcfa form option was destroyed') }
    end
  end
end
