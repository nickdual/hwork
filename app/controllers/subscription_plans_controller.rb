class SubscriptionPlansController < ApplicationController
  layout 'application'

  def index
    @subscriptionplans = SubscriptionPlan.all
  end

  def show
    @subscriptionplan = SubscriptionPlan.find(params[:id])
  end
end
