class PatientsController < AuthorizedController
  respond_to :html, :json, :js
  # GET /patients
  # GET /patients.json
  def index
    # @patients set by CanCan
    if params[:term]
      @patients = Patient.autocomplete(params[:term],@patients,params[:mode]).index_associations()
      respond_with @patients
    elsif params[:lname]
      @patients = @patients.by_last_name(params[:lname]).index_associations()
      respond_with @patients
    else
      logger.info("working with #{@patients.count} patients!")
      @datatable = PatientsDatatable.new(view_context,@patients)
      respond_to do |format|
        format.html
        format.json { render :json => @datatable }
      end
    end
  end

  #-----------------------------------------------------------------------------
  # Returns the list of last_name letters for patients in the current account.
  # OPTIMIZE : the result of the last name letters is cached (redis) and currently cache invalidation is blanket for 'patient' keys after create,update,delete ... the optimal would be to actually keep the last name letters up to date and only invalidate the cache periodically or perhaps only by rake task.
  #-----------------------------------------------------------------------------
  def letters
    respond_with(Patient.letters(current_account))
  end

  # GET /patients/1
  # GET /patients/1.json
  def show
    # set by CanCan @patient = Patient.find(params[:id])
    session[:patient_id] = @patient.id
    respond_with(@patient)
  end

  # GET /patients/new
  # GET /patients/new.json
  def new
    @patient = Patient.build
    @patient.clinic = current_clinic
    @patient.account = current_account
    @patient.employer.contact.physical_address.state = @patient.contact.physical_address.state = current_clinic.try(:state)
    respond_with(@patient)
  end

  # GET /patients/1/edit
  def edit
    # set by CanCan @patient = Patient.find(params[:id])
    session[:patient_id] = @patient.id
    respond_with(@patient)
  end

  # POST /patients
  # POST /patients.json
  def create
    @patient = Patient.new(params[:patient])
    respond_to do |format|
      if @patient.save
        last_used_new(@patient)
        session[:patient_id] = @patient.id
        @patient.account.flush('patient')
        format.html { redirect_to(patients_path, :notice => 'Patient added.') }
        format.json  { head :ok }
      else
        format.html { render :action => "new" }
        format.json  { render :json => @patient.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @patient.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /patients/1
  # PUT /patients/1.json
  def update
    # set by CanCan @patient = Patient.find(params[:id])

    respond_to do |format|
      if @patient.update_attributes(params[:patient])
        @patient.account.flush('patient')
        format.html { redirect_to(patients_path, :notice => 'Patient was successfully updated.') }
        format.json  { head :ok }
        # NOTE: if you add a format.js option here the editPopup request would choose that response
      else
        format.html { render :action => "edit" }
        format.json  { render :json => @patient.errors, :status => :unprocessable_entity }
        # NOTE: its actually the js format that's being rendered
        #       when called from the editPopup windows, not quite sure why
        format.js { render :json => { :errors => @patient.errors }, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /patients/1
  # DELETE /patients/1.json
  def destroy
    # set by CanCan @patient = Patient.find(params[:id])
    @patient.destroy
    @patient.account.flush('patient')

    respond_to do |format|
      format.json do
        # BLS this is necessary to avoid a 1-byte respons
        #  - see http://stackoverflow.com/questions/7064127/http-post-head-ok-returning-1-mystery-byte
        render :text => "", :status => :ok
      end
      format.html { redirect_to(patients_path) }
    end
  end
end
