class CalendarsController < AuthorizedController
  respond_to :html, :json, :js

  #-----------------------------------------------------------------------------
  # This method is primarily a callback for AJAX updates of the calendar data.
  # Its called when the user changes the current calendar view data outside of
  # the date range already sent to the client.
  #
  # Its also called with a version parameter to determine if there are 
  # appointment changes on the server which the client needs to load.
  #-----------------------------------------------------------------------------
  def show
    @calendar = Calendar.find(current_account.calendar.id)
    puts '0'
    if params[:version] #and @calendar.version == params[:version].to_i
      render :json => { :version => @calendar.version }
      return
    end
    puts '1'
    if params[:sDate]
      @calendar.event_selected_date = Date.parse(params[:sDate])
    end
    @calendar.clinic = current_clinic
    puts @calendar.appointments.to_yaml
    puts @calendar.clinic
    puts @calendar.to_yaml
    respond_with(@calendar)

  end

  def edit
    @calendar = Calendar.find(current_account.calendar.id)
    @rooms = current_account.rooms.order(:position, :name)
    @clinics = current_account.clinics
    @providers = current_account.providers.includes(:contact, :appointment_types).order(:signature_name)
    logger.debug("in CalendarsController#edit #{@calendar}")
    respond_with(@calendar)
  end

  def update
    @calendar = Calendar.find(current_account.calendar.id)
    if @calendar.update_attributes(whitelist(params))
      flash[:notice] = 'Calendar was successfully updated.'
      respond_with(@calendar, :location => edit_schedule_path)
    else
      respond_with(@calendar, :location => edit_schedule_path, :status => :unprocessable_entity)
    end
  end

  protected

  def whitelist(params)
    # include :calendar to support non-JSON requests.
    if params[:calendar]
      params[:calendar]
    else
      params.slice(:time_slots_per_hour, :days, :view_by) # put updatable attributes here
    end
  end

end
