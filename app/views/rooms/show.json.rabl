#-------------------------------------------------------------------------------
# Defines the room information that will be transmitted via JSON
#-------------------------------------------------------------------------------
object @room
attributes :id, :name, :position, :appointment_type_id
node(:for_clinics) do |room|
  room.for_clinics
end
node(:enabled) do |room|
  room.enabled
end
node(:providers_seq) do |room|
  room.providers_seq
end
