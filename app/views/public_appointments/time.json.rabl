object @time
node :time do |t|
  t[:time]
end
node :rooms do |t|
  t[:rooms].map { |r| r.id }
end
