object @clinic
attributes :id
node(:name) do |clinic|
  clinic.name
end
node(:address) do |clinic|
  clinic.physical_address_one_liner
end
