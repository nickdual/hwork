#-------------------------------------------------------------------------------
# Defines the appointment type information that will be transmitted via JSON
#-------------------------------------------------------------------------------
object @appointment_type
attributes :id, :name, :duration, :duration_min, :duration_max, :appointment_color
node :providers do |at|
 at.providers.map { |p| p.id }
end
node :rooms do |at|
 at.rooms.map { |r| r.id }
end
