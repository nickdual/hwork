object @calendar
attributes :id, :days, :version
node(:schedule_future_max_days) do |calendar|
  calendar.schedule_future_max_days
end
node(:schedule_history_max_days) do |calendar|
  calendar.schedule_history_max_days
end
node(:event_selected_date) do |calendar|
  calendar.event_selected_date
end
node(:event_start_date) do |calendar|
  calendar.event_start_date
end
node(:event_end_date) do |calendar|
  calendar.event_end_date
end
node(:provider_schedules) do |calendar|
  calendar.provider_schedules
end
node(:appts) do |calendar|
  calendar.appts
end
