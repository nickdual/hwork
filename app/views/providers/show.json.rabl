object @provider
attributes :id, :signature_name, :appointment_color, :tag_line, :your_code
node(:name) do |provider|
  provider.name
end
node(:enabled) do |provider|
  provider.enabled
end
child(:appointment_types) do
  attributes :id
end
