class ImportGroupsDatatable < ResourceDatatable

  def self.columns
    return [
     # disable the sorting of the first column with the link icons. 
     # also, don't include a title on the first column, or it'll clobber the (+) add button
     {
       "bSortable" => false,
       "sWidth" => "50px",
       "aTargets" => [0]
     },
     {
       "sTitle"   => "ID",
       "sWidth"   => "10%",
       "bVisible" => false,
       "aTargets" => [1]
     },
     {
       "sTitle" => "Status",
       "sWidth"   => "10%",
       "aTargets" => [2]
     },
     {
       "sTitle" => "Import Type",
       "sWidth"   => "10%",
       "aTargets" => [3]
     },
     {
       "sTitle" => "User",
       "sWidth"   => "10%",
       "aTargets" => [4]
     },
     {
       "sTitle" => "Record Count",
       "bSortable" => false,
       "sWidth"   => "10%",
       "aTargets" => [5]
     },
     {
       "sTitle" => "Loaded",
       "aTargets" => [6]
     }
   ] + super
  end

private

  def data
    resources.map { |ig|
      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "import-group-#{ig.id}",
        "0" => "",
        "1" => ig.id,
        "2" => ig.status,
        "3" => ig.import_group_type,
        "4" => ig.user.email,
        "5" => ig.finished? ? "#{ig.records_imported} of #{ig.records_total}" : "#{ig.records_imported}",
        "6" => ig.created_at,
        "id" => ig.id,
        "delete" => @view.can?(:destroy, ig) ? @view.import_group_path(ig) : ""
      }
    }
  end

  # first column is for controls, sort on #2
  def sort_column(i)
    columns = %w[controls id status import_group_type user_id id created_at]
    columns[i]
  end

  def search_for(term, relations)
    relations.where("id like :search or status like :search or import_group_type like :search or created_at like :search", :search => "%#{term}%")
  end

end

