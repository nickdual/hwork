class HcfaFormOptionsDatatable < ResourceDatatable

  def self.columns
    return [
     # disable the sorting of the first column with the link icons. 
     # also, don't include a title on the first column, or it'll clobber the (+) add button
     {
       "bSortable" => false,
       "sWidth" => "50px",
       "aTargets" => [0]
     },
     {
       "sTitle"   => "Form Type",
       "sWidth"   => "20%",
       "aTargets" => [1]
     },
     {
       "sTitle" => "Carrier",
       "aTargets" => [2]
     }
   ] + super
  end

private

  def data
    resources.map { |pc|
      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "hcfa_form_option-#{pc.id}",
        "0" => "",
        "1" => pc.form_type_code_key,
        "2" => pc.third_party.try(:name),
        "id" => pc.id,
        "edit" => @view.can?(:update, pc) ? @view.edit_hcfa_form_option_path(pc) : "",
        "delete" => (pc.third_party.nil? or @view.cannot?(:destroy, pc)) ? "" : @view.hcfa_form_option_path(pc)
      }
    }
  end

  def sort_column(i)
    columns = %w[controls form_type_code contacts.company_name]
    columns[i]
  end

  # FIXME : this was causing an INNER join to be used when what we're wanting is a LEFT OUTER join
  #       : as a result, we were not seeing the state default HCFA Form configurations returned in
  #       : the list. These sort_em / search_for methods really only need to be fully functional
  #       : if the HCFA Forms records for an account exceed 250, otherwise they only get applied
  #       : indirectly during the initial page load. Since I don't think this is an immediate 
  #       : need: 25 state accounts would do it, I'm just commenting out for now.
  #def sort_em(resources)
  #  resources.joins(:third_party => [:contact]).order(get_sorting())
  #end

  #def search_for(term, relations)
  #  relations.joins(:third_party => [:contact]).where("form_type_code like :search or contacts.company_name like :search", :search => "%#{term}%")
  #end

end

