class QuestionsDatatable < ResourceDatatable

  def self.columns
    return [
     # disable the sorting of the first column with the link icons. 
     # also, don't include a title on the first column, or it'll clobber the (+) add button
     {
       "bSortable" => false,
       "sWidth" => "50px",
       "aTargets" => [0]
     },
     {
       "sTitle"   => "Title",
       "sWidth"   => "25%",
       "aTargets" => [1]
     },
     {
       "sTitle"   => "State",
       "sWidth"   => "10%",
       "aTargets" => [2]
     },
     
     {
       "sTitle" => "Answer",
       "aTargets" => [3]
     }
   ] + super
  end

private

  def data
    resources.map { |question|
      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "question-#{question.id}",
        "0" => "",
        "1" => question.title,
        "2" => question.state,
        "3" => @view.truncate(question.body,:length => 120).gsub(/\s+/,' '),
        "id" => question.id,
        "edit" => @view.can?(:update, question) ? @view.edit_question_path(question) : "",
        "delete" => @view.can?(:destroy, question) ? @view.question_path(question) : ""
      }
    }
  end

  # first column is for controls, sort on #2
  def sort_column(i)
    columns = %w[controls title state body]
    columns[i]
  end

  def search_for(term, relations)
    relations.where("title like :search or body like :search", :search => "%#{term}%")
  end

end

