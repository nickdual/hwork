class HelpsDatatable < ResourceDatatable

  def self.columns
    return [
     # disable the sorting of the first column with the link icons. 
     # also, don't include a label on the first column, or it'll clobber the (+) add button
     {
       "bSortable" => false,
       "sWidth" => "50px",
       "aTargets" => [0]
     },
     {
       "sTitle"   => "Label",
       "sWidth"   => "30%",
       "aTargets" => [1]
     },
     {
       "sTitle"   => "Title",
       "sWidth"   => "30%",
       "aTargets" => [2]
     },
     {
       "sTitle" => "Updated",
       "aTargets" => [3]
     }
   ] + super
  end

private

  def data
    resources.map { |help|
      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "help-#{help.id}",
        "0" => "",
        "1" => help.label,
        "2" => help.title,
        "3" => help.updated_at,
#        "3" => @view.truncate(help.body,:length => 120).gsub(/\s+/,' '),
        "id" => help.id,
        "edit" => @view.can?(:update, help) ? @view.edit_help_path(help) : "",
        "delete" => @view.can?(:destroy, help) ? @view.help_path(help) : ""
      }
    }
  end

  # first column is for controls, sort on #2
  def sort_column(i)
    columns = %w[controls label title body]
    columns[i]
  end

  def search_for(term, relations)
    relations.where("label like :search or title like :search or body like :search", :search => "%#{term}%")
  end

end

