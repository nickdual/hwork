class ProcedureCodesDatatable < ResourceDatatable

  def self.columns
    return [
     # disable the sorting of the first column with the link icons. 
     # also, don't include a title on the first column, or it'll clobber the (+) add button
     {
       "bSortable" => false,
       "sWidth" => "50px",
       "aTargets" => [0]
     },
     {
       "sTitle"   => "Procedure",
       "sWidth"   => "11%",
       "aTargets" => [1]
     },
     {
       "sTitle" => "CPT",
       "sWidth"   => "9%",
       "aTargets" => [2]
      },
     {
       "sTitle" => "Code Category",
       "sWidth"   => "18%",
       "aTargets" => [3]
      },
     {
       "sTitle" => "Type of Service",
       "sWidth"   => "12%",
       "aTargets" => [4]
      },
     {  "sTitle" => "Tax %",
       "sWidth"   => "8%",
       "aTargets" => [5]
      },
     {
       "sTitle" => "Description",
       "aTargets" => [6]
     }
   ] + super
  end

private

  def data
    resources.map { |pc|
      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "procedure_code-#{pc.id}",
        "0" => "",
        "1" => pc.name,
        "2" => pc.cpt_code,
        "3" => pc.type_code_key,
        "4" => pc.service_type_code_key,
        "5" => pc.tax_rate_percentage,
        "6" => pc.description,
        "id" => pc.id,
        "edit" => @view.can?(:update, pc) ? @view.edit_procedure_code_path(pc) : "",
        "delete" => @view.can?(:destroy, pc) ? @view.procedure_code_path(pc) : ""
      }
    }
  end

  # first column is for controls, sort on #2
  def sort_column(i)
    columns = %w[controls name cpt_code type_code service_type_code tax_rate_percentage description]
    columns[i]
  end

  def search_for(term, relations)
    relations.where("name like :search or description like :search or cpt_code like :search", :search => "%#{term}%")
  end

end

