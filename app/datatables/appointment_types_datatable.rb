class AppointmentTypesDatatable < ResourceDatatable

  def self.columns
    return [
     # disable the sorting of the first column with the link icons. 
     # also, don't include a title on the first column, or it'll clobber the (+) add button
     {
       "bSortable" => false,
       "sWidth" => "50px",
       "aTargets" => [0]
     },
     {
       "sTitle"   => "Appointment Type".titleize(),
#       "sWidth"   => "13%",
       "aTargets" => [1]
     },
#     {
#       "sTitle" => AppointmentType.human_attribute_name(:description).titleize(),
#       "aTargets" => [2]
#     },
    #{
    #   "sTitle" => "Lead Time".titleize(),
    #   "sWidth"   => "10%",
    #   "sType" =>  "numeric",
    #   "aTargets" => [2]
    # },
    # {
    #   "sTitle" => "Pre Gap".titleize(),
    #   "sWidth"   => "8%",
    #   "sType" =>  "numeric",
    #   "aTargets" => [3]
    # },
    # {
    #   "sTitle" => "Post Gap".titleize(),
    #   "sWidth"   => "9%",
    #   "sType" =>  "numeric",
    #   "aTargets" => [4]
    # },
#     {
#       "sTitle" => "Min Time",
#       "sWidth"   => "9%",
#       "sType" =>  "numeric",
#       "aTargets" => [2]
#     },
     {
       "sTitle" => "Typical Time",
       "sWidth"   => "12%",
       "sType" =>  "numeric",
       "aTargets" => [2]
     },
#     {
#       "sTitle" => "Max Time",
#       "sWidth"   => "10%",
#       "sType" =>  "numeric",
#       "aTargets" => [4]
#     },
     {
       "sTitle" => "New Patients can schedule?".titleize(),
       "sWidth"   => "20%",
       "aTargets" => [3]
     },
     {
       "sTitle" => "Public can schedule?".titleize(),
       "sWidth"   => "17%",
       "aTargets" => [4]
     }
   ] + super
  end

private

  def data
    resources.map { |at|
      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "appointment_type-#{at.id}",
        "0" => "",
        "1" => at.name,
#        "1" => at.description,
#        "2" => at.schedule_lead_time,
#        "3" => at.pre_appointment_gap,
#        "4" => at.post_appointment_gap,
#        "2" => at.duration_min,
        "2" => at.duration,
#        "4" => at.duration_max,
        "3" => @view.t(at.for_new_patients.to_s),
        "4" => @view.t(at.is_public.to_s),
        "id" => at.id,
        "edit" => @view.can?(:update, at) ? @view.edit_appointment_type_path(at) : "",
        "delete" => @view.can?(:destroy, at) ? @view.appointment_type_path(at) : ""
      }
    }
  end

  # first column is for controls, sort on #2
  def sort_column(i)
    columns = %w[controls name duration is_public]
    columns[i]
  end


end


