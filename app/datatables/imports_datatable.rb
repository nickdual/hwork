class ImportsDatatable < ResourceDatatable

  def self.columns
    return [
     # disable the sorting of the first column with the link icons. 
     # also, don't include a title on the first column, or it'll clobber the (+) add button
     {
       "bSortable" => false,
       "sWidth" => "50px",
       "aTargets" => [0]
     },
     {
       "sTitle"   => "Id",
       "sWidth"   => "15%",
       "aTargets" => [1]
     },
      {
       "sTitle"   => "User",
       "bSortable" => false,
       "sWidth"   => "20%",
       "aTargets" => [2]
     },
    {
       "sTitle" => "Import Type",
       "aTargets" => [3]
     },
      {
       "sTitle"   => "Record Count",
       "bSortable" => false,
       "sWidth"   => "20%",
       "aTargets" => [4]
     },
      {
       "sTitle"   => "Import Group",
       "bSortable" => false,
       "sWidth"   => "10%",
       "aTargets" => [5]
     },
    {
       "sTitle" => "Loaded",
       "sWidth"   => "15%",
       "aTargets" => [6]
     }
   ] + super
  end

private

  def data
    resources.map { |import|
      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "import-#{import.id}",
        "0" => "",
        "1" => import.id,
        "2" => import.user.email,
        "3" => import.import_type,
        "4" => "#{import.records_imported} of #{import.records_total}",
        "5" => import.import_group_id,
        "6" => import.created_at,
        "id" => import.id,
        "edit" => @view.can?(:update, import) ? @view.edit_import_path(import) : "",
        "delete" => @view.can?(:destroy, import) ? @view.import_path(import) : ""
      }
    }
  end

  # TODO : the following code snippent was orphaned when converting to the 
  #        dataTables based index view of import.
  #- if params[:expert]
  #  - if can? :show, import and import.errors?
  #    = link_to 'View Errors', import

  # first column is for controls, sort on #2
  def sort_column(i)
    columns = %w[controls name id id]
    columns[i]
  end

  def search_for(term, relations)
    super(term,relations)
  end

end
