class ProvidersDatatable < ResourceDatatable

  def self.columns
    return [
     # disable the sorting of the first column with the link icons. 
     # also, don't include a title on the first column, or it'll clobber the (+) add button
     {
       "bSortable" => false,
       "sWidth" => "50px",
       "aTargets" => [0]
     },
     {
       "sTitle"   => "Provider",
       "sWidth"   => "10%",
       "aTargets" => [1]
     },
     {
        "sTitle" => "Name",
        "sWidth"   => "18%",
        "aTargets" => [2]
     },
     {
        "sTitle" => "Nickname",
        "sWidth"   => "10%",
        "aTargets" => [3]
     },
     {
       "sTitle" => "Speciality",
       "sWidth"   => "15%",
       "aTargets" => [4]
     },
     {
       "sTitle" => "NPI",
       "sWidth"   => "12%",
       "aTargets" => [5]
     },
     {
       "sTitle" => "Schedule",
       "bSortable" => false,
       "aTargets" => [6]
      }
   ] + super
  end

private

  def data
    # check if we're doing schedules or resource listing
    availability = /schedule\// =~ request.fullpath

    resources.map { |provider|
      # if we're doing schedules, then setup the edit link to 
      # take them to the the schedule directly, and also take
      # away the delete option which doesn't make any sense from
      # the schedules view.
      if availability
        edit_link = @view.edit_provider_schedule_path(provider)
        delete_link = ""
        alt_link = ""
      else
        edit_link = @view.edit_provider_path(provider)
        delete_link = @view.provider_path(provider)
        alt_link = @view.edit_provider_schedule_path(provider)
      end

      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "provider-#{provider.id}",
        "0" => "",
        "1" => provider.your_code,
        "2" => provider.signature_name,
        "3" => provider.try(:tag_line),
        "4" => provider.provider_type_code,
        "5" => provider.npi_uid,
        "6" => provider.try(:schedule).try(:summary),
        "id" => provider.id,
        "edit" => @view.can?(:update, provider) ? edit_link : "",
        "delete" => @view.can?(:destroy, provider) ? delete_link : "",
        "alt" => @view.can?(:update, provider) ? alt_link : ""
      }
    }
  end

  # first column is for controls, sort on #2
  def sort_column(i)
    columns = %w[controls your_code provider_type_code npi_uid signature_name]
    columns[i]
  end

  # define the search process for this resource
  def search_for(term,relation)
    relation.where("your_code like :search or signature_name like :search", :search => "%#{term}%")
  end

end


