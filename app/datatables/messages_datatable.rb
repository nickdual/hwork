class MessagesDatatable < ResourceDatatable

  def self.columns
    return [
     # disable the sorting of the first column with the link icons. 
     # also, don't include a title on the first column, or it'll clobber the (+) add button
     {
       "bSortable" => false,
       "sWidth" => "50px",
       "aTargets" => [0]
     },
     {
       "sTitle"   => "Message",
       "sWidth"   => "18%",
       "aTargets" => [1]
     },
     {
       "sTitle"   => "Type",
       "sWidth"   => "10%",
       "aTargets" => [2]
     },
     
     {
       "sTitle" => "Body",
       "aTargets" => [3]
     }
   ] + super
  end

private

  def data
    resources.map { |message|
      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "message-#{message.id}",
        "0" => "",
        "1" => message.label,
        "2" => message.message_format,
        "3" => @view.truncate(message.body,:length => 120),
        "id" => message.id,
        "edit" => @view.can?(:update, message) ? @view.edit_message_path(message) : "",
        "delete" => @view.can?(:destroy, message) ? @view.message_path(message) : ""
      }
    }
  end

  # first column is for controls, sort on #2
  def sort_column(i)
    columns = %w[controls label body]
    columns[i]
  end

  def search_for(term, relations)
    relations.where("label like :search or body like :search", :search => "%#{term}%")
  end

end

