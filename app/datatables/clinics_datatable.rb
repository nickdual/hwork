class ClinicsDatatable < ResourceDatatable

  def self.columns
    return [
     # disable the sorting of the first column with the link icons. 
     # also, don't include a title on the first column, or it'll clobber the (+) add button
     {
       "bSortable" => false,
       "sWidth" => "50px",
       "aTargets" => [0]
     },
     {
       "sTitle"   => "Location / Clinic",
       "sWidth"   => "20%",
       "aTargets" => [1]
     },
     {
       "sTitle" => "Address",
       "aTargets" => [2]
     },
     {
       "sTitle" => "Default Provider",
       "sWidth"   => "20%",
       "aTargets" => [3]
     },
     {
       "sTitle" => "NPI",
       "sWidth"   => "13%",
       "aTargets" => [4]
     }
   ] + super
  end

private

  def data
    clinic_count = resources.count
    resources.map { |clinic|
      edit_link = @view.edit_clinic_path(clinic)
      delete_link = @view.clinic_path(clinic)

      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "clinic-#{clinic.id}",
        "0" => "",
        "1" => clinic.name,
        "2" => clinic.physical_address_one_liner,
        "3" => clinic.new_patient_option.try(:default_provider).try(:signature_name),
        "4" => clinic.type_ii_npi_uuid,
        "id" => clinic.id,
        "edit" => @view.can?(:update, clinic) ? edit_link : "",
        "delete" => @view.can?(:destroy, clinic) && clinic_count > 1 ? delete_link : ""
      }
    }
  end

  # first column is for controls, sort on #2
  def sort_column(i)
    columns = %w[controls contacts.company_name addresses.street type_ii_npi_uuid]
    columns[i]
  end

  def sort_em(resources)
    resources.joins(:contact => [:physical_address]).order(get_sorting())
  end

  # define the search process for this resource
  def search_for(term,relation)
    relation.joins(:contact).where("contacts.company_name like :search or addresses.street like :search", :search => "%#{term}%")
  end

end

