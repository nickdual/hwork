class ReferencesDatatable < ResourceDatatable

  def self.columns
    return [
     # disable the sorting of the first column with the link icons. 
     # also, don't include a title on the first column, or it'll clobber the (+) add button
     {
       "bSortable" => false,
       "sWidth" => "50px",
       "aTargets" => [0]
     },
     {
       "sTitle"   => "Referral Source",
       "sWidth"   => "15%",
       "aTargets" => [1]
     },
     {
       "sTitle" => "NPI",
       "sWidth"   => "15%",
       "aTargets" => [2]
     },    
     {
       "sTitle" => "Name",
       "aTargets" => [3]
     }
   ] + super
  end

private

  def data
    resources.map { |ref|
      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "reference-#{ref.id}",
        "0" => "",
        "1" => ref.company_name,
        "2" => ref.npi,
        "3" => "#{ref.contact.first_name} #{ref.contact.last_name}",
        "id" => ref.id,
        "edit" => @view.can?(:update, ref) ? @view.edit_reference_path(ref) : "",
        "delete" => @view.can?(:destroy, ref) ? @view.reference_path(ref) : ""
      }
    }
  end

  # first column is for controls, sort on #2
  def sort_column(i)
    columns = %w[controls contacts.company_name npi contacts.last_name]
    columns[i]
  end

  def sort_em(resources)
    resources.joins(:contact).order(get_sorting())
  end

  def search_for(term, relations)
    relations.joins(:contact).where("npi like :search or contacts.company_name like :search or contacts.last_name like :search", :search => "%#{term}%")
  end

end


