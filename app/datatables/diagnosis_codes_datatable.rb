class DiagnosisCodesDatatable < ResourceDatatable

  def self.columns
    return [
     # disable the sorting of the first column with the link icons. 
     # also, don't include a title on the first column, or it'll clobber the (+) add button
     {
       "bSortable" => false,
       "sWidth" => "50px",
       "aTargets" => [0]
     },
     {
       "sTitle"   => "Diagnosis",
       "sWidth"   => "13%",
       "aTargets" => [1]
     },
     {
       "sTitle" => "ICD-9",
       "sWidth"   => "10%",
       "aTargets" => [2]
     },
#     {
#       "sTitle" => "ICD-10",
#       "sWidth"   => "10%",
#       "aTargets" => [3]
#     },
     {
       "sTitle" => "Description",
       "aTargets" => [3]
     }
   ] + super
  end

private

  def data
    resources.map { |dc|
      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "diagnosis_code-#{dc.id}",
        "0" => "",
        "1" => dc.name,
        "2" => dc.code,
       # "3" => dc.icd10,
        "3" => dc.description,
        "id" => dc.id,
        "edit" => @view.can?(:update, dc) ? @view.edit_diagnosis_code_path(dc) : "",
        "delete" => @view.can?(:destroy, dc) ? @view.diagnosis_code_path(dc) : ""
      }
    }
  end

  # first column is for controls, sort on #2
  def sort_column(i)
    columns = %w[controls name code description]
#    columns = %w[controls name code icd10 description]
    columns[i]
  end

  def search_for(term, relations)
#    relations.where("name like :search or code like :search or description like :search or icd10 like :search", :search => "%#{term}%")
    relations.where("name like :search or code like :search or description like :search", :search => "%#{term}%")
  end

end

