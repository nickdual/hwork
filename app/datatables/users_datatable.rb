class UsersDatatable < ResourceDatatable

  def self.columns
    return [
     # disable the sorting of the first column with the link icons. 
     # also, don't include a title on the first column, or it'll clobber the (+) add button
     {
       "bSortable" => false,
       "sWidth" => "75px",
       "aTargets" => [0]
     },
     {
       "sTitle"   => "User",
       "sWidth"   => "20%",
       "aTargets" => [1]
     },
     {
       "sTitle" => "Phone",
       "bSortable" => false,
       "sWidth"   => "18%",
       "aTargets" => [2]
     },
     {
       "sTitle" => "Email",
       "aTargets" => [3]
     },
     {
       "sTitle" => "Admin?",
       "sWidth" => "4%",
       "aTargets" => [4]
     },
     {
       "bSortable" => false,
       "sTitle" => "Patients",
       "sWidth" => "4%",
       "aTargets" => [5]
      },
      {
       "bSortable" => false,
       "sTitle" => "Calendar",
       "sWidth" => "4%",
       "aTargets" => [6]
      },
      {
       "bSortable" => false,
       "sTitle" => "Support",
       "sWidth" => "4%",
       "aTargets" => [7]
      },
      {
       "bSortable" => false,
       "sTitle" => "Reports",
       "sWidth" => "4%",
       "aTargets" => [8]
      },
      {
       "bSortable" => false,
       "sTitle" => "Billing",
       "sWidth" => "4%",
       "aTargets" => [9]
      },
      {
       "bSortable" => false,
       "sTitle" => "System",
       "sWidth" => "4%",
       "aTargets" => [10]
      }
   ] + super
  end

private

  #-----------------------------------------------------------------------------
  # make it more convenient to lookup the top level permissions groups
  # the group should be in ('PatientData','SupportData','Reports','Billing',
  # 'SystemSetup', 'Schedule')
  #-----------------------------------------------------------------------------
  def access_level(user, group)
    #debugger
    p = Permission.new()
    a_level = 'none'
    p.children.each do |perm|
      if perm.resource.to_s == group
        a_level = Permission.access_level(user,perm)
        break
      end
    end
    if a_level == 'full'
      style = 'success'
    elsif a_level == 'partial'
      style = 'warning'
    else
      style = 'inverse'
    end
    return @view.bootstrap_badge(a_level, style)
  end


  def data
    resources.map { |u|
      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "user-#{u.id}",
        "0" => "",
        "1" => u.try(:contact).try(:first_name),
        "2" => u.try(:contact).try(:phone),
        "3" => u.email,
        "4" => @view.bootstrap_badge(I18n.translate("#{u.admin?}"),u.admin? ? 'success' : nil),
        "5" => access_level(u,'PatientData'),
        "6" => access_level(u,'Calendar'),
        "7" => access_level(u,'SupportData'),
        "8" => access_level(u,'Reports'),
        "9" => access_level(u,'Billing'),
        "10" => access_level(u,'SystemSetup'),
        "id" => u.id,
        "edit" => @view.can?(:update, u) ? @view.edit_user_path(u) : "",
        "delete" => @view.can?(:destroy, u) ? @view.user_path(u) : ""
      }
    }
  end

  # first column is for controls, sort on #2
  def sort_column(i)
    #columns = %w[controls first_name, phone, email]
    columns = %w[controls first_name]
    #columns[i]
  end

  def search_for(term, relations)
    relations.joins(:contact).where("email like :search or contacts.last_name like :search or contacts.first_name like :search", :search => "%#{term}%")
  end

end



