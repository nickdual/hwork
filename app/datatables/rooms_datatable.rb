class RoomsDatatable < ResourceDatatable

  def self.columns
    return [
     # disable the sorting of the first column with the link icons. 
     # also, don't include a title on the first column, or it'll clobber the (+) add button
     {
       "bSortable" => false,
       "sWidth" => "50px",
       "aTargets" => [0]
     },
     {
       "sTitle" => I18n.translate('activerecord.attributes.room.name'),
       "sWidth"   => "15%",
       "aTargets" => [1]
     },
      {
       "sTitle"   => "Room Order",
       "bSortable" => true,
       "sWidth"   => "14%",
       "aTargets" => [2]
     },
      {
       "sTitle"   => "Location / Clinic",
       "bSortable" => false,
       "sWidth"   => "35%",
       "aTargets" => [3]
     },
    {
       "sTitle" => I18n.translate('activerecord.attributes.room.appointment_type'),
       "bSortable" => false,
       "aTargets" => [4]
     }
   ] + super
  end

private

  def data
    resources.map { |room|
      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "room-#{room.id}",
        "0" => "",
        "1" => room.name,
        "2" => room.try(:position) + 1,
        "3" => room.clinics.map { |c| c.name }.join(','),
        "4" => room.try(:appointment_type).try(:name),
        "id" => room.id,
        "edit" => @view.can?(:update, room) ? @view.edit_room_path(room) : "",
        "delete" => @view.can?(:destroy, room) ? @view.room_path(room) : ""
      }
    }
  end

  # first column is for controls, sort on #2
  def sort_column(i)
    columns = %w[controls name id id]
    columns[i]
  end

  def search_for(term, relations)
    super(term,relations)
  end

end
