class ThirdPartiesDatatable < ResourceDatatable

  def self.columns
    return [
     # disable the sorting of the first column with the link icons. 
     # also, don't include a title on the first column, or it'll clobber the (+) add button
     {
       "bSortable" => false,
       "sWidth" => "50px",
       "aTargets" => [0]
     },
     {
       "sTitle"   => '3rd Party',
       "sWidth"   => "15%",
       "aTargets" => [1]
     },
     {
       "sTitle" => ThirdParty.human_attribute_name(:carrier_type_code).titleize(),
       "sWidth"   => "14%",
       "aTargets" => [2]
     },
     {
       "sTitle" => ThirdParty.human_attribute_name(:name).titleize(),
       "sWidth"   => "35%",
       "aTargets" => [3]
     },
     {
       "sTitle" => ThirdParty.human_attribute_name(:plan_name).titleize(),
       "aTargets" => [4]
     }
   ] + super
  end

private

  def data
    # we're applying some eager loading with the index_associations() method
    resources.index_associations().map { |tp|
      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "third_party-#{tp.id}",
        "0" => "",
        "1" => tp.alias_name,
        "2" => APP_CONFIG['options']['carrier_types'].invert[tp.insurance_carrier_type_code],
        "3" => tp.name,
        "4" => tp.plan_name,
        "di" => tp.id,
        "edit" => @view.can?(:update, tp) ? @view.edit_third_party_path(tp) : "",
        "delete" => @view.can?(:destroy, tp) ? @view.third_party_path(tp) : ""
      }
    }
  end

  # first column is for controls, sort on #2
  def sort_column(i)
    columns = %w[controls alias_name insurance_carrier_type_code contacts.company_name plan_name]
    columns[i]
  end

  def sort_em(resources)
    resources.joins(:contact => [:physical_address]).order(get_sorting())
  end

  def search_for(term, relations)
    relations.where("alias_name like :search or plan_name like :search", :search => "%#{term}%")
  end

end


