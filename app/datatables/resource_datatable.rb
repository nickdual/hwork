class ResourceDatatable
  delegate :request, :params, :h, :link_to, :number_to_currency, :to => :@view

  DYNAMIC_THRESHOLD_LIMIT = 250
  DEFAULT_PER_PAGE        = 100

  #-----------------------------------------------------------------------------
  # Return a data structure for aoColumns
  #   returns a JSON structure containing information compatible with the 
  #   aoColumnDefs parameter. The table structure will be created with 10
  #   columns that can be used. by default column 0 contains controls
  #   and the remaining columns will be invisible.
  #
  #   this would be a place to define any defaults across all the tables.
  #-----------------------------------------------------------------------------
  def self.columns 
    [ ]
  end

  #-----------------------------------------------------------------------------
  # return the count of columns required to display the datatable. its using
  # the aoColumnDefs result provided by self.columns method, so its fallible if
  # this definition changes in such a way that the total number of columns
  # does not equal the total number of records in the array, which is quite 
  # possible and easy to do.
  #-----------------------------------------------------------------------------
  def self.column_count
    columns.size
  end

  #-----------------------------------------------------------------------------
  # initialize this helper for datatables server side processing with a 
  # Rails view_context and the res, the array of resources which the current 
  # user has access to.
  #-----------------------------------------------------------------------------
  def initialize(view,res)
    @view = view
    @resources = res
  end

  #-----------------------------------------------------------------------------
  # determines the appropriate display start based on the mru or idisplayStart
  # parameter. We only want to apply the mru focus logic for the initial new
  # request from the client, this will include the initial request that's made
  # after creating or updating a record. So on these requests there will also
  # be a parameter aaMru sent from the client, if that parameter is not present
  # the request is probably coming from pagination or sorting on the client 
  # side which we don't want to apply the mru focus.
  #-----------------------------------------------------------------------------
  def display_start
    n = params[:iDisplayStart].to_i
    if params[:mru] and params[:aaMru]
      idx = get_mru_rank(params[:mru].to_i)
      if idx < n or idx >= n + per_page
        n = (idx / per_page).to_i * per_page
      end
    end
    return n
  end

  #-----------------------------------------------------------------------------
  # calculate the current page to be displayed based on the index sent to the
  # server from the datatables plugin.
  #-----------------------------------------------------------------------------
  def page
    display_start/per_page + 1
  end

  #-----------------------------------------------------------------------------
  # references the current pages size provided by the server.
  #-----------------------------------------------------------------------------
  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : DEFAULT_PER_PAGE
  end

  #-----------------------------------------------------------------------------
  # return the i'th sort columns direction, asc by default
  #-----------------------------------------------------------------------------
  def sort_direction(i)
    params["sSortDir_#{i}"] == "desc" ? "desc" : "asc"
  end

  #-----------------------------------------------------------------------------
  # produce the JSON response in the format expected by dataTables plugin
  #-----------------------------------------------------------------------------
  def as_json(options = {})
    {
      :sEcho => params[:sEcho].to_i,
      :iTotalRecords => resources.count,
      :iTotalDisplayRecords => resources.total_entries,
      :iDisplayStart => display_start,
      :iDisplayLength => per_page,
      :mru => params[:mru],
      :aaData => data
    }
  end

  #-----------------------------------------------------------------------------
  # Apply the threshold logic and determine if this table should be processed
  # on the client side based on total record count. If the record count is above
  # the threshold, this must emit the option overrides necessary to enable
  # the server side processing.
  #
  # you can pass in additional option you want to customize the datatables 
  # view, for example initial sort / direction columns.
  #-----------------------------------------------------------------------------
  def html_as_json(options = {})
    res = options
    res ||= {}
    # set the attributes which are common to both client side and server side
    # processing.
    res[:aoColumnDefs] = self.class.columns
    res[:hwColCount] = self.class.column_count
    res[:iDisplayStart] = display_start
    res[:iDisplayLength] = per_page

    # threshold check to see if we'd be better of using dynamic server 
    # requests to process the data table records.
    if @resources.count >= DYNAMIC_THRESHOLD_LIMIT
      # configure the client for server side processing.
      res[:bServerSide] = true
    else
      # configure the client for client side processing which will include
      # sending it the aaData with the original HTML request. we'll also
      # go through and enable sorting for all columns which is "trivial"
      # once the data's been sent to the client.
      res[:bServerSide] = false

      # ensure the list of records is not filtered when we're sending directly
      # to the client, without this logic, it would only send 10 records.
      # the default for 1 page of data.
      resources(false)
      res[:aaData] = data
      res[:mru] = params[:mru]

      ## set all columns (except 0) as sortable for client side processing
      #res[:aoColumnDefs].each do |col|
      #  col["bSortable"] = true unless col["aTargets"].include?(0)
      #end
    end

    return res
  end


private

  # you must override this method
  def sort_column(i)
    columns = %w[col0 col1 col2 col3 col4 col5]
    columns[i]
  end

  # you must override this method
  def data
    resources.map { |res|
      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "#{res.id}",
        "0" => ""
      }
    }
  end

  # you must (most likely) override this method
  #   its just looking for the search term in a column "name" which
  #   may or may not be applicable to your model.
  def search_for(term, relation)
    relation.where("name like :search", :search => "%#{term}%")
  end

  #-----------------------------------------------------------------------------
  # look up the resource list. this will apply the searching / pagination 
  # filters if responded to a dynamic request; however, when selecting the
  # resources for small tables, we need to skip the search and pagination
  # filtering logic so the client will get a full list of records.
  #-----------------------------------------------------------------------------
  def resources(filtering = true)
    @_resources ||= fetch_resources(filtering)
  end

  #-----------------------------------------------------------------------------
  # build a SQL expression for the sorting on the table. this can include 
  # multiple columns if the user has been "shift" clicking to create a 
  # multi-column effect.
  #-----------------------------------------------------------------------------
  def get_sorting
    col_cnt = params[:iSortingCols]
    col_cnt ||= 0
    results = []
    i = 0
    until i >= col_cnt.to_i do
      col = params["iSortCol_#{i}"].to_i
      results << "#{sort_column(col)} #{sort_direction(i)}"
      i += 1
    end
    results.join(',')
  end

  #-----------------------------------------------------------------------------
  # override this to provide sorting on associated table columns
  #-----------------------------------------------------------------------------
  def sort_em(res)
    s = get_sorting()
    res.order(s)
  end

  #-----------------------------------------------------------------------------
  # this is the method that links it all together: sorting, pagination and
  # searching.
  #-----------------------------------------------------------------------------
  def fetch_resources(filtering)
    res = sort_em(@resources)
    if filtering
      res = res.page(page).per_page(per_page)
      if params[:sSearch].present?
        res = search_for(params[:sSearch],res)
      end
    end
    res
  end

  #-----------------------------------------------------------------------------
  # determine the rank of the mru record, to support correcting the result
  # page
  #-----------------------------------------------------------------------------
  def get_mru_rank(id)
    unless id.nil? or id == 0
      r = sort_em(@resources.reload).index(@resources.find(id))
      return r
    end
    return 0
  end

end
