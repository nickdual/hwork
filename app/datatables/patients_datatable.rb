class PatientsDatatable < ResourceDatatable

private

  def self.columns
    return [
     # disable the sorting of the first column with the link icons. 
     # also, don't include a title on the first column, or it'll clobber the (+) add button
     {
       "bSortable" => false,
       "sWidth" => "50px",
       "aTargets" => [0]
     },
     {
       "sTitle"   => "Last Name",
       "sWidth"   => "12%",
       "aTargets" => [1]
     },
     {
       "sTitle" => "First Name",
       "sWidth"   => "11%",
       "aTargets" => [2]
     },
     {
       "sTitle" => "Phone",
       "bSortable" => false,
       "sWidth"   => "12%",
       "aTargets" => [3]
     },
     {
       "sTitle" => "DOB",
       "sWidth"   => "5%",
       "aTargets" => [4]
     },
     {
       "sTitle" => "First Vis",
       "sWidth"   => "7%",
       "aTargets" => [5]
     },
     {
       "sTitle" => "Last Vis",
       "sWidth"   => "7%",
       "aTargets" => [6]
     },
     {
       "sTitle" => "Address",
       "aTargets" => [7]
     }
   ] + super
  end

private

  def data
    # we're applying some eager loading with the index_associations() method
    resources.index_associations().map { |pat|
      # if you modify the data columns, you may also need to modify 
      # the columns in sort_column() array.
      { 
        :DT_RowId => "patient-#{pat.id}",
        "0" => "",
        "1" => pat.last_name,
        "2" => pat.first_name,
        "3" => pat.contact.phone,
        "4" => pat.birthdate.nil? ? '' : I18n.l(pat.birthdate),
        "5" => "12/12/2012", #TODO get real info from visits
        "6" => "12/13/2012", #TODO get real info from visits
        "7" => pat.contact.physical_address_one_liner,
        "id" => pat.id,
        "edit" => @view.can?(:update, pat) ? @view.edit_patient_path(pat) : "",
        "delete" => @view.can?(:destroy, pat) ? @view.patient_path(pat) : ""
      }
    }
  end

  # first column is for controls, sort on #2
  def sort_column(i)
    columns = %w[controls contacts.last_name contacts.first_name phones.number birthdate addresses.street]
    columns[i]
  end

  def sort_em(resources)
    resources.joins(:contact => [:physical_address]).order(get_sorting())
  end

  def search_for(term, relations)
    relations.joins(:contact => [:physical_address]).where("contacts.last_name like :search or contacts.first_name like :search", :search => "%#{term}%")
  end

end

