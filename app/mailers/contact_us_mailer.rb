require 'resque_mailer'
class ContactUsMailer < AsyncMailer
  # TODO set this to either an environment variable or some other Rails setting.
  default from: "abc830844@gmail.com"

  def send_contact(contact_id, recipient)
    @message = ContactUs.find contact_id
    headers = {
        :subject => "Contact Us",
        :to => recipient ,
        :template_path => 'contact_us',
        :template_name => 'mailer'
    }
    mail(headers)
  end

end
