class UserMailer < AsyncMailer
  # TODO set this to either an environment variable or some other Rails setting.
  default from: "abc830844@gmail.com"
  def expire_email(user)
    mail(:to => user.email, :subject => "Subscription Cancelled")
  end

end
