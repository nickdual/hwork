#-------------------------------------------------------------------------------
#  CanCan Ability Definition
#   - !! Anytime you make a change to this file, you need to update and verify
#        the associated spec --> spec/models/ability_spec.rb !!
#-------------------------------------------------------------------------------
class Ability
  include CanCan::Ability

  # BLS - default values added to correct can? errors
  #       during RSpec testing
  def initialize(user,account_id=nil,clinic_id=nil)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :show, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :show, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities
    user ||= User.new # guest user (not logged in)
    account_id ||= user.try(:account).try(:id)
    clinic_id ||= user.try(:account).try(:clinics).try(:first).try(:id)

    basic_resources = [
      Reference, Clinic, Room, Provider, Schedule, User, FeeSchedule,
      ThirdParty, Calendar, Patient, ProcedureCode, DiagnosisCode, AppointmentType,
      CarrierHcfaFormOption, LegacyIdLabel
    ]
    admin_resources = [
      Import, ImportGroup,  Schedule, AccountSetup
    ]
    
    #---------------------------------------------------------------------------
    #---------------------------------------------------------------------------
    unless user.app_admin?
      if user.admin?
        cannot :manage, Resque
        can :manage, Permission
        basic_resources.each do |res|
          can :manage, res, :account_id => account_id
        end
        admin_resources.each do |res|
          can :manage, res, :account_id => account_id
        end
        # special cases
        can :manage, Account, :id => account_id
        can :manage, Subscription, :account_id => account_id
        can :manage, Event, :eventable_type => 'Schedule', :eventable => { :account_id => account_id }
        can [:edit, :update], NewPatientOption, :account_id => account_id
        can [:show, :update, :index], HcfaFormOption, :account_id => account_id
        can [:show, :update, :index], AccountHcfaFormOption, :account_id => account_id
        can :manage, Appointment, :calendar => { :account_id => account_id }
        can :manage, Fee, :fee_schedule => { :account_id => account_id }
        can :manage, Message, :messageable_type => 'Account', :messageable_id => account_id
      # can  create ImportGroups only 1 at a time / account
      # this rule must appear after the :manage rule for ImportGroups above.
      #  NOTE: the logic below is evaluated when the Ability instantiated, not when the 
      #        the runtime can? method is executed. This works fine given that a new 
      #        ability is being executed for each request and this is only being used
      #        to determine access to the "Create" control on the index page. The problem
      #        with using the more obvious block methodology is that CanCan doesn't currently
      #        execute the blocks when it is queried by a Class vs. an instance (e.g. for :create
      #        or :index actions).
      #  FIXME: since the import interface is not going to be primary, it doesn't make sense to take the performance hit
      #         for limiting the # of active import groups here. We should do it at the view level.
      #cannot :create, ImportGroup unless (
      #  ImportGroup.current_import(account_id).nil? or
      #  ImportGroup.current_import(account_id).finished? )

      # TODO - add logic to restrict user from modifying their own permissions
      #---------------------------------------------------------------------------
      #---------------------------------------------------------------------------
      elsif !user.new_record? and !user.account.nil?
        basic_resources.each do |res|
          can :show, res, :account_id => account_id if user.has_role?(:show, res)
          can [:show, :update], res, :account_id => account_id if user.has_role?(:update, res)
          can [:show, :update, :create], res, :account_id => account_id if user.has_role?(:create, res)
          can :manage, res, :account_id => account_id if user.has_role?(:manage, res)
        end
        # a user can edit / update thier own record
        can [:show, :edit, :update], User, :id => user.id
        #cannot [:create], User
        # special cases
        # Account
        can :show, Account, :id => account_id if user.has_role?(:show, Account)
        can [:show, :update], Account, :id => account_id if user.has_role?(:update, Account)
        can [:show, :update, :create], Account, :id => account_id if user.has_role?(:create, Account)
        can :manage, Account, :id => account_id if user.has_role?(:manage, Account)
        # Event
        can :show, Event, :eventable_type => 'Schedule', :eventable => { :account_id => account_id } if user.has_role?(:show, Event)
        can [:show, :update], Event, :eventable_type => 'Schedule', :eventable => { :account_id => account_id } if user.has_role?(:update, Event)
        can [:show, :update, :create], Event, :eventable_type => 'Schedule', :eventable => { :account_id => account_id } if user.has_role?(:create, Event)
        can :manage, Event, :eventable_type => 'Schedule', :eventable => { :account_id => account_id } if user.has_role?(:manage, Event)
        # Appointment
        can :show, Appointment, :calendar => { :account_id => account_id } if user.has_role?(:show, Appointment)
        can [:show, :update], Appointment, :calendar => { :account_id => account_id } if user.has_role?(:update, Appointment)
        can [:show, :update, :create], Appointment, :calendar => { :account_id => account_id } if user.has_role?(:create, Appointment)
        can :manage, Appointment, :calendar => { :account_id => account_id } if user.has_role?(:manage, Appointment)
        # Message
        can :show, Message, :messageable_type => 'Account', :messageable_id => account_id if user.has_role?(:show, Message)
        can [:show, :update], Message, :messageable_type => 'Account', :messageable_id => account_id if user.has_role?(:update, Message)
        can [:show, :update, :create], Message, :messageable_type => 'Account', :messageable_id => account_id if user.has_role?(:create, Message)
        can :manage, Message, :messageable_type => 'Account', :messageable_id => account_id if user.has_role?(:manage, Message)
        # Fee
        can :show, Fee, :fee_schedule => { :account_id => account_id } if user.has_role?(:show, Fee)
        can [:show, :update], Fee, :fee_schedule => { :account_id => account_id } if user.has_role?(:update, Fee)
        can [:show, :update, :create], Fee, :fee_schedule => { :account_id => account_id } if user.has_role?(:create, Fee)
        can :manage, Fee, :fee_schedule => { :account_id => account_id } if user.has_role?(:manage, Fee)
        # HCFA Forms
        can :show, HcfaFormOption, :account_id => account_id if user.has_role?(:show, HcfaFormOption)
        can [:show, :update], HcfaFormOption, :account_id => account_id if user.has_role?(:update, HcfaFormOption)
        can :show, AccountHcfaFormOption, :account_id => account_id if user.has_role?(:show, AccountHcfaFormOption)
        can [:show, :update], AccountHcfaFormOption, :account_id => account_id if user.has_role?(:update, AccountHcfaFormOption)

        # NOTE : 2012-08-02, we'll keep a full stack of permission records defined for each user
        #        the permissions will only "bubble" down or up on the settings screen. from the 
        #        CanCan layer, you'll only have access to those objects explicitly granted.
        #-------------------------------------------------------------------------
        # Now we'll handle the permission groups using the Permission configuration
        # For each group we're checking if the user has any roles for the group
        # and if so we'll assign the associated authorizations to the user, then
        # we'll also push those permissions down one level of children.
        # TODO : in ability, rework permission groups to be a recursive process
        # TODO : in ability, change the scope criteria to be specified in the configuration file
        # TODO : in ability, refactor all staff permissions to use the configuration file vs. explicit authorization rules.
        #-------------------------------------------------------------------------
        Permission.new.children.each do |perm|
          if perm.has_role?(user)
            can perm.allows(user), perm.resource, :account_id => account_id
            unless perm.children.empty?
              perm.children.each do |pc|
                can perm.allows(user), pc.resource, :account_id => account_id
              end
            end
          end
        end
      end
      if user.try(:account).try(:subscription)
        can :view, :basic if user.account.subscription.subscription_plan_name == 'basic'
        can :view, :plus if user.account.subscription.subscription_plan_name == 'plus'
        can :view, :premium if user.account.subscription.subscription_plan_name == 'premium'
        can :view, :max if user.account.subscription.subscription_plan_name == 'max'
      end
      can [:list, :read], Question, :state => 'published'
    else
      # Application Admin
      can :manage, :all
    end
  end
end
