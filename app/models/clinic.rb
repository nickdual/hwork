require_dependency 'csv_import'

class Clinic < ActiveRecord::Base
  resourcify # add instance method roles
  include CsvImport
  belongs_to :account
  belongs_to :location_clinic, :class_name => 'Clinic'
  has_one :contact, :as => :contactable, :class_name => 'ContactClinic'
  has_one :new_patient_option, :dependent => :destroy
  has_many :hcfa_form_options
  has_many :room_locations
  has_many :rooms, :through => :room_locations
  has_many :patients
  before_destroy :check_resource_reference

  accepts_nested_attributes_for :contact, :new_patient_option

  validates :account, :presence => true
  # sets the boundaries for future scheduling to maximum of 1 year.
  validates :schedule_future_max_days, :presence => true, :numericality => {
    :only_integer => true,
    :greater_than_or_equal_to => 0,
    :less_than_or_equal_to => 365
  }
  validates :schedule_history_max_days, :presence => true, :numericality => {
    :only_integer => true,
    :greater_than_or_equal_to => 14,
    :less_than_or_equal_to => 365
  }
  validates :website_url, :format => URI::regexp(%w(http https)), :allow_blank => true

  scope :by_account, lambda { |acc_id| where('account_id = ?',acc_id) }

  def self.switcher_order(account,clinic)
    Clinic.by_account(account).alphabetically
  end

  def self.alphabetically
    joins(:contact).order('contacts.company_name ASC')
  end

  #-----------------------------------------------------------------------------
  # "delegate" the physical_address_one_liner to the contact. which contact 
  # depends on if the address for this clinic is linked to another clinic's 
  # location.
  #-----------------------------------------------------------------------------
  def physical_address_one_liner
    if self.location_clinic
      self.location_clinic.contact.physical_address_one_liner
    else
      self.contact.physical_address_one_liner
    end
  end

  #-----------------------------------------------------------------------------
  # "delegate" the state to the contact. which contact depends on if the 
  # address for this clinic is linked to another clinic's locaiton.
  #-----------------------------------------------------------------------------
  def state
    if self.location_clinic
      self.location_clinic.contact.state
    else
      self.contact.state
    end
  end

  delegate :company_name, :company_name=, :name, :name=, :to => :contact, :allow_nil => true
  delegate :default_place_of_service, :to => :new_patient_option, :allow_nil => true

  after_create :build_options_if_nil
  after_create :load_hcfa_form_option_defaults
  after_save :set_master_billing_address_to_false

  alias :to_s :name
  alias :switcher_title :name

  #has_many :patients
  #has_many :appointments
  #has_many :ebill_setups
  #has_many :patient_cases, :through => :patients
  #has_many :patient_contacts, :through => :patients, :class_name => 'Contact', :source => :contact
  #has_many :versions, :dependent => :destroy
  

  #only 1 clinic can be the MBA
  def set_master_billing_address_to_false
    if self.master_billing_address
      self.account.clinics.where("id != ?", self.id).each do |other_clinic|
        other_clinic.master_billing_address = false
        other_clinic.save
      end
    end
  end
  
  def can_delete?
    patients.empty?
  end

  def self.build(configuration={})
    c = self.new(configuration)
    c.contact = ContactClinic.build(:contactable => c)
    c.build_new_patient_option
    # FIXME : having to set the foreign key on the Clinic.new_patient_option should not be necessary
    c.new_patient_option.clinic = c
    return c
  end

  def build_options_if_nil
    self.create_new_patient_option if self.new_patient_option.nil?
  end

  #-------------------------------------------------------------------------------
  # To assist with locating clincs in a natural way
  #-------------------------------------------------------------------------------
  def self.find_by_name(name)
    Clinic.joins(:contact).where(:contacts => { :company_name => name }).try(:first)
  end

  #-------------------------------------------------------------------------------
  # different_location?
  #  indicates if the specified clinic is at a different physical location
  #  first check to see if this clinic is co-located with another clinic
  #  before checking the address information.
  #-------------------------------------------------------------------------------
  def different_location?(other_clinic)
    return false if self == other_clinic
    return self.location_clinic.different_location?(other_clinic) if self.location_clinic
    return different_location?(other_clinic.location_clinic) if other_clinic.location_clinic
    self.contact.different_location?(other_clinic.contact)
  end

  #-------------------------------------------------------------------------------
  # loads any form_type / state level HCFA form defaults
  #  TODO hcfa form option defaults move to a helper module
  #-------------------------------------------------------------------------------
  def load_hcfa_form_option_defaults
    unless self.contact.nil? or self.contact.state.nil? then
      HcfaFormOption::FORM_TYPE_CODES.each do |ftc|
        #puts "trying #{ftc} for #{self}"
        if self.account.hcfa_form_options.where(:form_type_code => ftc[1], :state => self.state).empty? then
          default_hcfa_opt_q = Account.get_default.hcfa_form_options.where(:form_type_code => ftc[1], :state => self.state)
          unless default_hcfa_opt_q.empty? then
            default_hcfa_opt = default_hcfa_opt_q.first.clone
            default_hcfa_opt.account = self.account
            default_hcfa_opt.save!
          else
            #puts "missing #{ftc} defaults"
          end
        else
          #puts "  already have #{ftc} for #{self}"
        end
      end
    else
      # log an invalid clinic message
    end
  end

  JSON_OPTS = { :only => [:id], :methods => [:name] }
  def as_json(options={})
    super(JSON_OPTS.merge(options))
  end

  #-------------------------------------------------------------------------------
  # CSV loading interface
  #-------------------------------------------------------------------------------
  def self.get_csv_instance(import_type,account,row)
    c = Clinic.build()
    c.contact.physical_address.state = 'NY' # default state for clinic's imported from Handyworks
    return c
  end

  protected

  #-------------------------------------------------------------------------------
  # Active Record before_destroy callback. 
  #-------------------------------------------------------------------------------
  def check_resource_reference
    unless self.can_delete?
      raise Errors::ResourceReferencedError.new(I18n.translate("flash.error.resource.reference"))
    end
  end

end
