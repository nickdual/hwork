require_dependency 'csv_import'
class LegacyIdLabel < ActiveRecord::Base
  resourcify # add instance method roles
  include CsvImport
  belongs_to :account
  has_many :provider_legacy_id_labels, :class_name => 'ProviderLegacyIdLabel', :dependent => :destroy
  has_many :third_parties
  validates :label, :presence => true, :uniqueness => { :case_sensitive => false, :scope => :account_id }
  validates :account, :presence => true
  default_scope :order => 'label ASC'
  before_destroy :check_third_parties
  def self.by_account(account_id)
    where('account_id = ?',account_id)
  end
  def to_s
    label
  end
  private
  def check_third_parties
    if self.third_parties.count > 0 
      errors[:base] << "This label is currently referenced by third party #{self.third_parties.first.name}"    
      return false
    end
  end
end
