require_dependency 'sti_helpers'
require_dependency 'csv_import'
require_dependency 'enum_support'

class ThirdParty < ActiveRecord::Base
  resourcify # add instance method roles
  extend EnumSupport
  include StiHelpers
  include CsvImport
  #-------------------------------------------------------------------------------
  # Constants
  #-------------------------------------------------------------------------------
  #-------------------------------------------------------------------------------
  # Behaviours
  #-------------------------------------------------------------------------------
  # acts_as_paranoid
  #-------------------------------------------------------------------------------
  # associations
  #-------------------------------------------------------------------------------
  belongs_to :account
  belongs_to :legacy_id_label
  has_one :contact, :as => :contactable, :class_name => 'ContactThirdParty'
  has_one :carrier_hcfa_form_option, :dependent => :destroy # carrier only
  has_many :cases
   
  #has_many :patient_bills
  accepts_nested_attributes_for :contact
  accepts_nested_attributes_for :legacy_id_label
  #-------------------------------------------------------------------------------
  # validations
  #-------------------------------------------------------------------------------
  validates :account, :presence => true
  validates :alias_name, :presence => true, :uniqueness => { :case_sensitive => false, :scope => :account_id }
  
  #validates_associated :contact, :message => 'Hi Bob'
  validates :contact, :presence => true
  default_value_for :insurance_carrier_type_code, APP_CONFIG['options']['carrier_types']['Commercial']

  #-------------------------------------------------------------------------------
  # scopes
  #-------------------------------------------------------------------------------
  scope :alphabetically, :joins => :contact, :order => 'contacts.last_name ASC, contacts.first_name ASC, contacts.company_name ASC'

  def self.by_account(account_id)
    where('account_id = ?', account_id)
  end

  def self.switcher_order(account,clinic)
    sti_base_class.by_account(account).alphabetically
  end

  def self.index_associations
      # apply eager loading to reduce the number of queries which are going to be performed to 
      # generate the index page.
      includes(:contact => :physical_address).includes(:carrier_hcfa_form_option)
  end
  #-------------------------------------------------------------------------------
  # class methods
  #-------------------------------------------------------------------------------
  def self.build(configuration={})
    c = self.new(configuration)
    c.contact = ContactThirdParty.build(:contactable => c)
    return c
  end

  def self.humanize
    self.model_name.humanize.titleize
  end

  #-------------------------------------------------------------------------------
  # instance methods
  #-------------------------------------------------------------------------------
  def can_delete?
    patient_cases.empty? && patient_bills.empty?
  end

  def search_title
    name
  end

  def is_attorney?
    insurance_carrier_type_code == APP_CONFIG['options']['carrier_types']['Attorney']
  end

  def is_carrier?
    ! is_attorney?
  end

  def allows_hcfa?
    is_carrier?
  end

  def attorney=(name)
    names = name.split(" ")
    self.contact.first_name = names.first
    self.contact.last_name = names[1..names.size].join(" ")
  end

  def hcfa_address
    address.two_streets + "\n" + address.city + " " + address.state + " " + address.zip
  end

  def insurance_carrier_type
    APP_CONFIG['options']['carrier_types'].invert[insurance_carrier_type_code]
  end
  #-------------------------------------------------------------------------------
  # delegated methods
  #-------------------------------------------------------------------------------
  delegate :company_name, :company_name=, :name, :name=, :to => :contact, :allow_nil => true
  delegate :physical_address_one_liner, :to => :contact, :allow_nil => true
  delegate :state, :to => :contact
  #-------------------------------------------------------------------------------
  # aliased methods
  #-------------------------------------------------------------------------------
  alias_attribute :switcher_title, :name
  alias :law_firm :company_name
  
  #-------------------------------------------------------------------------------
  # private methods
  #-------------------------------------------------------------------------------  

  #-------------------------------------------------------------------------------
  # CSV Import
  #-------------------------------------------------------------------------------
  def self.get_csv_instance(import_type,account,row)
    t = ThirdParty.build()
    t.contact.build_fax_phone()
    return t
  end

  def csv_legacy_label_alt_id
    self.legacy_id_label.alt_id
  end

  def csv_legacy_label_alt_id=(new_id)
    legacy_id = LegacyIdLabel.where(:account_id => self.account_id, :alt_id => new_id).first
    self.legacy_id_label = legacy_id
  end

  #-----------------------------------------------------------------------------
  # the next two methods are here for legacy import purposes only, if a carrier
  # in Handyworks (Access) had an ebill_type_code of 99, they should suppress
  # ebills, going forward, suppress_ebill will only be a boolean option.
  #-----------------------------------------------------------------------------
  def csv_ebill_type_code
    return self.suppress_ebill ? 99 : 1
  end

  def csv_ebill_type_code=(new_type_code)
    logger.info("calling csv_ebill_type_code w/ #{new_type_code}")
    if new_type_code == "99"
      logger.info("--> supress_ebill true")
      self.suppress_ebill = true
    end
  end

  protected

end
