class Jobs::ExpireTrials
  @queue = :accounts

  def self.perform()
    Account.where(:state => 'trial').each do |account|
      if account.subscription and account.created_at < Time.now - account.subscription.trial_period.days 
        puts "expiring trial for this account"
        account.expire_trial!
      end
    end
  end
end
