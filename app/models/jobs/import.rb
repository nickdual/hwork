class Jobs::Import
  @queue = :imports

  def self.perform(import_group_id)
    ig = ImportGroup.find(import_group_id)
    ig.load_data_job()
  end
end
