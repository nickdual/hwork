class Jobs::Seed
  @queue = :seeds

  def self.perform(account_id)
    account = Account.find(account_id)
    account.initialize_seed_data()
    account.seed! # state change
  end
end
