class Address < ActiveRecord::Base
  validates :city, :presence => { :if => :should_validate_city? }
  validates :state, :presence => { :if => :should_validate_state? }, :length => { :maximum => 2 }
  validates :zip, :presence => { :if => :should_validate_zip? },
    :format => { :with => /^\d{5}(-\d{4})?$/, :message => "should be in the following format: XXXXX or XXXXX-XXXX",
      :allow_blank => true }
  validates :street, :presence => { :if => :should_validate_street? }

  belongs_to :addressable, :polymorphic => true

  default_scope :order => 'addresses.updated_at DESC'
  
  def line1
    [street, street2].compact.join(" ")
  end
  
  def line2
    [city, state, zip].compact.join(" ")
  end
  
  def one_liner
    "#{street} #{city}, #{state} #{zip} "
  end
  
  def should_validate_city?
    false
  end
  
  def should_validate_state?
    false
  end
  
  def should_validate_zip?
    false
  end
  
  def should_validate_street?
    false
  end

  def two_streets
    two_streets = []

    two_streets << street if street?
    two_streets << street2 if street2?

    two_streets.join(', ')
  end

  def streets
    streets = []

    streets << street unless street.blank?
    streets << street2 unless street.blank?

    streets.join(' ')
  end

  def address
    address = []
    address << state unless state.blank?
    address << zip unless zip.blank?
    address << street unless street.blank?
    address << street2 unless street2.blank?

    address.join(' ')
  end

  #-------------------------------------------------------------------------------
  # indicates if the other_location is different than this one.
  #-------------------------------------------------------------------------------
  def different_location?(other_location)
    a = self.one_liner
    b = other_location.nil? ? "" : other_location.one_liner
    return a.upcase! != b.upcase!
  end

  alias :to_s :address
end
