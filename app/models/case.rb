class Case < ActiveRecord::Base
  belongs_to :account
  belongs_to :patient
  belongs_to :provider
  belongs_to :third_party
  has_many :case_diagnoses
  has_many :case_carriers
  has_many :visits, :dependent => :destroy
  #has_many :patient_bills, :dependent => :destroy
  has_one :reference, :as => :referrable, :class_name => "Referral"
  has_one :fee_schedule
end
