class CaseCarrier < ActiveRecord::Base
  belongs_to :case
  belongs_to :guarantor
  belongs_to :carrier, :class_name => ThirdParty, :conditions => "insurance_carrier_type_code = #{APP_CONFIG['options']['carrier_types']['Attorney']}"
  monetize :deductible_cents
  monetize :paid_cents
end
