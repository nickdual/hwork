class RoomAppointmentType < ActiveRecord::Base
  belongs_to :room
  belongs_to :appointment_type
  validates :room, :presence => true
  validates :appointment_type, :presence => true
end
