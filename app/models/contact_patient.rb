class ContactPatient < Contact
  # TODO fill out patient contact rules / class
  has_one :physical_address, :as => :addressable, :class_name => "Address", :conditions => { :label => 'Physical' }
  has_one :personal_email, :as => :emailable, :class_name => "Email", :conditions => { :label => 'Personal' }
  accepts_nested_attributes_for :physical_address, :personal_email
  accepts_nested_attributes_for :phones, :allow_destroy => true
  validates :last_name, :presence => { :message => "either first or last name is required" }, :if => "first_name.blank?"
  validates_associated_if_present :physical_address
  validates_associated_if_present :personal_email, :email
  validates_associated_if_present :phones
  validate :only_one_primary_phone
  #-----------------------------------------------------------------------------
  # defines the contact structure for a Patient
  #-----------------------------------------------------------------------------
  def self.build(attributes={})
    contact = ContactPatient.new(attributes)
    contact.build(attributes)
    return contact
  end

  def build(attributes={})
    build_physical_address(attributes[:physical_address])
    build_personal_email(attributes[:personal_email])
    self.phones << HomePhone.new()
  end

  delegate :one_liner, :to => :physical_address, :prefix => true, :allow_nil => true
  delegate :email, :to => :personal_email

  #-----------------------------------------------------------------------------
  # returns an array of strings to be displayed as additional information 
  # when creating / editing an appointment for a patient.
  #-----------------------------------------------------------------------------
  def appointment_notes
    general_contact = super
    general_contact.slice(1..-1).concat [  # slices off the first element from general info, client type
      "Last Visit:", # TODO : add the last visit information to patient appointment notes
      "D.O.B: #{self.contactable.try(:birthdate)}",
      "Spouse: #{self.contactable.try(:spouse_name)}"
    ]
  end

  #-----------------------------------------------------------------------------
  # take the name a single field, and split it using basic ' ' logic into
  # components.
  #-----------------------------------------------------------------------------
  def full_name=(full_name)
    parts = full_name.split(' ',3)
    self.first_name = parts[0] if parts.length > 0
    self.last_name = parts[parts.length - 1] if parts.length > 1
    self.middle_initial = parts[1] if parts.length >= 3
  end

end
