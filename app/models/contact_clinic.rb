class ContactClinic < Contact
  # BLS could maybe switch from the Address label to a STI model like phones.
  has_one :physical_address, :as => :addressable, :class_name => "Address", :conditions => { :label => 'Physical' }
  has_one :billing_address, :as => :addressable, :class_name => "Address", :conditions => { :label => 'Billing' }
  has_one :business_email, :as => :emailable, :class_name => "Email", :conditions => { :label => 'Business' }
  has_one :business_phone, :as => :phoneable, :class_name => "BusinessPhone"
  has_one :fax_phone, :as => :phoneable, :class_name => "FaxPhone"

  accepts_nested_attributes_for :physical_address, :billing_address, :business_email, :business_phone, :fax_phone

  validates :company_name, :presence => true
  validates_associated_if_present :business_email, :email
  validates_associated_if_present :business_phone, :number
  validates :state, :presence => true, :unless => "physical_address.nil?"

  after_validation :check_state

  delegate :one_liner, :to => :physical_address, :prefix => true
  delegate :street, :street2, :city, :state, :zip, :to => :physical_address, :allow_nil => true
  delegate :street=, :street2=, :city=, :state=, :zip=, :to => :physical_address, :allow_nil => true
  # BLS - seems like either there might be a shortcut to delegating both the reader and writer attribute accessors
  #       or perhaps the reason this is not there is its not a great idea.
  delegate :phone, :to => :business_phone
  delegate :email, :to => :business_email

  #-----------------------------------------------------------------------------
  # defines the contact structure for a Clinic
  #  it must have a Business Phone, Fax Phone
  #  it must have a Business Address
  #-----------------------------------------------------------------------------
  def self.build(attributes={})
    contact = self.new(attributes)
    contact.build(attributes)
    return contact
  end

  # never references the first name / last name
  alias_attribute :name, :company_name

  def build(attributes={})
    build_physical_address(attributes[:physical_address])
    build_business_phone(attributes[:business_phone])
    build_fax_phone(attributes[:fax_phone]) if attributes[:fax_phone]
    build_business_email(attributes[:business_email]) if attributes[:business_email]
  end

  #-------------------------------------------------------------------------------
  # This method is to properly highlight the :state field on the Clinic form
  # Since the validation is actually implemented in the "parent" ContactClinic
  # formtastic was not picking up on the error on the field itself.
  #-------------------------------------------------------------------------------
  def check_state
    if ! self.errors[:state].empty? then
      self.errors[:state].each do |e|
        self.physical_address.errors.add(:state, e)
      end
    end
  end

end
