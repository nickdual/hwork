require_dependency 'enum_support'
require_dependency 'csv_import'

class Patient < ActiveRecord::Base
  has_paper_trail
  resourcify # add instance method roles
  include CsvImport
  extend EnumSupport
  belongs_to :account
  belongs_to :clinic
  #the next 2 lines?
  has_one :reference, :as => :referrable, :class_name => "Referral"
  has_many :referrals, :as => :reference
  has_many :cases, :dependent => :destroy
  has_many :providers, :through => :appointments
  belongs_to :employer
  has_one :contact, :as => :contactable, :class_name => 'ContactPatient'
  validates :contact, :presence => true
  validates :account, :presence => true
  validates :clinic, :presence => true
  accepts_nested_attributes_for :contact, :employer
  enum_map :marital_status_code, APP_CONFIG['options']['married']
  enum_map :employment_status_code, APP_CONFIG['options']['work_status']
  enum_map :student_status_code, APP_CONFIG['options']['student']
  enum_map :sex, APP_CONFIG['options']['sex']

  # TODO : add validation of patient.birthdate format, might require an additional hidden text field
  #        because once you get to the model, the date conversion (for better or worse) has already
  #        been performed.

  def self.build(configuration={})
    patient = Patient.new(configuration)
    patient.contact = ContactPatient.build(:contactable => patient)
    patient.employer = Employer.build()
    return patient
  end

  def age
    now = Time.now.utc.to_date
    now.year - birthdate.year - ((now.month > birthdate.month || (now.month == birthdate.month && now.day >= birthdate.day)) ? 0 : 1)
  end 


  #-------------------------------------------------------------------------------
  # filter the incoming list according to the indicated term
  #-------------------------------------------------------------------------------
  def self.autocomplete(term, list, mode)
    list.joins(:contact).merge(Contact.named_like(term, mode))
  end

  def self.by_last_name(name)
    joins(:contact).where('contacts.last_name like :name',:name => "#{name}%")
  end

  #-------------------------------------------------------------------------------
  # The results of the letters query are going to be stored in redis cache.
  # under patient:lname_letters.
  #-------------------------------------------------------------------------------
  def self.letters(account)
    c_key = account.redis_key('patient:lname_letters')
    redis = Account.redis
    if ( redis.exists(c_key) ) then
      letters = redis.smembers(c_key)
    else
      query = "select distinct substr(contacts.last_name,1,1) from patients,contacts where  patients.account_id = #{account.id} and contacts.contactable_type = 'Patient' and contacts.contactable_id = patients.id"
      letters = connection.select_values(query)
      letters.each do |l|
        redis.sadd(c_key, l)
      end
    end
    letters
  end

  def self.index_associations
      # apply eager loading to reduce the number of queries which are going to be performed to 
      # generate the index page.
      includes(:contact => [:phones, :physical_address])
  end

  #-------------------------------------------------------------------------------
  # scopes
  #-------------------------------------------------------------------------------
  def self.by_account(account_id)
    where(:account_id => account_id)
  end

  def self.alphabetically 
    joins(:contact).order('contacts.last_name ASC, contacts.first_name ASC')
  end

  delegate :first_name, :last_name, :name, :to => :contact

  #-------------------------------------------------------------------------------
  # some convenience methods for writing to / about a patient.
  #-------------------------------------------------------------------------------
  def him_or_her
    if sex == 1
      "him"
    else
      "her"
    end
  end
  
  def his_or_her
    if sex == 1
      "his"
    else
      "her"
    end
  end

  def he_or_she
    if sex == 1
      "he"
    else
      "she"
    end
  end

  #-------------------------------------------------------------------------------
  # default_provider
  #   returns the id this patient should be scheduled with for future appointments
  #   which depends on current cases, the clinic they're associated with or the
  #   the first provider on the account
  #-------------------------------------------------------------------------------
  def default_provider
    # TODO : check current case's provider to define patient's default provider
    # check the patients clinic's provider
    if self.try(:clinic).try(:new_patient_option).try(:default_provider)
      return self.clinic.new_patient_option.default_provider.id
    end
    # check the account's first provider
    if self.account.try(:providers).try(:first)
      return self.account.providers.first.id
    end
  end
  #-------------------------------------------------------------------------------
  # Create a new patient for an appointment. This'll only include the name and
  # a phone number. The name will be split into components and the phone number
  # will be stored as the home phone.
  #-------------------------------------------------------------------------------
  def self.newForAppointment(account,clinic,first_name,last_name,phone)
    p = Patient.build
    p.clinic = clinic
    p.account = account
    p.contact.first_name = first_name
    p.contact.last_name = last_name
    p.contact.phones.first.number = phone if phone
    p.contact.phones.pop() unless phone
    p.save!
    return p
  end

  #-------------------------------------------------------------------------------
  # JSON 
  # OPTIMIZE : since we're adding last_name and first_name into the JSON model, we should remove :name, but this'll need to be updated throughtout the appointments coffeescript code.
  #-------------------------------------------------------------------------------
  JSON_OPTS = { :only => [:id, :notes],
    :methods => [ :default_provider ],
    :include => {
      :contact => {
        :only => [:id, :last_name, :first_name ],
        :methods => [ :name, :phone ]
  } } }
  def as_json(options={})
    super(JSON_OPTS.merge(options))
  end

  #-------------------------------------------------------------------------------
  # CSV loading interface 
  #-------------------------------------------------------------------------------
  def self.get_csv_instance(import_type,account,row)
    p = Patient.new()
    p.contact = ContactPatient.new(:contactable => p)
    p.contact.build_physical_address()
    p.contact.build_personal_email()
    p.employer = Employer.build()
    return p
  end

    #;;Single;S;Married;M;Divorced;D;Widowed;W;Unknown;U
  CSV_MARRIAGE_STATUS = {
    'S' => 'Single',
    'M' => 'Married',
    'D' => 'Divorced',
    'W' => 'Widowed',
    'U' => 'Unknown'
  }
  # handles the conversion and nil values default nil ==> Chiropractic
  def csv_marriage_status_code=(i_marriage_status)
    unless i_marriage_status.nil?
      self.marital_status_code = CSV_MARRIAGE_STATUS[i_marriage_status]
    end
  end
  def csv_marriage_status_code
    # need to use the key accessor to reverse the rails enum mapping
    CSV_MARRIAGE_STATUS.invert[self.marital_status_code]
  end
  # ;;Unemployed;U;Full;F;Part;P;Retired;R;Disabled-Not Working;D;Self-Employed;S
  CSV_EMPLOYMENT_STATUS = {
    'U' => 'Unemployed',
    'F' => 'Full',
    'P' => 'Part',
    'R' => 'Retired',
    'D' => 'Disabled - Not working',
    'S' => 'Self-Employed',
    'U' => 'Unknown'
  }
  # handles the conversion and nil values default nil ==> Chiropractic
  def csv_employment_status_code=(i_employment_status)
    unless i_employment_status.nil?
      self.employment_status_code = CSV_EMPLOYMENT_STATUS[i_employment_status]
    end
  end
  def csv_employment_status_code
    # need to use the key accessor to reverse the rails enum mapping
    CSV_EMPLOYMENT_STATUS.invert[self.employment_status_code]
  end
  # ;;Full Time;F;Part Time;P
  CSV_STUDENT_STATUS = {
    'F' => 'Full time',
    'P' => 'Part time'
  }
  # handles the conversion and nil values default nil ==> Chiropractic
  def csv_student_status_code=(i_student_status)
    unless i_student_status.nil?
      self.student_status_code = CSV_STUDENT_STATUS[i_student_status]
    end
  end
  def csv_student_status_code
    # need to use the key accessor to reverse the rails enum mapping
    CSV_STUDENT_STATUS.invert[self.student_status_code]
  end
  def csv_is_active
    return self.is_active ? "False" : "True"
  end
  def csv_is_active=(is_inactive)
    self.is_active = is_inactive == "False" ? true : false
  end
  def referral_alt_id
    self.reference.try(:alt_reference_id)
  end
  def referral_alt_id=(i_alt_id)
    unless i_alt_id.nil?
      self.reference = Referral.new
      self.reference.alt_reference_id = i_alt_id
      # this will link the patient referral to a generic reference resource
      if i_alt_id.ends_with?("R")
        alt_id = i_alt_id.slice(0,i_alt_id.length - 1)
        r = Reference.where(:account_id => self.account_id, :alt_id => alt_id).order('created_at DESC').first
        self.reference.reference = r
      end
    end
  end
  def csv_home_phone
    self.contact.get_phone('Home')
  end
  def csv_home_phone=(a_phone)
    logger.info("home --> #{a_phone}")
    unless a_phone.blank?
      logger.info("home ++> #{a_phone}")
      self.contact.add_phone('Home',a_phone)
    end
  end
  def csv_work_phone
    self.contact.get_phone('Work')
  end
  def csv_work_phone=(a_phone)
    logger.info("work --> #{a_phone}")
    unless a_phone.blank?
      self.contact.add_phone('Work',a_phone)
      logger.info("work ++> #{a_phone}")
    end
  end
  def csv_mobile_phone
    self.contact.get_phone('Mobile')
  end
  def csv_mobile_phone=(a_phone)
    logger.info("mobile --> #{a_phone}")
    unless a_phone.blank?
      logger.info("++ mobile --> #{a_phone}")
      self.contact.add_phone('Mobile',a_phone)
    end
  end
  # callback after CSV import is complete
  def self.csv_link_patients(user, clinic, account, error_code=0, duplicate_code=0)
    logger.debug("in csv callback csv_link_patients")
    # OPTIMIZE : there might be an arel query which will restrict this list to only patients which need to be linked.
    Patient.where(:account_id => account.id).each do |patient|
      logger.debug("checking patient #{patient.id}  for #{patient.clinic_alt_id}")
      # go back through and link any dangling patient referrals
      if not patient.reference.try(:alt_reference_id).nil? and patient.reference.try(:reference).nil?
        # find the matching patient record
        p = Patient.where(:account_id => account.id, :alt_id => patient.reference.alt_reference_id).order('created_at DESC').first
        p.referrals << patient.reference unless p.nil?
        # previously tried to use patient.reference.reference = p, not sure why this didn't work
      end
      # go back through and link any dangling patient clinic references
      if not patient.clinic_alt_id.nil? and not patient.clinic_alt_id.starts_with?("DDD")
        c = Clinic.where(:account_id => account.id, :alt_id => patient.clinic_alt_id).order('created_at DESC').first
        logger.debug("Linking patient #{patient.inspect} to #{c.inspect}")
        c.patients << patient unless c.nil?
        patient.clinic_alt_id = "DDD" + patient.clinic_alt_id
        patient.save # necessary to preserve this change
      end

    end
  end
end
