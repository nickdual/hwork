class Schedule < ActiveRecord::Base
  resourcify # add instance method roles
  belongs_to :schedulable, :polymorphic => true
  delegate :account_id, :to => :schedulable
  has_many :events, :as => :eventable, :dependent => :destroy
  validates :max_concurrent_appointments, :presence => true, :numericality => { :only_integer => true, :greater_than_or_equal_to => 0 }
  validates :max_appointments_per_hour, :presence => true, :numericality => { :only_integer => true, :greater_than_or_equal_to => 0 }

  after_create :initialize_schedule

  SCHEDULE_EVENT = 'Schedule Event'

  # generate fake ids for the JSON encoded schedule events
  @@schedule_id = 0

  #-----------------------------------------------------------------------------
  # Returns the "live" schedule for the week of the given date
  #  This consists of events which are projected from the 'Schedule Events'
  #  for this schedule combined with any Exception events which happen
  #  to fall in this date.
  #
  # For now the "week" will be defined as going from Sunday - Saturday
  # TODO : generalize this algorithm to be valid based a configurable first day
  #        of the week to generate a schedule.
  #-----------------------------------------------------------------------------
  def get_schedule_for_week(date)
    if date.kind_of? Date
      start = date - date.wday # start on a Sunday
      # create a copy of the schedule for this week
      week_events = self.events.where(:title => Schedule::SCHEDULE_EVENT).includes(:event_locations, :recurrence ).map { |event|
        now_event = event.clone
        now_event.id = @@schedule_id += 1
        # advance events based on their day of the week
        now_event.start_date = start + now_event.start_date.wday
        # TODO : change the logic on creating schedule events to set the field free to true.
        now_event.free = true
        now_event
      }
      # add in any exceptions which occur during this week
      #  - to ensure there are no id collisions with the weekly schedule we need
      #    to use the @@schedule_id to generate a consistent fake id for the JSON
      #    events.
      self.events.for_week(start).where('title != "?"',Schedule::SCHEDULE_EVENT).includes(:event_locations, :recurrence).each do |event|
        now_event = event.clone
        now_event.id = @@schedule_id += 1
        week_events << now_event
      end
    else
      logger.error("invalid date argument #{date}")
    end
    return week_events
  end

  #-----------------------------------------------------------------------------
  # Create a short summary of this schedule
  # What we're going to do is sort the schedule by day and time, then create
  # a summary of the hours(truncated) worked each day (stored in summary).
  #
  # Then we'll loop through the summary and combine the results for any days
  # which have the same basic hours (for the whole day). So you could have
  # something like:
  #    M,W,F:8-12,13-18,Tu,Th:8-13
  #-----------------------------------------------------------------------------
  def summary
    days = ["Su","M","Tu","W","Th","F","Sa"]
    summary = ["","","","","","",""]
    self.events.where(:title => Schedule::SCHEDULE_EVENT).order(:start_date,:start_time).each do |event|
      day = event.start_date.wday
      summary[day] += "#{event.start_time.strftime('%I').to_i}-#{event.start_time.in(event.duration * 60).strftime('%I').to_i},"
    end
    r = ""
    done = []
    (0..6).each do |idx|
      if summary[idx].length > 0 and not done.include?(idx)
        d_label = days[idx]
        d_sched = summary[idx]
        done << idx
        # check for strings of days with the same basic schedule.
        (idx..6).each do |jdx|
          if summary[jdx].length > 0 and summary[idx] == summary[jdx] and not done.include?(jdx)
            d_label += ",#{days[jdx]}"
            done << jdx
          end
        end
        r += "#{d_label}:#{d_sched}"
      end
    end
    return r[0..-2] # trims off the last comma
  end

  JSON_OPTS = { :include => { :events => Event::JSON_OPTS } }
  # FIXME : Due to a "bug" in rails https://github.com/rails/rails/pull/2200, association
  #         models with custom as_json() methods will not be called when :included from a parent
  #         current workaround is to include all required json options as a constant in both parent 
  #         and association model.
  def as_json(options={})
    super(JSON_OPTS.merge(options))
  end

  private

  #-----------------------------------------------------------------------------
  # sets up an initial schedule events for this schedule. This is going to be
  # M - F from 0800 - 1200 and 1300 - 1700 at the moment.
  #
  # If we're in test configuration the schedule will also include Sundays
  # and Saturdays because test appointments are always going to be run for
  # "today" so it'll be more straightforward to have the schedule initialized
  # the same for each day.
  #-----------------------------------------------------------------------------
  def initialize_schedule
    initial_events = [
      { :day_of_week => 1, :start_date => "2000-01-03", :start_time => "08:00", :duration => 240, :title => Schedule::SCHEDULE_EVENT },
      { :day_of_week => 1, :start_date => "2000-01-03", :start_time => "13:00", :duration => 240, :title => Schedule::SCHEDULE_EVENT },
      { :day_of_week => 2, :start_date => "2000-01-04", :start_time => "08:00", :duration => 240, :title => Schedule::SCHEDULE_EVENT },
      { :day_of_week => 2, :start_date => "2000-01-04", :start_time => "13:00", :duration => 240, :title => Schedule::SCHEDULE_EVENT },
      { :day_of_week => 3, :start_date => "2000-01-05", :start_time => "08:00", :duration => 240, :title => Schedule::SCHEDULE_EVENT },
      { :day_of_week => 3, :start_date => "2000-01-05", :start_time => "13:00", :duration => 240, :title => Schedule::SCHEDULE_EVENT },
      { :day_of_week => 4, :start_date => "2000-01-06", :start_time => "08:00", :duration => 240, :title => Schedule::SCHEDULE_EVENT },
      { :day_of_week => 4, :start_date => "2000-01-06", :start_time => "13:00", :duration => 240, :title => Schedule::SCHEDULE_EVENT },
      { :day_of_week => 5, :start_date => "2000-01-07", :start_time => "08:00", :duration => 240, :title => Schedule::SCHEDULE_EVENT },
      { :day_of_week => 5, :start_date => "2000-01-07", :start_time => "13:00", :duration => 240, :title => Schedule::SCHEDULE_EVENT }
    ]
    if Rails.env == 'test'
      initial_events.concat [
        { :day_of_week => 0, :start_date => "2000-01-02", :start_time => "08:00", :duration => 240, :title => Schedule::SCHEDULE_EVENT },
        { :day_of_week => 0, :start_date => "2000-01-02", :start_time => "13:00", :duration => 240, :title => Schedule::SCHEDULE_EVENT },
        { :day_of_week => 6, :start_date => "2000-01-08", :start_time => "08:00", :duration => 240, :title => Schedule::SCHEDULE_EVENT },
        { :day_of_week => 6, :start_date => "2000-01-08", :start_time => "13:00", :duration => 240, :title => Schedule::SCHEDULE_EVENT }
      ]
    end
    initial_events.each do |event_attr|
      self.events << Event.new(event_attr)
    end
    save!
  end

end
