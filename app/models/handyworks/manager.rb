#-------------------------------------------------------------------------------
#  Model to perform Handyworks application management tasks
#    many of these operations will be exposed via handyworks:manage rake
#    tasks.
#-------------------------------------------------------------------------------
class Handyworks::Manager

  #-----------------------------------------------------------------------------
  # Authenticate Command Line Management Operations
  #-----------------------------------------------------------------------------
  def authenticate_manager(manager_password)
    # TODO implement manager command line authentication
    puts "Implment Manager command line authentication"
    # raise exception if authentication fails.
  end

  #-----------------------------------------------------------------------------
  # Creates a new user with the Application Administrator role
  #-----------------------------------------------------------------------------
  def create_app_admin(password, email)
    new_admin = User.build(:email => email,
                           :password => password,
                           :password_confirmation => password)
    new_admin.add_role(Role::APPLICATION_ADMIN)
    new_admin.account_id = Account::DEFAULT_ACCOUNT_ID
    new_admin.skip_confirmation!
    new_admin.save!
    return new_admin
  end
  #-----------------------------------------------------------------------------
  # load any missing HCFA option defaults
  #  will iterate through all clinics to ensure the accounts they belong to
  #  have all the necessary hcfa options defined.
  #-----------------------------------------------------------------------------
  def load_hcfa_options
    Clinic.all.each do |clinic|
      clinic.load_hcfa_form_option_defaults
    end
  end
end
