class NewPatientOption < ActiveRecord::Base
  resourcify # add instance method roles
  @@OVERDUE_FEE_PERCENTAGE_DEFAULT = 18
  @@DEFAULT_PLACE_OF_SERVICE_DEFAULT = 11

  default_value_for :overdue_fee_percentage, @@OVERDUE_FEE_PERCENTAGE_DEFAULT
  default_value_for :default_place_of_service, @@DEFAULT_PLACE_OF_SERVICE_DEFAULT

  serialize :insurance_carrier_assignment_policy, Array 

  belongs_to :clinic 
  belongs_to :default_provider, :class_name => 'Provider'
  validates :clinic , :presence => true
  # BLS - validations prevent this object from being instantiated in the clinic.after_create callback.
  # TODO - enable validations using a conditional attribute do_validations? which could be set in hidden form field.
  #validates :patient_number_scheme, :inclusion => { :in => APP_CONFIG['options']['patient_number_scheme'] }
  #validates :insurance_carrier_assignment_policy, :inclusion => { :in => APP_CONFIG['options']['insurance_carrier_assignment_policy'].store("", "") }
  #validates :default_place_of_service, :inclusion => { :in => APP_CONFIG['options']['places_of_service'].invert }

  #before_create :set_hcfa_margins 

  delegate :account_id, :to => :clinic

  # XXX - what is the purpose of this call?
  def insurance_carrier_assignment_policy 
    super || [] 
  end 

  protected 

end
