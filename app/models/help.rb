class Help < ActiveRecord::Base
  attr_accessible :body, :label, :title
  no_whitespace = /^[\S]+$/
  validates :label, :presence => true, :uniqueness => true, :format => { :with => no_whitespace }
  validates :title, :presence => true
  validates :body, :presence => true
end
