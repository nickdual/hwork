require_dependency 'csv_import'
class FeeSchedule < ActiveRecord::Base
  resourcify # add instance method roles
  include CsvImport
  #-------------------------------------------------------------------------------
  # associations
  #-------------------------------------------------------------------------------
  belongs_to :account
  has_many :fees, :dependent => :destroy
  has_many :procedure_codes, :through => :fees
  has_many :cases
  #-------------------------------------------------------------------------------
  # validations
  #-------------------------------------------------------------------------------
  validates :account, :presence => true
  validates :label, :presence => true,
    :uniqueness => { :scope => :account_id }
  before_destroy :check_cases
  #-------------------------------------------------------------------------------
  # class methods
  #-------------------------------------------------------------------------------
  #-------------------------------------------------------------------------------
  # scopes
  #-------------------------------------------------------------------------------
  def self.by_account(account_id)
    where(:account_id => account_id)
  end

  def self.ordered
    order("label")
  end

  def self.switcher_order(account, clinic)
    by_account(account.id).ordered
  end

  #-------------------------------------------------------------------------------
  # instance methods
  #-------------------------------------------------------------------------------
  def to_s
    self.label
  end

  # creates an appropriate sized title for the switcher drop down.
  def switcher_title
    st = self.to_s
    st.length > 25 ? st[0..24] + "..." : st
  end
  #-------------------------------------------------------------------------------
  # CSV Import
  #-------------------------------------------------------------------------------
  #-------------------------------------------------------------------------------
  # private methods
  #-------------------------------------------------------------------------------
  private
  def check_cases
    if self.cases.count > 0
  #   TODO identify the patient
  #   errors.add :base, "This fee schedule is currently being used by patient #{self.cases.account}"
      errors[:base] << "This fee schedule is currently being used by a patient"    
      return false
    end
  end
  #-------------------------------------------------------------------------------
  # aliased methods
  #-------------------------------------------------------------------------------
end
