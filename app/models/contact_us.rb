class ContactUs < ActiveRecord::Base
  attr_accessible :city, :comment, :email, :name, :phone
  validates_presence_of :name, :comment
  validates :email, :presence => true, :email => true
end
