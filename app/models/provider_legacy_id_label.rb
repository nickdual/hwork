require_dependency 'csv_import'

class ProviderLegacyIdLabel < ActiveRecord::Base
  include CsvImport
  belongs_to :provider
  belongs_to :legacy_id_label
  accepts_nested_attributes_for :legacy_id_label
  # ensure we only define one legacy id label value / legacy_id_label
  #  - the conditional check is necessary to allow creation process to happen, for
  #    some reason the uniqueness constraint was considered failing when provider_id is nil
  #    during the creation process. 
  validates :legacy_id_label_id, :uniqueness => { :scope => :provider_id }, :unless => "provider_id.nil?"
  
  #-------------------------------------------------------------------------------
  # class methods
  #-------------------------------------------------------------------------------
  # We're going to have to override the basic behaviour here to overcome an annoying
  # result of the triple nesting Provider --> Provider Legacy IDs --> Legacy ID Labels
  # which was resuling in the following type of error:
  #   ActiveRecord::RecordNotFound (Couldn't find LegacyIdLabel with ID=21 for ProviderLegacyIdLabel with ID=):
  #       app/models/provider_legacy_id_label.rb:15:in `new'
  # As best as I can determine, this was previously logged as a bug, fixed, and then the
  # patch for it was removed due to a security hole that it created. Here's the Rails bug
  # info:
  #   https://rails.lighthouseapp.com/projects/8994/tickets/2415-accepts_nested_attributes_for-doest-work-when-nested_attributes-hash-has-an-id-entry
  # FIXME: we should be either validating access to the LegacyIdLabel, or using account_id in the lookup
  # TODO: when we upgrade to Rails 3.2 we should try to walk back the changes to lookup / create LegacyIdLabels here.
  def self.new(attributes={}, options={})
    provider_legacy_id_label = super()
    lid_attr = attributes['legacy_id_label_attributes']
    lid = lid_attr['id'] if lid_attr
    provider_legacy_id_label.legacy_id_label = LegacyIdLabel.find(lid) if lid
    provider_legacy_id_label.legacy_id_label = LegacyIdLabel.new unless provider_legacy_id_label.legacy_id_label
    provider_legacy_id_label.attributes = attributes
    return provider_legacy_id_label
  end

  #-------------------------------------------------------------------------------
  # CSV Import
  #-------------------------------------------------------------------------------
  # the following 6 methods are designed to aid in the loading of data from CSV file
  # with external references to provider.alt_id and legacy_id_label.alt_id, these 
  # attribute methods encapsulate the lookups.
  def account
    @account
  end

  def account=(account)
    @account = account
  end

  def provider_alt_id
    self.provider.try(:alt_id)
  end

  def provider_alt_id=(i_alt_id)
    unless i_alt_id.nil?
      p = Provider.where(:account_id => @account.id, :alt_id => i_alt_id).order('created_at DESC').first
      self.provider = p
    end
  end

  def legacy_id_label_alt_id
    self.legacy_id_label.try(:alt_id)
  end

  def legacy_id_label_alt_id=(i_alt_id)
    unless i_alt_id.nil?
      l = LegacyIdLabel.where(:account_id => @account.id, :alt_id => i_alt_id).order('created_at DESC').first
      self.legacy_id_label = l
    end
  end
end
