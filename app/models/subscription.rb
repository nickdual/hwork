class Subscription < ActiveRecord::Base
  resourcify # add instance method roles
  belongs_to :account
  belongs_to :subscription_plan
  has_many :webhooks , :dependent => :destroy
  # TODO Import subscription class from the RailsKit
  #      This class contains a substantial portion of the functionality provided by
  #      the RailsKit.
  attr_accessible :full_domain, :subscription_plan, :subscription_plan_id, :stripe_token, :coupon
  attr_accessor  :customer, :coupon

  before_destroy :cancel_subscription
  delegate :name, :to => :subscription_plan, :prefix => true
  delegate :trial_period, :to => :subscription_plan

  monetize :amount_cents

  #-----------------------------------------------------------------------------
  # indicates if this subscription has a trial period
  #-----------------------------------------------------------------------------
  def no_trial_period?
    self.trial_period == 0
  end

  #-------------------------------------------------------------------------------
  # This method will take either the subscription_plan object or the name of a
  # subscription_plan object. It will update the local subscription plan as well
  # as push the changes out to Stripe.
  #-------------------------------------------------------------------------------
  def update_plan(subscription_plan)
    unless subscription_plan.is_a? SubscriptionPlan
      subscription_plan = SubscriptionPlan.find_by_name(subscription_plan)
    end
    self.subscription_plan = subscription_plan
    update_stripe_plan(subscription_plan.name)
    self.save!
  end

  def update_stripe_plan(plan_id)
    return true unless do_payments?
    if update_customer
      unless self.customer.subscription.plan.id == plan_id
        self.customer.update_subscription(
          plan: subscription_plan.name
        )
      end
    end
    true
  rescue Stripe::StripeError => e
    logger.error "Stripe Error: " + e.message
    errors.add :base, "Unable to update your subscription. #{e.message}."
    false
  end

  #-------------------------------------------------------------------------------
  # The input stripe token represents a card. It was generated through the client
  # side transactions.
  #-------------------------------------------------------------------------------
  def update_card(stripe_token)
    return true unless do_payments?
    if !stripe_token.blank? and update_customer
      self.customer.card = stripe_token
      self.customer.save
      self.last_4_digits = self.customer.active_card.last4 if self.customer.active_card
      #self.stripe_token = stripe_token
      self.save!
      return true
    end
    return false
  rescue Stripe::StripeError => e
    logger.error "Stripe Error: " + e.message
    errors.add :base, "Unable to create/update credit card. #{e.message}."
    body = e.json_body
    err = body[:error]
    err.delete(:type)
    err.delete(:param)
    @wh = self.webhooks.build(err)
    @wh.save
    false
  end
  def update_stripe_token(stripe_token)
    if !stripe_token.blank?
      self.stripe_token = stripe_token
      self.save
      return true
    end
    return false
  rescue Stripe::StripeError => e
    logger.error "Stripe Error: " + e.message
    errors.add :base, "Unable to create/update credit card. #{e.message}."
    body = e.json_body
    err = body[:error]
    err.delete(:type)
    err.delete(:param)
    @wh = self.webhooks.build(err)
    @wh.save
    false
  end

  #-------------------------------------------------------------------------------
  # determines if this subscription should be integrated with Stripe payments
  #-------------------------------------------------------------------------------
  def do_payments?
    return false if new_record?
    return false if self.account.nil? or self.account.id == Account::DEFAULT_ACCOUNT_ID
    o = self.account.owner()
    return false if o.nil?
    return false if o.email.include?(ENV['ADMIN_EMAIL'])
    return false if o.email.include?('@example.com') and not Rails.env.production?
    return false unless o.confirmed?
    return true
  end

  #-------------------------------------------------------------------------------
  # create or update the basic Stripe::Customer
  # it leaves the current Strip customer reference in the customer field.
  # if this is first creating a customer it will pass the plan argument to 
  # create the subscription within Stripe.
  #-------------------------------------------------------------------------------


  def update_customer
    return true unless do_payments?
    o = self.account.owner()
    if subscription_payment_id.nil?
      self.customer = Stripe::Customer.create(
          email: o.email,
          description: o.login,
          plan: subscription_plan.name
      )
    else
      self.customer = Stripe::Customer.retrieve(subscription_payment_id)
      if o.email != self.customer.email or o.login != self.customer.description
        self.customer.email = o.email
        self.customer.description = o.login
        self.customer.save
      else
        return true
      end
    end
    self.subscription_payment_id = self.customer.id
    unless self.subscription_payment_id.nil?
      self.save!
      return true
    end
    false
  rescue Stripe::StripeError => e
    logger.error "Stripe Error: " + e.message
    errors.add :base, "#{e.message}."
    body = e.json_body
    err = body[:error]
    err.delete(:type)
    err.delete(:param)
    @wh = self.webhooks.build(err)
    @wh.save
    false
  end
  #-------------------------------------------------------------------------------
  # 
  #-------------------------------------------------------------------------------
  def cancel_subscription
    unless subscription_payment_id.nil?
      customer = Stripe::Customer.retrieve(subscription_payment_id)
      unless customer.nil? or customer.respond_to?('deleted')
        if customer.subscription.status == 'active'
          customer.cancel_subscription
        end
      end
    end
  rescue Stripe::StripeError => e
    logger.error "Stripe Error: " + e.message
    errors.add :base, "Unable to cancel your subscription. #{e.message}."
    false

  rescue Stripe::APIError => e
    logger.error "Stripe Authentication error: #{e.message}"
    errors.add :base, "Our system is temporarily unable to process credit cards."
    false
  end

  #-------------------------------------------------------------------------------
  # Creates any missing payment accounts
  #-------------------------------------------------------------------------------
  def self.register_all_accounts
    Subscription.all.each do |s|
      if s.do_payments? and s.subscription_payment_id.nil? 
        s.update_customer
      end
    end
  end

  #-------------------------------------------------------------------------------
  # Deletes all payment accounts. This should really be restricted to use in 
  # development and test environments
  #-------------------------------------------------------------------------------
  def self.unregister_all_accounts
    Subscription.all.each do |s|
      unless s.subscription_payment_id.nil?
        customer = Stripe::Customer.retrieve(s.subscription_payment_id)
        customer.delete
        s.subscription_payment_id = nil
        s.last_4_digits = nil
        s.save!
      end
    end
  end

  #-------------------------------------------------------------------------------
  # First delets all customer payment accounts, then will recreate them.
  #-------------------------------------------------------------------------------
  def self.reset_accounts
    unregister_all_accounts
    register_all_accounts
  end

end
