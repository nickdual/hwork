class Question < ActiveRecord::Base
  attr_accessible :title, :body, :state
  validates :title, :presence => true
  validates :body, :presence => true

  state_machine :state, :initial => :draft do
    event :publish do
      transition any => :published
    end
  end

  def self.ordered
    self.order('created_at ASC')
  end

  def self.switcher_order(account,clinic)
    Question.ordered
  end
  alias_attribute :switcher_title, :title
end
