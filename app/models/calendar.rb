#-------------------------------------------------------------------------------
# Calendar Model
#  represents the container or collection in the Backbone.js sense of the word
#  for the set of appointments which are associated with the calendar's account
#
#  additionally it will store schedule / appointment related preferences which
#  are most relevant to the rendering or interaction with the Appointments
#  interface.
#-------------------------------------------------------------------------------
class Calendar < ActiveRecord::Base
  resourcify # add instance method roles
  has_many :appointments
  belongs_to :account
  delegate :appointment_types, :to => :account
  validates :account, :presence => true
  # The following line is necessary to allow access for the nested forms
  attr_accessible :provider_schedule_by_clinic, :time_slots_per_hour, :days, :view_by
  attr_accessor :clinic

  validates :days, :numericality => {
    :only_integer => true,
    :greater_than_or_equal_to => 1,
    :less_than_or_equal_to => 7
  }

  validates :view_by, :inclusion => { :in => ['room', 'provider'] }

  validates :time_slots_per_hour,
    :numericality => { :only_integer => true },
    :inclusion => { :in => [2, 3, 4, 6, 12], :message => "%{value} is not a valid size" }

  after_initialize :set_dates

  # TODO : add the validation the start and end date parameters are within the allowed bounds.
  attr_accessor :event_selected_date, :event_start_date, :event_end_date

  #-------------------------------------------------------------------------------
  # Does a provider need to specify which clinic they are in when?
  #  If there are multiple clinics, and they have different physical locations,
  #  then we need to know which clinic the provider is available in when.
  # 
  #  Otherwise we don't need to know unless the calendar.provider_schedule_by_clinic
  #  is true.
  #-------------------------------------------------------------------------------
  def provider_schedule_location_required?
    result = false
    if self.account.has_multiple_locations? or self.provider_schedule_by_clinic then
      result = true
    end
    return result
  end

  #-----------------------------------------------------------------------------
  # Convenience method to list the weeks (Mondays) between the two given dates
  # This will INCLUDE the lower bound if it is a Monday
  # This will EXCLUDE the uppper bound if it is a Monday
  #  TODO: move this method into a lib class.
  #-----------------------------------------------------------------------------
  def list_weeks(from_date,to_date)
    r = []
    if to_date and from_date and from_date <= to_date
      # calculates the Monday on the week of the from_date
      unless from_date.wday == 0
        start = from_date - from_date.wday + 1 if from_date
      else
        # for Sunday move to the previous Monday.
        start = from_date - 7 + 1 if from_date
      end
      while start < to_date do
        r << start
        start = start + 7
      end
    end
    return r
  end

  #-----------------------------------------------------------------------------
  # produce the provider schedules to include with appointment calendar
  #  returns the array of provider schedules
  #-----------------------------------------------------------------------------
  def provider_schedules
    self.account.providers.includes(:schedule).map { |provider|
      provider_schedule = []
      self.list_weeks(@event_start_date, @event_end_date).each do |week|
        provider_schedule.concat(provider.schedule.get_schedule_for_week(week))
      end
      data = {
        :id => provider.id,
        :schedule => provider_schedule
      }
    }
  end

  #-----------------------------------------------------------------------------
  # These values are derived from the clinics. It will control the "top" level
  #-----------------------------------------------------------------------------
  def schedule_history_max_days
    self.clinic.schedule_history_max_days
  end

  def schedule_future_max_days
    self.clinic.schedule_future_max_days
  end

  def clinic
    r = @clinic
    r ||= self.account.clinics.first
  end

  #-----------------------------------------------------------------------------
  # This method is called prior to RABL rendering of the calendar to set a 
  # clinic which could affect things like the current view of the calendar.
  #-----------------------------------------------------------------------------
  def appointment_calendar_for(clinic)
    if clinic
      self.clinic = clinic
    else
      ActiveRecord::Base.logger.warn("You don't have a current_clinic you dummy!")
      self.clinic = self.account.clinics.first
    end
    self
  end

  #-----------------------------------------------------------------------------
  # OPTIMIZE : the reason we're including the appointments in as_json through
  #  a method vs. using :include is that it wasn't working reliably in Railsl
  #  3.0.15 despite the fact that all the bugs I'd seen reported appear to have
  #  been fixed long ago, so it seems like it shouldn't be broken. We'll have
  #  to try to switch back to :include => { :appointments => Appointment::JSON_OPTS }
  #  after we upgrade to Rails 3.2.
  #
  #  NOTE: we're now taking advantage of the appts method to pass through the
  #   window of desired appointments.
  #-----------------------------------------------------------------------------
  def appts
    self.appointments.between(self.event_start_date, self.event_end_date)
  end

  protected

  #-----------------------------------------------------------------------------
  # Sets the default range for calendar data to be transmitted to the client
  #  these values will subsequently be controlled from the client side.
  #  OPTIMIZE : could move these values into an account or calendar preference
  #             setting to improve the flexibility for larger accounts.
  #-----------------------------------------------------------------------------
  def set_dates
    # set the central date to today unless it already be set
    self.event_selected_date = Date.today unless self.event_selected_date
    # computes the monday prior to the current week
    self.event_start_date = self.event_selected_date - self.event_selected_date.wday - 6
    # computes the monday 3 weeks after the current week
    self.event_end_date = self.event_selected_date + (8 - self.event_selected_date.wday) + 24
  end
end
