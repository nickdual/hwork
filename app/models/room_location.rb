class RoomLocation < ActiveRecord::Base
  belongs_to :room
  belongs_to :clinic
  validates :room, :presence => true
  validates :clinic, :presence => true
end
