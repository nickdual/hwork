class Visit < ActiveRecord::Base
  belongs_to :case
  has_many :visit_details, :dependent => :destroy
  has_many :visit_diagnoses
  has_many :third_party_bills
end
