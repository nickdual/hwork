#-------------------------------------------------------------------------------
# Employer
#  This is a model which is basically a place holder for additional contact
#  information for a patient. The patient's employer
#-------------------------------------------------------------------------------
class Employer < ActiveRecord::Base
  has_one :contact, :as => :contactable, :class_name => "ContactEmployer"
  has_one :patient
  accepts_nested_attributes_for :contact
  validates :contact, :presence => true 

  def self.build(configuration={})
    employer = Employer.new(configuration)
    employer.contact = ContactEmployer.build(:contactable => employer)
    return employer
  end

  #-------------------------------------------------------------------------------
  # scopes
  #-------------------------------------------------------------------------------
  def self.alphabetically
    joins(:contact).order('contacts.company_name ASC')
  end
end
