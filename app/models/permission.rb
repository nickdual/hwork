class Permission
  CONFIG = YAML.load(File.read("./config/permissions.yml")) 

  attr_accessor :config, :resource

  def initialize(perm=nil)
    if perm.nil?
      @resource = Permission
      @config = Permission::CONFIG['permissions']
    else
      @resource = perm.keys.first.constantize
      @config = perm[perm.keys.first]
    end
  end

  #-----------------------------------------------------------------------------
  # Generates the Node name displayed in the jstree UI for this permission
  #-----------------------------------------------------------------------------
  def resource_title
    @resource.to_s.titleize
  end

  def resource
    @resource
  end

  #-----------------------------------------------------------------------------
  # Some resources might not be fully manageable by the users. For example
  # general HCFA forms can not be created or destroyed by the users. So we 
  # define a lower max value for these nodes in the config/permission.yml
  #-----------------------------------------------------------------------------
  def max
    max_lvl = Permission::CONFIG['levels']['max']
    _config('max', Permission::CONFIG['levels'][max_lvl] )
  end

  #-----------------------------------------------------------------------------
  # Currently assuming the default permission is manage (wide open) to make the
  # system easier to use for new users. Restricting permissions will be 
  # considered an advanced user scenario.
  #-----------------------------------------------------------------------------
  def default
    _config('default',:manage)
  end

  #-----------------------------------------------------------------------------
  # If a permission is completely static or only adjustable programtically
  # it can specify false for configurable to exlude it from being displayed
  # in the jstree in user edit view.
  #-----------------------------------------------------------------------------
  def configurable?
    _config('configurable',true)
  end

  #-----------------------------------------------------------------------------
  # Walks through the children of this permission
  #-----------------------------------------------------------------------------
  def children
    children = _config('children',[])
    unless children.empty?
      return children.map do |c|
        Permission.new(c)
      end
    end
    return []
  end

  #-----------------------------------------------------------------------------
  # in order search of the permission tree to find the permission for the 
  # specified resource type.
  #-----------------------------------------------------------------------------
  def find(resource_type)
    return self if @resource.to_s == resource_type
    children.each do |c_perm|
      p = c_perm.find(resource_type)
      return p unless p.nil?
    end
    return nil
  end

  #-----------------------------------------------------------------------------
  # determine if the user has any defined role for the resource associated
  # with this permission
  # NOTE : we start checking the roles at 1 (vs. 0) becuase the 0 level 
  #        is taken to mean no access.
  #-----------------------------------------------------------------------------
  def has_role?(user)
    for i in 1..Permission::CONFIG['levels']['max']
      return true if user.has_role?( Permission::CONFIG['levels'][i], @resource)
    end
    return false
  end

  def get_access_level(user)
    return self.max if user.admin? or user.has_role?(:manage, @resource)
    return 'create' if user.has_role?(:create, @resource)
    return 'update' if user.has_role?(:update, @resource)
    return 'read' if user.has_role?(:read, @resource)
    return 'no_access'
  end

  #-----------------------------------------------------------------------------
  # return the array of permits granted to this user based on (a) the roles
  # they've been assigned (b) the heirarchy of permission levels
  #-----------------------------------------------------------------------------
  # TODO rewrite the Permission.allows logic to run from the configured levels in permissions.yml
  def allows(user)
    if user.has_role?(:manage, @resource)
      return [ :manage ]
    elsif user.has_role?(:create, @resource)
      return [ :read, :update, :create ]
    elsif user.has_role?(:update, @resource)
      return [ :read, :update ]
    elsif user.has_role?(:read, @resource)
      return [ :read ]
    end
  end

  #-----------------------------------------------------------------------------
  # Creates or fills in missing roles for a user based on the heirarchy of
  # roles defined in the config/permission.yml.
  # OPTIMIZE - Its a bit slow when its first initializing a user, there's a
  # possibility we could circumvent the Rolify add_role method and just create
  # the missing roles by hand within a single transaction ... it might be faster.
  #-----------------------------------------------------------------------------
  def self.initialize_user_roles(user, perm = nil)
    perm ||= Permission.new()
    # we're specifically checking the roles by query vs. has_role? because
    # we need to consider having a 'no_access' role to be having a role.
    if user.roles.where(:resource_type => perm.resource.to_s).empty?
      user.add_role(perm.default,perm.resource)
    end
    perm.children.each do |c_perm|
      initialize_user_roles(user,c_perm)
    end
  end

  #-----------------------------------------------------------------------------
  # To make the form work as expected, we're using roles with user
  # accepts_nested_attributes_for ; however, we don't actually want to update
  # the roles directly, what we want to do is update the join table users_roles
  # NOTE: when we remove roles from a user via the rolify interface, it 
  #       potentially deletes the underlying role if it is no longer referenced
  # OPTIMIZE: I think it would be faster to implement a remove_role version
  #           that directly updates the users_roles join table; however, would
  #           have to be verified to make sure it didn't conflict with rolify to
  #           do so.
  # OPTIMIZE: The update_roles process might be sped up significantly by looking
  #           only for role changes this could be done either by passing additional
  #           data from the client, or by running additional queries to determine
  #           the role is correct. The later approach would definitely be better
  #           assuming something else (like a trigger) would prevent the user 
  #           from having multiple roles for the same resource.
  #-----------------------------------------------------------------------------
  def self.update_roles(user, params)
    if params
      params.each do |r|
        props = r[1]
        resource_type = props['resource_type']
        name = props['name']
        if name and resource_type
          # now we need to remove all the other roles belonging to this user
          # for the resource_type, then add the new one.
          user.roles.where("resource_type = ? and name != ?",resource_type,name).each do |r|
            r.revoke(user)
          end
          begin
            user.add_role(name,resource_type.constantize)
          rescue
            Rails.logger.warn("tried #{name} with #{resource_type}")
          end
        end
      end
    end
  end

  #-----------------------------------------------------------------------------
  # return the access level summary value for the given permission
  # access levels are: full, partial, none
  #   full --> you have max access for this and all lower level permissions
  #   partial --> you have some access for tiehr this or a lower level permission
  #   none --> you have no_access for this and all lower level permissions
  #
  # from a traversal perspective, we want to avoid searching the entire tree
  # unless we have to, so 
  #   if not full and not none, then return partial will short circuit the 
  #   traversal
  #
  # NOTE : there's a little gap in this logic, if we set the max = no_access (0)
  #        for some resource, then it would potentially be both full and none
  #        and while the function would return 'full' its definitely arbitrary
  #-----------------------------------------------------------------------------
  def self.access_level(user, perm = nil)
    perm ||= Permission.new()
    return 'none' if user.nil?
    return 'full' if user.admin?
    user_level = perm.get_access_level(user)
    is_full = access_level_rank(user_level) < access_level_rank(perm.max) ? false : true
    is_none = access_level_rank(user_level) > 0 ? false : true
    return 'partial' unless is_full or is_none
    # since we're either full or none at this point we need to traverse children
    perm.children.each do |c_perm|
      c_level = Permission.access_level(user,c_perm)
      return 'partial' if is_full and c_level != 'full'
      return 'partial' if is_none and c_level != 'none'
    end
    # now we're done with traversal
    return 'full' if is_full
    return 'none' if is_none
  end

  #-----------------------------------------------------------------------------
  # returns the numeric rank for an access level to be used for comparison
  # purposes. a higher rank implies more access.
  #-----------------------------------------------------------------------------
  def self.access_level_rank(level)
    Permission::CONFIG['levels'].invert[level]
  end

  #-----------------------------------------------------------------------------
  # initialize all the current roles defined in the permission.yml that aren't
  # already defined. Designed to be called a from DB seed script, or as 
  # a rake task.
  #-----------------------------------------------------------------------------
  def self.initialize_all_roles
    perm = Permission.new()
    perm.initialize_roles
  end

  #-----------------------------------------------------------------------------
  # Initiliaze the resource roles specified by this Permission.
  #-----------------------------------------------------------------------------
  def initialize_roles
    for i in 0..Permission::CONFIG['levels']['max']
      if Role.where(:name => Permission::CONFIG['levels'][i], :resource_type => @resource.to_s).empty?
        Role.create!(:name => Permission::CONFIG['levels'][i], :resource_type => @resource.to_s)
      end
    end
    children.each do |c_perm|
      c_perm.initialize_roles
    end
  end

  #-----------------------------------------------------------------------------
  # return the valid access levels for the resource type. the defacto is to 
  # return all levels; however, the permission.yml configuration can specify
  # a more restrictive max for a particular resource type.
  #-----------------------------------------------------------------------------
  def self.access_levels(resource_type)
    result = []
    perm = Permission.new.find(resource_type)
    pmax = perm.nil? ? '' : perm.max
    for i in 0..Permission::CONFIG['levels']['max']
      level = Permission::CONFIG['levels'][i]
      result.push([I18n.translate("access.#{level}"), level])
      break if level == pmax
    end
    return result
  end

  private

  def _config(property,default)
    @config[property].nil? ? default : @config[property]
  rescue
    return default
  end

end
