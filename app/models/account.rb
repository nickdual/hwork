require_dependency 'resource_seeder'

class Account < ActiveRecord::Base
  resourcify # add instance method roles
  has_many :users, :dependent => :destroy
  has_many :clinics, :dependent => :destroy
  has_many :providers, :dependent => :destroy
  has_many :fee_schedules, :dependent => :destroy
  has_many :diagnosis_codes, :dependent => :destroy
  has_many :procedure_codes, :dependent => :destroy
  has_many :legacy_id_labels, :dependent => :destroy
  has_many :hcfa_form_options, :dependent => :destroy
  has_many :account_hcfa_form_options, :dependent => :destroy
  has_many :carrier_hcfa_form_options, :dependent => :destroy
  has_many :appointment_types, :dependent => :destroy
  has_one :subscription, :dependent => :destroy
  has_one :calendar, :dependent => :destroy
  has_one :account_setup, :dependent => :destroy
  has_many :rooms, :dependent => :destroy
  has_many :patients, :dependent => :destroy
  has_many :third_parties, :dependent => :destroy
  has_many :references, :dependent => :destroy
  has_many :messages, :as => :messageable, :dependent => :destroy
  attr_accessible :full_domain, :subscription, :clinic_name
  # The following line is necessary to allow access for the nested forms
  attr_accessible :subscription_attributes, :calendar_attributes, :stripe_token, :coupon
  accepts_nested_attributes_for :subscription, :users, :calendar

  DEFAULT_ACCOUNT_ID = 999999 # reference global defaults account
  # the global default account will be used to configure initial data for 
  # new accounts / clinics for things like fee schedules and permissions
  # settings.

  after_create :build_calendar_if_nil, :initialize_defaults

  delegate :subscription_plan_name, :no_trial_period?, :trial_period, :to => :subscription

  #-----------------------------------------------------------------------------
  # define the account workflow rules
  #-----------------------------------------------------------------------------
  state_machine :initial => :new do
    # event when automated account creation process is completed
    event :seed do
      transition :new => :ready
      transition :trial => :trial
    end
    event :confirm do
      transition [:new, :ready] => :trial
    end
    # event when any user signs onto this account
    event :login do
      transition all - [:new, :cancelled] => same
    end
    # event whenever any user attempts to use this account
    event :use do
      transition all - [:new, :collections] => same
    end
    event :expire_trial do
      transition :trial => :collections
    end
    event :cancel do
      transition all => :cancelled
    end
    # unseeded brand new account
    state :new do
      def notice
        I18n.translate("state.account.new.account_setup_processing")
      end
      def allow_request(request)
        return false
      end
    end
    # automated account setup complete
    state :ready do
    end
    # nominal account status
    state :active do
    end
    # something required for payment to activate this account
    # this could be due to no valid payment method, or payment failed
    state :collections do
      def notice
        I18n.translate("state.account.collections.setup_payment_method")
      end
      def fix_path(user)
        if self.admin?(user)
          return "update_card_subscription_path"
        end
        return "destroy_user_session_path"
      end
      def allow_request(request)
        return true if request.url =~ /admin\/subscription/
      end
    end

    state :cancelled do
      def notice
        I18n.translate("state.account.cancelled.been_cancelled")
      end
      def allow_request(request)
        return false
      end
    end

    state all - [:collections, :new] do
      def allow_request(request)
        logger.info "doing something with #{self}"
        if self.account_setup
          return false
        end
        return true
      end
      def fix_path(user)
        return "edit_account_account_setup_path"
      end
    end

    after_transition any => :trial, :do => :start_setup
    after_transition any => any, :on => :login do |account|
      account.account_setup.login! if account.account_setup
    end

  end

  #-----------------------------------------------------------------------------
  # Setup the interface to use the Redis key/value store to cache various bits
  # of information for fast access all keyed based on the account.
  #  
  # The keys will all be stored under the cache:[host abbrev] namespace see
  # the initializers/resque.rb for the details of host abbrev. Any cached data
  # is going to be associated with an account so it'll be under the sub-namespace
  # of account:[id].
  #
  # Flush will be processed under the base key cache:[host abbrev]:account:[id]
  # for any keys matching suffix* will be deleted.
  #-----------------------------------------------------------------------------
  REDIS_NAMESPACE = Resque.redis.namespace.sub("resque","cache")

  def redis_key(string)
    "#{REDIS_NAMESPACE}:account:#{self.id}:#{string}"
  end

  def self.redis
    Resque.redis
  end

  def flush(suffix)
    c_key = redis_key(suffix) + "*"
    r = Account.redis
    r.keys(c_key).each do |key|
      r.del(key)
    end
  end

  #-----------------------------------------------------------------------------
  # return the account containing default initial settings
  #  - to facilitate barebones testing we'll return an empty account defaults
  #    in the test environment assuming one is missing. if its missing and we're
  #    not in the test environment, then we'll re-throw the exception.
  #-----------------------------------------------------------------------------
  def self.get_default
    find(DEFAULT_ACCOUNT_ID)
  rescue ActiveRecord::RecordNotFound => e
    return Account.new() if Rails.env == 'test'
    raise
  end

  #-----------------------------------------------------------------------------
  # a scope method to exclude the default template account from a 
  # list
  #-----------------------------------------------------------------------------
  def self.exclude_default
    where("id != :id", :id => DEFAULT_ACCOUNT_ID)
  end

  #-----------------------------------------------------------------------------
  # This method is called upon the confirmation of a new user registration
  # which created the account. It could be used as a place to perform any 
  # new account setup tasks (e.g. assigning AccountAdmin role).
  #
  # This will also be the point at which we'll send information to the payment
  # system.
  #-----------------------------------------------------------------------------
  def init_user(user)
    # check if account already setup
    if self.init_user? or user.account.id != self.id then
      return
    end
    # assign admin role to initial user
    user.add_role Role::ACCOUNT_ADMIN
    return
  end

  #-----------------------------------------------------------------------------
  # determine if the account has been initialized, or is still pending.
  #-----------------------------------------------------------------------------
  def init_user?
    # currently checking users
    self.users.count > 1
  end

  #-----------------------------------------------------------------------------
  # should the user be directed through setup wizards and help information
  #-----------------------------------------------------------------------------
  def new_account?
    # currently checking clinics
    self.clinics.count == 0
  end

  #-----------------------------------------------------------------------------
  # indicates if the specified user is an admin for this account.
  #-----------------------------------------------------------------------------
  def admin?(user)
    user ||= current_user
    user.has_role? Role::ACCOUNT_ADMIN
  end

  #-----------------------------------------------------------------------------
  # indicates if the specified user has the super admin role
  #-----------------------------------------------------------------------------
  def app_admin?(user)
    user ||= current_user
    user.has_role? Role::APPLICATION_ADMIN
  end

  #-----------------------------------------------------------------------------
  # returns information about the "current" owner of an account.
  #-----------------------------------------------------------------------------
  def owner
    self.users.order(:created_at).each do |user|
      return user if self.admin?(user)
    end
    return nil
  end
  #-----------------------------------------------------------------------------
  # initialize the default values for a new account
  #-----------------------------------------------------------------------------
  def initialize_defaults
    Resque.enqueue(Jobs::Seed,self.id) unless Rails.env == 'test'
  end

  #-----------------------------------------------------------------------------
  # This method is typically called a Resque job.
  #-----------------------------------------------------------------------------
  def initialize_seed_data
    Account.transaction do
      self.initialize_appointment_types
      ResourceSeeder.seed_clinics(self)
      ResourceSeeder.seed_providers(self)
      ResourceSeeder.seed_legacy_id_labels(self)
      ResourceSeeder.seed_legacy_ids(self)
      ResourceSeeder.seed_third_parties(self)
      ResourceSeeder.seed_references(self)
      ResourceSeeder.seed_diagnosis_codes(self)
      ResourceSeeder.seed_procedure_codes(self)
      ResourceSeeder.seed_fee_labels(self)
      ResourceSeeder.seed_fees(self)
      ResourceSeeder.seed_messages(self)
      ResourceSeeder.seed_rooms(self)
      ResourceSeeder.seed_hcfa_form_options(self)
      ResourceSeeder.seed_patients(self)
      self.save()
    end
  end

  #-----------------------------------------------------------------------------
  # setup the default appointment types for a new account.
  #-----------------------------------------------------------------------------
  def initialize_appointment_types
    Account.transaction do
      Account.get_default.appointment_types.each do |st|
        logger.debug "working on #{st.name}"
        if self.appointment_types.where(:name => st.name).empty? then
          new_st = st.clone
          new_st.account_id = self.id
          new_st.save!
        end
      end
      self.save!
    end
  end

  #-----------------------------------------------------------------------------
  # initialize the calendar for this account
  #-----------------------------------------------------------------------------
  def build_calendar_if_nil
    logger.debug "building account calendar now"
    self.create_calendar if self.calendar.nil?
  end

  #-------------------------------------------------------------------------------
  # Indicates this account covers multiple locations
  #-------------------------------------------------------------------------------
  def has_multiple_locations?
    if self.clinics.count > 1 then
      # look though the list of clinics to see if there any that are in different locations
      base_location = self.clinics.first
      for index in 1...self.clinics.count
        if base_location.different_location?(self.clinics[index])
          return true
        end
      end
    end
    return false
  end

  #-------------------------------------------------------------------------------
  # Are room locations required?
  #  If there are multiple clinics, and they have different physical locations,
  #  then we need to know which clinic contains a particular room
  #-------------------------------------------------------------------------------
  def room_location_required?
    result = false
    if self.has_multiple_locations? then
      result = true
    end
    return result
  end

  #-------------------------------------------------------------------------------
  # return a string indicating the time remaining in the trial period for this 
  # account
  #-------------------------------------------------------------------------------
  def trial_remaining
    if self.state == "trial" 
      remaining = ( self.created_at + self.trial_period.days - Time.now ) / ( 60 * 60 * 24 )
      return "#{remaining.round} Days Remain"
    end
    return "Time Expired"
  end

  #-------------------------------------------------------------------------------
  # indicates if this account is in the process of being setup
  #-------------------------------------------------------------------------------
  def in_setup?
    if self.account_setup and not self.account_setup.completed?
      return true
    end
    return false
  end

  #-------------------------------------------------------------------------------
  # Applies the initial user requested clinic name to seed data clinic
  #-------------------------------------------------------------------------------
  def personalize_clinic_name
    logger.info("personalize clinic : #{self.clinic_name}")
    unless self.new?
      if self.clinic_name and self.clinics.count > 0
        clinic = self.clinics.first
        clinic.name = self.clinic_name
        clinic.save!
        self.clinic_name = nil
        self.save!
      end
    end
  end

  #-------------------------------------------------------------------------------
  # Returns a hash of clinics which can be referenced to define a physical location
  # based on an existing clinic.
  #-------------------------------------------------------------------------------
  def clinic_links(clinic)
    r = {}
    self.clinics.each do |c| 
      r[c.id] = c.physical_address_one_liner unless c.id == clinic.try(:id) or c.location_clinic
    end
    return r
  end


  protected

  #-------------------------------------------------------------------------------
  # Start the setup worflow for this account.
  #   - creates a new account setup object if necessary
  #   - applies the requested clinic name to the seed data
  #   - creates the payment customer object.
  #-------------------------------------------------------------------------------
  def start_setup
    self.create_account_setup unless self.account_setup
    self.personalize_clinic_name unless self.clinic_name.blank?
    self.subscription.update_customer if self.subscription.subscription_payment_id.nil?
  end

end
