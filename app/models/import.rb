require 'csv'

class Import < ActiveRecord::Base
  resourcify # add instance method roles
  belongs_to :account
  belongs_to :user
  belongs_to :import_group
  has_many :import_records, :dependent => :destroy
  validates :import_type, :presence => true
  validates :account, :presence => true
  validates :user, :presence => true
  # FIXME : Import attachments are not being stored in the requested location, they're ending up under public/system
  #         which is the default for paperclip. This was working under Ruby 1.9
  has_attached_file :data, :options => {
    :path => ":rails_root/tmp/files/:attachment/:id/:style/:filename",
    :default_url => "NODATA"
  }

  #-----------------------------------------------------------------------------
  # returns the total number of records for this import
  #-----------------------------------------------------------------------------
  def records_total
    self.import_records.size
  end

  def records_imported
    self.import_records.where(:status => 1).size
  end

  #-----------------------------------------------------------------------------
  # indicates if there were errors in this import
  #-----------------------------------------------------------------------------
  def errors?
    self.import_records.where("status != 1").size > 0
  end
  #-----------------------------------------------------------------------------
  # produce the report for displaying errors
  #-----------------------------------------------------------------------------
  def error_report
    Import.report_table( :all, :only => [:import_type],
      :include => { :import_records => { :only => [:record, :status] } },
      :conditions => [ "import_records.import_id = #{self.id} and import_records.status != 1" ])
  end

  #-----------------------------------------------------------------------------
  # returns the class to be used to load this imported data
  #-----------------------------------------------------------------------------
  def get_csv_model(import_type)
    file_configuration = APP_CONFIG['csv_import']['configuration']['groups'].map { |g| g['files'] }.flatten.select { |i| i['type'] == import_type }[0]
    m = eval "#{file_configuration['class']}"
  end

  #-----------------------------------------------------------------------------
  # primary method to interface with the csv loading methods
  #-----------------------------------------------------------------------------
  def load_data(user, clinic, account, error_code=1, duplicate_code=2)
    self.user = user
    self.account = account
    if error_code == 0
      Import.transaction do
        return _load_data(user,clinic,account, error_code, duplicate_code)
      end
    else
      return _load_data(user,clinic,account, error_code, duplicate_code)
    end

  end


  private

  def _load_data(user, clinic, account, error_code=1, duplicate_code=2)
    self.status = "Processing" and self.save
    a_model = get_csv_model(self.import_type)
    if a_model.nil?
      self.errors.add(:import_type,"Import type is not currently supported")
      return false
    end
    a_config = a_model.get_csv_config(self.import_type)
    quote_char = a_config['quote']
    counter = 0
    # create the records
    Import.get_csv_lib.parse(self.data.to_file.read,
              :headers => a_model.csv_headers,
              :converters => a_model.csv_converters,
              :quote_char => quote_char.nil? ? '"' : quote_char # for now assuming this can not be disabled by passing nil.
             ) do |row|
     counter = counter + 1
     self.status = "Processing record #{counter}" and self.save
     # add the import record
     row_data = row.to_s
     unless row_data.blank?
       self.import_records << (record = ImportRecord.new(:record => row_data))
       model_instance = a_model.load_csv_record(self.import_type, user, clinic, account, row)
       unless model_instance.nil?
         begin
           # check for duplicates
           model_duplicate = model_instance.csv_duplicate(self.import_type) 
           unless model_duplicate.nil?
             # duplicate model found
             case duplicate_code
             when 0
               logger.error("CSV :: Line ##{counter + 1} : duplicate record error")
               # treate duplicates as errors
               self.errors.add(:import_records, "Line ##{counter + 1} : duplicate record error")
               record.status = 2
               return false # FIXME : refactor this to behave according to error_code
             when 1
               logger.warn("CSV :: Line ##{counter + 1} : duplicate record replacement")
               # replace the duplicate record
               model_duplicate.destroy
               # NOTE: the behaviour in this case will result in the resource changing IDs, which 
               #       could break linkages which have been created after the initial import was performed.
             when 2
               # update the old record with the new record
               logger.info("CSV :: Line ##{counter + 1} : duplicate record update")
               logger.debug("CSV :: --> new duplicate data #{model_instance.inspect}")
               model_duplicate.csv_update_attributes_from_import(self.import_type,model_instance)
               model_instance = model_duplicate
               logger.debug("CSV :: --> duplicate record updated #{model_instance.inspect}")
             when 3
               logger.info("CSV :: Line ##{counter + 1} : duplicate record ignored")
               record.status = 4
               return true
             end
           end
           if model_instance.valid?
             model_instance.save! and logger.debug("CSV :: model instance saved #{model_instance.inspect}")
             record.model = model_instance
             record.status = 1
             record.save! and logger.debug("CSV :: import record saved #{record.inspect}")
           else
             # TODO handle import_record errors more gracefully
             logger.warn("CSV :: problem loading #{row}")
             logger.warn("CSV ::  --> #{model_instance.errors}")
             self.errors.add(:import_records, "Line ##{counter + 1} : #{model_instance.errors.to_s}")
             record.status = 2
           end
         rescue Exception => e
           message = "Line ##{counter + 1} exception: #{e.message}"
           logger.error("CSV :: CSV import EXCEPTION: #{message}")
           self.errors.add(:import_records,message)
         end
       else
         record.status = 3
         logger.info("CSV :: Line ##{counter + 1} : skip record intentionally")
       end
     end
   end
   # look for a callback in this configuration
   if a_config['callback']
     logger.info("CSV :: CSV import going to make a callback #{a_config['callback']}")
     begin
       eval "a_model.#{a_config['callback']}(user, clinic, account, error_code, duplicate_code)"
     rescue Exception => e
       message = "Line ##{counter + 1} exception: #{e.message}"
       logger.error("CSV :: CSV import callback error: #{message}")
       self.errors.add(:import_records,message)
     end
   end
   # if we fail on errors, this won't be saved, only if we continue will this be persisted
   self.status = "Loaded" and self.save
   if self.errors.size == 0
     return true
   end
   return false
  end

  #-----------------------------------------------------------------------------
  # Supports compatibility with Ruby 1.8 using FasterCSV and Ruby 1.9 using 
  # Ruby's CSV stdlib module. The idea for the patch came from:
  # http://wherethebitsroam.com/blogs/jeffw/fastercsv-csv-ruby-18-19-and-rails-30
  #-----------------------------------------------------------------------------
  def self.get_csv_lib
    if CSV.const_defined? :Reader
      csv = FasterCSV
    else
      csv = CSV
    end
    return csv
  end

end
