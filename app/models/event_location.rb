class EventLocation < ActiveRecord::Base
  belongs_to :event
  belongs_to :clinic
  validates :event, :presence => true
  validates :clinic, :presence => true
end
