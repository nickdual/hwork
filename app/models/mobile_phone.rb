class MobilePhone < Phone
  def self.new(attributes={}, options={})
    phone = super(attributes, options)
    phone.label = 'Mobile'
    return phone
  end
end
