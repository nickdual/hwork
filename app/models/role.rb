require_dependency 'errors'

class Role < ActiveRecord::Base
  has_and_belongs_to_many :users, :join_table => :users_roles
  belongs_to :resource, :polymorphic => true
  attr_accessible :name, :description, :resource_type, :resource_id
  
  scopify

  APPLICATION_ADMIN = 'ApplicationAdmin'
  ACCOUNT_ADMIN = 'AccountAdmin'
  ACCOUNT_STAFF = 'Staff'

  #-----------------------------------------------------------------------------
  # Assign a Role to a user
  #-----------------------------------------------------------------------------
  def assign(user)
    args = self.role_scope
    user.add_role *args
  end

  #-----------------------------------------------------------------------------
  # Revoke a role from a user
  #-----------------------------------------------------------------------------
  def revoke(user)
    args = self.role_scope
    user.revoke *args
  end

  #-----------------------------------------------------------------------------
  # indicate if this matches the indicated string rolename
  # will also match against a Role instance
  #-----------------------------------------------------------------------------
  def is?(rolename)
    return self.name == rolename.name if rolename.kind_of?(Role)
    return self.name == rolename.to_s.camelize
  end

  #-----------------------------------------------------------------------------
  # valid_resource? indicates if the resource_type specified with this role is
  # still valid. why this function is necessary is that if we remove or modify
  # types after permissions have been assigned, there will potentially be 
  # stale role information associated with a particular user. when that user's
  # permissions are next updated, it would potentially result in an
  # NameError: uninitialized constant for the stale roles. So we'll use this
  # method to filter out stale roles from an attempt to be reapplied.
  #-----------------------------------------------------------------------------
  def valid_resource?
    return true if self.resource_type.constantize
  rescue NameError => e
    return false
  end

  #-----------------------------------------------------------------------------
  # check for role
  #   WARNING : this is only valid for global roles
  #-----------------------------------------------------------------------------
  def self.has_role(user, rolename)
    if rolename.kind_of?(Role)
      r = rolename
    else
      r = Role.find_by_name(rolename.to_s.camelize)
    end
    !r.users.find(user).nil?
  rescue
    #TODO -- restrict to ActiveRecord::RecordNotFound
    return false
  end


  #-----------------------------------------------------------------------------
  # expand this Role into the arguments necessary to assign or revoke it
  # returns an array which can be expanded via the '*' operator to use as
  # arguments to user.add_role or user.revoke (rolify)
  #-----------------------------------------------------------------------------
  def role_scope
    if self.resource_type.nil?
      [self.name]
    elsif self.resource_id.nil?
      # returns the class form the string
      a_class = eval self.resource_type
      [self.name, a_class]
    else
      [self.name, self.resource]
    end
  end


  #-----------------------------------------------------------------------------
  # ensure there's always at least one admin user
  # for any account
  #-----------------------------------------------------------------------------
  def can_revoke?(user)
    if self.is?(Role::ACCOUNT_ADMIN)
      unless user.account.users.with_role(Role::ACCOUNT_ADMIN).count > 1
        raise Errors::RoleAssignmentError.new(I18n.translate("flash.error.roles.one_admin"))
      end
    end
    # to play nice with form validation, we need to return a true value if 
    # there's no problem with removing the role in question.
    return true
  end

  protected

end
