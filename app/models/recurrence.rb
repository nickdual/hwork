require_dependency 'enum_support'

class Recurrence < ActiveRecord::Base
  extend EnumSupport
  belongs_to :event
  validates :event, :presence => true
  validates :interval, :numericality => { :only_integer => true }, :allow_nil => true
  validates :count, :numericality => { :only_integer => true }, :allow_nil => true

  enum_map :interval_type, APP_CONFIG['options']['event_recur_interval_types']

  default_value_for :interval_type, 0
end
