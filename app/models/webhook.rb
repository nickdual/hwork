class Webhook < ActiveRecord::Base
  attr_accessible  :message,  :subscription_id
  belongs_to :subscription
end
