class CaseDiagnosis < ActiveRecord::Base
  belongs_to :case
  belongs_to :diagnosis_code
end
