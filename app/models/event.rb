class Event < ActiveRecord::Base
  resourcify # add instance method roles
  belongs_to :eventable, :polymorphic => true
  has_one :recurrence, :dependent => :destroy
  has_many :event_locations, :dependent => :destroy
  has_many :clinics, :through => :event_locations
  validates :eventable, :presence => true
  validates :start_time, :presence => true
  validates :duration, :numericality => { :only_integer => true, :greater_than => 0 }, :presence => true
  validates :day_of_week, :numericality => { :only_integer => true, :greater_than_or_equal_to => 0, :less_than_or_equal_to => 6 }, :presence => true
  validates :lead_time, :numericality => { :only_integer => true }, :presence => true
  validates :lag_time, :numericality => { :only_integer => true }, :presence => true

  before_save :set_day_of_week

  #-----------------------------------------------------------------------------
  # scopes
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  #  For the week Monday - Sunday which covers this date
  #-----------------------------------------------------------------------------
  def self.for_week(date)
    self.by_date_range((date - date.wday + 1),(date - date.wday + 7))
  end

  #-----------------------------------------------------------------------------
  # This is strictly looking at the *date* of from_date..to_date (inclusive)
  # So it will include events which occur anytime on the to_date.
  #-----------------------------------------------------------------------------
  def self.by_date_range(from_date,to_date)
    self.where(:start_date => from_date..to_date)
  end

  default_value_for :lead_time, 0
  default_value_for :lag_time, 0
  default_value_for :duration, 60
  default_value_for :day_of_week, 2

  JSON_OPTS = { :except => [:created_at, :updated_at], :include => :recurrence, :include => { :clinics => { :only => [:id] } } }
  #JSON_OPTS = { :except => [:created_at, :updated_at], :include => :recurrence }
  def as_json(options={})
    super(JSON_OPTS.merge(options))
  end

  private

  #-----------------------------------------------------------------------------
  # We're not using the day_of_week from the client side, but it's used in
  # generating the schedule for weeks which is processed on the server side,
  # so we'll just set the expected value when the event is created.
  #-----------------------------------------------------------------------------
  def set_day_of_week
    self.day_of_week = self.start_date.wday if self.start_date
  end

end
