require_dependency 'csv_import'
require_dependency 'enum_support'
class DiagnosisCode < ActiveRecord::Base
  resourcify # add instance method roles
  include CsvImport
  extend EnumSupport
  #-------------------------------------------------------------------------------
  # Behaviours
  #-------------------------------------------------------------------------------
  # acts_as_paranoid
  #-------------------------------------------------------------------------------
  # associations
  #-------------------------------------------------------------------------------
  #has_many :patient1_cases, :class_name => 'PatientCase', :foreign_key => 'diagnosis1_id'
  #has_many :patient2_cases, :class_name => 'PatientCase', :foreign_key => 'diagnosis2_id'
  #has_many :patient3_cases, :class_name => 'PatientCase', :foreign_key => 'diagnosis3_id'
  #has_many :patient4_cases, :class_name => 'PatientCase', :foreign_key => 'diagnosis4_id'
  #has_many :patient5_cases, :class_name => 'PatientCase', :foreign_key => 'diagnosis5_id'
  #has_many :patient6_cases, :class_name => 'PatientCase', :foreign_key => 'diagnosis6_id'
  #has_many :patient7_cases, :class_name => 'PatientCase', :foreign_key => 'diagnosis7_id'
  #has_many :patient8_cases, :class_name => 'PatientCase', :foreign_key => 'diagnosis8_id'
  #has_many :patient1_visits, :class_name => 'PatientVisit', :foreign_key => 'diagnosis1_id'
  #has_many :patient2_visits, :class_name => 'PatientVisit', :foreign_key => 'diagnosis2_id'
  #has_many :patient3_visits, :class_name => 'PatientVisit', :foreign_key => 'diagnosis3_id'
  #has_many :patient4_visits, :class_name => 'PatientVisit', :foreign_key => 'diagnosis4_id'
  #has_many :patient5_visits, :class_name => 'PatientVisit', :foreign_key => 'diagnosis5_id'
  #has_many :patient6_visits, :class_name => 'PatientVisit', :foreign_key => 'diagnosis6_id'
  #has_many :patient7_visits, :class_name => 'PatientVisit', :foreign_key => 'diagnosis7_id'
  #has_many :patient8_visits, :class_name => 'PatientVisit', :foreign_key => 'diagnosis8_id'
  belongs_to :account
  #-------------------------------------------------------------------------------
  # validations
  #-------------------------------------------------------------------------------
  validates :name, :presence => true, :length => { :maximum => 15 }, :uniqueness => { :case_sensitive => true, :scope => :account_id }

  #icd10 or icd9 is required, but we only validate 1 field to avoid duplicate error messages
  #when both are nil.
  validates :code, :presence => { :message => "either ICD-9 or ICD-10 code is required" }, :if => "icd10.blank?"
  #validates :icd10, :presence => { :message => "either ICD-10 or ICD-9 code is required" }, :if => "code.blank?"

  validates :description, :presence => true
  # enum_map :icd_version, APP_CONFIG['options']['icd_versions']
  # validates_uniqueness_of :name
  #-------------------------------------------------------------------------------
  # class methods
  #-------------------------------------------------------------------------------
  #-------------------------------------------------------------------------------
  # scopes
  #-------------------------------------------------------------------------------
  scope :alphabetically, :order => 'name ASC'

  def self.switcher_order(account,clinic)
    by_account(account.id).order(:name)
  end

  def self.by_account(account_id)
    where("account_id = ?",account_id)
  end

  #-------------------------------------------------------------------------------
  # CSV Import
  #-------------------------------------------------------------------------------
  #-------------------------------------------------------------------------------
  # instance methods
  #-------------------------------------------------------------------------------
  def dropdown_title
    "#{code} - #{name} - #{description}"
  end

  def title
    "#{name} - #{code} - #{description}"
  end

  def switcher_title
    "#{code} - #{name}"
  end
  
  # Do not allow deletion of diagnosis code if it involved in patient case or
  # visit
  def can_delete?
    PatientCase.patient_account_id_equals(self.account_id).diagnosis1_id_or_diagnosis2_id_or_diagnosis3_id_or_diagnosis4_id_equals(self.id).empty? &&
      PatientVisit.diagnosis1_id_or_diagnosis2_id_or_diagnosis3_id_or_diagnosis4_id_equals(self.id).
      find(:all, :joins => {:patient_case => :patient}, 
      :conditions => {:patient_case => {:patients => {:account_id => self.account_id}}}
    ).empty?
  end
end
