require_dependency 'csv_import'
require_dependency 'public_appointment'

class Appointment < ActiveRecord::Base
  resourcify # add instance method roles
  include CsvImport
  include PublicAppointment
  belongs_to :calendar
  belongs_to :provider
  belongs_to :contact
  belongs_to :appointment_type
  belongs_to :room
  has_one :event, :as => :eventable, :dependent => :destroy
  delegate :version, :to => :calendar

  validates :calendar, :presence => true
  #validates :contact, :presence => true
  validates :event, :presence => true

  #-------------------------------------------------------------------------------
  # scopes
  #-------------------------------------------------------------------------------
  def self.by_date
    joins(:event).order(:start_date, :start_time)
  end

  def self.between(start_date_time, end_date_time)
    self.includes(:event).joins(:event).where('events.start_date >= :first_date and events.start_date <= :next_date', first_date: start_date_time, next_date: end_date_time)
  end

  #JSON_OPTS = { :only => [:id, :contact_id], :include => { :event => Event::JSON_OPTS }}
  JSON_OPTS = {
    :except => [:updated_at, :created_at],
    :include => { :event => Event::JSON_OPTS },
    :methods => [:patient, :version]
  }
  def as_json(options={})
    super(JSON_OPTS.merge(options))
  end

  #-------------------------------------------------------------------------------
  # These 2 patient methods are a bit kludgy for a couple reasons
  #  (a) first was a design choice to make the association between an appointment
  #      and a contact, and the reason for this is to allow for more general 
  #      variations on the appointment system to potentially include other 
  #      types of appointment entities, or to extend the busines model to other
  #      domains.
  #  (b) the 2nd reason seems to be a bug in Rails 3 at least realted to how 
  #      its handling the :methods section of the JSON_OPTS which was generating
  #      and error of incorrect number of arguments (0 for 1 ) when calling as_json
  #      on the patient model, so instead I'm calling the as_json() directly here.
  #-------------------------------------------------------------------------------
  def patient
    self.contact.contactable.as_json({}) if self.contact.try(:contactable_type) == "Patient"
  end

  def patient=(a_patient)
    self.contact = a_patient.try(:contact)
  end

  def date
    self.event.start_date
  end

  #-----------------------------------------------------------------------------
  # define the appointment workflow rules
  #-----------------------------------------------------------------------------
  state_machine :initial => :new do
  end

  #-----------------------------------------------------------------------------
  # In order to make asynchronous client updates as efficient as possible, we
  # only want to transmit the data if there have been any appointment changes.
  # To keep track of this, each client (and the server) will have a calendar.version
  # that is incremented whenever any appointment is created, updated, or 
  # destroyed.
  #-----------------------------------------------------------------------------
  before_save :update_calendar_version
  before_destroy :update_calendar_version
  after_save :persist_calendar_version
  after_destroy :persist_calendar_version

  def update_calendar_version
    self.calendar.version = self.calendar.version + 1
  end

  def persist_calendar_version
    self.calendar.save
  end

  #-------------------------------------------------------------------------------
  # CSV loading interface 
  #-------------------------------------------------------------------------------
  def self.get_csv_instance(import_type, account, row)
    a = Appointment.new()
    a.build_event()
    return a
  end

  def account=(i_account)
    if i_account
      self.calendar = i_account.calendar
    end
  end

  def account
    self.calendar.account
  end

  @new_patient = false

  def csv_patient_id=(i_patient_alt_id)
    if i_patient_alt_id
      p = self.calendar.account.patients.find_by_alt_id(i_patient_alt_id)
      logger.debug("CSV :: patient #{i_patient_alt_id} ==> #{p.id}")
      self.contact = p.try(:contact)
    else
      logger.debug("CSV :: appointment for new patient")
      @new_patient = true
    end
  end

  def csv_patient_id
    self.contact.try(:contactable).try(:alt_id)
  end

  def csv_provider_id=(i_provider_id)
    self.provider = self.calendar.account.providers.find_by_alt_id(i_provider_id)
  end

  def csv_provider_id
    self.provider.try(:alt_id)
  end

  def csv_room_id=(i_room_id)
    self.room = self.calendar.account.rooms.find_by_alt_id(i_room_id)
  end

  def csv_room_id
    self.room.try(:alt_id)
  end

  #-----------------------------------------------------------------------------
  # For some reason when active record is loading the time it loads as 
  # Jan 1, 2000 rather than today. In contrast when you parse the time it
  # is interpreted to mean today.
  #-----------------------------------------------------------------------------
  def csv_end_time=(i_end_time)
    t1 = Time.parse(self.event.start_time.strftime('%I:%M:00 %p'))
    t2 = Time.parse(i_end_time)
    logger.debug("CSV :: model i_end_time #{i_end_time} from #{t1.strftime('%I:%M:00 %p')} == #{(t2 - t1)}")
    self.event.duration = ((t2 - t1) / 60).to_i
  end

  def csv_end_time
    te = self.event.start_time + (self.event.duration * 60)
    te.strftime('%I:%M:00 %p')
  end

  def csv_text_data=(i_text_data)
    logger.debug("CSV :: text data #{i_text_data}")
    @csv_text_data = i_text_data
  end

  def csv_text_data
    @csv_text_data
  end

  #-----------------------------------------------------------------------------
  # special cases this method is going to be designed to handle:
  #   - if patient id (alt_id) was blank, this is a new patient appointment
  #     setup the typical new patient appointment contact 
  #   - if patient id is null and text data = 'X' then this is a blocked slot
  #     + in this scenario, we're going to hijack the event associated with 
  #       this appointment and push it over to the associated Provider schedule
  #       with free = false.
  #     + this will result in an error when this appointment record is saved,
  #       effectively skipping the Appointment model for this record.
  #-----------------------------------------------------------------------------
  def csv_link_appointment(user, clinic, account, csv_row)
    if @new_patient
      if @csv_text_data == 'X'
        unless self.provider.nil?
          dups = provider.schedule.events.where("notes like :alt_id", :alt_id => "#{self.alt_id}%")
          if dups.count == 0
            logger.debug("CSV :: appointment import blocked slot")
            e = self.event
            self.event = nil
            e.free = false
            e.title = 'Blocked Slot'
            e.eventable = self.provider.schedule
            e.notes = "#{self.alt_id}, #{e.notes}"
            e.save!
          end
        end
      else
        names = @csv_text_data.split(' ')
        logger.debug("CSV :: text data for new patient --> #{clinic.inspect}")
        if names.length < 2
          p = Patient.newForAppointment(self.calendar.account, clinic, nil, names[0], nil)
        else
          p = Patient.newForAppointment(self.calendar.account, clinic, names[0], names[1], nil)
        end
        p.save!
        self.contact_id = p.contact.id
      end
    else
      logger.debug("CSV :: text data means nothing")
    end
  end

end
