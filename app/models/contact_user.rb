class ContactUser < Contact
  # BLS could maybe switch from the Address label to a STI model like phones.
  has_one :physical_address, :as => :addressable, :class_name => "Address", :conditions => { :label => 'Physical' }
  has_one :mobile_phone, :as => :phoneable, :class_name => "MobilePhone"

  accepts_nested_attributes_for :physical_address, :mobile_phone

  validates_associated_if_present :mobile_phone, :number
  validates_associated_if_present :physical_address

  delegate :one_liner, :to => :physical_address, :prefix => true, :allow_nil => true
  delegate :street, :street2, :city, :state, :zip, :to => :physical_address
  delegate :street=, :street2=, :city=, :state=, :zip=, :to => :physical_address
  # BLS - seems like either there might be a shortcut to delegating both the reader and writer attribute accessors
  #       or perhaps the reason this is not there is its not a great idea.
  delegate :phone, :to => :mobile_phone
  #-----------------------------------------------------------------------------
  # Class Methods
  #-----------------------------------------------------------------------------
  def self.build(attributes={})
    contact = self.new(attributes)
    contact.build(attributes)
    return contact
  end

  def build(attributes={})
    build_physical_address(attributes[:physical_address])
    build_mobile_phone(attributes[:mobile_phone])
  end

end
