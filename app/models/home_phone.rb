class HomePhone < Phone
  def self.new(attributes={}, options={})
    phone = super(attributes, options)
    phone.label = 'Home'
    return phone
  end
end
