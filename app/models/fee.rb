require_dependency 'csv_import'
class Fee < ActiveRecord::Base
  include CsvImport
  #-------------------------------------------------------------------------------
  # associations
  #-------------------------------------------------------------------------------
  belongs_to :procedure_code
  belongs_to :fee_schedule
  validates :fee_schedule_id, :uniqueness => { :scope => :procedure_code_id }
  accepts_nested_attributes_for :fee_schedule

  monetize :fee_cents
  monetize :copay_cents
  monetize :expected_insurance_payment_cents

  before_save :sync_copay
  
  #-------------------------------------------------------------------------------
  # class methods
  #   See the provider_legacy_id_label.new class method for an explanation of 
  #   this behavior to overcome a Rails bug.
  #
  # TODO : try removing this workaround after the rails 3.2 upgrade
  #-------------------------------------------------------------------------------
  def self.new(attributes={}, options={})
    fee = super()
    fsid_attr = attributes['fee_schedule_attributes']
    fsid = fsid_attr['id'] if fsid_attr
    fee.fee_schedule = FeeSchedule.find(fsid) if fsid
    fee.fee_schedule = FeeSchedule.new unless fee.fee_schedule
    fee.attributes = attributes
    return fee
  end

  #-------------------------------------------------------------------------------
  # CSV Import
  #-------------------------------------------------------------------------------
  def account
    @account
  end
  def account=(account)
    @account = account
  end
  def fee_schedule_alt_id
    self.fee_schedule.try(:alt_id)
  end
  def fee_schedule_alt_id=(i_alt_id)
    unless i_alt_id.nil?
      fs = FeeSchedule.where(:account_id => @account.id, :alt_id => i_alt_id).order('created_at DESC').first
      self.fee_schedule = fs
    end
  end
  def procedure_code_alt_id
    self.procedure_code.try(:alt_id)
  end
  def procedure_code_alt_id=(i_alt_id)
    unless i_alt_id.nil?
      pc = ProcedureCode.where(:account_id => @account.id, :alt_id => i_alt_id).order('created_at DESC').first
      self.procedure_code = pc
    end
  end
  def csv_is_dollar
    return self.is_percentage ? "False" : "True"
  end
  def csv_is_dollar=(is_dollar)
    self.is_percentage = is_dollar == "False" ? true : false
  end
  def csv_copay
    return self.is_percentage ? self.copay_pct * 100 : self.copay
  end
  def csv_copay=(i_copay)
    if self.is_percentage
      self.copay_pct = i_copay.to_f / 100
      self.copay = self.fee * self.copay_pct
    else
      self.copay = i_copay.to_f
      self.copay_pct = self.copay / self.fee unless self.fee == 0
    end
  end

  private 

  #-----------------------------------------------------------------------------
  # ensure the copay is kept in sync between copay_cents and copay_pct
  # one value is synced to the other depending on the value of is_percentage
  #-----------------------------------------------------------------------------
  def sync_copay
    if self.is_percentage
      self.copay = self.fee * self.copay_pct
    else
      self.copay_pct = self.copay / self.fee unless self.fee == 0
    end
  end
end
