require_dependency 'enum_support'

class ImportGroup < ActiveRecord::Base
  extend EnumSupport
  resourcify # add instance method roles
  belongs_to :account
  belongs_to :clinic
  belongs_to :user
  has_many :imports, :dependent => :destroy
  validates :import_group_type, :presence => true
  validates :account, :presence => true
  validates :user, :presence => true
  validate :has_required_files?, :on => :create
  enum_map :duplicate_code, APP_CONFIG['csv_import']['configuration']['duplicate_options']
  enum_map :error_code, APP_CONFIG['csv_import']['configuration']['error_options']
  accepts_nested_attributes_for :imports
  before_validation :propogate_ownership, :on => :create

  #-----------------------------------------------------------------------------
  # returns the total number of records for this import
  #-----------------------------------------------------------------------------
  def records_total
    self.imports.joins(:import_records).size
  end

  def records_imported
    self.imports.joins(:import_records).where("import_records.status = 1").size
  end
  #-----------------------------------------------------------------------------
  # Determines which import groups are available to the user
  #-----------------------------------------------------------------------------
  def self.get_available_import_groups(account,user)
    imports = ImportGroup.where(:account_id => account.id).select("distinct import_group_type").map { |ig| ig.import_group_type }
    group_list = APP_CONFIG['csv_import']['configuration']['groups'].map { |g| g["name"] }
    result = [ group_list[0] ] # always include the first group
    for i in 1..(group_list.size() - 1)
      if imports.include? group_list[i-1]
        result << group_list[i]
      else
        break
      end
    end
    return result
  end

  #-----------------------------------------------------------------------------
  # To avoid conflicts and unexpected results, each account is only allowed
  # 1 import at a time. This method will return the current import for this 
  # account which is being processed, or the most recently processed (which
  # allows the "Loaded" state to be depicted on the input screen).
  #-----------------------------------------------------------------------------
  def self.current_import(account_id)
    # first return the most recent import group which is not finished?
    igr = ImportGroup.not_finished(account_id)
    if igr.size > 0
      return igr.first
    else
      # if nothing's being processed, then return the most recent import
      return ImportGroup.where(:account_id => account_id).order('created_at DESC').first
    end
  end

  #-----------------------------------------------------------------------------
  # determine if the import is finished
  #-----------------------------------------------------------------------------
  def finished?
    ["Loaded", "Done", "Failed"].include?(self.status)
  end

  #-----------------------------------------------------------------------------
  # defines a scope to be used in determining the current import for this 
  # account.
  #-----------------------------------------------------------------------------
  def self.not_finished(account_id)
    ImportGroup.where('account_id = ? and status not in (?,?,?)',account_id,"Loaded","Done","Failed").
      order('created_at DESC')
  end

  #-----------------------------------------------------------------------------
  # return the import_group configuration 
  #-----------------------------------------------------------------------------
  def self.get_import_group_configuration(type)
    APP_CONFIG['csv_import']['configuration']['groups'].select { |g| g['name'] == type }[0]
  end

  #-----------------------------------------------------------------------------
  # indicates if there were errors in this import
  #-----------------------------------------------------------------------------
  def errors?
    self.imports.joins(:import_records).where("import_records.status != 1").size > 0
  end
  #-----------------------------------------------------------------------------
  # produce the report for displaying errors
  #-----------------------------------------------------------------------------
  def error_report
    table = Import.report_table( :all, :only => [:import_type],
      :include => { :import_records => { :only => [:record, :status] } },
      :conditions => [ "import_group_id = #{self.id} and import_records.status != 1" ])
    # table.rename_columns {|c| c.to_s.titleize }
    table.rename_columns({"import_type" => "Type",
                         "import_records.status" => "Status",
                         "import_records.record" => "Record" })
    table.replace_column("Status") { |s| APP_CONFIG['csv_import']['configuration']['record_status'][s.Status] }
    table.reorder("Status","Type","Record")
    return table
  end

  #-----------------------------------------------------------------------------
  # setup the resque job to process this data asynchronously
  #-----------------------------------------------------------------------------
  def create_job
    puts "created import job for #{self.id}"
    logger.info("created import job for #{self.id}")
    Resque.enqueue(Jobs::Import,self.id)
    # BLS : if you need to debug data jobs, easiest way is to 
    #       comment out the (3) lines above and call the job procedure directly below:
    #self.load_data_job
  end

  #-----------------------------------------------------------------------------
  # method which is called as part of the resque job to process this import 
  # group, it ensures that any exception rased by the job is caught and 
  # updates the job status.
  #-----------------------------------------------------------------------------
  def load_data_job
    self.load_data
  rescue Exception => e
    logger.error("[#{self.id}] import job raised exception: #{e.message}", e.backtrace)
    self.status = "Failed"
    self.save
    # now let the job go into the failed queue.
    raise e
  end

  #-----------------------------------------------------------------------------
  # if the user imports the same group multiple times, they are not required
  # to load the required files each time, although they could.
  #-----------------------------------------------------------------------------
  def is_reimport?
    ImportGroup.where("account_id = ? and import_group_type = ? and status like ?",
                      self.account_id, self.import_group_type, "Loaded%").size > 0
  end

  #-----------------------------------------------------------------------------
  # validator used to ensure this import has the required files as specified
  # in the configuration ( or that its a re-import ).
  #-----------------------------------------------------------------------------
  def has_required_files?
    result = true
    logger.info("checking import group #{self.id} for required files")
    config = ImportGroup.get_import_group_configuration(self.import_group_type)
    config['files'].each_with_index do |file, index|
      import = self.imports.select { |i| i.import_type == file['type'] }[0]
      if import.nil? or import.data.url == "NODATA"
        # if its required and this is the first type this group has been imported,
        # then we need to reject the 
        if file['required'] and !is_reimport?
          logger.debug("-- --> missing required data file #{file['type']}")
          self.errors.add(:imports,"missing required data #{file['type']}")
          result = false
        else
          import.destroy
        end
      end
    end
    logger.info("--> required files present: #{result}")
    return result
  end

  #-----------------------------------------------------------------------------
  # Primary method resposible for processing the CSV data sent from the client
  # for the import group
  #-----------------------------------------------------------------------------
  def load_data
    # the select returns an array, so we need to access the result at index 0
    config = ImportGroup.get_import_group_configuration(self.import_group_type)
    unless config.nil?
      logger.debug("loading configuration: #{self.import_group_type} with #{self.imports.size} imports")
      logger.debug("--> options error:#{self.error_code} duplicates:#{self.duplicate_code}")
      logger.debug("-->  using #{config['files'].to_s}")
      # we're only going to use a transaction to cover the entire process if we're
      # supposed to fail on error. using a global transaction will prevent the 
      # incremenetal status updates from being transmitted back to the client's browser (via AJAX).
      if self.error_code == 0 # fail on error
        ImportGroup.transaction do
          logger.debug("processing import group WITH global transaction")
          return _load_data(config)
        end
      else
        logger.debug("processing import group without global transaction")
        return _load_data(config)
      end
    else
      self.errors.add(:import_group_type,"Import group type is not valid")
      return false
    end
  end

  private

  #-----------------------------------------------------------------------------
  # Private method to load the data. This method does not operate within a 
  # transaction, so its up to the calling procedure to determine if a 
  # global transaction is required to keep data consistency.
  #-----------------------------------------------------------------------------
  def _load_data(config)
    self.status = "Processing" and  self.save
    file_count = config['files'].size
    config['files'].each_with_index do |file, index|
      logger.debug(" --> working on import #{file['type']}")
      self.status = "Processing #{index + 1} of #{self.imports.size}" and self.save
      import = self.imports.select { |i| i.import_type == file['type'] }[0]
      unless import.nil? or import.data.url == "NODATA"
        begin
          logger.debug("import data file #{import.data}")
          if import.load_data(user,clinic,account,self.error_code,self.duplicate_code)
            logger.debug(" -- --> #{file['type']} processed")
          else
            logger.error(" -- --> #{file['type']} failed with #{import.errors}")
            if import.errors.size > 10
              self.errors.add(:imports,"error processing #{file['type']} (first 10): #{import.errors[:import_records][0..9]}")
            else
              self.errors.add(:imports,"error processing #{file['type']}: #{import.errors}")
            end
            case self.error_code
            when 0 # fail import
              raise ActiveRecord::Rollback
            else # 1,2 continue with import processing
              logger.info("-- --> continuing with import processing")
            end
          end
          # Separate rescue options available under Ruby 1.9 with the changes to the CSV module
          # 
          # rescue CSV::MalformedCSVError => e
          #   self.errors.add(:imports,"CSV error processing #{file['type']}: #{e.message}")
          # rescue CsvDuplicateError => e
          #   self.errors.add(:imports, "error processing #{file['type']}: #{e.message}")
          rescue Exception => e
            self.errors.add(:imports, "error processing #{file['type']}: #{e.message}")
        end
      end
    end
    self.status = "Loaded" and self.save
    if self.errors.empty?
      return true
    else
      logger.error("import group errors #{self.errors}")
      return false
    end
  end

  #-----------------------------------------------------------------------------
  # pushes the ownership account/user/clinic from this import group into the
  # associated import resources. this is done before validation  
  #-----------------------------------------------------------------------------
  def propogate_ownership
    logger.debug("import_group propogate_ownership")
    self.imports.each do |i|
      i.account = self.account
      i.user = self.user
    end
  end
end
