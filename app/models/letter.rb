require_dependency 'enum_support'
#===============================================================================
# Holds the legacy letters from Handyworks CSV Import
#===============================================================================
class Letter < Message
  extend EnumSupport
  validates :label, :presence => true

end
