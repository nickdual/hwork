class Phone < ActiveRecord::Base
  include PhonesHelper
  belongs_to :phoneable, :polymorphic => true

  # allows for (###) ###-#### x ######
  validates :number, :presence => true,
    :length => { :minimum => 7, :maximum => 23 },
    :phone_format => true

  PHONE_LABELS = ['Home', 'Work', 'Mobile', 'Fax' ]

  validates :label, :presence => true, :inclusion => { :in => PHONE_LABELS }

  def empty?
    self.number.nil? || self.number.empty?
  end

  #-----------------------------------------------------------------------------
  # provides a convenience method through the model to return a formatted
  # version of the phone number using the PhonesHelper::phone_fmt method.
  #-----------------------------------------------------------------------------
  def phone
    formatted_phone_nbr = phone_fmt(self.number)
    return formatted_phone_nbr
  end

  #-----------------------------------------------------------------------------
  # this code is essentially overriding the attribute setter method for number
  # and will first strip out any formatting using phone_parse helper method.
  # the result will be separated into a country code and an unformatted number
  # removes any formatting from the number before its saved.
  # and moves the appropriate country code to country field
  #-----------------------------------------------------------------------------
  # OPTIMIZE : the Phone attribute setter method for number to remove formatting is not intuitive, perhaps a clearer way exists to do this.
  #            It was previously being done by a before_validation: callback; however, that was not being called early enough to be applied
  #            by the validate_associated_if_present processing, as the number fields was retrieved before the validation was initiated on
  #            the phone object.
  def number=(in_number)
    country, a_number = phone_parse(in_number)
    write_attribute(:number, a_number)
    self.country=country
  end

  private

end
