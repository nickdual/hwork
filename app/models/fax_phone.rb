class FaxPhone < Phone
  def self.new(attributes={}, options={})
    phone = super(attributes, options)
    phone.label = 'Fax'
    return phone
  end
end
