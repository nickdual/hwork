class CarrierHcfaFormOption < HcfaFormOption
  resourcify # add instance method roles
  validates :third_party, :presence => true
end
