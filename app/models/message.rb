require_dependency 'sti_helpers'
require_dependency 'csv_import'

class Message < ActiveRecord::Base
  resourcify # add instance method roles
  include CsvImport
  include StiHelpers

  belongs_to :messageable, :polymorphic => true
  
  MESSAGE_FORMATS = ['Letter (snail)', 'HTML for Email', 'SMS Text', 'Voice Message']

  def self.by_account(account)
    self.where(:messageable_type => 'Account', :messageable_id => account.id)
  end

  def self.ordered
    self.order('label ASC, created_at ASC')
  end

  def self.switcher_order(account,clinic)
    Message.by_account(account).ordered
  end
  alias_attribute :switcher_title, :label
end
