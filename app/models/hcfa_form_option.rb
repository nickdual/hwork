require_dependency 'csv_import'
require_dependency 'enum_support'
require_dependency 'sti_helpers'

class HcfaFormOption < ActiveRecord::Base
  resourcify # add instance method roles
  extend EnumSupport
  include StiHelpers
  include CsvImport
  belongs_to :account
  belongs_to :third_party
  validates :account, :presence => true

  # this is a placeholder to define the STATES enumeration in the base class w/o
  # using the enum_map which will also include the required validation
  # otherwise we'd have to reference this has as AccountHcfaFormOption::STATES
  STATES = State::ALL_STATES

  #----------------------------------------------------------------------------- 
  # Integer Options
  #----------------------------------------------------------------------------- 
  enum_map :form_type_code, APP_CONFIG['options']['hcfa']['form_types']
  enum_map :box9a, APP_CONFIG['options']['hcfa']['box9a']
  enum_map :box9c, APP_CONFIG['options']['hcfa']['box9c']
  enum_map :box9d, APP_CONFIG['options']['hcfa']['box9d']
  enum_map :box9_s_name_is_same, APP_CONFIG['options']['hcfa']['box9_s_name_is_same']
  enum_map :box11c, APP_CONFIG['options']['hcfa']['box11c']
  enum_map :box11d, APP_CONFIG['options']['hcfa']['box11d']
  enum_map :box24_diags, APP_CONFIG['options']['hcfa']['box24_diags']
  enum_map :box27, APP_CONFIG['options']['hcfa']['box27']
  enum_map :box32a, APP_CONFIG['options']['hcfa']['box32a']
  enum_map :box33b, APP_CONFIG['options']['hcfa']['box33b']
  enum_map :box1a, APP_CONFIG['options']['hcfa']['box1a']
  enum_map :box29, APP_CONFIG['options']['hcfa']['box29']

  #----------------------------------------------------------------------------- 
  # String Value Options
  #----------------------------------------------------------------------------- 
  # form_name
  enum :date_auth, APP_CONFIG['options']['hcfa']['date_auth']
  enum :date_format, APP_CONFIG['options']['hcfa']['date_format']
  enum :box17_upin, APP_CONFIG['options']['hcfa']['box17_upin']
  enum :box17_name, APP_CONFIG['options']['hcfa']['box17_name']
  enum :box17_npi, APP_CONFIG['options']['hcfa']['box17_npi']
  enum :box19, APP_CONFIG['options']['hcfa']['box19']
  enum :box24_tos, APP_CONFIG['options']['hcfa']['box24_tos']
  enum :box24_pos, APP_CONFIG['options']['hcfa']['box24_pos']
  enum :box24_modifier, APP_CONFIG['options']['hcfa']['box24_modifier']
  enum :box24_amount_format, APP_CONFIG['options']['hcfa']['box24_amount_format']
  enum :box24j_npi, APP_CONFIG['options']['hcfa']['box24j_npi']
  enum :box24j_legacy, APP_CONFIG['options']['hcfa']['box24j_legacy']
  enum :box27_30_format, APP_CONFIG['options']['hcfa']['box27_30_format']
  enum :license, APP_CONFIG['options']['hcfa']['license']
  enum :box32_name, APP_CONFIG['options']['hcfa']['box32_name']
  enum :box32b, APP_CONFIG['options']['hcfa']['box32b']
  enum :box33_name, APP_CONFIG['options']['hcfa']['box33_name']
  # no values defined: enum :box24i, APP_CONFIG['options']['hcfa']['box24i']
  # no values defined: enum :box25, APP_CONFIG['options']['hcfa']['box25']
  # no values defined: enum :box33, APP_CONFIG['options']['hcfa']['box33']
  # no values defined: enum :box33a, APP_CONFIG['options']['hcfa']['box33a']

  #----------------------------------------------------------------------------- 
  # Boolean Options --> Converted to Integers 0 = true, -1 = false
  #----------------------------------------------------------------------------- 
  enum_map :box5, APP_CONFIG['options']['hcfa']['box5']
  enum_map :plan_name, APP_CONFIG['options']['hcfa']['plan_name']
  enum_map :address, APP_CONFIG['options']['hcfa']['address']
  enum_map :box4_g_name_is_same, APP_CONFIG['options']['hcfa']['box4_g_name_is_same']
  enum_map :box4_7, APP_CONFIG['options']['hcfa']['box4_7']
  #no values defined: enum_map :box6, APP_CONFIG['options']['hcfa']['bob6']
  enum_map :box7_g_addr_is_same, APP_CONFIG['options']['hcfa']['box7_g_addr_is_same']
  enum_map :box7_g_addr_is_p_addr, APP_CONFIG['options']['hcfa']['box7_g_addr_is_p_addr']
  enum_map :box8, APP_CONFIG['options']['hcfa']['box8']
  enum_map :box9, APP_CONFIG['options']['hcfa']['box9']
  enum_map :box10, APP_CONFIG['options']['hcfa']['box10']
  enum_map :box11, APP_CONFIG['options']['hcfa']['box11']
  enum_map :box11a, APP_CONFIG['options']['hcfa']['box11a']
  enum_map :outside_lab, APP_CONFIG['options']['hcfa']['outside_lab']
  enum_map :print_diagnosis_description, APP_CONFIG['options']['hcfa']['print_diagnosis_description']
  enum_map :box21, APP_CONFIG['options']['hcfa']['box21']
  enum_map :date_to, APP_CONFIG['options']['hcfa']['date_to']
  enum_map :date_from, APP_CONFIG['options']['hcfa']['date_from']
  enum_map :box24_cpt_97014, APP_CONFIG['options']['hcfa']['box24_cpt_97014']
  #no values defined: enum_map :amount_paid, APP_CONFIG['options']['hcfa']['amount_paid']
  enum_map :total_owed, APP_CONFIG['options']['hcfa']['total_owed']
  #no values defined: enum_map :box31_provider_code, APP_CONFIG['options']['hcfa']['box31_provider_code']
  enum_map :clin_phone, APP_CONFIG['options']['hcfa']['clin_phone']
  enum_map :box32, APP_CONFIG['options']['hcfa']['box32']

  # booleans
  default_value_for :box5, 0
  default_value_for :plan_name, 1
  default_value_for :address, 1
  default_value_for :box4_g_name_is_same, 0
  default_value_for :box4_7, 0
  default_value_for :box7_g_addr_is_same, 0
  default_value_for :box7_g_addr_is_p_addr, 0
  default_value_for :box8, 0
  default_value_for :box9, 0
  default_value_for :box10, 0
  default_value_for :box11, 0
  default_value_for :box11a, 0
  default_value_for :outside_lab, 0
  default_value_for :print_diagnosis_description, 0
  default_value_for :box21, 0
  default_value_for :date_to, 0
  default_value_for :date_from, 0
  default_value_for :box24_cpt_97014, 0
  default_value_for :total_owed, 0
  default_value_for :clin_phone, 0
  default_value_for :box32, 0
  # integers
  default_value_for :form_type_code, 0
  default_value_for :box9a, 0
  default_value_for :box9c, 0
  default_value_for :box9d, 0
  default_value_for :box9_s_name_is_same, 0
  default_value_for :box11c, 0
  default_value_for :box11d, 0
  default_value_for :box24_diags, 0
  default_value_for :box27, 0
  default_value_for :box32a, 0
  default_value_for :box33b, 0
  default_value_for :box1a, 0
  # strings
  default_value_for :date_auth, APP_CONFIG['options']['hcfa']['date_auth'].first
  default_value_for :date_format, APP_CONFIG['options']['hcfa']['date_format'].first
  default_value_for :box17_upin, APP_CONFIG['options']['hcfa']['box17_upin'].first
  default_value_for :box17_name, APP_CONFIG['options']['hcfa']['box17_name'].first
  default_value_for :box17_npi, APP_CONFIG['options']['hcfa']['box17_npi'].first
  default_value_for :box19, APP_CONFIG['options']['hcfa']['box19'].first
  default_value_for :box24_tos, APP_CONFIG['options']['hcfa']['box24_tos'].first
  default_value_for :box24_pos, APP_CONFIG['options']['hcfa']['box24_pos'].first
  default_value_for :box24_modifier, APP_CONFIG['options']['hcfa']['box24_modifier'].first
  default_value_for :box24_amount_format, APP_CONFIG['options']['hcfa']['box24_amount_format'].first
  default_value_for :box24j_npi, APP_CONFIG['options']['hcfa']['box24j_npi'].first
  default_value_for :box24j_legacy, APP_CONFIG['options']['hcfa']['box24j_legacy'].first
  default_value_for :box27_30_format, APP_CONFIG['options']['hcfa']['box27_30_format'].first
  default_value_for :box29, APP_CONFIG['options']['hcfa']['box29'].first
  default_value_for :license, APP_CONFIG['options']['hcfa']['license'].first
  default_value_for :box32_name, APP_CONFIG['options']['hcfa']['box32_name'].first
  default_value_for :box32b, APP_CONFIG['options']['hcfa']['box32b'].first
  default_value_for :box33_name, APP_CONFIG['options']['hcfa']['box33_name'].first

  #-------------------------------------------------------------------------------
  # scopes
  #-------------------------------------------------------------------------------
  scope :alphabetically, :order => 'name ASC'

  def self.switcher_order(account,clinic)
    by_account(account.id).order('third_party_id, state, form_type_code')
  end

  def self.by_account(account_id)
    where("account_id = ?",account_id)
  end

  #-------------------------------------------------------------------------------
  # instance methods
  #-------------------------------------------------------------------------------
  def title
    if self.third_party then
      "#{self.third_party.name} - #{self.form_type_code_key}"
    else
      "#{HcfaFormOption::STATES.invert[self.state]} - #{self.form_type_code_key}" 
    end
  end

  def switcher_title
    st = self.title
    st.length > 25 ? st[0..24] + "..." : st
  end

  #-------------------------------------------------------------------------------
  # Class methods
  #-------------------------------------------------------------------------------

  #-------------------------------------------------------------------------------
  # CSV Import
  #-------------------------------------------------------------------------------
  def third_party_alt_id
    return self.third_party.try(:alt_id)
  end

  def third_party_alt_id=(new_id)
    # TODO : add logic to validate the resulting third party is accessible to the current user.
    unless new_id.nil?
      tp = ThirdParty.where(:account_id => self.account_id, :alt_id => new_id).order('created_at DESC').first
      self.third_party = tp
    end
  end

  BOOLEAN_COLUMNS = [
    'box5',
    'plan_name',
    'address',
    'box4_gname_is_same',
    'box4_7',
    'box6',
    'box7_gaddr_is_same',
    'box7_gaddr_is_paddr',
    'box8',
    'box9',
    'box10',
    'box11',
    'box11a',
    'outside_lab',
    'prt_dx_desc',
    'box21',
    'date_to',
    'date_from',
    'box24_cpt_97014',
    'amount_paid',
    'total_owed',
    'box31_provider_code',
    'clin_phone',
    'box32']

  def self.csv_converters
    r = super()
    # mapping for states being imported from csv for Carrier specific HCFA forms.
    r << lambda { |f, info| info.header.underscore.strip == 'state' ? (f.start_with?('Z') ? "NY" : f ) : f }
    r << lambda { |f, info| info.header.underscore.strip == 'license' ? (f == "LIC" ? "LIC: License" : f ) : f }
    r << lambda { |f, info| info.header.underscore.strip == 'box17_upin' ? (f == "TAXID" ? "TaxID" : f ) : f }
    r << lambda { |f, info| info.header.underscore.strip == 'box24_amount_format' ? (f == "With Blank" ? "WithBlank" : f ) : f }
    r << lambda { |f, info| BOOLEAN_COLUMNS.include?(info.header.underscore.strip) ? convert_bool(info.header.underscore,f) : f }
    return r
  end

  def self.get_csv_instance(import_type, account, row)
    # TODO need to add support / logic in the HCFA Form CSV import for Carrier HCFA Forms
    if row.field('InsID').blank?
      AccountHcfaFormOption.new
    else
      CarrierHcfaFormOption.new
    end
  end
  
end
