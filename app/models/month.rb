class Month
  MONTHS = {
    'January' => '01',
    'February' => '02',
    'March' => '03',
    'April' => '04',
    'May' => '05',
    'June' => '06',
    'July' => '07',
    'August' => '08',
    'September' => '09',
    'October' => '10',
    'November' => '11',
    'December' => '12' }

  def self.names
    MONTHS.keys
  end

  def self.values
    MONTHS.values
  end

  def self.to_number(name)
    MONTHS[name]
  end

  def self.to_name(abbrev)
    MONTHS.invert[abbrev]
  end
end
