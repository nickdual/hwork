class User < ActiveRecord::Base
  resourcify # add instance method roles
  rolify:role_cname => 'Role'
  has_and_belongs_to_many :roles, :join_table => "users_roles", :before_remove => :can_revoke?
  has_one :contact, :as => :contactable, :dependent => :destroy, :class_name => 'ContactUser'

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  # :validatable
  devise :database_authenticatable, :async, :registerable,
         :recoverable, :rememberable, :trackable,
         :confirmable, :lockable, :timeoutable,
         :authentication_keys => [:email] 
  # TODO : the devise-async gem should be handling the async emails and I can see they get queued, but not delivered.

  # Setup accessible (or protected) attributes for your model
  belongs_to :clinic
  belongs_to :account

  accepts_nested_attributes_for :contact, :account, :roles
  attr_accessible :email, :password, :password_confirmation, :remember_me, :clinic_id, :login, 
    :account_role, :contact_attributes, :account_attributes, :roles_attributes, :confirmation_token

  alias_attribute :switcher_title, :email

  delegate :new_account?, :subscription_plan_name, :to => :account

  #before_create :add_contact

  JSON_OPTS = { :only => [ :id, :email, :confirmed_at ] }
  #-------------------------------------------------------------------------------
  # apparently Devise is affecting the JSON generation of this class base possibly
  # upon the attr_accessible and the use of the include_root_in_json property ... a little
  # bit stranges and unpredictable behaviour occured in the resque queue while I was 
  # installing and configuring Backbone.js
  #   - commenting out the attr_accessible, then to_json behaves like I would
  #     expect it too, i think commenting out the devise piece would work as well.
  # 
  # the check for encoding_for_resque? is going to add in a root element when
  # the call is being made from Resque.encode()
  #-------------------------------------------------------------------------------
  def as_json(options={})
    if encoding_for_resque?
      # when encoding for resque, options includes an :only setting which overrides
      # our desired only fields, specifically it does not include the :id which causes
      # resque job to fail with ActiveRecord not found type errors.
      r = super(JSON_OPTS)
      r = { self.class.model_name.element => r }
    else
      r = super(JSON_OPTS.merge(options))
    end
    return r
  end

  #-------------------------------------------------------------------------------
  # validations
  #   We're writing the validations here by hand rather than relying on the 
  #   Devise validatable module. The main reason for this approach is to allow
  #   the email only signup process.
  #
  # BLS: This is in lieu of the Devise:validatable option, there may
  #      be a way to only adjust the email validation, but none was apparent
  #      so the approach instead is to not use that module.
  #        TODO at a minimum if it can't be configured to just affect the desired 
  #             validation, then we could reference the configuration values from config/initializers/devise.rb
  #
  #-------------------------------------------------------------------------------
  before_validation :populate_login
  validates :contact, :presence => true, :if => :confirmed?
  validates :email,
    :presence => true,
    :uniqueness => { :case_sensitive => false },
    :format => { :with => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i }
  validates :login,
    :presence => true,
    :uniqueness => { :case_sensitive => false },
    :if => :confirmed?
  validates :password,
    :confirmation => true,
    :length => { :minimum => 6, :maximum => 128 },
    :if => :validate_password?

  #-----------------------------------------------------------------------------
  # Scopes
  #-----------------------------------------------------------------------------
  #default_scope lambda { |acc_id| where( 'account_id = ? ', acc_id) }
 
  delegate :first_name, :last_name, :name, :to => :contact
  delegate :physical_address_one_liner, :to => :contact, :allow_nil => true
  delegate :state, :to => :contact

  scope :by_account, lambda { |acc_id| where( 'account_id = ?', acc_id ) }

  # TODO - this should be ordered based on contact.last_name once available.
  def self.switcher_order(account,clinic)
    User.by_account(account).order(:email)
  end

  #-------------------------------------------------------------------------------
  # class methods
  #-------------------------------------------------------------------------------
  def self.build(configuration={})
    p = self.new(configuration)
    p.contact = ContactUser.build(:contactable => p)
    return p
  end

  #-----------------------------------------------------------------------------
  # returns the list of users that have a given role.
  #   WARNING : this is only valid for global roles
  #-----------------------------------------------------------------------------
  def self.with_role(rolename)
    r = rolename.kind_of?(Role) ? rolename : Role.find_by_name(rolename)
    unless r.nil?
      joins(:roles).where("users_roles.role_id = ?",r.id)
    else
      self
    end
  end

  #-----------------------------------------------------------------------------
  # since we're not using the login fields, we'll normally want to display a
  # shorter version of the email to identify the user on screen.
  #-----------------------------------------------------------------------------
  def display_name
    self.email.split('@')[0]
  end

  def expire
    UserMailer.expire_email(self).deliver
    destroy
  end
  
  #-----------------------------------------------------------------------------
  # Role Names are stored in the database in CamelCase, but will
  # be referenced from Ruby using the underscore naming convention
  #-----------------------------------------------------------------------------
  def role?(role)
    return !!self.roles.find_by_name(role.name)
  end

  def admin?
    self.has_role? Role::ACCOUNT_ADMIN
  end

  def app_admin?
    self.has_role? Role::APPLICATION_ADMIN
  end

  def default_clinic
    return self.clinic
  end

  #-----------------------------------------------------------------------------
  # provides a singular attribute interface to the account role type for this 
  # user. currently there are only 2 possible values for the account role, either
  # admin or non-admin, which we'll call STAFF. In the future it's easier to add
  # additional roles of pre-defined types if we structure it through a single
  # valued attribute vs. through either a single admin attribute or through 2
  # independed admin, staff attributes.
  #-----------------------------------------------------------------------------
  def account_role
    if self.admin?
      return Role::ACCOUNT_ADMIN
    end
    return Role::ACCOUNT_STAFF
  end

  def account_role=(role_name)
    if role_name == Role::ACCOUNT_ADMIN 
      self.add_role Role::ACCOUNT_ADMIN
    else
      self.remove_role Role::ACCOUNT_ADMIN
    end
  end

  #-----------------------------------------------------------------------------
  # certain fragments are going to be cached based on the role of the user 
  # which will be viewing them, this method returns an indicator of what this
  # user's cache role should be represented.
  #-----------------------------------------------------------------------------
  def cache_role
    return "aa" if self.app_admin?
    return "a" if self.admin?
    return "s"
  end


  def can_revoke?(role)
    role.can_revoke?(self)
  end

  #-----------------------------------------------------------------------------
  # Require a password only after the user is confirmed.
  #-----------------------------------------------------------------------------
  def password_required?
    super if confirmed?
  end

  #-----------------------------------------------------------------------------
  # Perform password validations only if the user has been confirmed and 
  # is trying to change or set their password.
  #-----------------------------------------------------------------------------
  def validate_password?
    if self.confirmed? 
      if password or password_confirmation 
        return true
      end
    end
  end

  def password_match?
    self.errors[:password] << "can't be blank" if password.blank?
    self.errors[:password_confirmation] << "can't be blank" if password_confirmation.blank?
    self.errors[:password_confirmation] << "does not match password" if password != password_confirmation
    password == password_confirmation && !password.blank?
  end

  ##def add_contact
  ##  self.contact = ContactUser.build(:contactable => self) unless self.contact
  ##end

  #-----------------------------------------------------------------------------
  # right now we're just filling in a dummy login value the user does not 
  # need to supply a login, we're just using email. when the user is created
  # by admin, they won't need to confirm their email. Since confirmation is 
  # the step when we'd collect a login, then for those users we'll need to 
  # set the login here, as a before validate callback.
  #-----------------------------------------------------------------------------
  def populate_login
    if self.confirmed? and self.login.nil?
      self.login = self.email
    end
  end

  protected

  def self.cuser
    @current_user
  end

end
