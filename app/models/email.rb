class Email < ActiveRecord::Base
  belongs_to :emailable, :polymorphic => true

  # XXX - if necessary could use validates_email_format_of for
  # total RFC compliant email validations.
  # https://github.com/alexdunae/validates_email_format_of

  email_regex = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  # alternate format regex:  /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, :presence => true,
    :format => { :with => email_regex, :message => 'should be a valid email address' }


end
