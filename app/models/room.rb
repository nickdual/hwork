require_dependency 'csv_import'

class Room < ActiveRecord::Base
  resourcify # add instance method roles
  include CsvImport
  after_create :update_position
  #-------------------------------------------------------------------------------
  # Constants
  #-------------------------------------------------------------------------------
  #-------------------------------------------------------------------------------
  # Behaviours
  #-------------------------------------------------------------------------------
  # acts_as_paranoid
  #-------------------------------------------------------------------------------
  # associations
  #-------------------------------------------------------------------------------
  belongs_to :account
  has_many :appointments
  has_many :room_locations
  has_many :clinics, :through => :room_locations
  has_many :room_appointment_types, :dependent => :destroy
  has_many :appointment_types, :through => :room_appointment_types
  belongs_to :appointment_type
  has_many :provider_precedences, :order => "`order` ASC"
  has_many :providers, :through => :provider_precedences
  accepts_nested_attributes_for :provider_precedences, :allow_destroy => true

  #-------------------------------------------------------------------------------
  # validations
  #-------------------------------------------------------------------------------
  validates :name, :presence => true, :uniqueness => { :case_sensitive => false, :scope => :account_id }
  validates :account, :presence => true
  validate :room_in_single_location

  #-------------------------------------------------------------------------------
  # If a room does not have a appointment type assigned to it we'll still want to setup
  # an appointment type for this room in the appointments UI. So we'll default the 
  # appointment_type_id to be the first appointment_type on the account.
  # This will work well for accounts that only have a single appointment_type.
  # TODO : remove the room appointment_type selection box for accounts with a single appointment_type.
  #-------------------------------------------------------------------------------
  def appointment_type_id
    room_appointment_type = read_attribute(:appointment_type_id)
    if room_appointment_type.nil? and not self.account.appointment_types.empty?
      room_appointment_type = self.account.appointment_types.first.id
    end
    return room_appointment_type
  end

  #-----------------------------------------
  # Ensure there are not conflicting clinic
  #  locations, a room can be in only one
  #  physical location.
  #-----------------------------------------
  def room_in_single_location
    error = false
    if self.clinics.size > 1 
      first_clinic = self.clinics.first
      for index in 1...self.clinics.size do
        if first_clinic.different_location?(self.clinics[index])
          error = true
          break
        end
      end
    end
    errors.add(:clinics,"can't be assigned to multiple physical locations") if error
  end

  #-------------------------------------------------------------------------------
  # indicate the clinics with which this room should be associated
  #  this can either report back the explicit list, or all clinics if there's
  #  only one single location, otherwise empty.
  #-------------------------------------------------------------------------------
  def for_clinics
    if self.clinics and not self.clinics.empty?
      return self.clinics.map { |c| c.id } 
    end
    unless self.account.has_multiple_locations?
      return self.account.clinics.map { |c| c.id }
    end
    # here the account has multiple clinic locations, but this particular room is
    # not configured completely to be in one location or another.
    return [ self.account.clinics.try(:first).try(:id) ]
  end

  #-------------------------------------------------------------------------------
  # enabled
  #  - used to indicate if this room can be scheduled for appointments
  #  - for now this is always going to return true from the server side, could
  #    eventually be based on either user level access control or preferences
  #-------------------------------------------------------------------------------
  def enabled
    return true
  end

  #-------------------------------------------------------------------------------
  # Returns a sequence of providers active for this room used for appointment
  # scheduling purposes. This sequence can be changed through the Rooms edit 
  # view.
  #-------------------------------------------------------------------------------
  def providers_seq
    self.provider_precedences.map { |p|
      { :order => p.order,
        :provider_id => p.provider_id
      }
    }
  end

  #-------------------------------------------------------------------------------
  # scopes
  #-------------------------------------------------------------------------------
  def self.ordered
    order(:position)
  end
  def self.by_account(account_id)
    where('account_id = ?', account_id)
  end

  def self.switcher_order(account,clinic)
    by_account(account).ordered
  end
  #-------------------------------------------------------------------------------
  # aliased methods
  #-------------------------------------------------------------------------------
  alias_attribute :switcher_title, :name

  #-------------------------------------------------------------------------------
  # This method is called from the RoomsController.update process, specifically
  # when updates are done from the form where room.position might have changed.
  # If the room.position is changed then we need to reshuffle the position on
  # the other rooms to ensure the list is unique.
  #
  # To prevent a really frustrating scenario when the account is being setup
  # and there are mutliple Rooms with position = 0, we will only slide the other
  # rooms if (a) it doesn't make their position negative, (b) there is currently 
  # a room in the position targeted for the new room.
  #-------------------------------------------------------------------------------
  def self.reorder(move_from,room,room_list)
    if room.position != move_from
      # check to see if a slide of other rooms is even necessary, this would not
      # be necessary when initially setting these values for the first time with
      # multiple imported rooms.
      slide_ck = false
      room_list.each do |r|
        if r.id != room.id and r.position == room.position
          slide_ck = true
        end
      end
      if slide_ck
        Room.transaction do
          sliders = move_from < room.position ? Array((move_from + 1)..room.position) : Array(room.position...move_from)
          slide_direction = move_from < room.position ? -1 : 1
          room_list.each do |r|
            to_save = false
            if r.id != room.id and sliders.include?(r.position)
              r.position = r.position + slide_direction
              to_save = true
            end
            # clean up any incidental negative order values.
            if r.position < 0
              r.position = 0
              to_save = true
            end
            r.save if to_save
          end
        end
      end
    end
  end
  private
   def update_position
     @rom = Room.select('position').by_account(self.account_id).ordered.last
     if @rom
       self.position = @rom.position.to_i + 1
     else
       self.position = 0
     end
     self.save
   end
  end
