require_dependency 'csv_import'

class Provider < ActiveRecord::Base
  include CsvImport
  resourcify # add instance method roles
  #-------------------------------------------------------------------------------
  # Constants
  #-------------------------------------------------------------------------------
  #BLS include ActionController::UrlWriter
  #BLS CONTACT_REQUIRED_FIELDS = [ ]
  #-------------------------------------------------------------------------------
  # Behaviours
  #-------------------------------------------------------------------------------
  #-------------------------------------------------------------------------------
  # associations  
  #-------------------------------------------------------------------------------
  has_one :contact, :as => :contactable, :class_name => 'ContactProvider'
  has_one :schedule, :as => :schedulable
  has_many :provider_legacy_id_labels, :dependent => :destroy, :class_name => 'ProviderLegacyIdLabel', :include => :legacy_id_label, :order => 'legacy_id_labels.label ASC'
  has_many :legacy_id_labels, :through => :provider_legacy_id_labels, :order => "label ASC"
  has_many :appointments
  has_many :provider_appointment_types, :dependent => :destroy
  has_many :appointment_types, :through => :provider_appointment_types
  has_many :cases
  has_many :patients, :through => :cases 
  has_many :visit_details
  #has_many :patient_bills
  #has_many :patient_visit_details
  belongs_to :account
  has_many :provider_precedences
  has_many :rooms, :through => :provider_precedences
  before_destroy :check_resource_reference


  #-------------------------------------------------------------------------------
  # validations
  #-------------------------------------------------------------------------------
  validates :your_code, :presence => true, :uniqueness => { :case_sensitive => false, :scope => :account_id }
  validates :account, :presence => true
  validates :signature_name, :presence => true, :uniqueness => { :case_sensitive => false, :scope => :account_id }
  # BLS - should this format be applied to clinic.tax id?
  # BLS Regexp.new(APP_CONFIG['options']['validation']['tax_or_ss']['format']), <-- did not work
  validates_format_of :tax_uid, 
    :with => /^(\d{3}[-]?\d{2}[-]?\d{4})|(\d{2}-\d{7})$/i,
    :message => APP_CONFIG['options']['validation']['tax_or_ss']['message'],
    :allow_blank => true
  #validate :contact_first_name_blank, :contact_last_name_blank, :contact_sex_blank
  validates_length_of :npi_uid, :maximum => 10, :allow_blank => true

  accepts_nested_attributes_for :provider_legacy_id_labels, :allow_destroy => true
  accepts_nested_attributes_for :contact
  after_create :initialize_schedule
  before_destroy :check_cases

  #-------------------------------------------------------------------------------
  # class methods
  #-------------------------------------------------------------------------------
  def self.build(configuration={})
    p = self.new(configuration)
    p.contact = ContactProvider.build(:contactable => p)
    #p.preload_existing_legacy_ids
    #p.provider_legacy_id_labels.build.build_legacy_id_label
    return p
  end

  #BLS SLF: This is only used by its test. There must be a better way...
  #BLSdef self.find_by_name(name)
  #BLS  names = name.split(' ')
  #BLS  find(:first, 
  #BLS       :joins => :contact, 
  #BLS       :conditions => { :contacts => { :first_name => names.first, :last_name => names.second}})
  #BLSend
  #-----------------------------------------------------------------------------
  # this method is going to push all the legacy id labels which have been 
  # associated with the account to this provider. its applied anytime a provider
  # is created or updated.
  #-----------------------------------------------------------------------------
  def preload_existing_legacy_id_labels(persist = true)
    self.account.legacy_id_labels.each do |legacy_id_label|
      unless self.provider_legacy_id_labels.exists?(:legacy_id_label_id => legacy_id_label.id)
        if new_record? or not persist
          self.provider_legacy_id_labels.build(:legacy_id_label => legacy_id_label)
        else
          self.provider_legacy_id_labels.create(:legacy_id_label => legacy_id_label)
        end
      end
    end
  end


  #-------------------------------------------------------------------------------
  # scopes
  #-------------------------------------------------------------------------------
  #scope :alphabetically, :order => 'contacts.last_name ASC, contacts.first_name ASC', :joins => :contact
  #BLS - causes ActiveRecord::ReadOnlyError during ProvidersController#update
  #default_scope :joins => :contact, :order => 'contacts.last_name ASC, contacts.first_name ASC'
  scope :with_signature_name_starting_with, lambda { |l|
    {
      :conditions => ['lower(substr(signature_name, 1, 1)) = ?', l.downcase],
      :order => 'signature_name ASC'
    }
  }
  scope :alphabetically, :order => 'contacts.last_name ASC', :include => :contact
  scope :by_account, lambda { |acc_id| where('account_id = ?',acc_id) }

  def self.switcher_order(account,clinic)
    Provider.by_account(account)
  end

  #-----------------------------------------------------------------------------
  # delegations
  #-----------------------------------------------------------------------------
  delegate :full_name, :line1, :line2, :to => :contact

  #-------------------------------------------------------------------------------
  # instance methods
  #-------------------------------------------------------------------------------
  # BLS - could possibly delegate this to contact, depends on how its used
  def name
    return contact.full_name unless contact.full_name == " "
    return signature_name
  end

  def path_for_dropdown
    edit_provider_path(self.clinic, self)
  end

  # BLS - move this method down to contact 
  def address_stamp
    address_stamp = []
    address_stamp << signature_name
    address_stamp << contact.line1
    address_stamp << contact.line2
    address_stamp << contact.phone
    address_stamp.compact.reject(&:blank?).join("\n")
  end

  def can_delete?
    #patient_cases.empty? && patient_visit_details.empty? && patient_bills.empty? &&
    patients.empty? and cases.empty? and appointments.empty?
  end

  #def preload_existing_legacy_ids
  #  LegacyIdLabel.all.each do |legacy_label|
  #    unless provider_legacy_id_labels.exists?(:legacy_id_label_id => legacy_label.id)
  #      if new_record?
  #        provider_legacy_id_labels.build :legacy_id_label => legacy_label
  #      else
  #        provider_legacy_id_labels.create :legacy_id_label => legacy_label
  #      end
  #    end
  #  end
  #end

  #-------------------------------------------------------------------------------
  # enabled
  #  - used to indicate if this provider can be scheduled for appointments
  #  - for now this is always going to return true from the server side, could
  #    eventually be based on either user level access control or preferences
  #-------------------------------------------------------------------------------
  def enabled
    return true
  end

  #-------------------------------------------------------------------------------
  # private methods
  #-------------------------------------------------------------------------------
  private
    def check_cases
      if self.cases.count > 0
  #    TODO identify the patient
  #    errors.add_to_base "This third_party is currently being used by patient #{self.cases.account}"
      errors[:base] << "This provider is currently being used by patient cases"    
      return false
    end
  end
  

  #-------------------------------------------------------------------------------
  # CSV Import
  #-------------------------------------------------------------------------------
  def self.get_csv_instance(import_type,account,row)
    p = Provider.build()
    # build some legacy id
    l1 = LegacyIdLabel.where(:label => 'state Lic #', :account_id => account.id).first
    l1 = LegacyIdLabel.new(:label => 'state Lic #', :account_id => account.id) if l1.nil?
    p.provider_legacy_id_labels << ProviderLegacyIdLabel.new(:legacy_id_label => l1)
    return p
  end
  #C;Chiropractor;T;PhysicalTherapist;M;Medical Doctor;P;Podiatrist;H;Mth Therapist;A;Acupuncturist;S;Clinical Social Worker
  # TODO : convert the Provider CSV converter for TypeOfDoc into a customer attribute csv_type_of_doc
  #        the advantage of this approach is that it will preserve the CSV record properly.
  def self.csv_converters
    r = super()
    r << lambda { |f,info| 
      if info.header == "TypeOfDoc"
        case f
        when 'C'
          return 'Chiropractor'
        when 'T'
          return 'Physical Therapist'
        when 'M'
          return 'Medical Doctor'
        when 'P'
          return 'Podiatrist'
        when 'H'
          return 'Massage Therapist'
        when 'A'
          return 'Acupuncturist'
        when 'S'
          return 'Clinical Social Worker'
        else
          return 'Other'
        end
      else
        return f
      end
    }
  end

  #-------------------------------------------------------------------------------
  # links the default providers supplied during CSV import into the clinic
  # structures after the providers file has been loaded.
  #-------------------------------------------------------------------------------
  def self.csv_link_default_providers(user, i_clinic, account, error_code=0, duplicate_code=0)
    Clinic.where(:account_id => account.id).each do |clinic|
      if clinic.new_patient_option.default_provider.nil? and not clinic.new_patient_option.default_provider_alt_id.nil?
        p = Provider.where(:account_id => account.id, :alt_id => clinic.new_patient_option.default_provider_alt_id).order('created_at DESC').first
        logger.info("csv_link_default_providers #{p.inspect} for #{clinic.inspect}")
        clinic.new_patient_option.default_provider_id = p.id
        logger.debug("--> done #{clinic.new_patient_option.inspect}")
        logger.info("--> done #{clinic.new_patient_option.default_provider.inspect}")
        clinic.save
        # without doing the save at the end of this block, these changes are lost.
      end
    end
  end

  #-------------------------------------------------------------------------------
  # private methods
  #-------------------------------------------------------------------------------

  def initialize_schedule
    self.schedule = self.create_schedule({}) if self.schedule.nil?
  end

  #-------------------------------------------------------------------------------
  # aliased methods
  #-------------------------------------------------------------------------------
  #BLS alias :name  :full_name
  alias_attribute :switcher_title, :signature_name
  alias :title :full_name
  alias :search_title :name

  protected

  #-------------------------------------------------------------------------------
  # Active Record before_destroy callback. 
  #-------------------------------------------------------------------------------
  def check_resource_reference
    unless self.can_delete?
      raise Errors::ResourceReferencedError.new(I18n.translate("flash.error.resource.reference"))
    end
  end

end

