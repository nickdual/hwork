class ProviderAppointmentType < ActiveRecord::Base
  belongs_to :provider
  belongs_to :appointment_type
  validates :provider, :presence => true
  validates :appointment_type, :presence => true
end
