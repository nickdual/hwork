class AppointmentTypeObserver < ActiveRecord::Observer
  def after_create(model)
    if model.account.providers.length == 1 then
      model.providers << model.account.providers.first
    end
    if model.account.rooms.length == 1 then
      model.rooms << model.account.rooms.first
    end
    model.save!
  rescue NoMethodError
  end
end
