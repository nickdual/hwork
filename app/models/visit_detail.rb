class VisitDetail < ActiveRecord::Base
  belongs_to :visit
  belongs_to :procedure_code
  belongs_to :provider
  validates :visit, :provider, :presence => true
end
