class ContactProvider < Contact
  has_one :physical_address, :as => :addressable, :class_name => "Address", :conditions => { :label => 'Physical' }
  has_one :business_email, :as => :emailable, :class_name => "Email", :conditions => { :label => 'Business' }
  has_one :business_phone, :as => :phoneable, :class_name => "Phone", :conditions => { :label => 'Work' }
  has_one :mobile_phone, :as => :phoneable, :class_name => "Phone", :conditions => { :label => 'Mobile' }

  accepts_nested_attributes_for :physical_address, :business_email, :business_phone, :mobile_phone

  validates_associated_if_present :business_email, :email
  validates_associated_if_present :business_phone, :number
  validates_associated_if_present :mobile_phone, :number
  validates_associated_if_present :physical_address

  delegate :one_liner, :to => :physical_address, :prefix => true
  delegate :street, :street2, :city, :state, :zip, :to => :physical_address
  delegate :street=, :street2=, :city=, :state=, :zip=, :to => :physical_address
  # BLS - seems like either there might be a shortcut to delegating both the reader and writer attribute accessors
  #       or perhaps the reason this is not there is its not a great idea.
  delegate :phone, :to => :business_phone
  delegate :email, :to => :business_email
  delegate :line1, :line2, :to => :physical_address

  #-----------------------------------------------------------------------------
  # defines the contact structure for a Clinic
  #  it must have a Business Phone, Fax Phone
  #  it must have a Business Address
  #-----------------------------------------------------------------------------
  def self.build(attributes={})
    contact = ContactProvider.new(attributes)
    contact.build(attributes)
    return contact
  end

  def build(attributes={})
    build_physical_address(attributes[:physical_address])
    build_business_phone(attributes[:business_phone])
    build_mobile_phone(attributes[:mobile_phone])
    #build_fax_phone(attributes[:fax_phone]) if attributes[:fax_phone]
    build_business_email(attributes[:business_email]) if attributes[:business_email]
  end
end
