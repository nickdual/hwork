require_dependency 'csv_import'
require_dependency 'enum_support'

# BLS - imported many methods which need to be reviewed for currency
class ProcedureCode < ActiveRecord::Base
  resourcify # add instance method roles
  include CsvImport
  include ActionView::Helpers::TextHelper
  extend EnumSupport

  #-------------------------------------------------------------------------------
  # Constants
  #-------------------------------------------------------------------------------
  ADJUSTMENT_TYPES = [1, 3]
  INSURANCE_TYPES  = [0]
  PAYMENT_TYPES    = [0, 2]
  PATIENT_PAYMENT_TYPES = [2]
  SERVICE_TYPES    = [6, 7]

  SALES_TAX_PROCEDURE_CODE_NAME = "SalesTax"
  INSURANCE_WO_PROCEDURE_CODE_NAME = "WOI"
  INSURANCE_PAYMENT_PROCEDURE_CODE_NAME = "IN"
  PATIENT_ADJUSTMENT_PROCEDURE_CODE_NAME = "ADJ"

  #-------------------------------------------------------------------------------
  # Behaviours
  #-------------------------------------------------------------------------------
  # acts_as_paranoid
  before_save :delete_fee_schedules_if_non_service
  #-------------------------------------------------------------------------------
  # associations
  #-------------------------------------------------------------------------------
  has_many :visit_details
  has_many :visits, :through => :visit_details
  #accepts_nested_attributes_for :procedure_codes_fee_schedule_labels,
  #                              :reject_if => proc { |attributes| attributes['fee_in_dollars'].blank? }
  has_many :fees, :dependent => :destroy
  has_many :fee_schedules, :through => :fees
  belongs_to :account

  #-------------------------------------------------------------------------------
  # validations
  #-------------------------------------------------------------------------------
  # TODO - cleanup these validations
  validates :account, :presence => true
  validates :name, :presence => true, :uniqueness => { :case_sensitive => false, :scope => :account_id }
  validates :description, :presence => true
  validates :cpt_code, :presence => true, :if => :is_service?
  validate :check_type_code
  validate :check_from_payment_or_adjustment_to_billable_code_type, :on => :save
  validate :check_from_billable_to_payment_or_adjustment_code_type, :on => :save

  enum_map :type_code, APP_CONFIG['options']['code_types']
  enum_map :service_type_code, APP_CONFIG['options']['service_types']
  default_value_for :type_code, 6
  default_value_for :service_type_code, 0


  accepts_nested_attributes_for :fees
  #-------------------------------------------------------------------------------
  # scopes
  #-------------------------------------------------------------------------------
  scope :alphabetically, lambda{ |order| {:order => "#{order || 'name'} ASC"} }
  scope :by_account, lambda { |account_id| where( 'account_id = ?', account_id ) }

  def self.switcher_order(account,clinic)
    ProcedureCode.by_account(account).order(:name)
  end

  #-------------------------------------------------------------------------------
  # CSV Import
  #-------------------------------------------------------------------------------
  #C;Chiropractic;O;P.T.;4;X-Ray;1;Medical;2;Surgery;3;Consultation;A;Ambulance;B;Maternity;N;Pharmaceutical
  CSV_SERVICE_TYPES = {
    'C' => 'Chiropractic',
    'O' => 'P.T.',
    '4' => 'X-Ray',
    '1' => 'Medical',
    '2' => 'Surgery',
    '3' => 'Consultation',
    'A' => 'Ambulance',
    'B' => 'Maternity',
    'N' => 'Pharmaceutical'
  }
  # handles the conversion and nil values default nil ==> Chiropractic
  def csv_service_type_code=(i_service_type)
    unless i_service_type.nil?
      self.service_type_code = CSV_SERVICE_TYPES[i_service_type]
    end
  end
  def csv_service_type_code
    # need to use the key accessor to reverse the rails enum mapping
    CSV_SERVICE_TYPES.invert[self.service_type_code_key]
  end
  #-------------------------------------------------------------------------------
  # class methods
  #-------------------------------------------------------------------------------
  #-------------------------------------------------------------------------------
  # instance methods
  #-------------------------------------------------------------------------------
  def can_delete?
    self.visit_details.count == 0
  end

  def type
    APP_CONFIG["options"]["code_types"].invert[type_code]
  end

  def rendered
    visit_details.collect(&:amount_in_dollars).sum.to_money
  end

  def paid
    Money.new(visit_details.collect{|pvd| pvd.incoming_payments.collect(&:amount_cents).sum }.sum)
  end

  # TODO: calculate this
  def adjusted
    0.to_money.format
  end

  #-----------------------------------------------------------------------------
  # called before record is saved to remove the fees if they are no
  #  longer applicable.
  #-----------------------------------------------------------------------------
  def delete_fee_schedules_if_non_service
    fees.clear unless is_service?
  end

  #-----------------------------------------------------------------------------
  # can this record have associated fees?
  #  helper to be used from the view to determine whether to render the 
  #  fee schedules region of the screen.
  #-----------------------------------------------------------------------------
  def can_have_fee_schedules?
    # the third phrase is saying or it can become a service
    #   by intersecting the selectable type codes with the SERVICE_TYPES
    type_code.nil? || is_service? || !(get_select_type_codes.values & SERVICE_TYPES).empty?
  end

  #-----------------------------------------------------------------------------
  # this method is going to push all the fee_schedules defined for this account
  # to this procedure code. its applied anytime a procedure_code
  # is created or updated.
  #-----------------------------------------------------------------------------
  def preload_existing_fee_schedules(persist = true)
    account.fee_schedules.each do |fee_schedule|
      unless self.fees.exists?(:fee_schedule_id => fee_schedule.id)
        if new_record? or not persist
          self.fees.build(:fee_schedule => fee_schedule)
        else
          self.fees.create(:fee_schedule => fee_schedule)
        end
      end
    end
  end

  def truncated_title
    "#{name} " << truncate(self.description, :length => 25)
  end


  def title
    "#{name} #{description} (#{cpt_code})"
  end
  alias :title_name :title

  def title_cpt_code
    "#{cpt_code} - #{name} - #{description}"
  end

  # todo: rewrite this
  def fee(patient_case_id)
    default_fee
  end

  # todo: rewrite this
  def default_fee(patient_case=nil)
    return nil unless self.is_service?
    procedure_codes_fee_schedules.find(:first, :conditions => patient_case.nil? ? nil :
                                             { :fee_schedule_label_id => patient_case.fee_schedule_id })
  end

  def is_insurance_billable?
    type_code == 6 
  end

  def non_insurance_billable?
    type_code == 7
  end

  def is_payment?
    PAYMENT_TYPES.include?(type_code)
  end

  def is_insurance_payment?
    INSURANCE_TYPES.include?(type_code)
  end

  def is_patient_payment?
    PATIENT_PAYMENT_TYPES.include?(type_code)
  end

  def is_service?
    SERVICE_TYPES.include?(type_code)
  end

  def is_adjustment?
    ADJUSTMENT_TYPES.include?(type_code)
  end

  def is_patient_adjustment?
    3 == type_code
  end

  def is_insurance_adjustment?
    1 == type_code
  end

  #-------------------------------------------------------------------------------
  # determine the list of allowed type_codes given the following rules:
  #   If a code is unused (it might be new or simply unused in a transaction detail, the user can change the code type.
  #   If a code is used, the user can change it within a limited range.
  #   Service codes can change between insurance billable and non-insurance billable,
  #      because doing do has NO EFFECT on accounting, but only changes whether or not the code shows up on a HCFA.
  #   Payment codes can change to other payment codes.
  #   Adjustment codes (accounting entries where no $ changes hands) can change to other adjustment codes.
  #-------------------------------------------------------------------------------
  def get_select_type_codes
    if self.new_record? or self.visit_details.count == 0
      list = TYPE_CODES
    else
      list = _get_sub_type_codes
    end
    return list
  end

  #-------------------------------------------------------------------------------
  # returns just the list of codes similar to the current code type.
  #-------------------------------------------------------------------------------
  def _get_sub_type_codes
    if self.is_service?
      list = TYPE_CODES.select { |k,v| SERVICE_TYPES.include?(v) }
    elsif self.is_payment?
      list = TYPE_CODES.select { |k,v| PAYMENT_TYPES.include?(v) }
    elsif self.is_adjustment?
      list = TYPE_CODES.select { |k,v| ADJUSTMENT_TYPES.include?(v) }
    end
    return list
  end


  def exceeds_copay?
    result, count = false, 0
    self.procedure_codes_fee_schedules.each do |fee_schedule|
      if !fee_schedule.fee_cents.nil? && fee_schedule.calculated_copay > fee_schedule.fee_cents
        result = true; count += 1
      end
    end
    return result, count
  end

  protected

  #-----------------------------------------------------------------------------
  # server side validation routine to run on update to prevent procedure code
  # type_code changing between different code types if its already in use.
  #-----------------------------------------------------------------------------
  def check_type_code
    if !self.new_record? and self.visit_details.count > 0
      if self.type_code_changed? and (self.type_code_was != 6 or self.type_code != 7)
        # we want to call this to determine if the new value was an
        # allowed selection from the dropdown list
        new_list = get_select_type_codes
        unless new_list.contains?(self.type_code_was)
          self.errors.add(:type_code, 'invalid type_code value change')
        end
      end
    end
  end

  def check_from_payment_or_adjustment_to_billable_code_type
    self.check_from_to_code_type([0, 1, 2, 3, 4], [6, 7])
  end

  def check_from_billable_to_payment_or_adjustment_code_type
    self.check_from_to_code_type([6, 7], [0, 1, 2, 3, 4])
  end

  def check_from_to_code_type(was=[], now=[])
    self.errors.add(:type_code, 'invalid value') if !self.new_record? and
    self.type_code_changed? and
    self.patient_visit_details.any? and
    was.include?(self.type_code_was) and
    now.include?(self.type_code)
  end

  #-------------------------------------------------------------------------------
  # aliased methods
  #-------------------------------------------------------------------------------
  alias :switcher_title :truncated_title

  #-------------------------------------------------------------------------------
  # Sub Classes
  #-------------------------------------------------------------------------------
  class <<self
    def build(configuration={})
      p = ProcedureCode.new(configuration)
      p.name = "Procedure Code"
      p.description = "Simple Procedure"
      p.save
      return p
    end

    def find_or_create_taxing_procedure_code
      ProcedureCode.find_by_name(INSURANCE_WO_PROCEDURE_CODE_NAME) || ProcedureCode.create(
        :name                => SALES_TAX_PROCEDURE_CODE_NAME,
        :description         => SALES_TAX_PROCEDURE_CODE_NAME,
        :type_code           => 6,
        :tax_rate_percentage => 0,
        :cpt_code            => "99999"
      )
    end

    def find_or_create_insurance_wo_procedure(account_id)
      p = ProcedureCode.find_by_name(INSURANCE_WO_PROCEDURE_CODE_NAME)
      return p unless p.nil?

      p = ProcedureCode.new({
        :name                => INSURANCE_WO_PROCEDURE_CODE_NAME,
        :description         => "Insurance Write-Off procedure code",
        :type_code           => 1,
        :tax_rate_percentage => 0,
        :cpt_code            => "99999",
        :account_id           => account_id
      })
      p.preload_existing_fee_schedules
      p.save
      return p
    end

    def find_or_create_insurance_payment_procedure(account_id)
      p = ProcedureCode.find_by_name(INSURANCE_PAYMENT_PROCEDURE_CODE_NAME)
      return p unless p.nil?

      p = ProcedureCode.new({
        :name                => INSURANCE_PAYMENT_PROCEDURE_CODE_NAME,
        :description         => "Insurance payment",
        :type_code           => 0,
        :tax_rate_percentage => 0,
        :cpt_code            => "99999",
        :account_id           => account_id
      })
      p.save
      return p
    end

    def find_or_create_patient_adjustment_procedure(account_id)
      p = ProcedureCode.find_by_name(PATIENT_ADJUSTMENT_PROCEDURE_CODE_NAME)
      return p unless p.nil?

      p = ProcedureCode.new({
        :name                => PATIENT_ADJUSTMENT_PROCEDURE_CODE_NAME,
        :description         => "Patient adjustment",
        :type_code           => 3,
        :tax_rate_percentage => 0,
        :cpt_code            => "99999",
        :account_id           => account_id
      })
      p.save
      return p
    end
  end
end
