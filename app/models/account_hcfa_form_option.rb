class AccountHcfaFormOption < HcfaFormOption
  resourcify # add instance method roles
  #----------------------------------------------------------------------------- 
  # String Value Options
  #----------------------------------------------------------------------------- 
  enum_map :state, State::ALL_STATES
  default_value_for :state, 'NY'
end

