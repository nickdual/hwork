class ProviderPrecedence < ActiveRecord::Base
  belongs_to :room
  belongs_to :provider
  # FIXME : the limitation of the validation below avoids and issue with creating new rooms,
  #         but doesn't fix the underlying problem. One symtom of the underlying problem 
  #         is the providers are initially assumed to be active.
  validates :room, :presence => { :on => :update }
  validates :provider, :presence => true

  # FIXME : these options are not currently being applied when :include from Room is used.
  JSON_OPTS = { :only => [:id, :provider_id, :order, :room_id] }
  def as_json(options={})
    super(JSON_OPTS.merge(options))
  end
end
