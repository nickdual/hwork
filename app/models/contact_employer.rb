class ContactEmployer < Contact
  # TODO fill out employer contact rules / class
  has_one :physical_address, :as => :addressable, :class_name => "Address", :conditions => { :label => 'Physical' }

  accepts_nested_attributes_for :physical_address
  delegate :one_liner, :to => :physical_address, :prefix => true
  delegate :street, :street2, :city, :state, :zip, :to => :physical_address
  delegate :street=, :street2=, :city=, :state=, :zip=, :to => :physical_address

  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  def self.build(attributes={})
    contact = self.new(attributes)
    contact.build(attributes)
    return contact
  end

  # never references the first name / last name
  alias_attribute :name, :company_name

  def build(attributes={})
    build_physical_address(attributes[:physical_address])
  end
end
