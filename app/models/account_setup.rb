class AccountSetup < ActiveRecord::Base
  belongs_to :account

  state_machine :initial => :new do
    event :complete do
      transition all => :completed
    end

    event :setup_clinic do
      transition all => :in_progress
    end

    event :setup_provider do
      transition all => :in_progress
    end

    event :setup_schedule do
      transition all => :in_progress
    end

    event :login do
      transition all => same
    end

    after_transition any => :in_progress, :do => :check_done
    before_transition any => any, :on => :setup_clinic do |account_setup|
      account_setup.clinics = true
    end
    before_transition any => any, :on => :setup_provider do |account_setup|
      account_setup.providers = true
    end
    before_transition any => any, :on => :setup_schedule do |account_setup|
      account_setup.schedules = true
    end
    before_transition any => any, :on => :login do |account_setup|
      account_setup.first_session = true
    end
    
  end

  protected

  def check_done
    if self.first_session and 
      self.clinics and
      self.providers and
      self.schedules
      self.complete!
    end
  end

end
