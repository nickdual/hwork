class SubscriptionPlan < ActiveRecord::Base
  has_many :subscriptions
  # renewal_period is the number of months to bill at a time
  # default is 1
  validates_numericality_of :renewal_period, :trial_period, :only_integer => true, :greater_than => 0
  validates_presence_of :name
  monetize :amount_cents
  monetize :setup_amount_cents
end
