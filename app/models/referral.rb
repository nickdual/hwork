class Referral < ActiveRecord::Base
  belongs_to :reference, :polymorphic => true
  belongs_to :referrable, :polymorphic => true
end
