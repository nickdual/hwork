require_dependency 'validate_associated_present'
require_dependency 'enum_support'

class Contact < ActiveRecord::Base
  extend ValidateAssociatedPresent
  extend EnumSupport
  has_many :addresses, :as => :addressable
  has_many :phones, :as => :phoneable
  has_many :emails, :as => :emailable
  has_many :appointments, :dependent => :destroy
  belongs_to :contactable, :polymorphic => true

  def full_name
    [first_name, middle_initial_with_period, last_name].compact.join(' ')
  end

  def last_name_first
    [last_name, [first_name, middle_initial_with_period].compact.join(" ")].compact.join(', ')
  end  
  
  def middle_initial_with_period
    middle_initial + '.' unless middle_initial.blank?
  end
    
  alias :name :full_name

  #def validate_associated_records_for_addresses()
  #end

  #def validate_associated_records_for_emails()
  #end

  #-----------------------------------------------------------------------------
  # scopes
  #-----------------------------------------------------------------------------
  #  this will search any named field for a match
  #  this multi field psuedo concatenated field search is from:
  #    http://stackoverflow.com/questions/4384320/where-clause-with-concatenated-fields-in-rails-3
  #
  # def self.named_like(str)
  #   sql = str.split.map do |word|
  #     %w[first_name last_name middle_initial company_name].map do |column|
  #       sanitize_sql ["#{column} LIKE ?", "%#{word}%"]
  #     end.join(" or ")
  #   end.join(") and (")
  #   where("(#{sql})")
  # end
  #
  # the example above is the old method and is a good example of searching a set 
  # of columns individually. so its worth keeping around.
  #
  # what this method (below) will do is match a string against the name fields
  # as "last_name, first_name" using either a starts_with or contains match 
  # pattern according to the mode parameter.
  #
  # OPTIMIZE : the SQL below is most likely a MySQL specific query, and would need
  # to be generalized through AREL to work with other storage adapters.
  def self.named_like(str, mode="starts_with")
    base = "concat(last_name, ', ', first_name) LIKE"
    if mode == "contains"
      sql = sanitize_sql "#{base} '%#{str}%'" 
    else
      sql = sanitize_sql "#{base} '#{str}%'" 
    end
    where("#{sql}")
  end

  #-----------------------------------------------------------------------------
  # methods to test for particular associated objects
  #   used in performing conditional validation
  #-----------------------------------------------------------------------------
  def fax_phone_present?
    if self.fax_phone.nil? or self.fax_phone.empty? then
      return false
    else
      return true
    end
  end

  #-----------------------------------------------------------------------------
  # used to allow the import of records to safely "ovrride" value, but ignore the
  # the value if its blank.
  #-----------------------------------------------------------------------------
  def company_name_safe=(i_name)
    unless i_name.blank?
      self.company_name = i_name
    end
  end

  def company_name_safe
    self.company_name
  end

  #-----------------------------------------------------------------------------
  # indicates if the specified contact is at a different location than this
  # one.
  #-----------------------------------------------------------------------------
  def different_location?(other_contact)
    # first try to find a "Physical Address"
    #  - if not now just take the first address from both
    my_location = self.addresses.find_by_label('Physical')
    my_location ||= self.addresses.first
    other_location = other_contact.addresses.find_by_label('Physical')
    other_location ||= other_contact.addresses.first
    return my_location.different_location?(other_location)
  end

  #-----------------------------------------------------------------------------
  # generate notes to be displayed with appointments for this contact
  #   returns an array of strings
  #   you can override this method in any of the specific contact subclasses
  #   to alter the output for a particular type of contact.
  #-----------------------------------------------------------------------------
  def appointment_notes
    [ 
      # leave Client Type as the first value in the array, some of the subclasses
      # may opt to slice off this element (e.g. contact_patient)
      "Client Type: #{self.contactable_type}",
      "Last Appointment: #{self.appointments.by_date.last.try(:date)}",
      "Address: #{self.addresses.first.try(:one_liner)}",
      "Email: #{self.emails.first.try(:email)}",
    ]
  end

  #-----------------------------------------------------------------------------
  # Report back the primary phone for this contact or just pull the first 
  # phone in the list if none are marked as primary.
  #-----------------------------------------------------------------------------
  def phone
    phone = self.phones.select { |x| x.primary }[0]
    phone ||= self.phones.first
    phone.try(:phone)
  end

  def add_phone(p_label, p_number)
    self.phones.each do |p|
      if p.label == p_label
        p.number = p_number
        return
      end
    end
    self.phones << Phone.new(:label => p_label, :number => p_number)
  end

  def get_phone(p_label)
    self.phones.each do |p|
      if p.label == p_label
        return p
      end
    end
  end

  protected

  #-----------------------------------------------------------------------------
  # check for only one primary phone
  #-----------------------------------------------------------------------------
  def only_one_primary_phone
    # TODO : fill this method to check on the DB there's only one primary phone
  end

end
