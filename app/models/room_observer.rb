class RoomObserver < ActiveRecord::Observer
  def after_create(model)
    if model.account.providers.count == 1
      model.providers << model.account.providers.first
    end
    unless model.account.has_multiple_locations?
      model.clinics << model.account.clinics
    end
    model.save!
  rescue NoMethodError
  end
end
