require_dependency 'csv_import'
class Reference < ActiveRecord::Base
  resourcify # add instance method roles
  include CsvImport
  belongs_to :account
  has_one :contact, :as => :contactable, :class_name => 'ContactReference'
  accepts_nested_attributes_for :contact
  validates :account, :presence => true
  
  delegate :company_name, :company_name=, :name, :name=, :to => :contact, :allow_nil => true
  delegate :physical_address_one_liner, :to => :contact, :allow_nil => true
  delegate :state, :to => :contact
  alias_attribute :switcher_title, :company_name
  #-------------------------------------------------------------------------------
  # class methods
  #-------------------------------------------------------------------------------
  scope :alphabetically, :joins => :contact, :order => 'contacts.company_name ASC, contacts.last_name ASC, contacts.first_name ASC'

  def self.by_account(account_id)
    where('account_id = ?', account_id)
  end

  def self.switcher_order(account,clinic)
    by_account(account).alphabetically
  end
  def self.build(configuration={})
    c = self.new(configuration)
    c.contact = ContactReference.build(:contactable => c)
    return c
  end
  #-------------------------------------------------------------------------------
  # instance methods
  #-------------------------------------------------------------------------------
  def can_delete?
    patient_cases.empty? && patients.empty?
  end

  #-------------------------------------------------------------------------------
  # CSV Import
  #-------------------------------------------------------------------------------
  def self.get_csv_instance(import_type,account,row)
    t = Reference.build()
    t.contact.build_fax_phone()
    return t
  end
  def csv_referrer_name
    self.contact.full_name
  end
  def csv_referrer_name=(i_name)
    unless i_name.nil?
      parts = i_name.split(' ')
      if parts.size == 1
        self.contact.last_name = parts[0]
      elsif parts.size == 2
        self.contact.first_name = parts[0]
        self.contact.last_name = parts[1]
      elsif parts.size >= 3
        self.contact.first_name = parts[0]
        self.contact.middle_initial = parts[1]
        self.contact.last_name = parts[2]
      end
    end
  end
end
