class AppointmentType < ActiveRecord::Base
  resourcify # add instance method roles
  belongs_to :account
  has_many :provider_appointment_types, :dependent => :destroy
  has_many :room_appointment_types, :dependent => :destroy
  has_many :providers, :through => :provider_appointment_types, :uniq => true
  has_many :rooms, :through => :room_appointment_types, :uniq => true

  validates :name, :presence => true, :uniqueness => { :case_sensitive => false, :scope => :account_id }
  validates :duration_max, :presence => true, :numericality => { :only_integer => true, :greater_than_or_equal_to => lambda {|r| r.duration_min ? r.duration_min : 0} }
  validates :duration_min, :presence => true, :numericality => { :only_integer => true, :greater_than_or_equal_to => 0 }
  validates :provider_time, :presence => true, :numericality => { :only_integer => true, :greater_than_or_equal_to => 0, :less_than_or_equal_to => lambda {|s| s.duration ? s.duration : 0 } }
  validates :duration, :presence => true, :numericality => { :only_integer => true, :greater_than_or_equal_to => lambda {|r| r.duration_min ? r.duration_min : 0}, :less_than_or_equal_to => lambda {|s| s.duration_max ? s.duration_max : 0} }
  validates :schedule_lead_time, :presence => true, :numericality => { :only_integer => true, :greater_than_or_equal_to => 0 } 
  validates :pre_appointment_gap, :presence => true, :numericality => { :only_integer => true, :greater_than_or_equal_to => 0 }
  validates :post_appointment_gap, :presence => true, :numericality => { :only_integer => true, :greater_than_or_equal_to => 0 }

  accepts_nested_attributes_for :providers, :rooms

  def self.switcher_order(account,clinic)
    account.appointment_types.order(:name)
  end

  def self.public
    where(:is_public => true)
  end

  alias_attribute :switcher_title, :name
end
