class BusinessPhone < Phone
  def self.new(attributes={}, options={})
    phone = super(attributes, options)
    phone.label = 'Work'
    return phone
  end
end
