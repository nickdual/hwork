jQuery('body.admin').ready ->
  HandyworksApp.resourceDeleteHandler()
  HandyworksApp.resourceEditPopupHandler()
  HandyworksApp.setupColorPickerHandler()
  HandyworksApp.Popup.init()
  HandyworksApp.put_on_style()
  HandyworksApp.init('body')
