#-------------------------------------------------------------------------------
# Backbone application, dependencies and mixins for HW Applications
#-------------------------------------------------------------------------------
#= require underscore
#= require backbone
#= require backbone_rails_sync
#= require backbone_datalink
#= require backbone-support
#= require backbone-nested
#= require backbone.memento
#= require backbone.validations
#= require backbone.workflow
