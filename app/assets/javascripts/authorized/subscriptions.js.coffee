_.extend window.HandyworksApp,

  loadStripe: ->
    console.info('loading Stripe API')
    jQuery.externalScript("https://js.stripe.com/v1/").done (script, textStatus) ->
      Stripe.setPublishableKey $("meta[name=\"stripe-key\"]").attr("content")
      subscription =
        setupForm: ->
          console.info('setup card form callback')
          jQuery(".card_form").submit (event) ->
            console.log('in form submission', event)
            jQuery("input[type=submit]").prop "disabled", true
            console.info('in stripe setupForm callback')
            if jQuery("#card_number").length
              subscription.processCard()
              false
            else
              true
        processCard: ->
          console.log('--> process Card')
          card = undefined
          card =
            name: jQuery("#card_name").val()
            number: jQuery("#card_number").val()
            cvc: jQuery("#card_code").val()
            expMonth: jQuery("#card_month").val()
            expYear: jQuery("#card_year").val()
          Stripe.createToken card, subscription.handleStripeResponse
        handleStripeResponse: (status, response) ->
          console.log('stripe response',status,response)
          if status is 200
            jQuery("#subscription_stripe_token").val response.id
            jQuery(".card_form")[0].submit()
          else
            console.warn('Stripe response error',response)
            HandyworksApp.showError(response.error.message,'.flash')
            jQuery("input[type=submit]").prop "disabled", false
      subscription.setupForm()

