
_.extend window.HandyworksApp,

  procedureCodeForm: (procedure_code_id, procedure_code_name) ->
    # configures the JavaScript function hide/show the Fees as necessary based on
    # the procedure_code type value.
    jQuery('#procedure_code_type_code').change(type_code_fee_display)
    # below is an example of customizing the FB popup form view via JavaScript 
    # for a particular resource (procedure_code) basically we need to trigger the
    # change event for the type code selection box, but just calling trigger on it
    # was too fast for the FB popup which wouldn't have the field displayed yet. We're
    # leaving the initial trigger in as well in the event the form is not being viewed
    # in an FB popup.
    jQuery('body').bind 'afterShow.fb', ->
      jQuery('#procedure_code_type_code').trigger('change')
      jQuery('body').unbind 'afterShow.fb'
    # for non-FB popups.
    jQuery('#procedure_code_type_code').trigger('change')

    # setup fee schedule deletion
    jQuery('.delete_fee_schedule').bind('ajax:error', (evt, xhr, status, error) ->
      console.error('An error occured processing your request.',xhr.responseText)
    ).bind 'ajax:beforeSend', ->
      confirm('This will delete the fee schedule and all associated fees? Do you wish to proceed?')

    # simple client-side validation logic for new procedure codes
    HandyworksApp.validate "form.procedure_code",
      messages:
        'procedure_code[name]':
          remote: jQuery.validator.format("{0} has already been taken")
        'procedure_code[cpt_code]':
          required: "This field is required for Service category procedures"
      rules:
        'procedure_code[name]':
          required: true
          remote:
            param:
              url: "#{root_path}validate/procedure_code/name.json"
              data:
                'procedure_code[id]': procedure_code_id
            depends: ->
              return $("#procedure_code_name").val() != procedure_code_name
        'procedure_code[description]':
          required: true
        'procedure_code[cpt_code]':
          required: (element) ->
            code_t = jQuery("#procedure_code_type_code").val()
            console.log(" validating procedure code cpt_code",code_t)
            if code_t == "6" or code_t == "7"
              return true
            return false
