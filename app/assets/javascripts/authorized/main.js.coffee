jQuery('body.authorized').ready ->
  # TODO : move this resourceDeleteHandler into a more generic event delegation
  #        step. and also the resourceEditPopupHandler too.
  HandyworksApp.Data.Cache = new HandyworksApp.Models.Cache()
  HandyworksApp.resourceDeleteHandler()
  HandyworksApp.resourceEditPopupHandler()
  HandyworksApp.setupColorPickerHandler()
  HandyworksApp.Popup.init()
  HandyworksApp.put_on_style()
