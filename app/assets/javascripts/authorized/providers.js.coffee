
_.extend window.HandyworksApp,

  providerFormValidation: (provider_id, provider_code, provider_name) ->
    # simple client-side validation logic for new providers
    HandyworksApp.validate "form.provider",
      messages:
        'provider[your_code]':
          remote: jQuery.validator.format("{0} has already been taken")
        'provider[signature_name]':
          remote: jQuery.validator.format("{0} has already been taken")
      rules:
        'provider[your_code]':
          required: true
          remote:
            param:
              url: "#{root_path}validate/provider/your_code.json"
              data:
                'provider[id]': provider_id
            depends: ->
              return $("#provider_your_code").val() != provider_code
        'provider[signature_name]':
          required: true
          remote:
            param:
              url: "#{root_path}validate/provider/signature_name.json"
              data:
                'provider[id]': provider_id
            depends: ->
              return $("#provider_signature_name").val() != provider_name
        'provider[contact_attributes][business_email_attributes][email]':
          email: true

  providerSetupLegacyLabels: ->
    # setup fee schedule deletion
    jQuery('.delete_legacy_id_label').bind('ajax:error', (evt, xhr, status, error) ->
      console.error('An error occured processing your request',xhr.responseText)
    ).bind('ajax:beforeSend', ->
      return(confirm('This will delete the legacy id label and all associated legacy ids? Do you wish to proceed?'))
    )

    #-----------------------------------------------------------------------------
    # setup the color picker fields, see also HandyworksApp.setupColorPickerHandler
    #-----------------------------------------------------------------------------
    #jQuery("#color_picker").farbtastic("#provider_appointment_color")
