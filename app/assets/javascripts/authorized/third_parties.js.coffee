
# TODO : eliminate these global variables.
carrier_label = "Carrier"
attorney_label = "Firm Name"

_.extend window.HandyworksApp,

  get_third_party_type: (carrier_type) ->
    if type == "Attorney"
      return "Attorney"
    else
      return "InsuranceCarrier"

  switch_third_party_type: (type) ->
    if type == "Attorney"
      jQuery('.carrier_field').hide()
      jQuery('.attorney_field').show()
      jQuery('label[for=third_party_contact_attributes_company_name]').text(attorney_label)
    else
      jQuery('.carrier_field').show()
      jQuery('.attorney_field').hide()
      jQuery('label[for=third_party_contact_attributes_company_name]').text(carrier_label)

  thirdPartyEditSetup: ->
    jQuery('select#third_party_insurance_carrier_type_code').change ->
      z = jQuery(this).children('option:selected').text()
      HandyworksApp.switch_third_party_type(z)
    #---------------------------------------------------------------------------
    # apply the lightbox popup style to editing
    #---------------------------------------------------------------------------
    HandyworksApp.editPopup([".client_hcfa_edit",".client_hcfa_add"], document, false)

  thirdPartyFormValidation: (third_party_id, third_party_name) ->
    # basic client-side validation logic for third parties
    HandyworksApp.validate "form.third_party",
      messages:
        'third_party[alias_name]':
          remote: jQuery.validator.format("{0} has already been taken")
      rules:
        'third_party[alias_name]':
          required: true
          remote:
            param:
              url: "#{root_path}validate/third_party/alias_name.json"
              data:
                'third_party[id]': third_party_id
            depends: ->
              return $("#third_party_alias_name").val() != third_party_name
        'third_party[contact_attributes][company_name]':
          required: true
        'third_party[contact_attributes][business_email_attributes][email]':
          email: true

