_.extend window.HandyworksApp,
  feeScheduleFormValidation: ->
    # client side validation
    HandyworksApp.validate "form.fee_schedule",
      rules:
       'fee_schedule[label]':
         required: true

# TODO : change the fee schedule javascript functions to be under the HandyworksApp namespace.
#
#------------------------------------------------------------------------------
# Auto Update the Fee Schedule Insurance Payment values whenever the user
# sets the Fee or Copay fields while editting a procedure_code.
# - the "real" copay field(s) are hidden, there's a field for both % and $
#   the field the user has access to edit is a dummy field which can represent
#   either a % or $ amount. both hidden fields are updated whenever any fee 
#   change field is updated.
# - the dummy copay field has class .copay to distiguish it, and it doesn't 
#   have the same id format since its hand-coded instead of form builder
#   generated
# - there are bootstrap styled prepend and append text on the copay field
#   which are show/hide upon a change to is_percentage
# - the hidden percentage field is maintained as a true % 0.00 - 1.00, but
#   is multiplied by 100 for display to the user.
#------------------------------------------------------------------------------
changeExpectedInsuranceOfFeeSchedule = (event) ->
  id_string = '0'
  if jQuery(event.currentTarget).hasClass('copay')
    id_string = jQuery(event.currentTarget).closest('td').find('input[type=hidden]').first().attr('id')
  else
    id_string = event.currentTarget.id
  matches = /procedure_code_fees_attributes_(\d+)_.+/i.exec(id_string)
  number = parseInt(matches[1])
  row = number + 1
  fee = parseFloat(jQuery('#procedure_code_fees_attributes_'+number+'_fee').val())
  copay = parseFloat(jQuery('#procedure_code_fees_attributes_'+number+'_copay').parent().find('input.copay').val())
  isPercentage = jQuery('#procedure_code_fees_attributes_'+number+'_is_percentage').attr('checked')
  console.log("in changeExpectedInsuranceOfFeeSchedule",event,number,row,fee,copay,isPercentage)
  if isPercentage?
    expectedInsurancePayment = fee - ((fee * copay) / 100)
    jQuery('#procedure_code_fees_table tr:nth(' + row + ') .copayVal').hide()
    jQuery('#procedure_code_fees_table tr:nth(' + row + ') .copayPct').show()
    jQuery('#procedure_code_fees_attributes_'+number+'_copay_pct').val(copay / 100)
    jQuery('#procedure_code_fees_attributes_'+number+'_copay').val(fee * copay / 100)
  else
    expectedInsurancePayment = fee - copay
    jQuery('#procedure_code_fees_table tr:nth(' + row + ') .copayPct').hide()
    jQuery('#procedure_code_fees_table tr:nth(' + row + ') .copayVal').show()
    jQuery('#procedure_code_fees_attributes_'+number+'_copay').val(copay)
    jQuery('#procedure_code_fees_attributes_'+number+'_copay_pct').val(copay / fee)
  value = if expectedInsurancePayment >= 0 then expectedInsurancePayment.toFixed(2) else '0.00'
  console.log("-->expected insurance",expectedInsurancePayment,value)
  jQuery('#procedure_code_fees_attributes_'+number+'_expected_insurance_payment').val(value)

#-----------------------------------------------------------------------------
# global function for fee schedule binding
#   - use of the 'on' method for event handling allows us to just bind the
#     handler to the procedure_code_fees_table on page load and not have 
#     to re-bind the handler whenever fee_schedules are added.
#-----------------------------------------------------------------------------
window.bind_fee_schedule_calculation = () ->
  jQuery('#procedure_code_fees_table').on 'change.handyworks', 'input.changesExpectedInsurance', changeExpectedInsuranceOfFeeSchedule

#-----------------------------------------------------------------------------
# helper for procedure code fee schedule management
# TODO - Currently using a hard coded SERVICE_TYPES array.
#        this could be passed in using technique described here:
#             http://stackoverflow.com/questions/2511982/keeping-a-set-of-environment-variables-in-rails-and-javascript
#          (seem like a bit of overkill)
#-----------------------------------------------------------------------------
window.type_code_fee_display = ->
  if jQuery.inArray(@value, ['6','7']) == -1
    jQuery('div#fee_schedule_stuff').slideUp()
    jQuery('div#non_service_type').slideUp()
  else
    jQuery('div#fee_schedule_stuff').slideDown()
    jQuery('div#non_service_type').slideDown()
    if @value == '6'
      jQuery('.insBillable').show()
    else
      jQuery('.insBillable').hide()

