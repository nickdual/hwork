_.extend window.HandyworksApp,

  #-----------------------------------------------------------------------------
  # Initialize the jstree based interface to navigate through a set of user 
  # permissions. NOTE: this jstree used to use a nice "apple" theme; however,
  # the current version of the jstree-rails gem does not include the apple
  # theme. However, upgrading to the github edge version would allow to switch
  # the theme back to "apple".
  #-----------------------------------------------------------------------------
  setupUserPermissionsTree: ->
    jQuery(".permtree").jstree
      plugins: ["themes", "html_data", "ui"]
      themes:
        theme: "default"
      ui:
        select_limit: 1
    jQuery(".permtree").on 'select_node.jstree', (event, data) ->
      console.log("permissions tree node selected",event,data,data.rslt.obj,data.rslt.obj.data())
      field_name = data.rslt.obj.data("field")
      jQuery(".permission_field").hide()
      jQuery("#" + field_name).show()
    jQuery("#user_account_role").change (event) ->
      role = jQuery(event.target).val()
      if role.match(/Admin/)
        jQuery(".permissions.row").addClass("hidden")
      else
        jQuery(".permissions.row").removeClass("hidden")
