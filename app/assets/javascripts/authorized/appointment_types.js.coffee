_.extend window.HandyworksApp,

  appointmentTypeFormValidation: (appointment_type_id, appointment_type_name) ->
    # client side validation
    HandyworksApp.validate "form.appointment_type",
      messages:
        'appointment_type[name]':
          remote: jQuery.validator.format("{0} has already been taken")
      rules:
        'appointment_type[name]':
          required: true
          remote:
            param:
              url: "#{root_path}validate/appointment_type/name.json"
              data:
                'appointment_type[id]': appointment_type_id
            depends: ->
              return $("#appointment_type_name").val() != appointment_type_name
        'appointment_type[duration_max]':
          required: true
          digits: true
          min: 0
          greaterThanOrEqual: ["#appointment_type_duration_min", 'Duration Min']
        'appointment_type[duration_min]':
          required: true
          digits: true
          min: 0
        'appointment_type[duration]':
          required: true
          digits: true
          greaterThanOrEqual: ["#appointment_type_duration_min", 'Duration Min']
          lessThanOrEqual: ["#appointment_type_duration_max", 'Duration Max']
        'appointment_type[provider_time]':
          required: true
          digits: true
          min: 0
          lessThanOrEqual: ["#appointment_type_duration", 'Duration']
        'appointment_type[schedule_lead_time]':
          required: true
          digits: true
          min: 0
          max: (2*24*60)
        'appointment_type[pre_appointment_gap]':
          required: true
          digits: true
          min: 0
          max: 60
        'appointment_type[post_appointment_gap]':
          required: true
          digits: true
          min: 0
          max: 60
        'appointment_type[can_create_deadline]' :
          required: true
          digits: true
          min: 0
          max: 60
        'appointment_type[can_change_deadline]' :
          required: true
          digits: true
          min: 0
          max: 60
        'appointment_type[can_cancel_deadline]' :
          required: true
          digits: true
          min: 0
          max: 60
        'appointment_type[max_number_of_appointments_user_can_book_in_advance]' :
          required: true
          digits: true
          min: 0
          max: 20
