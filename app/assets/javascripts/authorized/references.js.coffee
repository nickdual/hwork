
_.extend window.HandyworksApp,

  referenceFormValidation: (reference_id, reference_name) ->
    # basic client-side validation logic for references
    HandyworksApp.validate "form.reference",
      messages:
        'reference[contact_attributes][company_name]':
          remote: jQuery.validator.format("{0} has already been taken")
      rules:
        'reference[contact_attributes][company_name]':
          required: true
          remote:
            param:
              url: "#{root_path}validate/reference/company_name.json"
              data:
                'reference[id]': reference_id
              depends: ->
                return $("#reference_contact_attributes_company_name").val() != reference_name
        'reference[contact_attributes][business_email_attributes][email]':
          email: true
