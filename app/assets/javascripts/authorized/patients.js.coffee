_.extend window.HandyworksApp,

  setupPatientFormView: ->
    jQuery("#patient_birthdate").datepicker
      changeMonth: true
      changeYear: true
      yearRange: "1900:c"
      dateFormat: HandyworksApp.Configuration.dateFormat
      firstDay: HandyworksApp.Configuration.firstDay

    #---------------------------------------------------------------------------
    # enforce the "primary" phone checkbox to behave like a radio button so
    # the user can only select one at a time.
    #---------------------------------------------------------------------------
    jQuery("#phone_list").on 'click','input[type="checkbox"]', (event) ->
      jQuery('#phone_list input[type="checkbox"]').not(event.target).attr('checked',false)

    #---------------------------------------------------------------------------
    # trigger an event that can be used to invalidate patient cached data.
    #---------------------------------------------------------------------------
    jQuery('form.patient').submit (event) ->
      HandyworksApp.dispatcher.trigger('patientchange')

  patientFormValidation: ->
    # basic client side validation for patients
    HandyworksApp.validate "form.patient",
      rules:
        'patient[contact_attributes][last_name]':
          required: true
        'patient[birthdate]':
          dateHW: true
        'patient[contact_attributes][personal_email_attributes][email]':
          email:true
