_.extend window.HandyworksApp,
  #---------------------------------------------------------------------------
  # we need to create a "fake" but unique index for any newly activated 
  # providers. Common solution to this problem is a timestamp based index; 
  # however, this solution has problems, which would be particularly exposed
  # when using an addAll operation.
  # 
  # the solution is straightforward enough, use a flat counter instead.
  # initialize it with the current time, so there's no practical limit on
  # the number of providers for this approach would be valid.
  #---------------------------------------------------------------------------
  new_provider_counter: new Date().getTime()

  getProviderFieldId: (opt) ->
    provider_id = jQuery(opt).attr('value')
    provider_hidden_field = jQuery('input.provider_id[type="hidden"][value="' + provider_id + '"]')
    provider_hidden_field.attr('id')

  #---------------------------------------------------------------------------
  # callback for the picklist widget after a provider has been moved to the 
  # active list. This logic needs to handle a few different cases:
  #   a) The provider is completely new to this room, we need to insert the
  #      equivalent forms as if this field had been created  by the server
  #   b) This record was previously active, but has been deactivated during
  #      this request, in this case we would have created some delete fileds
  #      which need to be cleaned up
  #   c) kind of a fall through, but in the event this provider is active,
  #      we need to make sure the order is correct.
  #---------------------------------------------------------------------------
  handleSelectedProvider: () =>
    # first push the order from the target list into the source list
    #  the provider id in the target list is stored as data-value attribute
    jQuery('.pickList_targetListContainer li').each (position, item) ->
      provider_id = jQuery(item).data('value')
      provider_option = jQuery('select#providerSelections option[value="' + provider_id + '"]')
      provider_option.data('order',position)
    # now we'll process the list source list in general to go back and updated
    # the underlying rails fields for the nested model association.
    jQuery('#providerSelections option:selected').each (pos,opt) ->
      # create placeholders for new activation
      #  - if 'new' is already set then we've previously created new activation fields for this provider
      #    technically this should not happen, cause they would have been deleted during deactivation.
      unless jQuery(opt).data('existing') or jQuery(opt).data('new')
        provider_id = jQuery(opt).val()
        jQuery('#provider_precedences_inputs').append(JST['rooms/_room_provider_precedence']({position: HandyworksApp.new_provider_counter, order: pos, provider_id: provider_id}))
        jQuery(opt).data('new',HandyworksApp.new_provider_counter)
        HandyworksApp.new_provider_counter = HandyworksApp.new_provider_counter + 1
      # update sequence for existing and new placeholders
      #  - this will take care of populating any undefined order, and will match the new order to the arrangement on the screen.
      provider_order_hidden_field_id = HandyworksApp.getProviderFieldId(opt).replace("_provider_id","_order")
      jQuery('#' + provider_order_hidden_field_id).val(jQuery(opt).data('order'))
      # cleanup previously deleted items
      #  when we deleted them the first time, we would have added some special fields to delete this association
      #  on the server side. we'll remove those fields and reset the data indicator.
      if jQuery(opt).data('destroy')
        provider_hidden_field_id = HandyworksApp.getProviderFieldId(opt)
        jQuery('#'+provider_hidden_field_id + '_destroy_fields').remove()
        jQuery(opt).removeData('destroy')

  #---------------------------------------------------------------------------
  # callback from the picklist when an item is removed from the active list.
  # there are few situations this logic needs to handle:
  #  a) if this was an existing active provider , then special fields need 
  #     to be added to delete this association on the server sided.
  #  b) if this as a new provider activation, then we previously would have
  #     added some special fields to build this association, we should clean
  #     the results up, and reset the indicator.
  #---------------------------------------------------------------------------
  handleRemovedProvider: () =>
    jQuery('#providerSelections option').not(":selected").each (pos,opt) ->
      # add delete commands for existing active providers.
      if jQuery(opt).data('existing')
        provider_hidden_field_id = HandyworksApp.getProviderFieldId(opt)
        v_destroy_id = provider_hidden_field_id.replace("_provider_id","__destroy")
        v_destroy_name = jQuery('#'+provider_hidden_field_id).attr('name').replace("[provider_id]","[_destroy]")
        jQuery('#provider_precedences_inputs').append(JST['rooms/_room_provider_precedence_destroy']({destroy_id: v_destroy_id, destroy_name: v_destroy_name, destroy_container: provider_hidden_field_id + '_destroy_fields'}))
        jQuery(opt).data('destroy','d')
      # remove create commands for newly active providers that have been now removed
      if jQuery(opt).data('new')
        provider_hidden_field_id = HandyworksApp.getProviderFieldId(opt)
        jQuery('#'+provider_hidden_field_id + '_create_fields').remove()
        jQuery(opt).removeData('new')

  roomFormValidation: (room_id, room_name) ->
    #---------------------------------------------------------------------------
    # basic client-side validation logic for rooms
    #---------------------------------------------------------------------------
    HandyworksApp.validate "form.room",
      messages:
        'room[name]':
          remote: jQuery.validator.format("{0} has already been taken")
      rules:
        'room[name]':
          required: true
          remote:
            param:
              url: "#{root_path}validate/room/name.json"
              data:
                'room[id]': room_id
            depends: ->
              return $("#room_name").val() != room_name

  roomFormProviderPickListSetup: ->
    #---------------------------------------------------------------------------
    # Controls the dual pane pickList interface.
    #---------------------------------------------------------------------------
    jQuery('#providerSelections').pickList
      sourceListLabel:    "Restricted"
      targetListLabel:    "Can Schedule"
      addAllLabel:        '<i class="icon-chevron-left"/><i class="icon-chevron-left"/>'  # "Add All"
      addLabel:           '<i class="icon-chevron-left"/>'
      removeAllLabel:     '<i class="icon-chevron-right"/><i class="icon-chevron-right"/>'  # "Remove All"
      removeLabel:        '<i class="icon-chevron-right"/>'
      sortItems:          true
      sortAttribute:      'data-order'
      afterAdd: HandyworksApp.handleSelectedProvider
      afterRemove: HandyworksApp.handleRemovedProvider
      afterAddAll: HandyworksApp.handleSelectedProvider
      afterRemoveAll: HandyworksApp.handleRemovedProvider

    #---------------------------------------------------------------------------
    # swap the pickList column order
    #  - this effectively puts the source column on the right and the target
    #    column on the left
    #  - the label images above are also backward to account for the swapping
    #    of these columns.
    #---------------------------------------------------------------------------
    source = jQuery('.pickList_sourceListContainer').detach()
    target = jQuery('.pickList_targetListContainer').detach()
    jQuery('.pickList').prepend(target)
    jQuery('.pickList_controlsContainer').after(source)

    #---------------------------------------------------------------------------
    # this will insert the up down buttons after the target container
    #---------------------------------------------------------------------------
    jQuery('.pickList_targetListContainer').before(JST['rooms/_room_provider_precedence_order']())

    #---------------------------------------------------------------------------
    # callback for the UP button. it needs to re-order the actual list in the
    # targetList container. Each selected item will be moved up one slot 
    # assuming it is not the first item in the list.
    #---------------------------------------------------------------------------
    jQuery('.pickList_up').click (event) ->
      event.preventDefault()
      event.stopPropagation()
      nextUp = "" # needs to be accessible to the conditional below.
      jQuery('.pickList_targetListContainer li').each (position, item) ->
        if jQuery(item).hasClass('ui-selected') and position > 0
          jQuery(item).detach().insertBefore(nextUp)
        nextUp = item
      # now we'll let the selected provider callback push the changes to the
      # underlying hidden form fields.
      HandyworksApp.handleSelectedProvider()
        
    #---------------------------------------------------------------------------
    # callback for the DOWN button. it re-orders the actual target list items
    # similar to the UP button callback above.
    #---------------------------------------------------------------------------
    jQuery('.pickList_down').click (event) ->
      event.preventDefault()
      event.stopPropagation()
      jQuery('.pickList_targetListContainer li').each (position, item) ->
        if jQuery(item).hasClass('ui-selected')
          # look for the next item
          nextDown = jQuery(item).next('li')
          if nextDown.length > 0
            jQuery(item).detach().insertAfter(nextDown)
      # now we'll let the selected provider callback push the changes to the
      # underlying hidden form fields.
      HandyworksApp.handleSelectedProvider()
