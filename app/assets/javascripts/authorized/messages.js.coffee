
_.extend window.HandyworksApp,

  messageFormValidation: (message_id, message_name) ->
    # basic client-side validation logic for messages
    HandyworksApp.validate "form.message",
      messages:
        'message[label]':
          remote: jQuery.validator.format("{0} has already been taken")
      rules:
        'message[label]':
          required: true
          remote:
            param:
              url: "#{root_path}validate/message/label.json"
              data:
                'message[id]': message_id
            depends: ->
              return $("#message_label").val() != message_name


