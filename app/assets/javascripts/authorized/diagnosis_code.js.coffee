_.extend window.HandyworksApp,
  diagnosisCodeValidation: (diagnosis_code_id, diagnosis_code_name) ->
    # basic client-side validation logic for diagnosis codes
    HandyworksApp.validate "form.diagnosis_code",
      messages:
        'diagnosis_code[name]':
          remote: jQuery.validator.format("{0} has already been taken")
      rules:
        'diagnosis_code[name]':
          required: true
          maxlength: 15
          remote:
            param:
              url: "#{root_path}validate/diagnosis_code/name.json"
              data:
                'diagnosis_code[id]': diagnosis_code_id
            depends: ->
              return $("#diagnosis_code_name").val() != diagnosis_code_name
        'diagnosis_code[description]':
          required: true
        'diagnosis_code[code]':
          required: true

