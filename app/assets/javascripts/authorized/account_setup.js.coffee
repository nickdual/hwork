jQuery('body.account_setup').ready ->
  HandyworksApp.editPopup([".tabbable a.btn.new", ".tabbable a.btn.edit"], window.document, false)
  HandyworksApp.schedulePopup(jQuery(".tabbable a.btn.schedule"))
