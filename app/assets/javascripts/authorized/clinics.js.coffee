
_.extend window.HandyworksApp,

  setupClinicForm: (new_record) ->
    # client side validation
    HandyworksApp.validate "form.clinic",
      rules:
        'clinic[contact_attributes][company_name]':
          required: true

    #---------------------------------------------------------------------------
    # initialize the address fields for a clinic. if this is a new record, then
    # we default to referencing the first clinic's address by default and the
    # user can select 'New Address' to expose the address fields.
    #
    # for an existing record, we will either hide or show the address fields
    # depending if the location_clinic_id is set. if the location_clinic_id
    # is not set, the "blank" option for the selection list will read 
    # 'Current Address' for existing records.
    #---------------------------------------------------------------------------
    if new_record
      # select the first clinic in the list by default.
      jQuery('.clinic_address').hide()
      jQuery('select#clinic_location_clinic_id option:nth(0)').text('New Address')
      v = jQuery('select#clinic_location_clinic_id option:nth(1)').attr('value')
      jQuery('select#clinic_location_clinic_id').val(v)
    else
      c = jQuery('select#clinic_location_clinic_id').val()
      if c > 0
        jQuery('.clinic_address').hide()
        jQuery('select#clinic_location_clinic_id option:nth(0)').text('New Address')
      else
        jQuery('select#clinic_location_clinic_id option:nth(0)').text('Unique Address')

    #---------------------------------------------------------------------------
    # change logic for the clinic address field
    #---------------------------------------------------------------------------
    jQuery('select#clinic_location_clinic_id').change ->
      v = jQuery('select#clinic_location_clinic_id').val()
      if v > 0
        jQuery('.clinic_address').hide()
      else
        jQuery('.clinic_address').show()
