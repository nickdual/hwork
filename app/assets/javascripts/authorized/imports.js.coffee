_.extend window.HandyworksApp,

  importFormValidation: ->
    # client side validation
    HandyworksApp.validate "form.import",
      rules:
        'import[data]':
          required: true

  importGroupFormValidation: ->
    # basic client-side validation logic for diagnosis codes
    HandyworksApp.validate "form.import_group",
      rules:
        'import_group[import_group_type]':
          required: true
