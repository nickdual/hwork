#===============================================================================
# Popups
#  a "module" of functionality wrapped around the fancybox plugin to generate
#  a rotation of popup display windows.
#===============================================================================
_.extend window.HandyworksApp,
Popup:
  _first_init: true
  _push_active: false # only one popup opening at a time.
  _maxLevel: 5
  _init: ->
    p = HandyworksApp.Popup
    p._currentLevel= 0
    p._options= [{},{},{},{},{},{}] # include one more level (0) than you'll use
    p._first_init = false
    console.log("Handyworksapp Popup _init",p._currentLevel)
  # NOTE : while the width and height are passed into init() as variables, this is
  # only called once, when the first popup opens, after that the width and height
  # will remain static.
  init: (w_width = jQuery(window).width(), w_height = jQuery(window).height()) ->
    p = HandyworksApp.Popup
    p._init()
    console.log("HandyworksApp Popup", p._currentLevel)
    jQuery('#popups').hide()
    jQuery('.popup-trigger').fancybox
      openEffect: 'elastic'
      closeEffect: 'elastic'
      nextEffect: 'elastic'
      prevEffect: 'elastic'
      maxWidth: 1600
      maxHeight: 1600
      minWidth: 450
      modal: true
      openMethod: 'zoomIn'
      nextMethod: 'zoomIn'
      #----------------------------------------------------------------------
      # The following register generic callbacks which will then call the 
      # appropriate stack levels callbacks.
      #----------------------------------------------------------------------
      onCancel: ->
        p._onCancel()
      beforeLoad: ->
        p._beforeLoad()
      afterLoad: ->
        p._afterLoad()
      beforeShow: ->
        p._beforeShow()
      afterShow: ->
        HandyworksApp.Configuration.dontDisplayBusy = true
        # FIXME : while this approach does improve the rendering of dynamic content in the fancybox, it causes flashing on form opens
        jQuery.fancybox.update() # improve rendering of dynamic content in new fancybox popup
        jQuery('.fancybox-wrap').show() # this is part of preventing a flash on the screen when resizing the window by .update()
        p._afterShow()
      beforeClose: ->
        p._beforeClose()
      afterClose: ->
        p._afterClose()
        p._init()
        HandyworksApp.Configuration.dontDisplayBusy = false
        return true # or leave the background overlay darkenened
      onUpdate: ->
        p._onUpdate()
      onPlayStart: ->
        p._onPlayStart()
      onPlayEnd: ->
        p._onPlayEnd()
  #---------------------------------------------------------------------------
  # register a fancybox trigger callback to open the window in the future 
  # allows for more thorough configuration 
  #---------------------------------------------------------------------------
  add: (element, options) ->
    console.log("Handyworks App Popup add",element)
    element.click (event) ->
      event.preventDefault()
      console.log("--> popup add click trigger",event)
      href = element.attr('href')
      HandyworksApp.Popup.push href, options
  #---------------------------------------------------------------------------
  # This method is called when the new window's content is available and 
  # ready to be displayed either by triggering (click) or by rotating (next)
  # Click is only used for the first window.
  #
  # In the event multiple windows are opened simultaneously, we might get 
  # response back for level 2 before level 1, so we also need to check the
  # fancybox is already open before using .next() method.
  #---------------------------------------------------------------------------
  _pushDone: (current_level) ->
    p = HandyworksApp.Popup
    p._push_active = false
    if current_level > 1 and jQuery('.fancybox-inner').length > 0
     jQuery.fancybox.next()
    else
     jQuery("#popup-trigger-#{current_level}").click()
    p._options[current_level - 1].afterPush() if p._options[current_level - 1].afterPush

  #---------------------------------------------------------------------------
  # Method called to open a new window. It pushes the new view onto the stack
  #---------------------------------------------------------------------------
  push: (href, options={}) ->
    p = HandyworksApp.Popup
    unless p._push_active
      p._push_active = true
      p.init(jQuery(window).width(),jQuery(window).height()) if p._first_init
      if not p._options[p._currentLevel].beforePush or p._options[p._currentLevel].beforePush()
        p._currentLevel++
        p._options[p._currentLevel] = options
        console.log("pushing popup", p._currentLevel)
        # call the current views push callbacks
        if href.match(/^#/)
          if not p._options[p._currentLevel].beforeLoad or p._options[p._currentLevel].beforeLoad()
            jQuery("#popup#{p._currentLevel}").html(jQuery(href))
            if not p._options[p._currentLevel].afterLoad or p._options[p._currentLevel].afterLoad()
              p._pushDone(p._currentLevel)
        else
          if not p._options[p._currentLevel].beforeLoad or p._options[p._currentLevel].beforeLoad()
            c_level = p._currentLevel
            jQuery.ajax
              url : href
              data :
                popup : "fb" # this is used internally to affect page rendering in popups
              success: (data, textStatus) ->
                console.log('successfully retrieved ajax content')
                jQuery("#popup#{c_level}").html(data)
                if not p._options[c_level].afterLoad or p._options[c_level].afterLoad()
                  p._pushDone(c_level)
              error: (jqXHR, textStatus, errorThrown) ->
                console.log('popup load failed ajax error',errorThrown)
                p._push_active = false
                p._currentLevel--
    
  #---------------------------------------------------------------------------
  # Pops the current view off the stack and returns the user to the default
  # view if there are no more views left on the stack.
  #---------------------------------------------------------------------------
  pop: ->
    p = HandyworksApp.Popup
    console.log("popping popup", p._currentLevel)
    if not p._options[p._currentLevel].beforeClose or p._options[p._currentLevel].beforeClose()
      if p._currentLevel > 1
        jQuery.fancybox.prev()
        #jQuery("#popup#{p._currentLevel}").empty()
        p._options[p._currentLevel].afterClose() if p._options[p._currentLevel].afterClose
        p._currentLevel--
        return false
      else
        # in this scenario, just let fancybox cleanup and use the standard afterClose
        # callback mechanism for afterClose
        return true
    return false

  #---------------------------------------------------------------------------
  # OPTIMIZE : not sure when the fancybox.onCancel callback is used, this 
  #            might not be needed.
  #---------------------------------------------------------------------------
  _onCancel: ->
    p = HandyworksApp.Popup
    console.log("popping onCancel", p._currentLevel)
    if not p._options[p._currentLevel].onCancel or p._options[p._currentLevel].onCancel()
      return true
    return false
  #---------------------------------------------------------------------------
  # TODO : remove unsed callbacks from the fancybox stack implementation
  #---------------------------------------------------------------------------
  _beforeLoad: ->
    p = HandyworksApp.Popup
    console.log("popping beforeLoad", p._currentLevel)
  #---------------------------------------------------------------------------
  #---------------------------------------------------------------------------
  _afterLoad: ->
    p = HandyworksApp.Popup
    console.log("popping afterLoad", p._currentLevel)
  #---------------------------------------------------------------------------
  #---------------------------------------------------------------------------
  _beforeShow: ->
    p = HandyworksApp.Popup
    console.log("popping beforeShow", p._currentLevel)
    p._options[p._currentLevel].beforeShow() if p._options[p._currentLevel].beforeShow
  #---------------------------------------------------------------------------
  #---------------------------------------------------------------------------
  _afterShow: ->
    p = HandyworksApp.Popup
    console.log("popping afterShow", p._currentLevel)
    p._options[p._currentLevel].afterShow() if p._options[p._currentLevel].afterShow
  #---------------------------------------------------------------------------
  #---------------------------------------------------------------------------
  _beforeClose: ->
    p = HandyworksApp.Popup
    console.log("popping beforeClose", p._currentLevel)
    return p.pop()
  #---------------------------------------------------------------------------
  #---------------------------------------------------------------------------
  _afterClose: ->
    p = HandyworksApp.Popup
    console.log("popping afterClose", p._currentLevel)
    p._options[p._currentLevel].afterClose() if p._options[p._currentLevel].afterClose
  #---------------------------------------------------------------------------
  #---------------------------------------------------------------------------
  _onUpdate: ->
    p = HandyworksApp.Popup
    console.log("popping onUpdate", p._currentLevel)
    if not p._options[p._currentLevel].onUpdate or p._options[p._currentLevel].onUpdate()
      return true
    return false
  #---------------------------------------------------------------------------
  #---------------------------------------------------------------------------
  _onPlayStart: ->
    p = HandyworksApp.Popup
    console.log("popping onPlayStart", p._currentLevel)
    p._options[p._currentLevel].onPlayStart() if p._options[p._currentLevel].onPlayStart
  #---------------------------------------------------------------------------
  #---------------------------------------------------------------------------
  _onPlayEnd: ->
    p = HandyworksApp.Popup
    console.log("popping onPlayEnd", p._currentLevel)
    p._options[p._currentLevel].onPlayEnd() if p._options[p._currentLevel].onPlayEnd


