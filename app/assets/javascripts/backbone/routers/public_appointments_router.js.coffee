window.HandyworksApp.Routers.PublicAppointmentsRouter = Support.SwappingRouter.extend
  routes:
    "": "new"
    "appts/:account_id": "new"
    "clinics/:clinic_id": "selectLocation"
    "services/:service_id": "selectService"
    "providers/:provider_id": "selectProvider"

  new: ->
    @appointment = new HandyworksApp.Models.PublicAppointment()
    pav = new HandyworksApp.Views.PublicAppointment
      model: @appointment
    jQuery('#public_appointments').html(pav.render().$el)

  selectLocation: (clinic_id) ->
    console.info("in router selectLocation",clinic_id)
    HandyworksApp.Data.CurrentClinic = HandyworksApp.Data.Clinics.get(clinic_id)
    @appointment.selectLocation(clinic_id)

  selectService: (service_id) ->
    console.info("in router selectService", service_id)
    @appointment.selectService(service_id)

  selectProvider: (provider_id) ->
    console.info("in router selectProvider", provider_id)
    @appointment.selectProvider(provider_id)
