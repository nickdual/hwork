#------------------------------------------------------------------------------
# A single common router for all the Handyworks Backbone Apps. The only shared
# route should be *index* which then will check the @mode to determine which
# action callback to make for the default route.
#------------------------------------------------------------------------------
window.HandyworksApp.Router = Support.SwappingRouter.extend
  routes:
    ''         : 'index'

  #-----------------------------------------------------------------------------
  # initialize the schedule backbone application
  #-----------------------------------------------------------------------------
  schedule_initialize: (container, data = {}) ->
    console.info('starting schedules view')
    @main.renderSchedule(container, data)

  #-----------------------------------------------------------------------------
  # initialize the appointments backbone application
  #-----------------------------------------------------------------------------
  appointment_initialize: (container, data = {}) ->
    console.info('starting appointments view')
    @main.renderAppointments(container, data)

  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  navbar_initialize: ->
    console.info('starting navbar initialize')

  #-----------------------------------------------------------------------------
  # default route determined based on the current mode of the router
  #-----------------------------------------------------------------------------
  index: ->
    unless @main
      console.info('index route, initialize main view')
      @main = new HandyworksApp.Views.Main
        el: @el
      @swap(@main)

  #-----------------------------------------------------------------------------
  # base route for all appointment calendar views.
  #-----------------------------------------------------------------------------
  root: ->
    console.log('in appointments router root')

