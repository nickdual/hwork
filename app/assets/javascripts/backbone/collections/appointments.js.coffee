jQuery ->
#===============================================================================
#
#===============================================================================
  window.HandyworksApp.Collections.Appointments = Backbone.Collection.extend
    model: HandyworksApp.Models.Appointment
    url: "#{root_path}appointments"

    #-----------------------------------------------------------------------------
    # convert a calendar event to the rails event model
    #-----------------------------------------------------------------------------
    toEvent: (calEvent) ->
      console.log('in toEvent',calEvent)
      defaults =
        title: 'Appointment'
      attributes = HandyworksApp.toEvent(calEvent,defaults)


    #-----------------------------------------------------------------------------
    # Called to save the initial appointment to the server. Data based on the 
    # current state of the user session as well as the calendar location
    # clicked. Its going to be loading the current_patient info by default
    #
    # if the new appointment was created by clicking somewhere in the calendar,
    # then the default duration will be derived from the appointment type for
    # the assigned room. if the appointment was created by dragging, then it
    # will use the dragged time interval.
    #-----------------------------------------------------------------------------
    createAppointment: (data) ->
      console.info('in createAppointment',data)
      HandyworksApp.Data.refreshBlock = true
      attributes =
        room_id: data.room_id
        provider_id: data.provider_id
        event: @toEvent(data.event)
      appointment = new HandyworksApp.Models.Appointment(attributes)
      appointment.loadPatientInfo() unless appointment.patient
      # set the typical duration for this appointment based on appointment type 
      if appointment.getAppointmentType()? and not data.event?.drag
        appointment.set({'event.duration': appointment.getAppointmentType().get('duration')})
      options =
        success: (model,response) ->
          console.info('createAppointment success',model,response)
          HandyworksApp.Data.refreshBlock = false
          data.event.id = model.id
          HandyworksApp.Data.Appointments.add(model)
          HandyworksApp.Data.AppointmentInfo.set({ appointment: model })
          jQuery('#calendar').weekCalendar("refresh")
          jQuery('div.wc-cal-event.ui-resizable').on('resize',(e)->
            e.stopPropagation())
        error: (model,response) ->
          console.error('createAppointment failed', model, response,data)
          HandyworksApp.Data.refreshBlock = false
          # TODO : try using a model.clear() call here and subscribing to that event.
          model.collection.trigger('createAppointmentError',model,response.responseText)
      appointment.save({}, options)

    #-----------------------------------------------------------------------------
    # Called when a schedule event is resized or drag'n'dropped to a new time.
    # Only process changes to send to the server if the given newCalEvent and
    # oldCalEvent objects differ.
    #
    # Successful update will also update the current AppointmentInfo display.
    #-----------------------------------------------------------------------------
    updateAppointment: (newCalEvent,oldCalEvent) ->
      console.info('in updateAppointment',newCalEvent,oldCalEvent)
      unless _.isEqual(newCalEvent, oldCalEvent)
        HandyworksApp.Data.refreshBlock = true
        model = @get(newCalEvent.id)
        attributes =
          provider_id: newCalEvent.provider_id
          room_id: newCalEvent.room_id
          contact_id: newCalEvent.contact_id
          event: @toEvent(newCalEvent)
        options =
          success: (model,response) ->
            console.info('updateAppointment success',model,newCalEvent, response)
            HandyworksApp.Data.refreshBlock = false
            model._objectify() # pushes changes to the associated object attribute.
            HandyworksApp.Data.AppointmentInfo.set({appointment: model})
          error: (model,response) ->
            console.error('updateAppointment failed', model, response)
            HandyworksApp.Data.refreshBlock = false
            model.collection.trigger('updateAppointmentError',oldCalEvent,response.responseText)
        console.info('--> updating',model,attributes)
        model.save attributes, options
    #-----------------------------------------------------------------------------
    # called when the user clicks the (x) delete button on a calendar 
    # appointment
    #
    # The patient from the selected appointment will become the current active
    # patient to facilitate an appointment rescheduling workflow.
    #-----------------------------------------------------------------------------
    deleteAppointment: (calEvent) ->
      console.info('in deleteAppointment',calEvent)
      HandyworksApp.Data.refreshBlock = true
      model = @get(calEvent.id)
      HandyworksApp.Data.CurrentPatient.set({patient: model.getPatient()})
      options =
        success: (model,response) ->
          console.info('deleteAppointment success',model,calEvent)
          HandyworksApp.Data.refreshBlock = false
        error: (model,response) ->
          console.error('deleteAppointment failed', model, response)
          HandyworksApp.Data.refreshBlock = false
          model.collection.trigger('deleteAppointmentError',calEvent,response.responseText)
      model.destroy options

