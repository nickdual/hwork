jQuery ->
#===============================================================================
#
#===============================================================================
  window.HandyworksApp.Collections.Rooms = Backbone.Collection.extend
    model: HandyworksApp.Models.Room

    #---------------------------------------------------------------------------
    # return the "first" available room for the given provider
    #---------------------------------------------------------------------------
    getRoomForProvider: (provider) ->
      room_options = _.filter @models, (room) ->
        room.get('provider_id') == provider.id
      # TODO : add some more logic here which would exclude busy rooms from provider room list
      _.first room_options

    #---------------------------------------------------------------------------
    # ensure that at least one room remains enabled, you can't disable 
    # them all.
    #---------------------------------------------------------------------------
    atLeastOne: ->
      console.log('in at least one room')
      count = 0
      @forEach (provider) ->
        if provider.get('enabled')
          count = count + 1
      console.log(" --> How many rooms left?",count)
      if count > 1
        return true
      return false

