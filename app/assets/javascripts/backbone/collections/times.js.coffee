jQuery ->
  window.HandyworksApp.Collections.Times = Backbone.Collection.extend
    model: HandyworksApp.Models.Time
    url: "#{root_path}appts/#{HandyworksApp.Data.account_id}/times"

    setData: (data) ->
      @data = data

    getData: ->
      @data

    #-------------------------------------------------------------------------------
    # Start with any models that are currently loaded for this collection of
    # appointment times after initiating a request to the server for times.
    #-------------------------------------------------------------------------------
    fetchTimes: ->
      @fetch({data: @data})
      return @models
