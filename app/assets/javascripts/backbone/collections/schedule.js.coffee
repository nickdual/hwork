jQuery ->
#===============================================================================
#
#===============================================================================
  window.HandyworksApp.Collections.Schedule = Backbone.Collection.extend
    model: HandyworksApp.Models.FreeBusy

    #---------------------------------------------------------------------------
    # keep this list sorted by start / end times.
    #---------------------------------------------------------------------------
    comparator: (model1, model2) ->
      s1 = model1.start
      s2 = model2.start
      cmp = s1.compareTo(s2)
      return cmp unless cmp == 0
      e1 = model1.end
      e2 = model2.end
      return e1.compareTo(e2)

    #---------------------------------------------------------------------------
    # Because of the mode we're using the calendar in, this method needs to 
    # combine the unavailable periods with the free periods and only return
    # a list of free periods (b/c) the default is busy, so if we produce a list
    # that contains both free and busy (which might work as expected for a single
    # user) when its used for rooms it will make the room look unavailable if 
    # one of the providers has marked themselves as unavailable for that time
    # period.
    #---------------------------------------------------------------------------
    getFreeBusys: (start, end) ->
      #console.log("in colleciton schedule getFreeBusys", @id, start, end)
      result = []
      available = []
      unavailable = []
      @forEach (event) ->
        if event.inWindow(start, end)
          if (not event.get('free'))
            unavailable.push(event)
          else
            available.push(event)
      for time in available
        do (time) ->
          result = result.concat time.slice(unavailable)
      result

    setSchedulable: (model) ->
      @schedulable = model


