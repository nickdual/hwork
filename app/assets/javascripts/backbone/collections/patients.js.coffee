jQuery ->
#===============================================================================
#
#===============================================================================
  window.HandyworksApp.Collections.Patients = Backbone.Collection.extend
    model: HandyworksApp.Models.Patient
    url: "#{root_path}interaction/patients"

    #---------------------------------------------------------------------------
    # this method is a wrapper around the add / get methods that will either
    # return and existing patient model, or will create a new one. its assumed
    # this patient data originates from the server, so this method is not a way
    # to create a new patient, that should be done with the create method.
    #
    # this allows us to take a lassiez-fair approach to loading patients, we'll
    # load the current_patient from the _layout.html.haml page, then we'll 
    # also load the patient data transmitted with each existing appointment,
    # but that should only be exactly as much data as we need.
    #---------------------------------------------------------------------------
    load: (data) ->
      n_patient = @get(data.id)
      unless n_patient
        n_patient = new HandyworksApp.Models.Patient(data)
        @add(n_patient)
      n_patient

    comparator: (patient) ->
      patient.mru

    #---------------------------------------------------------------------------
    # Return the specified count of the most recently used patients. This is
    # used to populate quick pick lists such as the Appointment Info patient
    # selections.
    #---------------------------------------------------------------------------
    getMru: (count) ->
      @models.slice(0,count)

    #---------------------------------------------------------------------------
    # This function will (a) keep inserted patients ordered according to last
    # and first name and (b) it can be called through the global Patients list
    # to sort subsets of patients via underscore's sortBy method.
    #---------------------------------------------------------------------------
    comparator: (patient) ->
     return patient.get('contact.last_name') + ' ' + patient.get('contact.first_name')

    #---------------------------------------------------------------------------
    # filter the list of patients based on a matching string.
    # mode in ['contains','starts_with'] with starts_with being the default.
    #---------------------------------------------------------------------------
    match: (term, mode) ->
      if mode == 'contains'
        m = $.ui.autocomplete.escapeRegex( term )
      else
        m = "^" + $.ui.autocomplete.escapeRegex( term )
      matcher = new RegExp( m, "i" )
      @filter (patient) ->
        matcher.test(patient.listName())

