jQuery ->
#===============================================================================
# Events used with scheduling interface.
#===============================================================================
  window.HandyworksApp.Collections.Events = Backbone.Collection.extend
    model: HandyworksApp.Models.Event
    url: "#{root_path}setup/events"

    #-----------------------------------------------------------------------------
    # options will contain the id of the eventable resource which needs to be 
    # referenced when creating any new events in the collection.
    #-----------------------------------------------------------------------------
    initialize: (models,options) ->
      console.info('Events.initialize',models,options)
      @options = options

    #-----------------------------------------------------------------------------
    # convert a rails event to a calendar event
    #-----------------------------------------------------------------------------
    toCalEvent: (event) ->
      attributes =
        id: event.id
        title: event.get('title')
        start: event.start()
        end: event.end()
        eventable_id: event.get('eventable_id')
        eventable_type: event.get('eventable_type')
      # optional fields
      notes = event.get('notes')
      free = event.get('free')
      clinics = event.get('clinics')
      attributes.notes = notes if notes?
      attributes.free = free if free?
      attributes.clinics = clinics if clinics?
      return attributes

    #-----------------------------------------------------------------------------
    # convert a calendar event to the rails event model
    #-----------------------------------------------------------------------------
    toEvent: (calEvent) ->
      console.log('in toEvent',calEvent)
      defaults =
        title: 'Schedule Event'
        eventable_id: @options.eventable_id
        eventable_type: @options.eventable_type
      attributes = HandyworksApp.toEvent(calEvent,defaults)

    #-----------------------------------------------------------------------------
    # translate the active record events into the much simpler calEvents.
    #   returns array of events for schedule calendar
    #-----------------------------------------------------------------------------
    getCalEvents: ->
      @map (event) =>
        @toCalEvent(event)

    deleteCalEvent: (calEvent) ->
      console.info('in deleteCalEvent',calEvent)
      model = @get(calEvent.id)
      options =
        success: (model,response) ->
          console.info('deleteCalEvent success',model,calEvent)
        error: (model,response) ->
          console.error('deleteCalEvent failed', model, response)
          model.collection.trigger('deleteEventError',calEvent)
      model.destroy options

    #-----------------------------------------------------------------------------
    # Called when a schedule event is resized or drag'n'dropped to a new time.
    #-----------------------------------------------------------------------------
    updateCalEvent: (newCalEvent,oldCalEvent) ->
      console.info('in updateCalEvent',newCalEvent,oldCalEvent)
      attributes = @toEvent(newCalEvent)
      model = @get(newCalEvent.id)
      options =
        success: (model,response) ->
          console.info('updateCalEvent success',model,newCalEvent)
        error: (model,response) ->
          console.error('updateCalEvent failed', model, response)
          model.collection.trigger('updateEventError',oldCalEvent)
      console.info('--> updating',model,attributes)
      model.save attributes, options

    #-----------------------------------------------------------------------------
    # Transmit the event location update to the server
    #   * keep the calEvent object in current state to revert the calendar
    #     in the event the request to the server fails.
    #-----------------------------------------------------------------------------
    updateEventLocation: (calEvent,clinics) ->
      console.info('in updateEventLocation',calEvent,clinics)
      attributes = @toEvent(calEvent)
      attributes.clinics = clinics
      model = @get(calEvent.id)
      options =
        success: (model,response) ->
          console.info('updateEventLocation success',model,calEvent)
          model.collection.trigger('updateEventLocationSuccess',calEvent,clinics)
        error: (model,response) ->
          console.error('updateEventLocation failed', model, response)
          model.collection.trigger('updateEventLocationError',calEvent,clinics)
      console.info('--> updating',model,attributes)
      model.save attributes, options

    createCalEvent: (newCalEvent) ->
      console.info('in createCalEvent',newCalEvent.id,newCalEvent)
      attributes = @toEvent(newCalEvent)
      options =
        success: (model,response) ->
          console.info('createCalEvent success',model,newCalEvent)
          newCalEvent.id = model.id
          newCalEvent.eventable_id = model.get('eventable_id')
          newCalEvent.eventable_type = model.get('eventable_type')
        error: (model,response) ->
          console.error('createCalEvent failed', model, response)
          # TODO : try using a model.clear() call here and subscribing to that event.
          model.collection.trigger('createEventError',newCalEvent.id)
      @create attributes, options

    createExceptionEvent: (formEvent) ->
      console.log('in createExceptionEvent',formEvent)
      attributes = @toEvent(formEvent)
      options =
        success: (model,response) ->
          console.info('createExceptionEvent success',model,formEvent)
        error: (model,response) ->
          console.error('createExceptionEvent failed',model,response)
      @create attributes, options
