window.HandyworksApp.Models.Event = Backbone.Model.extend
  #-----------------------------------------------------------------------------
  # determine the actual Date for the start of this event.
  #-----------------------------------------------------------------------------
  start: ->
    HandyworksApp.calculate_time(@get('start_date'),@get('start_time'))

  #-----------------------------------------------------------------------------
  # calculate the end of the event based on duration.
  #-----------------------------------------------------------------------------
  end: ->
    HandyworksApp.calculate_time(@get('start_date'),@get('start_time'),@get('duration'))

  remove: ->



