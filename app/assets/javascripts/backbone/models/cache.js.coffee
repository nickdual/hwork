#===============================================================================
# Used for storing cache'd data that's not specifically resource related.
# TODO : shift to using HTML local data store, session store or sqlite once browser compatibility is better established.
#===============================================================================
window.HandyworksApp.Models.Cache = Backbone.NestedModel.extend

  initialize: ->
    console.log('initialize cache')
    HandyworksApp.dispatcher.on "patientchange", ->
      HandyworksApp.Data.Cache.flush('patient_last_name_letters')

  flush: (pattern) ->
    console.log('in cache flush',pattern)
    try
      @unset(pattern) if @has(pattern)
    catch e
      # FIXME : an error was getting thrown here which was preventing the SAVE/close of a patient edit window
      #   (a) if it was opened from the navbar link, and (b) you had searched for the patient using patient search feature
      #   its possible there's a problem with the caching data, but seems like it might be a bug in one of the backbone
      #   extensions classes (backbone.nested-model).
      console.info('--> error in cache flus',e)
