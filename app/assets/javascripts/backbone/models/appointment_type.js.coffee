#===============================================================================
#
#===============================================================================
window.HandyworksApp.Models.AppointmentType = Backbone.NestedModel.extend
  defaults:
    type: 'AppointmentType'

  urlRoot: "#{root_path}schedule/appointment_types"

  initialize: (options={}) ->
    console.log("initializing model appointment type",options)

  #-----------------------------------------------------------------------------
  # The providers attribute which is being sent to the client just includes
  # the id attribute, but its embedded in an object. So this will produce
  # the
  #-----------------------------------------------------------------------------
  getProviders: ->
    providers = @get('providers')
    _.map providers, (id) ->
      HandyworksApp.Data.Providers.get(id)

  getProviderIds: ->
    providers = @get('providers')
    if providers and providers.length > 0
      return providers
    else
      HandyworksApp.showAlert("Appointment Type #{@get('name')} is not assigned to any providers")
      HandyworksApp.Data.Providers.map (provider) ->
        provider.id
