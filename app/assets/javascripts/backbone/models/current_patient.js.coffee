#===============================================================================
# This model is the container for the current patient to be displayed in the 
# navbar. Wrapping it in a model allows for us
# to use conventional Backbone.js techniques to dynamically refresh this 
# view on change to the nested appointment.
#===============================================================================
window.HandyworksApp.Models.CurrentPatient = Backbone.NestedModel.extend
  defaults:
    type: 'CurrentPatient'
    
  initialize: (options={}) ->
    console.log("initializing model current_patient",options)

  isCurrent: (patient) ->
    cur_patient = @get('patient')
    if ( patient? and patient.id == cur_patient.id )
      return true
    return false

  update: (patient_id) ->
    console.log("in CurrentPatient update",patient_id)
    if patient_id > 0 and patient_id != @get('patient.id')
      patient = HandyworksApp.Data.Patients.get(patient_id)
      if patient
        @set({patient: patient})
        patient.touch()
      else
        patient = new HandyworksApp.Models.Patient({id: patient_id})
        patient.loadPatient(@_update)

  _update: (patient) ->
    console.log("in _update CurrentPatient (after load)")
    HandyworksApp.Data.CurrentPatient.update(patient.id)

