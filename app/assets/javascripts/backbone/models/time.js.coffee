window.HandyworksApp.Models.Time = Backbone.NestedModel.extend
  defaults:
    type: 'Time'

  initialize: (options={}) ->
    console.log("initializing model time", options)

  display: ->
    new Date(@get('time')).toString(HandyworksApp.Configuration.timeFormatStr)
