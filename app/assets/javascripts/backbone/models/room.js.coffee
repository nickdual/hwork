#===============================================================================
#
#===============================================================================
window.HandyworksApp.Models.Room = Backbone.NestedModel.extend
  urlRoot: "#{root_path}schedule/rooms"
  defaults:
    type: 'Room'
    enabled: true

  initialize: (options={}) ->
    console.log("initializing model room",options)
      
  getFreeBusys: (start, end) ->
    ##if start and end
    ##  console.log("in room getFreeBusys",start.toString("MM/dd/yyyy HH:mm"),end.toString("MM/dd/yyyy HH:mm"),@get('name'))
    result = []
    providers = @getProviders()
    if providers?
      _.each providers, (provider) ->
        result = result.concat provider.getFreeBusys(start, end)
      # need to update the clinic schedule reference to point to this room
      model_id = @id
      model_type = @get('type')
      for event in result
        event.model_id = model_id
        event.model_type = model_type
    result

  #-----------------------------------------------------------------------------
  # join the Room model to its list of associated provider models
  # This originates from the server side association provider_precedences
  # which is passed through the providers_seq property.
  #-----------------------------------------------------------------------------
  getProviders: ->
    result = []
    if @has('providers_seq')
      _.map @get('providers_seq'), (ps) ->
        result.push HandyworksApp.Data.Providers.get(ps.provider_id)
    result

  setClinics: (clinics) ->
    @clinics = clinics

  #-----------------------------------------------------------------------------
  # This method is called when a user clicks on the appointment calendar 
  # selecting an appointment time and a room (by room view)
  #-----------------------------------------------------------------------------
  initializeAppointment: (data) ->
    # determine the appointment type for the room, or if not configured, then
    # show a message to the user
    ati = @get('appointment_type_id')
    unless ati?
      HandyworksApp.showAlert("Room #{@get('name')} does not have an appointment type configured")
      data.appointment_type_id = HandyworksApp.Data.AppointmentTypes.first().id
    else
      data.appointment_type_id = ati

    appointment_type = HandyworksApp.Data.AppointmentTypes.get(data.appointment_type_id)
    appointment_type_provider_ids = appointment_type.getProviderIds()

    # now look for the best provider choice for this appointment initially
    # at this point we don't know the patient, so the best logic will look at
    # providers for this room (interesected) providers for this appointment_type
    # out of the result, the first provider in the order of precedence for this
    # room that is available will be selected.
    provider_seq = @get('providers_seq')
    unless provider_seq.length > 0
      HandyworksApp.showAlert("Room #{@get('name')} does not have any active providers configured")
      provider_ids = HandyworksApp.Data.Providers.map (provider) ->
        provider.id
    else
      provider_ids = _.map provider_seq, (ps) ->
        ps.provider_id

    eligible_provider_ids = _.intersection provider_ids, appointment_type_provider_ids
    if eligible_provider_ids.length > 0
      # now look through this list to find the first available provider
      # use the room's provider_ids array to derive the sequence intended for use
      # with providers
      _.each provider_ids, (pid) ->
        if not data.provider_id and _.indexOf(eligible_provider_ids, pid) != -1
          provider = HandyworksApp.Data.Providers.get(pid)
          data.provider_id = pid if provider.isAvailable(data.event.start, data.event.end)
      HandyworksApp.showAlert("No Provider Available for the Appointment Type for this Room") unless data.provider_id
    else
      HandyworksApp.showError("No Provider Available for this Appointment")

    return data
    ## get this list of free busy's
    #freeBusys = room.getFreeBusys(calEvent.start, calEvent.end)
    #freeBusyProviders = _.map freeBusys, (freeBusy) ->
    #  if freeBusy.free
    #    freeBusy.provider_id
    #  else
    #    0 # this freeBusy is not for  free provider
    #    # TODO : might have to actually remove the provider_id from consideration
    #a_provider = _.find room.getProviders(), (provider) ->
    #  _.include freeBusyProviders, provider.id
    #appointment.provider_id = a_provider.id if a_provider




