window.HandyworksApp.Models.PublicAppointment = Backbone.NestedModel.extend
  urlRoot: "#{root_path}appointments"

  workflow:
    initial: 'new'
    events: [
      { name: 'locate', from: 'new', to: 'located' }
      { name: 'select', from: 'located', to: 'selected' }
      { name: 'assign', from: 'selected', to: 'assigned' }
      { name: 'schedule', from: 'assigned', to: 'scheduled' }
    ]

  initialize: (options={}) ->
    _.extend @, new Backbone.Workflow(@)
    #memento = new Backbone.Memento(@)
    #_.extend(@, memento)
    @bind "transition:to:located", @autoServiceSelection
    @bind "transition:to:selected", @autoProviderSelection
    if HandyworksApp.Data.CurrentClinic
      @set({clinic_id: HandyworksApp.Data.CurrentClinic.id, workflow_state: 'located'})
      @autoServiceSelection()

  selectLocation: (clinic_id) ->
    @triggerEvent('locate')
    @set({clinic_id: clinic_id})

  getServices: ->
    HandyworksApp.Data.AppointmentTypes.models

  getAppointmentType: ->
    HandyworksApp.Data.AppointmentTypes.get(@get('appointment_type_id')) if @has('appointment_type_id')

  autoServiceSelection: ->
    services = @getServices()
    if services.length == 1
      @selectService(services[0].id)

  selectService: (service_id) ->
    @set({appointment_type_id: service_id})
    @triggerEvent('select')

  getProviders: ->
    if @has('appointment_type_id')
      return @getAppointmentType().getProviders()
    return HandyworksApp.Data.Providers.models

  selectProvider: (provider_id) ->
    @set({provider_id: provider_id})
    @triggerEvent('assign')

  autoProviderSelection: ->
    providers = @getProviders()
    if providers and providers.length == 1
      @selectProvider(providers[0].id)

  fetchTimes: ->
    data =
      clinic_id: @get('clinic_id')
      appointment_type_id: @get('appointment_type_id')
      provider_id: @get('provider_id')
      start_time: new Date().getTime()
    HandyworksApp.Data.Times.setData(data)
    HandyworksApp.Data.Times.fetchTimes()

  #----------------------------------------------------------------------------
  # indicate which step should currently be rendered back to the user.
  #----------------------------------------------------------------------------
  getStep: ->
    state = @get('workflow_state')
    return 'location' if state == 'new'
    return 'service' if state == 'located'
    return 'staff' if state == 'selected'
    return 'schedule' if state == 'assigned'
