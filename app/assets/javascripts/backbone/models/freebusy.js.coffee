#===============================================================================
#===============================================================================
window.HandyworksApp.Models.FreeBusy = Backbone.NestedModel.extend
  defaults:
    type: 'FreeBusy'

  initialize: (options={}) ->
    #console.log("initializing model free_busy",options)
    @start = HandyworksApp.calculate_time(@get('start_date'),@get('start_time'))
    @end = HandyworksApp.calculate_time(@get('start_date'),@get('start_time'),@get('duration'))

  getModel: ->
    @collection.schedulable if @collection?

  #-----------------------------------------------------------------------------
  # determine if this event is in the indicated window
  #  true if event.end > window.start and event.start < window.end
  #-----------------------------------------------------------------------------
  inWindow: (start=Date.today().addYears(-1), end=Date.today().addYears(1)) ->
    #console.log("in model free_busy inWindow",start,end)
    @end.compareTo(start) >= 0 and @start.compareTo(end) < 0

  getFreeBusy: ->
    #console.log("in model free_busy getFreeBusy")
    data =
      id: @id
      start: @start
      end: @end
      free: @get('free')
      title: @get('title')
      notes: @get('notes')
    model = @getModel()
    if model?
      data.model_id = model.id
      data.model_type = model.get('type')
    data
    
  #-----------------------------------------------------------------------------
  # Will take an array of busy events unavailable and slice the current event
  # into either none, one or many sub-events based on how the base timeblock
  # of availability is broken up by the array of unavailable times.
  #
  # OPTIMIZE : The process of subtracting the unavailable / blocked times is tedious, perhaps there is a better storage structure for this scheduled information that would make this particular function simpler. keeping in mind that  just moving this process to the server would not help, as it would not scale as well.
  #-----------------------------------------------------------------------------
  slice: (unavailable) ->
    result = []
    if unavailable and unavailable.length > 0 and @get('free')
      times = [{start: @start, end: @end}]
      for busy in unavailable
        do (busy) ->
          new_times = []
          for available in times
            do (available) ->
              bsae = busy.start.compareTo(available.end)
              beas = busy.end.compareTo(available.start)
              # check if the busy period overlaps this period at all
              if bsae < 0 and  beas > 0
                bsas = busy.start.compareTo(available.start)
                beae = busy.end.compareTo(available.end)
                #     |-------F-------|
                #          |------ B ------|
                if bsas > 0 and beae >= 0
                  new_times.push({start: available.start, end: busy.start })
                #     |-------F-------|
                #       |--- B ---|
                else if bsas >= 0 and beae < 0
                  new_times.push({start: available.start, end: busy.start })
                  new_times.push({start: busy.end, end: available.end })
                #     |-------F-------|
                #  |---- B ----|
                else if bsas <= 0 and beae < 0
                  new_times.push({start: busy.end, end: available.end })
                #     |-------F-------|
                #   |-------- B --------|
                #else if bsas <= 0 and beae >= 0
                  # no portion is avilable.
              else
                new_times.push(available)
          times = new_times
      # now create the array of freebusy elements
      if times and times.length > 0
        fb_base = @getFreeBusy()
        fb_list = _.map times, (time) -> 
          fb_new = _.clone(fb_base)
          fb_new.start = time.start
          fb_new.end = time.end
          return fb_new
        result.push(fb_list)
    else
      result.push(@getFreeBusy())
    result


