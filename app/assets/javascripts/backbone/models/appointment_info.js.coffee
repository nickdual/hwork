#===============================================================================
# This model is the container for the appointment to be displayed in the 
# lower portion of the controls pane. Wrapping it in a model allows for us
# to use conventional Backbone.js techniques to dynamically refresh this 
# view on change to the nested appointment.
#===============================================================================
window.HandyworksApp.Models.AppointmentInfo = Backbone.NestedModel.extend
  defaults:
    type: 'AppointmentInfo'
    
  initialize: (options={}) ->
    console.log("initializing model appointment_info",options)


