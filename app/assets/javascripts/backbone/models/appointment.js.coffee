#===============================================================================
# Uses the NestedModel extension to access the 'event' as a nested model.
#===============================================================================
window.HandyworksApp.Models.Appointment = Backbone.NestedModel.extend
  urlRoot: "#{root_path}appointments"
  defaults:
    type: 'Appointment'

  workflow:
    initial: 'new'
    states:
      new:
        events:
          confirm: { transitionsTo: 'confirmed' }

  initialize: (options={}) ->
    #console.log("initializing model appointment",options)
    _.extend @, new Backbone.Workflow(@)
    memento = new Backbone.Memento(@)
    _.extend(@, memento)
    @_objectify()

  _objectify: ->
    if @has('patient') and @get('patient')
      @setPatient(HandyworksApp.Data.Patients.load(@get('patient')))
    # OPTIMIZE : doing this to default the appointment type on create, there might be a better way
    if @has('room_id') and @get('room_id')
      @setRoom(HandyworksApp.Data.Rooms.get(@get('room_id')))

  #-----------------------------------------------------------------------------
  # determine the actual Date for the start of this event.
  #-----------------------------------------------------------------------------
  start: ->
    HandyworksApp.calculate_time(@get('event.start_date'),@get('event.start_time'))

  #-----------------------------------------------------------------------------
  # calculate the end of the event based on duration.
  #-----------------------------------------------------------------------------
  end: ->
    HandyworksApp.calculate_time(@get('event.start_date'),@get('event.start_time'),@get('event.duration'))

  #-----------------------------------------------------------------------------
  # convert a rails appointment to a calendar event
  #-----------------------------------------------------------------------------
  toCalEvent: ->
    attributes =
      id: @id
      title: @get('event.title')
      start: @start()
      end: @end()
      eventable_id: @get('event.eventable_id')
      eventable_type: @get('event.eventable_type')
    # optional fields
    notes = @get('event.notes')
    provider_id = @get('provider_id')
    room_id = @get('room_id')
    contact_id = @get('contact_id')
    appointment_type_id = @get('appointment_type_id')
    attributes.notes = notes if notes?
    attributes.provider_id = provider_id if provider_id?
    attributes.room_id = room_id if room_id?
    attributes.contact_id = contact_id if contact_id?
    attributes.appointment_type_id = appointment_type_id if appointment_type_id?
    return attributes
  #-----------------------------------------------------------------------------
  # convert a calendar event to the rails appointment resource
  #-----------------------------------------------------------------------------
  # toEvent: (calEvent) ->
  #   console.log('in toEvent')
  #   defaults =
  #     provider_id: calEvent.provider_id
  #     room_id: calEvent.room_id
  #     contact_id: calEvent.contact_id
  #   attributes = HandyworksApp.toEvent(calEvent,defaults)

  #-----------------------------------------------------------------------------
  # called to initialize the information for the current patient
  #  its first going to check the cached patient which was loaded when the
  #  the screen was initialized.
  #-----------------------------------------------------------------------------
  loadPatientInfo: (id=HandyworksApp.Data.current_patient_id) ->
    patient = HandyworksApp.Data.Patients.get(id)
    if patient?
      @setPatient(patient)
    else
      console.info("appointment patient data fetch")
      patient = new HandyworksApp.Models.Patient({id: id})
      patient.loadPatient()
      @setPatient(patient)

  #-----------------------------------------------------------------------------
  # pass some information back to the server to create a new patient record
  #-----------------------------------------------------------------------------
  newPatient: (first_name, last_name, phone) ->
    console.log("in appointment newPatient",first_name,last_name,phone)
    # these fields are a placeholder to update the appointment on the
    # screen once the save is completed.
    @patient = new HandyworksApp.Models.Patient
      'contact.name': first_name + ' ' + last_name
      'contact.phone': phone
    HandyworksApp.Data.CurrentPatient.set({patient: @patient})
    # these fields will be sent to the server side controller, and will
    # actually trigger the model creation
    @save
      new_first_name: first_name
      new_last_name: last_name
      new_phone: phone
      contact_id: 0
      'event.title': first_name + ' ' + last_name


  #-----------------------------------------------------------------------------
  # When an appointment's patient is set, we need to push the contact_id from
  # that patient to the appointment object, and the name from the patient to
  # the event.title.
  #-----------------------------------------------------------------------------
  setPatient: (patient) ->
    patient.setAppointment(@)
    @setProvider(patient.getProvider()) unless @getProvider()?
    @patient = patient
    @set({contact_id: patient.get('contact.id')})
    @set({'event.title': patient.get('contact.name')})

  #-----------------------------------------------------------------------------
  # returns the current Patient model, or if not present will return a new
  # Patient model.
  #-----------------------------------------------------------------------------
  getPatient: ->
    if @patient then @patient else new HandyworksApp.Models.Patient()

  #-----------------------------------------------------------------------------
  # See setRoom for a comment on this conditional check.
  #-----------------------------------------------------------------------------
  setProvider: (provider) ->
    if provider
      @set({provider_id: provider.id}) unless provider.id == @get('provider_id')
    else
      @unset('provider_id')

  getProvider: ->
    HandyworksApp.Data.Providers.get(@get('provider_id'))

  #-----------------------------------------------------------------------------
  # See setRoom for a comment on this conditional check.
  #-----------------------------------------------------------------------------
  setAppointmentType: (appointmentType) ->
    if appointmentType
      @set({appointment_type_id: appointmentType.id}) unless appointmentType.id == @get('appointment_type_id')
    else
      @unset('appointment_type_id')

  getAppointmentType: ->
    HandyworksApp.Data.AppointmentTypes.get(@get('appointment_type_id'))

  #-----------------------------------------------------------------------------
  # The conditional check is necessary currently if a room is deleted, the 
  # appointments which reference it will still send the room_id resulting
  # in this method being called with an undefined argument.
  #-----------------------------------------------------------------------------
  setRoom: (room) ->
    if room
      unless @get('provider_id') or not room.get('provider_id')
        @setProvider(HandyworksApp.Data.Providers.get(room.getProviders()[0]))
      unless @get('appointment_type_id') or not room.get('appointment_type_id')
        @setAppointmentType(HandyworksApp.Data.AppointmentTypes.get(room.get('appointment_type_id')))
      @set({room_id: room.id}) unless room.id == @get('room_id')
    else
      @unset('room_id')


  getRoom: ->
    HandyworksApp.Data.Rooms.get(@get('room_id'))

  setCalendar: (calendar) ->
    @calendar = calendar
    @set({calendar_id: calendar.id})

  getTimes: ->
    @calendar.getTimesForEvent(@get('event'),@get('duration'),@get('provider_id'),@get('room_id'))

  #-----------------------------------------------------------------------------
  # indicates if the appointment has been saved to the server and confirmed
  #-----------------------------------------------------------------------------
  isComplete: ->
    console.log("in appointment model isComplete")
    return @id?

  #-----------------------------------------------------------------------------
  # takes care of cleaning up incomplete appointments
  #-----------------------------------------------------------------------------
  cleanup: ->
    console.log("in appointment model cleanup")
    HandyworksApp.dispatcher.trigger("appointmentcancel",@get('event'))

  #-----------------------------------------------------------------------------
  # filter out null from notes display
  #-----------------------------------------------------------------------------
  getNotes: ->
    n = @get('event.notes')
    return n unless n == null
    return ''
