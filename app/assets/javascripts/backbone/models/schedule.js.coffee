#===============================================================================
# Schedule model is used for the schedule of a particular resource (e.g. 
# Provider schedule ) interface.
#===============================================================================
window.HandyworksApp.Models.Schedule = Backbone.Model.extend
  urlRoot: "#{root_path}setup/schedules"
  initialize: ->
    console.info('initialize schedule model',@)
    @collection_options =
      eventable_id: @id
      eventable_type: 'Schedule'
    @_separateEventsFromExceptions(@get('events'))
    @bind('change:events',@eventChange)

  eventChange: (model,events) ->
    console.info('change:events fired',events)
    @_separateEventsFromExceptions(@get('events'))

  _separateEventsFromExceptions: (combined_data) ->
    console.info('in _separateEventsFromExceptions',combined_data)
    split_data = _.groupBy combined_data, (datum) ->
      return if datum.title is HandyworksApp.Configuration.defaultScheduleEventTitle then 'events' else 'exceptions'
    console.log('--> split events into',split_data)
    if @events?
      @events.reset(split_data.events)
    else
      @events = new HandyworksApp.Collections.Events(split_data.events,@collection_options)
    if @exceptions?
      @exceptions.reset(split_data.exceptions)
    else
      @exceptions = new HandyworksApp.Collections.Events(split_data.exceptions,@collection_options)
