#===============================================================================
# Patient and related models / collections for dynamic client side interface
# controls.
#===============================================================================
window.HandyworksApp.Models.Patient = Backbone.NestedModel.extend
  defaults:
    mode: 0 # new patient
    type: 'Patient'

  urlRoot: "#{root_path}interaction/patients"

  initialize: (options={}) ->
    #console.log("initializing model patient",options)
    # if we initialize a new patient with an id, then it's existing
    #@transition('loading') if @id?
    @touch()

  setAppointment: (appointment) ->
    @appointment = appointment

  #-----------------------------------------------------------------------------
  # Returns the default provider for this patient. Used when creating a new
  # appointment.
  #-----------------------------------------------------------------------------
  getProvider: ->
    if @has('default_provider')
      return HandyworksApp.Data.Providers.get(@get('default_provider'))

  #-----------------------------------------------------------------------------
  # reflects the current state of this patient resource, it should correspond
  # to the appointment mode, but rather look externally for this status, it
  # seemed logical to derive it from the actual state of the object.
  #-----------------------------------------------------------------------------
  getMode: ->
    #return 2 if @workflowState() is 'new'
    return 0 # default existing

  #-----------------------------------------------------------------------------
  # loads a patient from the server given the id, the result will be added
  # to the Patients collection.
  #  toi initialize:  patient = new HandyworksApp.Models.Patient({id: id})
  #-----------------------------------------------------------------------------
  loadPatient: (callback) ->
    @fetch
      #async: false
      success: =>
        console.info("appointment loadPatientInfo success",@)
        @touch()
        HandyworksApp.Data.Patients.add(@)
        callback(@)
      error: =>
        console.error("appointment loadPatientInfo failure")

  #-----------------------------------------------------------------------------
  # update the 'most recently used' index for this patient
  #   we're using a -1 to allow for ascending sorts to return the mru first.
  #-----------------------------------------------------------------------------
  touch: ->
    @mru = -1 * Date.now()

  #-----------------------------------------------------------------------------
  # generate a last, first name for this user that is intended to be displayed
  # and sorted for lists
  #-----------------------------------------------------------------------------
  listName: ->
    @get('contact.last_name') + ', ' + @get('contact.first_name')

  #-----------------------------------------------------------------------------
  # a short cut to open the edit popup view for a patient.
  #-----------------------------------------------------------------------------
  editPopup: ->
    url = @url() + "/edit"
    HandyworksApp.editPopupResource(url,'patient-edit')
