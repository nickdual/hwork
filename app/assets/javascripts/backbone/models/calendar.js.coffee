#===============================================================================
# The Calendar Model is the vehicle to transmit the data sets required to 
#  drive the appointments interface including
#===============================================================================
window.HandyworksApp.Models.Calendar = Backbone.Model.extend
  defaults:
    type: 'Calendar'
    days: 1
    date: Date.today()

  urlRoot: "#{root_path}interaction/calendar"
  #-----------------------------------------------------------------------------
  # Override the "default" Backbone model behaviour to accomdate two aspects
  # aspects of this model:
  #  1) This is a singular resource within the current session, so id's are not
  #     being passed to the server
  #  2) In order to accomodate date "pagination" we need to pass parameters
  #     to the server indicating the date range we'd like to load
  #-----------------------------------------------------------------------------
  url: () ->
    @urlRoot + "?sDate=" + @.get('date').toString('yyyy-MM-dd')

  initialize: ->
    console.info('initialize calendar model')
    _.bindAll @, "setDate"
    @_pushSchedulesToResources()

  #-----------------------------------------------------------------------------
  # updates the currently selected calendar date, determines if more data
  # needs to be requested from the server, when we're requesting the data
  # from the server, we're trying to use the silent: true option to avoid 
  # triggering the calendar Backbone model 'change:date' event. It seems 
  # to fire anyway. Otherwise we want to set the date attribute normally
  # if the data is already available.
  #-----------------------------------------------------------------------------
  setDate: (date) ->
    unless date.compareTo(Date.parse(@.get('event_start_date'))) >= 0 and date.compareTo(Date.parse(@.get('event_end_date'))) < 0
      @.set({date: date},{silent: true})
      @loadEventData(date)
    else
      @.set({date: date})
  #-----------------------------------------------------------------------------
  # Take the schedules transmitted in the attributes of the calendar object
  # and push them into the Clinics and Providers collections.
  #-----------------------------------------------------------------------------
  _pushSchedulesToResources: ->
    HandyworksApp.Data.Appointments = new HandyworksApp.Collections.Appointments(@.get('appts'))
    @_setSchedules(HandyworksApp.Data.Providers,@.get('provider_schedules'))

  #-----------------------------------------------------------------------------
  # When this is triggered on a date change ( see @setDate() ), it needs to 
  # push the new schedules to the resource collections Clinics / Providers
  # and trigger a refresh on the calendar display
  #-----------------------------------------------------------------------------
  loadEventData: (new_date) ->
    console.info('in appointment calendar loadEventData',new_date)
    current_version = @get('version')
    dataLoad = (model,response) ->
      console.log('Calendar fetch some dater',model,response)
      model._pushSchedulesToResources()
      HandyworksApp.dispatcher.trigger('eventsloaded')
    @fetch
      success: dataLoad

  #-----------------------------------------------------------------------------
  # This method is used as a timed refresh callback to pickup any events
  # which would have been created by other users of this account, or through
  # the public interface. It will only send and receive the 'version' 
  # for the calendar.
  #-----------------------------------------------------------------------------
  checkEventData: ->
    console.log("in appointment calendar checkEventData")
    current_version = @get('version')
    HandyworksApp.Configuration.dontDisplayBusy = true
    console.log("-- ++ >",@urlRoot)
    jQuery.getJSON @urlRoot, { version: current_version }, (data,textStatus, jqXHR) ->
      console.log("in loadEvent data pre-load",data,textStatus)
      HandyworksApp.Configuration.dontDisplayBusy = false
      if data.version != current_version
        console.log("==> out of sync")
        HandyworksApp.dispatcher.trigger('eventsoutofsync')
      else
        console.log("==> in sync")
        HandyworksApp.dispatcher.trigger('eventsinsync')


  #-----------------------------------------------------------------------------
  # return the list of "user" items to be displayed as columns on the calendar
  # this can either be rooms or providers depending on the current view_by 
  # setting.
  #-----------------------------------------------------------------------------
  getCalUsers: ->
    console.log("in calendar getCalUsers")
    if HandyworksApp.Data.CalendarOption.get('view_by') == "room"
      collection = HandyworksApp.Data.Rooms.sortBy (room) ->
        room.get('position')
    else
      collection = HandyworksApp.Data.Providers
    users = collection.filter (user) ->
      user.get('enabled')

  #-----------------------------------------------------------------------------
  # generate a list of free/busy event records to be passed to the calendar
  #-----------------------------------------------------------------------------
  getFreeBusys: (start, end) ->
    console.log("in calendar getFreeBusys")
    result = []
    users = @getCalUsers()
    _.each users, (user) ->
      result = result.concat user.getFreeBusys(start, end)
    result

  #-----------------------------------------------------------------------------
  # generate the list of appointments to be displayed to the calendar
  #-----------------------------------------------------------------------------
  getAppointments: (start, end) ->
    console.log("in calendar getAppointments")
    if HandyworksApp.Data.Appointments
      result = HandyworksApp.Data.Appointments.map (appt) ->
        appt.toCalEvent()
    else
      result = []
    console.log('---> ',result)
    return result

  #-----------------------------------------------------------------------------
  # applys the schedules attribute to the appropriate resource (either clinic
  # or provider) the schedule information is associated with the calendar on 
  # server side to ensure the scope (start date --> end date) is kept appropriate
  # for the current window on the calendar.
  #-----------------------------------------------------------------------------
  _setSchedules: (collection, schedules) ->
    console.log("in calendar _setSchedules")
    _.each schedules, (schedule) ->
      console.info("--> resource for ",schedule.id)
      resource = collection.get(schedule.id)
      if resource?
        resource.setSchedule(schedule.schedule)
      else
        console.warn("--> ! no resource for this schedule")

  #-----------------------------------------------------------------------------
  # use the best estimate based on schedule data loaded from the server to 
  # determine the allowed start times for the event through the end of the day
  # on whhich the event occurs
  #   business rules:
  #     a room can only be used by a single appointment at any given time
  #     a provider can see an unlimited number of patients (currently) and
  #     is only limited by the number of rooms available.
  #-----------------------------------------------------------------------------
  getTimesForEvent: (event, duration, provider_id, room_id) ->
    console.info("in appointment calendar getTimesForEvent",event,duration,provider_id,room_id)
    times = [
      event.start.toString('HH:mm')
    ]
    return times


