#===============================================================================
# A model to contain and sync the preferences for the calendar with the 
# server side calendar model.
#===============================================================================
window.HandyworksApp.Models.CalendarOption = Backbone.Model.extend
  url: "#{root_path}interaction/calendar"

  initialize: (options={}) ->
    console.log("initializing model calendar_option",options)
