#===============================================================================
#
#===============================================================================
window.HandyworksApp.Models.Provider = Backbone.NestedModel.extend
  defaults:
    type: 'Provider'
    enabled: true

  urlRoot: "#{root_path}setup/providers"

  initialize: (options={}) ->
    console.log("initializing model provider",options)

  #-----------------------------------------------------------------------------
  # return this list of free busy objects for this provider. include the
  # provider_id in the objects to identify them later, for example when they're
  # used in generating a schedule for a room based on the list of assigned
  # providers.
  #-----------------------------------------------------------------------------
  getFreeBusys: (start, end) ->
    ##if start and end
    ##  console.log("in provider getFreeBusys",start.toString("MM/dd/yyyy HH:mm"),end.toString("MM/dd/yyyy HH:mm"),@get('signature_name'))
    result = []
    if @schedule?
      result = @schedule.getFreeBusys(start, end)
      for event in result
        event.provider_id = @id
    result


  setSchedule: (schedule) ->
    console.log("in provider #{@id} setSchedule")
    @schedule = new HandyworksApp.Collections.Schedule(schedule)
    @schedule.setSchedulable(@)

  isAvailable: (start, end) ->
    pFreeBusys = @getFreeBusys(start, end)
    for fb in pFreeBusys
      return true if fb.free
    return false
