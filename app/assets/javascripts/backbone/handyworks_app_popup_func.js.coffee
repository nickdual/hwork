#===============================================================================
# A collection of functions which apply the Handyworks.Popup features.
#===============================================================================

_.extend window.HandyworksApp,

  #-----------------------------------------------------------------------------
  # update the current active tab content with the specified html data
  #-----------------------------------------------------------------------------
  tabContent: (data) ->
    jQuery(".content-container.active").html(data)

  updateContent: (data) ->
    x = jQuery("[data-container]").first()
    if x?
      jQuery("#" + x.data('container')).empty().html(data)

  #-----------------------------------------------------------------------------
  # This method is delegating an event handler which will manage the popup
  # resource links (except for the index rows) that we'll appear throughout
  # the site.
  #-----------------------------------------------------------------------------
  resourceEditPopupHandler: () ->
    console.log("setting up edit popup process")
    jQuery('body').on "click.handyworks", 'a.edit-popup', (event) ->
      event.preventDefault()
      event.stopPropagation()
      url = jQuery(event.target).attr('href')
      HandyworksApp.editPopupResource(url,'edit-popup')

  #-----------------------------------------------------------------------------
  # opens the FB popup to edit the RESTful resource identified by the user at
  # the given URL.
  #   row parameter is required for the index view popups to close out currently
  #-----------------------------------------------------------------------------
  editPopupResource: (url, label='index', row=null) ->
    console.info('edit popup',url)
    HandyworksApp.Popup.push url,
     afterShow: ->
       HandyworksApp._setupEditPopup(jQuery('.fancybox-inner'))
       HandyworksApp.put_on_style()
     beforeClose: ->
       HandyworksApp._fbBeforeClose(row,label,null)

  #-----------------------------------------------------------------------------
  # open the payment method window in a popup to retrieve the CC information
  # from Braintree  
  #-----------------------------------------------------------------------------
  genericPopup: (type, popup_target) ->
    console.info('in genericPopup',popup_target,type)
    HandyworksApp.Popup.add popup_target,
      afterShow: ->
        console.info("#{type} popup complete")
        HandyworksApp.put_on_style()
        HandyworksApp.dispatcher.trigger( type + "ready")
      beforeClose: ->
        console.info("#{type} popup closing")
        HandyworksApp.dispatcher.trigger(type + "closing")
        return true
      afterClose: ->
        console.info("#{type} area popup closed")
        return true

  #-----------------------------------------------------------------------------
  # Creates the light box popup interface to manage a resource's schedule.
  #-----------------------------------------------------------------------------
  schedulePopup: ($schedule_area, row_link = false) ->
    $schedule_area.each (idx,element) ->
      if row_link
        # hide the element so its not directly usuable
        jQuery(element).css("visibility","hidden").css("display","none")
        # store a reference to this element with its parent row
        # this will be used by the indexTable click event to trigger the fancybox
        jQuery(element).closest("tr").data("HandyworksApp.editPopupLink",jQuery(element))
      # setup the light box display
      HandyworksApp.Popup.add jQuery(element),
        beforeLoad: ->
          jQuery.fancybox.showLoading()
          return true
        afterShow: ->
          console.info('schedule area popup complete',element)
          HandyworksApp.dispatcher.trigger("scheduleready")
          HandyworksApp.put_on_style()
        beforePush: ->
          console.info('schedule area popup closing (pushed)')
          HandyworksApp.dispatcher.trigger("scheduleclosing")
          return true
        afterClose: ->
          console.info('schedule area popup closed')
          HandyworksApp.router.leave() if HandyworksApp.router
          return true

  #-----------------------------------------------------------------------------
  # called before the switch will go to a new resource. 
  #  NOTE : there are a lot of similarities between the _fbBeforeSwitch and 
  #         the _fbBeforeClose methods which are performing very similar functions
  #           * checking for proper form submission conditions, state change
  #           * validating the form
  #           * display validation errors
  #    so be looking for either macking changes in both methods, or to 
  #    combine them together someday.
  #-----------------------------------------------------------------------------
  _fbBeforeSwitch: (label) ->
    h = HandyworksApp
    console.info(label + ' area popup switching')
    HandyworksApp.dispatcher.trigger("popupswitching")
    if h.isFormStateChanged('.fancybox-inner')
      a_form = jQuery('form',jQuery('.fancybox-inner')).first()
      console.info('form state change detected',a_form)
      validator = a_form.validate()
      # to allow for a very special case, we have to make the submit process substantially more
      # complicated. if there is a remote validated field, the special case is where the user 
      # edits that field, and before focusing on any other field, they click SAVE. in this case
      # there is a remote validation request pending, due to some questionable jquery.validation
      # logic, we can't just check validator.form() we need to include a check of pending requests
      # and if we prevent submission now b/c of a pending request, 99.99% of the time the form is
      # valid and we'd need the user to click on SAVE again once the validation request completes,
      # so what we'll do in the event there is a pending request is schedule another attempt (tryAgain)
      # in 1/2 second, up to 4 times. if it takes long than that the user will have to click SAVE
      # again.
      doSubmit = ->
        if validator.form() and validator.pendingRequest == 0
          console.info("====> submitting form",validator)
          a_form.submit()
        else
          a_form.validate().showErrors()
          console.warn("--> form errors need to be corrected")
          return false
      if validator.pendingRequest > 0
        return false
      else
        doSubmit()
    else
      return true
    
  #-----------------------------------------------------------------------------
  # because there is a lot of logic involved with closing the FB popup, we'll
  # extract it into its own function.
  #-----------------------------------------------------------------------------
  _fbBeforeClose: (nRow,label,element) ->
    h = HandyworksApp
    console.info(label + ' area popup closing') if label?
    HandyworksApp.dispatcher.trigger("popupclosing")
    nRow = jQuery(element).closest("tr") if not nRow? and element?
    unless h.Data.popupCanceled
      # the following logic will only be triggered if the FB popup is being closed by clicking
      # on "SAVE" which sets the popupSubmitted = true
      if h.Data.popupSubmitted > 0 and h.isFormStateChanged('.fancybox-inner')
        a_form = if h.Data.popupFormSubmitted? then h.Data.popupFormSubmitted else jQuery('form',jQuery('.fancybox-inner')).first()
        console.info('form state change detected',a_form)
        validator = a_form.validate()
        # to allow for a very special case, we have to make the submit process substantially more
        # complicated. if there is a remote validated field, the special case is where the user 
        # edits that field, and before focusing on any other field, they click SAVE. in this case
        # there is a remote validation request pending, due to some questionable jquery.validation
        # logic, we can't just check validator.form() we need to include a check of pending requests
        # and if we prevent submission now b/c of a pending request, 99.99% of the time the form is
        # valid and we'd need the user to click on SAVE again once the validation request completes,
        # so what we'll do in the event there is a pending request is schedule another attempt (tryAgain)
        # in 1/2 second, up to 4 times. if it takes long than that the user will have to click SAVE
        # again.
        doSubmit = ->
          if validator.form() and validator.pendingRequest == 0
            console.info("====> submitting form",validator)
            h.bgPopupSubmit(nRow, '.fancybox-inner', a_form)
            jQuery(nRow).addClass("update_pending") if nRow?
            return true
          else
            a_form.validate().showErrors()
            console.warn("--> form errors need to be corrected")
            return false
        tryAgain = ->
          console.log("--> popup submit tryAgain",h.Data.popupSubmitted)
          if h.Data.popupSubmitted < 5
            h.Data.popupSubmitted = h.Data.popupSubmitted + 1
            jQuery.fancybox.close()
          else
            console.error("--> popup submit exceed wait for remote validation")
        if validator.pendingRequest > 0
          _.delay tryAgain, 500
          return false
        else
          doSubmit()
      else
        return true
    else
      return true

  #-----------------------------------------------------------------------------
  # Setup the Edit / Show windows to be displayed in light box via AJAX
  # This logic assumes there will be only 1 action / popup per row
  #   selectors is an array of jQuery selectors to operate on. 
  #-----------------------------------------------------------------------------
  editPopup: (selectors, scope = document, row_link = true) ->
    console.info("in Handyworks editPopup")
    _.each selectors, (selector) ->
      console.log("--> working on #{selector}")
      jQuery(selector,scope).each (idx,element) ->
        if row_link
          # hide the element so its not directly usuable
          jQuery(element).css("visibility","hidden").css("display","none")
          # store a reference to this element with its parent row
          # this will be used by the indexTable click event to trigger the fancybox
          jQuery(element).closest("tr").data("HandyworksApp.editPopupLink",jQuery(element))
        # setup the light box display
        HandyworksApp.Popup.add jQuery(element),
         beforeLoad: ->
           console.info('edit popup event start',element)
           if row_link
             HandyworksApp.indexTableSelectRow(jQuery(element).closest("tr"))
           return true
         afterShow: ->
           console.info('edit popup complete',jQuery(element))
           HandyworksApp._setupEditPopup(jQuery('.fancybox-inner'))
           HandyworksApp.dispatcher.trigger("popupready")
           HandyworksApp.put_on_style()
         beforeClose: ->
           HandyworksApp._fbBeforeClose(null,'edit',element)
  
  #-----------------------------------------------------------------------------
  # _setupEditPopup
  #  performs some manipulations of the edit view DOM to function properly in
  #  a popup window:
  #-----------------------------------------------------------------------------
  _setupEditPopup: (container) ->
    h = HandyworksApp
    h.recordFormState(container)
    h.Data.popupCanceled = false
    h.Data.popupSubmitted = 0
    h.Data.popupFormSubmitted = null
    unless h.Data.bgPopupHandlersBound
      h.Data.bgPopupHandlersBound = true
      jQuery(document).on "ajax:success", h.bgPopupSubmitSuccess
      jQuery(document).on "ajax:error", (event, xhr, status, error) ->
        if h.Data.bgPopupSubmitted
          if xhr.status == 200
            event.stopPropagation()
            h.bgPopupSubmitSuccess(event, xhr.responseText, status, xhr)
      jQuery(document).on "ajax:complete", (event, xhr, status) ->
        if h.Data.bgPopupSubmitted
          console.info("handyworks app bgPopupSubmit complete",event,xhr)
          event.stopPropagation()
          # TODO : the selector below removing the update_pending class is not as precise as I'd like it to be
          #        it will remove all currently rows currently submitted for update, so if the user has 
          #        already submitted a 2nd update form, they'd both be marked complete here.
          jQuery("tr.update_pending").removeClass("update_pending")
  #-----------------------------------------------------------------------------
  # bgPopupSubmit
  #  submits the form(s) which are contained in a popup window via AJAX in
  #  the background. Its intended to be used when saving data back to the 
  #  server from a fancybox popup.
  #  NOTE : right now I'm assuming there's only one form on the popup, provide for multiple forms 
  #         if needed.
  #  NOTE : the AJAXishness is being done by the rails-ujs based on data-remote
  #         attributes
  #-----------------------------------------------------------------------------
  bgPopupSubmit: (edit_link, container = document, iform = null) ->
    console.log("in HandyworksApp bgPopupSubmit",edit_link)
    form = if iform? then iform else jQuery('form',jQuery(container)).first()
    # this is a bit of a hack. currently the popup submit success handlers are being bound on
    # generic ajax:success and ajax:error events which are also being triggered by other
    # requests while a popup is open. this boolean is intended to tell the handlers to
    # only be applied if the popup was really submitted.
    HandyworksApp.Data.bgPopupSubmitted = true
    # keeps track of if we want to return to the index view on successful submission
    # OPTIMIZE : we're currently getting back the index data with the response to form submissions regardless if we're going to display the dataTable or not.
    HandyworksApp.Data.bgPopupSubmittedStandalone = true unless edit_link
    form.submit()

  bgPopupSubmitSuccess: (event, data, status, xhr) ->
    if HandyworksApp.Data.bgPopupSubmitted
      event.stopPropagation()
      console.info("handyworks app bgPopupSubmit success",event,status)
      unless HandyworksApp.Data.bgPopupSubmittedStandalone
        console.log("going to replace main content")
        main_content = jQuery('div#content')
        if ( main_content.length > 0 and main_content.children().length > 0 )
          console.log("--> main content replaced")
          main_content.html(data)
        else
          console.log("--> skipped content replacement")
          HandyworksApp.tabContent(data)
      else
        # update a content container specified in the data result
        HandyworksApp.updateContent(data)
    HandyworksApp.Data.bgPopupSubmitted = false
    HandyworksApp.Data.bgPopupSubmittedStandalone = false

