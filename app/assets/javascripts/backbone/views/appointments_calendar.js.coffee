#-------------------------------------------------------------------------------
# HandyworksApp.Views.Calendar
#  provides the Backbone style integration with the jQuery.weekCalendar 
#  plugin ( https://github.com/themouette/jquery-week-calendar/wiki )
#-------------------------------------------------------------------------------
window.HandyworksApp.Views.AppointmentsCalendar =  Support.CompositeView.extend
  initialize: (options={}) ->
    console.log('initialize appointments calendar',options)
    _.bindAll @, "render", "calendarTitle", "setDaysToShow", "dateSelected",
      "updateDisplayedResources", "displayEvents", "removeEvent", "getAppointmentUserId",
      "setAppointmentUserId", "appointmentResize", "appointmentDrop", "appointmentDelete",
      "createAppointmentError", "updateAppointmentError", "deleteAppointmentError",
      "appointmentRefresh", "appointmentClick", "appointmentBody",
      "scheduleRender", "newEvent",
      "rollCalendarVersion", "calculateLayoutHeight"
    @bindTo @model, "change:days", @setDaysToShow
    @bindTo @model, "change:date", @dateSelected
    @bindTo HandyworksApp.Data.CalendarOption, "change", @updateDisplayedResources
    @bindTo HandyworksApp.Data.Rooms, "change:enabled",@updateDisplayedResources
    @bindTo HandyworksApp.dispatcher, "roomposition",@updateDisplayedResources
    @bindTo HandyworksApp.Data.Providers, "change:enabled",@updateDisplayedResources
    @bindTo HandyworksApp.dispatcher, "appointmentcancel", @removeEvent
    @bindTo HandyworksApp.dispatcher, "eventsloaded", @updateDisplayedResources
    @bindTo HandyworksApp.Data.Appointments, "createAppointmentError", @createAppointmentError
    @bindTo HandyworksApp.Data.Appointments, "updateAppointmentError", @updateAppointmentError
    @bindTo HandyworksApp.Data.Appointments, "deleteAppointmentError", @deleteAppointmentError
    @bindTo HandyworksApp.Data.Appointments, "change:room_id",@updateDisplayedResources
    @bindTo HandyworksApp.Data.Appointments, "change:provider_id",@updateDisplayedResources
    @bindTo HandyworksApp.Data.Appointments, "change:contact_id", @updateDisplayedResources
    @bindTo HandyworksApp.Data.Appointments, "change:appointment_type_id", @updateDisplayedResources
    @bindTo HandyworksApp.Data.Appointments, "change:version", @rollCalendarVersion

  #-----------------------------------------------------------------------------
  # this method is used to resize the calendar when the window size is changed
  # so in order to keep the container smooth, we need to adjust its height as 
  # well as the height of the calendar itself. 23px is an empirically determined
  # value that in Google Chrome eliminates to scrollbar between the #calendar_layout
  # and the center layout pane in which its rendered.
  #-----------------------------------------------------------------------------
  calculateLayoutHeight: (calendar) ->
    x = HandyworksApp.getAppointmentCalendarHeight()
    jQuery('#calendar_layout').css('height', x + 'px')
    return x - 23

  render: ->
    console.log('appointments calendar render',@model.get('days'))
    @calendar = @$el.weekCalendar
      headerSeparator: '&nbsp;&ndash;&nbsp;'
      defaultEventLength: 4
      newEventText: "Creating Appointment..."
      showHeader: false
      hourLine: true # display current time as a thin red line
      minDate: Date.today().add((-1)*@model.get('schedule_history_max_days')).days()
      maxDate: Date.today().add(@model.get('schedule_future_max_days')).days()
      height:  @calculateLayoutHeight
      buttons: false
      getUserId: (user, index, calendar) ->
        user.id
      getUserName: (user, index, calendar) ->
        if user.has('signature_name') then user.get('signature_name') else user.get('name')
      firstDayOfWeek: HandyworksApp.Configuration.firstDay
      displayFreeBusys: true
      data: (start, end, callback) =>
        callback(@displayEvents(start,end))
      getEventUserId: @getAppointmentUserId
      setEventUserId: @setAppointmentUserId
      eventHeader: @appointmentBody
      eventResize: @appointmentResize
      eventDrop: @appointmentDrop
      eventClick: @appointmentClick
      eventRefresh: @appointmentRefresh
      totalEventsWidthPercentInOneColumn: 95
      eventGap: 5 # vertical space between abutting events.
      #timeslotHeight: 20
      #eventMouseover:
      eventBody: (calEvent, calendar) ->
        ""
      allowEventDelete: true
      eventDelete: @appointmentDelete
      allowCalEventOverlap: true
      overlapEventsSeparate: true
      getFreeBusyUserId: (calFreeBusy, calendar) ->
        calFreeBusy.model_id
      freeBusyRender: @scheduleRender
      eventNew: @newEvent
      beforeEventNew: HandyworksApp.beforeEventNew
      # TODO : could make business start and end time account preferences
      businessHours :{start: 8, end: 18, limitDisplay: false }
    # the following delayed call to "setDaysToShow" improves the display of the calendar in Chrome
    # browsers, and doesn't seem to negatively impact old FireFox browsers, haven't tested it
    # in any other browser. In FireFox 9.X it doesn't improve the display, but correctly adjusts
    # the height of the scrollable area. 
    doit = =>
      @setDaysToShow(@model.get('days'))
    _.delay( doit, 100 )
    jQuery('#calendar_layout').css('overflow','auto')
    @

  #-----------------------------------------------------------------------------
  # Called to Render Free Busy display. It will do the default work of the 
  # weekCalendar but will also add additional display data to Exception like
  # blocked time such as vacations or open hours as well, like extended holiday
  # hours.
  #-----------------------------------------------------------------------------
  scheduleRender: (freeBusy, $freeBusy, calendar) ->
    #console.log("in freeBusyRender",freeBusy)
    if not freeBusy.free
      $freeBusy.addClass('free-busy-busy')
    else
      $freeBusy.addClass('free-busy-free')
    if freeBusy.title and freeBusy.title != "Schedule Event"
      $freeBusy.addClass('free-busy-exception')
      $freeBusy.html(JST['appointments/_appointment_schedule_exception']({title: freeBusy.title}))
    $freeBusy

  #-----------------------------------------------------------------------------
  # Called when an appointment is refreshed. Applies the associated provider 
  # style for appointment color. Also apply a visual cue via border color
  # of the appointment type.
  #
  # OPTIMIZE : there may be a more efficient way to do this using a set of "static" CSS
  # rules that would be injected into the appointment page when it first loads.
  #
  # NOTE: This was originally written as a callback eventAfterRender; however,
  #   this callback is only used on the initial event rendering and was "reverted"
  #   to the default display on resize and ?sometimes? drag/drop actions as well.
  #   vs. the eventRefresh which is called in several places.
  #-----------------------------------------------------------------------------
  appointmentRefresh: (calEvent, element) ->
    console.log("in appointmentRefresh",calEvent,element)
    white = false
    if calEvent.appointment_type_id?
      console.info('in appointmentRefresh',calEvent.appointment_type_id)
      appointment_type = HandyworksApp.Data.AppointmentTypes.get(calEvent.appointment_type_id)
      if appointment_type?
        at_color = appointment_type.get('appointment_color')
        text_color = HandyworksApp.textColorToShow(at_color)
        white = true if text_color == '#fff'
        jQuery(element).css('background-color',at_color).css('color',text_color)
        jQuery(element).attr('title',appointment_type.get('name'))
    if calEvent.provider_id?
      provider = HandyworksApp.Data.Providers.get(calEvent.provider_id)
      if provider?
        p_code = provider.get('your_code')
        title = jQuery(element).find('.wc-time')
        title_text = title.text()
        title.html(JST['appointments/_appointment_title']
          provider_code: p_code
          title_text: title_text
          white: white
        )

    element

  #-----------------------------------------------------------------------------
  # Display the patient's name as the eventBody. If the appointment or the 
  # patient is not available, then just display the calEvent's title.
  #-----------------------------------------------------------------------------
  appointmentBody: (calEvent, calendar) ->
    appointment = HandyworksApp.Data.Appointments.get(calEvent.id)
    if appointment? and appointment.patient?
      title = appointment.patient.get('contact.name')
    else
      title = calEvent.title
    console.info('in appointmentBody',calEvent,title)
    title


  #-----------------------------------------------------------------------------
  # This is the interface between the calendar column and the appointment. 
  # userId is how the calendar identifies to which columns this appointment
  # should be displayed. In order to make this method work for both events
  # that were loaded from the server as well as new events created by the 
  # weekCalendar plugin, we'll first look at the appropriate attribute id based
  # on the current view, and default to the userId property which would have
  # been set by the weekCalendar plugin.
  #
  # NOTE: the default userId should not actually be needed
  #-----------------------------------------------------------------------------
  getAppointmentUserId: (calEvent,calendar) ->
    #console.log("in getEventUserId",@model,calEvent)
    v = HandyworksApp.Data.CalendarOption.get('view_by')
    if v is 'room' and calEvent.room_id?
      return calEvent.room_id
    if v is 'provider' and calEvent.provider_id?
      return calEvent.provider_id
    calEvent.userId

  #-----------------------------------------------------------------------------
  # called by the weekCalendar plugin to specify which column is going to be 
  # used for a new or updated appointment.
  #-----------------------------------------------------------------------------
  setAppointmentUserId: (userId, calEvent, calendar) ->
    v = HandyworksApp.Data.CalendarOption.get('view_by')
    calEvent.room_id = userId if v is 'room'
    calEvent.provider_id = userId if v is 'provider'
    return calEvent

  #-----------------------------------------------------------------------------
  # then trigger the event to open the appointment details view.
  #-----------------------------------------------------------------------------
  appointmentClick: (calEvent,calElement,freeBusyManager,calendar,event) ->
    console.log("in appointmentClick")
    @trigger("appointmentpopup",calEvent)
    # check for single / dbl click on this element
    $target = jQuery(event.target)
    if HandyworksApp.doubleClick($target)
      # this was the dbl click
      appointment = HandyworksApp.Data.Appointments.get(calEvent.id)
      if appointment?
        patient = appointment.getPatient()
        if patient?
          patient.editPopup()

  appointmentResize: (newCalEvent, oldCalEvent, element) ->
    jQuery('div.wc-cal-event.ui-resizable').on('resize',(e)->
      e.stopPropagation())
    console.log("in appointmentResize",element[0])
    HandyworksApp.Data.Appointments.updateAppointment(newCalEvent,oldCalEvent)
  appointmentDrop: (newCalEvent, oldCalEvent, element) ->
    console.log("in appointmentDrop", newCalEvent, oldCalEvent)
    HandyworksApp.Data.Appointments.updateAppointment(newCalEvent,oldCalEvent)

  appointmentDelete: (calEvent,calElement,freeBusyManager,calendar,DomEvent) ->
    console.log("in appointmentDelete")
    if confirm('Are you sure?')
      calendar.weekCalendar('removeEvent',calEvent.id)
      HandyworksApp.Data.Appointments.deleteAppointment(calEvent)

  dateSelected: () ->
    console.log('in appointments calendar dateSelected', @calendar, @model)
    @$el.weekCalendar("gotoDate",@model.get('date'))

  setDaysToShow: () ->
    console.log('in appointments calendar setDaysToShow', @calendar, @model)
    if @calendar?
      @calendar.weekCalendar("setDaysToShow",@model.get('days'))
    else
      console.log('--> no calendar to change days')
    # FIXME : to get the scrollToHour working in Chrome, it requires a little delay
    #         Firefox did well using _.defer. Alternatively, this should be corrected
    #         by the plugin itself, although it may be suffering from the same browser
    #         behaviours seen when calling the procedure manually.
    #
    #  As for the timing here, 50 is too short, 100 seems reliable.
    scroll = =>
      console.info('scrollToHour',@)
      @$el.weekCalendar("scrollToHour",8)
    _.delay(scroll,600)

  #-----------------------------------------------------------------------------
  # This method is called when the user clicks the calendar to create a new
  # appointment. We're going to allow the user (adminsitrative user) to click
  # on a grayed out time slot ( not normal business hours ) to create an 
  # appointment. This is driven by a configuration setting:
  #   HandyworksApp.Configuration.appointmentByHoursRestriction
  # which is currently statically set to false; however, it could at a later
  # date either be driven by a click preference or based on user permissions
  #-----------------------------------------------------------------------------
  newEvent: (calEvent, $event, freeBusyManager, calendar) ->
    console.info('in appointment calendar newEvent',calEvent, freeBusyManager)
    apptTimeAvailable = true
    $.each freeBusyManager.getFreeBusys(calEvent.start, calEvent.end), ->
      if @getStart().getTime() != calEvent.end.getTime() and
        @getEnd().getTime() != calEvent.start.getTime() and not @getOption('free')
          apptTimeAvailable = false
    unless apptTimeAvailable
      # if the appointment time slot is not available, then we'll either block
      # the appointment creation or allow it depending on the value of this 
      # configuration setting.
      if HandyworksApp.Configuration.appointmentByHoursRestriction
        HandyworksApp.showInfo("This appointment time slot is not available.")
        @$el.weekCalendar('removeEvent',calEvent.id)
        return false
      else
        HandyworksApp.showAlert("This appointment time slot is not normally available.")
    # capture the most information about the appointment request
    appointment = @getNewEventInfo(calEvent, $event, freeBusyManager, calendar)
    @trigger("newappointment",appointment)

  #-----------------------------------------------------------------------------
  # called if an error occurs during the initial appointment creation to reset
  # the calendar visual interface to its previous state.
  #-----------------------------------------------------------------------------
  createAppointmentError: (appointment, responseText) ->
    console.log("in createAppointmentError",appointment,responseText)
    @$el.weekCalendar('removeUnsavedEvents')
    if responseText?
      HandyworksApp.showError("Create appointment failed: #{responseText}",".flash")

  #-----------------------------------------------------------------------------
  # called if an update to an existing appointment fails to be persisted into
  # the DB to reset the appointment to its previous state.
  #-----------------------------------------------------------------------------
  updateAppointmentError: (oldCalEvent, responseText) ->
    console.log("in updateAppointmentError",oldCalEvent, responseText)
    @$el.weekCalendar('updateEvent',oldCalEvent)
    if responseText?
      HandyworksApp.showError("Update appointment failed: #{responseText}",".flash")

  #-----------------------------------------------------------------------------
  # called to put the event back when deletion fails.
  #-----------------------------------------------------------------------------
  deleteAppointmentError: (oldCalEvent, responseText) ->
    console.log("in deleteAppointmentError",oldCalEvent, responseText)
    @$el.weekCalendar('updateEvent',oldCalEvent)
    if responseText?
      HandyworksApp.showError("Delete appointment failed: #{responseText}",".flash")

  #-----------------------------------------------------------------------------
  # returns data structure of extracted information based on where the user 
  # clicked to request a new appointment, and the current state of the calendar
  # (e.g. view_by room or provider will dictate if the click specifies a room
  # or provider)
  #
  # if the appointment room_id is et,but not the provider_id, then we'll look
  # through the schedule for the room based on the provider_seq (provider_precedences)
  # to determine which provider should get this appointment by default.
  #-----------------------------------------------------------------------------
  getNewEventInfo: (calEvent, $event, freeBusyManager, calendar) ->
    console.info("in appointment calendar getNewEventInfo", calEvent, $event,freeBusyManager)
    appointment =
      room_id: calEvent.room_id
      provider_id: calEvent.provider_id
      event: calEvent
    console.log("==> creating new appointment",appointment)
    if appointment.room_id and not appointment.provider_id
      # FIXME : it seems like the FBM for this event should include FreeBusy's from all providers, but it does not
      #         to work around this, I'm going directly to the Room model to ask for the free busys.
      #         NOTE : this freeBusyManager should also match the one passed in as an argument.
      # fbm = @$el.weekCalendar("getFreeBusyManagerForEvent",calEvent)
      room = HandyworksApp.Data.Rooms.get(appointment.room_id)
      appointment = room.initializeAppointment(appointment)
    appointment
  #-----------------------------------------------------------------------------
  # passes the information we want to be displayed to the calendar as an 
  # object.
  #
  # this method is also going to enforce a minimum width of approximately 180px
  # for each calendar column. A column is a single user for a single day, where
  # user could either be provider or room based on the current calendar view.
  # The additional buffer of 65px was calculated empirically using Google chrome
  # so the widths won't be exact on all browsers, but should be in the ballpark
  #-----------------------------------------------------------------------------
  displayEvents: (start, end) ->
    console.info('displaying events to calendar',start, end)
    users = @model.getCalUsers()
    freeBusys = @model.getFreeBusys(start, end)
    appointments = @model.getAppointments(start, end)
    days = HandyworksApp.Data.CalendarOption.get('days')
    total_columns = users.length * days
    container_width = "#{total_columns * 180 + 65}px"
    set_cal_width = ->
      container = jQuery('.wc-container')
      console.log('--> resulting calendar width', container, container_width)
      container.css('min-width',container_width)
    _.delay set_cal_width, 250
    data =
      options:
        users: users
        date: @model.get('date')
        timeslotsPerHour: HandyworksApp.Data.CalendarOption.get('time_slots_per_hour')
        daysToShow: days
      events: appointments
      freebusys: freeBusys

  calendarTitle: (date, calendar) ->
    console.log('in calendarTitle',@,date,calendar)
    if @model.get('days') == 1
      '%date%'
    else
      '%start% - %end%'

  updateDisplayedResources: ->
    console.info('in appointments calendar updateDisplayedResources')
    @$el.weekCalendar("refresh")
  
  removeEvent: (event) ->
    console.info('in appointments calendar removeEvent',event)
    # NOTE: this will remove any unsaved event, not just the one specifically
    @$el.weekCalendar("removeUnsavedEvents")

  #-----------------------------------------------------------------------------
  # If we create or update an appointment, its going to return to us with the
  # current calendar version updated, which will trigger this function to update
  # the client side calendar version. This should avoid unnecessarily reloading
  # the calendar resources when the version is out of date due to change in 
  # the clients browser.
  #
  # TODO: There seems to be a little bit of timing sensitivity here, in that
  #   if a request is in progress to checkEvent data, it can result in triggering
  #   an update before this method is triggered to roll the version forward.
  #-----------------------------------------------------------------------------
  rollCalendarVersion: (model, value, options) ->
    console.info("in appointments calendar rollCalendarVersion",model,value,options)
    @model.set({version: value}) if value > @model.get('version')


