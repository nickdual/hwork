window.HandyworksApp.Views.AppointmentsControlsProvider =  Support.CompositeView.extend
  tagName: "li"
  events:
    "click input" : "toggleEnabled"

  initialize: (options={}) ->
    console.log('initialize appointments controls provider',options)
    _.bindAll @, "render", "toggleEnabled"

  render: ->
    @$el.html(JST['appointments/_controls_provider']({ provider: @model }))
    @

  toggleEnabled: (event) ->
    console.log("in provider control toggleEnabled", event)
    id = parseInt(jQuery(event.target).val())
    enabled = jQuery(event.target).attr('checked')
    # check to make sure this wasn't the last one checked
    if not enabled and not HandyworksApp.Data.Providers.atLeastOne()
      jQuery(event.target).attr('checked','checked')
      return
    @model.set({enabled: enabled})

window.HandyworksApp.Views.AppointmentsControlsProviders =  Support.CompositeView.extend
  initialize: (options={}) ->
    console.log('initialize appointments controls providers',options)
    _.bindAll @, "render"

  render: ->
    @renderLayout()
    @renderProviderList()
    @

  renderLayout: ->
    @$el.html(JST['appointments/_controls_providers']())

  renderProviderList: ->
    _provider = (provider) ->
      providerView = new HandyworksApp.Views.AppointmentsControlsProvider
        model: provider
      @appendChildTo(providerView,@.$('#provider_control_list'))
    @collection.forEach _provider, @
