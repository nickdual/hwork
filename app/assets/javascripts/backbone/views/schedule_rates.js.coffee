window.HandyworksApp.Views.ScheduleRates =  Support.CompositeView.extend

  events:
    "slidechange #max_concurrent_appointments" : "updateConcurrentAppointments"
    "slidechange #max_appointments_per_hour" : "updateAppointmentRate"

  initialize: (options) ->
    _.bindAll @, "render", "updateConcurrentAppointments", "updateAppointmentRate"

  updateConcurrentAppointments: (event) ->
    # only trigger the model update if the value has changed, this prevents loops
    # occurring when an error happens. For some reason, this logic needs to come
    # first in the method ... or early anyway
    doit = (typeof event.originalEvent != "undefined")
    console.log('schedule rates update concurrent rate',event.originalEvent,doit)
    if doit
      @_saveRates()
    else
      console.log('--> non user slider change',event.originalEvent)


  updateAppointmentRate: (event) ->
    # only trigger the model update if the value has changed, this prevents loops
    # occurring when an error happens. For some reason, this logic needs to come
    # first in the method ... or early anyway
    doit = (typeof event.originalEvent != "undefined")
    console.log('schedule rates update appointment rate / hour',event.originalEvent,doit)
    if doit
      @_saveRates()
    else
      console.log('--> non user slider change',event.originalEvent)

  _saveRates: ->
      new_values = [
        @.$('#max_concurrent_appointments').slider("value")
        @.$('#max_appointments_per_hour').slider("value")
      ]
      old_values = [
        @model.get('max_concurrent_appointments')
        @model.get('max_appointments_per_hour')
      ]
      console.log('new value, old value',new_values,old_values)
      if new_values[0] != old_values[0] or new_values[1] != old_values[1]
        @model.save({
          max_concurrent_appointments: new_values[0]
          max_appointments_per_hour: new_values[1]
        },
          error: =>
            console.log('schedule rates save failed',@)
            if new_values[0] != old_values[0]
              @.$('#max_concurrent_appointments').slider("value", old_values[0])
              @.$('select')[0].selectedIndex = old_values[0]
              @model.set({max_concurrent_appointments: old_values[0]})
              @parent.error('Unable to update concurrent appointment rate')
            if new_values[1] != old_values[1]
              @.$('#max_appointments_per_hour').slider("value",old_values[1])
              @.$('select')[1].selectedIndex = old_values[1]
              @model.set({max_appointments_per_hour: old_values[1]})
              @parent.error('Unable to update appointments per hour')
        )
      else
        console.log('--> no change')

  render: ->
    console.log('schedule rates render')
    @$el.html(JST['schedules/_rates']())
    select = @.$('select')
    @slider_concurrent = @.$('#max_concurrent_appointments').slider
      min: 0
      max: 10
      value: @model.get('max_concurrent_appointments')
      slide: ( event, ui ) ->
        select[ 0 ].selectedIndex = ui.value
    @slider_per_hour = @.$('#max_appointments_per_hour').slider
      min: 0
      max: 10
      value: @model.get('max_appointments_per_hour')
      slide: ( event, ui ) ->
        select[ 1 ].selectedIndex = ui.value

    @.$('#schedule_max_concurrent_appointments').change { view: @ }, (event) ->
      console.log('change max concurrent appointment selector')
      jQuery('#max_concurrent_appointments').slider("value", @selectedIndex)
      event.data.view._saveRates()

    @.$('#schedule_max_appointments_per_hour').change { view: @ }, (event) ->
      console.log('change max appointments per hour selector')
      jQuery('#max_appointments_per_hour').slider("value", @selectedIndex)
      event.data.view._saveRates()

    select[0].selectedIndex = @model.get('max_concurrent_appointments')
    select[1].selectedIndex = @model.get('max_appointments_per_hour')

    @
