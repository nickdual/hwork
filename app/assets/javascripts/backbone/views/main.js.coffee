#===============================================================================
# This view will be started first within the authenticated base template to
# manage all other Backbone.js based views.
#===============================================================================
window.HandyworksApp.Views.Main =  Support.CompositeView.extend
  id: 'main_bbv'

  initialize: ->
    console.info('in main view initialize')
    _.bindAll @, "render", "renderAppointments"
    @activeViews = {}

  render: ->
    console.log('render main view')
    # handles dynamic elements within the navbar view.
    @renderNavbar()
    @

  renderNavbar: ->
    console.log('render main navbar')
    cpid = HandyworksApp.Data.current_patient_id
    if cpid
      cp = HandyworksApp.Data.Patients.get(cpid)
      if cp
        HandyworksApp.Data.CurrentPatient = new HandyworksApp.Models.CurrentPatient({patient: cp})
      else
        HandyworksApp.Data.CurrentPatient = new HandyworksApp.Models.CurrentPatient()
        console.info("unable to find patient with id",HandyworksApp.Data.current_patient_id)
    else
      HandyworksApp.Data.CurrentPatient = new HandyworksApp.Models.CurrentPatient()
      console.info("no patient id is defined")
    # FIXME : need to get the main view attached to the <body> tag to get the best view functions
    currentPatientContainer = jQuery('header .current-patient')
    @activeViews.currentPatient = new HandyworksApp.Views.CurrentPatient
      el: currentPatientContainer
      model: HandyworksApp.Data.CurrentPatient
    @renderChild(@activeViews.currentPatient)

  #-----------------------------------------------------------------------------
  # This render method is called directly from the router to initialize the
  # appointment calendar view.
  #-----------------------------------------------------------------------------
  renderAppointments: (container, data) ->
    @calendar = new HandyworksApp.Models.Calendar(data)
    # initialize the calendar with today's date
    @calendar.set({date: Date.today()})
    @activeViews.appointment = new HandyworksApp.Views.Appointments
      el: jQuery(container)
      model: @calendar
    @renderChild(@activeViews.appointment)

  #-----------------------------------------------------------------------------
  # This render method is called directly from the router to initialize the
  # appointment calendar view.
  #-----------------------------------------------------------------------------
  renderSchedule: (container, data) ->
    @schedule = new HandyworksApp.Models.Schedule(data)
    @activeViews.schedule = new HandyworksApp.Views.Schedule
      el: jQuery(container)
      model: @schedule
    @renderChild(@activeViews.schedule)

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
window.HandyworksApp.Views.CurrentPatient =  Support.CompositeView.extend
  events:
    "click .patient-search-icon" : "doPatientSearch"
    "click button.close" : "close"
    "mouseleave .popover" : "mouseToClose"
    "mouseenter .popover" : "mouseToOpen"

  initialize: (options={}) ->
    console.info('initialize current patient view')
    _.bindAll @, "render", "updateCurrentPatient", "doPatientSearch",
      "renderPatientSearch", "close", "mouseToClose", "mouseToOpen",
      "removeCurrentPatient"
    @bindTo @model, "change", @render if @model
    @bindTo HandyworksApp.Data.Patients, "add", @updateCurrentPatient
    @bindTo HandyworksApp.Data.CurrentPatient, "change:patient", @close
    @bindTo HandyworksApp.Data.Patients, "remove", @removeCurrentPatient

  render: ->
    patient = @model.get('patient')
    if patient
      unless patient.id == 0
        HandyworksApp.changeCurrentPatient(patient.id)
      @$el.html(JST['session_settings/_current_patient']({ patient: patient }))
    @

  removeCurrentPatient: (model) ->
    console.info("in CurrentPatient removeCurrentPatient",model)
    if model.id == @model.get('patient.id')
      @$el.empty()

  updateCurrentPatient: ->
    console.info("in current patient updateCurrentPatient")
    patient_id = @model.get('patient.id')
    if patient_id != HandyworksApp.Data.current_patient_id
      patient = HandyworksApp.Data.Patients.get(HandyworksApp.Data.current_patient_id)
      if patient
        @model.set({patient: patient})

  #-----------------------------------------------------------------------------
  # generate a title for the popover which includes a close button. Note,
  # this doesn't work automagically as you would expect for alerts, but it
  # looks good and is easily hooked up through the events hash for this view.
  #-----------------------------------------------------------------------------
  patientSearchTitle: ->
    'Patient Search <button type="button" class="close" data-dismiss="popover">×</button>'

  doPatientSearch: ->
    console.info("in current patient doPatientSearch")
    unless @patientSearch
      @patientSearch = @.$('.patient-search-icon').popover
        html: true
        title: @patientSearchTitle
        content: '<div id="patient-search-container"></div>'
        placement: 'bottom'
        trigger: 'manual'
        template: '<div class="popover" style="width: auto;"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"></div></div></div>'
      @.$('.patient-search-icon').popover('show')
      @renderPatientSearch()
      jQuery('#patient-search-container').html(@patientSearchView.el).closest('.popover').css('width','auto')
      @patientSearchView.renderPatientList()
    else
      @close()

  #-----------------------------------------------------------------------------
  # This is the callback from the mouseleave event on the patient-search
  # interface. It's specifically checking to see if the new element entered
  # by the mouse pointer is the autocomplete selection list. This list lives
  # outside of the patient search element, so it was causing the window to
  # close when trying to select one of the dropdown choices with the mouse.
  #-----------------------------------------------------------------------------
  mouseToClose: (event) ->
    whereAmI = event.toElement
    whereAmI = event.relatedTarget unless whereAmI
    unless jQuery(whereAmI).hasClass("ui-autocomplete")
      @donTClose = false
      _.delay( =>
        console.log("going to close",@donTClose)
        @close() unless @donTClose
      ,1000)

  #-----------------------------------------------------------------------------
  # We're using the mouseToOpen callback to allow a little tolerance on the
  # patient search window closing. If the user gets the mouse back into
  # the search screen within the alloted time we'll block the close.
  #-----------------------------------------------------------------------------
  mouseToOpen: (event) ->
    @donTClose = true

  close: ->
    if @patientSearch
      @.$('.patient-search-icon').popover('hide')
      @patientSearchView.leave()
      @patientSearch = undefined

  renderPatientSearch: ->
    @patientSearchView = new HandyworksApp.Views.PatientSearch
      collection: HandyworksApp.Data.Patients
    @renderChild(@patientSearchView)

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
window.HandyworksApp.Views.PatientSearch =  Support.CompositeView.extend
  id: "patient-search"
  alphabet: "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("")

  events:
    "click a.patient-search-select" : "selectPatient"
    "click a.last-name" : "searchByName"
    "click button.patient-search-button" : "searchForMatch"

  initialize: (options={}) ->
    console.info('initialize patient search view')
    _.bindAll @, "render", "renderPatientList", "selectPatient", "searchByName",
      "limitLetters", "searchForMatch", "acListSource", "acListFocus", "acListSelect",
      "acListFormat"

  #-----------------------------------------------------------------------------
  # renders the patient search view from the dropdown menu. attach an autocomplete
  # widget to the match input field. also focus the match input field
  # NOTE: in order to focus the field, we have to delay the focus() a bit to
  # ensure the field is visible first. its possible the 300ms delay would need
  # to be adjusted if this stops working ... focus() on hidden elements causes
  # failures in IE browsers.
  #-----------------------------------------------------------------------------
  render: ->
    @$el.html(JST['patients/_patient_search']())
    last_names = @$el.find('.last-name-index')
    _.each @alphabet, (letter) ->
      last_names.append(JST['patients/_patient_search_last_name']({letter: letter, label: letter}))
    if HandyworksApp.Data.totalPatientCount <= 500
      last_names.append(JST['patients/_patient_search_last_name']({letter: '%', label: 'all'}))
    @limitLetters()
    match = @$el.find('.patient-search-match')
    match.autocomplete(
      source: @acListSource
      focus: @acListFocus
      select: @acListSelect
      open: (event, ui) ->
        jQuery('ul.ui-autocomplete').css("z-index","1502")
    ).data('autocomplete')._renderItem = @acListFormat
    setTimeout( () ->
      match.focus()
    , 300)
    @

  #-----------------------------------------------------------------------------
  # This solution to format the list entries using properties other than lable
  # and value comes from the jQuery UI custom data example:
  #  http://jqueryui.com/demos/autocomplete/#custom-data
  #-----------------------------------------------------------------------------
  acListFormat: (ul, patient) ->
    console.log("in formatPatientACList",patient)
    return jQuery("<li>")
      .data("item.autocomplete",patient)
      .append("<a>" + patient.listName() + "</a>")
      .appendTo(ul)

  #-----------------------------------------------------------------------------
  # auto complete source data function, uses the Patients collection match
  # method. if there are no currently matching patient records loaded to the
  # client it will "flash" the SEARCH button to suggest the individual click
  # that to look further for a potential match.
  #-----------------------------------------------------------------------------
  acListSource: (request, response) ->
    matches = HandyworksApp.Data.Patients.match(request.term)
    if matches? and matches.length > 0
      @renderPatientList(matches)
      @.$('button.patient-search-button').hide().show('highlight')
      unless @pov
        @pov =  @.$('button.patient-search-button').popover(
          html: true
          content: '<span style="font-size: 12px; text-align: center;">Please click the button</span>'
          placement: 'bottom'
          trigger: 'manual'
          template: '<div id="pov" class="popover" style="width: auto; padding: 10px;"><div class="arrow-s"></div><div class="popover-content" style="padding: 0; line-height: 12px;"></div></div>'
        )
      @pov.popover('hide') if @pov
    else
      @.$('button.patient-search-button').css({'display': "inline-block",'background-image': "none",'background-color': 'rgb(255, 255, 153)'})
      @pov.popover('show')  unless $("#pov").hasClass('in')


    response(matches)

  #-----------------------------------------------------------------------------
  # auto complete option list focus, stops the field from being updated which
  # would blank it out.
  #-----------------------------------------------------------------------------
  acListFocus: (event,ui) ->
    false # stops the field from updating

  #-----------------------------------------------------------------------------
  # auto complete option selected, will now update the field and re-render the
  # selected list.
  #
  # if the user selects a patient from the list then it will become the current
  # patient and close the search interface.
  #-----------------------------------------------------------------------------
  acListSelect: (event,ui) ->
    patient = ui.item
    HandyworksApp.Data.CurrentPatient.set({patient: patient})
    false # false stops the field from being blanked out by the regular select/update process

  #-----------------------------------------------------------------------------
  # this method renders the list of patients which will be displayed on the '
  # lower portion of the patient search interface. if the patients argument is
  # empty it will pull the 10 most recently used patient records from the global
  # patients collections.
  #-----------------------------------------------------------------------------
  renderPatientList: (patients) ->
    @.$('button.patient-search-button').css({'display': "inline-block",'background-image': "none",'background-color': ''})
    unless patients?
      patients = @collection.getMru(10)
    console.log("in patient search renderPatientList",patients)
    list = @.$('.patient-search-list table tbody')
    list.empty()
    _.each patients, (patient) ->
      res = JST['patients/_patient_search_patient']({patient: patient})
      list.append(res)

  #-----------------------------------------------------------------------------
  # this method is used when the user clicks on a patient from the listing
  # it is NOT what gets called when the user is interacting with the autocomplete
  # interface to filter the list.
  #-----------------------------------------------------------------------------
  selectPatient: (event) ->
    console.log("in patient search selectPatient",event)
    id = jQuery(event.target).data('patient-id')
    patient = HandyworksApp.Data.Patients.get(id)
    HandyworksApp.Data.CurrentPatient.set({patient: patient})

  #-----------------------------------------------------------------------------
  # This method is triggered whenever the patient search is rendered to update
  # the list of usable last name letters, so only letters that actually correspond
  # to patients will appear highlighted.
  #
  # The results of this query are cached to improve efficiency on subsequent
  # renderings of the patient search interface, when it is unlikely the results
  # will have changed.
  #
  # FIXME: need to build in some mechanism to invalidate the cache or update
  # the cached letters in the background
  #-----------------------------------------------------------------------------
  limitLetters: ->
    letters = HandyworksApp.Data.Cache.get('patient_last_name_letters')
    if letters?
      @_limitLetters(letters)
    else
      jQuery.getJSON "#{root_path}interaction/patients/letters", (data, status, xhr) =>
        if data? and data.length > 0
          HandyworksApp.Data.Cache.set('patient_last_name_letters',data)
          @_limitLetters(data)

  #-----------------------------------------------------------------------------
  # Go through and enable each of the letters specified in the list. Also we'll
  # manually enable the 'all' option if its present.
  #-----------------------------------------------------------------------------
  _limitLetters: (letters) ->
    @.$('a.last-name').addClass('disabled')
    container = @.$('.last-name-index')
    _.each letters, (letter) ->
      container.find('a[data-letter="' + letter + '"]').removeClass('disabled')
    container.find('a[data-letter="%"]').removeClass('disabled')

  #-----------------------------------------------------------------------------
  # this method will query the server to find all patients whose last name
  # begins with the selected target letter.
  #-----------------------------------------------------------------------------
  searchByName: (event) ->
    link = jQuery(event.target)
    unless link.hasClass('disabled')
      name_starts_with = jQuery(event.target).data('letter')
      console.log("in patient search by name",name_starts_with)
      # resquest data from server
      lastXhr = jQuery.getJSON "#{root_path}interaction/patients", {lname: name_starts_with}, (data, status, xhr) =>
        if xhr is lastXhr
          console.log('--> returning dynamic client list',@,data,name_starts_with)
          patients = new HandyworksApp.Collections.Patients(data)
          @renderPatientList(patients.models)
          HandyworksApp.Data.Patients.add(patients.models)

  #-----------------------------------------------------------------------------
  # This method is called when the user clicks the SEARCH button in the patient
  # search form. It specifically requests the matching results through the
  # patients index.
  #-----------------------------------------------------------------------------
  searchForMatch: (event) ->
    match = @.$('input.patient-search-match').val()
    console.log('in patient searchForMatch',match)
    if match?
      # resquest data from server
      lastXhr = jQuery.getJSON "#{root_path}interaction/patients", {term: match}, (data, status, xhr) =>
        if xhr is lastXhr
          console.log('--> returning dynamic client list',@,data,match)
          patients = new HandyworksApp.Collections.Patients(data)
          @renderPatientList(patients.models)
          HandyworksApp.Data.Patients.add(patients.models)
