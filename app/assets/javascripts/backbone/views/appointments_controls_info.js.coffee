#===============================================================================
#
#===============================================================================
window.HandyworksApp.Views.AppointmentsControlsInfo =  Support.CompositeView.extend

  initialize: (options={}) ->
    _.bindAll @, "render", "cleanUp", "unfocus"
    # we're binding to change events on the AppointmentInfo model as well as
    # the appointment itself. think of the appointment info model as the pointer
    # to the current data, so we want to know if either the pointer or the data
    # changes.
    @bindTo @model, "change", @render if @model
    @bindTo HandyworksApp.dispatcher, "showhide", @unfocus
    HandyworksApp.Data.Appointments.on("remove", @cleanUp)

  #-----------------------------------------------------------------------------
  # Render the Appointment Info view.
  # This view will either let the user select a different Provider or Room 
  # depending on which view the calendar is currently displaying (view_by in
  # ['room', 'provider']) since the other variable can be controlled with 
  # drag and drop.
  #-----------------------------------------------------------------------------
  render: ->
    appointment = @model.get('appointment')
    view_by = HandyworksApp.Data.CalendarOption.get('view_by')
    # need to cleanup child views, the next two lines accomplish that
    # without at least 1 of these lines, there was a horrendous memory leak as you'd
    # go through the appointment info interface to make changes.
    @_leaveChildren()
    @$el.empty()
    if appointment
      @did = appointment.id
      @$el.html(JST['appointments/_controls_info']({ appointment: appointment, view_by: view_by }))
      @bindTo appointment, "change", @render
      @renderPatients()
      @renderAppointmentTypes()
      if "room" == view_by
        @renderProviders()
      else
        @renderRooms()
      @renderNotes()
    @

  renderProviders: ->
    providersContainer = @.$("#provider_control_info_list")
    providersView = new HandyworksApp.Views.AppointmentsControlsInfoProviders
      el: providersContainer
      model: @model
    @renderChildInto(providersView)

  renderAppointmentTypes: ->
    appointmentTypesContainer = @.$("#service_control_info_list")
    appointmentTypesView = new HandyworksApp.Views.AppointmentsControlsInfoAppointmentTypes
      el: appointmentTypesContainer
      model: @model
    @renderChildInto(appointmentTypesView)

  renderRooms: ->
    roomsContainer = @.$("#room_control_info_list")
    roomsView = new HandyworksApp.Views.AppointmentsControlsInfoRooms
      el: roomsContainer
      model: @model
    @renderChildInto(roomsView)

  renderPatients: ->
    patientsContainer = @.$("#patient_control_info_search")
    patientsView = new HandyworksApp.Views.AppointmentsControlsInfoPatients
      el: patientsContainer
      model: @model.get('appointment')
    @renderChildInto(patientsView)

  renderNotes: ->
    notesContainer = @.$("#appointment_notes")
    notesView = new HandyworksApp.Views.AppointmentsControlsInfoNotes
      el: notesContainer
      model: @model.get('appointment')
    @renderChildInto(notesView)

  #-----------------------------------------------------------------------------
  # This method is triggered when an appointment is removed. If the current 
  # appointment reference is no longer valid, remove it from the AppointmentInfo
  # model, which will trigger a re-rendering of this view w/o the appointment
  # info content.
  #-----------------------------------------------------------------------------
  cleanUp: ->
    unless HandyworksApp.Data.Appointments.get(@did)
      @unfocus()

  #-----------------------------------------------------------------------------
  # This is called to unfocus the current appointment. This is done when 
  # returning to the calendar view, or perhaps when the appointment information
  # might have changed through some other interface.
  #
  #-----------------------------------------------------------------------------
  unfocus: ->
    # for some reason this line calling unset generates an exception from the
    # backbone-nested plugin. I'm guessing its a bug in the plugin, but this
    # call to set has the desired effect.
    #@model.unset('appointment') if @model.has('appointment')
    @model.set({appointment: undefined})

#===============================================================================
#
#===============================================================================
window.HandyworksApp.Views.AppointmentsControlsInfoProviders =  Support.CompositeView.extend
  initialize: (options={}) ->
    _.bindAll @, "render"

  render: ->
    @renderProviderList()
    @

  renderProviderList: ->
    _provider = (provider) ->
      unless provider is @model.get('appointment').getProvider()
        providerView = new HandyworksApp.Views.AppointmentsControlsInfoProvider
          model: provider
        @appendChildTo(providerView,@el)
    HandyworksApp.Data.Providers.forEach _provider, @

window.HandyworksApp.Views.AppointmentsControlsInfoProvider =  Support.CompositeView.extend
  tagName: "li"
  events:
    "click .appointment-provider" : "changeProvider"

  initialize: (options={}) ->
    _.bindAll @, "render", "changeProvider"

  render: ->
    @$el.html(JST['appointments/_controls_info_provider']({ provider: @model }))
    @

  changeProvider: (event) ->
    provider_id = jQuery(event.target).data('value')
    appointment = HandyworksApp.Data.AppointmentInfo.get('appointment')
    # closes the closest accordion element.
    #jQuery(event.target).closest('.accordion-toggle').click()
    appointment.save({provider_id: provider_id})

#===============================================================================
#
#===============================================================================
window.HandyworksApp.Views.AppointmentsControlsInfoAppointmentTypes =  Support.CompositeView.extend
  initialize: (options={}) ->
    _.bindAll @, "render"

  render: ->
    @renderAppointmentTypeList()
    @

  renderAppointmentTypeList: ->
    _appointmentType = (appointmentType) ->
      unless appointmentType is @model.get('appointment').getAppointmentType()
        appointmentTypeView = new HandyworksApp.Views.AppointmentsControlsInfoAppointmentType
          model: appointmentType
        @appendChildTo(appointmentTypeView,@el)
    HandyworksApp.Data.AppointmentTypes.forEach _appointmentType, @

window.HandyworksApp.Views.AppointmentsControlsInfoAppointmentType =  Support.CompositeView.extend
  tagName: "li"
  events:
    "click .appointment-appointment_type" : "changeAppointmentType"

  initialize: (options={}) ->
    _.bindAll @, "render", "changeAppointmentType"

  render: ->
    @$el.html(JST['appointments/_controls_info_appointment_type']({ appointment_type: @model }))
    @

  changeAppointmentType: (event) ->
    appointmentType_id = jQuery(event.target).data('value')
    appointment = HandyworksApp.Data.AppointmentInfo.get('appointment')
    # closes the closest accordion element.
    #jQuery(event.target).closest('.accordion-toggle').click()
    appointment.save({appointment_type_id: appointmentType_id})
#===============================================================================
#
#===============================================================================
window.HandyworksApp.Views.AppointmentsControlsInfoRooms =  Support.CompositeView.extend
  initialize: (options={}) ->
    _.bindAll @, "render"

  render: ->
    @renderRoomList()
    @

  renderRoomList: ->
    _room = (room) ->
      unless room is @model.get('appointment').getRoom()
        roomView = new HandyworksApp.Views.AppointmentsControlsInfoRoom
          model: room
        @appendChildTo(roomView,@el)
    HandyworksApp.Data.Rooms.forEach _room, @

window.HandyworksApp.Views.AppointmentsControlsInfoRoom =  Support.CompositeView.extend
  tagName: "li"
  events:
    "click .appointment-room" : "changeRoom"

  initialize: (options={}) ->
    _.bindAll @, "render", "changeRoom"

  render: ->
    @$el.html(JST['appointments/_controls_info_room']({ room: @model }))
    @

  changeRoom: (event) ->
    room_id = jQuery(event.target).data('value')
    appointment = HandyworksApp.Data.AppointmentInfo.get('appointment')
    # closes the closest accordion element.
    #jQuery(event.target).closest('.accordion-toggle').click()
    appointment.save({room_id: room_id})

#===============================================================================
#
#===============================================================================
window.HandyworksApp.Views.AppointmentsControlsInfoNotes =  Support.CompositeView.extend

  events:
    "click #update_notes" : "updateNotes"

  initialize: (options={}) ->
    _.bindAll @, "render", "updateNotes"

  render: ->
    @$el.html(JST['appointments/_controls_info_notes']({ appointment: @model }))
    @

  updateNotes: (event) ->
    event.preventDefault()
    new_notes = @.$('#appointment_notes').val()
    @model.save
      'event.notes': new_notes

#===============================================================================
#
#===============================================================================
window.HandyworksApp.Views.AppointmentsControlsInfoPatients =  Support.CompositeView.extend
  events:
    "click #appointment_client_mode_0" : "doExistingPatient"
    "click #appointment_client_mode_2" : "doNewPatient"
    "click #create_patient" : "createPatient"
    "click #calendar_controls_info_patient_more" : "doPatientSearch"

  patientSearch: false

  initialize: (options={}) ->
    _.bindAll @, "render", "doExistingPatient", "doNewPatient", "createPatient",
      "doPatientSearch", "changePatient"
    @bindTo HandyworksApp.Data.CurrentPatient, "change:patient", @changePatient

  render: ->
    @$el.html(JST['appointments/_controls_info_patients']({ patient: @model.getPatient() }))
    @renderPatientList()
    #jQuery("input#appointment_client_mode_" + @model.getMode()).attr('checked', true)
    @

  renderPatientList: ->
    container = @.$('.patient-selection')
    _patient = (patient) ->
      unless patient is @model.getPatient()
        patientView = new HandyworksApp.Views.AppointmentsControlsInfoPatient
          model: patient
        @appendChildTo(patientView,container)
    _.forEach HandyworksApp.Data.Patients.getMru(4), _patient, @
    container.append(JST['appointments/_controls_info_patients_more']())

  #-----------------------------------------------------------------------------
  # Create a new patient record for this appointment
  #-----------------------------------------------------------------------------
  createPatient: (event) ->
    console.info('creating patient')
    event.preventDefault()
    c_form = @$('#appointment_client_form')
    validator = c_form.validate
      rules:
        'appointment_client[first_name]':
          required: true
        'appointment_client[last_name]':
          required: true
        'appointment_client[phone]':
          required: true
    if validator.form() and validator.pendingRequest == 0
      $.rails.disableFormElements(c_form)
      first_name = @$('#appointment_client_first_name').val()
      last_name = @$('#appointment_client_last_name').val()
      phone = @$('#appointment_client_phone').val()
      @model.newPatient(first_name, last_name, phone)
    else
      c_form.validate().showErrors()

  #-----------------------------------------------------------------------------
  # setup the form and validations to look to create a new patient
  #-----------------------------------------------------------------------------
  doNewPatient: ->
    console.info('in appointment details contact new patient')
    @$('.patient-selection').hide()
    @$('.patient-create').show()
    HandyworksApp.phoneMask('#appointment_client_phone')

  #-----------------------------------------------------------------------------
  # setup the form to search for existing patient.
  #-----------------------------------------------------------------------------
  doExistingPatient: ->
    console.info('in appointment details contact existing patient')
    @$('.patient-create').hide()
    @$('.patient-selection').show()
    
  #-----------------------------------------------------------------------------
  # This method is going to transfer the focus between the appointment info
  # and the patient search interfaces.
  #-----------------------------------------------------------------------------
  doPatientSearch: (event) ->
    jQuery('.patient-search-icon').click()
    # closes the closest accordion element.
    #jQuery(event.target).closest('.accordion-toggle').click()
    jQuery('a.accordion-toggle[href="#info_patient"]').click()
    @patientSearch = true

  #-----------------------------------------------------------------------------
  # Callback method from the Patient Search interface. This method is triggered
  # when the patient search is executed.
  #
  # OPTIMIZE: this method is very similar to the changePatient method in the
  #  AppointmentInfoPatient view (below). There's probably some opportunity
  #  to reduce code by combining these methods.
  #-----------------------------------------------------------------------------
  changePatient: ->
    if @patientSearch
      patient = HandyworksApp.Data.CurrentPatient.get('patient')
      unless patient is @model.getPatient()
        @model.patient = patient
        @model.save
          contact_id: patient.get('contact.id')
          'event.title': patient.get('contact.name')
      else
        console.log("keeping the same patient ... do nothing")

window.HandyworksApp.Views.AppointmentsControlsInfoPatient =  Support.CompositeView.extend
  tagName: "li"
  events:
    "click .appointment-patient" : "changePatient"

  initialize: (options={}) ->
    _.bindAll @, "render", "changePatient"

  render: ->
    @$el.html(JST['appointments/_controls_info_patient']({ patient: @model }))
    @

  changePatient: (event) ->
    appointment = HandyworksApp.Data.AppointmentInfo.get('appointment')
    unless @model is appointment.getPatient()
      HandyworksApp.Data.CurrentPatient.set({patient: @model})
      appointment.patient = @model
      appointment.save
        contact_id: @model.get('contact.id')
        'event.title': @model.get('contact.name')
    else
      console.log("keeping the same patient ... do nothing")
      jQuery('a[href="#info_patient"]').click()
