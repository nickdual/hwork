window.HandyworksApp.Views.AppointmentsOptions =  Support.CompositeView.extend
  events:
    "change #calendar_tsph" : "calendarTimeSlotsPerHour"
    "change #calendar_days" : "calendarDays"
    "change #calendar_view_by" : "calendarViewBy"

  initialize: (options={}) ->
    console.log('appointment options view initialized')
    _.bindAll @, "render", "calendarTimeSlotsPerHour", "calendarDays", "calendarViewBy"

  render: ->
    console.log('options rendered')
    @$el.html(JST['appointments/_options']())
    # set the initial time slots per hour / based on model value
    tsph = @model.get('time_slots_per_hour')
    days = @model.get('days')
    view_by = @model.get('view_by')
    @.$('input[name="calendar[time_slots_per_hour]"][value="' + tsph + '"]').attr('checked','checked')
    @.$('input[name="calendar[days]"][value="' + days + '"]').attr('checked','checked')
    @.$('input[name="calendarViewBy"][value="' + view_by + '"]').attr('checked','checked')
    @renderRooms()
    @renderProviders()
    @

  renderRooms: ->
    roomsContainer = @.$("#room_controls")
    roomsView = new HandyworksApp.Views.AppointmentsControlsRooms
      el: roomsContainer
      collection: HandyworksApp.Data.Rooms
    @renderChild(roomsView)

  renderProviders: ->
    providersContainer = @.$("#provider_controls")
    providersView = new HandyworksApp.Views.AppointmentsControlsProviders
      el: providersContainer
      collection: HandyworksApp.Data.Providers
    @renderChild(providersView)


  calendarTimeSlotsPerHour: (event) ->
    console.log('in calendar options calendarTimeSlotsPerHour',event)
    tsph = @.$('input[name="calendar[time_slots_per_hour]"]:checked').val()
    if tsph
      @model.save {time_slots_per_hour: tsph},
        success: (model) ->
          console.info("calendar options save successful",model)
        error: (model) ->
          console.warn("calendar options save failed",model)

  calendarDays: (event) ->
    days = @.$('input[name="calendar[days]"]:checked').val()
    if days
      @model.save {days: days},
        success: (model) ->
          console.info("calendar options save successful",model)
        error: (model) ->
          console.warn("calendar options save failed",model)

  calendarViewBy: (event) ->
    view_by = @.$('input[name="calendarViewBy"]:checked').val()
    if view_by
      @model.save {view_by: view_by},
        success: (model) ->
          console.info("calendar options save successful",model)
        error: (model) ->
          console.warn("calendar options save failed",model)


