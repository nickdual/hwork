# OPTIMIZE : combine the code for AppointmentsControlsRoom(s) and AppointmentsControlsProvider(s) as they are bascially identical.
window.HandyworksApp.Views.AppointmentsControlsRoom =  Support.CompositeView.extend
  tagName: "li"
  events:
    "click input" : "toggleEnabled"

  initialize: (options={}) ->
    console.log('initialize appointments controls room',options)
    _.bindAll @, "render"

  render: ->
    @$el.html(JST['appointments/_controls_room']({ room: @model }))
    @

  toggleEnabled: (event) ->
    console.log("in room control toggleEnabled", event)
    id = parseInt(jQuery(event.target).val())
    enabled = jQuery(event.target).attr('checked')
    # check to make sure this wasn't the last one checked
    if not enabled and not HandyworksApp.Data.Rooms.atLeastOne()
      jQuery(event.target).attr('checked','checked')
      return
    @model.set({enabled: enabled})

window.HandyworksApp.Views.AppointmentsControlsRooms =  Support.CompositeView.extend
  initialize: (options={}) ->
    console.log('initialize appointments controls rooms',options)
    _.bindAll @, "render"

  render: ->
    @renderLayout()
    @renderRoomList()
    @$('#room_control_list').sortable
      placeholder: "ui-state-highlight"
      handle: '.sorter-handle'
      forcePlaceholderSize: true
      forceHelperSize: true
      update: (event, ui) ->
        console.info("sort event", event, ui)
        jQuery('#room_control_list input').each (idx, elem) ->
          jQuery(elem).data('position',idx)
          id = jQuery(elem).val()
          room = HandyworksApp.Data.Rooms.get(id)
          room.set
            position: jQuery(elem).data('position')
        HandyworksApp.Data.Rooms.forEach (room) ->
          # OPTIMIZE : Currently this is going to submit an UPDATE for each room, it only needs to send one if position changes
          #   This is not critical as the room order should not be changed very often, just a little snappier to send 
          #   fewer requests.
          room.save() # if room.hasChanged('position')
        HandyworksApp.dispatcher.trigger('roomposition')
    @$('#room_control_list').disableSelection()
    @

  renderLayout: ->
    @$el.html(JST['appointments/_controls_rooms']())

  renderRoomList: ->
    _room = (room) ->
      roomView = new HandyworksApp.Views.AppointmentsControlsRoom
        model: room
      @appendChildTo(roomView,@.$('#room_control_list'))
    @collection.forEach _room, @
