window.HandyworksApp.Views.Schedule =  Support.CompositeView.extend
  tagName: 'div'
  id: 'handyworks_views_schedule'

  initialize: (options) ->
    _.bindAll @, "render", "error", "handleCalendarConflict"
    console.log('schedule view initialized', @model)

  render: ->
    @handleCalendarConflict()
    @renderLayout()
    @renderControls()
    @renderCalendar()
    #@renderRates()
    @renderExceptions()
    @.$('#schedule_tabs').tabs
      show: (event, ui) ->
        HandyworksApp.dispatcher.trigger("scheduletabshow",ui)
    # if we have a done button close the form on done.
    @close_schedule()
    @
    
  #-----------------------------------------------------------------------------
  # A bug was found when moving the time blocks on the schedule view. If the 
  # the Appointment Calendar was already open the schedule blocks would 
  # disappear when dragged. This is likely due to some conflict in the 
  # calendar plugin which is confusing which calendar area is active. This 
  # solution is close out the appointment calendar view so it will not 
  # conflict.
  #
  # TODO : this code would make more sense in the Appointments view, or
  #        at least the portion of the code that knows the details of how
  #        the view should be cleaned up.
  #-----------------------------------------------------------------------------
  close_schedule: ->
    jQuery('button.done').click (event) =>
      id = jQuery('button.done').attr('id').substr(0)
      pid = '#provider-'+ id
      console.info("done button pressed!",event)
      @leave()
      jQuery.fancybox.close()
      $.ajax
        type: "GET"
        url: "/providers/getData"
        data:
          "p-id": 'provider-'+ id
        dataType: "json"
        success: (data) ->
          $(pid).find('td:last').text(data)
        error: (e) ->
          alert e
  handleCalendarConflict: ->
    if jQuery('#appointment_control_button.loaded').length > 0
      a = HandyworksApp.router.main.activeViews.appointment
      a._leaveChildren()
      jQuery('#appointment_control_button.loaded').removeClass('loaded')

  renderLayout: ->
    console.log('render schedule layout',@el)
    @$el.html(JST['schedules/_layout']())

  renderControls: ->
    console.log('render controls')
    controlsContainer = @.$('#schedule_controls')
    controlsView = new HandyworksApp.Views.ScheduleControls
      el: controlsContainer
    @renderChild(controlsView)

  renderCalendar: ->
    console.log('render schedule calendar')
    calendarContainer = @.$('#schedule_calendar')
    calendarView = new HandyworksApp.Views.ScheduleCalendar
      collection: @model.events
      el: calendarContainer
    @renderChild(calendarView)

  renderRates: ->
    console.log('render schedule rates')
    ratesContainer = @.$('#schedule_rates')
    ratesView = new HandyworksApp.Views.ScheduleRates
      model: @model
      el: ratesContainer
    @renderChild(ratesView)

  renderExceptions: ->
    console.log('render schedule exceptions')
    exceptionsContainer = @.$('#schedule_exceptions')
    exceptionsView = new HandyworksApp.Views.ScheduleExceptions
      collection: @model.exceptions
      el: exceptionsContainer
    @renderChild(exceptionsView)

  error: (message) =>
    console.info('in Views.Schedule.error',message)
    HandyworksApp.showError(message,'#schedule_notifications',@el)
