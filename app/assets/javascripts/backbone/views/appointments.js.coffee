window.HandyworksApp.Views.Appointments =  Support.CompositeView.extend
  tagName: 'div'
  id: 'appointment_views'

  initialize: ->
    console.log('appointment view initialized')
    _.bindAll @, "render", "newAppointment", "appointmentDetails", "refresh",
      "_refresh", "doRefresh", "doLayout"
    @bindTo HandyworksApp.dispatcher, "eventsloaded", @refresh
    @bindTo HandyworksApp.dispatcher, "eventsinsync", @refresh
    @bindTo HandyworksApp.dispatcher, "eventsoutofsync", @doRefresh
    @bindTo HandyworksApp.dispatcher, "showhide", @doLayout

  render: ->
    console.log('appointment view render',@el)
    @renderLayout()
    @renderControls()
    @renderCalendar()
    @renderOptions()
    @refresh()
    @printview()
    @done_ex_pdf()
    @

  renderLayout: ->
    console.log('render appointments layout')
    @$el.html(JST['appointments/_appointments']())
    container = @.$('#calendar_layout')
    container.css('height', HandyworksApp.getAppointmentCalendarHeight() + 'px')
    @layout = container.layout
      defaults:
        scrollToBookmarkOnLoad: false # handled by custom code so can 'unhide' section first
        initClosed: false
      west:
        size: 250
        spacing_closed: 22
        togglerLength_closed: 165
        togglerAlign_closed: "top"
        togglerContent_closed: "C<br>o<br>n<br>t<br>r<br>o<br>l<br>s"
        togglerTip_closed: "Open & Pin Controls"
        sliderTip: "Slide Open Controls"
        slideTrigger_open: "mouseover"
      east:
        initClosed: true
        #size: 250
        spacing_closed: 22
        togglerLength_closed: 165
        togglerAlign_closed: "top"
        togglerContent_closed: "O<br>p<br>t<br>i<br>o<br>n<br>s"
        togglerTip_closed: "Open & Pin Options"
        sliderTip: "Slide Open Options"
        slideTrigger_open: "mouseover"
      center:
        minHeight: 300
        minWidth: 300

  doLayout: ->
    @layout.resizeAll()

  renderControls: ->
    console.log('render appointments controls')
    controlsContainer = @.$('#calendar_controls')
    controlsView = new HandyworksApp.Views.AppointmentsControls
      el: controlsContainer
      model: @model
    @renderChild(controlsView)

  renderOptions: ->
    console.log('render appointments options')
    optionsContainer = @.$('#calendar_options')
    optionsView = new HandyworksApp.Views.AppointmentsOptions
      el: optionsContainer
      model: HandyworksApp.Data.CalendarOption
    @renderChild(optionsView)

  renderCalendar: ->
    console.log('render appointments calendar')
    calendarContainer = @$('#calendar')
    @calendarView = new HandyworksApp.Views.AppointmentsCalendar
      el: calendarContainer
      model: @model
    @calendarView.on("newappointment",@newAppointment)
    @calendarView.on("appointmentpopup",@appointmentDetails)
    @renderChild(@calendarView)

  newAppointment: (data) ->
    console.info("in appointments calendar newAppointment ", data)
    HandyworksApp.Data.Appointments.createAppointment(data)

  #-----------------------------------------------------------------------------
  # This method will update the AppointmentInfo display by updating the targeted
  # appointment in HandyworksApp.Data.AppointmentInfo
  #
  # It will also update the CurrentPatient 
  #-----------------------------------------------------------------------------
  appointmentDetails: (calEvent) ->
    appointment = HandyworksApp.Data.Appointments.get(calEvent.id)
    console.log("in appointments appointmentDetails",appointment.get('event.title'))
    if appointment?
      HandyworksApp.Data.AppointmentInfo.set({appointment: appointment})
      patient = appointment.getPatient()
      HandyworksApp.Data.CurrentPatient.set({patient: patient}) if patient?

  #-----------------------------------------------------------------------------
  # The following 3 methods are basically the hooks to maintain the calendar
  # data in sync with the server. They perform the following operations:
  #
  #   refresh: 
  #     this is the timer routine. it will be initialized when this view is
  #     initially rendered, its also going to be reset after each event
  #     in-sync or out-of-sync event resulting from the call to _refresh
  #
  #   _refresh:
  #     this is the worker routine, which will request the current 'version'
  #     from the server, if the current version doesn't match the client 
  #     version, it will trigger an out-of-sync event.
  #
  #   doRefresh:
  #     this is the result of the out-of-sync event. it will re-load the 
  #     appointments from the server.
  #-----------------------------------------------------------------------------
  refresh: () ->
    d = HandyworksApp.Data
    clearTimeout(d.refreshTimer) if d.refreshTimer
    d.refreshTimer = setTimeout @_refresh, HandyworksApp.Configuration.refreshRate

  _refresh: () ->
    unless HandyworksApp.Data.refreshBlock
      console.log("in _refresh for version",@model.get('version'))
      @model.checkEventData()
    else
      # need to schedule another check.
      @refresh()

  doRefresh: () ->
    console.log("in doEventRefresh SKIPPED!")
    @model.loadEventData()
    HandyworksApp.dispatcher.trigger('eventsloaded')

  printview: ()->
    obj = this;
    $('#daily_appointment_view').click((e) ->
      value_time = $('input#time_selected').val()
      if value_time != ''
        date = new Date(value_time)
      else
        date = new Date()
      collection = HandyworksApp.Data.Appointments
      room_collection =  HandyworksApp.Data.Rooms
      dayofweek = date.getDay()
      dayofweek = obj.day_of_week(dayofweek)
      day = date.getDate()
      month = date.getMonth() + 1
      appointment_array = []  # array contain appointment
      room_ids = [] #room array contain rooms id
      time_array = ['**']
      if (month < 10)
        month = '0' + month
      year = date.getFullYear()
      fulltime = "Daily Appointments Listing: "+dayofweek+" " +day+"/"+month+"/"+year
      today = year+"-"+month+"-"+day
      _.each(collection.models,  (item) ->
        if item.get('event').start_date == today
          time = item.get('event').start_time
          flag = obj.check_present_time(time_array,time)
          if flag == 0
            time_array.push(time)
          appointment_array.push([obj.format_time(time),item.get('room_id'),item.get('event').title])
      , this)
      obj.format_time_array(time_array)
      time_array.push('Total')
      for i in [0...time_array.length ]
        if time_array[i] != '**' &&  time_array[i] != 'Total'
          time_array[i] = obj.format_time(time_array[i])
      $('#calendar_content').append(JST['appointments/_appointments_view']())
      $('#calendar_layout').addClass('hide')
      document.getElementById('full_time_today').innerHTML = fulltime

      _.each(room_collection.models,  (room) ->
        $('#room_name').append(JST['appointments/_appointments_room']({room_name: room.get('name')}))
        room_ids.push(room.id)
      , this)
      _.each(time_array,  (time) ->
        $('.body-appointment').append(JST['appointments/_appointments_time']({time: time}))
        _.each(room_collection.models,  (room) ->
          app_name = obj.getAppointmentName(appointment_array,room.get('id'),time)
          $('.body-appointment').find('tr.time-tr:last').append(JST['appointments/_appointments_td_template']({room_id: room.get('id'), appointment: app_name}))
        , this)
      , this)
      arr_room = {x: room_ids}
      string_href = '/appointments/export_pdf.pdf?fulltime='+fulltime+'&today='+today+'&room_ids='+JSON.stringify(arr_room)
      link = document.getElementById("export_to_pdf")
      link.setAttribute("href",string_href)
    )

  done_ex_pdf:() ->
    $('#done_ex_pdf').live('click',(e)->
      $('#calendar_content').find('#calendar_print_view').remove()
      $('#calendar_content').find('#calendar_layout').removeClass('hide')
      return false
    )

  check_present_time:(time_array,date) ->
    for i in [1...time_array.length ]
      if date == time_array[i]
        return 1
    return 0

  format_time_array:(time_array) ->
    time_array.sort( (a, b) ->
      return new Date(a) - new Date(b))
    for i in [1...time_array.length - 1 ]
      if time_array[i] != '**' || time_array[i + 1] != '**'
        date1 = new Date(time_array[i + 1])
        h1 = date1.getHours()
        date2 =  new Date(time_array[i])
        h2 = date2.getHours()
        if h1 - h2 > 1
          time_array.splice(i + 1, 0, "**")
    if time_array.length > 2
      time_array.splice(0, 1)
      time_array.push('**')


  format_time:(item)->
    d = new Date(item)
    h = d.getHours()
    m = d.getMinutes()
    if m == 0
      m = '00'
    if h > 12
      time = (h %12)+':'+m+'PM'
    else if h == 12 && m > 0
      time =  (h %12)+':'+m+'PM'
    else
      time =  h+':'+m+'AM'
    return time

  day_of_week:(dayofweek) ->
    if dayofweek == 0
      return 'Sunday'
    else if dayofweek == 1
      return 'Monday'
    else if dayofweek == 2
      return 'Tuesday'
    else if dayofweek == 3
      return 'Wednesday'
    else if dayofweek == 4
      return 'Thursday'
    else if dayofweek == 5
      return 'Friday'
    else if dayofweek == 6
      return 'Saturday'

  getAppointmentName:(array_data,room_id,time) ->
    console.log array_data
    result = ''
    if time == 'Total'
      temp = 0
      _.each(array_data,  (data) ->
        if data[1] == room_id
          temp += 1
      )
      return temp
    else if time != '**'
      _.each(array_data,  (data) ->
        if data[0] == time && data[1] == room_id
          result =  data[2]
      )
      return result
    else
      return ''
