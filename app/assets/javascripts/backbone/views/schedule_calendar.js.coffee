#-------------------------------------------------------------------------------
# HandyworksApp.Views.ScheduleCalendar
#  provides the Backbone style integration with the jQuery.weekCalendar 
#  plugin ( https://github.com/themouette/jquery-week-calendar/wiki )
#-------------------------------------------------------------------------------
window.HandyworksApp.Views.ScheduleCalendar =  Support.CompositeView.extend
  defaults:
    mode:  'tbd' # TODO : remove defaults if I don't need them.

  events:
    'createEventError' : 'createEventError'

  initialize: (options) ->
    #_.defaults(options,@defaults)
    _.bindAll @, "render", "reset", "displayEvents", "openEventDialog",
      "scheduleEventRender", "dropEvent", "newEvent",
      "resizeEvent", "deleteEvent", "clickEvent"
    @bindTo @collection,'reset',@reset
    @bindTo @collection,'createEventError',@createEventError
    @bindTo @collection,'updateEventError',@updateEventError
    @bindTo @collection,'deleteEventError',@deleteEventError
    @bindTo @collection,'updateEventLocationError',@updateEventLocationError
    @bindTo @collection,'updateEventLocationSuccess',@updateEventLocationSuccess
    @bindTo HandyworksApp.dispatcher, "scheduleready", @reset

  #-----------------------------------------------------------------------------
  # renders the calendar to define schedules for providers and rooms 
  #   documentation: https://github.com/themouette/jquery-week-calendar/wiki/Script-options
  #-----------------------------------------------------------------------------
  render: ->
    console.log('schedule calendar render')
    @calendar = @$el.weekCalendar
      showHeader: false
      date: Date.parse('Mon, 3 Jan 2000 07:00:00')
      daysToShow: 7
      firstDayOfWeek: HandyworksApp.Configuration.firstDay
      textSize: 10
      newEventText: HandyworksApp.Configuration.defaultScheduleEventTitle
      displayOddEven:true
      allowCalEventOverlap: true
      allowEventDelete: true
      allowCalEventOverlap: true
      # TODO : could make business start and end time account preferences
      businessHours :{start: 8, end: 18, limitDisplay: false },
      buttons: false # no navigation on schedule
      title: (date,calendar) ->
        'Schedule'
      getHeaderDate: (date,calendar) ->
        date.toString('dddd')
      timeslotHeight: 10
      defaultEventLength: 4
      height:  (@calendar) ->
        HandyworksApp.getCalendarPopupHeight(0.6)
      data: (start, end, callback) =>
        callback(@displayEvents(start,end))
      eventBody: (calEvent, calendar) ->
        '&nbsp;'
      eventRender: @scheduleEventRender
      eventDrop: @dropEvent
      eventNew: @newEvent
      beforeEventNew: HandyworksApp.beforeEventNew
      eventResize: @resizeEvent
      eventDelete: @deleteEvent
      eventClick: @clickEvent
      timeHeaderCellAdjustment: (calendar) ->
        12 # this is not a standard option, just my proposed solution to an alignment problem
    @

  reset: ->
    console.info('reset calendar events')
    @$el.weekCalendar("refresh")
    # FIXME : to get the scrollToHour working in Chrome, it requires a little delay
    #         Firefox did well using _.defer. Alternatively, this should be corrected
    #         by the plugin itself, although it may be suffering from the same browser
    #         behaviours seen when calling the procedure manually.
    #
    #  As for the timing here, 50 is too short, 100 seems reliable.
    scroll = =>
      console.info('scrollToHour',@)
      @$el.weekCalendar("scrollToHour",8)
    _.delay(scroll,100)

  handleError: (options) ->
    console.info('in handleError',options)

  displayEvents: (start, end) ->
    v = @collection.getCalEvents()
    console.info('displaying events to calendar',v,start, end)
    data =
      options: {}
      events: v

  #-----------------------------------------------------------------------------
  # Custom Render Method
  #   * adds a custom class to these event objects which will be used to 
  #     differentiate between schedule and appointment events.
  #   * adds a custom class to these events if they are missing information
  #     which means the user would need to update them typically by clicking
  #     on the event
  #-----------------------------------------------------------------------------
  scheduleEventRender: (calEvent, $event) ->
    console.log('in schedule calendar scheduleEventRender',calEvent,$event)
    $event.addClass("wc-scheduleEvent")
    if HandyworksApp.Data.ProviderScheduleLocationRequired and ( not calEvent.clinics? or calEvent.clinics.length == 0 )
      $event.addClass("wc-cal-event-invalid")

  dropEvent: (newCalEvent,oldCalEvent,element) ->
    console.info('in dropEvent',newCalEvent,oldCalEvent,element)
    @collection.updateCalEvent(newCalEvent,oldCalEvent)

  newEvent: (calEvent,calElement,freeBusyManager,calendar,DomEvent) ->
    console.info('in newEvent',calEvent,calElement,freeBusyManager,calendar,DomEvent)
    @collection.createCalEvent(calEvent)

  clickEvent: (calEvent, element, freeBusyManager, $calendar, DomEvent) ->
    console.info('in clickEvent')
    if HandyworksApp.Data.ProviderScheduleLocationRequired
      console.info('--> clinic information required')
      @openEventDialog(calEvent)
    else
      console.info('--> clinic information not required')

  openEventDialog: (calEvent) ->
    console.info('open event dialog')
    @$el.append('<div id="schedule_event_dialog"></div>')
    event_dialog_container = @.$('#schedule_event_dialog')
    event_dialog_view = new HandyworksApp.Views.ScheduleEventDialog
      el: event_dialog_container
      collection: @collection
      calEvent: calEvent
    @renderChild(event_dialog_view)

  deleteEvent: (calEvent,calElement,freeBusyManager,calendar,DomEvent) ->
    if confirm('Are you sure?')
      calendar.weekCalendar('removeEvent',calEvent.id)
      @collection.deleteCalEvent(calEvent)

  resizeEvent: (newCalEvent,oldCalEvent,element) ->
    console.info('in resizeEvent',newCalEvent, oldCalEvent, element)
    @collection.updateCalEvent(newCalEvent,oldCalEvent)

  deleteEventError: (calEvent) =>
    console.info('in deleteEventError',calEvent)
    @.$('#schedule_calendar').weekCalendar('updateEvent',calEvent)
    HandyworksApp.router.currentView.error('An error occured when deleting an event.')

  createEventError: (eventId) =>
    console.info('in createEventError',eventId,@)
    @.$('#schedule_calendar').weekCalendar('removeUnsavedEvents')
    HandyworksApp.router.currentView.error('An error occured when creating a new event.')

  updateEventError: (oldCalEvent) =>
    console.info('in updateEventError',oldCalEvent)
    @.$('#schedule_calendar').weekCalendar('updateEvent',oldCalEvent)
    HandyworksApp.router.currentView.error('An error occured when upating an event.')

  updateEventLocationError: (calEvent, clinics) =>
    console.info('in updateEventLocationError',calEvent)
    @.$('#schedule_calendar').weekCalendar('updateEvent',calEvent)
    HandyworksApp.router.currentView.error('An error occured when upating an event location.')

  updateEventLocationSuccess: (calEvent, clinics) =>
    console.info('in updateEventLocationSuccess',calEvent)

  #-----------------------------------------------------------------------------
  # Called from the dialog child view to propogate changes to the locations
  # list to the event object.
  #-----------------------------------------------------------------------------
  updateEventLocation: (calEvent,clinics) ->
    newCalEvent = jQuery.extend(true, {}, calEvent, { clinics: clinics })
    console.log('in schedule calendar updateEventLocation',@,newCalEvent)
    x = $('.wc-new-cal-event')
    x.removeClass('wc-new-cal-event')
    jQuery('#schedule_calendar').weekCalendar('updateEvent',newCalEvent)
    x.addClass('wc-new-cal-event')
    @collection.updateEventLocation(calEvent,clinics)
