window.HandyworksApp.Views.AppointmentsControls =  Support.CompositeView.extend
  events:
    "click .today-button" : "goToToday"

  initialize: (options={}) ->
    console.log('appointment controls view initialized')
    _.bindAll @, "render", "dateSelected", "goToToday"

  render: ->
    console.log('controls rendered')
    @renderLayout()
    @renderNavigator()
    @renderAppointmentInfo()
    @

  renderLayout: ->
    @$el.html(JST['appointments/_controls']())

  renderNavigator: ->
    @.$('#calendar_navigator').datepicker
      minDate: (-1) * @model.get('schedule_history_max_days')
      maxDate: @model.get('schedule_future_max_days')
      firstDay: HandyworksApp.Configuration.firstDay
      defaultDate: @model.get('date')
      onSelect: (dateText, picker) =>
        @dateSelected(dateText, picker)

  renderAppointmentInfo: ->
    appointmentInfoContainer = @.$('.appointment_info')
    HandyworksApp.Data.AppointmentInfo = new HandyworksApp.Models.AppointmentInfo
    appointmentInfoView = new HandyworksApp.Views.AppointmentsControlsInfo
      el: appointmentInfoContainer
      model: HandyworksApp.Data.AppointmentInfo
    @renderChild(appointmentInfoView)

  dateSelected: (dateText, picker) ->
    console.info('you selected a date',@,dateText, picker)
    console.log(dateText)
    newDate = Date.parse(dateText)
    @model.setDate(newDate)
    $('#time_selected').val(dateText)
  goToToday: ->
    console.info("in appointment controls go to today")
    today = new Date(Date.now())
    @.$('#calendar_navigator').datepicker('setDate',today)
    @model.setDate(today)
    $('#time_selected').val(today)