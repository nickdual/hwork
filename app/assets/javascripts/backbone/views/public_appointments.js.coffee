#===============================================================================
# Main composite view for the Public Appointments interface
#===============================================================================
window.HandyworksApp.Views.PublicAppointment = Support.CompositeView.extend
  id: 'pa_main'

  initialize: ->
    console.info('initialize public appointments view')
    _.bindAll @, "render"
    @bindTo @model, "change:clinic_id", @renderTitle
    @bindTo @model, "change", @renderStep

  render: ->
    @renderLayout()
    @renderTitle()
    @renderStep()
    @

  renderLayout: ->
    @$el.html(JST['public_appointments/layout']())

  renderTitle: ->
    @$('.title').html(JST['public_appointments/title']({clinic: HandyworksApp.Data.CurrentClinic}))

  renderStep: ->
    step = @model.getStep()
    @_leaveChildren()
    if step == 'location'
      @renderLocations()
    if step == 'service'
      @renderServices()
    if step == 'staff'
      @renderProviders()
    if step == 'schedule'
      @renderTimes()

  renderLocations: ->
    v = new HandyworksApp.Views.PublicAppointmentLocation
      model: @model
    @renderChildInto(v, @$('.body'))

  renderServices: ->
    v = new HandyworksApp.Views.PublicAppointmentService
      model: @model
    @renderChildInto(v, @$('.body'))

  renderProviders: ->
    v = new HandyworksApp.Views.PublicAppointmentProvider
      model: @model
    @renderChildInto(v, @$('.body'))

  renderTimes: ->
    @model.fetchTimes()
    v = new HandyworksApp.Views.PublicAppointmentTime
      collection: HandyworksApp.Data.Times
    @renderChildInto(v, @$('.body'))

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
window.HandyworksApp.Views.PublicAppointmentLocation = Support.CompositeView.extend
  id: 'pa_location'

  initialize: ->
    console.info('initialize public appointment location')
    _.bindAll @, "render"

  render: ->
    @$el.html(JST['public_appointments/locations']())
    list = @$el.find('.locations_list')
    HandyworksApp.Data.Clinics.forEach (clinic) ->
      list.append(JST['public_appointments/_location']({clinic: clinic}))
    @

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
window.HandyworksApp.Views.PublicAppointmentService = Support.CompositeView.extend
  id: 'pa_service'

  intialize: ->
    console.info('initialize public appointment service')
    _.bindAll @, "render", "selectService"

  render: ->
    @$el.html(JST['public_appointments/services']())
    list = @$el.find('.services_list')
    HandyworksApp.Data.AppointmentTypes.forEach (service) ->
      list.append(JST['public_appointments/_service']({service: service}))
    @

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
window.HandyworksApp.Views.PublicAppointmentProvider = Support.CompositeView.extend
  id: 'pa_provider'

  initialize: ->
    console.info('initialize public appointment provider')
    _.bindAll @, "render"

  render: ->
    @$el.html(JST['public_appointments/providers']())
    list = @$el.find('.providers_list')
    providers = @model.getProviders()
    if providers?.length > 0
      _.each @model.getProviders(), (provider) ->
        c = JST['public_appointments/_provider']({provider: provider})
        console.warn('you displaying',list,provider,c)
        list.append(c)
    else
      list.append('<li class="unavailable">There are currently no providers available.</li>')
    @

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
window.HandyworksApp.Views.PublicAppointmentTimeSlot = Support.CompositeView.extend

  initialize: ->
    _.bindAll @, "render"

  render: ->
    @$el.html(JST['public_appointments/_time']({time: @model}))
    @

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
window.HandyworksApp.Views.PublicAppointmentTime = Support.CompositeView.extend
  id: 'pa_time'

  initialize: ->
    console.info('initialize public appointment time')
    _.bindAll @, "render"
    @bindTo @collection, "reset", @renderTimesList

  render: ->
    @$el.html(JST['public_appointments/times']())
    @renderTimesList()
    @

  renderTimesList: ->

    list = @$el.find('.times_list')
    list.empty()
    _time = (time) ->
      timeView = new HandyworksApp.Views.PublicAppointmentTimeSlot
        model: time
      @appendChildTo(timeView, list)
    @collection.forEach _time, @

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
window.HandyworksApp.Views.PublicAppointmentConfirm = Support.CompositeView.extend
  id: 'pa_confirm'

  initialize: ->
    console.info('initialize public appointment confirm')
    _.bindAll @, "render"

  render: ->
    @$el.html(JST['public_appointments/confirm']())
    @

