window.HandyworksApp.Views.ScheduleExceptions =  Support.CompositeView.extend
  initialize: ->
    _.bindAll @, "render", "showAddException", "hideAddException", "renderAddExceptionForm", "adjust"
    @bindTo @collection,'reset',@reset
    @bindTo @collection,'add',@addException
    @bindTo HandyworksApp.dispatcher, "scheduleready", @ready
    @bindTo HandyworksApp.dispatcher, "popupready", @ready
    @bindTo HandyworksApp.dispatcher, "scheduleclosing", @closing
    @bindTo HandyworksApp.dispatcher, "popupclosing", @closing
    @bindTo HandyworksApp.dispatcher, "scheduletabshow", @adjust

  events:
    'click #add_schedule_exception' : 'showAddException'
  
  addException: (exception) ->
    console.log('in addException',exception)
    if @.$('#schedule_exceptions_list') and @dataTable?
      @renderExceptions(exception)

  renderException: (exception) ->
    console.log('in schedule exception renderException',exception)
    exceptionRecord = new HandyworksApp.Views.ScheduleExceptionRecord
      model: exception
      collection: @collection
    @appendChildTo(exceptionRecord,@.$('#schedule_exceptions_list_body'))

  _drawTable: ->
    console.log('in _drawTable')
    exceptions_list = @.$('#schedule_exceptions_list')
    exceptions_list.show()
    unless @dataTable
      @dataTable = HandyworksApp.indexTable exceptions_list,
        "sScrollY": 150
        "oLanguage":
          "sEmptyTable": "No Current Schedule Exceptions"

  renderExceptions: ->
    exceptions_list_records = @.$('#schedule_exceptions_list_body')
    exceptions_list = @.$('#schedule_exceptions_list')
    console.info('in renderExceptions',@collection)
    unless @collection?.models?.length > 0
      console.log('--> there are no schedule exceptions')
    else
      exceptions_list.hide()
      exceptions_list_records.empty()
      @collection.each (exception) =>
        @renderException(exception)

  reset: (foo) ->
    console.info('reset exception events', foo, @)
    @renderExceptions()

  render: ->
    console.log('schedule exceptions rendered',@collection)
    @$el.html(JST['schedules/_exceptions']())
    @renderExceptions()
    @

  #-----------------------------------------------------------------------------
  # called when the fancybox popup is completed
  #  - in order correctly build the table display, the dimensions of the popup
  #    container must be stable / static, so we may need to redraw the table.
  #    Specifically, this is to correct problems with the header label widths.
  #-----------------------------------------------------------------------------
  ready: ->
    console.info('called Views.Exceptions.ready', @)
    @_drawTable()

  #-----------------------------------------------------------------------------
  # we need to re-adjust the exception table layout whenever the tab is shown
  #-----------------------------------------------------------------------------
  adjust: ->
    console.log("in schedule exceptions adjust")
    # this causes the FB popup window to resize / recenter when a tab is clicked.
    jQuery.fancybox.update()
    @dataTable.fnAdjustColumnSizing() if @dataTable?

  closing: ->
    console.info('called Views.Exceptions.closing',@)
    @dataTable.fnDestroy() if @dataTable?
    @leave()

  #-----------------------------------------------------------------------------
  # The Add Exception form is potentially opened and closed many times, when
  # the view is closed by leave() it is removed from the dom, so this allows 
  # the form element to be re-created and appended to the existing view's
  # DOM tree. This is actually very typical backbone view usage, but is 
  # different than most of the other view work done for Handyworks.
  #-----------------------------------------------------------------------------
  renderAddExceptionForm: ->
    console.log('render add exception form',@collection)
    formView =  new HandyworksApp.Views.ScheduleExceptionForm
      id: 'new_exception_form'
      collection: @collection
    @appendChild(formView)

  #-----------------------------------------------------------------------------
  # This will render the exception form view and push it onto the FB popup
  # stack.
  #-----------------------------------------------------------------------------
  showAddException: ->
    console.log('in showAddException')
    @renderAddExceptionForm()
    HandyworksApp.Popup.push '#new_exception_form',
      afterShow: ->
        jQuery('#new_exception_form').show()
        HandyworksApp.dispatcher.trigger('newexceptionready')
        jQuery.fancybox.update()

  hideAddException: =>
    console.log('in hideAddException')

window.HandyworksApp.Views.ScheduleExceptionForm =  Support.CompositeView.extend
  initialize: (options) ->
    _.bindAll @, "render", "addNewException", "resetEventForm", "setupNewExceptionForm", "pushDate"
    @bindTo @collection,'add',@_resetEventForm
    @bindTo HandyworksApp.dispatcher, "newexceptionready", @setupNewExceptionForm

  events:
    "click #event_submit" : 'addNewException'
    "click #event_reset" : 'resetEventForm'
    "change #event_all_day" : 'pushDate'
    "change #event_start_date" : 'pushDate'

  render: ->
    console.log('in ScheduleExceptionForm render',@collection)
    @$el.html(JST['shared/_new_event_form']())
    @$el.hide()
    @

  setupNewExceptionForm: ->
    console.log('in ScheduleExceptionForm setup new exception form')
    start = @$('#event_start_date')
    console.log("this is the start time: ", start)
    dt_attributes =
      timeFormat: 'hh:mm tt'
      dateFormat: HandyworksApp.Configuration.dateFormat
      firstDay: HandyworksApp.Configuration.firstDay
      stepMinute: 15
      #showOn: 'button'
      #buttonImage: "#{root_path}images/calendar.gif"
      #buttonImageOnly: true
    start.datepicker dt_attributes
    end = @$('#event_end_date')
    console.log("this is the end time: ", end)
    end.datepicker dt_attributes
    @$el.find('#new_event_form').validate
      rules:
        'event[title]':
          required: true
        'event[start_date]':
          required: true
          date: true
        'event[end_date]':
          required: true
          date: true
          dateGreaterThanOrEqual: ['#event_start_date','Start Date']
        'event[start_time]':
          required: ->
            not jQuery('#event_all_day').is(':checked')
        'event[end_time]':
          required: ->
            not jQuery('#event_all_day').is(':checked')

  _resetEventForm: ->
    #form = @.$('#new_event_form')
    #form.validate().resetForm()
    jQuery.fancybox.close()
    @leave()

  #-----------------------------------------------------------------------------
  # This method will be called whenever either he start date changes, or the
  # "All Day" checkbox is selected. It will:
  #   * show/hide the end date field based on the "All Day" setting
  #   * for all day events, it will force the start time to the beginning of 
  #     the day, and set the end_date to the last second of that day.
  #   * for non-all day events, it will push the start time + 1 hr to the 
  #     end time
  #-----------------------------------------------------------------------------
  pushDate: (event) ->
    console.log("in schedule exception form pushDate")
    v = @.$("#event_start_date").val()
    sd = new Date(v) if v
    w = @.$("#event_end_date").val()
    ed = new Date(w) if w
    x = HandyworksApp.Configuration.dateFormatStr # + ' HH:mm'
    if sd and ( not ed or sd.compareTo(ed) >= 0 )
      @.$("#event_end_date").val(sd.toString(x))
    if @.$("#event_all_day").is(":checked")
      @.$(".time").hide()
    else
      @.$(".time").show()


  #-----------------------------------------------------------------------------
  # This procedure is bound to the Cancel button, so it needs to clear the
  # form and exit the dialog (fb).
  #-----------------------------------------------------------------------------
  resetEventForm: (event) ->
    console.log('in resetEventForm')
    event.preventDefault()
    @._resetEventForm()

  #-----------------------------------------------------------------------------
  # follows the same schema as the calendar events.
  #-----------------------------------------------------------------------------
  addNewException: (event) ->
    console.log('in addNewException',event,@el)
    if event?
      console.log('--> event driven, proceed')
      event.preventDefault()
    unless jQuery('#new_event_form').validate().form()
      console.info('there are invalid fields')
      jQuery('#new_event_form').validate().showErrors()
      return false
    start_time = @.$('#event_start_time').val()
    end_time = @.$('#event_end_time').val()
    if @.$('#event_all_day').is(':checked')
      start_time = '00:00'
      end_time = '23:59'
    formEvent =
      title: @.$('#event_title').val()
      notes: @.$('#event_notes').val()
      free: @.$('#event_free').val()
      start: new Date(@.$('#event_start_date').val() + ' ' + start_time)
      end: new Date(@.$('#event_end_date').val() + ' ' + end_time)
    @collection.createExceptionEvent(formEvent)
    return true

window.HandyworksApp.Views.ScheduleExceptionRecord = Support.CompositeView.extend
  tagName: 'tr'
  className: 'schedule_exception_record'

  intialize: (options) ->
    _.bindAll @, "render", "destroy"

  events:
    "click .destroy_exception" : "destroy"

  _formatFields: (data) ->
    formatted_data =
      title: if data.title? then data.title else ''
      notes: if data.notes? then data.notes else ''
      free: if data.free is "true" then 'Available' else 'Unavailable'
      start: data.start.toString('MM/dd/yyyy HH:mm')
      end: data.end.toString('MM/dd/yyyy HH:mm')

  render: ->
    data = @_formatFields(@collection.toCalEvent(@model))
    console.log('in render schedule exception record',@model, data)
    @$el.html(JST['schedules/_exception_record']({ exception: data }))
    @

  destroy: ->
    console.info('in destroy exception',@)
    if not @$el.hasClass('disabled') and confirm('Are you sure?')
      @$el.addClass('disabled')
      @model.destroy
        success: (model,response) =>
          console.info('destroy exception success',@)
          @parent.dataTable.fnDeleteRow(@el)
          @leave()
        error: (model, response) =>
          console.error('destroy exception failed',@el)
          @$el.removeClass('disabled')
          HandyworksApp.router.currentView.error('An error occured when deleting a schedule exception.')
