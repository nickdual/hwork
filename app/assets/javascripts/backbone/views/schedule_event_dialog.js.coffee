window.HandyworksApp.Views.ScheduleEventDialog =  Support.CompositeView.extend

  events:
    "dialogclose" : "dialogClose"
    "click input:checkbox" : "formChanged"

  submissionRequired: false

  initialize: (options) ->
    _.bindAll @, "render"

  render: ->
    console.log('schedule event dialog render',@options)
    @$el.html(JST['schedules/_event_dialog']({ calEvent: @options.calEvent }))
    @$el.dialog
      title: 'Schedule Time Location'
      zIndex: HandyworksApp.Configuration.fbZindex
      modal: true
      width: 500
    HandyworksApp.put_on_style(@el)

  #-----------------------------------------------------------------------------
  # Called when the user closes the dialog. This will save the changes to the
  # clinics list to the associated event model.
  #-----------------------------------------------------------------------------
  dialogClose: ->
    console.log('in event dialogClose',@)
    if @submissionRequired
      clinics = @.$('form#event_clinics input:checkbox:checked').map( ->
        { id: parseInt(jQuery(@).val()) }
      ).get()
      console.info('--> clinics',clinics)
      @parent.updateEventLocation(@options.calEvent,clinics)
    else
      console.log('--> nothing changed')
    @$el.dialog("destroy")
    @$el.empty()
    @leave()
    
  #-----------------------------------------------------------------------------
  # Keeps track of any changes to this form. The form data will only be 
  # submitted to the server if it has been changed.
  #-----------------------------------------------------------------------------
  formChanged: ->
    console.log('in event dialog formChanged')
    @submissionRequired = true

