#===============================================================================
# A collection of functions used for the index table views.
#===============================================================================

_.extend window.HandyworksApp,

  #-----------------------------------------------------------------------------
  # calculate the height for index table displays. we'll take the current
  # window height minus an empirically determined pixel buffer for the header
  # bar and footer margin. if the result is less than 300px then we'll set
  # the table to 300px with the expectation that region will scroll.
  #-----------------------------------------------------------------------------
  indexTableHeight: ->
    #h = Math.floor(jQuery(window).height()*55/100)
    h = Math.max(jQuery(window).height() - 185,300)
    return h + 'px'

  #-----------------------------------------------------------------------------
  # select the indicated row, deselecting any others
  #-----------------------------------------------------------------------------
  indexTableSelectRow: ($row) ->
    if $row and $row.length > 0
      $row.closest('tbody').find('tr').removeClass("row_selected")
      $row.addClass("row_selected")

  #-----------------------------------------------------------------------------
  # this method is used to configure the dataTables widgets throughout
  # Handyworks. See http://datatables.net for additional information about
  # configuring dataTables widgets.
  #
  #   $index_area : jQuery selector which will be initialized with widget
  #   options     : list of dataTable options which will override the common
  #                 defaults being used in Handyworks.
  #         click_cb: custom option to add a handler to a click event within
  #                   the data table, row highlighting is done before this
  #                   function is called. the default function does nothing
  #
  # We've introduced a node based locking mechanism to overcome an issue with
  # dataTables: it was generating multiple requests to the server (a) when the
  # table was initialized and (b) whenver the window was resized. I believe the
  # underlying cause of the duplicated (or nearly duplicated) requests is a
  # bug in dataTables as it makes no sense issue multiple simultaneous requests
  # when the table is initializing. We're also looking at the lock inside of
  # the resize event itself, because if we let it generate a new request
  # via fnServerData, the plugin will wait for the most recent request to
  # complete before turning control over to the user.
  #
  #     .data('server-called') == open calls to the server.
  #-----------------------------------------------------------------------------
  indexTable: ($index_area, options={}) ->
    # expert layout includes the Change Cols (C) setting
    layout = '<"H"r>t<"F"ifp>' unless HandyworksApp.Configuration.expert
    layout = '<"H"Cr>t<"F"ifp>' if HandyworksApp.Configuration.expert
    _resource = $index_area.data('resource')
    $index_area.data('server-called',0)
    _mru = options.mru
    firstRequest = true
    settings =
      "aaSorting": [[1,'asc']] # initialize the tables to sort on the first data column (2nd physical column)
      "bJQueryUI": true
      "bStateSave": true
      "iDisplayLength" : 100
      "sPaginationType": "full_numbers"
      "sScrollY": HandyworksApp.indexTableHeight()
      "sScrollX": "100%"
      "sScrollXInner": "100%"
      "oColVis":
        "buttonText": "change cols"
      "sDom": layout
      "fnDrawCallback": (settings) ->
        console.log("indexTable draw callback",settings, _resource, _mru)
        # highlight the current most recently used record if its in the view
        if _mru?
          HandyworksApp.indexTableSelectRow(jQuery("tr##{_resource}-#{_mru}"))
        # ensure the seleted row is visible in the scrollable area
        if jQuery(".row_selected",$index_area).length > 0
          console.log("going to try to scroll to selected row")
          jQuery(".dataTables_scrollBody").scrollTo(jQuery(".row_selected",$index_area))
        else
          # if we don't have a selected row, then scroll to the top of the list
          jQuery(".dataTables_scrollBody").scrollTo("tr")
      "fnServerParams": (aoData) ->
        # we only want to apply the MRU focusing logic from the server on the
        # initial request, subsequent requests will be for pagination and
        # sorting, and we don't want the MRU logic to override those user
        # navigations.
        console.log("data tables sending to the server ", firstRequest,aoData)
        if firstRequest
          firstRequest = false
          aoData.push
           name : "aaMru"
           value : "1"
        aoData
      "fnServerData": ( url, data, callback, settings ) ->
        console.log("called the server data",data,settings);
        open_calls = $index_area.data('server-called')
        unless open_calls > 0
          $index_area.data('server-called',(open_calls + 1))
          # we're overriding the default method to retrieve data from the server
          # in order to extract the MRU setting before the data is displayed
          # the row highlight will then be handled by the fnDrawCallback
          settings.jqXHR = jQuery.ajax
            "url": url,
            "data": data,
            "complete" : (jqXHR, textStatus) ->
              c_calls = $index_area.data('server-called')
              $index_area.data('server-called',(c_calls - 1))
            "success": (data, textStatus, jqXHR) ->
              console.info("dataTables server side success",data.mru)
              _mru = data.mru
              # if the iDisplayStart returned by the server does not match
              # the current tables _iDisplayStart setting, then we need to
              # for a page update to get the two in sync. this could happen
              # after an add or update causes the MRU to change to a different
              # page.
              if settings._iDisplayStart != data.iDisplayStart
                console.log("data tables server side page change",data)
                settings._iDisplayStart = data.iDisplayStart
                $(settings.oInstance).trigger('page',settings)
              # trigger an initial adjustment to the column sizing without fetching data
              # from the server again (that's apparently what false is accomplishing).
              $index_area.dataTable().fnAdjustColumnSizing(false)
              callback(data)
            "dataType": "json",
            "cache": false,
            "error": (xhr, error, thrown) ->
              if error == "parsererror"
                alert( "DataTables warning: JSON data from server could not be parsed. "+
                  "This is caused by a JSON formatting error." )
      "fnRowCallback": ( nRow, aData, iDisplayIndex, iDisplayIndexFull ) ->
        # fix Bug #242 : row click does not open in FB popup
        #   b/c the events were getting double bound / double triggered
        #   each time the table is sorted. the aData.done is ensuring
        #   this row's callbacks are only registered once.
        unless aData.done
          # need to add a class to each <td> tag to help with styling
          # specifically this is being used to differentiate between selected and non-selected rows when setting :hover styles
          jQuery(nRow).find("td").addClass("row_data")
          aData.done = true
          if aData.id
            jQuery(nRow).data('id',aData.id)

          if aData.delete
            jQuery('td:eq(0)',nRow).html(JST['shared/_delete_link'](path: aData.delete, resource: _resource))
          if aData.alt
            jQuery('td:eq(0)',nRow).append(JST['shared/_alt_link'](path: aData.alt, resource: _resource))
          if aData.edit
            jQuery(nRow).click (event) ->
              console.log("index table row click",event,aData.edit)
              HandyworksApp.indexTableSelectRow(jQuery(nRow))
              # FIXME : the next line is a bit of a kludge to separate the delete events from the row clicks
              #         since we're not handling the events directly its bubling up to the row and this was
              #         the only immediately available location to prevent the edit popup from opening while
              #         delete icon is clicked, the delete icon is an %a element.
              return true if jQuery(event.target).is('a')
              HandyworksApp.editPopupResource(aData.edit, 'index', nRow)
        return nRow
    jQuery.extend(settings,options)
    # check to see if we're getting the data locally or need to setup data source.
    if settings.bServerSide
      settings.sAjaxSource = $index_area.data('source')
    console.log("--> initializing indexTable",settings)
    indexTable = $index_area.dataTable(settings)
    # try to display the current MRU record
    if _mru?
      rs = _.filter indexTable.fnGetNodes(), (n) ->
        jQuery(n).attr('id') == "#{_resource}-#{_mru}"
      if rs.length > 0
        indexTable.fnDisplayRow(rs[0])
    # binding the fnAdjustColumnSizing is necessary to keep the columns sized
    # correctly when the window is resized
    jQuery(window).smartresize (event) ->
      # filter out calendar event resize events
      if event.target is window
        console.log("data tables resize triggered",event)
        if $index_area.is(':visible') and indexTable
          jQuery('div.dataTables_scrollBody').css('height',HandyworksApp.indexTableHeight())
          indexTable.fnAdjustColumnSizing()
    # FIXME : the showhide trigger of fnAdjustColumnSizing() was triggering multiple requests to the server
    HandyworksApp.dispatcher.on "showhide", ->
      open_calls = $index_area.data('server-called')
      if $index_area.is(':visible') and open_calls == 0
        console.info("triggering index table showhide resize",indexTable)
        indexTable.fnAdjustColumnSizing() if indexTable
    # if the switcher dropdown is used to change to a different resource, we
    # want to updated the selected row
    HandyworksApp.dispatcher.on "resourceswitch", (resource_name,resource_id) ->
      a_row = $index_area.find('tr#' + resource_name + '-' + resource_id)
      if a_row.length > 0
        HandyworksApp.indexTableSelectRow(a_row)
    # must return the dataTables object so downstream code can use it.
    return indexTable

  #-----------------------------------------------------------------------------
  # changes the data table icon size, which in turn changes the line height
  #-----------------------------------------------------------------------------
  setIconSize: (event, ui) ->
    console.info('in HandyworksApp set Icon Size',event, ui)
    size = ui.value
    sizeClass = HandyworksApp.Configuration.iconSizeMap[size]
    console.log('--> going to map icon size',size,sizeClass)
    HandyworksApp.switchClass('.dataTables_scrollBody .icon32',"icon32",sizeClass) unless sizeClass == "icon32"
    HandyworksApp.switchClass('.dataTables_scrollBody .icon24',"icon24",sizeClass) unless sizeClass == "icon24"
    HandyworksApp.switchClass('.dataTables_scrollBody .icon16',"icon16",sizeClass) unless sizeClass == "icon16"

  #-----------------------------------------------------------------------------
  # Change the Current Patient context. This method is the callback from
  # clicking on a patient record in the index view.
  #
  # This method is registered as a 'click' event callback on the patients
  # index view. If the event was actually triggered by a delete request we
  # need to ignore and return true to allow the event to continue.
  #
  # The 'id' of the selected resource is now expected to be present in the
  # underlying <tr> data attributes.
  #-----------------------------------------------------------------------------
  changeCurrentPatientCB: (event) ->
    return true if jQuery(event.target).data('method') == 'delete'
    console.info("in Handyworks changeCurrentPatientCB")
    selector = jQuery(event.target).closest('tr')
    try
      new_patient_id = selector.data('id')
      new_patient_id ||= selector.attr('id').split('-')[1]
      if new_patient_id
        HandyworksApp.Data.CurrentPatient.update(new_patient_id)
        HandyworksApp.changeCurrentPatient(new_patient_id)
    catch err
      console.info('error on current patient CB',err)

  #-----------------------------------------------------------------------------
  # This method pushes a new patient_id for the current patient back to the
  # server to be persisted during the users session.
  #-----------------------------------------------------------------------------
  changeCurrentPatient: (new_patient_id) ->
    unless new_patient_id == HandyworksApp.Data.current_patient_id
      HandyworksApp.Data.current_patient_id = new_patient_id
      HandyworksApp.indexTableSelectRow(jQuery('tr#patient-' + new_patient_id))
      jQuery.ajax
        url: "#{root_path}session_settings"
        type: 'PUT'
        data:
          patient_id: new_patient_id
        success: (data, textStatus, jqXHR) ->
          console.info('changeCurrentPatient success',new_patient_id)
        error: (jqXHR, textStatus, errorThrown) ->
          console.error('changeCurrentPatient failure',textStatus,errorThrown)

