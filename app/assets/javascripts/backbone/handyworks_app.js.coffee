#-------------------------------------------------------------------------------
# Handyworks Appointment Scheduling Client Side Components
#-------------------------------------------------------------------------------
#= require_self
#= require_tree ./models
#= require_tree ./collections
#= require_tree ./views
#= require_tree ./routers
#
#= require ./handyworks_app_popup
#= require ./handyworks_app_import
#= require ./handyworks_app_popup_func
#= require ./handyworks_app_index
#
_.extend window.HandyworksApp,
  dispatcher: _.clone(Backbone.Events)
  #-----------------------------------------------------------------------------
  # method called from Haml Rails views to initialize the Backbone application
  # on the client side.
  #-----------------------------------------------------------------------------
  init: (container, data = {}) =>
    h = HandyworksApp
    console.log('handyworks app init',container, h.router)
    #h.el = jQuery(container)
    # the following router
    if not h.router
      h.router =  new HandyworksApp.Router()
      # on some screens, history may have already been started which throws an error
      try
        Backbone.history.start()
      catch err
        console.error("backbone router history related error: ", err.message, err)
        # the following line of code, which has the "benefit" of trying to work through
        # the silly Backbone.history() interface limitations ( and it might actually work
        # sometimes ) has a very negative side-effect. If this subsequent loadUrl() fails
        # with an uncaught exception, it leave jQuery in a state where it won't trigger
        # a ready() function for ANY subsequent page loads. (i.e. the whole site stops
        # working unless the user hits refresh.). On the flip side, its a pretty useful
        # way to get a better idea of the actual error that's occuring as opposed to the
        # truncated stack trace that gets printed out above. Use with caution.
        #Backbone.history.loadUrl()
    else
      Backbone.history.loadUrl()

  #-----------------------------------------------------------------------------
  # Setup the trigger for color pickers and register a callback to ensure they
  # get closed up when the popups are closed.
  #-----------------------------------------------------------------------------
  setupColorPickerHandler: ->
    jQuery('body').on "click.handyworks", 'span.color_picker', (event) ->
      console.info('you are in the color picker callback')
      # need zIndex to get in front of fancybox
      jQuery("#color_picker").dialog({ zIndex: HandyworksApp.Configuration.fbZindex })
    #---------------------------------------------------------------------------
    # The farbtastic plugin is a bit rudimentary in its plugin maturity. it
    # does not appear to have been designed to be used multiple times on the
    # the same selectors. So to help things along we're going to forcibly
    # remove the element to which the plugin is bound. We'll also close out
    # the dialog to ensure it doesn't stay open after the popup is closed.
    #
    # After upgrades to jQuery 1.8.2 / jQuery-UI 1.9.1 it was necessary to add
    # a check to make sure the color picker was actually open before trying to
    # to close it. The error was "cannot call methods on dialog before its
    # been initialized"
    #---------------------------------------------------------------------------
    HandyworksApp.dispatcher.on "popupclosing", ->
      color_picker = jQuery("#color_picker")
      color_picker.dialog('destroy').remove() if color_picker.is(':data(dialog)')

  #-----------------------------------------------------------------------------
  # This method is delegating an event handler which replaces the conventional
  # Rails data-confirm, data-method triggers. Its specifically setup to work
  # on the index views through the DataTables plugin.
  #
  # We're also going to setup a global DELETE button handler for the FB popups.
  #-----------------------------------------------------------------------------
  resourceDeleteHandler: () ->
    console.log("setting up delete process")
    jQuery('div#container').on "click.handyworks", 'a[data-method="delete"]', (event) ->
      # don't fire if link as a no-remote class
      return true if jQuery(event.target).hasClass('no-remote')
      console.log("you've clicked a delete row",event)

      event.preventDefault()
      event.stopPropagation()
      if confirm("Are you sure?")
        # add disabled class to nearest row
        # trigger the delete request
        #   on success remove the row
        #   on failure remove the disabled class, and show error
        link = jQuery(event.target).attr('href')
        row = jQuery(event.target).closest('tr')
        HandyworksApp._deleteIt(link,row)
    jQuery('body').on "click.handyworks", '.form-buttons a[data-method="delete"]', (event) ->
      #console.log("you've clicked a delete button",event)
      event.preventDefault()
      event.stopPropagation()
      if confirm("Are you sure you want to delete this item?")
        # mark the popup as cancelled and close it
        # find the row in the index view if its visible and disable it
        # submit the delete request.
        #   on success remove the row
        #   on failure remove the disabled class, and show error
        link = jQuery(event.target).attr('href')
        row_id = jQuery(event.target).data('target')
        row = jQuery('#' + row_id)
        jQuery.fancybox.close()
        HandyworksApp._deleteIt(link,row)

  #-----------------------------------------------------------------------------
  # setup the click events for the auth_control buttons to load content via
  # Ajax
  #   - a screenchange event is triggered either when a new tab is clicked or
  #     when a 2nd level control button is clicked.
  #-----------------------------------------------------------------------------
  setupControlButtons: ->
    # this local function encapsulates the logic to locate the appropriate .content-container
    # and link information, its going to be used both by the AJAX loads and by direct
    # URL requests for content (e.g. /admin/import_groups).
    _locate = ($anchor) ->
      result = {}
      # since we're preventing the default, we need to manually close the dropdown
      # also add the 'active' designation, this might be a different element (if the
      # link itself is the toplevel element on the navbar), so these are separated.
      $anchor.closest('li.dropdown').removeClass('open')
      $anchor.closest('.nav-button').addClass('active')
      result.link = $anchor.closest("li.auth_control_2_button")
      result.container_id = $anchor.closest("li.dropdown").data('container')
      result.container_id = jQuery(result.link).data('container') unless result.container_id
      result.container = jQuery("#" + result.container_id) if result.container_id
      result.container = jQuery('.content-container').first() unless result.container
      result

    jQuery(".auth_control_2_button").not(".popup").not(".manual").click (event) ->
      # prevent the default behaviour for this event b/c we want to load the new
      # content manually.
      event.preventDefault()
      # remove the previously active dropdown designation, note active means it is indicative of
      # what is currently being viewed on the screen.
      jQuery('#primary_controls .nav-button').removeClass('active')

      obj = _locate(jQuery(event.target))

      #console.log("in click event callback",event.target, obj.container_id, obj.container)

      _showHide = () ->
        # clear out any existing content external to the tabs interface
        jQuery("#content").empty()
        HandyworksApp.dispatcher.trigger("screenchange")
        obj.container.addClass('active')
        # we want
        jQuery(".content-container").not(obj.container).removeClass('active').slideUp 100
        # in the test environment, we've disabled effects, so we need
        # to explicitly display the new content in this case.
        unless HandyworksApp.Configuration.rails_env in ['test']
          obj.container.effect 'slide', {}, 300, () ->
            obj.container.addClass('active')
            HandyworksApp.dispatcher.trigger "showhide"
        else
          obj.container.addClass('active').show()
          HandyworksApp.dispatcher.trigger "showhide"

      # what we're doing here is either showing the previously loaded data ('loaded')
      # OR if the content was already being displayed ('active') then we'll refresh it.
      if obj.link.hasClass('loaded') and not obj.container.hasClass('active')
        _showHide()
      else
        # make the call to load the content, everything below this will only happen
        # if the initial Ajax call is successful.
        obj.container.load jQuery(event.target).attr('href'), (responseText, textStatus, xhr) ->
          jQuery(event.target).closest('.dropdown-menu').find('li.auth_control_2_button').removeClass('loaded')
          obj.link.addClass('loaded')
          if obj.container.hasClass('active') or jQuery('.content-container.active').length == 0
            obj.container.addClass('active')
            HandyworksApp.dispatcher.trigger "showhide"
          else
            _showHide()

    # load any directly requested content
    #   first check to see if the content was included in the body of the initial response.
    #   if not then check for a matched pattern to indicate the content should be reloaded.
    #
    #   what we're going to do is check to see if we can find the current URL location in
    #   the menus. if so we'll just act like the link for this URL was clicked. for example
    #   if the browser is pointed directly at /setup/providers vs. clicking the link.
    #
    #   in the event we don't find the associated menu link and dropdown, then we'll just
    #   load the content into the first .content-container
    #
    loc = window.location.pathname
    #console.log("going to check location",loc)
    loadPatterns = [ /\/interaction\//, /\/setup\//, /\/reporting\//, /\/billing\//, /\/schedule\//, /\/admin\//, /\/users\//, /\/content\// ]
    for idx in [1..8]
      if loadPatterns[idx - 1].test loc
        #console.log("something has matched",loadPatterns[idx-1])
        target = jQuery('a[href="' + loc + '"]')
        obj = _locate(target)
        direct_content = jQuery('#direct_content').children()
        unless direct_content.length > 0
          obj.container.load loc, (responseText, textStatus, xhr) ->
            #console.log("direct content loaded")
            target.closest('.dropdown-menu').find('li.auth_control_2_button').removeClass('loaded') if target
            obj.link.addClass('loaded') if obj.link
            obj.container.addClass('active')
        else
          jQuery('#direct_content').addClass('active')

  #-----------------------------------------------------------------------------
  # disable forms for users with only show views.
  #   - we exclude the Cancel buttons because they're sort of needed in popup
  #     views to allow the window to be closed.
  #-----------------------------------------------------------------------------
  showViewFormDisable: ->
    jQuery(".formtastic :input").not("input.cancel").attr('disabled',true).addClass('disabled')

  #-----------------------------------------------------------------------------
  # Looks through the Data to determine which rooms belong to the given clinic
  #-----------------------------------------------------------------------------
  getRoomsForClinic: (clinic_id) ->
    _.filter HandyworksApp.Data.Rooms, (room) ->
      _.include(room.for_clinics,clinic_id)

  #-----------------------------------------------------------------------------
  # Access method for data elements stored under the App namespace to be
  # referenced by name.
  #-----------------------------------------------------------------------------
  getData: (d_name) ->
    r = eval "HandyworksApp.Data.#{d_name}"

  #-----------------------------------------------------------------------------
  # Perform an AJAX request to delete a resource
  #  - if the row defined is part of a table that references a collection,
  #    and the collection contains a model for this resource, then it will
  #    be deleted through the backbone model's destroy() method.
  #  - otherwise the link which triggered this method call will be used for
  #    an AJAX POST request to remove the resource.
  #-----------------------------------------------------------------------------
  _deleteIt: (link, row) ->
    do_ajax = true
    _error = ->
      row.removeClass('disabled') if row.length > 0
    _success = ->
      table = row.closest('.indexTable')
      row.closest('.indexTable').dataTable().fnDeleteRow(row[0]) if row.length > 0
      HandyworksApp.showSuccess("Deleted!",".flash")
      row_clinic = table.children('tbody').children('tr')
      if row_clinic.length == 1
        $.each(row_clinic,(index, value) ->
          a = $(value).find('td > a.clinic-delete');
          a.remove()
        )

    if row.length > 0
      row.addClass('disabled')
      collection_name = row.closest(".indexTable").data("collection")
      if collection_name
        collection = HandyworksApp.getData(collection_name)
        if collection
          resource = collection.get(row.data('id'))
          if resource
            console.info("==>> using collection destroy method")
            do_ajax = false
            resource.destroy
              error: (model, xhr, options) ->
                _error()
              success: (model, response, options) ->
                _success()

    if do_ajax
      jQuery.ajax
        url: link
        type: 'POST'
        data:
          _method: 'delete'
        error: (jqXHR, textStatus, errorThrown) ->
          _error()
        success: (data, textStatus, jqXHR) ->
          _success()

  #-----------------------------------------------------------------------------
  # determines the height for the Calendar popup window.
  #-----------------------------------------------------------------------------
  getCalendarPopupHeight: (dim = 0.6) ->
    ht = jQuery(window).height()
    console.info('window height',ht)
    ht * dim

  #-----------------------------------------------------------------------------
  # calculate the height of the appointment calendar display view
  # we'll use a minimum height of 400px if the viewport won't hold that, then
  # there'll be scrollbars added to keep the calendar functional.
  #-----------------------------------------------------------------------------
  getAppointmentCalendarHeight: ->
    ht = jQuery(window).height() - 100
    return Math.max(ht,400)

  #-----------------------------------------------------------------------------
  # User changes their current_clinic context.
  #  The previous setting is stored in HandyworksApp.Data.current_clinic_id
  #-----------------------------------------------------------------------------
  changeCurrentClinic: (new_clinic_id) ->
    unless new_clinic_id == HandyworksApp.Data.current_clinic_id
      jQuery.ajax
        url: "#{root_path}session_settings"
        type: 'PUT'
        data:
          clinic_id: new_clinic_id
        success: (data, textStatus, jqXHR) ->
          #console.log('changeCurrentClinic success')
          # update the current clinic data reference.
          HandyworksApp.Data.current_clinic_id = new_clinic_id
          # OPTIMIZE: currently this reload will pick-up the theme change if any, but could be made more efficient if the response
          #           either (a) indicated if a theme change was necessary, or (b) indicated the new theme, all themes were loaded
          #           initially and then just switch the body theme namespace. (c) change the form to a simple HTML form submission
          #           and include the current browser location as a hidden field.
          #           2012-07-18 : no longer completely true with the bootstrap header nav bar, it would also need to re-render
          #                        the selection list for clinics since its not actually a selection list.
          location.reload()
        error: (jqXHR, textStatus, errorThrown) ->
          console.error('changeCurrentClinic failure',textStatus)
          HandyworksApp.showError("Error switching clinics",".flash")

  #-----------------------------------------------------------------------------
  # Applies CSS styles based on the provider appointment colors
  #-----------------------------------------------------------------------------
  applyProviderStyles: ->
    if HandyworksApp.Data.Providers?
      _.each HandyworksApp.Data.Providers, (provider) ->
        jQuery(".provider_" + provider.id).css("background-color",provider.appointment_color) if provider?

  #-----------------------------------------------------------------------------
  # used to enable disable the credit card number field on the payment
  # methods form
  #-----------------------------------------------------------------------------
  pre_credit_card_edit: (click_me) ->
    number_field = jQuery(click_me).siblings('input')
    if ( number_field.attr('disabled') )
      number_field.removeAttr('disabled').removeClass('disabled')
    else
      number_field.attr('disabled',true).addClass('disabled')
  #-----------------------------------------------------------------------------
  # convert a calendar event to the rails event model
  #-----------------------------------------------------------------------------
  toEvent: (calEvent, defaults) ->
    #console.log('in HandyworksApp toEvent',calEvent)
    attributes =
      start_time: calEvent.start.toString('HH:mm:ss')
      start_date: calEvent.start.toString('yyyy-MM-dd') # format to send to rails server
      duration: HandyworksApp.getDuration(calEvent.start,calEvent.end)
    if calEvent.title?
      attributes.title = calEvent.title
    if calEvent.eventable_id?
      attributes.eventable_id = calEvent.eventable_id
    if calEvent.eventable_type?
      attributes.eventable_type = calEvent.eventable_type
    if calEvent.id > 0
      attributes.id = calEvent.id
    if calEvent.notes?
      attributes.notes = calEvent.notes
    if calEvent.free?
      attributes.free = calEvent.free
    if calEvent.clinics?
      attributes.clinics = calEvent.clinics
    # apply defaults if not present in attributes.
    jQuery.extend({},defaults,attributes)

  #-----------------------------------------------------------------------------
  # This method is used by both the scheduling (Provider) UI and the Appointment
  # Calendar UI.
  #
  # Keep track of whether the new appointment is created by clicking or by
  # dragging. In the former case we want to use the default duration based on
  # appointment type, vs. the later
  #
  # We're also going to reject the first create on click event. This is to
  # create events on double clicks instead of single clicks. The mechanism
  # here is to store a data-firstClick attribute on the first click, and
  # set a callback to remove after a standard dbl click time of 500ms.
  #-----------------------------------------------------------------------------
  beforeEventNew: (event, data) ->
    #console.log("in HandyworksApp beforeEventNew",event,data)
    if data.createdFromSingleClick
      # check for single / dbl click on this element
      $target = jQuery(event.target)
      if HandyworksApp.doubleClick($target)
        # this was the dbl click
        data.calEvent.drag = false
      else
        return false # need to wait for 2nd click
    else
      data.calEvent.drag = true
    true # to proceed with event creation

  providerFormValidation: ->
    # basic client side validation for new provider
    HandyworksApp.validate "form#new_provider",
      rules:
        'provider[tax_uid]':
          EINSS: true
      messages:
        'provider[tax_uid]': "tax_uid : should be in the following formats: ###-##-#### or ##-#######",


