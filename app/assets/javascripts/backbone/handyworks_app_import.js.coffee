#===============================================================================
# A collection of functions used by the Handyworks CSV Import process.
#===============================================================================

_.extend window.HandyworksApp,
  #-----------------------------------------------------------------------------
  # used to generate the appropriate file input fields and instructions based
  # the users selection of import group
  #-----------------------------------------------------------------------------
  setupImportFiles: (event) ->
    console.info('in HandyworksApp setupImportFiles',event)
    importGroupName = jQuery("#import_group_import_group_type").val()
    importGroup = _.find HandyworksApp.Data.ImportConfiguration.groups,(group) ->
      group.name == importGroupName
    console.info('--> switch to group',importGroup)
    jQuery("#import_group_help").html(importGroup.instruction)
    # remove an existing file imports
    jQuery("#add_fields_here").parent().find("li").remove()
    rules = {}
    _.each importGroup.files, (file) ->
      add_fields "#add_fields_here","imports",HandyworksApp.Data.import_form_content, (new_id) ->
        console.info('-- --> in setupImportFiles CB',new_id)
        jQuery.extend(rules, HandyworksApp._setupImportFile(file, new_id))
    validator_settings =
      rules: rules
      onsubmit: false
    HandyworksApp.Data.importValidator =
      HandyworksApp.newValidator(HandyworksApp.Data.importValidator, "#new_import_group",validator_settings)
    jQuery("#new_import_group").ajaxForm
      iframe: true # without this, it was blowing up in Chrome with maximum stack exceeded
      data:
        ajaxForm: true # tells the backend code this is XHR request sinces its coming via iframe
      dataType: 'script'
      beforeSerialize: (form, options) ->
        console.log("--> ajaxForm beforeSerialize validating form inputs")
        unless HandyworksApp.Data.importValidator.form()
          console.log("--> ajaxForm validation failed")
          # BLS : the enableFormElements method must be called here to counteract the 
          # effect of the rails-ujs disableFormElements method which is triggered just
          # slightly after this one (at least in Firefox & Chrome )
          _.delay jQuery.rails.enableFormElements, 50, jQuery(form)
          return false
        console.log("-- --> proceeding with form submission")
        return true
      beforeSubmit: (arr, form, options) ->
        console.log("--> ajaxForm beforeSubmit")
        # on AWS servers, the busy indicator is really annoying with the repeated
        # status update requests.
        HandyworksApp.Configuration.dontDisplayBusy = true
        HandyworksApp.Data.import_group.status = "Uploading"
        # don't trigger repeated status updates here, wait until the uploads complete
        HandyworksApp.importGroupStatus(HandyworksApp.Data.import_group, false)
      success: (data, textStatus, jqXHR, jqForm) ->
        console.info("--> ajaxForm success",textStatus,data)
        HandyworksApp.importGroupFormSuccess(data, textStatus, jqXHR, jqForm)
      error: (jqXHR, textStatus, errorThrown, jqForm) ->
        # FIXME: this method is not getting called even when the response is not 200 (ok)
        console.warn("--> ajaxForm error",jqForm)
        jQuery.rails.enableFormElements(jQuery(form))

  #-----------------------------------------------------------------------------
  # create the form fields for a single file upload for this import group.
  # this method is designed as a callback from the add_fields procedure
  # @return : the validation rules for this file.
  #-----------------------------------------------------------------------------
  _setupImportFile: (file, new_id) ->
    rules = {}
    file.id = new_id
    # add placeholder to display status information.
    jQuery("#import_group_imports_attributes_" + new_id + "_data_input").append("<div class='inline-status'></div>")
    jQuery("label[for=import_group_imports_attributes_" + new_id + "_data]").html(if file.label then file.label else file.type)
    jQuery("#import_group_imports_attributes_" + new_id + "_data_input .help-block").html(file.instruction)
    jQuery("#import_group_imports_attributes_" + new_id + "_import_type").val(file.type)
    # store the expected filename in the data attribute of the input element to be 
    # used for client side confirmation of input file selections.
    file_name = HandyworksApp.endingAfter(file.instruction,"\\")
    # setup a rule to require a specific file to be loaded
    rules['import_group[imports_attributes][' + new_id + '][data]'] =
      specificFile: [ file_name, HandyworksApp.Configuration.expert ]
    # handle displaying required fields
    if file.required
      jQuery("label[for=import_group_imports_attributes_" + new_id + "_data]").append(HandyworksApp.Configuration.requiredLabel)
      rules['import_group[imports_attributes][' + new_id + '][data]'].required = true
    return rules

  #-----------------------------------------------------------------------------
  # update the status information for this import group 
  #-----------------------------------------------------------------------------
  importGroupStatus: (import_group, repeat = true) ->
    console.info("in Handyworks importGroupStatus",import_group)

    if not import_group? or _.isEmpty(import_group)
      # request a new import group statusfrom the server
      console.log("--> requesting new import group status")
      jQuery.ajax
        url: "#{root_path}admin/import_groups/import_status"
        success: (data, textStatus, jqXHR) ->
          console.log("in importGroup request status success",HandyworksApp.Data.import_group)
          HandyworksApp._importGroupStatus(HandyworksApp.Data.import_group, repeat)
    else
      return HandyworksApp._importGroupStatus(import_group, repeat)

  _importGroupStatus: (import_group, repeat = true) ->
    console.log("--> in HandyworksApp _importGroupStatus")
    done = false
    if import_group? and not _.isEmpty(import_group)
      # update the individual file status areas, first clear them all
      jQuery(".file div.inline-status").empty()
      HandyworksApp._updateFileStatus file_info, index for file_info,index in import_group.files
      # re-schedule importGroupStatus if status is not like Loaded
      loaded = import_group.status.match(/^Loaded/)
      if loaded? and loaded.length > 0
        done = true
      # update the main status area
      if import_group.status
        unless done
          img = "<img src='#{root_path}assets/processing.gif' alt='...'/>"
          jQuery("#import_status_area").html("#{import_group.status}#{img}")
        else
          jQuery("#import_status_area").html("Done")
      else
        jQuery("#import_status_area").empty()

    if done
      # we're done take them back to the import index (by reloading the current page)
      go_back = ->
        window.location.reload()
      _.delay go_back, 1500
    # maximum repeat of 100 iterations, then move them back to the index page
    else if repeat and HandyworksApp.Data.ajaxRequestCount < 100
      console.log("-- --> scheduling next import group status update")
      _.delay HandyworksApp.importGroupStatus, 5000

  _updateFileStatus: (file_info,index) ->
    # FIXME : surely there is a better way to get the nth element in jQuery set than slice
    if file_info.status == "Loaded"
      jQuery(".file div.inline-status").slice(index,index + 1).html("<div class='icon24 ok'> </div>")
    else
      jQuery(".file div.inline-status").slice(index,index + 1).html(file_info.status)

  #-----------------------------------------------------------------------------
  # Handle Import Group form submission success return
  # expects the HandyworksApp.Data.import_group to have been 
  # populated from the script response.
  # NOTE : this method gets called regardless of whether the form submission
  #        response return status OK (200).
  #-----------------------------------------------------------------------------
  importGroupFormSuccess: (data, textStatus, jqXHR, jqForm) ->
    console.info("in Handyworks importGroupFormSuccess",textStatus,jqXHR)
    # FIXME : this is ajaxFormSuccess hack because the error method is not getting called properly
    #         so we need to explicitly look for error responses in the data
    #         add the information to the existing error array.
    unless data.indexOf("HandyworksApp.Data.import_group.status") == 0
      console.log("-- --> adding server error to error messages")
      HandyworksApp.Data.import_group.errors.push(data)
    errors = HandyworksApp.Data.import_group.errors
    unless errors? and _.isEmpty(errors)
      # we need to reset the submission button
      jQuery.rails.enableFormElements(jqForm)
      # display errors 
      if errors?
        console.log("-- --> import errors occured",errors)
        HandyworksApp.showError(errors.join("<br/>"),".flash")
      else
        # this is some kind of generic server error (e.g. Internal Server Error)
        HandyworksApp.showError(textStatus,".flash")
      # clear the processing status and errors
      HandyworksApp.Data.import_group.status = ""
      HandyworksApp.Data.import_group.errors = []
      HandyworksApp.importGroupStatus(HandyworksApp.Data.import_group, false)
    else
      # update status displays
      HandyworksApp.importGroupStatus(HandyworksApp.Data.import_group)

