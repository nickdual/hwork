﻿/**
 * @license Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.plugins.setLang( 'hwrepl', 'en', {
	title: 'Hw Replacement Properties',
	toolbar: 'Create HW Replacement',
	text: 'HW Replacement Text',
	edit: 'Edit HW Replacement',
	textMissing: 'The HW Replacement must contain text.'
});
