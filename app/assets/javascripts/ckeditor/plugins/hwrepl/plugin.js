﻿/**
 * @license Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

/**
 * @fileOverview The "placeholder" plugin.
 *
 */

(function() {
	var hwreplReplaceRegex = /\${[^}]+}/g;
	CKEDITOR.plugins.add( 'hwrepl', {
		requires: 'dialog',
		lang: 'en', // %REMOVE_LINE_CORE%
		icons: 'hwrepl', // %REMOVE_LINE_CORE%
		onLoad: function() {
			CKEDITOR.addCss( '.cke_hwrepl' +
				'{' +
					'background-color: #ffff00;' +
					( CKEDITOR.env.gecko ? 'cursor: default;' : '' ) +
				'}'
				);
		},
		init: function( editor ) {
			var lang = editor.lang.hwrepl;

			editor.addCommand( 'createhwrepl', new CKEDITOR.dialogCommand( 'createhwrepl' ) );
			editor.addCommand( 'edithwrepl', new CKEDITOR.dialogCommand( 'edithwrepl' ) );

			editor.ui.addButton && editor.ui.addButton( 'CreateHwRepl', {
				label: lang.toolbar,
				command: 'createhwrepl',
				toolbar: 'insert,5',
				icon: 'hwrepl'
			});

            /* FIXME : This code provides a right click "Edit HW Replacement" option
             *    which does not pre-select the current value. Also when you actually
             *    choose something it doesn't get inserted where you intended, instead
             *    the result gets inserted at the beginning of the editor window. This
             *    plugin is based on the placeholder plugin, and the same bug exists in
             *    that plugin with the Edit context menu.
             */
            //  if ( editor.addMenuItems ) {
            //      editor.addMenuGroup( 'hwrepl', 20 );
            //      editor.addMenuItems({
            //          edithwrepl: {
            //              label: lang.edit,
            //              command: 'edithwrepl',
            //              group: 'hwrepl',
            //              order: 1,
            //              icon: 'hwrepl'
            //          }
            //      });

            //      if ( editor.contextMenu ) {
            //          editor.contextMenu.addListener( function( element, selection ) {
            //              if ( !element || !element.data( 'cke-hwrepl' ) )
            //                  return null;
            //              return { edithwrepl: CKEDITOR.TRISTATE_OFF };
            //          });
            //      }
            //  }

			editor.on( 'doubleclick', function( evt ) {
				if ( CKEDITOR.plugins.hwrepl.getSelectedHwRepl( editor ) ) {
					evt.data.dialog = 'edithwrepl';
                }
			});

			editor.on( 'contentDom', function() {
				editor.editable().on( 'resizestart', function( evt ) {
					if ( editor.getSelection().getSelectedElement().data( 'cke-hwrepl' ) )
						evt.data.preventDefault();
				});
			});

			CKEDITOR.dialog.add( 'createhwrepl', this.path + 'dialogs/hwrepl.js' );
			CKEDITOR.dialog.add( 'edithwrepl', this.path + 'dialogs/hwrepl.js' );
		},
		afterInit: function( editor ) {
			var dataProcessor = editor.dataProcessor,
				dataFilter = dataProcessor && dataProcessor.dataFilter,
				htmlFilter = dataProcessor && dataProcessor.htmlFilter;

			if ( dataFilter ) {
				dataFilter.addRules({
					text: function( text ) {
						return text.replace( hwreplReplaceRegex, function( match ) {
							return CKEDITOR.plugins.hwrepl.createHwRepl( editor, null, match, 1 );
						});
					}
				});
			}

			if ( htmlFilter ) {
				htmlFilter.addRules({
					elements: {
						'span': function( element ) {
							if ( element.attributes && element.attributes[ 'data-cke-hwrepl' ] )
								delete element.name;
						}
					}
				});
			}
		}
	});
})();

CKEDITOR.plugins.hwrepl = {
	createHwRepl: function( editor, oldElement, text, isGet ) {
		var element = new CKEDITOR.dom.element( 'span', editor.document );
		element.setAttributes({
			contentEditable: 'false',
			'data-cke-hwrepl': 1,
			'class': 'cke_hwrepl'
		});

		text && element.setText( text );

		if ( isGet )
			return element.getOuterHtml();

		if ( oldElement ) {
			if ( CKEDITOR.env.ie ) {
				element.insertAfter( oldElement );
				// Some time is required for IE before the element is removed.
				setTimeout( function() {
					oldElement.remove();
					element.focus();
				}, 10 );
			} else
				element.replace( oldElement );
		} else
			editor.insertElement( element );

		return null;
	},

	getSelectedHwRepl: function( editor ) {
		var range = editor.getSelection().getRanges()[ 0 ];
		range.shrink( CKEDITOR.SHRINK_TEXT );
		var node = range.startContainer;
		while ( node && !( node.type == CKEDITOR.NODE_ELEMENT && node.data( 'cke-hwrepl' ) ) )
			node = node.getParent();
		return node;
	}
};
