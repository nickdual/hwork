﻿/**
 * @license Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

(function() {
    function hwreplDialog( editor, isEdit ) {

        var lang = editor.lang.hwrepl,
            generalLabel = editor.lang.common.generalTab;
        return {
            title: lang.title,
            minWidth: 300,
            minHeight: 80,
            contents: [
                {
                id: 'info',
                label: generalLabel,
                title: generalLabel,
                elements: [
                    {
                    id: 'repl',
                    type: 'select',
                    style: 'width: 100%;',
                    label: 'Select the field value',
                    items: HandyworksApp.Data.HwReplacements,
                    'default': 'AppointmentDate',
                    required: true,
                    validate: CKEDITOR.dialog.validate.notEmpty( lang.textMissing ),
                    setup: function( element ) {
                        if ( isEdit )
                            this.setValue( element.getText().slice( 2, -1 ) );
                    },
                    commit: function( element ) {
                        var text = '${' + this.getValue() + '}';
                        // The placeholder must be recreated.
                        CKEDITOR.plugins.hwrepl.createHwRepl( editor, element, text );
                    }
                }
                ]
            }
            ],
            onShow: function() {
                if ( isEdit )
                    this._element = CKEDITOR.plugins.hwrepl.getSelectedHwRepl( editor );

                this.setupContent( this._element );
            },
            onOk: function() {
                this.commitContent( this._element );
                delete this._element;
            }
        };
    }

    CKEDITOR.dialog.add( 'createhwrepl', function( editor ) {
        return hwreplDialog( editor );
    });
    CKEDITOR.dialog.add( 'edithwrepl', function( editor ) {
        return hwreplDialog( editor, 1 );
    });
})();
