#-------------------------------------------------------------------------------
#  CkEditor configuration file in the location specified to work with
#  the ckeditor gem.
#    https://github.com/galetahub/ckeditor/wiki
# 
#  Define changes to default configuration here.
#  For the complete reference:
#  http://docs.ckeditor.com/#!/api/CKEDITOR.config
#-------------------------------------------------------------------------------
CKEDITOR.editorConfig = (config) ->
  console.info('he hello editor config',config.extraPlugins,'abc')
  config.language = "en"
  # Remove some buttons, provided by the standard plugins, which we don't
  # need to have in the Standard(s) toolbar.
  config.removeButtons = 'Underline,Subscript,Superscript'
  # Extra Plugins
  config.extraPlugins = 'hwrepl'
  # FIXME : the hwrepl plugin is not working as intended and cause blank editor areas.

