#-------------------------------------------------------------------------------
# Application Global Area
#-------------------------------------------------------------------------------
#  Support for Mixins in Coffee script
#    http://arcturo.github.com/library/coffeescript/03_classes.html
# Usage
#   include Parrot,
#     isDeceased: true
#   (new Parrot).isDeceased
#-------------------------------------------------------------------------------
#
#
# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
# or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# the compiled file.
#
# WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
# GO AFTER THE REQUIRES BELOW.
#
#= require jquery
#= require jquery_ujs
#= require bootstrap
#= require jquery.externalscript
#= require jquery.readyselector
#= require underscore
#= require fancybox
#= require plugins/jquery.maskedinput
#= require base
#= require_tree ./measure/


jQuery('body.confirmations.show').ready ->
  HandyworksApp.phoneMask('input.phone-input')

jQuery(document).ready ->
  $("form#new_contact_us").bind("ajax:beforeSend", ->
    $("form#new_contact_us button").text('Sending...')
    $("form#new_contact_us button").attr("disabled", true)
    )
    .bind("ajax:complete", ->
      $("form#new_contact_us button").text('Send')
      $("form#new_contact_us button").removeAttr("disabled") )
