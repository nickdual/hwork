#-------------------------------------------------------------------------------
# Public Appointments Global Area
#-------------------------------------------------------------------------------
# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
#
#= require date
#= require jquery
#= require jquery_ujs
#= require bootstrap
#= require jquery.externalscript
#= require jquery.readyselector
#= require backbone_stack
#= require fancybox
#= require base
#= require backbone/handyworks_app
#= require_self
#= require_tree ../templates/public_appointments

_.extend window.HandyworksApp,

  initialize_public_appointment: ->
    HandyworksApp.router = new HandyworksApp.Routers.PublicAppointmentsRouter()
    Backbone.history.start()

