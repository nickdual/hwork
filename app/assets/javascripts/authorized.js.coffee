#-------------------------------------------------------------------------------
# Application Global Area
#-------------------------------------------------------------------------------
#  Support for Mixins in Coffee script
#    http://arcturo.github.com/library/coffeescript/03_classes.html
# Usage
#   include Parrot,
#     isDeceased: true
#   (new Parrot).isDeceased
#-------------------------------------------------------------------------------
#
#
# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
# or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# the compiled file.
#
# WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
# GO AFTER THE REQUIRES BELOW.
#
#= require date
#= require jquery
#= require jquery_ujs
#= require bootstrap
#= require jquery.ui.all
#= require jquery.externalscript
#= require jquery.readyselector
#= require backbone_stack
#= require fancybox
#= require farbtastic
#= require jstree
#= require jquery/layout
#= require plugins/jquery.cookie
#= require plugins/jquery.maskedinput
#= require plugins/jquery.weekcalendar
#= require plugins/jquery.mousewheel
#= require plugins/jquery.easing.1.3.js
#= require plugins/jquery.rule
#= require plugins/jquery.form
#= require plugins/jquery.scrollTo
#= require plugins/jquery.smartresize
#= require plugins/jquery-picklist
#= require dataTables/jquery.dataTables
#= require dataTables/extras/ColVis
#= require plugins/jquery.dataTables-plugins
#= require ckeditor/init
#= require base
#= require_tree ./authorized/
#= require backbone/handyworks_app
#= require_tree ../templates/
