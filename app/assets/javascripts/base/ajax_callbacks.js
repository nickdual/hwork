//------------------------------------------------------------------------------
//  Document.ready one time js operations go in this context.
//------------------------------------------------------------------------------
$(function() {
   // Ajax activity indicator bound to ajax start/stop document events
   // this applies to all AJAX operations.
   $(document).ajaxSend(function(event, jqXHR, ajaxOptions){ 
     HandyworksApp.ajaxSend(event,jqXHR,ajaxOptions);
   }).ajaxComplete(function(event, jqXHR, ajaxOptions){
     HandyworksApp.ajaxComplete(event,jqXHR,ajaxOptions);
   }).ajaxStop(function(){ 
     HandyworksApp.ajaxStop();
   }).ajaxError(function(event, jqXHR, ajaxSettings, thrownError){
     HandyworksApp.ajaxError(event,jqXHR, ajaxSettings, thrownError);
   });

});
