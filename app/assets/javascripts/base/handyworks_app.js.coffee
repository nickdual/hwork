#-------------------------------------------------------------------------------
# Top Level Namespace and functions for the entire client side Handyworks
# Application
#-------------------------------------------------------------------------------
window.HandyworksApp =
  Models: {}
  Collections: {}
  Routers: {}
  Views: {}
  Data:
    clientAutoCompleteCache: [{},{}]
    popInit: false # keeps track of the initial popups, they should only display once
    bgPopupHandlersBound: false
    bgPopupSubmitted: false
    bgPopupSubmittedStandalone: false
    ajaxRequestCount: 0
    ProviderScheduleLocationRequired: false # dynamic setting by views/schedules/edit.html.haml
    refreshBlock: false # dynamic setting to prevent screen background refresh actions.
    import_group:
      errors: []
      status: ""
      files: []

  Configuration:
    phoneFormat: "(999) 999-9999? x99999"
    defaultScheduleEventTitle: 'Schedule Event'
    notificationDisplayTime: 12000 # 12 seconds
    dontDisplayBusy: false
    rails_env: "production" # set dynamically in layouts/_javascripts
    calendarViewBy: 'room' # default appointment calendar view [ room | provider ]
    sessionResourceSelected: "resource_selected"
    sessionResourceNotSelected: "resource_unselected"
    requiredLabel: '<abbr title="required">*</abbr>'
    expert: false
    iconSize: 1
    iconSizeMap: ["icon16", "icon24", "icon32"]
    # changing the date format here can affect the systems ability to save events
    # properly as rails receives them as strings and will nil out any date it
    # doesn't recognize. the format strings between the date.js and jQuery-UI are
    # not compatible, both of the formats below indicate numeric 2-digit months and
    # 4-digit years.
    dateFormat: "mm/dd/yy" # date format compatible with jQuery-UI datepicker
    dateFormatStr: "MM/dd/yyyy" # date format compatible with date.js Date.toString()
    timeFormatStr: "h:mm tt" # time format compatible with date.js Date.toString()
    dateTimeFormatStr: ->
      HandyworksApp.Configuration.dateFormatStr + " " + HandyworksApp.Configuration.timeFormatStr
    firstDay: 0 # first day of the week for calendar interfaces
    appointmentByHoursRestriction: false # allows admin to click in grey time slots
    refreshRate: 30000 # interval for refreshing persistent screens (e.g. appointments)
    doubleClickRate: 500 # interval to monitor for double click events
    # keep track of z-index required to get in front of fancybox
    fbZindex: 8050

  #-----------------------------------------------------------------------------
  # Generic function called when sending any AJAX request. This is registered
  # from the application.js file. It is responsible for displaying the busy
  # indicator.
  #-----------------------------------------------------------------------------
  ajaxSend: (event, jqXHR, ajaxOptions) ->
    HandyworksApp.Data.ajaxRequestCount = HandyworksApp.Data.ajaxRequestCount + 1
    unless HandyworksApp.Configuration.dontDisplayBusy
      jQuery.fancybox.showLoading()

  #-----------------------------------------------------------------------------
  # ajaxComplete will be called when each AJAX request is completed.
  # it will be responsible for extracting any flash messages from the
  # response headers to be displayed.
  #-----------------------------------------------------------------------------
  ajaxComplete: (event, jqXHR, ajaxOptions) ->
    message = jqXHR.getResponseHeader('X-flash')
    console.log("Handyworks App ajaxComplete",message)
    HandyworksApp.displayFlash(jQuery.parseJSON(message)) if message?
  #-----------------------------------------------------------------------------
  # ajaxStop is called when an AJAX request is completed AND no other AJAX
  # requests are currently pending
  #-----------------------------------------------------------------------------
  ajaxStop: ->
    jQuery.fancybox.hideLoading()

  ajaxError: (event, jqXHR, ajaxSettings, thrownError) ->
    console.info('in Handyworks ajaxError',event,jqXHR,ajaxSettings,thrownError)
    # check for invalid/expired sessions
    if thrownError == "Unauthorized" or thrownError == "Authorization Required"
      # redirect to a login screen, I think "Unauthorized" is Ruby 1.9 and "Auth Required" is Ruby 1.8
      # OPTIMIZE : redirecting to login screen on Unauthorized AJAX request could be more elegant
      #            currently, the users receive no message on the screen indicating why they're being
      #            redirected and depending on the timing of this request, they'll start seeing the
      #            UI changes resulting from the AJAX request. And lastly the user is being redirected
      #            with no record of where they are coming from ... to which they can return.
      # window.location = "#{root_path}users/sign_in"
      HandyworksApp.doLogin("Your session has expired. Please sign in.")
      return
    if thrownError
      if jqXHR.status == 200
        console.info('ajax error ignored, please fix:',thrownError)
      if jqXHR.status == 0 and thrownError == "abort"
        # FIXME : I'm not sure why, but clicking CANCEL on a destroy confirmation alert is triggering
        #         an ajaxError with the signature status = 0 and error = "abort"
        console.log('ajax request aborted')
      else
        #
        # the logic below here is looking for JSON response containing an element errors
        # which should be a hash of field names (keys) and error messages (values)
        #
        console.info("handyworks app ajax errorx",event, jqXHR, ajaxSettings, thrownError)
        # only display errors if the app specific code hasn't displayed anything
        if jQuery('#showMessage').length == 0 or jQuery('#showMessage').css('display') == 'none'
          no_errors = true
          try
            result = jQuery.parseJSON(jqXHR.responseText)
            console.log("parse json response",result)
            if result.errors?
              _.each result.errors, (error, attribute) ->
                error_msg = "An error occurred: " + "#{attribute} : #{error} "
                HandyworksApp.showError(error_msg,".flash")
                no_errors = false
          catch err
            console.error(" ajax error handler error",err)
          finally
            if no_errors
              HandyworksApp.showError(thrownError,".flash") unless thrownError == "OK"

  #-----------------------------------------------------------------------------
  # Initially the console will be disabled by some standalone JavaScript code
  # in layouts/base.html.haml. When this is done, the original console is
  # backed up on the global variable oconsole. You can call this function to
  # re-enable the original console methods.
  #-----------------------------------------------------------------------------
  turnOnConsole: ->
    if window.oconsole and window.oconsole.log
      _.each window._lmethods, (mname) ->
        eval('console.'+ mname + ' = oconsole.' + mname)

  #-----------------------------------------------------------------------------
  # You can call this function to turn off the console, it iterates through
  # a predefined list of methods and sets them to noop functions.
  # NOTE: this method does not back up the original console methods, this is
  #   only done by the standalone JavaScript in the layouts/base.html.haml
  #   This is because it must be done immediately as the page is loading,
  #   before any coffeescript objects are processed which might call the
  #   console methods. It has to be here for browsers like IE<=8 which do
  #   not provide the console object and thus would choke on code trying
  #   to call methods against it.
  #-----------------------------------------------------------------------------
  turnOffConsole: ->
      _.each window._lmethods, (mname) ->
        eval('console.'+ mname + ' = function() {}')

  #-----------------------------------------------------------------------------
  # this method is designed to either create or update a jQuery.validate form
  # there is an "unatural" limitation with the plugin that if you change the form
  # dynamically and try to create a new validator on the same form, it doesn't
  # recognize the changed fields and will not validate properly. The idea for
  # this method came from
  # http://stackoverflow.com/questions/1510165/how-can-i-add-remove-or-swap-jquery-validation-rules-from-a-page
  #-----------------------------------------------------------------------------
  newValidator: (validator, selector,settings) ->
    console.log("in Handyworks newValidator",validator,selector, settings)
    if validator?
      console.log("--> using existing validator")
      jQuery.extend(validator.settings, settings)
    else
      console.log("--> creating a new validator")
      validator = HandyworksApp.validate(selector, settings)
    return validator # support chaining

  #-----------------------------------------------------------------------------
  # extension of the jquery.validate plugin, this adds a handler when a form
  # is submitted with invalid fields that will display a top level indication
  # of the number of fields containing invalid characters.
  #-----------------------------------------------------------------------------
  validate: (selector, settings) ->
    settings = {} unless settings?
    settings.invalidHandler = (form, validator) ->
      if validator.numberOfInvalids()
        HandyworksApp.showError("Your form contains " + validator.numberOfInvalids() + " errors, see details below.",".flash")
    settings.onkeyup = false
    validator = jQuery(selector).validate(settings)
    return validator

  #-----------------------------------------------------------------------------
  # display Rails flash messages using the client side display routine to
  # keep them consistent with error messages passed over AJAX requests.
  #-----------------------------------------------------------------------------
  displayFlash: (messages) ->
#    console.log('Handyworks App displayFlash',messages)
    _.each messages, (msg) ->
      if ( msg[0] == 'notice' )
        HandyworksApp.showSuccess(msg[1],'.flash')
      if ( msg[0] == 'error' )
        HandyworksApp.showError(msg[1],'.flash')
      if ( msg[0] == 'success' )
        HandyworksApp.showSuccess(msg[1],'.flash')
      if ( msg[0] == 'alert' )
        HandyworksApp.showAlert(msg[1],'.flash')
  #-----------------------------------------------------------------------------
  # display stylized client side error message, similar to the style generated
  # from server side errors
  #
  # Errors will not fadeOut, but will be persisted.
  #-----------------------------------------------------------------------------
  showError: (message, selector, context = document) ->
    HandyworksApp._showMessage('fail',message,selector,context,false)

  #-----------------------------------------------------------------------------
  # display stylized client side info message, similar to the style generated
  # from server side errors
  #-----------------------------------------------------------------------------
  showInfo: (message, selector, context = document, fadeOut = true) ->
    HandyworksApp._showMessage('notification',message,selector,context,fadeOut,5000)

  #-----------------------------------------------------------------------------
  # display stylized alert messages from the client side.
  #-----------------------------------------------------------------------------
  showAlert: (message, selector, context = document, fadeOut = false) ->
    HandyworksApp._showMessage('alert',message,selector,context,fadeOut)

  #-----------------------------------------------------------------------------
  # display stylized success messages from the client side.
  #-----------------------------------------------------------------------------
  showSuccess: (message, selector, context = document, fadeOut = true) ->
    HandyworksApp._showMessage('success',message,selector,context,fadeOut,5000)

  #-----------------------------------------------------------------------------
  # show messages, type in ('error','info','alert','success')
  #   to support non-JavaScript testing, which should be looking at error/success
  #   fadeOut will be disabled in those environments.
  #-----------------------------------------------------------------------------
  _showMessage: (type, message, selector, context, fadeOut, delayTime = 5000) ->
    console.log("showing message",type,message,selector,context)
    options =
      title: message
      text: ' '
      sticky: !fadeOut
      class_name: type
      time: delayTime
    if _.indexOf(['test', 'cucumber' ], HandyworksApp.Configuration.rails_env) != -1
      options.sticky = false
    jQuery.gritter.add options


  #-----------------------------------------------------------------------------
  # highlight the session menu if there are special notifications in there
  # OPTIMIZE : There might be a way to replace this function with some form of nested CSS rules in Sass, it could definitely be done with LESS.
  #-----------------------------------------------------------------------------
  highlightSessionMenu: ->
    notices = jQuery('.special-notice','header')
    if notices.length > 0
      jQuery('.session-menu').addClass('active')

  #-----------------------------------------------------------------------------
  # give the user a login option
  #-----------------------------------------------------------------------------
  doLogin: (message) ->
    console.info("in HandyworksApp doLogin")
    jQuery("#login").html(JST['shared/_login'](msg: message, url: "#{root_path}users/sign_in"))
    jQuery("#login").modal()

  #-----------------------------------------------------------------------------
  # This function implements a consistent double click event on the requested
  # target element. It will check a defined data value on the element and if its
  # not present will set it with a timeout of doubleClickRate delay it removes
  # the value.
  # @return - true if this is the 2nd click, and false otherwise
  #-----------------------------------------------------------------------------
  doubleClick: (target) ->
    # check for single / dbl click on this element
    firstClick = target.data('firstClick')
    if firstClick?
      # this was the dbl click
      target.removeData('firstClick')
      return true

    # this was the single click
    target.data('firstClick','1')
    setTimeout( () ->
      target.removeData('firstClick')
    ,HandyworksApp.Configuration.doubleClickRate)
    return false # need to wait for 2nd click

  #-----------------------------------------------------------------------------
  # convenience utility to take an arbitrary string and return a truncated
  # version with the full string still accessible through a title attribute
  #-----------------------------------------------------------------------------
  chop: (str, len) ->
    if str?
      if str.length > len
        n_str = "<span title='#{str}'>#{str.substr(0,len)}&hellip;</span>"
      else
        n_str = str
    else
      n_str = ''
    n_str

  #-----------------------------------------------------------------------------
  # trim off the end of a string after the last occurence of pattern
  #  e.g. endingAfter("abcde","b") ==> "cde"
  #-----------------------------------------------------------------------------
  endingAfter: (input_str,ending_pattern) ->
    if input_str? and ending_pattern?
      x = input_str.lastIndexOf(ending_pattern)
      if x >= 0 and x < input_str.length
        return input_str.substr(x + 1)
    return input_str

  #-----------------------------------------------------------------------------
  # clears the current selection within the browser window.
  # I needed this specifically to correct a behaviour when using the dblclick
  # event from the indexTable.
  # This solution was taken from:
  #    http://stackoverflow.com/questions/880512/prevent-text-selection-after-double-click
  #-----------------------------------------------------------------------------
  clearSelection: ->
    if document.selection and document.selection.empty
      document.selection.empty()
    else if window.getSelection
      sel = window.getSelection()
      sel.removeAllRanges()

  #-----------------------------------------------------------------------------
  # this method is designed to either create or update a jQuery.validate form
  # there is an "unatural" limitation with the plugin that if you change the form
  # dynamically and try to create a new validator on the same form, it doesn't
  # recognize the changed fields and will not validate properly. The idea for
  # this method came from
  # http://stackoverflow.com/questions/1510165/how-can-i-add-remove-or-swap-jquery-validation-rules-from-a-page
  #-----------------------------------------------------------------------------
  newValidator: (validator, selector,settings) ->
    console.log("in Handyworks newValidator",validator,selector, settings)
    if validator?
      console.log("--> using existing validator")
      jQuery.extend(validator.settings, settings)
    else
      console.log("--> creating a new validator")
      validator = HandyworksApp.validate(selector, settings)
    return validator # support chaining

  #-----------------------------------------------------------------------------
  # extension of the jquery.validate plugin, this adds a handler when a form
  # is submitted with invalid fields that will display a top level indication
  # of the number of fields containing invalid characters.
  #-----------------------------------------------------------------------------
  validate: (selector, settings) ->
    settings = {} unless settings?
    settings.invalidHandler = (form, validator) ->
      if validator.numberOfInvalids()
        HandyworksApp.showError("Your form contains " + validator.numberOfInvalids() + " errors, see details below.",".flash")
    settings.onkeyup = false
    validator = jQuery(selector).validate(settings)
    return validator

  #-----------------------------------------------------------------------------
  # The following two functions are extracted from the farbtastic plugin for
  # use in determining the best tone for text color in appointments given the
  # user selected appointment color
  #-----------------------------------------------------------------------------
  farbtasticUnpack: (color) ->
    if (color.length == 7)
      return [parseInt('0x' + color.substring(1, 3)) / 255,
        parseInt('0x' + color.substring(3, 5)) / 255,
        parseInt('0x' + color.substring(5, 7)) / 255]
    else if (color.length == 4)
      return [parseInt('0x' + color.substring(1, 2)) / 15,
        parseInt('0x' + color.substring(2, 3)) / 15,
        parseInt('0x' + color.substring(3, 4)) / 15]

  farbtasticRGBToHSL: (rgb) ->
    r = rgb[0]
    g = rgb[1]
    b = rgb[2]
    min = Math.min(r, Math.min(g, b))
    max = Math.max(r, Math.max(g, b))
    delta = max - min
    l = (min + max) / 2
    s = 0
    if (l > 0 && l < 1)
      s = delta / (if l < 0.5 then (2 * l) else (2 - 2 * l))
    h = 0
    if (delta > 0)
      if (max == r && max != g)
        h += (g - b) / delta
      if (max == g && max != b)
        h += (2 + (b - r) / delta)
      if (max == b && max != r)
        h += (4 + (r - g) / delta)
      h /= 6
    return [h, s, l]

  #-----------------------------------------------------------------------------
  # Takes a color string and determines if white or black text would show up
  # better.
  #-----------------------------------------------------------------------------
  textColorToShow: (color) ->
    rgb = HandyworksApp.farbtasticUnpack(color)
    hsl = HandyworksApp.farbtasticRGBToHSL(rgb)
    if hsl[2] > 0.5
      return '#000'
    else
      return '#fff'

  #-----------------------------------------------------------------------------
  # determine the duration between two times in minutes.
  #  always returns an integer value
  #-----------------------------------------------------------------------------
  getDuration: (start, end) ->
    Math.floor((end.getTime() - start.getTime()) / ( 1000 * 60 ))

  #-----------------------------------------------------------------------------
  # isFormStateChanged
  #  To be used in conjunction with recordFormState method to determing if the
  #
  #  form has changed.
  #-----------------------------------------------------------------------------
  isFormStateChanged: (container = document) ->
    console.log("in HandyworksApp isFormStateChanged",container)
    result = false # forms unchanged
    jQuery('form',jQuery(container)).each (idx, form) ->
      current_state = jQuery(form).serialize()
      previous_state = jQuery(form).data("initial_state")
      if current_state != previous_state
        console.log("form state has been changed")
        result = true
    result # returns true if any form is changed

  #-----------------------------------------------------------------------------
  # recordFormState
  #  This method can be used to store the state (values) of a form
  #  The intended purpose of this would be to compare the form's state
  #  at some later event to determine if its changed.
  #  It will apply to any <form> element in the container
  #-----------------------------------------------------------------------------
  recordFormState: (container = document) ->
    console.log("in HandyworksApp recordFormState",container)
    jQuery('form',jQuery(container)).each (idx, form) ->
      current_state = jQuery(form).serialize()
      console.log("recording form data",form,current_state)
      jQuery(form).data("initial_state",current_state)

  #-----------------------------------------------------------------------------
  # prepends the class to the existing class list.
  # OPTIMIZE: There might be a better jQueryish way to prepend a class to an element
  #-----------------------------------------------------------------------------
  addClassInFront: (element,newClass) ->
    newClassName = "#{newClass} #{element.className}"
    jQuery(element).removeClass(element.className).addClass(newClassName)

  #-----------------------------------------------------------------------------
  # does an "in order" replacement of classFrom with classTo in the set of
  # given elements
  #-----------------------------------------------------------------------------
  switchClass: (selector,classFrom,classTo) ->
    console.info('in HandyworksApp switchClass')
    jQuery(selector).each (index,element) ->
      newClassName = element.className.replace(classFrom,classTo)
      console.log('--> looking at',element.className,newClassName)
      jQuery(element).removeClass(element.className).addClass(newClassName)

  #-----------------------------------------------------------------------------
  # Compute JS start time given separate date and time fields
  #  With duration specified this is an "end" time, otherwise its a "start"
  #  time.
  #-----------------------------------------------------------------------------
  calculate_time: (start_date,start_time,duration = 0) ->
    #console.log("in calculate_time",start_date, start_time, duration)
    time = Date.parse(start_time)
    date = Date.parse(start_date)
    if date?
      date.add
        hours: time.getHours()
        minutes: (time.getMinutes() + duration)
        seconds: time.getSeconds()

  #-----------------------------------------------------------------------------
  # Display a Dialog Box with Help Information
  #-----------------------------------------------------------------------------
  help_dialog: (title, content) ->
    jQuery("#help_dialog").empty().append(content).dialog
      title: title
      zIndex: HandyworksApp.Configuration.fbZindex # need to get in front of fancybox
      width: 'auto'

  #-----------------------------------------------------------------------------
  # This method is basically obsolete, but to the extent that it has "hooks"
  # into some existing portions of the dynamic HTML logic, it'd be a good place
  # to put some common dynamic element initializations like tool tips or
  # help
  #-----------------------------------------------------------------------------
  put_on_style: (top=jQuery('body')) ->
    unless HandyworksApp.Configuration.rails_env in ["test"]
      console.log("in put_on_style")
      # handle foling of advanced options
      #init_advanced_option(top)
      #init_local_tips(top)
      # add the window.location.hash to include internal link and query string
      # set_2nd_control_button(window.location.pathname + window.location.hash)
      #under_construction()

  #-----------------------------------------------------------------------------
  # Apply an input mask to the indicated phone field.
  #  NOTE : the maskedinput plugin was not stable when running with Cucumber
  #         for integraiton tests, and so is disabled for that environment.
  #-----------------------------------------------------------------------------
  phoneMask: (selector) ->
    unless HandyworksApp.Configuration.rails_env in ['test']
      jQuery(selector).mask(HandyworksApp.Configuration.phoneFormat)

