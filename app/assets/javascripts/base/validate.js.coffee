#===============================================================================
# Extensions to jQuery.validate plugin
#===============================================================================
#= require plugins/jquery.validate
#= require plugins/jquery.validate.additional-methods

#-------------------------------------------------------------------------------
# validate greater than another field
#-------------------------------------------------------------------------------
jQuery.validator.addMethod("greaterThan", (value,element,params) ->
  target = jQuery(params[0]).unbind(".validate-greaterThan").bind "blur.validate-greaterThan", ->
    jQuery(element).valid()
  return @optional(element) or parseInt(value) > parseInt(target.val())
"Must be greater than {1}")

#-------------------------------------------------------------------------------
# validate less than another field
#-------------------------------------------------------------------------------
jQuery.validator.addMethod("lessThan", (value,element,params) ->
  target = jQuery(params[0]).unbind(".validate-lessThan").bind "blur.validate-lessThan", ->
    jQuery(element).valid()
  return @optional(element) or parseInt(value) < parseInt(target.val())
"Must be less than {1}")

#-------------------------------------------------------------------------------
# validate greater than or equal to another field
#-------------------------------------------------------------------------------
jQuery.validator.addMethod("greaterThanOrEqual", (value,element,params) ->
  target = jQuery(params[0]).unbind(".validate-greaterThanOrEqual").bind "blur.validate-greaterThanOrEqual", ->
    jQuery(element).valid()
  return @optional(element) or parseInt(value) >= parseInt(target.val())
"Must be greater than or equal to {1}")

#-------------------------------------------------------------------------------
# validate less than another field
#-------------------------------------------------------------------------------
jQuery.validator.addMethod("lessThanOrEqual", (value,element,params) ->
  target = jQuery(params[0]).unbind(".validate-lessThanOrEqual").bind "blur.validate-lessThanOrEqual", ->
    jQuery(element).valid()
  return @optional(element) or parseInt(value) <= parseInt(target.val())
"Must be less than or equal to {1}")

#-------------------------------------------------------------------------------
# assumes you are also including the date validation on both fields.
#-------------------------------------------------------------------------------
jQuery.validator.addMethod("dateGreaterThanOrEqual", (value,element,params) ->
  # bind to the blur event of the target in order to revalidate whenever the target field is updated
  # TODO find a way to bind the event just once, avoiding the unbind-rebind overhead
  target = jQuery(params[0]).unbind(".validate-dateGreaterThan").bind "blur.validate-dateGreaterThan", ->
    jQuery(element).valid()
  return @optional(element) or new Date(value) >= new Date(target.val())
"Must be after {1}")

jQuery.validator.addMethod("dateLessThan", (value,element,params) ->
  # bind to the blur event of the target in order to revalidate whenever the target field is updated
  # TODO find a way to bind the event just once, avoiding the unbind-rebind overhead
  target = jQuery(params[0]).unbind(".validate-dateLessThan").bind "blur.validate-dateLessThan", ->
    jQuery(element).valid()
  return @optional(element) or new Date(value) < new Date(target.val())
"Must be before {1}")

jQuery.validator.addMethod("specificFile", (value,element,params) ->
  console.log('in validate specificFile',value,element,params)
  expected_file = params[0]
  selected_file = value
  console.log("--> checking",expected_file,selected_file);
  # The following line checks the selected file (path + filename) ends with the expected value (filename)
  # console.log('--> numbers',selected_file.lastIndexOf(expected_file),(selected_file.length - expected_file.length))
  if selected_file.length > 0 and expected_file.length > 0 and selected_file.lastIndexOf(expected_file) != (selected_file.length - expected_file.length)
    # check for the "expert" parameter
    if params[1]
      message = "You selected #{HandyworksApp.endingAfter(selected_file,"\\")} rather than #{expected_file}. "
      return confirm(message + "Are you sure?")
    else
      return false
  return true
"You must select the file {0}")

jQuery.validator.addMethod( "dateTime", (value, element) ->
  check = false
  re = /^\d{1,2}\/\d{1,2}\/\d{4} ([01][0-9])|(2[0123]):([0-5])([0-9])$/
  if re.test(value)
    adata = value.split('/')
    mm = parseInt(adata[0],10)
    gg = parseInt(adata[1],10)
    aaaa = parseInt(adata[2],10)
    xdata = new Date(aaaa,mm-1,gg)
    if xdata.getFullYear() == aaaa and xdata.getMonth() == mm - 1 and xdata.getDate() == gg
      check = true
    else
      check = false
  else
    check = false
  return this.optional(element) or check
"Please enter a correct date time (mm-dd-yyyy hh:mi)")

jQuery.validator.addMethod( "dateHW", (value, element) ->
  check = false
  re = /^\d{1,2}\/\d{1,2}\/\d{4}$/
  if re.test(value)
    adata = value.split('/')
    mm = parseInt(adata[0],10)
    gg = parseInt(adata[1],10)
    aaaa = parseInt(adata[2],10)
    xdata = new Date(aaaa,mm-1,gg)
    if xdata.getFullYear() == aaaa and xdata.getMonth() == mm - 1 and xdata.getDate() == gg
      check = true
      console.log("passed date check",xdata.toString())
    else
      check = false
      console.log("failed date match",xdata.toString(),aaaa, mm, gg)
  else
    console.log("failed re check",value,element)
    check = false
  return this.optional(element) or check
"Please enter a correct date time (mm-dd-yyyy)")

jQuery.validator.addMethod("EINSS", (value, element) ->
  check = false
  regexObj = /^\(?([0-9]{3})\)?[-]?([0-9]{2})[-]?([0-9]{4})$/
  regexObj2 = /^\(?([0-9]{2})\)?[-]?([0-9]{7})$/
  if (regexObj.test(value) || regexObj2.test(value))
    check = true
  else
    check = false
  return check
)
