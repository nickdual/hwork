_.extend window.HandyworksApp,

  resourceSwitcherSetup: (switcher_url, resource_name) ->
    jQuery('span.switcher > select').change (event) ->
      b_url = switcher_url
      b_val = jQuery(event.target).val()
      b_target = b_url + "/"  + b_val + "/edit?popup=1"
      console.log("in switcher change callback", b_target)
      if HandyworksApp._fbBeforeSwitch('switcher')
        jQuery.fancybox.showLoading()
        jQuery('.fancybox-inner > div').load b_target, (responseText, textStatus, XMLHttpRequest) ->
          console.log("switcher content loaded",resource_name,b_val)
          HandyworksApp._setupEditPopup(jQuery('.fancybox-inner'))
          HandyworksApp.dispatcher.trigger("resourceswitch",resource_name,b_val)
