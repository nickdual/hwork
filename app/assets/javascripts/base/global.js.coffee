# OPTIMIZE : move these functions into the HandyworksApp namespace at some point.
#-------------------------------------------------------------------------------
# Application Global Area
#-------------------------------------------------------------------------------
#  Support for Mixins in Coffee script
#    http://arcturo.github.com/library/coffeescript/03_classes.html
# Usage
#   include Parrot,
#     isDeceased: true
#   (new Parrot).isDeceased
#-------------------------------------------------------------------------------
window.extend = (obj, mixin) ->
  obj[name] = method for name, method of mixin
  obj

window.include = (klass, mixin) ->
  extend klass.prototype, mixin

#------------------------------------------------------------------------------
# when using JavaScript styled form, need to reset the fields after dynamic
# updates and form resets
#------------------------------------------------------------------------------
window.reset_uni_form = (link) ->
  link.form.reset()
  # TODO : reset_uni_form should no longer be needed after removing uni form  plugin

#------------------------------------------------------------------------------
# sets up the folding / hiding toggle for advanced option field sets.
# TODO : turn the advanced option functions into a jQuery plugin / widget.
#------------------------------------------------------------------------------
window.init_advanced_option = (top=jQuery('body')) ->
  advanced_options = jQuery("fieldset.advanced").not(".initialized").addClass("initialized")
  advanced_options.children("ol").hide()
  advanced_options.children("legend").append(
    '<span class="advanced_icon ui-icon ui-icon-circle-triangle-s"></span>'
  ).click (event) ->
    console.info('-- --> in advanced_option cb',event)
    jQuery(event.target).toggleClass("ui-icon-circle-triangle-s").toggleClass("ui-icon-circle-triangle-n")
    jQuery(event.target).parent().parent().children("ol").toggle("easeInOutExpo")

#------------------------------------------------------------------------------
# setup any local tip dialogs, these are similar to the remotely fetched
# ajax dialogs; however, if the client side logic needs to affect the content
# then its more efficient to just pass the help information along, let the 
# client side figure it out and display it. The HTML structure required to
# use this needs to include an icon element (the trigger) and the content
# as sibling elements with the classes as follows as an example (in haml):
#
#    .local_tip_icon.icon16.help
#    .local_tip 
#      %p 
#         You need to click something first.
#
# also going to initialize the bootstrap popover fields as well. there are
# two variations
#
#   .pop              => defines a dialog that will display on hover
#   .pop.pop-initial  => defines a dialog that will display on page load
#
#------------------------------------------------------------------------------
window.init_local_tips = (top = jQuery('body')) ->
  jQuery(".local_tip",top).each (index,element) ->
    jqe = jQuery(element)
    jqe.prev(".local_tip_icon").data("local_tip_dialog",element).click (event) ->
      d_element = jQuery(event.target).data("local_tip_dialog")
      jQuery(d_element).dialog('open')
    jqe.dialog({ zIndex: HandyworksApp.Configuration.fbZindex, autoOpen: false })
  # initialize  the bootstrap popover help dialogs.
  jQuery('.pop').popover
    trigger: 'hover'  # backward compatibility with bootstrap v2.0.4
  unless HandyworksApp.Data.popInit
    jQuery('.pop.pop-initial').popover('show')
    HandyworksApp.Data.popInit = true

#------------------------------------------------------------------------------
# Basic style application, can also be called after DOM manipulation 
#   e.g. AJAX update
#
# applies styles to any input selected below
# specific fields can be excluded from the list by adding the no-style
# class to them.
# (Bootstrap) tooltip: create overlay title popup's using CSS
# does not apply changes in the test environment, specifically Selenium 
#  does not play well with JavaScript styled forms.
#
# use the 'top' argument to restrict the scope of application when applying
# stylings to newly added DOM elements. This improves efficiency.
#------------------------------------------------------------------------------
window.put_on_style = (top=jQuery('body')) ->
  unless HandyworksApp.Configuration.rails_env in ["test","cucumber"]
    jQuery(".crud_button",top).button()
    #jQuery(".option_link, .field_help, .field_opt, .crud_buttons a",top).tooltip()
    # handle foling of advanced options
    init_advanced_option(top)
    init_local_tips(top)
    # add the window.location.hash to include internal link and query string
    # set_2nd_control_button(window.location.pathname + window.location.hash)
    under_construction()

window.under_construction = ->
  jQuery("#under_construction")

#------------------------------------------------------------------------------
# Railscasts #197 Nested Forms
#------------------------------------------------------------------------------
window.remove_fields = (link) ->
  jQuery(link).prev("input[type=hidden]").val("1")
  jQuery(link).closest('.fields').hide()

window.add_fields = (link, association, content, callback) ->
  new_id = new Date().getTime()
  regexp = new RegExp("new_" + association, "g")
  if jQuery(link).closest("#add_fields_here")
    jQuery(link).closest("#add_fields_here").before(content.replace(regexp, new_id))
  else if jQuery("#add_fields_here")
    jQuery("#add_fields_here").before(content.replace(regexp, new_id))
  else
    jQuery(link).parent().before(content.replace(regexp, new_id))
  if callback
    callback(new_id)
  put_on_style()

#------------------------------------------------------------------------------
# Combo Box Form Options
#   Loosely based on Railscasts #197 nested forms.
#------------------------------------------------------------------------------
# array of saved selection field contents
window.combo_fields_saved = new Object()

# extracts the id numeric value from the id attribute of a nested form element
window.combo_get_id = (in_id) ->
  parts = in_id.split('_')
  for p in parts
    # check for numeric parts, return the first found
    unless isNaN(p)
      return p

#------------------------------------------------------------------------------
# swaps the "new" vs. static fields from  the current DOM.
#  the initial fields which are deleted are stored in the 
#  combo fields saved array to potentially be swapped back
#  into the form again.
#------------------------------------------------------------------------------
window.combo_fields = (link, association, content) ->
  container = jQuery(link).closest(".combo_fields_here")
  if content?
    # switch from select to new and save select fields to hash
    new_id = new Date().getTime()
    regexp = new RegExp("new_" + association, "g")
    saved_fields = container.children().remove()
    container.append(content.replace(regexp, new_id))
    container.find(".combo_field_link").removeClass("add").addClass("stop-alt")
    id = combo_get_id(container.find("input").first().attr('id'))
    combo_fields_saved[id] = saved_fields
  else
    # switch from new to select, the fields to be selected were
    # previously stored in combo_fields_saved, just need to get the id.
    id = combo_get_id(container.find("input").first().attr('id'))
    container.children().remove()
    h = combo_fields_saved[id]
    container.append(h)
    container.find(".combo_field_link").removeClass("stop-alt").addClass("add")

#------------------------------------------------------------------------------
# removes the row from the table for the current link
#   generically its removing the contents of the closest .arow ancestor.
#------------------------------------------------------------------------------
window.remove_a_row = (link, to_confirm) ->
  to_confirm = if typeof(to_confirm) != 'undefined' then to_confirm else true
  if not to_confirm or confirm('Are you sure?')
    jQuery(link).closest(".arow").remove()

#------------------------------------------------------------------------------
# removes a row from the association table and updates the
# _destroy option for this element to be true
#------------------------------------------------------------------------------
window.destroy_a_row = (link, to_confirm) ->
  destroy_input = jQuery(link).closest(".arow").next("input.destroy-resource")
  remove_a_row(link,to_confirm)
  destroy_input.val("1")

#------------------------------------------------------------------------------
# Permissions Form Helpers
#------------------------------------------------------------------------------
window.set_admin = (option) ->
  perms = jQuery(option).closest('tr').find('select')
  perms.val('')
  perms.attr('disabled',true).addClass('disabled')

window.clear_admin = (option,set_values) ->
  perms = jQuery(option).closest('tr').find('select')
  perms.removeAttr('disabled').removeClass('disabled')
  if (set_values)
    perms.val('0')

window.admin_select = (option) ->
   if ($(option).is(":checked"))
      set_admin(option)
   else
      clear_admin(option,true)

window.admin_reset = (link) ->
  reset_uni_form link
  # fix disabled select boxes
  jQuery("input[type=checkbox]").not(":checked").each (index,option) ->
    clear_admin(option,false)
  jQuery("input[type=checkbox]").filter(":checked").each (index,option) ->
    set_admin(option)



