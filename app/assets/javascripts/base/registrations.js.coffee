_.extend window.HandyworksApp,

  #-----------------------------------------------------------------------------
  # validation logic for the 'Edit Profile' form.
  #-----------------------------------------------------------------------------
  userProfileFormValidation: (user_id, user_email) ->
    HandyworksApp.validate "form.user",
      messages:
        'user[email]':
          remote: jQuery.validator.format("{0} has already been taken")
      rules:
        'user[email]':
          required: true
          email: true
          minlength: 5
          maxlength: 255
          remote:
            param:
              url: "#{root_path}validate/user/email.json"
              data:
                'user[id]': user_id
            depends: ->
              return $("#access_user_email").val() != user_email
        'user[current_password]':
          required: true
        'user[password_confirmation]':
          equalTo: '#access_user_password'

  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  userFormValidation: (user_id, user_role, user_email) ->
    HandyworksApp.validate "form.user",
      messages:
        'user[email]':
          remote: jQuery.validator.format("{0} has already been taken")
        'user[account_role]':
          remote: jQuery.validator.format("You can not change the account role, there must be an admin for each account")
      rules:
        'user[account_role]':
          required: true
          remote:
            param:
              url: "#{root_path}validate/user/account_role.json"
              data:
                'user[id]': user_id
            depends: ->
              return $("#user_account_role").val() != user_role
        'user[email]':
          required: true
          email: true
          minlength: 5
          maxlength: 255
          remote:
            param:
              url: "#{root_path}validate/user/email.json"
              data:
                'user[id]': user_id
            depends: ->
              return $("#user_email").val() != user_email
        'user[password_confirmation]':
          equalTo: '#user_password'

#-------------------------------------------------------------------------------
# The following calls will only be made against the registration screens.
#-------------------------------------------------------------------------------
jQuery(".registrations").ready ->

  jQuery("form.new_user").ready ->
    initial_id = jQuery('form.new_user input#user_id').val()
    initial_email = jQuery('form.new_user input#user_email').val()
    HandyworksApp.validate "form.new_user",
      messages:
        'user[email]':
          remote: jQuery.validator.format("{0} has already been taken")
      rules:
        'user[email]':
          required: true
          email: true
          minlength: 6
          maxlength: 128
          remote:
            param:
              url: "#{root_path}validate/user/email.json"
              data:
                'user[id]': 0
            depends: ->
              return $("#user_email").val() != initial_email


# From the old views/devise/confirmations/show.html.haml
#
#:coffeescript
#  jQuery ->
#    # client side validation
#    HandyworksApp.validate "form.user",
#      rules:
#        'user[password]':
#          required: true
#        'user[password_confirmation]':
#          required: true
#          equalTo: '#user_password'
#        'user[account_attributes][clinic_name]':
#          required: true
#        'user[contact_attributes][first_name]':
#          required: true
#        'user[contact_attributes][last_name]':
#          required: true
