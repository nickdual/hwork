#-------------------------------------------------------------------------------
# This logic came from shared/_form_buttons_v2
#-------------------------------------------------------------------------------
_.extend window.HandyworksApp,
  setupFormButtons: ->
    # if we have a cancel button hook up the reset / close logic
    jQuery('.form-buttons input[type="button"].cancel').unbind('click').click (event) ->
      console.info("cancel button pressed!",event)
      event.target.form.reset()
      # this next step shouldn't be necessary; however, its an additional step to ensure closing the form
      # by clicking cancel does not trigger a submit
      HandyworksApp.Data.popupCanceled = true
      jQuery.fancybox.close()
    # if we have a submit button (in a popup) hook up the submit / close logic
    jQuery('.form-buttons input[type="submit"].submit.popup-true').unbind('click').click (event) ->
      event.preventDefault()
      console.info("going to try to submit this stuff")
      # if there are any WYSIWYG editor interfaces open, then we need to push any
      # changes from the editors down to the textareas in order for the form validation 
      # and submission logic to pick up any changes.
      if CKEDITOR.instances?
        _.each CKEDITOR.instances, (editor)->
          editor.updateElement()
      HandyworksApp.Data.popupSubmitted = 1
      HandyworksApp.Data.popupFormSubmitted = jQuery(event.target).closest('form')
      console.info("selected form",HandyworksApp.Data.popupFormSubmitted)
      jQuery.fancybox.close()
