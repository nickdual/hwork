/*******************************************************************************
 * Employ Google Fonts.
 *   See https://developers.google.com/webfonts/docs/webfont_loader
 *
 * NOTE: When using the script from the above noted site directly there was 
 *   an undesirable "flash" on the logo text displaying first in a base font 
 *   like sans-serif.
 *
 *   Also when converted to coffee script doesn't work because the WebFontConfig
 *   needs to be set before the document is ready.
 ******************************************************************************/
WebFontConfig = {
  google: { families: [ 'Indie+Flower', 'Terminal+Dosis' ] }
};

(function() {
  jQuery.externalScript('https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js');
})();
