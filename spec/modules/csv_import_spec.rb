require 'spec_helper'
require 'csv_import'

class Dummy
  include CsvImport
end

describe CsvImport do
  before(:each) do
    @dummy = Dummy.new
  end

  # test DATE_PATTERN
  describe "DATE_PATTERN" do
    it "should define DATE_PATTERN" do
      pending "this doesn't appear to be how the constant is included in the class"
      Dummy::DATE_PATTERN.should exist
    end
  end

  # csv_headers
  describe "csv_headers" do
    it "should define csv_headers" do
      Dummy.csv_headers.should be_true
    end
  end

  # csv_converters
  describe "csv_converters" do
    it "should define csv_converters" do
      Dummy.csv_converters.should be_kind_of(Array)
    end

    describe "method csv_converter_dates" do
      pending "write tests for csv_converter_dates method" 
    end

    describe "method csv_converter_semicolons" do
      pending "write tests for csv_converter_semicolons method"
    end
  end

  # get_csv_instance
  describe "get_csv_instance" do
    it "should define get_csv_instance" do
      Dummy.should respond_to(:get_csv_instance)
    end
  end

  # load_csv_record
  describe "load_csv_record" do
    it "should define load_csv_record" do
      Dummy.should respond_to(:load_csv_record)
    end
  end

  # year2to4
  describe "year2to4" do
    it "should define year2to4" do
      Dummy.should respond_to(:year2to4)
    end
  end

  # csv_duplicate
  describe "csv_duplicate" do
    it "should define csv_duplicate" do
      @dummy.should respond_to(:csv_duplicate)
    end
  end

  # get_csv_config
  describe "get_csv_config" do
    it "should define get_csv_config" do
      Dummy.should respond_to(:get_csv_config)
    end
    it "should return the class config" do
      Message.get_csv_config('Letter')['account'].should eql('messageable=')
    end
    it "should return the base class config" do
      AccountHcfaFormOption.get_csv_config('HcfaFormOption')['account'].should eql('account=')
    end
    it "should return the base class config (2)" do
      AccountHcfaFormOption.get_csv_config('AccountHcfaFormOption')['account'].should eql('account=')
    end
  end

  describe "csv_update_attributes_from_import" do
    pending "write test for csv_update_attributes_from_import updates attributes based on csv import type configuration"
  end

  describe "csv_header_lookup" do
    pending "write tests for csv_header_lookup"
  end

  describe "csv_skip_row" do
    pending "write tests for csv_skip_row"
  end

end
