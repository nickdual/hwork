require 'spec_helper'
require 'public_appointment'

class Dummy
  include PublicAppointment
end

describe PublicAppointment do
  before(:each) do
    @dummy = Dummy.new
  end

  describe ".validate_times_inputs" do
    before(:each) do
      @params = {
        account_id: 1,
        clinic_id: 1,
        appointment_type_id: 1,
        provider_id: 1,
        start_time: Time.now.to_i * 1000
      }
    end
    it "valid for valid parameters" do
      Dummy.validate_times_inputs(@params).should be_true
    end
    it "requires clinic_id" do
      @params.delete(:clinic_id)
      Dummy.validate_times_inputs(@params).should be_false
    end
    it "requires appointment_type_id" do
      @params.delete(:appointment_type_id)
      Dummy.validate_times_inputs(@params).should be_false
    end
    it "requires provider_id" do
      @params.delete(:provider_id)
      Dummy.validate_times_inputs(@params).should be_false
    end
    it "requires start_time" do
      @params.delete(:start_time)
      Dummy.validate_times_inputs(@params).should be_false
    end
  end

  describe ".get_times_for_appointment" do
    before(:each) do
      @params = {
        account_id: 1,
        clinic_id: 1,
        appointment_type_id: 1,
        provider_id: 1,
        start_time: Time.now.to_i * 1000
      }
    end
    #it "returns nothing if the provider has no schedule" do
    #  times = Dummy.get_times_for_appointment(@params)
    #  times.should be_empty
    #end
  end
end
