FactoryGirl.define do
  factory :patient do
    association :account, :subscribed
    association :clinic
    association :contact, factory: :contact_patient
  end
end
