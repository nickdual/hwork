# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :diagnosis_code do
    name 'Code'
    code '123AA'
    description 'Test Code Desc'
    association :account, :subscribed
  end
end
