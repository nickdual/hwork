# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :account do
    # no required attributes.
    trait :subscribed do
      association :subscription
      state :ready
    end
  end
end
