# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :clinic do

    tax_uuid '12-2443997'
    account
    contact factory: :contact_clinic

    trait :with_account_subscribed do
      association :account, :subscribed
    end

    trait :without_contact do
      contact nil
    end

    factory :clinic_with_account, :traits => [:with_account_subscribed]
    factory :clinic_without_contact, :traits => [:without_contact]
  end
end
