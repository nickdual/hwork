# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :fee_schedule do
    label 'Default'
    account_id 999999
    alt_id 1
    association :account, :subscribed
  end

end


