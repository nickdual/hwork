
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  factory :email do
    email { FactoryGirl.generate(:email_address) }
  end

end
