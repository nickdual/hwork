# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :new_patient_option do
    default_place_of_service 11
    overdue_fee_percentage 18
    default_provider_alt_id 13
    association :clinic
    association :default_provider
  end
end

