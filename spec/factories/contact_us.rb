# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  factory :contact_us do
    name  'Some Person'
    comment 'Hello'
    email { FactoryGirl.generate(:email_address) }

    trait :full_details do
      city  'New York'
      phone '555-333-1234'
    end

    factory :contact_us_page, :traits => [:full_details]
  end

end
