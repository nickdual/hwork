# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :schedule do
    max_concurrent_appointments 2
    max_appointments_per_hour 6
  end

end




