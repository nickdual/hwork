# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :address do
    street { FactoryGirl.generate(:street_address) }
    city 'Fitzwilliam'
    state 'NH'
    zip '03447'
    label 'Physical'
  end

end


