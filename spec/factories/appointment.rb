# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :appointment do
    provider
    room
    appointment_type
    calendar
    event
    after(:create) { |appointment|
      appointment.event.eventable = appointment
      appointment.save!
    }
  end

end



