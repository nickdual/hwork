# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :room do
    name { FactoryGirl.generate(:room_name) }
    association :account, :subscribed
  end
end

