# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :webhook do
    message "MyString"
    association :subscription
  end
end
