# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  factory :user do
    login 'Test User'
    email { FactoryGirl.generate(:email_address) }
    password 'please'
    password_confirmation 'please'

    trait :with_account do
      account
    end

    trait :with_account_subscribed do
      association :account, :subscribed
    end

    trait :confirmed do
      after(:create) { |user| user.confirm! }
    end

    trait :as_app_admin do
      after(:create) { |user| user.add_role Role::APPLICATION_ADMIN }
    end

    trait :as_admin do
      after(:create) { |user| user.add_role Role::ACCOUNT_ADMIN }
    end

    trait :as_staff do
      after(:create) { |user| Permission.initialize_user_roles(user) }
    end

    factory :confirmed_user, :traits => [:confirmed]
    factory :subscribed_user, :traits => [:with_account_subscribed, :confirmed, :as_staff]
    factory :subscribed_admin, :traits => [:with_account_subscribed, :confirmed, :as_admin]
    factory :application_admin, :traits => [:with_account_subscribed, :confirmed, :as_app_admin]
  end
end
