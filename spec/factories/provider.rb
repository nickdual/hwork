# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :provider do
    signature_name { FactoryGirl.generate(:provider_signature_name) }
    your_code { FactoryGirl.generate(:provider_code) }
    notes 'Test Code Desc'
    association :account, :subscribed
    association :contact, factory: :contact_provider
    factory :default_provider, class: Provider
  end
end

