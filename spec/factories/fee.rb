# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :fee do
    is_percentage 1
    procedure_code_id 1
    fee_schedule_id 1
    alt_id 1
    fee_cents 3800
    copay_pct 0.2
    expected_insurance_payment_cents 3040
    association :procedure_code
    association :fee_schedule
  end

end


