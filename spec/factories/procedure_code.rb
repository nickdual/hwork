# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :procedure_code do
    name 'A'
    description 'Spinal Adj./Manip.'
    type_code 6
    service_type_code 0
    cpt_code 72020
    account_id 999999
    tax_rate_percentage 0
    alt_id 1
    association :account, :subscribed
  end
  factory :procedure_code2 ,:class => ProcedureCode do
    name 'B'
    description 'Spinal Adj./Manip.'
    type_code 6
    service_type_code 0
    cpt_code 72020
    tax_rate_percentage 0
    alt_id 1
    association :account, :subscribed
  end

end


