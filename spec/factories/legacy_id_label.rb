# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :legacy_id_label do
    label 'License'
    alt_id 1
    association :account, :subscribed
  end

end


