# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :question do
    title "You have a question?"
    body "This is the answer."

    trait :draft do
      after(:create) { |question| question.state = 'draft' }
    end

    trait :published do
      after(:create) { |question| question.state = 'published' }
    end

    factory :draft_question, :traits => [:draft]
    factory :published_question, :traits => [:published]
  end
end
