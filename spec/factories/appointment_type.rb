# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :appointment_type do
    name { FactoryGirl.generate(:appointment_type_name) }
    duration_min 15
    duration_max 60
    schedule_lead_time 0
    duration 30
    provider_time 30
    is_public true
    for_new_patients true

    trait :with_account do
      account
    end
  end
end

