# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :message do
    label 'New Patient'
    messageable_id 999999
    messageable_type 'Account'
    alt_id 'New Patient'
    body '$(TodayShortDate}<BR>$(PatientFullName}<BR>${PatientAddress}<BR>Dear ${PatientFirstName},<BR><BR>Welcome to my office.  I hope that your first visit was satisfactory.<BR><BR>As a Chiropractor and Acupuncturist, I see a wide range of health-related concerns. I am continually encouraged by the beneficial effects of the treatment protocols we administer.  While not everyone responds equally to each modality, most people find that they get some relief after their first few visits.  And over time, Chiropractic and Acupuncture treatments have helped more fully solve many problems that had previously not responded to other therapies. Please keep in mind that problems of longer and greater intensity will require a longer and more intense course of treatment.  Rest assured that I will take the time to give you the utmost in care.      <BR><BR>I encourage you to refer your friends, family and coworkers to this office as well, because whether they are in pain or not, people of all ages can greatly benefit from the services we provide here. <BR><BR>Also, please let me know if there are any other areas in which I can assist you, for example, facial acupuncture, our newest service. My associates are specialists in such fields as massage, nutrition, herbology and homeopathy. I also maintain a very comprehensive referral network with a wide range of other health care providers.  Depending on your health related issue, most likely I can assist you or I can refer you to someone who can (and I would be happy to do so).<BR><BR>We have probably discussed some aspects of my office policies.  If you are concerned with any aspect of our service procedures, please mention it to me. I am committed to working honestly and ethically and to being responsive to your needs. I encourage your comments and suggestions.<BR><BR>During the course of your treatment, I expect to build a relationship that helps healing occur.<BR><BR>Sincerely,<BR><BR>${ProviderName}'
    message_format 'Letter(snail)'
  end

end


