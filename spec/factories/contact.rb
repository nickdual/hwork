# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contact do
    ignore do
      phones_count 1
      emails_count 1
      addr_count 1
    end

    # support the mapping of addresses and phones from the generic list to 
    # the specific labels
    after(:create) do |contact, evaluator|
      contact.reload
    end

    trait :with_phones do
      after(:create) do |contact, evaluator|
        FactoryGirl.create_list(:phone, evaluator.phones_count, phoneable: contact)
      end
    end

    trait :with_emails do
      after(:create) do |contact, evaluator|
        FactoryGirl.create_list(:email, evaluator.emails_count, emailable: contact, label: 'Business')
      end
    end

    trait :with_addresses do
      after(:create) do |contact, evaluator|
        FactoryGirl.create_list(:address, evaluator.addr_count, addressable: contact)
      end
    end

    trait :as_person do
      first_name 'Rob'
      last_name 'Bob'
      occupation 'Scientist'
    end

    trait :as_company do
      company_name 'IniTech'
    end

    factory :contact_provider, class: ContactProvider, traits: [:as_person] do
      after(:create) do |contact, evaluator|
        FactoryGirl.create_list(:email, evaluator.emails_count, emailable: contact, label: 'Business')
        FactoryGirl.create_list(:phone, evaluator.phones_count, phoneable: contact, label: 'Work')
        contact.reload
      end

    end

    factory :contact_patient, class: ContactPatient, traits: [:as_person, :with_addresses]

    factory :contact_clinic, class: ContactClinic, traits: [:as_company, :with_phones, :with_addresses] 

    factory :contact_third_party, class: ContactThirdParty, traits: [:as_company, :with_addresses, :with_phones]

  end
end

