# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :help do
    label "help_label"
    body "Help Body"
    title "Help Title"
  end
end
