# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  sequence :email_address do |n|
    "email#{n}@factory.com"
  end

  sequence :appointment_type_name do |n|
    "Test Appt Type #{n}"
  end

  sequence :street_address do |n|
    "#{n} Main St."
  end

  sequence :provider_code do |n|
    "DO#{n}"
  end

  sequence :provider_signature_name do |n|
    "Dr. Test Alot the #{n}th"
  end

  sequence :room_name do |n|
    "Room #{n}"
  end

end
