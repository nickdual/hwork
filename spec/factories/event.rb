# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :event do
    association :eventable, factory: :schedule
    start_time { 2.days.from_now }
    duration  45
    day_of_week 1
    lead_time 10
    lag_time 10
  end
end

