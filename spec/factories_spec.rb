#-------------------------------------------------------------------------------
# This test is going to ensure that each of the defined factories is creating
# a valid model by default. Theoretically these tests will be run before all
# the other specs when runing `rake` to perform all tests which allows you 
# to catch a Factory definition problem ( normally resulting from model changes
# ) first before waiting for the entire test suite to run.
#
# FIXME : see Rakefile which adds factory_specs to the default rake task, but doesn't seem to be working.
#-------------------------------------------------------------------------------

require 'spec_helper'

FactoryGirl.factories.map(&:name).each do |factory_name|
  describe "The #{factory_name} factory" do
    it 'is valid' do
      FactoryGirl.build(factory_name).should be_valid
    end
  end
end
