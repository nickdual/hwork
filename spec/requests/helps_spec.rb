require 'spec_helper'

describe "Helps" do
  describe "GET /helps" do
    # See the following reference for this approach to testing without authenticating explicitly
    #   https://github.com/plataformatec/devise/wiki/How-To:-Test-with-Capybara   
    #   See Also spec/support/devise.rb
    before(:each) do
      Warden.test_mode!
      @user = FactoryGirl.create(:application_admin)
      login_as @user, scope: :user
    end
    after(:each) do
      Warden.test_reset!
    end

    it "works! (now write some real specs)" do
      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
      get helps_path
      response.status.should be(200)
    end
  end
end
