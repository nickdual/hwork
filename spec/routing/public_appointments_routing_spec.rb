require "spec_helper"

describe PublicAppointmentsController do
  describe "routing" do

    it "routes by account id to #new" do
      get("/appts/111").should route_to("public_appointments#new", :account_id => "111")
    end

    it "routes by account and clinic to #new" do
      get("/appts/222/clinics/333").should route_to("public_appointments#new", :account_id => "222", :clinic_id => "333")
    end

    it "routes for available times to #times" do
      get("/appts/333/times").should route_to("public_appointments#times", :account_id => "333")
    end

  end
end
