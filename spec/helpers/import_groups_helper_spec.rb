require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the ImportGroupsHelper. For example:
#
# describe ImportGroupsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       helper.concat_strings("this","that").should == "this that"
#     end
#   end
# end
describe ImportGroupsHelper do
  describe "method get_csv_configuration_for_js" do
    pending "add tests for get_csv_configuration_for_js #{__FILE__}"
  end
  describe "method get_available_import_groups" do
    pending "add tests for get_available_import_groups #{__FILE__}"
  end
  describe "method get_current_imports_for_js" do
    pending "add tests for get_current_imports_for_js #{__FILE__}"
  end
end
