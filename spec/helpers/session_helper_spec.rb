require 'spec_helper'

describe SessionHelper do
  before(:each) do
    @user = FactoryGirl.create(:subscribed_user)
    @account = @user.account
    @clinic = FactoryGirl.create(:clinic, :account => @account)
    @clinic2 = FactoryGirl.create(:clinic, :account => @account)
  end

  describe "method current_patient" do
    it "should handle an invalid id (e.g. deleted record id)" do
      pending "implement current_patient methods"
    end
    it "should return nil if the current user does not have access to patient data" do
      pending "implement current_patient access control tests"
    end
  end
  describe "method current_clinic" do
    it "should handle an invalid id (e.g. deleted record id)" do
      pending "implement current_clinic handles an invalid id"
    end
    it "should return nil if user is not signed in" do
      helper.stub(:current_user) { nil }
      helper.current_clinic.should be_nil
    end

    it "should first look at the session clinic" do
      session[:clinic_id] = @clinic.id
      helper.stub(:current_user) { @user }
      helper.current_clinic.should eql(@clinic)
    end

    it "should return the user's prefered clinic" do
      @user.stub(:clinic) { @clinic2 }
      helper.stub(:current_user) { @user }
      helper.current_clinic.should eql(@clinic2)
    end

    it "should return the first clinic on the account" do
      helper.stub(:current_user) { @user }
      helper.current_clinic.should eql(@clinic)
    end

    it "should handle users with no clinics" do
      helper.stub(:current_user) { @user }
      @user.stub(:clinic) { nil }
      @account.stub(:clinics) { [] }
      helper.current_clinic.should be_nil
    end
  end

  describe "method current_clinic_list" do
    it "should return nil if a user is not signed in" do
      helper.stub(:current_user) { nil }
      helper.current_clinic_list.should be_nil
    end

    it "should return clinics the current_user has access to" do
      helper.stub(:current_user) { @user }
      helper.stub(:current_account) { @user.account }
      helper.current_clinic_list.size.should eql(2)
      helper.current_clinic_list.should include(@clinic)
      helper.current_clinic_list.should include(@clinic2)
    end

    it "should return an empty list if the account has no clinics" do
      user = FactoryGirl.create(:subscribed_user)
      helper.stub(:current_user) { user }
      helper.stub(:current_account) { user.account }
      helper.current_clinic_list.size.should eql(0)
    end
  end
end
