require 'spec_helper'
# Specs in this file have access to a helper object that includes
# the ClinicsHelper. For example:
#
# describe ClinicsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       helper.concat_strings("this","that").should == "this that"
#     end
#   end
# end
describe PhonesHelper do
  describe "phone_fmt" do
    it "displays 7 or few chars w/o area code" do
      helper.phone_fmt('1234567').should == '123-4567'
    end

    it "displays 10 chars w/ area code format" do
      helper.phone_fmt('1234567890').should == '(123) 456-7890'
    end

    it "displays 11 or characters with extension" do
      helper.phone_fmt('123456789012').should == '(123) 456-7890 x 12'
    end
  end

  describe "phone_parse" do
    it "strips [()-x] chars" do
      a,b = helper.phone_parse('(123) 456-7890 x 123')
      a.should be_nil
      b.should == '1234567890123'
    end

    it "handles nil values" do
      a,b = helper.phone_parse(nil)
      a.should be_nil
      b.should be_nil
    end

    it "handles a stripped number" do
      a,b = helper.phone_parse('1234567')
      a.should be_nil
      b.should == '1234567'
    end

    it "handles a country code" do
      a,b = helper.phone_parse('+86 (10) 69445464')
      a.should eql('86')
      b.should eql('1069445464')
    end
  end
end
