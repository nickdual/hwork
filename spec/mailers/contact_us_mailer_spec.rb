require "spec_helper"

describe ContactUsMailer do
  describe '#send_contact' do
    let(:contact) { FactoryGirl.create(:contact_us, comment: 'Test Contact') }
    let(:mail) { ContactUsMailer.send_contact(contact.id, ENV['ADMIN_EMAIL']) }

    it "has the correct user email" do
      mail.to.should == [ENV['ADMIN_EMAIL']]
    end

    it "has the correct senders email" do
      mail.from.should == ["abc830844@gmail.com"]
    end

    it "has the correct subject" do
      mail.subject.should == "Contact Us"
    end

    it "contains the comment" do
      mail.body.should have_text("Test Contact")
    end

  end
end
