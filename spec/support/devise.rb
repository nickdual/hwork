# See the following reference for this approach to testing without authenticating explicitly
#   https://github.com/plataformatec/devise/wiki/How-To:-Test-with-Capybara   
#   See Also spec/support/devise.rb
RSpec.configure do |config|
  config.include Devise::TestHelpers, :type => :controller
  config.include Warden::Test::Helpers, :type => :request
end
