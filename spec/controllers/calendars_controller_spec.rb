require 'spec_helper'

describe CalendarsController do

  before(:each) do
    @user = FactoryGirl.create(:subscribed_admin)
    sign_in @user
  end

  def mock_calendar(stubs={:account => @user.account})
    @mock_calendar ||= mock_model(Calendar, stubs).as_null_object
  end

  describe "GET edit" do
    it "assigns the requested calendar as @calendar" do
      get :edit
      assigns(:calendar).should == @user.account.calendar
    end
    describe "assigns additional values for client initialization" do
      describe "for rooms" do
        it "(empty)" do
          get :edit
          assigns(:rooms).should be_empty
        end
        it "(non-empty)" do
          room = FactoryGirl.create(:room, :account => @user.account)
          @user.account.rooms << room
          get :edit
          assigns(:rooms).should == [room]
        end
      end
      describe "for providers" do
        it "(empty)" do
          get :edit
          assigns(:providers).should be_empty
        end
        it "(non-empty)" do
          provider = FactoryGirl.create(:provider, :account => @user.account)
          @user.account.providers << provider
          get :edit
          assigns(:providers).should == [provider]
        end
      end
      describe "for clinics" do
        it "(empty)" do
          get :edit
          assigns(:clinics).should be_empty
        end
        it "(non-empty)" do
          clinic = FactoryGirl.create(:clinic, :account => @user.account)
          @user.account.clinics << clinic
          get :edit
          assigns(:clinics).should == [clinic]
        end
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested calendar" do
        calendar_id = @user.account.calendar.id
        put :update, :calendar => {}
        calendar = Calendar.find(calendar_id)
      end

      it "assigns the requested calendar as @calendar" do
        Calendar.stub(:find) { mock_calendar(:update_attributes => true, :account_id => @user.account.id) }
        put :update, :id => 1
        assigns(:calendar).should be(mock_calendar)
      end

      it "redirects to the schedulable path" do
        Calendar.stub(:find) { mock_calendar(:update_attributes => true, :account_id => @user.account.id) }
        put :update, :id => 1
        response.should redirect_to(edit_schedule_path)
      end
    end

    describe "with invalid params" do
      it "assigns the calendar as @calendar" do
        Calendar.stub(:find) { mock_calendar(:update_attributes => false) }
        put :update, :id => 1
        assigns(:calendar).should be(mock_calendar)
      end

      it "re-renders the 'edit' template" do
        Calendar.stub(:find) { mock_calendar(:update_attributes => false, :account_id => @user.account.id) }
        put :update, :id => 1
        response.should render_template("edit")
      end
    end
  end

end
