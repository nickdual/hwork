require 'spec_helper'

describe ContactUsController do
  describe 'PUT #send_us' do
    let(:contact) { FactoryGirl.create(:contact_us) }
    let(:contact_mailer) { mock }
    before do
      ContactUsMailer.should_receive(:send_contact).with(contact.id, ENV['ADMIN_EMAIL']).and_return(contact_mailer)
      contact_mailer.should_receive(:deliver!)
      vars = { "name" =>  contact.name, "email" => contact.email, "comment" => contact.comment }
      ContactUs.should_receive(:new).with(vars).and_return(contact)
      post :send_us, :format => :js, :contact_us => vars
    end
    it "sets the flash" do
      flash[:notice].should == "Contact was successfully sent."
    end
  end
end
