require 'spec_helper'

describe ContentController do

  before (:each) do
    @user = FactoryGirl.create(:subscribed_user)
    sign_in @user
    @ability = Ability.new(@user)
  end

  describe "GET 'basic'" do
    it "returns http success" do
      get 'basic'
      response.should @ability.can?(:view, :basic) ? be_success : redirect_to(root_url)
    end
  end

  describe "GET 'plus'" do
    it "returns http success" do
      get 'plus'
      response.should @ability.can?(:view, :plus) ? be_success : redirect_to(root_url)
    end
  end

  describe "GET 'premium'" do
    it "returns http success" do
      get 'premium'
      response.should @ability.can?(:view, :premium) ? be_success : redirect_to(root_url)
    end
  end

  describe "GET 'max'" do
    it "returns http success" do
      get 'max'
      response.should @ability.can?(:view, :max) ? be_success : redirect_to(root_url)
    end
  end

end
