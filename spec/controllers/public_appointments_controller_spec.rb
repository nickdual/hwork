require 'spec_helper'

describe PublicAppointmentsController do

  before(:each) do
    @account = FactoryGirl.create(:account)
  end

  describe "GET 'index'" do
    it "should be successful" do
      get :index
      response.should be_success
    end
  end

  describe "GET 'new'" do

    describe "for valid account" do

      it "should be successful" do
        get :new, :account_id => @account.id
        response.should be_success
      end

      it "provides account context" do
        get :new, :account_id => @account.id
        assigns(:account).should == @account
      end

    end

    describe "for invalid account" do

      it "should fail (invalid account)" do
        get :new, :account_id => 2323
        response.should_not be_success
      end

      it "should redirect to index (invalid account)" do
        get :new, :account_id => 2424
        response.should redirect_to(public_appointments_url)
      end

    end

  end

  describe "GET 'new'" do
    describe "should succeed if" do
      it "has all required inputs" do
        get :times, :format => :json, :account_id => 1, :clinic_id => 1, :appointment_type_id => 1, :provider_id => 1, :start_time => Time.now.to_i
        response.should be_success
      end
    end
    describe "should fail if missing" do
      it "clinic_id" do
        get :times, :format => :json, :account_id => 1, :clinic_id => nil, :appointment_type_id => 1, :provider_id => 1, :start_time => Time.now.to_i
        response.should_not be_success
      end

      it "appointment_type_id" do
        get :times, :format => :json, :account_id => 1, :clinic_id => 1, :appointment_type_id => nil, :provider_id => 1, :start_time => Time.now.to_i
        response.should_not be_success
      end

      it "provider_id" do
        get :times, :format => :json, :account_id => 1, :clinic_id => 1, :appointment_type_id => 1, :provider_id => nil, :start_time => Time.now.to_i
        response.should_not be_success
      end

      it "start_time" do
        get :times, :format => :json, :account_id => 1, :clinic_id => 1, :appointment_type_id => 1, :provider_id => 1, :start_time => nil
        response.should_not be_success
      end
    end
  end

end
