require 'spec_helper'

describe UsersController do

  before (:each) do
    @user = FactoryGirl.create(:subscribed_admin)
    sign_in @user
  end

  describe "admin GET 'show'" do
    
    it "should be successful" do
      get :show, :id => @user.id
      Rails.logger.debug response.body.inspect
      Rails.logger.debug response.status.inspect
      response.should be_success
    end

    it "should find the right user" do
      get :show, :id => @user.id
      assigns(:user).should == @user
    end
    
  end

end
