#= require authorized

describe "Appointments Models", ->
  describe "Calendar", ->
    describe "default", ->
      calendar = undefined
      beforeEach ->
        calendar = new HandyworksApp.Models.Calendar()

      it "days should be 1", ->
        expect(calendar.get("days")).toEqual 1


    describe "getCalUsers method", ->
      it "should return a list of rooms if view_by is room", ->

      it "should return a list of providers if view_by is provider", ->



  describe "Rooms", ->
    describe "Colleciton", ->

    describe "Model", ->


  describe "Providers", ->
    describe "Colleciton", ->

    describe "Model", ->


  describe "Clinics", ->
    describe "Colleciton", ->

    describe "Model", ->


  describe "Appointment", ->
    appointment = model = undefined
    beforeEach ->
      appointment = id: null
      model = new HandyworksApp.Models.Appointment(event: appointment)

    describe "workflow", ->
      describe "for staff", ->
        it "should start in new state", ->
          expect(model.get('workflow_state')).toEqual "new"



    describe "method isComplete", ->
      it "should return true if the event has not been saved", ->
        expect(model.isComplete()).toBeFalsy()

      it "should return false if the event has been saved", ->
        model.id = 101
        expect(model.isComplete()).toBeTruthy()


    describe "method cleanup", ->
      it "should trigger an appointment cancel event", ->
        mock = sinon.mock(HandyworksApp.dispatcher)
        mock.expects("trigger").withArgs "appointmentcancel", appointment
        model.cleanup()
        mock.verify()
