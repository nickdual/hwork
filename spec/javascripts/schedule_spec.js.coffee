#= require authorized

describe "Schedule Features", ->
  describe "Event Model", ->
    event = undefined
    event_attr = undefined
    beforeEach ->
      event = new HandyworksApp.Models.Event()
      event_attr =
        created_at: "2011-09-16T16:28:06Z"
        day_of_week: 2
        duration: 60
        eventable_id: 8
        eventable_type: "Schedule"
        free: false
        id: 74
        lag_time: 0
        lead_time: 0
        start_date: "2000-01-07"
        start_time: "2000-01-01T08:15:00Z"
        title: "Schedule Event"
        updated_at: "2011-09-16T16:28:06Z"

    describe "method start()", ->
      it "should be defined", ->
        expect(event.start).toBeDefined()

      it "properly 'parse' the start date", ->
        ev = new HandyworksApp.Models.Event(event_attr)
        expect(ev.start()).toEqual Date.parse("2000-01-07T08:15:00Z")


    describe "method end()", ->
      it "should be defined", ->
        expect(event.end).toBeDefined()



  describe "Events Collection", ->
    events = undefined
    calEvent = undefined
    beforeEach ->
      events = new HandyworksApp.Collections.Events([
        id: 99
        eventable_id: 900
        eventable_type: "MySchedule"
        start_date: new Date()
        start_time: new Date()
        duration: 120
      ],
        eventable_id: 1
        eventable_type: "Test"
      )
      calEvent =
        id: -2
        start: new Date()
        end: new Date().add(hours: 2)
        title: "Test Event"

    describe "method toEvent()", ->
      it "should be defined", ->
        expect(events.toEvent).toBeDefined()

      it "should not include ids < 0", ->
        res = events.toEvent(calEvent)
        expect(res.id).not.toBeDefined()

      it "should include ids > 0", ->
        calEvent.id = 101
        res = events.toEvent(calEvent)
        expect(res.id).toEqual 101

      it "should use the 'calendar' eventable fields by default", ->
        expect(calEvent.eventable_id).not.toBeDefined()
        expect(calEvent.eventable_type).not.toBeDefined()
        res = events.toEvent(calEvent)
        expect(res.eventable_id).toEqual 1
        expect(res.eventable_type).toEqual "Test"

      it "should use eventable fields from calEvent if present", ->
        calEvent.eventable_id = 101
        calEvent.eventable_type = "MyTest"
        res = events.toEvent(calEvent)
        expect(res.eventable_id).toEqual 101
        expect(res.eventable_type).toEqual "MyTest"


    describe "method toCalEvent()", ->
      it "should be defined", ->
        expect(events.toCalEvent).toBeDefined()

      xit "should calculate start time", ->

      it "should include the eventable fields", ->
        e = events.get(99)
        expect(e).toBeDefined()
        res = events.toCalEvent(e)
        expect(res.eventable_id).toBeDefined()
        expect(res.eventable_type).toBeDefined()



