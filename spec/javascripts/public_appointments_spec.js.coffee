#= require public_appointments

describe "HandyworksApp", ->
  describe "Namespace", ->
    it "should be defined", ->
      expect(HandyworksApp).toBeDefined

  describe "Routing", ->
    it "should be defined", ->
      expect(HandyworksApp.Routers.PublicAppointmentsRouter).toBeDefined


  describe ".initialize_public_appointments", ->
    it "should be defined", ->
      expect(HandyworksApp.initialize_public_appointments).toBeDefined

  describe "Models", ->
    describe "PublicAppointment", ->
      pa = undefined
      beforeEach ->
        pa = new HandyworksApp.Models.PublicAppointment()
      describe "workflow", ->
        it "initial state is :new", ->
          expect(pa.get('workflow_state')).toBe 'new'

        it "initial step is :location", ->
          expect(pa.getStep()).toBe 'location'

      describe "fetchTimes", ->
        beforeEach ->
          HandyworksApp.Data.Times = new HandyworksApp.Collections.Times()
        it "updates the collection of times", ->
          fts = sinon.spy(HandyworksApp.Data.Times,"fetchTimes")
          sds = sinon.spy(HandyworksApp.Data.Times,"setData")
          pa.fetchTimes()
          expect(fts).toHaveBeenCalled()
          expect(sds).toHaveBeenCalled()

    describe "Time", ->

  describe "Views", ->
    pa = undefined
    beforeEach ->
      pa = new HandyworksApp.Models.PublicAppointment()
      HandyworksApp.Data.Clinics = new HandyworksApp.Collections.Clinics()
      HandyworksApp.Data.AppointmentTypes = new HandyworksApp.Collections.AppointmentTypes()
      HandyworksApp.Data.Providers = new HandyworksApp.Collections.Providers()
      HandyworksApp.Data.Times = new HandyworksApp.Collections.Times()
    describe "PublicAppointment", ->
      it "should exist", ->
        expect(HandyworksApp.Views.PublicAppointment).toBeDefined()
      it "renders new appointment interface", ->
        view = new HandyworksApp.Views.PublicAppointment({model: pa})
        view.render()
        expect(view.$el).toBe("#pa_main")
        expect(view.$el).toContain(".body")

    describe "PublicAppointmentLocation", ->
      it "should exist", ->
        expect(HandyworksApp.Views.PublicAppointmentLocation).toBeDefined()
      it "renders a list of clinics", ->
        view  = new HandyworksApp.Views.PublicAppointmentLocation()
        view.render()
        expect(view.$el).toBe("#pa_location")
        expect(view.$el).toContain(".locations_list")

    describe "PublicAppointmentService", ->
      it "should exist", ->
        expect(HandyworksApp.Views.PublicAppointmentService).toBeDefined()
      it "renders a list of appointment types", ->
        view = new HandyworksApp.Views.PublicAppointmentService()
        view.render()
        expect(view.$el).toBe("#pa_service")
        expect(view.$el).toContain(".services_list")

    describe "PublicAppointmentProviders", ->
      it "should exist", ->
        expect(HandyworksApp.Views.PublicAppointmentProvider).toBeDefined()
      it "renders a list of providers", ->
        view = new HandyworksApp.Views.PublicAppointmentProvider({model: pa})
        view.render()
        expect(view.$el).toBe("#pa_provider")
        expect(view.$el).toContain(".providers_list")

    describe "PublicAppointmentTime", ->
      it "should exist", ->
        expect(HandyworksApp.Views.PublicAppointmentTime).toBeDefined()
      it "should render a list of times", ->
        view = new HandyworksApp.Views.PublicAppointmentTime({collection: HandyworksApp.Data.Times})
        view.render()
        expect(view.$el).toBe("#pa_time")
        expect(view.$el).toContain(".times_list")

    describe "PublicAppointmentTimeSlot", ->
      ts = undefined
      beforeEach ->
        ts = new HandyworksApp.Models.Time({time: new Date().getTime(), rooms: [1,3]})
      it "should exist", ->
        expect(HandyworksApp.Views.PublicAppointmentTimeSlot).toBeDefined()
      it "should render a time slot", ->
        view = new HandyworksApp.Views.PublicAppointmentTimeSlot({model: ts})
        view.render()
        expect(view.$el).toContain(".time_slot")

    describe "PublicAppointmentConfirm", ->
      it "should exist", ->
        expect(HandyworksApp.Views.PublicAppointmentConfirm).toBeDefined()
      it "should render confirmation of appointment details", ->
        view = new HandyworksApp.Views.PublicAppointmentConfirm()
        view.render()
        expect(view.$el).toBe('#pa_confirm')
        expect(view.$el).toContain('.appointment_details')
