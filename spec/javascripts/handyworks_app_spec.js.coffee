#= require authorized

describe "Handyworks App", ->
  ha = undefined
  beforeEach ->
    ha = HandyworksApp

  describe "application object", ->
    it "should be defined", ->
      expect(ha).toBeDefined
    describe "structure", ->
      it "provides Data", ->
        expect(ha.Data).toBeDefined

  hac = undefined
  describe "Configuration", ->
    beforeEach ->
      hac = ha.Configuration
    it "should exist", ->
      expect(hac).toBeDefined
    # note: would have expected rails_env to be either 'test' or 'development'
    #  however, since its not be explicitly initialized in the JavaScript
    #  testing framework, its falling back on a default production setting.
    it "should have rails_evn", ->
      expect(hac.rails_env).toBeDefined

  describe ".getData()", ->
    item = {}
    beforeEach ->
      HandyworksApp.Data.Item = item
    it "should return elements from HandyworksApp.Data namespace", ->
      expect(HandyworksApp.getData('Item')).toBe(item)
    it "should handle undefined references", ->
      expect(HandyworksApp.getData('What')).not.toBeDefined()

  describe "method init", ->


  # TODO : get the HandyworksApp.init() tests working.
  #  it("initializes the requested router",function() {
  #      loadFixtures('handyworks_app_init.html');
  #      HandyworksApp.Routers.Test = sinon.spy();
  #      HandyworksApp.init("#dummy",HandyworksApp.Routers.Test);
  #      expect(HandyworksApp.Routers.Test).toHaveBeenCalled();
  #  });
  #  it("stores a reference to the router",function() {
  #      loadFixtures('handyworks_app_init.html');
  #      HandyworksApp.Routers.Test = sinon.spy();
  #      HandyworksApp.init("#dummy",HandyworksApp.Routers.Test);
  #      expect(HandyworksApp.router).toBeDefined();
  #  });
  #  it ("stores a reference to the router's container", function() {
  #      loadFixtures('handyworks_app_init.html');
  #      HandyworksApp.Routers.Test = sinon.spy();
  #      HandyworksApp.init("#dummy",HandyworksApp.Routers.Test);
  #      expect(HandyworksApp.el).toBeDefined();
  #  });
  #  it ("starts Backbone.history", function() {
  #      loadFixtures('handyworks_app_init.html');
  #      HandyworksApp.Routers.Test = sinon.spy();
  #      HandyworksApp.init("#dummy",HandyworksApp.Routers.Test);
  #      expect(Backbone.history.start).toHaveBeenCalled();
  #  });
  describe "method getCalendarPopupHeight()", ->
    it "should be defined", ->
      expect(HandyworksApp.getCalendarPopupHeight).toBeDefined()

    it "should return a portion of the window size", ->
      ht = jQuery(window).height()
      expect(HandyworksApp.getCalendarPopupHeight()).toBeLessThan ht


  describe "schedulePopup() method", ->
    it "should be defined", ->
      expect(HandyworksApp.schedulePopup).toBeDefined()


  describe "method indexTable()", ->
    it "should be defined", ->
      expect(HandyworksApp.indexTable).toBeDefined()

    it "should create a dataTables display", ->
      loadFixtures "index_table.html"
      i = HandyworksApp.indexTable($("#index_table"))
      expect($(".dataTables_wrapper")).toExist()


  describe "method getDuration()", ->
    it "should be defined", ->
      expect(HandyworksApp.getDuration).toBeDefined()

    it "should return positive integer for future time", ->
      d1 = new Date()
      d2 = new Date(d1).addMinutes(2)
      console.log "dates", d1, d2
      expect(HandyworksApp.getDuration(d1, d2)).toEqual 2

    it "should round down to next integer value", ->
      d1 = new Date()
      d2 = new Date(d1).addSeconds(200)
      expect(HandyworksApp.getDuration(d1, d2)).toEqual 3

    it "should return negative values if the first date is after the second", ->
      d1 = new Date()
      d2 = new Date(d1).addMinutes(2)
      expect(HandyworksApp.getDuration(d2, d1)).toEqual -2


  describe "method calculate_time()", ->
    start_date = start_time = undefined
    beforeEach ->
      start_date = "2000-01-07"
      start_time = "2000-01-01T08:15:00"

    it "should be defined", ->
      expect(HandyworksApp.calculate_time).toBeDefined()

    it "should calculate expected start times", ->
      expect(HandyworksApp.calculate_time(start_date, start_time)).toEqual Date.parse("2000-01-07T08:15:00")

    it "should calculate expecte end times", ->
      expect(HandyworksApp.calculate_time(start_date, start_time, 60)).toEqual Date.parse("2000-01-07T09:15:00")


  describe "dispatcher", ->
    it "should exist", ->
      expect(HandyworksApp.dispatcher).toBeDefined()

    it "should support event binding via on", ->


    # TODO : write test for dispatcher.on
    it "should support event triggering", ->




# TODO : wrtie test for dispatcher.trigger

# TODO : tests for getAppointmentDetailsWidth

# TODO : test getDisabledAppointmentSteps (a) returns everything > 0 initially (b) returns > AppointmentStep

# TODO : write tests for HandyworksApp.endingAfter

# TODO : write test to verify indexTable method returns a DataTables object (e.g. test fnDraw is defined)

# TODO : write test for method showError

# TODO : write test for switchClass

# TODO : write test for addClassInFront

# TODO : write tests for clearSelection
