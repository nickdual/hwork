#= require authorized

describe "Appointment Features", ->
  describe "Calendar Navigation", ->
    beforeEach ->


    # setup the appointment view
    describe "selecting the number of days", ->
      it "should default to 5 days", ->


      # verify the calendar model has days: 5
      it "should update the calendar days to show", ->



    # click one of the tabs (weekly, 3 days, 1 days)
    # verify the weekCalendar days to show changed appropriately
    describe "selecting the date", ->
      it "should default to today's date", ->


      # verify the calendar model has today's date
      # verify the weekCalendar date is today
      it "should update the calendar date", ->



    # click one of the days in the datePicker
    # verify the weekCalendar is selected.
    describe "filtering by providers", ->
      it "should default to all providers enabled", ->



    # create a new provider, verify its enabled
    describe "filtering by rooms", ->
      it "should default to all rooms enabled", ->



    # create a new room, verify its enabled.
    describe "view the calendar by room or provider", ->
      it "should default to view by room", ->





# create a new Calendar model
# verify the default is view_by "room"
