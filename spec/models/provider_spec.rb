require 'spec_helper'

describe Provider do
  before(:each) do
    @provider = FactoryGirl.create(:provider)
  end
  it { should have_one(:contact) }
  it { should have_one(:schedule) }
  it { should belong_to(:account) }
  #BLS - the test below should be passing, but it may be a bug within RSpec / Shoulda or ActiveRecord
  #        ** If its ActiveRecord, then we'll have to change the join model name
  #  Failure/Error: it { should have_many(:provider_legacy_id_labels) }
  #    NameError:
  #            uninitialized constant Provider::ProviderLegacyIdLabel
  #BLS it { should have_many(:provider_legacy_id_labels) }
  it { should have_many(:provider_legacy_id_labels) }
  it { should have_many(:legacy_id_labels) }
  it { should have_many(:appointments) }
  it { should have_many(:appointment_types) }
  #it { should have_many(:patient_cases) }
  #it { should have_many(:patient_bills) }
  #it { should have_many(:patient_visit_details) }
  it { should validate_presence_of(:signature_name) }
  it { should_not allow_value("12345678901").for(:npi_uid) }

  [ '123-456-789', '12345', '123-45-678a' ].each do |bad_uid|
    it { should_not allow_value(bad_uid).for(:tax_uid) }
  end

  [ '123-45-7890', '12-3456789' ].each do |good_uid|
    it { should allow_value(good_uid).for(:tax_uid) }
  end

  it "should create a new instance given valid attributes" do
    FactoryGirl.create(:provider, :signature_name => "Test Test M.D.")
  end

  it 'should be valid with a full set of valid attributes' do
    @provider.should be_valid
  end

  it 'should have a default appointment color' do
    p = Provider.new
    p.appointment_color.should eql("#68a1e5")
  end

  it "should validate uniqueness of account / signature_name" do
    pending "TODO : write tests validating signature name is unique, but only within one account"
  end
end
