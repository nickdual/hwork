require 'spec_helper'
require 'enum_support'

describe EnumSupport do
  
  class EnumSupportTest 
    include ActiveModel::Validations
    include ActiveModel::Conversion
    include ActiveModel::Naming
    include ActiveModel::AttributeMethods
    extend EnumSupport
    attr_accessor :name, :age, :day, :month
    enum :name, %w{tom mary wendy jim}
    enum_map :month, { 'jan' => 1, 'feb' => 2, 'mar' => 3, 'apr' => 4 }
    enum_str :day, ['mon','tue','wed']
    enum_i :age, [ 1, 5, 10, 25, 100 ]
  end

  describe "basic" do
    it "should include value array name of symbols" do
      EnumSupportTest::NAMES.should be_kind_of(Array)
      EnumSupportTest::NAMES[0].should be_kind_of(Symbol)
    end

    it "should validate the presence of name" do
      pending "need to figure out a better ActiveRecord style approach to testing or implementing"
      e = EnumSupportTest.new
      e.should_not be_valid
      e.errors[:name].should contain('is not in the set')
    end
  end

  describe "map" do
    it "should include the hash of values" do
      EnumSupportTest::MONTHS.should be_kind_of(Hash)
      EnumSupportTest::MONTHS['jan'].should be_kind_of(Fixnum)
    end
  end

  describe "string" do
    it "should include the array of values as strings" do
      EnumSupportTest::DAYS.should be_kind_of(Array)
      EnumSupportTest::DAYS[0].should be_kind_of(String)
    end
  end

  describe "integer" do
    it "should include the array of values as integers" do
      EnumSupportTest::AGES.should be_kind_of(Array)
      EnumSupportTest::AGES[0].should be_kind_of(Fixnum)
    end
  end

end
