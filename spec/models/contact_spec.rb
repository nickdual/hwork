require 'spec_helper'

describe Contact do
  before(:each) do
    @contact = FactoryGirl.create(:contact)
  end

  it { should belong_to(:contactable) }
  it { should have_many(:appointments) }

  it "should create a new instance given valid attributes" do
    FactoryGirl.create(:contact)
  end

  it "should be valid with a full set of valid attributes" do
    @contact.should be_valid
  end

  describe "scopes" do
    describe "named_like" do
      it "should return contact matching the indicated name" do
        # TODO : write tests for the contact.named_like scope
        pending "write tests for the contact.named_like scope"
      end
    end
  end

  describe "method appointment_notes" do
    it "should have tests written" do
      pending "contact.appointment_notes should have tests written" 
      #TODO : contact.appointment_notes should have tests written
    end
  end

  describe "method different_location?" do
    before(:each) do
      @c1 = FactoryGirl.create(:contact)
      @c1.addresses<< FactoryGirl.create(:address, :label => 'Billing', :street => '115 Bally Hoo')
      @c1.addresses<< FactoryGirl.create(:address, :label => 'Physical', :street => '100 Bally Hoo')
      @c1.save
    end

    it "should use physical address if available" do
      c2 = FactoryGirl.create(:contact)
      c2.addresses<< FactoryGirl.create(:address, :label => 'Billing', :street => '119 Bally Hoo')
      c2.addresses<< FactoryGirl.create(:address, :label => 'Physical', :street => '100 Bally Hoo')
      c2.different_location?(@c1).should be_false
    end

    it "should use first address if physical address is not available" do
      c2 = FactoryGirl.create(:contact)
      c2.addresses<< FactoryGirl.create(:address, :label => 'AddressB', :street => '100 Bally Hoo')
      c2.addresses<< FactoryGirl.create(:address, :label => 'AddressA', :street => '118 Bally Hoo')
      c2.different_location?(@c1).should be_false
    end

    it "should be true for different locations" do
      c2 = FactoryGirl.create(:contact)
      c2.addresses<< FactoryGirl.create(:address, :label => 'Physical', :street => '100 Sally Hoo')
      c2.different_location?(@c1).should be_true
    end
  end

  #-----------------------------------------------------------------------------
  # ContactThirdParty
  #-----------------------------------------------------------------------------
  describe ContactThirdParty do
    before(:each) do
      @contact_attorney = FactoryGirl.create(:contact_third_party)
    end

    it "should basically be valid" do
      @contact_attorney.should be_valid
    end

    #it { should validate_presence_of(:company_name) }
    it "should not work" do
      @contact_attorney.company_name = nil
      @contact_attorney.save
      @contact_attorney.should_not be_valid
    end

    it "should have a fax number" do
      @contact_attorney.should respond_to(:fax_phone)
    end

    it "should not validate business phone of '() -' [an empty phone]" do
      pending "write more tests on business_phone.number= method"
    end
  end

  #-----------------------------------------------------------------------------
  # ContactProvider
  #-----------------------------------------------------------------------------
  describe ContactProvider do
    before(:each) do
      @contact_provider = FactoryGirl.create(:contact_provider)
    end

    it "should be valid" do
      @contact_provider.should be_valid
    end

    it "should have street" do
      @contact_provider.should respond_to(:street)
    end

    it "should have street2" do
      @contact_provider.should respond_to(:street2)
    end

    it "should have zip" do
      @contact_provider.should respond_to(:zip)
    end

    it "should have city" do
      @contact_provider.should respond_to(:city)
    end

    it "should have state" do
      @contact_provider.should respond_to(:state)
    end

    it "should have physical_address_one_liner" do
      @contact_provider.should respond_to(:physical_address_one_liner)
    end

    describe "naming conventions" do
      it "should have a name" do
        @contact_provider.should respond_to(:name)
      end

      it "should match the first_name and last_name" do
        @contact_provider.name.should == @contact_provider.first_name + ' ' + @contact_provider.last_name
      end
    end

    describe "phone attributes" do
      it "should have a business phone" do
        @contact_provider.should respond_to(:business_phone)
      end

      it "should not require the business phone" do
        @contact_provider.business_phone = nil
        @contact_provider.should be_valid
      end

      it "validate the phone if present" do
        @contact_provider.business_phone.number = '1234'
        @contact_provider.should_not be_valid
      end

    end

    describe "email attributes" do
      it "should have an email" do
        @contact_provider.should respond_to(:email)
      end

      it "should not require an email" do
        @contact_provider.business_email = nil
        @contact_provider.should be_valid
      end

      it "should validate email if present" do
        @contact_provider.business_email.email = 'bob at world.com'
        @contact_provider.should_not be_valid
      end
    end

  end

  #-----------------------------------------------------------------------------
  # ContactThirdParty
  #-----------------------------------------------------------------------------
  describe "ContactThirdParty" do
    it "should not required an email" do
      cic = FactoryGirl.create(:contact_third_party)
      cic.should be_valid
      cic.business_email = nil
      cic.should be_valid
    end
  end
end
