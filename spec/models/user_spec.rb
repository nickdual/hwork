require 'spec_helper'

describe User do

  before(:each) do
    @attr = { 
      :login => "Example User",
      :email => "user@example.com",
      :password => "foobar",
      :password_confirmation => "foobar",
    }
  end

  describe "validations" do
    it { should belong_to(:account) }
    it { should belong_to(:clinic) }
    it { should have_and_belong_to_many(:roles) }
    it { should respond_to(:default_clinic) }
  end
  
  it "should create a new instance given a valid attribute" do
    User.create!(@attr)
  end
  
  it "should require an email address" do
    no_email_user = User.new(@attr.merge(:email => ""))
    no_email_user.should_not be_valid
  end
  
  it "should accept valid email addresses" do
    addresses = %w[user@foo.com THE_USER@foo.bar.org first.last@foo.jp]
    addresses.each do |address|
      valid_email_user = User.new(@attr.merge(:email => address))
      valid_email_user.should be_valid
    end
  end
  
  it "should reject invalid email addresses" do
    addresses = %w[user@foo,com user_at_foo.org example.user@foo.]
    addresses.each do |address|
      invalid_email_user = User.new(@attr.merge(:email => address))
      invalid_email_user.should_not be_valid
    end
  end
  
  it "should reject duplicate email addresses" do
    User.create!(@attr)
    user_with_duplicate_email = User.new(@attr)
    user_with_duplicate_email.should_not be_valid
  end
  
  it "should reject email addresses identical up to case" do
    upcased_email = @attr[:email].upcase
    User.create!(@attr.merge(:email => upcased_email))
    user_with_duplicate_email = User.new(@attr)
    user_with_duplicate_email.should_not be_valid
  end
  
  describe "passwords" do

    before(:each) do
      @user = User.new(@attr)
    end

    it "should have a password attribute" do
      @user.should respond_to(:password)
    end

    it "should have a password confirmation attribute" do
      @user.should respond_to(:password_confirmation)
    end
  end
  
  describe "password validations" do

    it "should require a password" do
      u = User.new(@attr.merge(:password => "", :password_confirmation => ""))
      u.confirm!
      u.should_not be_valid
    end

    it "should require a matching password confirmation" do
      u = User.new(@attr.merge(:password_confirmation => "invalid"))
      u.confirm!
      u.should_not be_valid
    end
    
    it "should reject short passwords" do
      short = "a" * 5
      hash = @attr.merge(:password => short, :password_confirmation => short)
      u = User.new(hash)
      u.confirm!
      u.should_not be_valid
    end
    
  end
  
  describe "password encryption" do
    
    before(:each) do
      @user = FactoryGirl.create(:confirmed_user)
    end
    
    it "should have an encrypted password attribute" do
      @user.should respond_to(:encrypted_password)
    end

    it "should set the encrypted password attribute" do
      @user.encrypted_password.should_not be_blank
    end

  end

  describe "expire" do

    before(:each) do
      @user = User.create!(@attr)
    end

    it "sends an email to user" do
      @user.expire
      ActionMailer::Base.deliveries.last.to.should == [@user.email]
    end

  end

  describe "has roles" do

    it "should have a roles association" do
      user = User.new
      user.should respond_to(:roles)
    end

    it "should check for a role assignment" do
      user = User.new
      user.should respond_to(:role?)
    end

  end

  describe "method as_json" do
    before(:each) do
      @model = FactoryGirl.create(:user)
    end
    it { User::JSON_OPTS.should be_a_kind_of(Hash) }
    it { should respond_to(:as_json) }
    #===========================================================================
    # NOTE: BLS, overriding does not behave the way you'd like it to
    #       as the options are going to get passed down the association
    #       tree, and thus misinterpreted.
    #===========================================================================
    it "should accept option arguments to override the JSON_OPTS" do
      r = @model.as_json(:only => [:created_at])
      r["created_at"].should_not be_nil
      r["id"].should be_nil
    end
  end

end
