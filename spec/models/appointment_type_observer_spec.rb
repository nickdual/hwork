require 'spec_helper'

describe AppointmentTypeObserver do
  describe "to provider" do
    before(:each) do
      @provider = FactoryGirl.create(:provider)
    end
    it "should link to singular providers" do
      appointment_type = FactoryGirl.create(:appointment_type, :account => @provider.account)
      appointment_type.providers.should include(@provider)
    end
    it "should not link if multiple providers" do
      provider2 = FactoryGirl.create(:provider, :account => @provider.account)
      appointment_type = FactoryGirl.create(:appointment_type, :account => @provider.account)
      appointment_type.providers.should be_empty
    end
  end
  describe "to room" do
    before(:each) do
      @room = FactoryGirl.create(:room)
    end
    it "should link if single room" do
      appointment_type = FactoryGirl.create(:appointment_type, :account => @room.account)
      appointment_type.rooms.should include(@room)
    end
    it "should not link if multiple rooms" do
      room2 = FactoryGirl.create(:room, :account => @room.account)
      appointment_type = FactoryGirl.create(:appointment_type, :account => @room.account)
      appointment_type.rooms.should be_empty
    end
  end
end
