require 'spec_helper'

describe Role do
  before(:each) do
    @user = FactoryGirl.create(:user)
    @role = FactoryGirl.create(:role)
    @role.assign(@user)
  end

  describe "attributes" do
    it "should have users" do
      @role.should have_and_belong_to_many(:users)
    end
  end

  describe "lookup" do
    it "should lookup by name" do
      role = Role.find_by_name('ApplicationAdmin')
      role.should_not be_nil
    end

    it "should not find nonexistant roles" do
      role = Role.find_by_name('FakeRole')
      role.should be_nil
    end
  end

  describe "assignment" do
    it "should assign to user" do
      @user.role?(@role).should be_true
    end
  end

  describe "revoke" do
    it "should remove a role from a user that has it" do
      @user.role?(@role).should be_true
      @role.revoke(@user)
      @user.role?(@role).should be_false
    end

    it "should silently do nothing if the user does not have the role" do
      user2 = FactoryGirl.create(:user)
      @role.revoke(user2)
    end

  end

  describe "query" do
    it "should check for assigned roles" do
      Role.has_role(@user,@role.name).should be_true
    end

    it "should check for non-assigned roles" do
      role2 = FactoryGirl.create(:role, :name => 'NewRole')
      Role.has_role(@user,role2.name).should be_false
    end

    it "should allow check by role object" do
      Role.has_role(@user,@role).should be_true
    end
  end

  describe "#valid_resource?" do
    it "should be true for models" do
      Role.new(name: 'a_role', resource_type: 'User').valid_resource?.should be_true
    end
    it "should be false for junk" do
      Role.new(name: 'a_role', resource_type: 'Loser').valid_resource?.should_not be_true
    end
  end

end
