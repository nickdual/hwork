require 'spec_helper'

describe NewPatientOption do
  before(:each) do
    @default_provider = FactoryGirl.create(:default_provider)
    @clinic = FactoryGirl.create(:clinic)
    @new_patient_option = FactoryGirl.create(:new_patient_option,:clinic => @clinic, :default_provider => @default_provider)
  end

  it { should belong_to(:clinic) } 
  it { should belong_to(:default_provider) } 

  it "should create a new instance given valid attributes" do
    FactoryGirl.create(:new_patient_option ,:clinic => @clinic)
  end 

  it "should be valid with a full set of valid attributes" do 
    @new_patient_option.should be_valid 
  end 

  it "Bug #97 should accept floating point value for overdue_fee_percentage" do
    npo = FactoryGirl.create(:new_patient_option, :overdue_fee_percentage => 13.35,:clinic => @clinic)
    npo.should be_valid
    npo.overdue_fee_percentage.should eql(13.35)
  end
end
