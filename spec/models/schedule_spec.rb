require 'spec_helper'

describe Schedule do
  it { should belong_to(:schedulable) }
  it { should have_many(:events) }
  # TODO : validates presence of scheduable did seem like a good idea, but proved difficult to unit test.
  #it { should validate_presence_of(:schedulable) }
  it { should respond_to(:account_id) }
  it { should respond_to(:max_concurrent_appointments) }
  it { should respond_to(:max_appointments_per_hour) }

  [:max_concurrent_appointments, :max_appointments_per_hour].each do |field|
    describe "integer values for #{field} > 0" do
      it { should validate_presence_of(field) }
      it { should validate_numericality_of(field) }
      it { should allow_value(5).for(field) }
      it { should_not allow_value(10.1).for(field) }
      it { should_not allow_value(-1).for(field) }
    end
  end
  describe "method as_json" do
    before(:each) do
      @model = FactoryGirl.create(:schedule)
    end
    it { Schedule::JSON_OPTS.should be_a_kind_of(Hash) }
    it { should respond_to(:as_json) }
    #===========================================================================
    # NOTE: BLS, overriding does not behave the way you'd like it to
    #       as the options are going to get passed down the association
    #       tree, and thus misinterpreted.
    #===========================================================================
    it "should accept option arguments to override the JSON_OPTS" do
      r = @model.as_json(:only => [:created_at])
      r["created_at"].should_not be_nil
      r["id"].should be_nil
    end
  end

  describe "method get_schedule_for_week" do
    before(:each) do
      @monday = Date.today()
      @monday = @monday - @monday.wday + 1
      @schedule = FactoryGirl.create(:schedule)
      @time_available = FactoryGirl.create(:event, :eventable => @schedule, :start_date => Date.parse('2000-01-03'), :title => 'Schedule Event')
      @exception = FactoryGirl.create(:event, :eventable => @schedule, :start_date => @monday + 2, :title => 'Vacation')
    end
    it "should return array of events" do
      r = @schedule.get_schedule_for_week(@monday)
      r.should be_kind_of(Array)
      r.first.should be_kind_of(Event)
    end
    it "should always include the base events" do
      r = @schedule.get_schedule_for_week(@monday - 365)
      #r.size.should eql(1)
      r.first.title.should eql('Schedule Event')
    end
    it "should include exceptions where they apply" do
      r = @schedule.get_schedule_for_week(@monday)
      #r.size.should eql(2)
      r.last.title.should eql('Vacation')
    end
    it "should map the schedule to events with current start dates" do
      r = @schedule.get_schedule_for_week(@monday - 7)
      r.first.start_date.should eql(@monday - 7)  # 2000-01-03 was a monday
    end
    it "should mark the schedule events as free" do
      r = @schedule.get_schedule_for_week(@monday)
      #r.size.should eql(2)
      r.each do |ri|
        ri.free.should be_false if ri.title == 'Vacation'
        ri.free.should be_true if ri.title == 'Schedule Event'
      end
    end
  end
end
