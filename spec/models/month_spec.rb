require 'spec_helper'

describe Month do
  it "should have hash of names to numbers" do
    Month::MONTHS.should be_kind_of(Hash)
  end

  it "should offer an array of names" do
    Month.names.should be_kind_of(Array)
    Month.names.should include('December')
  end

  it "should offer the array of numbers" do
    Month.values.should be_kind_of(Array)
    Month.values.should include('01')
  end

  it "should convert from name to number" do
    Month.to_number('April').should eql('04')
  end

  it "should convert from number to name" do
    Month.to_name('05').should eql('May')
  end
end
