require 'spec_helper'

describe Calendar do
  before(:each) do
    @model = FactoryGirl.create(:calendar)
  end
  it { should belong_to(:account) }
  it { should validate_presence_of(:account) }
  it { should have_many(:appointments) }
  it { should respond_to(:schedule_future_max_days) }



  describe "allowed values for days" do
    it { should validate_numericality_of(:days) }
    [1,2,3,4,5,6,7].each do |days| 
      it { should allow_value(days).for(:days) }
    end
    it { should_not allow_value(3.5).for(:days) }
    it { should_not allow_value(-1).for(:days) }
    it { should_not allow_value(12).for(:days) }
    it "should default to 1" do
      Calendar.new().days.should eql(1)
    end
  end

  describe "method list_weeks" do
    it "should return empty array for nil dates" do
      @model.list_weeks(nil,nil).should be_empty
    end
    it "should return no weeks for matching Monday dates" do
      @monday = Date.today - Date.today.wday + 1
      r = @model.list_weeks(@monday,@monday)
      r.size.should eql(0)
    end
    [0,2,3,4,5,6].each do |day|
      it "should return 1 weeks for matching day-#{day} dates" do
        @day = Date.today - Date.today.wday + day
        r = @model.list_weeks(@day,@day)
        r.size.should eql(1)
      end
    end
    it "should return empty array if dates are crossed up" do
      @model.list_weeks(Date.today + 1, Date.today).should be_empty
    end
    it "should return 1 weeks for 7-day span on Monday" do
      @monday = Date.today - Date.today.wday + 1
      r = @model.list_weeks(@monday, @monday + 7)
      r.size.should eql(1)
      r[0].should eql(@monday)
    end
    [0,2,3,4,5,6].each do |day|
      it "should return 2 weeks for 7-day span on day-#{day}" do
        @day = Date.today - Date.today.wday + day
        r = @model.list_weeks(@day, @day + 7)
        r.size.should eql(2)
        unless @day.wday == 0
          r[0].should eql(@day - @day.wday + 1) # Monday before @day
        else
          r[0].should eql(@day - 7 + 1) # Monday before @day previous
        end
      end
    end
  end
  describe "date range parameters" do
    it { should respond_to(:event_start_date) }
    it { should respond_to(:event_end_date) }
    it "should span 4 weeks interval by default" do
      @model.event_end_date - @model.event_start_date = 28
    end
    it "should not exceed max_future_schedule_days" do
      pending "need to add this validation"
    end
    it "should not preceed schedule history limit" do
      # TODO : implement calendar date limit boundary validations
      pending "need to add a preference and hard limit, with validation logic"
    end
  end



  describe "method as_json" do
    it { should respond_to(:as_json) }
    #===========================================================================
    # NOTE: BLS, overriding does not behave the way you'd like it to
    #       as the options are going to get passed down the association
    #       tree, and thus misinterpreted.
    #===========================================================================
    it "should accept option arguments to override the JSON_OPTS" do
      r = @model.as_json(:only => [:created_at])
      r["created_at"].should_not be_nil
      r["id"].should be_nil
    end
    it "should include event ids in the calendar json data" do
      pending "need to have a schedule for clinics with events"
    end
  end

  describe "method provider_schedule_location_required?" do
    before(:each) do
      @account = FactoryGirl.create(:account)
    end
    it "should return false if there is only 1 clinic" do
      clinic = double("Clinic")
      @account.stub(:clinics) { [ clinic ] }
      @account.calendar.provider_schedule_location_required?.should be_false
    end

    it "should return false if there are no clinics" do
      @account.stub(:clinics) { [] }
      @account.calendar.provider_schedule_location_required?.should be_false
    end

    it "should return true if there are multiple clinics with different_locations" do
      clinic = double("Clinic")
      clinic.stub(:different_location?) { true }
      @account.stub(:clinics) { [clinic, clinic] }
      @account.calendar.account = @account  # FIXME : this should be taken care of by the factory which creates the account / calendar
      @account.calendar.provider_schedule_location_required?.should be_true
    end

    it "should return false with multiple clinics at the same location" do
      clinic = double("Clinic")
      clinic.stub(:different_location?) { false }
      @account.stub(:clinics) { [clinic, clinic] }
      @account.calendar.provider_schedule_location_required?.should be_false
    end

    it "should return true if the preference setting rooms_by_clinic is true" do
      clinic = double("Clinic")
      clinic.stub(:different_location?) { false }
      @account.stub(:clinics) { [clinic, clinic] }
      @account.calendar.account = @account
      @account.calendar.provider_schedule_by_clinic = true # this did not work as a stub
      @account.calendar.provider_schedule_location_required?.should be_true
    end
  end

  # TODO : add test methods for provider_schedule_by_clinic attribute
end
