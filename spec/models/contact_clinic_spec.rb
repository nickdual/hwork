require 'spec_helper'

#-----------------------------------------------------------------------------
# ContactClinic
#-----------------------------------------------------------------------------
describe ContactClinic do
  before(:each) do
    @contact_clinic = FactoryGirl.create(:contact_clinic)
  end

  it "should be valid" do
    Rails.logger.debug("errors: #{@contact_clinic.errors.inspect}") unless @contact_clinic.valid?
    @contact_clinic.should be_valid
  end

  it "should require company_name" do
    @contact_clinic.company_name = nil
    @contact_clinic.should_not be_valid
  end

  it "should require state" do
    @contact_clinic.state = nil
    @contact_clinic.should_not be_valid
  end

  it "should have street" do
    @contact_clinic.should respond_to(:street)
  end

  it "should have street2" do
    @contact_clinic.should respond_to(:street2)
  end

  it "should have zip" do
    @contact_clinic.should respond_to(:zip)
  end

  it "should have city" do
    @contact_clinic.should respond_to(:city)
  end

  it "should have state" do
    @contact_clinic.should respond_to(:state)
  end

  it "should have physical_address_one_liner" do
    @contact_clinic.should respond_to(:physical_address_one_liner)
  end

  describe "naming conventions" do
    it "should have a name" do
      @contact_clinic.should respond_to(:name)
    end

    it "should match the company_name field" do
      @contact_clinic.name.should == @contact_clinic.company_name
    end
  end

  describe "physical address attributes" do

    it "should have a physical address" do
      @contact_clinic.physical_address.should_not be_nil
    end

  end

  describe "billing address attributes" do

    it "should have a billing address" do
      @contact_clinic.should respond_to(:billing_address)
    end

    it "should not require a billing address" do
      @contact_clinic.billing_address = nil
      @contact_clinic.should be_valid
    end

  end

  describe "phone attributes" do
    it "should have a business phone" do
      @contact_clinic.should respond_to(:business_phone)
    end

    it "should NOT require the business phone" do
      @contact_clinic.business_phone = nil
      @contact_clinic.should be_valid
    end

    it "should have fax phone" do
      @contact_clinic.should respond_to(:fax_phone)
    end

    it "should not require a fax phone" do
      @contact_clinic.fax_phone  = nil
      @contact_clinic.should be_valid
    end

    it "should have an email" do
      @contact_clinic.should respond_to(:email)
    end

    it "should not require an email" do
      @contact_clinic.business_email = nil
      @contact_clinic.should be_valid
    end

  end

end

