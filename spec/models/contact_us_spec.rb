require 'spec_helper'

describe ContactUs do

  before(:each) do
  end

  describe "validations" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:comment) }
    it { should allow_value("one@two.com").for(:email) }
    it { should_not allow_value("bad").for(:email) }
    it { should allow_mass_assignment_of(:name) }
    it { should allow_mass_assignment_of(:email) }
    it { should allow_mass_assignment_of(:comment) }
  end

end
