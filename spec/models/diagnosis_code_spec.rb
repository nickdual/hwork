require 'spec_helper'

describe DiagnosisCode do
  before(:each) do
    @diagnosis_code = FactoryGirl.create(:diagnosis_code)
  end
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:description) }
  it { should ensure_length_of(:name).is_at_most(15) }
  it { should belong_to(:account) }

  it "should create a new instance given valid attributes" do
    FactoryGirl.create(:diagnosis_code)
  end

  it 'should be valid with a full set of valid attributes' do
    @diagnosis_code.should be_valid
  end

  describe 'requires either code or icd10' do
    it 'should be valid with just code' do
      @diagnosis_code.icd10 = nil
      @diagnosis_code.code = '12345'
      @diagnosis_code.should be_valid
    end

    it 'should be valid with just icd10' do
      @diagnosis_code.icd10 = '12345'
      @diagnosis_code.code = nil
      @diagnosis_code.should be_valid
    end

    it 'should not be valid with both codes missing' do
      @diagnosis_code.icd10 = nil
      @diagnosis_code.code = nil
      @diagnosis_code.should_not be_valid
    end
  end

  describe "Bug #151 : validates uniqueness" do
    before(:each) do
      @account = FactoryGirl.create(:account)
      @account2 = FactoryGirl.create(:account)
      @dc1 = FactoryGirl.create(:diagnosis_code, :code => '1234', :name => 'dc1', :account => @account, :description => 'dc1 is a code')
    end

    describe "of code" do
      it "should be required within the same account" do
        pending "need to see if uniqueness constraints get re-enabled"
        dc2 = FactoryGirl.build(:diagnosis_code, :code => '1234', :account => @account)
        dc2.should_not be_valid
        dc2.errors[:code].should include('has already been taken')
      end

      it "should allow same code in different accounts" do
        dc3 = FactoryGirl.build(:diagnosis_code, :code => '1234', :account => @account2)
        dc3.should be_valid
      end
    end

    describe "of name" do
      it "should be required within the same account" do
        dc2 = FactoryGirl.build(:diagnosis_code, :name => 'dc1', :account => @account)
        dc2.should_not be_valid
        dc2.errors[:name].should include('has already been taken')
      end

      it "should be allowed in different accounts" do
        dc3 = FactoryGirl.build(:diagnosis_code, :name => 'dc1', :account => @account2)
        dc3.should be_valid
      end

      it "should not be case sensitive" do
        pending "need to see if uniqueness constraints get re-enabled"
        dc2 = FactoryGirl.build(:diagnosis_code, :name => 'DC1', :account => @account)
        dc2.should_not be_valid
        dc2.errors[:name].should include('has already been taken')
      end
    end

    describe "of description" do
      it "should be required within the same account" do
        pending "need to see if uniqueness constraints get re-enabled"
        dc2 = FactoryGirl.build(:diagnosis_code, :description => 'dc1 is a code', :account => @account)
        dc2.should_not be_valid
        dc2.errors[:description].should include('has already been taken')
      end

      it "should be allowed in different accounts" do
        dc3 = FactoryGirl.build(:diagnosis_code, :description => 'dc1 is a code', :account => @account2)
        dc3.should be_valid
      end

      it "should not be case sensitive" do
        pending "need to see if uniqueness constraints get re-enabled"
        dc2 = FactoryGirl.build(:diagnosis_code, :description => 'DC1 is a code', :account => @account)
        dc2.should_not be_valid
        dc2.errors[:description].should include('has already been taken')
      end
    end
  end

end
