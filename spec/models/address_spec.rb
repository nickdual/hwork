require 'spec_helper'

describe Address do
  before(:each) do
    @address = FactoryGirl.create(:address)
  end

  describe "validations" do

    it { should belong_to(:addressable) }
    it { should_not allow_value('123456').for(:zip) }
    it { should_not allow_value('1234b').for(:zip) }
    it { should allow_value('12345').for(:zip) }
    it { should allow_value('12345-6789').for(:zip) }

  end

  describe "happy addresses" do
    it "should create a new instance given valid attributes" do
      FactoryGirl.create(:address)
    end

    it 'should be valid with a full set of valid attributes' do
      @address.should be_valid
    end
  end

  describe "method different_location?" do
    it "should match identicals" do
      a = FactoryGirl.create(:address)
      b = a.clone
      a.different_location?(b).should be_false
    end

    it "should match with different case" do
      a = FactoryGirl.create(:address, :street => "100 west Way")
      b = FactoryGirl.create(:address, :street => "100 WEST Way")
      a.different_location?(b).should be_false
    end

    it "should recognize difference in street" do
      a = FactoryGirl.create(:address, :street => "105 West Way")
      b = FactoryGirl.create(:address, :street => "103 West Way")
      a.different_location?(b).should be_true
    end
  end
end
