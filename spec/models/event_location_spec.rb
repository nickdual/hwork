require 'spec_helper'

describe EventLocation do
  it { should belong_to(:event) }
  it { should belong_to(:clinic) }
  it { should validate_presence_of(:event) }
  it { should validate_presence_of(:clinic) }
end
