require 'spec_helper'

describe Appointment do
  it { should belong_to(:calendar) }
  it { should belong_to(:provider) }
  it { should belong_to(:contact) }
  it { should belong_to(:room) }
  it { should have_one(:event) }
  it { should validate_presence_of(:calendar) }
  #it { should validate_presence_of(:contact) }
  it { should validate_presence_of(:event) }

  describe "valid appointments" do
    before(:each) do
      @appt = FactoryGirl.create(:appointment)
    end

    it "should be valid with required attributes" do
      @appt.should be_valid
    end
  end

  describe "scopes" do
    before(:each) do
      @account = FactoryGirl.create(:account)
      @e1 = FactoryGirl.build(:event, :start_time => Time.now + 3600)
      @a1 = FactoryGirl.create(:appointment, :event => @e1, :calendar => @account.calendar)
      @e2 = FactoryGirl.build(:event, :start_time => Time.now + 3900)
      @a2 = FactoryGirl.create(:appointment, :event => @e2, :calendar => @account.calendar)
    end

    it "should sort appointments by event start_date" do
      app = Appointment.by_date
      app.length.should eql(2)
      app[0].id.should eql(@a1.id)
      app[1].id.should eql(@a2.id)
    end
  end

  describe "method as_json" do
    before(:each) do
      @model = FactoryGirl.create(:appointment)
    end
    it { Appointment::JSON_OPTS.should be_a_kind_of(Hash) }
    it { should respond_to(:as_json) }
    #===========================================================================
    # NOTE: BLS, overriding does not behave the way you'd like it to
    #       as the options are going to get passed down the association
    #       tree, and thus misinterpreted.
    #===========================================================================
    it "should accept option arguments to override the JSON_OPTS" do
      r = @model.as_json(:only => [:created_at])
      r["created_at"].should_not be_nil
      r["id"].should be_nil
    end
    it "should include a contact_id" do
      @model.contact = FactoryGirl.create(:contact)
      @model.as_json({})["contact_id"].should_not be_nil
    end

  end
end
