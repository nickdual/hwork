require 'spec_helper'

describe Event do
  it { should belong_to(:eventable) }
  it { should have_one(:recurrence) }
  it { should_not validate_presence_of(:recurrence) }
  it { should respond_to(:title) }
  it { should respond_to(:notes) }
  it { should have_many(:event_locations) }
  it { should have_many(:clinics) }

  [:eventable, :start_time, :duration, :day_of_week, :lead_time, :lag_time].each do |field|
    it { should validate_presence_of(field) }
  end

  [:duration, :day_of_week, :lead_time, :lag_time].each do |field|
    describe "integer values for #{field}" do
      it { should validate_numericality_of(field) }
      it { should allow_value(5).for(field) }
      it { should_not allow_value(10.1).for(field) }
    end
  end

  (0..6).to_a.each do |v|
    it { should allow_value(v).for(:day_of_week) }
  end

  [-10,-1,0].to_a.each do |v|
    it { should_not allow_value(v).for(:duration) }
  end

  describe "default values" do
    before(:each) do
      @ev = Event.new
    end

    it "should use 0 for lead_time" do
      @ev.lead_time.should eql(0)
    end

    it "should use 0 for lag_time" do
      @ev.lag_time.should eql(0)
    end

    it "should use false for free" do
      @ev.free.should be_false
    end

    it "should use 60 for duration" do
      @ev.duration.should eql(60)
    end
  end
  describe "method as_json" do
    before(:each) do
      @schedule = FactoryGirl.create(:schedule)
      @model = FactoryGirl.create(:event, :eventable => @schedule)
    end
    it { Event::JSON_OPTS.should be_a_kind_of(Hash) }
    it { should respond_to(:as_json) }
    #===========================================================================
    # NOTE: BLS, overriding does not behave the way you'd like it to
    #       as the options are going to get passed down the association
    #       tree, and thus misinterpreted.
    #===========================================================================
    it "should accept option arguments to override the JSON_OPTS" do
      r = @model.as_json(:only => [:created_at])
      r["created_at"].should_not be_nil
      r["id"].should be_nil
    end
  end

  describe "scopes" do
    describe "method by_date_range" do
      before(:each) do
        @sunday = Date.today()
        @sunday = @sunday - @sunday.wday
        @e1 = FactoryGirl.create(:event, :start_date => @sunday - 3)
        @e2 = FactoryGirl.create(:event, :start_date => @sunday + 3, :start_time => 130000)
      end

      it "should handle empty data sets" do
        r = Event.by_date_range(@sunday - 1000,@sunday - 999)
        r.should be_empty
      end

      it "should include events on the end date" do
        r = Event.by_date_range(@sunday,@sunday + 3)
        r.should include(@e2)
      end
      it "should include events on the start date" do
        r = Event.by_date_range(@sunday - 3,@sunday)
        r.should include(@e1)
      end
      it "should not return events beyond the end date" do
        Event.by_date_range(@sunday,@sunday + 2).should be_empty
      end
      it "should not return dates before the from date" do
        Event.by_date_range(@sunday - 2,@sunday + 2).should be_empty
      end
      it "should return relation of events in time range" do
        r = Event.by_date_range(@sunday - 4, @sunday + 4)
        r.should be_kind_of(ActiveRecord::Relation)
        r.should include(@e1)
        r.should include(@e2)
      end
    end
    describe "method for_week" do
      before(:each) do
        @monday = Date.today()
        @monday = @monday - @monday.wday + 1
        @e1 = FactoryGirl.create(:event, :start_date => @monday - 7)
        @e2 = FactoryGirl.create(:event, :start_date => @monday + 3)
        @e3 = FactoryGirl.create(:event, :start_date => (@monday - @monday.wday + 8))
      end
      it "should handle empty data sets" do
        Event.for_week(@monday - 1000).should be_empty
      end
      it "should not return events before the beginning of the requested week" do
        r = Event.for_week(@monday)
        r.should_not include(@e1)
      end
      it "should not return events beyond the end of the requested week" do
        r = Event.for_week(@monday - 7)
        r.should_not include(@e2)
      end
      it "should not return events at 12:00 AM Monday of the following week" do
        r = Event.for_week(@monday)
        r.should_not include(@e3)
      end
      it "should return a relation of events" do
        r = Event.for_week(@monday)
        r.should be_kind_of(ActiveRecord::Relation)
      end
    end
  end

end
