require 'spec_helper'
require 'cancan/matchers'

describe Ability do

  describe "permits" do
    subject { ability }
    let(:ability){ Ability.new(user) }
    let(:user){ nil }

    context "when is an application admin" do
      let(:user){ FactoryGirl.create(:application_admin) }
      it { should be_able_to(:manage, :all) }
      it { should be_able_to(:manage, :resque) }
      it { should be_able_to(:manage, :question) }
    end

    describe "for account users" do
      before(:each) do
        # create resources under a different account
        @user_a2 = FactoryGirl.create(:subscribed_user)
      end

      context "account admin" do
        let(:user){ FactoryGirl.create(:subscribed_admin) }
        it{ should_not be_able_to(:manage, :resque) }
        it{ should be_able_to(:manage, User) }
        it{ should_not be_able_to(:show, @user_a2) }
      end

      context "staff user" do
        user = FactoryGirl.create(:subscribed_user) 
        user2 = FactoryGirl.create(:user, :account => user.account)
        let(:user){ user }
        # test user profile access
        it{ should be_able_to(:update, user) }
        it{ should be_able_to(:show, user) }
        it{ should_not be_able_to(:create, User) }
        it{ should_not be_able_to(:show, user2) }
        it{ should_not be_able_to(:update, user2) }
        it{ should_not be_able_to(:show, @user_a2) }
      end

    end

    context "when public user" do
      let(:user){ User.new() }
      it{ should_not be_able_to(:list, FactoryGirl.create(:draft_question)) }
      it{ should be_able_to(:list, FactoryGirl.create(:published_question)) }
    end

  end
end
