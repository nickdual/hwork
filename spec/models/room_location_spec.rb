require 'spec_helper'

describe RoomLocation do
  it { should belong_to(:room) }
  it { should belong_to(:clinic) }
  it { should validate_presence_of(:room) }
  it { should validate_presence_of(:clinic) }
end
