require 'spec_helper'

describe SubscriptionPlan do
  it { should have_many(:subscriptions) }
  it { should validate_presence_of(:name) }
  it { should validate_numericality_of(:renewal_period) }
  it { should validate_numericality_of(:trial_period) }
end
