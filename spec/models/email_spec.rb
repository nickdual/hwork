require 'spec_helper'

describe Email do

  [ 'blabla.ru', 'blablabla#bla.ru', 'blablabla@123' ].each do |bad_email|
    it { should_not allow_value(bad_email).for(:email).with_message(/should be a valid email/) }
  end

  [ 'my@email.com' ].each do |good_email|
    it { should allow_value(good_email).for(:email) }
  end

end
