require 'spec_helper'

describe ProcedureCode do
  before(:each) do
    @account = FactoryGirl.create(:account)
    @procedure_code = FactoryGirl.create(:procedure_code, :account => @account)
  end

  #it { should have_many(:patient_visit_details) }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:description) }
  it { should belong_to(:account) }
  it { should validate_presence_of(:account) }
  it { should have_many(:fees) }
  it { should have_many(:fee_schedules) }

  # validation of the integer (mapped) fields
  [ :type_code, :service_type_code].each do |field|
    describe "integer (mapped) field " + field.to_s do
      map_name = field.to_s.pluralize.upcase
      v_list = eval "ProcedureCode::#{map_name}"
      key_accessor = (field.to_s + "_key").to_sym

      it " should contain a definition of the value map " do
        v_list.should be_kind_of(Hash)
      end

      describe "allows key values from value map" do
        v_list.keys.each do |v|
          it { should allow_value(v).for(field) }
        end
      end

      describe "allows value values from value map" do
        v_list.values.each do |v|
          it { should allow_value(v).for(field) }
        end
      end

      describe "defines key value accessor method" do
        it { should respond_to(key_accessor.to_sym) }
      end

      [ 'no good', 'not real' ].each do |p|
        it { should_not allow_value(p).for(field) }
      end
    end
  end

  it "should require cpt_code if procedure is service" do
    @procedure_code.stub(:is_service?).and_return(true)
    @procedure_code.cpt_code = nil
    @procedure_code.should_not be_valid
  end

  it "should be valid with a full set of valid attributes" do
    @procedure_code.should be_valid
  end

  it "should require cpt_code for service procedures" do
    proc_code = FactoryGirl.build(:procedure_code2, :type_code => ProcedureCode::SERVICE_TYPES[0], :account => @account)
    proc_code.cpt_code = nil
    proc_code.should_not be_valid
  end

  it "should not require a cpt_code for non-service_procedures" do
    proc_code = FactoryGirl.build(:procedure_code2, :type_code => ProcedureCode::ADJUSTMENT_TYPES[0], :account => @account)
    proc_code.cpt_code = nil
    proc_code.should be_valid
  end

  it "Bug #97 should accept floating point value tax_rate_percentage" do
    proc_code2 = FactoryGirl.create(:procedure_code2, :tax_rate_percentage => 8.35, :account => @account)
    proc_code2.should be_valid
    proc_code2.tax_rate_percentage.should eql(8.35)
  end


   describe "type_code rules" do
     it "should allow new codes to change to any type" do
       p = ProcedureCode.new
       p.get_select_type_codes.size.should eql(APP_CONFIG['options']['code_types'].size)
     end

     it "should allow unused codes to change to any type" do
       pending "waiting for the real patient_visits model"
       @procedure_code.get_select_type_codes.size.should eql(APP_CONFIG['options']['code_types'].size)
     end

     describe "for used codes" do
       before(:each) do
         @procedure_code.stub_chain(:patient_visit_details, :empty?).and_return(false)
       end

       #it "should only allow service types ==> service types" do
       #  @procedure_code.stub(:is_service?).and_return(true)
       #  @procedure_code.get_select_type_codes.size.should eql(ProcedureCode::SERVICE_TYPES.size)
       #end
       #
       #it "should only allow payment types ==> payment types" do
       #  @procedure_code.stub(:is_payment?).and_return(true)
       #  @procedure_code.get_select_type_codes.size.should eql(ProcedureCode::PAYMENT_TYPES.size)
       #end
       #
       #it "should only allow adjustment types ==> adjustment types" do
       #  @procedure_code.stub(:is_adjustment?).and_return(true)
       #  @procedure_code.get_select_type_codes.size.should eql(ProcedureCode::ADJUSTMENT_TYPES.size)
       #end
     end

   end

end
