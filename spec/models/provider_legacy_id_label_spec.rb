require 'spec_helper'

describe ProviderLegacyIdLabel do
  before(:each) do
    @account =  FactoryGirl.create(:account)
    @legacy_id_label = FactoryGirl.create(:legacy_id_label, :account => @account)
    @valid_attributes = {
      :legacy_id_label_id => @legacy_id_label.id
    }
  end

  it { should belong_to(:provider) }
  it { should belong_to(:legacy_id_label) }

  it "should create a new instance given valid attributes" do
    ProviderLegacyIdLabel.create!(@valid_attributes)
  end

end
