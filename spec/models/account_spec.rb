require 'spec_helper'

describe Account do

  before(:each) do
    @account = FactoryGirl.create(:account)
  end

  it { should have_many(:users) }
  it { should have_many(:providers) }
  it { should have_many(:clinics) }
  it { should have_many(:fee_schedules) }
  it { should have_many(:legacy_id_labels) }
  it { should have_many(:hcfa_form_options) }
  it { should have_many(:account_hcfa_form_options) }
  it { should have_many(:carrier_hcfa_form_options) }
  it { should have_many(:appointment_types) }
  it { should have_many(:procedure_codes) }
  it { should have_many(:diagnosis_codes) }
  it { should have_many(:third_parties) }
  it { should have_many(:references) }
  it { should have_many(:rooms) }
  it { should have_many(:patients) }
  it { should have_many(:messages) }
  it { should have_one(:subscription) }
  it { should have_one(:calendar) }
  it { should have_one(:account_setup) }

  describe "creation" do

    it "should be valid with a full set of valid attributes" do
      @account.should be_valid
    end

  end

  describe "account roles" do
    before(:each) do
      @admin = FactoryGirl.create(:subscribed_admin, :account => @account)
      @user = FactoryGirl.create(:subscribed_user, :account => @account)
    end

    it "should check for admin true" do
      @account.admin?(@admin).should be_true
    end

    it "should check for admin false" do
      @account.admin?(@user).should be_false
    end

  end

  describe "calendar" do
    it "should have one calendar after creation" do
      @account.calendar.should_not be_nil
    end
    describe "method build_calendar_if_nil" do
      it "should not modify the account calendar if existing" do
        id = @account.calendar.id
        @account.build_calendar_if_nil
        @account.calendar.id.should eql(id)
      end
    end
  end

  describe "owner" do
  end

  describe "setup process" do
    it "should indicate new accounts" do
      a = Account.new
      a.new_account?.should be_true
    end

    it "should indicate a NOT new account" do
      clinic = FactoryGirl.create(:clinic_with_account)
      clinic.account.new_account?.should be_false
    end

  end

  describe "initial configuration" do
    it "should have object groups" do
      account = FactoryGirl.build(:account)
      account.should_receive(:initialize_defaults)
      account.save!
    end
  end


  describe "method room_location_required?" do
    it "should return false if there is only 1 clinic" do
      clinic = double("Clinic")
      @account.stub(:clinics) { [ clinic ] }
      @account.room_location_required?.should be_false
    end

    it "should return false if there are no clinics" do
      @account.stub(:clinics) { [] }
      @account.room_location_required?.should be_false
    end

    it "should return true if there are multiple clinics with different_locations" do
      clinic = double("Clinic")
      clinic.stub(:different_location?) { true }
      @account.stub(:clinics) { [clinic, clinic] }
      @account.room_location_required?.should be_true
    end

    it "should return false with multiple clinics at the same location" do
      clinic = double("Clinic")
      clinic.stub(:different_location?) { false }
      @account.stub(:clinics) { [clinic, clinic] }
      @account.room_location_required?.should be_false
    end

    # TODO : fix the logic spec test about room_location_required? to reference the new separate_rooms_by_clinic setting
    #it "should return true if the preference setting rooms_by_clinic is true" do
    #  clinic = double("Clinic")
    #  clinic.stub(:different_location?) { false }
    #  @account.stub(:clinics) { [clinic, clinic] }
    #  @account.stub_chain(:account_preference,:rooms_by_clinic) { true }
    #  @account.room_location_required?.should be_true
    #end
  end

  describe "method has_multiple_locations?" do
    before(:each) do
      @clinic = double("Clinic")
    end

    it "should return false if there are no clinics" do
      @account.has_multiple_locations?.should be_false
    end

    it "should return false if there is just one clinic" do
      @account.stub(:clinics) { [ @clinic ] }
      @account.has_multiple_locations?.should be_false
    end

    it "should return true if there are multipe physical locations" do
      @clinic.stub(:different_location?) { true }
      @account.stub(:clinics) { [ @clinic, @clinic ] }
      @account.has_multiple_locations?.should be_true
    end
  end

end
