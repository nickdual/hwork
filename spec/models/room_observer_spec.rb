require 'spec_helper'

describe RoomObserver do
  describe "link to providers" do
    before(:each) do
      @provider = FactoryGirl.create(:provider)
    end
    it "should happen for single providers" do
      room = FactoryGirl.create(:room, :account => @provider.account)
      room.providers.should include(@provider)
    end
    it "should not happen for multiple providers" do
      provider2 = FactoryGirl.create(:provider, :account => @provider.account)
      room = FactoryGirl.create(:room, :account => @provider.account)
      room.providers.should be_empty
    end
  end

  describe "link to clinics" do
    before(:each) do
      @clinic = FactoryGirl.create(:clinic)
    end
    it "should happen for single clinic" do
      room = FactoryGirl.create(:room, :account => @clinic.account)
      room.clinics.should include(@clinic)
    end
    it "should happen for multiple clinics with 1 location" do
      clinic2 = FactoryGirl.create(:clinic_without_contact, :account => @clinic.account)
      clinic2.location_clinic = @clinic
      clinic2.save!
      room = FactoryGirl.create(:room, :account => @clinic.account)
      room.clinics.should include(@clinic)
      room.clinics.should include(clinic2)
    end
    it "should not happen for multiple locations" do
      clinic2 = FactoryGirl.create(:clinic, :account => @clinic.account)
      room = FactoryGirl.create(:room, :account => @clinic.account)
      room.clinics.should be_empty
    end
  end
end
