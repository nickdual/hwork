require 'spec_helper'

describe Question do
  before(:each) do
    @question = FactoryGirl.create(:question)
  end

  it "should be valid" do
    @question.should be_valid
  end

  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:body) }

  describe ".state" do
    it "should default to draft" do
      @question.draft?.should be_true
    end

    it "can be published" do
      @question.can_publish?.should be_true
    end
  end

end
