require 'spec_helper'

describe FeeSchedule do
  before(:each) do
    @a1 = FactoryGirl.create(:account)
    @fs = FactoryGirl.create(:fee_schedule, :account => @a1)
  end

  it { should belong_to(:account) }
  it { should have_many(:fees).dependent(:destroy) }
  it { should have_many(:procedure_codes) }
  it { should validate_presence_of(:label) }
  it { should validate_presence_of(:account) }

  describe "labels" do
    before(:each) do
      @a2 = FactoryGirl.create(:account)
    end

    it "are unique within accounts" do
      fs2 = FactoryGirl.build(:fee_schedule, :account => @a1, :label => @fs.label)
      fs2.should_not be_valid
    end

    it "can be duplicated in different accounts" do
      fs3 = FactoryGirl.create(:fee_schedule, :account => @a2)
      fs3.should be_valid
    end
  end

  describe "account scope functionality" do
    before(:each) do
      @a1 = FactoryGirl.create(:account, :clinic_name => "Account 1")
      @a2 = FactoryGirl.create(:account, :clinic_name => "Account 2")
      @fs1 = FactoryGirl.create(:fee_schedule, :account => @a1, :label => "Test Fee 50")
      @fs2 = FactoryGirl.create(:fee_schedule, :account => @a2, :label => "Test Fee 50")
    end


    it "should allow the same label in different accounts" do
      @fs1.should be_valid
      @fs2.should be_valid
    end

    it "should allow me to list fee schedules by account" do
      fss = FeeSchedule.by_account(@a1.id)
      fss.should include(@fs1)
    end

    it "Bug #123 should destroy fees when destroyed" do
      p_c = FactoryGirl.create(:procedure_code, :account => @a1)
      fee_id = FactoryGirl.create(:fee, :fee_schedule  => @fs1,:procedure_code => p_c).id
      @fs1.destroy
      lambda { Fee.find(fee_id) }.should raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
