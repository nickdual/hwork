require 'spec_helper'

describe Message do
  before(:each) do
    @message = FactoryGirl.create(:message)
  end

  it { should belong_to(:messageable) }
  it { should respond_to(:alt_id) }

  it "should create an instance given valid attributes" do
    m = Message.new(FactoryGirl.attributes_for(:message))
    m.should be_valid
  end

  it "should be valid with a full set of attributes" do
    @message.should be_valid
  end

end
