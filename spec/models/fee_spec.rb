require 'spec_helper'

describe Fee do
  it { should belong_to(:procedure_code) }
  it { should belong_to(:fee_schedule) }
  # TODO : add a test to verify the default value for is_percentage is false
end
