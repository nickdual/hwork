require 'spec_helper'

describe Phone do
  before(:each) do
    @phone = FactoryGirl.create(:phone)
  end

  describe "validations" do
    it "should accept valid attributes" do
      p1 = FactoryGirl.create(:phone, :number => '2125556789')
      p1.should be_valid
    end

    it "should reject short numbers" do
      p2 = Phone.create(:number => "555 345-124")
      p2.should_not be_valid
    end

    [ '123-123', '(12) 123-1234', '(123) 123-1234 x 12345678901', ].each do |bad_phone|
      it { should_not allow_value(bad_phone).for(:number).with_message(/invalid format/) }
    end

    [ '123-1234', '(123) 123-1234','(123) 123-1234 x 1234' ].each do |good_phone|
      it { should allow_value(good_phone).for(:number) }
    end

  end

  describe "method empty?" do
    it "should be true for a new phone" do
      p1 = Phone.new()
      p1.empty?.should be_true
    end
    it "should be false for a valid phone" do
      @phone.should be_valid
      @phone.empty?.should be_false
    end
  end

  describe "saves as numbers" do
    it "should strip normal formatting characters" do
      p1 = FactoryGirl.create(:phone, :number => '(123) 456-7890 x 12345')
      p1.should be_valid
      p1.number.should == '123456789012345'
    end
  end

  describe "phone method" do
    it "should return formated numbers (short)" do
      p1 = FactoryGirl.create(:phone, :number => '1234567')
      p1.phone.should == '123-4567'
    end

    it "should return formated numbers (w/ area code)" do
      p1 = FactoryGirl.create(:phone, :number => '5551234567')
      p1.phone.should == '(555) 123-4567'
    end

    it "should return formated numbers (w/ extension)" do
      p1 = FactoryGirl.create(:phone, :number => '55512345670011')
      p1.phone.should == '(555) 123-4567 x 0011'
    end
  end
end
