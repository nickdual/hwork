require 'spec_helper'

describe Clinic do

  before(:each) do
    @clinic = FactoryGirl.create(:clinic)
  end

  it { should have_one(:contact) }
  it { should have_one(:new_patient_option) }
  it { should have_many(:room_locations) }
  it { should have_many(:rooms) }
  it { should have_many(:patients) }
  it { should respond_to(:different_location?) }
  it { should respond_to(:alt_id) }

  it "should have a name" do
    c = FactoryGirl.create(:clinic)
    c.name.should_not be_nil
  end

  it "should have a name=" do
    c = FactoryGirl.create(:clinic)
    c.name = 'New Name'
    c.name.should == 'New Name'
  end

  it "should be valid with a full set of valid attributes" do
    @clinic.should be_valid
  end

  describe "scopes" do
    it "should be able to find by name" do
      clinic = FactoryGirl.create(:clinic)
      clinic.contact.company_name = 'ClinicT'
      clinic.save
      Clinic.find_by_name('ClinicT').should eql(clinic)
    end

    it "should return nil if no such name" do
      Clinic.find_by_name('NoClinic').should be_nil
    end
  end
  describe "method as_json" do
    before(:each) do
      @model = FactoryGirl.create(:clinic)
    end
    it { Clinic::JSON_OPTS.should be_a_kind_of(Hash) }
    it { should respond_to(:as_json) }
    #===========================================================================
    # NOTE: BLS, overriding does not behave the way you'd like it to
    #       as the options are going to get passed down the association
    #       tree, and thus misinterpreted.
    #===========================================================================
    it "should accept option arguments to override the JSON_OPTS" do
      r = @model.as_json(:only => [:created_at])
      r["created_at"].should_not be_nil
      r["id"].should be_nil
    end
  end
  describe "method build" do
    it "should create new_patient_option" do
      c = Clinic.build()
      c.new_patient_option.should_not be_nil
      c.valid?
      c.errors["new_patient_option.clinic"].should be_empty
    end
  end
  describe "method initialize_schedule" do
    it "should be called automatically after create" do
      pending
    end
    it "should not create a schedule if one already exists" do
      pending
    end
  end
end
