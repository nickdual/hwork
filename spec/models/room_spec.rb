require 'spec_helper'

describe Room do
  before(:each) do
    @room = FactoryGirl.create(:room)
  end

  it { should have_many(:appointments) }
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:account) }
  it { should have_many(:room_locations) }
  it { should have_many(:clinics) }
  it { should have_many(:provider_precedences) }
  it { should have_many(:providers).through(:provider_precedences) }

  it "should create a new instance given valid attributes" do
    FactoryGirl.create :room
  end

  it 'should be valid with a full set of valid attributes' do
    @room.should be_valid
  end

  describe "validates a single location" do
    before(:each) do
      @room2 = FactoryGirl.create(:room)
      @account = FactoryGirl.create(:account)
      @clinic1 = FactoryGirl.create(:clinic, :account => @account)
      @clinic2 = FactoryGirl.create(:clinic, :account => @account)
      # start these clinics off with the same address
      @clinic2.contact.physical_address.street = @clinic1.contact.physical_address.street
      @clinic2.save
      @room2.clinics<< @clinic1
      @room2.clinics<< @clinic2
    end

    it "should allow multiple clinics with same address" do
      @room2.clinics.size.should eql(2)
      @room2.should be_valid
    end

    it "should not allow clinics with different addresses" do
      @clinic2.contact.physical_address.street = "99 East Gate Dr."
      @clinic2.save
      @room2.should_not be_valid
      @room2.errors[:clinics].should include("can't be assigned to multiple physical locations")
    end
  end

  describe ".reorder" do
    before(:each) do
      @account = FactoryGirl.create(:account)
      @room1 = FactoryGirl.create(:room, account: @account, name: 'room1')
      @room2 = FactoryGirl.create(:room, account: @account, name: 'room2')
      @room3 = FactoryGirl.create(:room, account: @account, name: 'room3')
    end

    it "should NOT push negative positions" do
      @room1.position = 0
      @room2.position = 1
      Room.reorder(0, @room2, @account.rooms)
      @room1.position.should == 0
      @room2.position.should == 1
    end

    it "should NOT reorder if not necessary" do
      @room1.position = 0
      @room2.position = 1
      @room3.position = 2
      Room.reorder(0, @room3, @account.rooms)
      @room1.position.should == 0
      @room2.position.should == 1
      @room3.position.should == 2
    end
  end
end
