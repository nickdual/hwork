require 'spec_helper'

describe Recurrence do
  it { should belong_to(:event) }
  it { should validate_presence_of(:event) }

  [:interval, :count].each do |field|
    describe "integer values for #{field}" do
      it { should validate_numericality_of(field) }
      it { should allow_value(5).for(field) }
      it { should_not allow_value(10.1).for(field) }
    end
  end

  [ :interval_type ].each do |field|
    describe "integer (mapped) field #{field}" do
      map_name = field.to_s.pluralize.upcase
      v_list = eval "Recurrence::#{map_name}"
      key_accessor = "#{field}_key".to_sym

      it " should contain a definition of the value map " do
        v_list.should be_kind_of(Hash)
      end

      describe "allows key values from value map" do
        v_list.keys.each do |v|
          it { should allow_value(v).for(field) }
        end
      end

      describe "allows value values from value map" do
        v_list.values.each do |v|
          it { should allow_value(v).for(field) }
        end
      end

      describe "defines key value accessor method" do
        it { should respond_to(key_accessor.to_sym) }
      end

      [ 'no good', 'not real' ].each do |p|
        it { should_not allow_value(p).for(field) }
      end
    end
  end

  describe "default values" do
    before(:each) do
      @rc = Recurrence.new
    end

    it "should use 0 for recur_interval_type" do
      @rc.interval_type.should eql(0)
    end
  end

end
