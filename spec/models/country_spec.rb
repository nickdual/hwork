require 'spec_helper'

describe Country do
  describe "validations" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:alpha2) }
    it { should validate_presence_of(:alpha3) }
    it { should validate_presence_of(:numeric) }
    it { should ensure_length_of(:alpha2).is_equal_to(2) }
    it { should ensure_length_of(:alpha3).is_equal_to(3) }
    it { should ensure_length_of(:numeric).is_equal_to(3) }
  end

  describe "constants" do
    it "should provide array of all countries" do
      Country::COUNTRIES.should be_kind_of(Array)
    end

    it "should provider hash of alpha2 codes" do
      Country::COUNTRIES_A2.should be_kind_of(Hash)
    end

    it "should provider hash of alpha3 codes" do
      Country::COUNTRIES_A3.should be_kind_of(Hash)
    end

    it "should provider hash of numeric codes" do
      Country::COUNTRIES_NUMERIC.should be_kind_of(Hash)
    end
  end
end
