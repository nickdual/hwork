require 'spec_helper'

describe AppointmentType do
  before(:each) do
    @st = FactoryGirl.create(:appointment_type, :with_account)
  end

  describe("validations") do
    it { should validate_presence_of(:name) }

    [ :provider_time, :duration, :duration_min, :duration_max, :schedule_lead_time, :pre_appointment_gap, :post_appointment_gap ].each do |field|
      describe "integer values for #{field}" do
        it { should validate_presence_of(field) }
        it { should validate_numericality_of(field) }
        it { should allow_value(60).for(field) }
        it { should_not allow_value(field == :duration_min ? 10.5 : 90.5).for(field) }
        it { should_not allow_value(-1).for(field) }
      end
    end


    describe "name is" do
      it "unique for account" do
        st2 = FactoryGirl.build(:appointment_type, :account => @st.account, :name => @st.name)
        st2.should_not be_valid()
      end
      it "not unique across account entities" do
        st2 = FactoryGirl.build(:appointment_type, :with_account, :name => @st.name)
        st2.should be_valid()
      end
    end

    describe("default values") do
      before(:each) do
        @appointment_type = AppointmentType.new
      end

      it "should have 30 min duration" do
        @appointment_type.duration_min.should eql(30)
      end

      it "should have 90 max duration" do
        @appointment_type.duration_max.should eql(90)
      end

      it "should have 60 duration" do
        @appointment_type.duration.should eql(60)
      end

      it "should have 0 lead time" do
        @appointment_type.schedule_lead_time.should eql(0)
      end

      it "should have 0 pre appointment gap" do
        @appointment_type.pre_appointment_gap.should eql(0)
      end

      it "should have 0 post appointment gap" do
        @appointment_type.post_appointment_gap.should eql(0)
      end

      it "should not be public" do
        @appointment_type.is_public.should be(false)
      end

      it "should not be for new patients" do
        @appointment_type.for_new_patients.should be(false)
      end

    end

    describe "#public scope" do
      it "should provide public scope" do
        AppointmentType.should respond_to(:public)
      end
    
      describe "results" do
        before(:each) do
          @ap1 = FactoryGirl.create(:appointment_type, :with_account, :is_public => false)
          @acnt = @ap1.account
          @ap2 = FactoryGirl.create(:appointment_type, :account => @acnt, :is_public => true)
        end

        it "should include appointment types with is_public true" do
          @acnt.appointment_types.public.should include(@ap2)
        end

        it "should not include appointment types with is_public false" do
          @acnt.appointment_types.public.should_not include(@ap1)
        end
      end
      
    end

    describe "duration and provider time" do
      it "must be less than duration" do
        @st.should be_valid()
        @st.provider_time = @st.duration + 1
        @st.should_not be_valid()
      end
    end

    describe "duration_min and duration_max" do
      it "should be less than respectively" do
        FactoryGirl.build(:appointment_type,:duration_min => 9,:duration_max => 10, :duration => 9, :provider_time => 9).should be_valid()
      end
      it "can be equal" do
        FactoryGirl.build(:appointment_type,:duration_min => 10,:duration_max => 10, :duration => 10, :provider_time => 10).should be_valid()
      end
      it "should not be greater than" do
        FactoryGirl.build(:appointment_type,:duration_min => 11,:duration_max => 10, :duration => 10, :provider_time => 10).should_not be_valid()
      end
      it "with duration below min" do
        FactoryGirl.build(:appointment_type,:duration_min => 9,:duration_max => 10, :duration => 8, :provider_time => 8).should_not be_valid()
      end
      it "with duration above max" do
        FactoryGirl.build(:appointment_type,:duration_min => 9,:duration_max => 10, :duration => 11, :provider_time => 11).should_not be_valid()
      end
    end
  end

  describe "unique associations" do
    describe "to providers" do
      before(:each) do
        @p1 = FactoryGirl.create(:provider, :account => @st.account)
      end
      it "starts with no providers" do
        @st.providers.should be_empty
      end
      it "does not accept duplicates" do
        @st.providers << @p1
        @st.save
        @st.providers << @p1
        @st.save
        @st.providers.count.should be 1
      end
    end
    describe "to rooms" do
      before(:each) do
        @r1 = FactoryGirl.create(:room, :account => @st.account)
      end
      it "starts with no rooms" do
        @st.rooms.should be_empty
      end
      it "does not accept duplicates" do
        @st.rooms << @r1
        @st.save
        @st.rooms << @r1
        @st.save
        @st.rooms.count.should be 1
      end
    end
  end
end
