require 'spec_helper'

describe AccountHcfaFormOption do
  # validation of the string (mapped) fields
  [:state].each do |field|
    describe "string (mapped) field " + field.to_s do
      map_name = field.to_s.pluralize.upcase
      v_list = eval "AccountHcfaFormOption::#{map_name}"
      key_accessor = (field.to_s + "_key").to_sym

      it " should contain a definition of the value map " do
        v_list.should be_kind_of(Hash)
      end

      describe "allows value values from value map" do
        v_list.values.each do |v|
          it { should allow_value(v).for(field) }
        end
      end

      describe "allows key values from value map" do
        v_list.keys.each do |v|
          it { should allow_value(v).for(field) }
        end
      end

      describe "defines key value accessor method" do
        it { should respond_to(key_accessor.to_sym) }
      end

      [ 'no good', 'not real' ].each do |p|
        it { should_not allow_value(p).for(field) }
      end
    end
  end

  describe "key value accessors" do
    before(:each) do
      @af = AccountHcfaFormOption.new
    end

    it "should return key value" do
      AccountHcfaFormOption::STATES.keys.should include(@af.state_key)
    end
    it "should return mapped value by default" do
      AccountHcfaFormOption::STATES.values.should include(@af.state)
    end
  end
end
