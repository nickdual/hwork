require 'spec_helper'

describe Permission do
  before(:each) do
    @x = { 'Account' => { :children => [ 'Clinic' => { :max => 3 }, 'Provider' => { :configurable => false } ] } }
    @permission = Permission.new
  end

  it "should initialize with permissions" do
    @permission.children.should_not be_empty
  end

  it "children should be Permissions" do
    @permission.children.first.should be_kind_of(Permission)
  end

  it "private _config should return default value if not present" do
    p = Permission.new(@x)
    p.send(:_config,'foo',-1).should be_equal(-1)
  end

  it "should report a class as the resource" do
    @permission.children.first.resource.should be_kind_of(Class)
  end

  it "should be configurable by default" do
    p = Permission.new(@x)
    p.configurable?
  end

end
