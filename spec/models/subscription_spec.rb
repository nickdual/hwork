require 'spec_helper'

describe Subscription do
  describe "#update_plan" do
    before do
      @subscription = FactoryGirl.create(:account, :subscribed).subscription
      @plan1 = FactoryGirl.create(:subscription_plan, name: "silver")
      @plan2 = FactoryGirl.create(:subscription_plan, name: "gold")
      @base_count = SubscriptionPlan.all.count
      @subscription.stub(:update_stripe_plan) { true }
      @subscription.update_plan(@plan1)
    end

    it "updates the local subscription" do
      @subscription.subscription_plan_name.should == "silver"
      @subscription.update_plan(@plan2)
      @subscription.subscription_plan_name.should == "gold"
    end

    it "wont remove original role from database" do
      @subscription.update_plan(@plan2)
      SubscriptionPlan.all.count.should == @base_count
    end
  end

  describe "update_customer" do

    context "with a non-existing account" do

      before do
        successful_stripe_response = StripeHelper::Response.new("success")
        Stripe::Customer.stub(:create).and_return(successful_stripe_response)
        @subscription = FactoryGirl.create(:subscribed_admin).account.subscription
        @subscription.stub(:do_payments?) { true } 
      end

      it "creates a new account with a succesful stripe response" do
        @subscription.update_customer
        @subscription.subscription_payment_id.should eq("youAreSuccessful")
      end

    end
  end


end
