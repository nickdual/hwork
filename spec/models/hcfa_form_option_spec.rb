require 'spec_helper'

describe HcfaFormOption do
  before(:each) do
    @hf = HcfaFormOption.new
  end

  it { should belong_to(:account) }
  it { should belong_to(:third_party) }
  it { should respond_to(:alt_id) }

  describe "validations" do
    before(:each) do
      @hf.valid?
    end

    it { should validate_presence_of(:account) }
    it { should_not validate_presence_of(:state) }
    it { should_not validate_presence_of(:third_party_id) }

    # validation of the integer (mapped) fields
    [ :form_type_code, :box9a, :box9c, :box9d, :box9_s_name_is_same, :box11c, :box11d, :box24_diags, :box27, :box32a, :box33b, :box1a, :box29].each do |field|
      describe "integer (mapped) field " + field.to_s do
        map_name = field.to_s.pluralize.upcase
        v_list = eval "HcfaFormOption::#{map_name}"
        key_accessor = (field.to_s + "_key").to_sym

        it " should contain a definition of the value map " do
          v_list.should be_kind_of(Hash)
        end

        describe "allows key values from value map" do
          v_list.keys.each do |v|
            it { should allow_value(v).for(field) }
          end
        end

        describe "allows value values from value map" do
          v_list.values.each do |v|
            it { should allow_value(v).for(field) }
          end
        end

        describe "defines key value accessor method" do
          it { should respond_to(key_accessor.to_sym) }
        end

        [ 'no good', 'not real' ].each do |p|
          it { should_not allow_value(p).for(field) }
        end
      end
    end

    # validation of the boolean (mapped) fields
    [:box5, :plan_name, :address, :box4_g_name_is_same, :box4_7, :box7_g_addr_is_same, :box7_g_addr_is_p_addr, :box8, :box9, :box10, :box11, :box11a, :outside_lab, :print_diagnosis_description, :box21, :date_to, :date_from, :box24_cpt_97014, :total_owed, :clin_phone, :box32].each do |field|
      describe "boolean (mapped) field " + field.to_s do
        map_name = field.to_s.pluralize.upcase
        v_list = eval "HcfaFormOption::#{map_name}"
        key_accessor = (field.to_s + "_key").to_sym

        it " should contain a definition of the value map " do
          v_list.should be_kind_of(Hash)
        end

        describe "allows key values from value map" do
          v_list.keys.each do |v|
            it { should allow_value(v).for(field) }
          end
        end

        describe "allows value values from value map" do
          v_list.values.each do |v|
            it { should allow_value(v).for(field) }
          end
        end

        describe "defines key value accessor method" do
          it { should respond_to(key_accessor.to_sym) }
        end

        [ 'no good', 'not real' ].each do |p|
          it { should_not allow_value(p).for(field) }
        end
      end
    end

    # validate of the string hash types
    [ :date_auth,:date_format,:box17_upin,:box17_name,:box17_npi,:box19,:box24_tos,:box24_pos,:box24_modifier,:box24_amount_format,:box24j_npi,:box24j_legacy,:box27_30_format,:license,:box32_name,:box32b,:box33_name ].each do |field|
      describe "string field " + field.to_s do

        map_name = field.to_s.pluralize.upcase
        v_list = eval "HcfaFormOption::#{map_name}"

        it " should have a definition of the value list " do
          v_list.should be_kind_of(Array)
        end
        v_list.each do |v|
          it { should allow_value(v).for(field) }
        end
        [ 'no good', 'not real' ].each do |p|
          it { should_not allow_value(p).for(field) }
        end
      end
    end
  end

  describe "switcher behaviour" do
    it "bug #120 : should truncate long titles" do
      @hf.should_receive(:title).and_return("12345678901234567890123456")
      @hf.switcher_title.should eql("1234567890123456789012345..." )
    end
    it "bug #120 : should keep short titles" do
      @hf.should_receive(:title).and_return("12345678901234567890123")
      @hf.switcher_title.should eql("12345678901234567890123" )
    end
  end
end
