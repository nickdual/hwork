require 'spec_helper'

describe LegacyIdLabel do
  before(:each) do
    @account = FactoryGirl.create(:account)
    @label = FactoryGirl.create(:legacy_id_label, :account => @account)
  end

  it "should create a new instance given valid attributes" do
    @label.should be_valid
  end

  it { should validate_presence_of(:label) }
  it { should belong_to(:account) }
  it { should validate_presence_of(:account) }

  it "should have by_account scope" do
    # now include "state Lic #" default legacy_label_id
    LegacyIdLabel.by_account(@account.id).size.should be(1)
  end

  it "should validate label is unique" do
    ll = FactoryGirl.build(:legacy_id_label, :label => @label.label, :account => @account)
    ll.should_not be_valid
  end


  it "should validate label is unique - case insensitive" do
    ll = FactoryGirl.build(:legacy_id_label, :label => @label.label.swapcase, :account => @account)
    ll.should_not be_valid
  end

end
