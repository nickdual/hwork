#===============================================================================
# Define any custom exceptions to be used
#===============================================================================
module Errors

  class HandyworksError < StandardError
  end

  #-----------------------------------------------------------------------------
  # Role Assignment Error : raised when the assignment or revocation of a role
  # is going to voilate a business rule. 
  #   (a) There must be at least one 'admin' user assigned to each account.
  #-----------------------------------------------------------------------------
  class RoleAssignmentError < HandyworksError
  end

  #-----------------------------------------------------------------------------
  # This error is thrown when attempting to destroy a resource that is referenced
  # (foreign key) by other resources.
  #-----------------------------------------------------------------------------
  class ResourceReferencedError < HandyworksError
  end

  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  class CsvDuplicateError < RuntimeError
  end

end
