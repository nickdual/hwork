module NeedsTo
  def self.included(base)
    base.send :include, InstanceMethods
    base.extend ClassMethods
  end

  module ClassMethods
  end

  module InstanceMethods
    #---------------------------------------------------------------------------
    # looks for things the user needs to do and will add flash messages and/or 
    # return a redirection path to be used from the calling method.
    #  @param do_redirect boolean parameter indicating if redirection should
    #         be done or return the redirect url instead
    #---------------------------------------------------------------------------
    def check_needs(do_redirect=false)
      where_to = _check_needs
      if where_to and do_redirect then
        redirect_to where_to
      else
        return where_to
      end
    end

    private 

    #---------------------------------------------------------------------------
    #---------------------------------------------------------------------------
    def _check_needs
      return new_user_session_path unless current_user
      return destroy_user_session_path unless current_account
      # account is disabled or not usable
      unless current_account.can_login? or current_account.allow_request(request)
        flash[:notice] =  current_account.notice
        return destroy_user_session_path
      end
      # there is a problem with the account that an admin needs to
      # fix such as a payments issue.
      unless current_account.can_use? or current_account.allow_request(request)
        flash[:notice] = current_account.notice
        return eval current_account.fix_path(current_user)
      end
      # check the current user and its needs
      # check the current clinic and its needs
      # check the current patient and its needs
    end
  end
end
