module PublicAppointment
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods

    #---------------------------------------------------------------------------
    # validates the params required to produce a list of available times.
    #---------------------------------------------------------------------------
    def validate_times_inputs(params)
      return true if params[:clinic_id] and params[:appointment_type_id] and params[:provider_id] and params[:start_time]
      return false
    end

  end
end
