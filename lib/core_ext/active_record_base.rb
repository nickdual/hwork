"""
 From The Rails 3 Way, suggested to make this type of 
 functionality as extension to ActiveRecord::Base or
 plugin rather than a Module.

 This code comes from handyworks-r2/lib/module/addressable.rb
"""
module ActiveRecordExtensions
  def self.included(base)
    base.send :include, InstanceMethods
    base.extend ClassMethods
  end

  module ClassMethods
    def acts_as_addressable
      before_save :assign_addressable
      belongs_to :address, :validate => false, :dependent => :destroy
      accepts_nested_attributes_for :address
    end
  end

  module InstanceMethods
    def assign_addressable
      address.addressable = contact unless address.nil?
    end

    def encoding_for_resque?
      Kernel.caller.find_all{|frame| frame =~ /resque\/helpers/ }.size > 0
    end
  end
end
# include the extension 
ActiveRecord::Base.send(:include, ActiveRecordExtensions)
