require 'csv'

class ResourceSeeder

  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  def self.seed_clinics(account)
    index = 0
    Clinic.transaction do
      Import.get_csv_lib.foreach('db/seeds/csv/HW_Clinics.csv',
                  :headers           => Clinic.csv_headers,
                  :converters        => Clinic.csv_converters) do |row|
        index = index + 1
        #
        # check to see if this resource is already loaded
        #   currently not looking at third_party_id for uniqueness in this list.
        #
        rec = Clinic.load_csv_record('Clinic',nil,nil,account,row)
        unless rec.nil?
          unless rec.csv_duplicate('Clinic')
            # 
            # filter out any records which need to be skipped
            #
            rec.save(:validate => false)
            unless rec.valid? then
              puts "Clinic : #{rec.inspect} #{index} :: #{rec.errors}"
            end
          else
            puts "duplicated record #{rec.inspect}"
          end
        else
          puts "clinics skiping null record #{index}"
        end
      end
    end
  end

  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  def self.seed_providers(account)
    index = 0
    Provider.transaction do
      Import.get_csv_lib.foreach('db/seeds/csv/HW_Providers.csv',
                  :headers           => Provider.csv_headers,
                  :converters        => Provider.csv_converters) do |row|
        index = index + 1
        #
        # check to see if this resource is already loaded
        #   currently not looking at third_party_id for uniqueness in this list.
        #
        rec = Provider.load_csv_record('Provider',nil,nil,account,row)
        unless rec.nil?
          unless rec.csv_duplicate('Provider')
            # 
            # filter out any records which need to be skipped
            #
            rec.save(:validate => false)
            unless rec.valid? then
              puts "Provider : #{rec.inspect} #{index} :: #{rec.errors}"
            end
          else
            puts "duplicated record #{rec.inspect}"
          end
        else
          puts "providers skiping null record #{index}"
        end
      end
      # TODO : csv_link_default_providers is configured as a callback in the config.yml. Making this call automatically would help DRY resource_seeder up.
      Provider.csv_link_default_providers(nil,nil,account)
    end
  end

  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  def self.seed_legacy_id_labels(account)
    index = 0
    LegacyIdLabel.transaction do
      Import.get_csv_lib.foreach('db/seeds/csv/HW_Legacy_Labels.csv',
                  :headers           => LegacyIdLabel.csv_headers,
                  :converters        => LegacyIdLabel.csv_converters) do |row|
        index = index + 1
        #
        # check to see if this resource is already loaded
        #   currently not looking at third_party_id for uniqueness in this list.
        #
        rec = LegacyIdLabel.load_csv_record('LegacyLabel',nil,nil,account,row)
        unless rec.nil?
          unless rec.csv_duplicate('LegacyLabel')
            # 
            # filter out any records which need to be skipped
            #
            rec.save(:validate => false)
            unless rec.valid? then
              puts "LegacyIdLabel : #{rec.inspect} #{index} :: #{rec.errors}"
            end
          else
            puts "duplicated record #{rec.inspect}"
          end
        else
          puts "legacy label _id skiping null record #{index}"
        end
      end
    end
  end
  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  def self.seed_legacy_ids(account)
    index = 0
    ProviderLegacyIdLabel.transaction do
      Import.get_csv_lib.foreach('db/seeds/csv/HW_Legacies.csv',
                  :headers           => ProviderLegacyIdLabel.csv_headers,
                  :converters        => ProviderLegacyIdLabel.csv_converters) do |row|
        index = index + 1
        #
        # check to see if this resource is already loaded
        #   currently not looking at third_party_id for uniqueness in this list.
        #
        rec = ProviderLegacyIdLabel.load_csv_record('Legacy',nil,nil,account,row)
        unless rec.nil?
          unless rec.csv_duplicate('Legacy')
            # 
            # filter out any records which need to be skipped
            #
            rec.save(:validate => false)
            unless rec.valid? then
              puts "ProviderLegacyIdLabel : #{rec.inspect} #{index} :: #{rec.errors}"
            end
          else
            puts "duplicated record #{rec.inspect}"
          end
        else
          puts "legacy id skiping null record #{index}"
        end
      end
    end
  end
  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  def self.seed_third_parties(account)
    index = 0
    ThirdParty.transaction do
      Import.get_csv_lib.foreach('db/seeds/csv/HW_ThirdParties.csv',
                  :headers           => ThirdParty.csv_headers,
                  :converters        => ThirdParty.csv_converters) do |row|
        index = index + 1
        #
        # check to see if this resource is already loaded
        #   currently not looking at third_party_id for uniqueness in this list.
        #
        rec = ThirdParty.load_csv_record('ThirdParty',nil,nil,account,row)
        unless rec.nil?
          unless rec.csv_duplicate('ThirdParty')
            # 
            # filter out any records which need to be skipped
            #
            rec.save(:validate => false)
            unless rec.valid? then
              puts "ThirdParty : #{rec.inspect} #{index} :: #{rec.errors}"
            end
          else
            puts "duplicated record #{rec.inspect}"
          end
        else
          puts "third parties skiping null record #{index}"
        end
      end
    end
  end
  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  def self.seed_references(account)
    index = 0
    Reference.transaction do
      Import.get_csv_lib.foreach('db/seeds/csv/HW_Referrers.csv',
                  :headers           => Reference.csv_headers,
                  :converters        => Reference.csv_converters) do |row|
        index = index + 1
        #
        # check to see if this resource is already loaded
        #   currently not looking at third_party_id for uniqueness in this list.
        #
        rec = Reference.load_csv_record('Reference',nil,nil,account,row)
        unless rec.nil?
          unless rec.csv_duplicate('Reference')
            # 
            # filter out any records which need to be skipped
            #
            rec.save(:validate => false)
            unless rec.valid? then
              puts "Reference : #{rec.inspect} #{index} :: #{rec.errors}"
            end
          else
            puts "duplicated record #{rec.inspect}"
          end
        else
          puts "references skiping null record #{index}"
        end
      end
    end
  end
  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  def self.seed_diagnosis_codes(account)
    index = 0
    DiagnosisCode.transaction do
      Import.get_csv_lib.foreach('db/seeds/csv/HW_Diagnosis.csv',
                  :headers           => DiagnosisCode.csv_headers,
                  :converters        => DiagnosisCode.csv_converters) do |row|
        index = index + 1
        #
        # check to see if this resource is already loaded
        #   currently not looking at third_party_id for uniqueness in this list.
        #
        rec = DiagnosisCode.load_csv_record('Diagnoses',nil,nil,account,row)
        unless rec.nil?
          unless rec.csv_duplicate('Diagnoses')
            # 
            # filter out any records which need to be skipped
            #
            rec.save(:validate => false)
            unless rec.valid? then
              puts "DiagnosisCode : #{rec.inspect} #{index} :: #{rec.errors}"
            end
          else
            puts "duplicated record #{rec.inspect}"
          end
        else
          puts "diagnosis_code skiping null record #{index}"
        end
      end
    end
  end
  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  def self.seed_procedure_codes(account)
    index = 0
    ProcedureCode.transaction do
      Import.get_csv_lib.foreach('db/seeds/csv/HW_Procedures.csv',
                  :headers           => ProcedureCode.csv_headers,
                  :converters        => ProcedureCode.csv_converters) do |row|
        index = index + 1
        #
        # check to see if this resource is already loaded
        #   currently not looking at third_party_id for uniqueness in this list.
        #
        rec = ProcedureCode.load_csv_record('Procedure',nil,nil,account,row)
        unless rec.nil?
          unless rec.csv_duplicate('Procedure')
            # 
            # filter out any records which need to be skipped
            #
            rec.save(:validate => false)
            unless rec.valid? then
              puts "ProcedureCode : #{rec.inspect} #{index} :: #{rec.errors}"
            end
          else
            puts "duplicated record #{rec.inspect}"
          end
        else
          puts "procedure_code skiping null record #{index}"
        end
      end
    end
  end
  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  def self.seed_fee_labels(account)
    index = 0
    FeeSchedule.transaction do
      Import.get_csv_lib.foreach('db/seeds/csv/HW_Fee_Labels.csv',
                  :headers           => FeeSchedule.csv_headers,
                  :converters        => FeeSchedule.csv_converters) do |row|
        index = index + 1
        #
        # check to see if this resource is already loaded
        #   currently not looking at third_party_id for uniqueness in this list.
        #
        rec = FeeSchedule.load_csv_record('FeeLabel',nil,nil,account,row)
        unless rec.nil?
          unless rec.csv_duplicate('FeeLabel')
            # 
            # filter out any records which need to be skipped
            #
            rec.save(:validate => false)
            unless rec.valid? then
              puts "FeeLabel : #{rec.inspect} #{index} :: #{rec.errors}"
            end
          else
            puts "duplicated record #{rec.inspect}"
          end
        else
          puts "fee_label skiping null record #{index}"
        end
      end
    end
  end
  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  def self.seed_fees(account)
    index = 0
    Fee.transaction do
      Import.get_csv_lib.foreach('db/seeds/csv/HW_Fee_Schedules.csv',
                  :headers           => Fee.csv_headers,
                  :converters        => Fee.csv_converters) do |row|
        index = index + 1
        #
        # check to see if this resource is already loaded
        #   currently not looking at third_party_id for uniqueness in this list.
        #
        rec = Fee.load_csv_record('FeeSchedule',nil,nil,account,row)
        unless rec.nil?
          unless rec.csv_duplicate('FeeSchedule')
            # 
            # filter out any records which need to be skipped
            #
            rec.save(:validate => false)
            unless rec.valid? then
              puts "Fee : #{rec.inspect} #{index} :: #{rec.errors}"
            end
          else
            puts "duplicated record #{rec.inspect}"
          end
        else
          puts "fees skiping null record #{index}"
        end
      end
    end
  end
  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  def self.seed_messages(account)
    index = 0
    Message.transaction do
      Import.get_csv_lib.foreach('db/seeds/csv/HW_Letters.csv',
                  :headers           => Message.csv_headers,
                  :converters        => Message.csv_converters) do |row|
        index = index + 1
        #
        # check to see if this resource is already loaded
        #   currently not looking at third_party_id for uniqueness in this list.
        #
        rec = Message.load_csv_record('Letter',nil,nil,account,row)
        unless rec.nil?
          unless rec.csv_duplicate('Letter')
            # 
            # filter out any records which need to be skipped
            #
            rec.save(:validate => false)
            unless rec.valid? then
              puts "Message : #{rec.inspect} #{index} :: #{rec.errors}"
            end
          else
            puts "duplicated record #{rec.inspect}"
          end
        else
          puts "messages skiping null record #{index}"
        end
      end
    end
  end
  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  def self.seed_rooms(account)
    index = 0
    Room.transaction do
      Import.get_csv_lib.foreach('db/seeds/csv/HW_Rooms.csv',
                  :headers           => Room.csv_headers,
                  :converters        => Room.csv_converters) do |row|
        index = index + 1
        #
        # check to see if this resource is already loaded
        #   currently not looking at third_party_id for uniqueness in this list.
        #
        rec = Room.load_csv_record('Room',nil,nil,account,row)
        unless rec.nil?
          unless rec.csv_duplicate('Room')
            # 
            # filter out any records which need to be skipped
            #
            rec.save(:validate => false)
            unless rec.valid? then
              puts "Room : #{rec.inspect} #{index} :: #{rec.errors}"
            end
          else
            puts "duplicated record #{rec.inspect}"
          end
        else
          puts "rooms skiping null record #{index}"
        end
      end
    end
  end
  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  def self.seed_hcfa_form_options(account)
    index = 0
    HcfaFormOption.transaction do
      Import.get_csv_lib.foreach('db/seeds/csv/HW_HCFA_Setup.csv',
                  :headers           => HcfaFormOption.csv_headers,
                  :converters        => HcfaFormOption.csv_converters) do |row|
        index = index + 1
        #
        # check to see if this HCFA Form Option is already loaded
        #   currently not looking at third_party_id for uniqueness in this list.
        #
        hfo = HcfaFormOption.load_csv_record('HcfaFormOption',nil,nil,account,row)
        unless hfo.nil?
          unless hfo.csv_duplicate('HcfaFormOption')
            # 
            # BLS : filtering out some of these records for the moment
            #
            unless hfo.form_type_code.nil? or hfo.state.nil? or hfo.third_party_id != nil then
              hfo.save(:validate => false)
              unless hfo.valid? then
                puts "#{hfo.form_type_code} - #{hfo.state} : import hcfa_form_options error line #{index} #{hfo.errors}"
              end
            else
              #puts "skipping record #{index}"
            end
          else
            puts "duplicated record #{hfo}"
          end
        else
          puts "hcfa form opt skiping null record #{index}"
        end
      end
    end
  end
  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  def self.seed_patients(account)
    index = 0
    clinic  = account.clinics.first
    Patient.transaction do
      Import.get_csv_lib.foreach('db/seeds/csv/HW_Patients.csv',
                  :headers           => Patient.csv_headers,
                  :converters        => Patient.csv_converters) do |row|
        index = index + 1
        #
        # check to see if this resource is already loaded
        #   currently not looking at third_party_id for uniqueness in this list.
        #
        rec = Patient.load_csv_record('Patient',nil,clinic,account,row)
        unless rec.nil?
          unless rec.csv_duplicate('Patient')
            # 
            # filter out any records which need to be skipped
            #
            rec.save(:validate => false)
            unless rec.valid? then
              puts "Patient : #{rec.inspect} #{index} :: #{rec.errors}"
            end
          else
            puts "duplicated record #{rec.inspect}"
          end
        else
          puts "patients skiping null record #{index}"
        end
      end
      # TODO : csv_link_default_providers is configured as a callback in the config.yml. Making this call automatically would help DRY resource_seeder up.
      Patient.csv_link_patients(nil,nil,account)
    end
  end

end
