#-------------------------------------------------------------------------------
# Support for enumerated data types
#   from: http://gavin-mulligan.tumblr.com/post/3825225016
#    modified to support strings and arrays of symbols
#-------------------------------------------------------------------------------
module EnumSupport

  #-----------------------------------------------------------------------------
  # original method supports a word quoted list of strings like so:
  #    enum_sym :day, %w{mon tue wed thur fri sat sun }
  #      via : #{const_name} = %w{#{values.join(' ')}}.map{ |val| val.to_sym }
  #
  # original method results in 2 issues:
  #    1) you get a NoMethodError: undefined method `to_sym' for nil:NilClass
  #        when you call valid? against the uninitialize object
  #    2) also you get 2 errors for any blank enumerated field, it appears
  #        that the :inclusion validation is sufficient.
  #-----------------------------------------------------------------------------
  def enum(name, values)
    const_name = name.to_s.pluralize.upcase
    self.class_eval "
    #{const_name} = #{values.inspect}.map{ |val| val.to_sym }
            validates :#{name}, :inclusion => {:in => #{const_name}, :message => self.bad_enum_message(#{const_name})}
            def #{name}
                v = read_attribute(:#{name})
                v.to_sym if v
            end
            def #{name}=(value)
                write_attribute :#{name}, value.to_s
            end            
            "
  end

  #-----------------------------------------------------------------------------
  # allows you to use a hash to define the enumeration, the keys define a list
  # of acceptable input / output values (e.g. strings) and the values define
  # the codes which actually get stored for these keys. the mapping can be
  # considered completely hidden, but makes for more efficient transfer of values
  #
  #   example:
  #      enum_map :days, [ "Mon" => 0, "Tue" => 1 ]
  #    you can set the attribute with either a key or value, so make sure they're
  #    distinct sets
  #        h.days = 0
  #        h.days = "Mon"
  #
  #    by default when you read the value for this attribute, the value returned
  #    will be the mapped value, this would allow the majority of your application
  #    to be completely ignorant of the stored code values; however, in the event
  #    you need to read the value of the atribute directly, you can use the 
  #    <attribute>_raw method to access the uninverted code.
  #-----------------------------------------------------------------------------
  def enum_map(name, hash)
    const_name = name.to_s.pluralize.upcase
    self.class_eval "
            #{const_name} = hash
            validates :#{name}, :inclusion => {:in => #{const_name}.values, :message => self.bad_enum_message(#{const_name})}
            def #{name}
                read_attribute(:#{name})
            end
            def #{name}_key
                #{const_name}.invert[read_attribute(:#{name})]
            end
            def #{name}=(value)
                v = #{const_name}.values.map{|x| x.to_s }.include?(value.to_s) ? value : #{const_name}[value]
                write_attribute :#{name}, v
            end            
            "
  end

  #-----------------------------------------------------------------------------
  # allows you to use an array of integers for the enumeration
  #   example:
  #      enum_i :days, [ 0, -1, 2, 15, 27 ]
  #-----------------------------------------------------------------------------
  def enum_i(name, values)
    enum_str(name, values)
  end

  #-----------------------------------------------------------------------------
  # allows you to use an array of strings for the enumeration
  #   example:
  #      enum_str :days, [ 'mon', 'tue', 'wed', 'thur', 'fri', 'sat', 'sun' ]
  #-----------------------------------------------------------------------------
  def enum_str(name, values)
    const_name = name.to_s.pluralize.upcase
    self.class_eval "
    #{const_name} = values
            validates :#{name}, :inclusion => {:in => #{const_name}, :message => self.bad_enum_message(#{const_name})}
            def #{name}
                read_attribute(:#{name})
            end
            def #{name}=(value)
                write_attribute :#{name}, value
            end            
            "
  end

  protected

  def bad_enum_message(enum)
    if enum.kind_of? Hash
      "is not in the set (#{enum.keys.join(', ')})"
    else
      "is not in the set (#{enum.join(', ')})"
    end
  end
end
