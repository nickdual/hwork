#-------------------------------------------------------------------------------
# Handyworks Application Management & Migration Tasks
#  Migration
#    certain DML operations which you might think would be best left in a 
#    data migration step actually cause problems there because they don't 
#    evolve in the same way code does, so if your model changes (e.g. you 
#    change a column name) which was being manipulated during a DML operation,
#    then your old migration becomes invalid. this is a major pitfall as it
#    can bite you even during the short-term development of a single feature
#    when that feature ends up having multiple migrations before its initial
#    implementation is complete.
#
#    !!! Most migrations are one time only operations !!!
#
#  Management
#    create_app_admin
#       Creates a new account with the given username, password, email
#       and sets it up with the AppAdmin role and access to the GLOBAL DEFAULT
#       templates.
#-------------------------------------------------------------------------------
begin
  require 'highline'

  namespace :handyworks do
    namespace :manage do
      desc 'Reset help text from seed data'
      task :load_helps => :environment do
        result = `mysql -uhandyworks -p4dogs! #{Rails.configuration.database_configuration[Rails.env]['database']} < db/seeds/helps.sql`
      end
      desc 'Reset faq questions from seed data'
      task :load_questions => :environment do
        result = `mysql -uhandyworks -p4dogs! #{Rails.configuration.database_configuration[Rails.env]['database']} < db/seeds/questions.sql`
      end
      desc 'Load new HCFA form option defaults'
      task :load_hcfa_options => :environment do
        io = HighLine.new
        mp = io.ask("Enter application manager password:  ") { |q| q.echo = "*" }
        hwm = Handyworks::Manager.new
        hwm.authenticate_manager(mp)
        hwm.load_hcfa_options
      end
      desc 'Create an Application Admin account'
      task :create_app_admin => :environment do
        io = HighLine.new
        mp = io.ask("Enter application manager password:  ") { |q| q.echo = "*" }
        hwm = Handyworks::Manager.new
        hwm.authenticate_manager(mp)
        email = io.ask("Enter the new user's email:")
        password = io.ask("Enter the new user's password:  ") { |q| q.echo = "*" }
        hwm.create_app_admin(password, email)
      end
    end
    namespace :migrate do
      desc 'Cleanup empty phone fields'
      task :remove_empty_phones => :environment do
        Phone.where("number is null").each do |phone|
          phone.destroy
        end
      end
      desc 'to mark all existing imports as completed'
      task :complete_import_group_processing => :environment do
        ImportGroup.where("status not like 'Loaded%'").each do |ig|
          ig.status = "Done"
          ig.save
        end
      end
      desc 'to setup mobile phone fields for existing providers'
      task :create_provider_mobile_phones => :environment do
        Provider.all.each do |provider|
          if provider.contact and provider.contact.try(:mobile_phone).nil?
            provider.contact.build_mobile_phone
            provider.save
          end
        end
      end
      desc 'to setup contact fields for existing users'
      task :create_user_contacts => :environment do
        User.all.each do |user|
            unless user.contact
              user.contact = ContactUser.build(:contactable => user)
              user.save
            end
         end
      end
      desc 'copy down default appointment types, WARNING: this could add back deleted appointment types'
      task :initialize_default_appointment_types => :environment do
        Account.exclude_default.each do |account|
          account.initialize_appointment_types
        end
      end
      desc 'copy down default legacy_id_labels, WARNING: this could add back deleted labels'
      task :initialize_default_legacy_id_labels => :environment do
        Account.exclude_default.each do |account|
          account.initialize_legacy_id_labels
        end
      end
      desc 'ensure all accounts have associated dashboard objects'
      task :create_account_dashboards => :environment do
        Account.where('id not in (select account_id from dashboards)').each do |account|
          account.build_dashboard_if_nil
        end
      end
      desc 'ensure all accounts have associated calendar objects'
      task :create_account_calendars => :environment do
        Account.where('id not in (select account_id from calendars)').each do |account|
          account.build_calendar_if_nil
        end
      end
      desc 'ensure all accounts have associated option objects'
      task :create_account_preferences => :environment do
        Account.where('id not in (select account_id from account_preferences)').each do |account|
          account.build_account_preference_if_nil
        end
      end
      desc 'ensure all providers have schedules'
      task :create_provider_schedules => :environment do
        Provider.all.each do |provider|
          provider.initialize_schedule if provider.schedule.nil?
        end
      end
      desc 'ensure all clinics have schedules'
      task :create_clinic_schedules => :environment do
        Clinic.all.each do |clinic|
          clinic.initialize_schedule if clinic.schedule.nil?
        end
      end
      desc 'assign rooms primary providers'
      task :assign_providers_to_rooms => :environment do
        Room.all.each do |room|
          if room.provider.nil?
            room.provider = room.account.providers[0] if room.account.providers 
            room.save
          end
        end
      end
      desc 'theme change from [old] to [new], use the jQuery theme names'
      task :swap_theme, [:old, :new] => :environment do |t, args|
        old = args[:old]
        new = args[:new]
        raise "ERROR ==> you must specify the old and new theme names" unless old and new
        Theme.where('name = ?',old).each do |theme|
          theme.name = new
          theme.save
        end
      end
      namespace :payments do
        desc 'Create payment accounts for any Handyworks account missing a payment account'
        task :register_accounts => :environment do
          Subscription.register_all_accounts
        end
        desc 'Delete payment accounts for any Handyworks account which has one'
        task :unregister_accounts => :environment do
          Subscription.unregister_all_accounts
        end
        desc 'Reset payment accounts will delete and re-create the payment accounts'
        task :reset_accounts => :environment do
          Subscription.reset_accounts
        end
      end
    end
    namespace :assets do
      desc 'use jammit to package assets'
      task :jammit => [:environment] do
        require 'jammit'
        Jammit.package!
      end
    end
  end

rescue LoadError
  puts "Missing highline gem"
end
