#-------------------------------------------------------------------------------
# Augment the Rails stats output to better represent the makeup of this 
# application. Specifically this is intended to pick up
#  1. Cucumber Tests
#  2. View code
#  3. Coffeescript
#  4. Jasmine tests
#
# This technique comes from:
#  http://www.pervasivecode.com/blog/2007/06/28/hacking-rakestats-to-get-gross-loc/
#-------------------------------------------------------------------------------
namespace :spec do
  desc "Add files that DHH doesn't consider to be 'code' to stats"
  task :statsetup do
  require 'rails/code_statistics'

  class CodeStatistics
    alias calculate_statistics_orig calculate_statistics
    def calculate_statistics
      @pairs.inject({}) do |stats, pair|
        if 3 == pair.size
          stats[pair.first] = calculate_directory_statistics(pair[1], pair[2]); stats
        else
          stats[pair.first] = calculate_directory_statistics(pair.last); stats
        end
      end
    end
  end
  ::STATS_DIRECTORIES << ['Views',  'app/views', /\.(haml|erb|rb)$/]
  ::STATS_DIRECTORIES << ['JavaScripts', 'app/assets/javascripts', /\.(coffee|js)$/]
  ::STATS_DIRECTORIES << ['Styles', 'app/assets/stylesheets', /\.(scss|sass|less|css)$/]
  ::STATS_DIRECTORIES << ['Jasmine specs', 'spec/javascripts', /_spec\.(js|coffee)/]
  ::CodeStatistics::TEST_TYPES << "Jasmine specs" if File.exist?('spec/javascripts')
  end
end
task :stats => "spec:statsetup"  # Cuke
