require 'resque/tasks'
require 'resque_scheduler/tasks'

namespace :resque do
  desc 'ensure the environment is loaded before running resque tasks'
  task :setup => :environment do
    require 'resque'
    require 'resque_scheduler'
    require 'resque/scheduler'
    Resque.schedule = YAML.load_file(File.join(Rails.root.to_s, '/config/schedule.yml'))
  end
end
