#-------------------------------------------------------------------------------
# This module is meant to be included in the Base class of an STI model 
# hierarchy.
#
# Heavily inspired by a couple key blog posts
#   http://code.alexreisner.com/articles/single-table-inheritance-in-rails.html
#   http://coderrr.wordpress.com/2008/04/22/building-the-right-class-with-sti-in-rails/#comment-1826
# 
#  Thanks to Alex and coderrr!
#-------------------------------------------------------------------------------
module StiHelpers
  def self.included(base)
    base.send :include, InstanceMethods
    base.extend ClassMethods
  end

  module ClassMethods

    def select_options
      descendants.map{ |c| c.to_s }.sort
    end

    def inherited(child)
      child.instance_eval do
        def model_name
          sti_base_class.model_name
        end
      end
      super(child)
    end

    def sti_base_class
      if "#{self.superclass.name}" == "ActiveRecord::Base"
        return self
      end
      return superclass.sti_base_class
    end

    def new(*a, &b)
      if (self == sti_base_class) and (h = a.first).is_a? Hash and (type = h[:type] || h['type']) and (klass = type.constantize) != self
        raise "wtF hax #{klass} = #{self.name}!!"  unless klass < self  # klass should be a descendant of us
        return klass.new(*a, &b)
      end
      super(*a, &b)
    end
  end

  module InstanceMethods
    def sti_base_class
      self.class.sti_base_class
    end
    #def update_attirbutes(attributes={})
    #  if attributes[:type] 
    #    self.update_attribute(:type, attributes[:type])
    #  end
    #  super(attributes)
    #end
  end
end

