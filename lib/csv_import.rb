#===============================================================================
# To be Included in any class which is going to be used for CSV imports
#  defines the api necessar to do the imports
#===============================================================================
module CsvImport
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    # defines a polymorphic association back to the import record.
    #has_one :import_record, :as => :model
    # This does not work here, have to define the association from the other direction
    # only

    #-----------------------------------------------------------------------------
    # constants
    #-----------------------------------------------------------------------------
    # DATE_PATTERN = /^(?<month>\d\d)\.(?<day>\d\d)\.(?<year>\d+)$/ # TODO: named captures requires Ruby 1.9
    DATE_PATTERN = /^(\d\d)\.(\d\d)\.(\d+)$/

    #-----------------------------------------------------------------------------
    # indicates if the import is expected to have headers
    #-----------------------------------------------------------------------------
    def csv_headers
      true
    end
    #-----------------------------------------------------------------------------
    # defines the column conversions to be applied during import
    #-----------------------------------------------------------------------------
    def csv_converters
      [
        lambda { |f,info| csv_converter_dates(f,info) },
        lambda { |f,info| csv_converter_semicolons(f,info) }
      ]
    end

    def csv_converter_dates(f,info)
      begin
        date = DATE_PATTERN.match f
        month = $1
        day = $2
        year = $3
        if date.nil?
          return f
        else
          #c = Date.parse("#{self.year2to4(date[:year])}-#{date[:month]}-#{date[:day]}")
          c = Date.parse("#{self.year2to4(year)}-#{month}-#{day}")
          return c
        end
      rescue Exception => e
        puts "csv_converter_dates error:: #{e.message}"
        return f
      end
    end

    # in the Handyworks Access export, any field containing a comma replaces it with a semi-colon ';'
    # this will convert sem-colons back to commas.
    def csv_converter_semicolons(f,info)
      if f.index(';').nil?
        return f
      else
        return f.gsub(/;/,',')
      end
    end

    #-----------------------------------------------------------------------------
    # returns instance of the class to be used in loading CSV data
    #  override this method if you need to build associations to load
    #  the csv data into.
    #-----------------------------------------------------------------------------
    def get_csv_instance(import_type, account, row)
      self.new
    end

    #-----------------------------------------------------------------------------
    # determines the most appropriate configuration for this CSV load
    #  by looking for the first class in the hierarchy which has an actual
    #  configuration defined
    #-----------------------------------------------------------------------------
    def get_csv_config(import_type)
      # added to handle cases like Attorney which is no longer an independent
      # class, could also be used to handle multiple versions of the CSV import
      # definition for the same target class.
      r = APP_CONFIG['csv_import']['formats'][import_type.singularize]
      return r unless r.nil?

      # OPTIMIZE : there should be no need to keep class name based logic in get_csv_config, remove it.
      me = self
      while not me.nil? do
        r = APP_CONFIG['csv_import']['formats'][me.name]
        unless r.nil?
          return r
        else
          me = me.superclass
        end
      end
    end

    #-----------------------------------------------------------------------------
    # normalize the headers between configuration file and CSV file.
    # just making sure the spaces are stripped out for now.
    #-----------------------------------------------------------------------------
    def csv_header_lookup(field_name,csv_row)
      header = csv_row.headers.select { |h| h.to_s.strip == field_name }[0]
    end
    #-----------------------------------------------------------------------------
    # csv_skip_row determines based on a configured expression if the record 
    # should just be skipped. Allows for some rudimentary filtering processing
    #-----------------------------------------------------------------------------
    def csv_skip_row(config,csv_row)
      config.keys.each do |field_name|
        header = csv_header_lookup(field_name,csv_row)
        val = csv_row.field(header)
        s = "val#{config[field_name]}"
        if eval s
          return true
        end
      end
      return false
    end
    #-----------------------------------------------------------------------------
    # This method applies the mapping defined in the config.yml file under
    # csv_import.
    #-----------------------------------------------------------------------------
    def load_csv_record(import_type, user, clinic, account, csv_row)
      begin
        config = get_csv_config(import_type)
        if config['skip_condition'].nil? or not csv_skip_row(config['skip_condition'],csv_row)
          new_model = get_csv_instance(import_type,account,csv_row)
          new_model.send(config['account'],account) if config['account']
          new_model.send(config['clinic'],clinic) if config['clinic']
          config['fields'].keys.each do |field_name|
            # loops through the fields from the configuration (so fields can be referenced multiple times)
            # looks up the "best match" header from the CSV file and then sets the attribute as indicated
            # in the value for that field from the configuration.
            #
            # NOTE: after the Ruby 1.9.3 / Rails 3.2 upgrade results of the config['fields'][field_name]
            #       switched from Arrays to Strings. Not sure why. Now we'll handle both.
            header = csv_header_lookup(field_name,csv_row)
            maps = config['fields'][field_name]
            if maps.kind_of?(Array)
              maps.each do |to_field|
                logger.debug("new_model.#{to_field} = csv_row.field(header)")
                eval "new_model.#{to_field} = csv_row.field(header)" unless to_field.nil?
              end
            else
              logger.debug("new_model.#{maps} = csv_row.field(header)")
              eval "new_model.#{maps} = csv_row.field(header)" unless maps.nil?
            end
          end
          if config['record_callback']
            logger.info("CSV :: model callback #{config['record_callback']}")
            begin
              eval "new_model.#{config['record_callback']}(user, clinic, account, csv_row)"
            rescue Exception => e
              message = "exception: #{e.message}"
              logger.error("CSV :: CSV import record callback error: #{message}")
            end
          end
          logger.debug("CSV :: new model built: #{new_model.inspect}")
          return new_model
        else
          # intentionally skipping this record for some specified condition
          return nil
        end
      rescue => e
        logger.error("an exception occured #{e.message}")
        logger.error(e.backtrace)
        return nil
      end
    end

    #-----------------------------------------------------------------------------
    # helper methods
    #-----------------------------------------------------------------------------
    # converts a year value (integer) from 2 to 4 digit representation
    # TODO: enhance year2to4 method to accept a 4-digit year and echo it back.
    def year2to4(year2)
      # In Ruby when converting a string to an integer, you need to strip the leading 0's
      # because they indicate the number is in Octal format, and "08", "09" would be invalid
      year2 = Integer(year2.sub(/^0+/,'')) if year2.is_a? String
      if year2 <= Date.today.year - 2000
        year4 = year2 + 2000
      else
        year4 = year2 + 1900
      end
      return year4
    end
    
    # conversion from True --> 1 and False --> 0
    def convert_bool(name,value)
      value == "True" ? 1 : 0
    end

  end

  # Instance Methods

  #-----------------------------------------------------------------------------
  # determines if this record is a duplicate and returns the first duplicate
  # record matching this criteria
  #-----------------------------------------------------------------------------
  def csv_duplicate(import_type)
    dup_fields = self.class.get_csv_config(import_type)['duplicates']
    dup_chk = {}
    dup_fields.each do |field|
      dup_chk[field.to_sym] = self.send(field)
    end
    dup_list = self.class.where(dup_chk).order(:created_at)
    return dup_list.first
  end

  #-----------------------------------------------------------------------------
  # updates a current model instance from the attributes of another model 
  # instance. only those attributes configured in the import for this csv
  # model will be updated
  #-----------------------------------------------------------------------------
  def csv_update_attributes_from_import(import_type,import_model)
    fields = self.class.get_csv_config(import_type)['fields'].values
    fields.each do |field|
      field.each do |to_field|
        eval "self.#{to_field} = import_model.#{to_field}"
      end
    end
  end
end
