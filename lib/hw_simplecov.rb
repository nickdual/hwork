#-------------------------------------------------------------------------------
# code coverage tool, enabled based on COVERAGE environment variable.
#  - configuration is used by both RSpec and Cucumber
#  - if you modify this configuration you'll need to make sure it runs
#    in all 6 environments (rspec, cucumber) x (rake, command, and spork ).
#-------------------------------------------------------------------------------
if( ENV['COVERAGE'] == 'on' )
  require 'simplecov'
  require 'simplecov-rcov'
  class SimpleCov::Formatter::MergedFormatter
    def format(result)
      SimpleCov::Formatter::HTMLFormatter.new.format(result)
      SimpleCov::Formatter::RcovFormatter.new.format(result)
    end
  end
  # setup rcov like formatting for integration with jenkins
  SimpleCov.formatter = SimpleCov::Formatter::MergedFormatter
  SimpleCov.at_exit do
    # for braintree sandbox cleanup
    #
    # BLS : the next line is just opportunistic to inject a bit of code to 
    #       cleanup Braintree test case after ALL the test files have been 
    #       processed.
    # BLS - it would be "nice" to have similar global cleanup after cucumber
    #       test runs; however, the at_exit method does not work properly as
    #       best as I can determine at this time, so it seems sufficient that
    #       rspec will periodically do the dirty work to cleanup cucumber created
    #       Braintree objects.
    Handyworks::PaymentManager.delete_test_data
    puts "Coverage done"
    SimpleCov.result.format!
  end
  SimpleCov.start 'rails' do
    add_filter '/spec'
    add_filter '/features'
    merge_timeout '7200'
  end
end

