module ValidateAssociatedPresent
  #-----------------------------------------------------------------------------
  # this method prevents Rails from pre-emptively performing validations on 
  # the fax_phone fields and it will instead be driven by the validates_associated
  # configuration.
  #-----------------------------------------------------------------------------
 def validates_associated_if_present(name, key_field=nil)
   self.class_eval "
      validates_associated :#{name}, :if => :#{name}_present?
      def validate_associated_records_for_#{name}()
      end
      def #{name}_present?
        if self.#{name}.nil? then
          return false
        elsif not '#{key_field}'.blank? and self.#{name}.#{key_field}.blank? then
          return false
        else
          return true
        end
      end
   "
 end
end
